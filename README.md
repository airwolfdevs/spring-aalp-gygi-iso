![Alt text](https://airwolfdevs.bitbucket.io/images/readme-logo.png)
# Airwolf Arch Linux Pseudo-Graphical & YAD Graphical Installer #
## Powered by Spring ##
-----------------------
This maven project uses Spring Shell to create an ISO that will run the AALP-GYGInstaller. AAPL-GYGI is designed to
give both novice and experienced Linux users a single and elegant user experience to build a killer customized
Arch Linux installation. To learn more about AAPL-GYGI, please visit the homepage.

[Download ISO](https://drive.google.com/open?id=1A0RSiMlKyZRHAmB0stvdWIBkF1o0i7sR) | [Download JAR](https://drive.google.com/uc?export=download&id=1gaFNtTQhzWvsFNH5aKfOcDa2wGQwn7tN) | [Homepage](https://airwolfdevs.bitbucket.io)
-----------------------
## Screen Shots ##
![IMAGE](https://airwolfdevs.bitbucket.io/images/screen-shots/init-screen.png)
Help Screen

![IMAGE](https://airwolfdevs.bitbucket.io/images/screen-shots/commands.png)
Display status of Commands

-----------------------
### Requirements ###
* JDK/OpenJDK 10 & above
* Internet connection
### Commands ###
* __Built-In Commands__
    * These commands come standard with Spring Shell.  

* __Spring AALP-GYGI Shell Commands__
    * __build-installer__: Build the AALP-GYGInstaller
    * __build-iso__: Build the ISO file.
    * __run-next__: Run the next step whose status is neither DONE nor PASSED
    * __show-steps__: Show the steps & their status that will build either the AALP-GYGInstaller or the ISO.

### Build the AALP-GYGInstaller ###
1. Make sure the requirements specified above are fulfilled.
2. Download the executable JAR using the link above.
3. Open a terminal and cd to the directory the jar was downloaded to
4. Run the application by entering "_java -jar spring-aalp-gygi-iso.jar_" (without the double quotes)
5. When the shell prompt displays, enter "_build-installer_" (without the double quotes)

### Build the ISO ###
In addition to the requirements above, you will need to create a local repository and add some AUR packages that
are not only required by the AALP-GYGInstaller, but also by the build process.  

* __Create local repository__		
    1. mkdir -p /var/local/aalp-gygi
    2. Add the __html-xml-utils__ package
		1. git clone https://aur.archlinux.org/html-xml-utils.git /tmp/html-xml-utils
		2. cd /tmp/html-xml-utils
		3. makepkg -cs
		4. sudo repo-add /var/local/aalp-gygi/aalp-gygi.db.tar.gz html-xml-utils-7.7-1-x86_64.pkg.tar.xz
		5. sudo cp html-xml-utils-7.7-1-x86_64.pkg.tar.xz /var/local/aalp-gygi/.
    3. Add the __adp__ package
		1. git clone https://aur.archlinux.org/apg.git /tmp/apg
		2. cd /tmp/apg
		3. makepkg -cs
		4. sudo repo-add /var/local/aalp-gygi/aalp-gygi.db.tar.gz apg-2.2.3-4-x86_64.pkg.tar.xz
		5. sudo cp apg-2.2.3-4-x86_64.pkg.tar.xz /var/local/aalp-gygi/.
    4. Add the __phantomjs__ package
		1. git clone https://aur.archlinux.org/phantomjs-bin.git /tmp/phantomjs
		2. cd /tmp/phantomjs
		3. makepkg -cs
		4. sudo repo-add /var/local/aalp-gygi/aalp-gygi.db.tar.gz phantomjs-bin-2.1.1-2-x86_64.pkg.tar.xz
		5. sudo cp phantomjs-bin-2.1.1-2-x86_64.pkg.tar.xz /var/local/aalp-gygi/.
    5. Add the __yad__ package compiled from this [GitHub Repository](https://github.com/v1cont/yad)
		1. [Download compiled YAD package](https://bitbucket.org/airwolfdevs/spring-aalp-gygi-iso/src/master/src/main/resources/aalp-gygi/scripts/software-install/YAD/yad-0.42.0-2-x86_64.pkg.tar.xz)
		2. sudo repo-add /var/local/aalp-gygi/aalp-gygi.db.tar.gz yad-0.42.0-2-x86_64.pkg.tar.xz
		3. sudo cp yad-0.42.0-2-x86_64.pkg.tar.xz /var/local/aalp-gygi/.
    6. Download the executable JAR using the link above.
    7. Open a terminal and cd to the directory the jar was downloaded to
    8. Run the application as root by entering "_sudo java -jar spring-aalp-gygi-iso.jar_" (without the double quotes)
    9. When the shell prompt displays, enter "_build-iso_" (without the double quotes)
