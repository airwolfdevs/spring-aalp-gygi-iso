package com.bitbucket.kooshballtb.isocreator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IsoApplication {

	public static void main(String[] args) {
		SpringApplication.run(IsoApplication.class, args);
	}

}
