package com.bitbucket.kooshballtb.isocreator.commands;

import com.bitbucket.kooshballtb.isocreator.common.CommandStatusEnum;
import com.bitbucket.kooshballtb.isocreator.common.InstallerException;
import com.bitbucket.kooshballtb.isocreator.common.UtilityBean;
import com.bitbucket.kooshballtb.isocreator.config.YAMLConfig;
import com.bitbucket.kooshballtb.isocreator.models.IsoBuildCommands;
import com.bitbucket.kooshballtb.isocreator.services.commands.GenBashScriptFileService;
import com.bitbucket.kooshballtb.isocreator.services.jpa.IsoBuildCommandsService;
import com.bitbucket.kooshballtb.isocreator.shell.ProgressCounter;
import com.bitbucket.kooshballtb.isocreator.shell.ShellHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.BeanFactoryAnnotationUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.shell.standard.ShellCommandGroup;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.table.ArrayTableModel;
import org.springframework.shell.table.BorderStyle;
import org.springframework.shell.table.TableBuilder;
import org.springframework.shell.table.TableModel;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@ShellComponent
@ShellCommandGroup("Spring AALP-GYGI Shell Commands")
@Slf4j
public class CreateIsoCommand {
  private static final String[] COLUMN_NAMES = {"Cmd #", "Status", "Name", "Description"};
  private static final String BUILD_SCRIPT_NAME = "buildISO.sh";

  @Autowired
  private UtilityBean utilBean;

  @Autowired
  private ShellHelper shellHelper;

  @Autowired
  private YAMLConfig ymlConfig;

  @Autowired
  private ProgressCounter progressCounter;

  @Autowired
  private ApplicationContext appCtx;

  @Autowired
  @Qualifier("isoBuildCommandsServiceImpl")
  private IsoBuildCommandsService cmdSrvc;

  @Autowired
  @Qualifier("buildSetupServiceImpl")
  private GenBashScriptFileService setupSrvc;

  @Autowired
  @Qualifier("genDesktopConfigFileServiceImpl")
  private GenBashScriptFileService deskConfigSrvc;

  @Autowired
  @Qualifier("genRequiredFilesServiceImpl")
  private GenBashScriptFileService reqSrvc;

  @Autowired
  @Qualifier("genAurRepoListServiceImpl")
  private GenBashScriptFileService repoSrvc;

  @Autowired
  @Qualifier("genPostInstMenuServiceImpl")
  private GenBashScriptFileService postInstSrvc;

  @ShellMethod("Show Steps & Status")
  public void showSteps() {
    displayTable();
  }

  @ShellMethod("Build the ISO file.")
  public void buildISO() {
    List<IsoBuildCommands> cmdList = cmdSrvc.findAll();
    for (IsoBuildCommands isoCmd : cmdList) {
      if(!CommandStatusEnum.DONE.getTextVal().contentEquals(isoCmd.getStatus())) {
        runStep(isoCmd);
        if(CommandStatusEnum.ERROR.getTextVal().contentEquals(isoCmd.getStatus())) {
          break;
        }
      }
    }
    displayTable();
  }

  @ShellMethod("Run the next step")
  public void runNext() throws IOException, InstallerException {
    List<IsoBuildCommands> cmdList = cmdSrvc.findAll();
    Optional<IsoBuildCommands> optVal = cmdList.stream().filter(cmd -> (StringUtils.isBlank(cmd.getStatus()) ||
        cmd.getStatus().contentEquals("Error"))).findFirst();
    if(optVal.isPresent()) {
      runStep(optVal.get());
      displayTable();
    }
  }

  @ShellMethod("Build the AALP-GYGInstaller")
  public void buildInstaller() {
    List<IsoBuildCommands> cmdList = cmdSrvc.findAll();
    cmdList.remove(cmdList.size() - 1);
    for (IsoBuildCommands isoCmd : cmdList) {
      if(!CommandStatusEnum.DONE.getTextVal().contentEquals(isoCmd.getStatus())) {
        runStep(isoCmd);
        if(CommandStatusEnum.ERROR.getTextVal().contentEquals(isoCmd.getStatus())) {
          break;
        }
      }
    }
    displayTable();
  }

  private void runStep(IsoBuildCommands cmd) {
    CommandStatusEnum cmdStatus = CommandStatusEnum.RUNNING;
    cmd.setStatus(cmdStatus.getTextVal());
    cmdSrvc.saveOrUpdate(cmd);
    displayTable();
    GenBashScriptFileService srvc = BeanFactoryAnnotationUtils.qualifiedBeanOfType(
        appCtx.getAutowireCapableBeanFactory(), GenBashScriptFileService.class, cmd.getQualifierName());
    cmdStatus = srvc.execService();
    cmd.setStatus(cmdStatus.getTextVal());
    cmdSrvc.saveOrUpdate(cmd);
  }

  private void displayTable() {
    List<IsoBuildCommands> list = cmdSrvc.findAll();
    List<Object[]> tableRows = list.stream().map(t -> new Object[]{t.getId(), t.getStatus(), t.getCmdName(), t.getCmdDesc()}).collect(Collectors.toList());
    String msg = "AALP-GYGI Build ISO commands:";
    tableRows.add(0,COLUMN_NAMES);

    Object[][] tableData = tableRows.stream().toArray(Object[][]::new);
    TableModel model = new ArrayTableModel(tableData);
    TableBuilder tableBuilder = new TableBuilder(model);

    shellHelper.printInfo(" ");
    shellHelper.printInfo(msg);
    tableBuilder.addInnerBorder(BorderStyle.fancy_light);
    tableBuilder.addHeaderBorder(BorderStyle.fancy_double);
    shellHelper.print(tableBuilder.build().render(100));
  }
}
