package com.bitbucket.kooshballtb.isocreator.common;

public enum CommandStatusEnum {
  DONE("DONE"),
  ERROR("Error"),
  PASSED("PASSED"),
  PENDING(""),
  RUNNING("Running");

  private String textVal;

  CommandStatusEnum(String text) {
    this.textVal = text;
  }

  public String getTextVal() {
    return textVal;
  }
}
