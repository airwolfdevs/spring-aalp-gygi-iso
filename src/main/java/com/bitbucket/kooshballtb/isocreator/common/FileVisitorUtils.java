package com.bitbucket.kooshballtb.isocreator.common;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

import static java.nio.file.FileVisitResult.CONTINUE;
import static java.nio.file.FileVisitResult.SKIP_SUBTREE;

@Slf4j
public class FileVisitorUtils extends SimpleFileVisitor<Path> {
    private boolean skipSubDirs;
    private List<Path> fileVisitResults = new ArrayList<>();
    private Pattern regExPattern;
    private Path rootDir;

    @Autowired
    private UtilityBean utilBean;

    private FileVisitorUtils() {
    }

    private static class SingletonHelper {
        private static final FileVisitorUtils INSTANCE = new FileVisitorUtils();
    }

    public static FileVisitorUtils getInstance() {
        return SingletonHelper.INSTANCE;
    }

    public List<Path> getFileVisitResults() {
        return this.fileVisitResults;
    }

    public void setRegExPattern(Pattern p) {
        this.regExPattern = p;
    }

    public void setSkipSubDirs() {
        this.skipSubDirs = true;
    }

    /**
     * Print information about each type of file.
     */
    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attr) {
        if (attr.isSymbolicLink()) {
            log.debug(String.format("Symbolic link: %s ", file));
        } else if (attr.isRegularFile()) {
            log.debug(String.format("Regular file: %s ", file));
            boolean foundFile =
                    !Objects.nonNull(this.regExPattern) || this.regExPattern.matcher(file.getFileName().toString()).matches();
            if (foundFile) {
                this.fileVisitResults.add(file);
            }
        } else {
            log.debug(String.format("Other: %s ", file));
        }
        return CONTINUE;
    }

    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
        FileVisitResult contOrSkip = CONTINUE;
        if(Objects.isNull(rootDir)) {
            this.rootDir = dir;
        } else if (this.skipSubDirs) {
            contOrSkip = SKIP_SUBTREE;
        }
        return contOrSkip;
    }
    /**
     * Print each directory visited.
     */
    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) {
        log.debug(String.format("Directory: %s%n", dir));
        return CONTINUE;
    }

    /**
     * If there is some error accessing the file, let the user know.
     */
    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) {
        log.error(String.format("Error:  Visit File Failed: [%s]!\n%s", file,
                utilBean.getCustomStackTrace("Stack Trace:", exc)));
        return CONTINUE;
    }
}
