package com.bitbucket.kooshballtb.isocreator.common;

public class InstallerException extends Exception {

  public InstallerException() {
    super();
  }

  public InstallerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

  public InstallerException(String message, Throwable cause) {
    super(message, cause);
  }

  public InstallerException(String message) {
    super(message);
  }

  public InstallerException(Throwable cause) {
    super(cause);
  }

}
