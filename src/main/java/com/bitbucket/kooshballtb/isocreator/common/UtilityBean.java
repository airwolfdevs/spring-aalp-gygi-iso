package com.bitbucket.kooshballtb.isocreator.common;

import ch.qos.logback.core.CoreConstants;
import com.bitbucket.kooshballtb.isocreator.shell.ProgressCounter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.http.impl.client.CloseableHttpClient;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermission;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class UtilityBean {
  public static final Map<String, Map<String, String>> MAP_FILE_URLS = new LinkedHashMap<>() {{
    put("appList", new HashMap<>() {{
      put("file", "/tmp/list_of_applications.html");
      put("hrSize", "1 MB");
      put("url", "https://wiki.archlinux.org/index.php/list_of_applications");
    }});
    put("aurRepos", new HashMap<>() {{
      put("file", "/tmp/unofficial_user_repositories.html");
      put("hrSize", "83 KB");
      put("url", "https://wiki.archlinux.org/index.php/unofficial_user_repositories");
    }});
    put("desktopEnvironments", new HashMap<>() {{
      put("file", "/tmp/desktop_environments.html");
      put("hrSize", "40 KB");
      put("url", "https://wiki.archlinux.org/index.php/Desktop_environment");
    }});
    put("displayManager", new HashMap<>() {{
      put("file", "/tmp/display_managers.html");
      put("hrSize", "34 KB");
      put("url", "https://wiki.archlinux.org/index.php/display_manager");
    }});
    put("kde", new HashMap<>() {{
      put("file", "/tmp/kde-themes.html");
      put("hrSize", "26 KB");
      put("url", "https://aur.archlinux.org/packages.php?K=plasma+theme");
    }});
    put("windowManager", new HashMap<>() {{
      put("file", "/tmp/window_managers.html");
      put("hrSize", "71 KB");
      put("url", "https://wiki.archlinux.org/index.php/window_manager");
    }});
  }};

  public static final String ID_COL = "id";

  private static final String ESCAPE_DOUBE_QUOTE = new StringBuilder().append('\\').append('"').toString();

  private static final int MAX_LENGTH = 75;
  public static final Set<PosixFilePermission> SCRIPT_PERMS = new HashSet<>() {
    {
      // Add read, write and execute permission to owner.
      add(PosixFilePermission.OWNER_READ);
      add(PosixFilePermission.OWNER_WRITE);
      add(PosixFilePermission.OWNER_EXECUTE);

      // Add read and execute permission to owner group users.
      add(PosixFilePermission.GROUP_READ);
      add(PosixFilePermission.GROUP_EXECUTE);

      // Add read and execute permission to other group users
      add(PosixFilePermission.OTHERS_READ);
      add(PosixFilePermission.OTHERS_EXECUTE);
    }
  };

  private static final Set<PosixFilePermission> NON_SCRIPT_PERMS = new HashSet<>() {
    {
      // Add read and write permission to owner.
      add(PosixFilePermission.OWNER_READ);
      add(PosixFilePermission.OWNER_WRITE);

      // Add read permission to owner group users.
      add(PosixFilePermission.GROUP_READ);

      // Add read permission to other group users
      add(PosixFilePermission.OTHERS_READ);
      add(PosixFilePermission.OTHERS_EXECUTE);
    }
  };

  public Document getDocument(String key) throws IOException, InstallerException {
    Map<String, String> map = MAP_FILE_URLS.get(key);
    Path htmlFile = Paths.get(map.get("file"));
    Document doc;

    if (htmlFile.toFile().exists()) {
      doc = Jsoup.parse(htmlFile.toFile(), "UTF-8");
    } else {
      throw new InstallerException("HTML File: [" + htmlFile.toString() + "] was NOT found!!!!!!!");
    }
    return doc;
  }

  public String execCmd(Logger logger, List<String> cmdList) {
    ProcessBuilder pb = new ProcessBuilder();
    String cmd = StringUtils.join(cmdList.iterator(), ' ');
    Path baseDir = Paths.get(System.getProperty("user.dir"));
    String errMsg = "";
    pb.directory(baseDir.toFile());
    pb.command("sh", "-c", cmd);
    try {
      logger.debug("Executing command: [{}].......", cmd);
      Process process = pb.start();
      int exitCode = process.waitFor();
      if (exitCode > 1) {
        InputStream is = process.getErrorStream();
        byte[] errorBytes = is.readAllBytes();
        errMsg = new String(errorBytes);
      } else if (exitCode > 0) {
        BufferedReader br = new BufferedReader(
            new InputStreamReader(process.getInputStream()));
        errMsg = br.readLine();
        br.close();
      }
      logger.debug("Process finished with exitCode: [{}]!", exitCode);
    } catch (IOException e) {
      errMsg = String.format("Unable to start/run command [%s].", cmd);
      logger.error("Unable to start/run command [{}].", cmd, e);
    } catch (InterruptedException e) {
      errMsg = String.format("Error waiting on command [%s].", cmd);
      logger.error("Error waiting on command [{}].", cmd, e);
    }

    return errMsg;
  }

  public void printHeader(PrintWriter printWriter) {
    printWriter.println("#!/bin/bash");
    printWriter.println();
    printWriter.println("#======================================================================================");
    printWriter.println("#");
    printWriter.println("# Author  : Thomas Bednarek");
    printWriter.println("# License : This program is free software: you can redistribute it and/or modify");
    printWriter.println("#           it under the terms of the GNU General Public License as published by");
    printWriter.println("#           the Free Software Foundation, either version 3 of the License, or");
    printWriter.println("#           (at your option) any later version.");
    printWriter.println("#");
    printWriter.println("#           This program is distributed in the hope that it will be useful,");
    printWriter.println("#           but WITHOUT ANY WARRANTY; without even the implied warranty of");
    printWriter.println("#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the");
    printWriter.println("#           GNU General Public License for more details.");
    printWriter.println("#");
    printWriter.println("#           You should have received a copy of the GNU General Public License");
    printWriter.println("#           along with this program.  If not, see <http://www.gnu.org/licenses/>");
    printWriter.println("#======================================================================================");
    printWriter.println();
    printWriter.println("#===============================================================================");
    printWriter.println("# globals");
    printWriter.println("#===============================================================================");
    printWriter.println();
  }

  public void addMenuOptsArray(String varName, List<String> menuOpts, PrintWriter printWriter) {
    List<String> list = new ArrayList<>();

    printWriter.printf("declare %s=(", varName);
    for (String opt : menuOpts) {
      list.add(String.format("\"%s\"", opt));
      if (list.size() > 4) {
        printWriter.printf("%s", StringUtils.join(list.iterator(), ' '));
        printWriter.println();
        list.clear();
      }
    }

    if (list.size() > 0) {
      printWriter.printf("%s", StringUtils.join(list.iterator(), ' '));
    }

    printWriter.println(")");
  }

  public void addAURPckgsArray(Map<String, String> aurPckgs, String varName, PrintWriter printWriter) {
    List<String> list = new ArrayList<>();

    printWriter.printf("declare -A %s=(", varName);

    for (Map.Entry<String, String> entry : aurPckgs.entrySet()) {
      list.add(String.format("[\"%s\"]=\"%s\"", entry.getKey(), entry.getValue()));
      if (list.size() > 3) {
        printWriter.printf("%s", StringUtils.join(list.iterator(), ' '));
        printWriter.println();
        list.clear();
      }
    }

    if (list.size() > 0) {
      printWriter.printf("%s", StringUtils.join(list.iterator(), ' '));
    }

    printWriter.println(")");
  }

  public void addAURDescsArray(Map<String, String> aurPckgDescs, String varName, PrintWriter printWriter) {
    printWriter.printf("declare -A %s=(", varName);

    for (Map.Entry<String, String> entry : aurPckgDescs.entrySet()) {
      printWriter.println(String.format("[\"%s\"]=\"%s\"", entry.getKey(), entry.getValue()));
    }

    printWriter.println(")");
  }

  public String getDescription(String desc) {
    if (desc.length() > MAX_LENGTH) {
      int idx = desc.indexOf(' ', MAX_LENGTH);
      if (idx > 0) {
        desc = desc.substring(0, idx) + " ...";
      }
    }

    String[] splitArray = StringUtils.split(desc, '"');

    return (splitArray.length > 1) ? StringUtils.join(splitArray, ESCAPE_DOUBE_QUOTE) : desc;
  }

  public void setPermissions(String baseDir) throws IOException {
    Path dir = Paths.get(System.getProperty("user.dir"), baseDir);
    setScriptPermissions(dir);
  }

  public void setScriptPermissions(Path dir) throws IOException {
    String[] extensions = new String[]{"sh"};
    List<File> files = (dir.endsWith("usr/bin"))
        ? Files.list(dir).filter(Files::isRegularFile).map(p -> p.toFile()).collect(Collectors.toList())
        : (List<File>) FileUtils.listFiles(dir.toFile(), extensions, true);

    for (File file : files) {
      Files.setPosixFilePermissions(file.toPath(), SCRIPT_PERMS);
    }
  }

  public void setPermissions(List<Path> files) throws IOException {
    for (Path file : files) {
      String ext = FilenameUtils.getExtension(file.getFileName().toString());
      if(ext.equalsIgnoreCase("sh")) {
        Files.setPosixFilePermissions(file, SCRIPT_PERMS);
      } else {
        Files.setPosixFilePermissions(file, NON_SCRIPT_PERMS);
      }
    }
  }

  public String getCustomStackTrace(String msg, Throwable t) {
    StringBuilder customStackTrace = new StringBuilder(msg);
    Throwable rootCause = ExceptionUtils.getRootCause(t);

    if (Objects.nonNull(rootCause) && !rootCause.getClass().isAssignableFrom(t.getClass())) {
      customStackTrace.append(CoreConstants.LINE_SEPARATOR).append("*************************************************************");
      customStackTrace.append(CoreConstants.LINE_SEPARATOR).append("*                    Root Cause Exception                   *");
      customStackTrace.append(CoreConstants.LINE_SEPARATOR).append("*************************************************************");
      customStackTrace.append(getCustomStackTrace(rootCause));
    }
    customStackTrace.append(CoreConstants.LINE_SEPARATOR).append("*************************************************************");
    customStackTrace.append(CoreConstants.LINE_SEPARATOR).append("*                      Full Stack Trace                     *");
    customStackTrace.append(CoreConstants.LINE_SEPARATOR).append("*************************************************************");
    customStackTrace.append(getCustomStackTrace(t));
    return customStackTrace.toString();
  }

  public String getCustomStackTrace(Throwable exc) {
    StringBuilder customStackTrace = new StringBuilder(CoreConstants.LINE_SEPARATOR);
    StackTraceElement[] ary = exc.getStackTrace();
    List<StackTraceElement> stackTraceElements = (!Objects.isNull(ary)) ? Arrays.asList(ary) : new ArrayList<>();

    customStackTrace.append(exc.toString()).append(CoreConstants.LINE_SEPARATOR)
            .append(StringUtils.join(stackTraceElements.iterator(), CoreConstants.LINE_SEPARATOR));

    return customStackTrace.toString();
  }

  public void initDirFile(Path dir, Path file) throws IOException {
    if (!dir.toFile().exists()) {
      Files.createDirectories(dir);
    } else {
      Files.deleteIfExists(file);
    }
  }
}
