package com.bitbucket.kooshballtb.isocreator.config;

import com.bitbucket.kooshballtb.isocreator.common.FileVisitorUtils;
import com.bitbucket.kooshballtb.isocreator.common.UtilityBean;
import com.bitbucket.kooshballtb.isocreator.shell.ProgressCounter;
import com.bitbucket.kooshballtb.isocreator.shell.ShellHelper;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.apache.http.ssl.SSLContexts;
import org.jline.terminal.Terminal;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import javax.net.ssl.SSLContext;
import java.security.GeneralSecurityException;

@ComponentScan("com.bitbucket.kooshballtb")
@Configuration
public class ApplicationConfig {

  @Bean
  public ShellHelper shellHelper(@Lazy Terminal terminal) {
    return new ShellHelper(terminal);
  }

  @Bean
  public CloseableHttpClient httpClient() throws GeneralSecurityException {
    TrustStrategy acceptingTrustStrategy = (cert, authType) -> true;
    SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
    SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext,
        NoopHostnameVerifier.INSTANCE);

    Registry<ConnectionSocketFactory> socketFactoryRegistry =
        RegistryBuilder.<ConnectionSocketFactory> create()
            .register("https", sslsf)
            .register("http", new PlainConnectionSocketFactory())
            .build();

    BasicHttpClientConnectionManager connectionManager =
        new BasicHttpClientConnectionManager(socketFactoryRegistry);
    RequestConfig reqConf =  RequestConfig.custom()
        .setCircularRedirectsAllowed(true)
        .build();
    CloseableHttpClient httpClient = HttpClients.custom()
        .setRedirectStrategy(new LaxRedirectStrategy())
        .setDefaultCookieStore(new BasicCookieStore())
        .setSSLSocketFactory(sslsf)
        .setConnectionManager(connectionManager)
        .setUserAgent("Mozilla/5.0 Firefox/26.0")
        .build();

    return httpClient;
  }

  @Bean
  public ProgressCounter progressCounter(@Lazy Terminal terminal) {
    return new ProgressCounter(terminal);
  }

  @Bean(name = "utilBean")
  public UtilityBean getUtilityBean() {
    return new UtilityBean();
  }

  @Bean(name = "fileVisitor")
  public FileVisitorUtils getFileVisitorUtils() {
    return FileVisitorUtils.getInstance();
  }

}
