package com.bitbucket.kooshballtb.isocreator.config;

import com.querydsl.sql.HSQLDBTemplates;
import com.querydsl.sql.SQLQueryFactory;
import com.querydsl.sql.SQLTemplates;
import com.querydsl.sql.spring.SpringConnectionProvider;
import com.querydsl.sql.spring.SpringExceptionTranslator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.inject.Provider;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.sql.Connection;
import java.util.Properties;

@Configuration
@EnableJpaRepositories(basePackages = "com.bitbucket.kooshballtb.isocreator.repositories")
@EnableTransactionManagement
public class DatabaseConfig {

  @Autowired
  private Environment env;

  @Autowired
  private DataSource ds;

  @Bean
  public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
    final LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
    em.setDataSource(ds);
    em.setPackagesToScan(new String[] { "com.bitbucket.kooshballtb.isocreator.models" });
    em.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
    em.setJpaProperties(additionalProperties());
    return em;
  }

  @Bean
  JpaTransactionManager transactionManager(final EntityManagerFactory entityManagerFactory) {
    final JpaTransactionManager transactionManager = new JpaTransactionManager();
    transactionManager.setEntityManagerFactory(entityManagerFactory);
    return transactionManager;
  }

  final Properties additionalProperties() {
    final Properties hibernateProperties = new Properties();
    if (env.getProperty("hibernate.hbm2ddl.auto") != null) {
      hibernateProperties.setProperty("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));
    }
    if (env.getProperty("hibernate.dialect") != null) {
      hibernateProperties.setProperty("hibernate.dialect", env.getProperty("hibernate.dialect"));
    }
    if (env.getProperty("hibernate.show_sql") != null) {
      hibernateProperties.setProperty("hibernate.show_sql", env.getProperty("hibernate.show_sql"));
    }
    return hibernateProperties;
  }

  @Bean
  public JdbcTemplate jdbcTempate() {
    JdbcTemplate template = new JdbcTemplate();
    template.setDataSource(ds);
    return template;
  }
}
