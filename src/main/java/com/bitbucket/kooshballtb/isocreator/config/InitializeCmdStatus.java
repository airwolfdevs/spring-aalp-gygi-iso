package com.bitbucket.kooshballtb.isocreator.config;

import com.bitbucket.kooshballtb.isocreator.common.CommandStatusEnum;
import com.bitbucket.kooshballtb.isocreator.models.IsoBuildCommands;
import com.bitbucket.kooshballtb.isocreator.services.commands.GenBashScriptFileService;
import com.bitbucket.kooshballtb.isocreator.services.jpa.IsoBuildCommandsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.BeanFactoryAnnotationUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;

@Component
@Slf4j
public class InitializeCmdStatus {
  @Autowired
  Environment env;

  @Autowired
  private ApplicationContext appCtx;

  @Autowired
  @Qualifier("isoBuildCommandsServiceImpl")
  private IsoBuildCommandsService cmdSrvc;

  @PostConstruct
  public void init() {
    log.debug("Entering...........");
    boolean isTestAP = Arrays.stream(env.getActiveProfiles()).anyMatch(activeProfile -> activeProfile.contentEquals("test"));
    if(!isTestAP) {
      List<IsoBuildCommands> list = cmdSrvc.findAll();

      for (IsoBuildCommands cmd : list) {
        GenBashScriptFileService srvc = BeanFactoryAnnotationUtils.qualifiedBeanOfType(
            appCtx.getAutowireCapableBeanFactory(), GenBashScriptFileService.class, cmd.getQualifierName());
        CommandStatusEnum cmdStatus = srvc.getCmdStatus();
        if(cmdStatus.equals(CommandStatusEnum.PENDING)) {
          break;
        } else {
          cmd.setStatus(cmdStatus.getTextVal());
          cmdSrvc.saveOrUpdate(cmd);
          if(cmdStatus.equals(CommandStatusEnum.ERROR)) {
            break;
          }
        }
      }
    }
    log.debug("Exiting............");
  }
}
