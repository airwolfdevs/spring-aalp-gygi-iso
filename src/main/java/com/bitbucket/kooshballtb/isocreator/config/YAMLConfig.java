package com.bitbucket.kooshballtb.isocreator.config;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.nio.file.Path;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "shell-app")
@NoArgsConstructor
@Getter
@Setter
@Slf4j
public class YAMLConfig {
  private String infoColor;
  private String successColor;
  private String warningColor;
  private String errorColor;
  private String bashProj;
  private String jarName;
  private String isoProj;
}
