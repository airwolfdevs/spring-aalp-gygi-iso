package com.bitbucket.kooshballtb.isocreator.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigInteger;
import java.util.Objects;

@Entity
@Table(name = "aur_packages")
@NoArgsConstructor
@Getter @Setter
public class AurPackages {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name="id")
  private BigInteger id;

  @Column(name="display_name")
  private String displayName;

  @Column(name="aur_package")
  private String aurPackage;

  @Column(name="custom_script")
  private String customScript;

  @Column(name="aur_description")
  private String aurDescription;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    AurPackages that = (AurPackages) o;
    return id.equals(that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    return "AurPackages{" +
        "id=" + id +
        ", displayName='" + displayName + '\'' +
        ", aurPackage='" + aurPackage + '\'' +
        ", aurDescription='" + aurDescription + '\'' +
        '}';
  }
}
