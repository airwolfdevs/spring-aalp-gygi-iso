package com.bitbucket.kooshballtb.isocreator.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "custom_category_header")
@NoArgsConstructor
@Getter
@Setter
@NamedEntityGraph(name = "CCHeader.details",
    attributeNodes = @NamedAttributeNode(value = "details", subgraph = "details"),
    subgraphs = @NamedSubgraph(name = "details", attributeNodes = @NamedAttributeNode("aurPackage"))
)

public class CustomCategoryHeader {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name="id")
  private BigInteger id;

  @Column(name="name")
  private String name;

  @Column(name="script_var_name")
  private String scriptVarName;

  @OneToMany(mappedBy = "ccHeader", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @OrderBy("id ASC")
  private Set<CustomCategoryDetails> details = new LinkedHashSet<>();

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    CustomCategoryHeader that = (CustomCategoryHeader) o;
    return id.equals(that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    return "CustomCategoryHeader{" +
        "id=" + id +
        ", name='" + name + '\'' +
        ", scriptVarName='" + scriptVarName + '\'' +
        '}';
  }
}
