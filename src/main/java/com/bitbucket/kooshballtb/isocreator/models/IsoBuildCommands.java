package com.bitbucket.kooshballtb.isocreator.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.math.BigInteger;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "iso_build_commands")
@NoArgsConstructor
@Getter
@Setter
@NamedEntityGraph(name = "IsoBuildCommands.files",
    attributeNodes = @NamedAttributeNode("postInstFiles")
)
public class IsoBuildCommands {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name="id")
  private BigInteger id;

  @Column(name="status")
  private String status;

  @Column(name="cmd_name")
  private String cmdName;

  @Column(name="cmd_desc")
  private String cmdDesc;

  @Column(name="qualifier_name")
  private String qualifierName;

  @OneToMany(mappedBy = "isoBuildCmd", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  private Set<PostInstallFiles> postInstFiles = new LinkedHashSet<>();

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    IsoBuildCommands that = (IsoBuildCommands) o;
    return id.equals(that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    return "IsoBuildCommands{" +
        "id=" + id +
        ", cmdName='" + cmdName + '\'' +
        ", cmdDesc='" + cmdDesc + '\'' +
        '}';
  }
}
