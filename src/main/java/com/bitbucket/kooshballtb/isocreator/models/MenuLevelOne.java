package com.bitbucket.kooshballtb.isocreator.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "menu_level_one")
@NoArgsConstructor
@Getter @Setter
@NamedEntityGraph(name = "MenuLevelOne.subCats",
    attributeNodes = @NamedAttributeNode("subCats")
)
public class MenuLevelOne {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name="id")
  private BigInteger id;

  @Column(name="menu_option")
  private String menuOption;

  @Column(name="menu_label")
  private String menuLabel;

  @OneToMany(mappedBy = "levelOne", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @OrderBy("id ASC")
  private Set<MenuLevelTwo> subCats = new LinkedHashSet<>();

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    MenuLevelOne that = (MenuLevelOne) o;
    return id.equals(that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    return "MenuLevelOne{" +
        "id=" + id +
        ", menuOption='" + menuOption + '\'' +
        ", menuLabel='" + menuLabel + '\'' + +
        '}';
  }
}
