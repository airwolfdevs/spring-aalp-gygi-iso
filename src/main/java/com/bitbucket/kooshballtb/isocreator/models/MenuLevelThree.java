package com.bitbucket.kooshballtb.isocreator.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "menu_level_three")
@NoArgsConstructor
@Getter @Setter
@NamedEntityGraph(name = "MenuLevelThree.lvl3CatOpts",
    attributeNodes = @NamedAttributeNode(value = "lvl3CatOpts", subgraph = "lvl3CatOpts"),
    subgraphs = @NamedSubgraph(name = "lvl3CatOpts", attributeNodes = @NamedAttributeNode("aurPackage"))
)
public class MenuLevelThree {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name="id")
  private BigInteger id;

  @Column(name="menu_option")
  private String menuOption;

  @Column(name="menu_label")
  private String menuLabel;

  @Column(name="script_var_name")
  private String scriptVarName;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "level_two_id", nullable = false, insertable = true, updatable = true)
  private MenuLevelTwo levelTwo;

  @OneToMany(mappedBy = "level3", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @OrderBy("id ASC")
  private Set<CategoryOptions> lvl3CatOpts = new LinkedHashSet<>();

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    MenuLevelThree that = (MenuLevelThree) o;
    return id.equals(that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    return "MenuLevelThree{" +
        "id=" + id +
        ", menuOption='" + menuOption + '\'' +
        ", menuLabel='" + menuLabel + '\'' +
        ", scriptVarName='" + scriptVarName + '\'' +
        '}';
  }
}
