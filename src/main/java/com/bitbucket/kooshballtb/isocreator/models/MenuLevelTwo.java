package com.bitbucket.kooshballtb.isocreator.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "menu_level_two")
@NoArgsConstructor
@Getter @Setter
@NamedEntityGraphs({@NamedEntityGraph(name = "MenuLevelTwo.subCatsLevel3",
                    attributeNodes = @NamedAttributeNode(value = "subCatsLevel3")),
  @NamedEntityGraph(name = "MenuLevelTwo.level2CatOpts",
                    attributeNodes = @NamedAttributeNode(value = "level2CatOpts", subgraph = "menuOpts"),
                    subgraphs = @NamedSubgraph(name = "menuOpts", attributeNodes = @NamedAttributeNode("aurPackage")))
})

public class MenuLevelTwo {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name="id")
  private BigInteger id;

  @Column(name="menu_option")
  private String menuOption;

  @Column(name="menu_label")
  private String menuLabel;

  @Column(name="script_var_name")
  private String scriptVarName;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "level_one_id", nullable = false, insertable = true, updatable = true)
  private MenuLevelOne levelOne;

  @OneToMany(mappedBy = "levelTwo", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @OrderBy("id ASC")
  private Set<MenuLevelThree> subCatsLevel3 = new LinkedHashSet<>();

  @OneToMany(mappedBy = "level2", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @OrderBy("id ASC")
  private Set<CategoryOptions> level2CatOpts = new LinkedHashSet<>();

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    MenuLevelTwo that = (MenuLevelTwo) o;
    return id.equals(that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    return "MenuLevelTwo{" +
        "id=" + id +
        ", menuOption='" + menuOption + '\'' +
        ", menuLabel='" + menuLabel + '\'' +
        ", scriptVarName='" + scriptVarName + '\'' +
        '}';
  }
}
