package com.bitbucket.kooshballtb.isocreator.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigInteger;
import java.util.Objects;

@Entity
@Table(name = "post_install_files")
@NoArgsConstructor
@Getter
@Setter
public class PostInstallFiles {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name="id")
  private BigInteger id;

  @Column(name="file_name")
  private String fileName;

  @Column(name="hr_exp_size")
  private String hrExpSize;

  @Column(name="output_dir")
  private String outputDir;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "cmd_id", nullable = false, insertable = true, updatable = true)
  private IsoBuildCommands isoBuildCmd;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    PostInstallFiles that = (PostInstallFiles) o;
    return id.equals(that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    return "PostInstallFiles{" +
        "id=" + id +
        ", fileName='" + fileName + '\'' +
        ", hrExpSize='" + hrExpSize + '\'' +
        ", outputDir='" + outputDir + '\'' +
        '}';
  }
}
