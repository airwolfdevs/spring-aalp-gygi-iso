package com.bitbucket.kooshballtb.isocreator.repositories;

import com.bitbucket.kooshballtb.isocreator.models.AurPackages;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface AurPackagesRepository extends BaseJpaRepositoryForQueryDSL<AurPackages, BigInteger> {
}
