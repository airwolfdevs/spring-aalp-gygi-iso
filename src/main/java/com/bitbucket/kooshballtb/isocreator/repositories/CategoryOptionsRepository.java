package com.bitbucket.kooshballtb.isocreator.repositories;

import com.bitbucket.kooshballtb.isocreator.models.CategoryOptions;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface CategoryOptionsRepository extends BaseJpaRepositoryForQueryDSL<CategoryOptions, BigInteger> {
}
