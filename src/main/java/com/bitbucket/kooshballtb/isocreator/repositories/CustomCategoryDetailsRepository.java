package com.bitbucket.kooshballtb.isocreator.repositories;

import com.bitbucket.kooshballtb.isocreator.models.CustomCategoryDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface CustomCategoryDetailsRepository extends BaseJpaRepositoryForQueryDSL<CustomCategoryDetails, BigInteger> {
}
