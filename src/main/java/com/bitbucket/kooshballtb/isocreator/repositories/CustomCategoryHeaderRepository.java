package com.bitbucket.kooshballtb.isocreator.repositories;

import com.bitbucket.kooshballtb.isocreator.models.CustomCategoryHeader;
import com.querydsl.core.types.Predicate;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.Optional;


@Repository
public interface CustomCategoryHeaderRepository extends BaseJpaRepositoryForQueryDSL<CustomCategoryHeader, BigInteger> {
  @EntityGraph(type = EntityGraph.EntityGraphType.FETCH, value = "CCHeader.details")
  Optional<CustomCategoryHeader> findOne(Predicate predicate);
}
