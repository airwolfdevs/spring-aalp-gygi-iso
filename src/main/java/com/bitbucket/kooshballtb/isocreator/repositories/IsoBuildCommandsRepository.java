package com.bitbucket.kooshballtb.isocreator.repositories;

import com.bitbucket.kooshballtb.isocreator.models.IsoBuildCommands;
import com.querydsl.core.types.Predicate;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

@Repository
public interface IsoBuildCommandsRepository extends BaseJpaRepositoryForQueryDSL<IsoBuildCommands, BigInteger> {
  @EntityGraph(type = EntityGraph.EntityGraphType.FETCH, value = "IsoBuildCommands.files")
  Optional<IsoBuildCommands> findOne(Predicate predicate);

  @NotNull
  @Override
  @EntityGraph(value = "IsoBuildCommands.files")
  List<IsoBuildCommands> findAll();
}
