package com.bitbucket.kooshballtb.isocreator.repositories;

import com.bitbucket.kooshballtb.isocreator.models.MenuLevelOne;
import com.querydsl.core.types.Predicate;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.Optional;

@Repository
public interface MenuLevelOneRepository extends BaseJpaRepositoryForQueryDSL<MenuLevelOne, BigInteger> {
  @EntityGraph(type = EntityGraph.EntityGraphType.FETCH, value = "MenuLevelOne.subCats")
  Optional<MenuLevelOne> findOne(Predicate predicate);
}
