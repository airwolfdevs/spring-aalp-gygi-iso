package com.bitbucket.kooshballtb.isocreator.repositories;

import com.bitbucket.kooshballtb.isocreator.models.MenuLevelThree;
import com.querydsl.core.types.Predicate;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.Optional;

@Repository
public interface MenuLevelThreeRepository extends BaseJpaRepositoryForQueryDSL<MenuLevelThree, BigInteger> {

  @EntityGraph(type = EntityGraph.EntityGraphType.FETCH, value = "MenuLevelThree.lvl3CatOpts")
  Optional<MenuLevelThree> findOne(Predicate predicate);
}
