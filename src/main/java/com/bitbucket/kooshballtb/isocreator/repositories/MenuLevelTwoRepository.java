package com.bitbucket.kooshballtb.isocreator.repositories;

import com.bitbucket.kooshballtb.isocreator.models.MenuLevelTwo;
import com.querydsl.core.types.Predicate;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.Optional;

@Repository
public interface MenuLevelTwoRepository extends BaseJpaRepositoryForQueryDSL<MenuLevelTwo, BigInteger> {

  @EntityGraph(type = EntityGraph.EntityGraphType.FETCH, value = "MenuLevelTwo.subCatsLevel3")
  Optional<MenuLevelTwo> findById(BigInteger id);

  @EntityGraph(type = EntityGraph.EntityGraphType.FETCH, value = "MenuLevelTwo.level2CatOpts")
  Optional<MenuLevelTwo> findOne(Predicate predicate);
}
