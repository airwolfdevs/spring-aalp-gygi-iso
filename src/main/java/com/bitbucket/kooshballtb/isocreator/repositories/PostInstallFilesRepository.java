package com.bitbucket.kooshballtb.isocreator.repositories;

import com.bitbucket.kooshballtb.isocreator.models.PostInstallFiles;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface PostInstallFilesRepository extends BaseJpaRepositoryForQueryDSL<PostInstallFiles, BigInteger> {
}
