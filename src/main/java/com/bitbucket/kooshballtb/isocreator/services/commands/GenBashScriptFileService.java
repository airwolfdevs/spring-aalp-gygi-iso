package com.bitbucket.kooshballtb.isocreator.services.commands;

import com.bitbucket.kooshballtb.isocreator.common.CommandStatusEnum;
import com.bitbucket.kooshballtb.isocreator.common.InstallerException;

import java.io.IOException;

public interface GenBashScriptFileService {
  CommandStatusEnum execService();
  CommandStatusEnum getCmdStatus();
}
