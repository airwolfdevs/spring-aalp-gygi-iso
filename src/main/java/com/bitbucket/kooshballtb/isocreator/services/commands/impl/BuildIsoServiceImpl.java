package com.bitbucket.kooshballtb.isocreator.services.commands.impl;

import com.bitbucket.kooshballtb.isocreator.common.CommandStatusEnum;
import com.bitbucket.kooshballtb.isocreator.common.UtilityBean;
import com.bitbucket.kooshballtb.isocreator.config.YAMLConfig;
import com.bitbucket.kooshballtb.isocreator.services.commands.GenBashScriptFileService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static com.bitbucket.kooshballtb.isocreator.common.UtilityBean.SCRIPT_PERMS;

@Service
@Slf4j
public class BuildIsoServiceImpl implements GenBashScriptFileService {
  private static final String BUILD_SCRIPT_NAME = "buildISO.sh";
  private static final DateTimeFormatter DT_FMT = DateTimeFormatter.ofPattern("yyyy.MM.dd");

  @Autowired
  private UtilityBean utilBean;

  @Autowired
  private YAMLConfig ymlConfig;

  private CommandStatusEnum cmdStatus = CommandStatusEnum.PENDING;

  @Override
  public CommandStatusEnum execService() {
    cmdStatus = CommandStatusEnum.DONE;

    System.setProperty("user.dir", System.getProperty("orig.user.dir"));
    Path path = Paths.get(System.getProperty("user.dir"), ymlConfig.getIsoProj());

    extractDir();
    try {
      moveDir();
      doCleanUp();

      utilBean.setPermissions(ymlConfig.getIsoProj());
      createBuildScriptFile();
      moveDir(path);

      log.info("Building ISO...........");
      path = Paths.get(System.getProperty("user.dir"), BUILD_SCRIPT_NAME);
      execCmd(path, null);

      String fileName = String.format("%s-%s-x86_64.iso", ymlConfig.getBashProj(), DT_FMT.format(LocalDate.now()));
      path = Paths.get(System.getProperty("user.dir"), ymlConfig.getIsoProj(), "out", fileName);

      if(path.toFile().exists()) {
        moveFile(fileName);
        doCleanUp();
      } else {
        cmdStatus = CommandStatusEnum.ERROR;
        log.error("ISO file: [{}] was NOT found!", path.toString());
      }
    } catch (InterruptedException | IOException e) {
      cmdStatus = CommandStatusEnum.ERROR;
      log.error("Unable to build ISO!", e);
    }

    return cmdStatus;
  }

  @Override
  public CommandStatusEnum getCmdStatus() {
    return cmdStatus;
  }

  private void extractDir() {
    List<String> cmdList = new ArrayList<>();
    cmdList.add("jar");
    cmdList.add("-xvf");
    cmdList.add(ymlConfig.getJarName());
    cmdList.add("BOOT-INF/classes/" + ymlConfig.getIsoProj());
    utilBean.execCmd(log, cmdList);
  }

  private void moveDir() throws IOException {
    Path sourceDir = Paths.get(System.getProperty("user.dir"), "BOOT-INF", "classes", ymlConfig.getIsoProj());
    Path destDir = Paths.get(System.getProperty("user.dir"), ".");
    List<String> cmdList = new ArrayList<>();
    cmdList.add("mv");
    cmdList.add(sourceDir.toString());
    cmdList.add(destDir.toString());
    utilBean.execCmd(log, cmdList);
    Path dir = Paths.get(destDir.toString(), ymlConfig.getIsoProj(), "airootfs", "usr", "bin");
    utilBean.setScriptPermissions(dir);
  }

  private void doCleanUp() throws IOException, InterruptedException {
    List<String> cmdList = new ArrayList<>();
    Path buildScript = Paths.get(System.getProperty("user.dir"), BUILD_SCRIPT_NAME);
    cmdList.add("rm");
    cmdList.add("-rf");

    if(buildScript.toFile().exists()) {
      log.info("Removing '{}'...........", buildScript.toString());
      cmdList.add(buildScript.toString());
      execCmd(null, cmdList);
      Path isoDir = Paths.get(System.getProperty("user.dir"), ymlConfig.getIsoProj());
      log.info("Removing ISO directory '{}'...........", isoDir.toString());
      cmdList.set(cmdList.size() - 1, isoDir.toString());
      execCmd(null, cmdList);
    } else {
      Path extractedDir = Paths.get(System.getProperty("user.dir"), "BOOT-INF");
      cmdList.add(extractedDir.toString());
      utilBean.execCmd(log, cmdList);
    }
  }

  private void moveDir(Path archIsoDir) {
    Path sourceDir = Paths.get(System.getProperty("user.dir"), ymlConfig.getBashProj());
    Path destDir = Paths.get(archIsoDir.toString(), "airootfs", "root");
    List<String> cmdList = new ArrayList<>();
    cmdList.add("mv");
    cmdList.add(sourceDir.toString());
    cmdList.add(destDir.toString());
    utilBean.execCmd(log, cmdList);
  }

  private void moveFile(String isoFileName) throws IOException, InterruptedException {
    Path src = Paths.get(System.getProperty("user.dir"), ymlConfig.getIsoProj(), "out", isoFileName);
    Path target = Paths.get(System.getProperty("user.dir"), isoFileName);
    List<String> cmdList = new ArrayList<>();
    cmdList.add("mv");
    cmdList.add(src.toString());
    cmdList.add(target.toString());
    execCmd(null, cmdList);
  }

  /**
   * This file is used by the AUR "screen" package
   * in order to tail the installer's log file.
   *
   * @throws IOException
   */
  private void createBuildScriptFile() throws IOException {
    log.debug("Entering...........");
    Path dir = Paths.get(System.getProperty("user.dir"));
    Path scriptFile = Paths.get(dir.toString(), BUILD_SCRIPT_NAME);
    List<String> lines = new ArrayList<>();
    lines.add("#===============================================================================");
    lines.add("# globals");
    lines.add("#===============================================================================");
    lines.add("CUR_DIR=\"${PWD}\"");
    lines.add("#===============================================================================");
    lines.add("# functions/methods");
    lines.add("#===============================================================================");
    lines.add("");
    lines.add("#---------------------------------------------------------------------------------------");
    lines.add("#  Main");
    lines.add("#---------------------------------------------------------------------------------------");
    lines.add("main() {");
    lines.add("  cd archlive");
    lines.add("  ./build.sh -v > /tmp/spring-aalp-gygi-iso.log");
    lines.add("}");
    lines.add("");
    lines.add("main \"$@\"");
    lines.add("exit 0");

    try (FileWriter fileWriter = new FileWriter(scriptFile.toFile());
         PrintWriter printWriter = new PrintWriter(fileWriter)) {
      utilBean.printHeader(printWriter);
      printWriter.println(StringUtils.join(lines.iterator(), '\n'));
    } catch (IOException e) {
      log.error("Error trying to create file: [{}]", BUILD_SCRIPT_NAME, e);
      throw e;
    }
    Files.setPosixFilePermissions(scriptFile, SCRIPT_PERMS);
    log.debug("Exiting............");
  }

  private void execCmd(Path path, List<String> cmdList) throws IOException, InterruptedException {
    ProcessBuilder pb = new ProcessBuilder();
    String errMsg = "";
    String cmd;

    if(CollectionUtils.isNotEmpty(cmdList)) {
      cmd = StringUtils.join(cmdList.iterator(), ' ');
      pb.command("sh", "-c", cmd);
    } else {
      String fileName = path.getFileName().toString();
      String parentDir = path.getParent().toString();
      log.info("Parent Dir: [{}], File Name: [{}]", parentDir, fileName);

      pb.directory(path.getParent().toFile());
      cmd = String.format("cd %s; sh %s", parentDir, fileName);
      pb.command("sh", fileName);
    }

    log.info("Executing command: [{}].......", cmd);
    LocalTime startTime = LocalTime.now();
    Process process = pb.start();
    int exitCode = process.waitFor();
    if (exitCode > 1) {
      InputStream is = process.getErrorStream();
      byte[] errorBytes = is.readAllBytes();
      errMsg = new String(errorBytes);
    } else if (exitCode > 0) {
      BufferedReader br = new BufferedReader(
          new InputStreamReader(process.getInputStream()));
      errMsg = br.readLine();
      br.close();
    }
    log.info("Process finished with exitCode: [{}]!", exitCode);
    if(errMsg.length() > 0) {
      log.error("Error: [{}]!", errMsg);
    } else {
      logTotalTime(startTime, LocalTime.now());
    }
  }

  private void logTotalTime(LocalTime startTime, LocalTime endTime) {
    Duration duration = Duration.between(startTime, endTime);
    List<String> labels = new ArrayList<>();
    List<Integer> timeParts = new ArrayList<>();

    int hrs = duration.toHoursPart();
    int mins = duration.toMinutesPart();
    int secs = duration.toSecondsPart();

    if(hrs > 0) {
      labels.add("Hours: [{}]");
      timeParts.add(hrs);
    }
    if(mins > 0) {
      labels.add("Minutes: [{}]");
      timeParts.add(mins);
    }
    if(secs > 0) {
      labels.add("Seconds: [{}]");
      timeParts.add(secs);
    }

    if(CollectionUtils.isNotEmpty(labels)) {
      String logFmt = "Total Time - " + StringUtils.join(labels.iterator(), ", ");
      Object[] vals = timeParts.stream().toArray();
      log.info(logFmt, vals);
    }
  }
}
