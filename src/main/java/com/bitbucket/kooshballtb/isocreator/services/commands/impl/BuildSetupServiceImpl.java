package com.bitbucket.kooshballtb.isocreator.services.commands.impl;

import com.bitbucket.kooshballtb.isocreator.common.CommandStatusEnum;
import com.bitbucket.kooshballtb.isocreator.common.UtilityBean;
import com.bitbucket.kooshballtb.isocreator.config.YAMLConfig;
import com.bitbucket.kooshballtb.isocreator.services.commands.GenBashScriptFileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
@Slf4j
public class BuildSetupServiceImpl implements GenBashScriptFileService {
  @Autowired
  private UtilityBean utilBean;

  @Autowired
  private YAMLConfig ymlConfig;

  private CommandStatusEnum cmdStatus = CommandStatusEnum.PENDING;

  public CommandStatusEnum execService() {
    try {
      Path baseDir = Paths.get(System.getProperty("user.dir"));
      Path archIsoDir = Paths.get(System.getProperty("user.dir"), ymlConfig.getIsoProj());
      String pathSuffix = ymlConfig.getBashProj() + "/scripts";
      if(!baseDir.endsWith(pathSuffix)) {
        Path projDir = Paths.get(baseDir.toString(), ymlConfig.getBashProj());
        if(!projDir.toFile().exists() && !archIsoDir.toFile().exists()) {
          extractDir();
          moveDir();
          doCleanUp();
          utilBean.setPermissions(pathSuffix);
        }
        System.setProperty("orig.user.dir", baseDir.toString());
        System.setProperty("user.dir", projDir.toString() + "/scripts");
      }
      cmdStatus = CommandStatusEnum.DONE;
    } catch (IOException e) {
      cmdStatus = CommandStatusEnum.ERROR;
      log.error("Error trying to initialize aalp-gygi directory", e);
    }

    return cmdStatus;
  }

  @Override
  public CommandStatusEnum getCmdStatus() {
    if(Objects.isNull(this.cmdStatus) || cmdStatus.equals(CommandStatusEnum.PENDING)) {
      execService();
    }
    return this.cmdStatus;
  }

  private void extractDir() {
    List<String> cmdList = new ArrayList<>();
    cmdList.add("jar");
    cmdList.add("-xvf");
    cmdList.add(ymlConfig.getJarName());
    cmdList.add("BOOT-INF/classes/" + ymlConfig.getBashProj());
    utilBean.execCmd(log, cmdList);
  }

  private void moveDir() {
    Path sourceDir = Paths.get(System.getProperty("user.dir"), "BOOT-INF", "classes", ymlConfig.getBashProj());
    Path destDir = Paths.get(System.getProperty("user.dir"), ".");
    List<String> cmdList = new ArrayList<>();
    cmdList.add("mv");
    cmdList.add(sourceDir.toString());
    cmdList.add(destDir.toString());
    utilBean.execCmd(log, cmdList);
  }

  private void doCleanUp() {
    Path extractedDir = Paths.get(System.getProperty("user.dir"), "BOOT-INF");
    List<String> cmdList = new ArrayList<>();
    cmdList.add("rm");
    cmdList.add("-rf");
    cmdList.add(extractedDir.toString());
    utilBean.execCmd(log, cmdList);
  }
}
