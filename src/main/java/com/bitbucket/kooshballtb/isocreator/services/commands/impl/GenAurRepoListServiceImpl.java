package com.bitbucket.kooshballtb.isocreator.services.commands.impl;

import com.bitbucket.kooshballtb.isocreator.common.CommandStatusEnum;
import com.bitbucket.kooshballtb.isocreator.common.InstallerException;
import com.bitbucket.kooshballtb.isocreator.common.UtilityBean;
import com.bitbucket.kooshballtb.isocreator.services.commands.GenBashScriptFileService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class GenAurRepoListServiceImpl implements GenBashScriptFileService {
  @Autowired
  private UtilityBean utilBean;

  public static final List<String> AUR_REPO_FILES = List.of("aur-signed-repos.sh", "aur-unsigned-repos.sh");
  private Map<String, Map<String, Map<String,String>>> aurRepoMap = new HashMap<>();
  private List<Path> generatedFiles = new ArrayList<>();
  private CommandStatusEnum cmdStatus = CommandStatusEnum.PENDING;

  @Override
  public CommandStatusEnum execService() {
    log.debug("Entering...........");
    try {
      setChoices();
      createFiles();
      utilBean.setPermissions(generatedFiles);
      cmdStatus = CommandStatusEnum.DONE;
    } catch (IOException | InstallerException e) {
      cmdStatus = CommandStatusEnum.ERROR;
      log.error("Unable to create AUR Repository data files: {}", AUR_REPO_FILES, e);
    }
    log.debug("Exiting............");
    return this.cmdStatus;
  }

  @Override
  public CommandStatusEnum getCmdStatus() {
    cmdStatus = CommandStatusEnum.DONE;
    Path dir = Paths.get(System.getProperty("user.dir"), "inc", "post-inst");
    for (String fileName : AUR_REPO_FILES) {
      Path incFile = Paths.get(dir.toString(), fileName);
      if(!incFile.toFile().exists()) {
        cmdStatus = CommandStatusEnum.PENDING;
        break;
      }
    }
    return cmdStatus;
  }

  private void setChoices() throws IOException, InstallerException {
    log.debug("Entering...........");
    Document doc = utilBean.getDocument("aurRepos");
    Elements h2Tags = doc.select("h2");
    processTags(h2Tags.stream().collect(Collectors.toList()));
    log.debug("Exiting...........");
  }

  private void processTags(List<Element> elemList) {
    for (Element h2Tag : elemList) {
      Elements spanTags = h2Tag.select("span");
      Element spanTag = (spanTags.size() > 0) ? spanTags.get(0) : null;
      String name = (Objects.nonNull(spanTag)) ? ((TextNode) spanTag.childNode(0)).text() : "";
      if(name.equalsIgnoreCase("signed") || name.equalsIgnoreCase("unsigned")) {
        Map<String, Map<String,String>> repoMap = new LinkedHashMap<>();
        aurRepoMap.put(name, repoMap);
        Element nextSibling = h2Tag.nextElementSibling();
        String tagName = nextSibling.tagName();
        while(!tagName.equalsIgnoreCase("h2")) {
          if(tagName.equalsIgnoreCase("h3")) {
            addToRepoMap(nextSibling, repoMap);
          }
          nextSibling = nextSibling.nextElementSibling();
          if(Objects.isNull(nextSibling)) {
            tagName = "h2";
          } else {
            tagName = nextSibling.tagName();
          }
        }
      }
    }
  }

  private void addToRepoMap(Element repoNameTag, Map<String, Map<String, String>> repoMap) {
    String aurRepoName = ((TextNode) repoNameTag.select("span").get(0).childNode(0)).text();
    Map<String, String> map = new HashMap<>();
    switch (aurRepoName) {
      case "repo-ck": {
        map.put("desc", "An unofficial Arch Linux repository hosting generic and CPU-optimized kernels and support packages, featuring MuQSS (pronounced mux)");
        map.put("url", "http://repo-ck.com/$arch");
        map.put("keyID", "5EE46C4C");
        break;
      }
      default: {
        Element ulTag = repoNameTag.nextElementSibling();
        Element liDescTag = null;
        Element liKeyTag = null;
        if(aurRepoName.equalsIgnoreCase("archlinuxfr")) {
          map.put("desc", "Default unofficial repository maintained in France");
        } else {
          List<String> list = new ArrayList<>();
          Elements liTags = ulTag.select("li");
          for (Element listTag: liTags) {
            Element boldTag = listTag.select("b").get(0);
            String text = ((TextNode)boldTag.childNode(0)).text().trim();
            if(text.contains("Description")) {
              liDescTag = listTag;
            } else if(text.contains("Key-ID")) {
              liKeyTag = listTag;
            }
          }

          if(Objects.nonNull(liDescTag)) {
            setDescription(liDescTag, list, map);
          }
        }

        if(Objects.nonNull(liKeyTag) && !"archlinuxcn".equalsIgnoreCase(aurRepoName)) {
          setKeyID(aurRepoName, liKeyTag, map);
        }

        Element preTag = ulTag.nextElementSibling();
        if(!preTag.tagName().equalsIgnoreCase("pre")) {
          preTag = preTag.nextElementSibling();
        }

        map.put("url", getRepoURL(((TextNode) preTag.childNode(0)).text()));

        break;
      }
    }

    log.debug("aurRepoName: [{}]\nDesc: [{}]\nURL: [{}]", aurRepoName, map.get("desc"), map.get("url"));
    repoMap.put(aurRepoName, map);
  }

  private String getRepoURL(String text) {
    String url = "";
    String[] splitStr = StringUtils.splitByWholeSeparator(text, "] ");
    List<String> list = new ArrayList<>(Arrays.asList(splitStr));
    list.remove(0);
    for (String elemText : list) {
      if (elemText.startsWith("Server")) {
        splitStr = StringUtils.split(text, '=');
        splitStr = StringUtils.split(splitStr[1].trim(), ' ');
        url = splitStr[0];
        break;
      } else {
        splitStr = StringUtils.splitByWholeSeparator(text, "Server =");
        url = splitStr[splitStr.length - 1].trim();
      }
    }
    return url;
  }

  private void setKeyID(String aurRepoName, Element liKeyTag, Map<String, String> map) {
    if("chaotic-aur".equalsIgnoreCase(aurRepoName)) {
      map.put("keyID", "87B78AEB");
    } else {
      liKeyTag.childNodes().forEach(child -> {
        String nodeName = child.nodeName();
        String text;
        if(nodeName.equalsIgnoreCase("#text")) {
          text = ((TextNode)child).text().trim();
          if((text.length() > 0) && !text.contains(", fingerprint")) {
            map.put("keyID", text);
            return;
          }
        } else if(nodeName.equalsIgnoreCase("a")) {
          text=((TextNode)child.childNode(0)).text().trim();
          map.put("keyID", text);
          return;
        }
      });
    }
  }

  private void setDescription(Element liDescTag, List<String> list, Map<String, String> map) {
    liDescTag.childNodes().forEach(child -> {
      String nodeName = child.nodeName();
      if(nodeName.equalsIgnoreCase("#text")) {
        String text = ((TextNode)child).text().trim();
        if(!text.equalsIgnoreCase(":")) {
          list.add(((TextNode) child).text().trim());
        }
      } else if(!nodeName.equalsIgnoreCase("b") && !nodeName.equalsIgnoreCase("sup")) {
        try {
          if(child.nodeName().equalsIgnoreCase("span")) {
            list.add(String.format("\\\"%s\\\"", ((TextNode) child.childNode(0).childNode(0)).text().trim()));
          } else {
            list.add(String.format("\\\"%s\\\"",((TextNode)child.childNode(0)).text().trim()));
          }
        } catch (Exception e) {
          log.error("childNode: [{}]", child.nodeName());
        }
      }
    });
    String desc = StringUtils.join(list.iterator(), ' ');
    map.put("desc", desc);
  }

  private void createFiles() throws IOException {
    for (Map.Entry<String, Map<String, Map<String,String>>> me : aurRepoMap.entrySet()) {
      String repoType = me.getKey();
      Map<String, Map<String, String>> map = me.getValue();
      log.info("Repository Type: [{}], Size: [{}]", repoType, map.size());
      String fileName = String.format("aur-%s-repos.sh", repoType.toLowerCase());
      Path dir = Paths.get(System.getProperty("user.dir"), "inc", "post-inst");
      Path incFile = Paths.get(dir.toString(), fileName);

      utilBean.initDirFile(dir, incFile);
      generatedFiles.add(incFile);

      try (FileWriter fileWriter = new FileWriter(incFile.toFile(), true);
           PrintWriter printWriter = new PrintWriter(fileWriter)) {

        utilBean.printHeader(printWriter);
        List<String> menuOpts = new ArrayList<>();
        Map<String, String> descs = new LinkedHashMap<>();
        Map<String, String> urls = new LinkedHashMap<>();
        Map<String, String> keyIDs = new LinkedHashMap<>();
        map.entrySet().forEach(meRepo ->
            {
              String aurName = meRepo.getKey();
              Map<String, String> repoMap = meRepo.getValue();
              menuOpts.add(aurName);
              descs.put(aurName, repoMap.get("desc").trim());
              String repoURL = repoMap.get("url");
              String[] splitStr = StringUtils.split(repoURL, '$');
              urls.put(aurName, (splitStr.length > 1) ? StringUtils.join(splitStr, "\\$") : repoURL);
              String keyID = repoMap.get("keyID");
              if(StringUtils.isNotBlank(keyID)) {
                if(!keyID.startsWith("Not needed") && !keyID.startsWith("Not required")) {
                  if(keyID.charAt(0) == ':') {
                    keyID = keyID.replace(':', ' ').trim();
                  }
                  keyIDs.put(aurName, keyID);
                }
              }
            }
        );
        utilBean.addMenuOptsArray(String.format("%s_MENU_OPTS", repoType.toUpperCase()), menuOpts, printWriter);
        utilBean.addAURDescsArray(descs,String.format("%s_REPO_DESCS", repoType.toUpperCase()), printWriter);
        utilBean.addAURDescsArray(urls,String.format("%s_REPO_URLS", repoType.toUpperCase()), printWriter);
        if(MapUtils.isNotEmpty(keyIDs)) {
          utilBean.addAURDescsArray(keyIDs,String.format("%s_REPO_KEY_IDS", repoType.toUpperCase()), printWriter);
        }
      } catch (IOException e) {
        log.error("Error trying to create file: [{}]", fileName, e);
        throw e;
      }

    }
  }
}
