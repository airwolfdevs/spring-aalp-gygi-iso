package com.bitbucket.kooshballtb.isocreator.services.commands.impl;

import com.bitbucket.kooshballtb.isocreator.common.CommandStatusEnum;
import com.bitbucket.kooshballtb.isocreator.common.InstallerException;
import com.bitbucket.kooshballtb.isocreator.common.UtilityBean;
import com.bitbucket.kooshballtb.isocreator.config.YAMLConfig;
import com.bitbucket.kooshballtb.isocreator.models.AurPackages;
import com.bitbucket.kooshballtb.isocreator.services.jpa.AurPackagesService;
import com.bitbucket.kooshballtb.isocreator.services.commands.GenBashScriptFileService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class GenDesktopConfigFileServiceImpl implements GenBashScriptFileService {
  public static final String FILE_NAME = "de-dm-wm-consts.sh";
  public static final Map<String,String> WINDOW_MANAGER_TYPES = new LinkedHashMap<>() {{
    put("Dynamic", "— window managers that can dynamically switch between tiling or floating window layout.");
    put("Stacking", "— (aka floating) window managers that provide the traditional desktop metaphor used in "
        + "commercial operating systems like Windows and OS X.  Windows act like pieces of paper on a "
        + "desk, and can be stacked on top of each other.");
    put("Tiling", "— window managers that \"tile\" the windows so that none are overlapping.  They"
        + "usually make very extensive use of key-bindings and have less (or no) reliance on the mouse.  "
        + "Tiling window managers may be manual, offer predefined layouts, or both.");
  }};

  private static final Map<String, List<String>> DE_EXTRAS = new HashMap<>() {{
    put("Deepin", List.of("deepin-extra"));
    put("Enlightenment", List.of("ecrire-git", "edi", "eluminance-git", "enjoy-git", "eperiodique"
        , "ephoto", "epour", "epymc-git", "equate-git", "eruler-git", "efbb-git"
        , "elemines-git", "rage", "terminology"));
    put("GNOME", List.of("gnome-extra"));
    put("GNOME Flashback", List.of("gnome"));
    put("KDE Plasma", List.of("plasma-wayland-session", "kde-applications"));
    put("LXQt", List.of("breeze", "breeze-gtk", "sddm"));
    put("MATE", List.of("mate-extra"));
    put("Sugar", List.of("sugar-fructose", "sugar-runner"));
    put("Xfce", List.of("xfce4-goodies"));
  }};

  private static final List<String> DE_SKIP_COMP_MAN = new ArrayList<>() {{
    add("Budgie");
    add("Cinnamon");
    add("Enlightenment");
    add("GNOME");
    add("GNOME Flashback");
    add("KDE Plasma");
    add("MATE");
    add("Xfce");
  }};

  private static final List<String> DE_SKIP_DISP_MAN = new ArrayList<>() {{
    add("Deepin");
    add("GNOME");
    add("KDE Plasma");
    add("LXQt");
    add("Xfce");
  }};

  private static final Map<String, List<String>> DM_EXTRAS = new HashMap<>() {{
    put("GDM", List.of("gdm3setup-utils"));
    put("LightDM", List.of("accountsservice", "light-locker", "lightdm-gtk-greeter", "lightdm-gtk-greeter-settings", "numlockx"));
    put("LXDM", List.of("librsvg", "lxdm-themes"));
    put("SDDM", List.of("breeze", "breeze-gtk", "sddm-config-editor-git"));
    put("XDM", List.of("qiv", "xdm-archlinux"));
  }};

  private static final Map<String, List<String>> WM_EXTRAS = new HashMap<>() {{
    put("Compiz", List.of("emerald", "emerald-themes", "fusion-icon"));
    put("Openbox", List.of("gtk2-perl", "lxappearance-obconf", "lxrandr", "ob-autostart", "obapps", "obconf",
        "obkey-git", "oblogout", "obmenu", "obmenu-generator", "openbox-themes"));
    put("PekWM", List.of("pekwm-menu"));
    put("Window Maker", List.of("windowmaker-extra"));
    put("Xfwm", List.of("xfce4-settings","xfwm4-themes"));
    put("EXWM", List.of("emacs"));
    put("i3", List.of("j4-dmenu-desktop-git"));
    put("Ratpoison", List.of("transset-df", "xcompmgr"));
    put("sway", List.of("swaylock", "swayidle"));
    put("dwm", List.of("dzen2"));
    put("echinus", List.of("dmenu", "ipager", "ourico"));
    put("wmii", List.of("dmenu"));
    put("xmonad", List.of("dmenu", "dzen2", "xmonad-contrib", "xmobar", "xterm"));
  }};

  private static final List<String> WM_SKIP_COMP_MAN = new ArrayList<>() {{
    add("Compiz");
    add("Gala");
    add("KWin");
    add("Marco");
    add("Metacity");
    add("Muffin");
    add("Mutter");
    add("Ratpoison");
    add("Xfwm");
  }};

  @Autowired
  @Qualifier("aurPackagesServiceImpl")
  private AurPackagesService aurSrvc;

  @Autowired
  private UtilityBean utilBean;

  @Autowired
  private YAMLConfig ymlConfig;

  private List<String> desktopEnvList = new ArrayList<>();
  private List<String> dispManList = new ArrayList<>();
  private List<String> winManList = new ArrayList<>();
  private Map<String, List<String>> winManMap = new LinkedHashMap<>();
  private Path destDir;
  private Path incFile;
  private CommandStatusEnum cmdStatus = CommandStatusEnum.PENDING;

  @Override
  public CommandStatusEnum execService() {
    log.debug("Entering...........");
    destDir = Paths.get(System.getProperty("user.dir"), "inc", "post-inst");
    incFile = Paths.get(destDir.toString(), FILE_NAME);

    try {
      setDesktopEnvironments();
      setDisplayManagers();

      winManMap.put("Stacking_window_managers", new ArrayList<>());
      winManMap.put("Tiling_window_managers", new ArrayList<>());
      winManMap.put("Dynamic_window_managers", new ArrayList<>());
      setWindowManagers();

      generateConstantsFile();
      cmdStatus = CommandStatusEnum.DONE;
    } catch (IOException | InstallerException e) {
      cmdStatus = CommandStatusEnum.ERROR;
      log.error("Unable to create Desktop Configuration script file: [{}]", FILE_NAME, e);
    }

    log.debug("Exiting............");
    return cmdStatus;
  }

  @Override
  public CommandStatusEnum getCmdStatus() {
    Path dir = Paths.get(System.getProperty("user.dir"), "inc", "post-inst");
    Path incFile = Paths.get(dir.toString(), FILE_NAME);
    if(incFile.toFile().exists()) {
      cmdStatus = CommandStatusEnum.DONE;
    } else {
      cmdStatus = CommandStatusEnum.PENDING;
    }
    return cmdStatus;
  }

  private void setDesktopEnvironments() throws IOException, InstallerException {
    log.debug("Entering...........");
    Document doc = utilBean.getDocument("desktopEnvironments");
    Elements deTypes = doc.select("h3");
    List<Element> elems = deTypes.stream().filter(el -> el.toString().contains("Officially supported")).collect(Collectors.toList());
    Element deType = elems.get(0);
    Element ulDL = deType.nextElementSibling();
    desktopEnvList.addAll(getMenuOpts(ulDL));
    log.debug("Exiting............");
  }

  private void setDisplayManagers() throws IOException, InstallerException {
    log.debug("Entering...........");
    Document doc = utilBean.getDocument("displayManager");
    Elements dmTypes = doc.select("h3");
    List<Element> elems = dmTypes.stream().filter(el -> el.toString().contains("Graphical")).collect(Collectors.toList());
    Element dmType = elems.get(0);
    Element ulDL = dmType.nextElementSibling();
    dispManList.addAll(getMenuOpts(ulDL));
    log.debug("Exiting............");
  }

  private List<String> getMenuOpts(Element ulDL) {
    List<String> menuOpts = new ArrayList<>();
    String desc = null;
    String optName = null;
    String tagName = ulDL.tagName();
    while (Objects.nonNull(ulDL) && !tagName.equalsIgnoreCase("h3") && !tagName.equalsIgnoreCase("h2")) {
      Element firstChild = ulDL.child(0);
      if (tagName.equalsIgnoreCase("dl") && (ulDL.children().size() > 1)) {
        firstChild = ulDL.child(1);
      }
      Element aTag = (tagName.equalsIgnoreCase("ul")) ? firstChild.child(0) : firstChild.child(1);
      if (aTag.children().size() > 0) {
        aTag = aTag.child(0);
      }
      if (tagName.equalsIgnoreCase("ul")) {
        optName = aTag.text();
        firstChild.child(0).remove();
        desc = StringUtils.split(firstChild.text(), '.')[0];
      } else {
        Optional<AurPackages> opt = aurSrvc.findByDisplayName(optName);
        if(!opt.isPresent()) {
          menuOpts.add(optName);
          AurPackages aurPckg = new AurPackages();
          aurPckg.setDisplayName(optName);
          aurPckg.setAurPackage(aTag.text());
          aurPckg.setAurDescription(desc);
          aurSrvc.saveOrUpdate(aurPckg);
        }
      }

      ulDL = ulDL.nextElementSibling();
      tagName = ulDL.tagName();
    }

    return menuOpts;
  }

  private void setWindowManagers() throws IOException, InstallerException {
    log.debug("Entering...........");

    Document doc = utilBean.getDocument("windowManager");
    Elements wmTypes = doc.select("h3");
    List<Element> elems = wmTypes.stream().filter(el -> el.toString().contains("window managers")).
        limit(4).collect(Collectors.toList());
    for (Map.Entry<String, String> me : WINDOW_MANAGER_TYPES.entrySet()) {
      String key = me.getKey();
      winManList.add(key);
    }

    for (Element wmType: elems) {
      String key = wmType.child(0).attr("id");
      log.debug("\nwmType:  {},[{}]", key, wmType.child(0).text());
      Element ulDL = wmType.nextElementSibling();
      List<String> menuOpts = getMenuOptsForWMs(ulDL);
      winManMap.get(key).addAll(menuOpts);
    }

    log.debug("Exiting............");
  }

  /**
   * This will strip out the Window Managers that either:
   *    A) DO NOT have their name as a link (i.e. aewm)
   *    B) The link is not a relative link to the Arch Wiki site
   * @param ulDL
   * @return
   */
  private List<String> getMenuOptsForWMs(Element ulDL) {
    List<String> menuOpts = new ArrayList<>();
    String desc = null;
    String wmName = null;
    String tagName = ulDL.tagName();
    while (Objects.nonNull(ulDL) && !tagName.equalsIgnoreCase("h3") && !tagName.equalsIgnoreCase("h2")) {
      Element firstChild = ulDL.child(0);
      if (tagName.equalsIgnoreCase("dl") && (ulDL.children().size() > 1)) {
        firstChild = ulDL.child(1);
      }
      Element aTag = (tagName.equalsIgnoreCase("ul")) ? firstChild.child(0) : firstChild.child(1);
      if (aTag.children().size() > 0) {
        aTag = aTag.child(0);
      }
      if (tagName.equalsIgnoreCase("ul")) {
        String hrefAttr = aTag.attr("href");
        if (StringUtils.isNotBlank(hrefAttr)) {
          char firstChar = hrefAttr.charAt(0);
          if (firstChar == '/') {
            wmName = aTag.text();
            firstChild.child(0).remove();
            desc = StringUtils.split(firstChild.text(), '.')[0];
          } else {
            wmName = "";
          }
        } else {
          wmName = "";
        }
      } else if (StringUtils.isNotBlank(wmName)){
        Optional<AurPackages> opt = aurSrvc.findByDisplayName(wmName);
        if(!opt.isPresent() && !wmName.equalsIgnoreCase("WMFS")) {
          menuOpts.add(wmName);
          AurPackages aurPckg = new AurPackages();
          aurPckg.setDisplayName(wmName);
          aurPckg.setAurPackage(aTag.text());
          aurPckg.setAurDescription(desc);
          aurSrvc.saveOrUpdate(aurPckg);
        }
      }

      ulDL = ulDL.nextElementSibling();
      tagName = ulDL.tagName();
    }

    return menuOpts;
  }

  private void generateConstantsFile() throws IOException {
    Map<String, String> aurPckgs = new HashMap<>();
    Map<String, String> aurPckgDescs = new HashMap<>();

    utilBean.initDirFile(destDir, incFile);

    setDataForMaps(aurPckgs, aurPckgDescs);

    try (FileWriter fileWriter = new FileWriter(incFile.toFile());
         PrintWriter printWriter = new PrintWriter(fileWriter)) {

      utilBean.printHeader(printWriter);
      utilBean.addMenuOptsArray("DESKTOP_ENVIRONMENTS", desktopEnvList, printWriter);
      utilBean.addMenuOptsArray("DISPLAY_MANAGERS", dispManList, printWriter);
      utilBean.addMenuOptsArray("WINDOW_MANAGERS", winManList, printWriter);

      winManMap.forEach((k,v)->utilBean.addMenuOptsArray(k.toUpperCase(),v,printWriter));

      addDesktopEnvExtrasArray(aurPckgDescs, printWriter);
      addDispManExtrasArray(aurPckgDescs, printWriter);
      addWinManExtrasArray(aurPckgDescs, printWriter);

      addArraysToSkipTasks(aurPckgDescs, printWriter);

      utilBean.addAURPckgsArray(aurPckgs,"DESKTOP_AUR_PCKGS", printWriter);
      utilBean.addAURDescsArray(aurPckgDescs,"DESKTOP_AUR_DESCS", printWriter);

    } catch (IOException e) {
      log.error("Error trying to create file: [{}]", incFile.toString(), e);
      throw e;
    }
  }

  private void setDataForMaps(Map<String, String> aurPckgs, Map<String, String> aurPckgDescs) {
    desktopEnvList.forEach(deOpt -> addToMaps(deOpt, aurPckgs, aurPckgDescs));

    dispManList.forEach(dmOpt -> addToMaps(dmOpt, aurPckgs, aurPckgDescs));

    winManMap.forEach((key,val) -> {
      String[] splitArray = StringUtils.split(key, '_');
      String dispName = splitArray[0];
      addToMaps(dispName, aurPckgs, aurPckgDescs);
      val.forEach(wmOpt -> addToMaps(wmOpt, aurPckgs, aurPckgDescs));
    });
  }

  private void addToMaps(String dispName, Map<String, String> aurPckgs, Map<String, String> aurPckgDescs) {
    Optional<AurPackages> opt = aurSrvc.findByDisplayName(dispName);
    if(opt.isPresent()) {
      AurPackages aurEntity = opt.get();
      String aurPckg = aurEntity.getAurPackage();
      if(StringUtils.isNotBlank(aurPckg)) {
        aurPckgs.put(dispName, aurPckg);
      }
      aurPckgDescs.put(dispName, utilBean.getDescription(aurEntity.getAurDescription()));
    } else {
      log.error("Entity not found for dispName: [{}]", dispName);
    }
  }

  private void addDesktopEnvExtrasArray( Map<String, String> aurPckgDescs, PrintWriter printWriter) {
    String varName = "DE_EXTRAS";
    printWriter.printf("declare -A %s=(", varName);

    for (Map.Entry<String, List<String>> me : DE_EXTRAS.entrySet()) {
      List<String> list = me.getValue();
      String extraPckgs = StringUtils.join(list.iterator(), ' ');
      printWriter.println(String.format("[\"%s\"]=\"%s\"", me.getKey(), extraPckgs));
      list.forEach(dispName -> addToDescMap(dispName, aurPckgDescs));
    }

    printWriter.println(")");
  }

  private void addToDescMap(String dispName, Map<String, String> aurPckgDescs) {
    Optional<AurPackages> opt;
    if(dispName.contentEquals("sddm")) {
      opt = aurSrvc.findByDisplayName(dispName.toUpperCase());
    } else {
      opt = aurSrvc.findByDisplayName(dispName);
    }
    if(opt.isPresent()) {
      AurPackages aurEntity = opt.get();
      aurPckgDescs.put(dispName, utilBean.getDescription(aurEntity.getAurDescription()));
    } else {
      log.error("Entity not found for dispName: [{}]", dispName);
    }
  }

  private void addDispManExtrasArray(Map<String, String> aurPckgDescs, PrintWriter printWriter) {
    String varName = "DM_EXTRAS";
    printWriter.printf("declare -A %s=(", varName);

    for (Map.Entry<String, List<String>> me : DM_EXTRAS.entrySet()) {
      List<String> list = me.getValue();
      String extraPckgs = StringUtils.join(list.iterator(), ' ');
      printWriter.println(String.format("[\"%s\"]=\"%s\"", me.getKey(), extraPckgs));
      list.forEach(dispName -> addToDescMap(dispName, aurPckgDescs));
    }

    printWriter.println(")");
  }

  private void addWinManExtrasArray(Map<String, String> aurPckgDescs, PrintWriter printWriter) {
    String varName = "WM_EXTRAS";
    printWriter.printf("declare -A %s=(", varName);

    for (Map.Entry<String, List<String>> me : WM_EXTRAS.entrySet()) {
      List<String> list = me.getValue();
      String extraPckgs = StringUtils.join(list.iterator(), ' ');
      printWriter.println(String.format("[\"%s\"]=\"%s\"", me.getKey(), extraPckgs));
      list.forEach(dispName -> addToDescMap(dispName, aurPckgDescs));
    }

    printWriter.println(")");
  }

  private void addArraysToSkipTasks(Map<String, String> aurPckgDescs, PrintWriter printWriter) {
    List<String> staticVars = List.of("DE_SKIP_COMP_MAN", "DE_SKIP_DISP_MAN", "WM_SKIP_COMP_MAN");
    List<String> skipList;
    for (String varName : staticVars) {
      List<String> arrayList = new ArrayList<>();
      switch (varName) {
        case "DE_SKIP_COMP_MAN": {
          skipList = List.copyOf(DE_SKIP_COMP_MAN);
          break;
        }
        case "DE_SKIP_DISP_MAN": {
          skipList = List.copyOf(DE_SKIP_DISP_MAN);
          break;
        }
        default: {
          skipList = List.copyOf(WM_SKIP_COMP_MAN);
          break;
        }
      }

      for (String deWM : skipList) {
        if (aurPckgDescs.containsKey(deWM)) {
          arrayList.add(deWM);
        }
      }
      utilBean.addMenuOptsArray(varName, arrayList, printWriter);
    }
  }
}
