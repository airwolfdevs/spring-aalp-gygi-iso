package com.bitbucket.kooshballtb.isocreator.services.commands.impl;

import com.bitbucket.kooshballtb.isocreator.common.CommandStatusEnum;
import com.bitbucket.kooshballtb.isocreator.common.FileVisitorUtils;
import com.bitbucket.kooshballtb.isocreator.common.InstallerException;
import com.bitbucket.kooshballtb.isocreator.common.UtilityBean;
import com.bitbucket.kooshballtb.isocreator.config.YAMLConfig;
import com.bitbucket.kooshballtb.isocreator.models.AurPackages;
import com.bitbucket.kooshballtb.isocreator.models.CategoryOptions;
import com.bitbucket.kooshballtb.isocreator.models.CustomCategoryDetails;
import com.bitbucket.kooshballtb.isocreator.models.CustomCategoryHeader;
import com.bitbucket.kooshballtb.isocreator.models.MenuLevelOne;
import com.bitbucket.kooshballtb.isocreator.models.MenuLevelThree;
import com.bitbucket.kooshballtb.isocreator.models.MenuLevelTwo;
import com.bitbucket.kooshballtb.isocreator.services.jpa.AurPackagesService;
import com.bitbucket.kooshballtb.isocreator.services.jpa.CustomCategoryDetailsService;
import com.bitbucket.kooshballtb.isocreator.services.jpa.CustomCategoryHeaderService;
import com.bitbucket.kooshballtb.isocreator.services.commands.GenBashScriptFileService;
import com.bitbucket.kooshballtb.isocreator.services.jpa.MenuLevelAbstractFactory;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
@Slf4j
public class GenPostInstMenuServiceImpl implements GenBashScriptFileService {
  public static final Map<String, String> SCRIPT_FILE_NAMES = new HashMap<>() {{
    put("Basic Setup", "cat-basic-setup.sh");
    put("Database Software", "cat-db-soft.sh");
    put("Desktop Configuration", "cat-desktop-config.sh");
    put("Accessory Software", "cat-accessory-soft.sh");
    put("Application & Web Servers", "cat-servers-soft.sh");
    put("Development Software", "cat-dev-soft.sh");
    put("Documents & Texts Software", "cat-doc-text-soft.sh");
    put("Internet Software", "cat-internet-soft.sh");
    put("Multimedia Software", "cat-multimedia-soft.sh");
    put("System & Utility Software", "cat-sys-util-soft.sh");
    put("Security Software", "cat-security.sh");
    put("Software Extras", "cat-soft-extras.sh");
    put("KDE", "kde-constants.sh");
    put("VCD", "video-card-drivers.sh");
    put("PostInstMenu", "soft-cats.sh");
  }};

  private static final List<String> SUB_CAT_FILES = List.of("Audio & Video Codecs", "Communication"
      , "File Sharing", "Files", "Image", "Integrated Development Environments (IDEs)"
      , "Network connection", "Password Managers", "Readers & Viewers", "System Utils"
      , "Terminal Emulators", "Web Browsers");

  private static final List<String> CONKY_PCKGS = List.of("Conky", "Conky CLI", "Conky Lua", "Conky Lua NV", "Conky Nvidia");
  private Map<String, List<String>> catOptsMap = new HashMap<>();
  private Path incDir;
  private CommandStatusEnum cmdStatus = CommandStatusEnum.PENDING;

  @Autowired
  private UtilityBean utilBean;

  @Autowired
  private YAMLConfig ymlConfig;

  @Autowired
  @Qualifier("aurPackagesServiceImpl")
  private AurPackagesService aurSrvc;

  @Autowired
  @Qualifier("customCategoryHeaderServiceImpl")
  private CustomCategoryHeaderService cchSrvc;

  @Autowired
  @Qualifier("customCategoryDetailsServiceImpl")
  private CustomCategoryDetailsService ccdSrvc;

  @Autowired
  @Qualifier("menuLevelOneServiceImpl")
  private MenuLevelAbstractFactory level1Service;

  @Autowired
  @Qualifier("menuLevelTwoServiceImpl")
  private MenuLevelAbstractFactory level2Service;

  @Autowired
  @Qualifier("menuLevelThreeServiceImpl")
  private MenuLevelAbstractFactory level3Service;

  @Autowired
  @Qualifier("categoryOptionsServiceImpl")
  private MenuLevelAbstractFactory catOptSrvc;

  @Autowired
  private FileVisitorUtils fileVisitor;

  @Override
  public CommandStatusEnum execService() {
    log.debug("Entering...........");
    incDir = Paths.get(System.getProperty("user.dir"), "inc", "post-inst");

    try {
      setThemesForKDE();

      addAurPackages();

      setCategoryOptions();

      createFileForKDE();

      createFileForVCD();

      List<MenuLevelOne> listLevel1 = createFilesForPostInst();

      appendToBasicSetupFile(listLevel1.remove(0));

      for (MenuLevelOne mlo : listLevel1) {
        appendCatOptsToFile(mlo);
      }

      utilBean.setPermissions("inc/post-inst");

      cmdStatus = CommandStatusEnum.DONE;
    } catch (IOException | InstallerException e) {
      cmdStatus = CommandStatusEnum.ERROR;
      log.error("Unable to create all the bash script files for the menu of the Post-Installation step", e);
    }

    log.debug("Exiting............");
    return cmdStatus;
  }

  @Override
  public CommandStatusEnum getCmdStatus() {
    Path pathToScripts = Paths.get(System.getProperty("user.dir"), "inc", "post-inst");
    try {
      Files.walkFileTree(pathToScripts, fileVisitor);
      List<Path> pathList = fileVisitor.getFileVisitResults();
      if(pathList.size() > 3) {
        cmdStatus = CommandStatusEnum.DONE;
      } else {
        cmdStatus = CommandStatusEnum.PENDING;
      }
    } catch (IOException e) {
      cmdStatus = CommandStatusEnum.ERROR;
    }

    return this.cmdStatus;
  }

  private void setThemesForKDE() throws IOException, InstallerException {
    log.debug("Entering...........");
    Document doc = utilBean.getDocument("kde");
    Elements tableTags = doc.select("table");
    Element tableTag = tableTags.get(0);
    Node tbodyTag = tableTag.childNode(3);
    Elements trTags = ((Element)tbodyTag).select("tr");
    List<Element> trList = trTags.stream().collect(Collectors.toList());

    String cchName = "KDE Themes";
    Optional<CustomCategoryHeader> cchOpt = cchSrvc.findByName(cchName);
    if(cchOpt.isPresent()) {
      CustomCategoryHeader cch = cchOpt.get();
      trList.forEach(trTag -> {
        Elements tdTags = trTag.select("td");
        List<Element> tdList = tdTags.stream().collect(Collectors.toList());
        Element colName = tdList.get(0);
        Element colDesc = tdList.get(4);
        String aurPckg = colName.child(0).text();
        String desc = String.format("— %s",colDesc.text().trim());
        switch (aurPckg) {
          case "gnome-breeze-git":
          case "klassik-plasma-theme":
          case "kvantum-qt5-git":
          case "maia-cursor-theme-git":
          case "plasma-theme-helium":
          case "sddm-acidhub-theme":
          case "sddm-old-breeze-theme":
          case "sddm-old-breeze-theme-tweak":
          case "sddm-theme-kde-plasma-chili":
          case "xcursor-breeze":
          case "xfwm4-theme-breeze": {
            break;
          }
          default: {
            log.debug("[{}] - [- {}]", aurPckg, desc);
            Optional<AurPackages> optVal = aurSrvc.findByPackageName(aurPckg);
            CustomCategoryDetails ccd = new CustomCategoryDetails();
            ccd.setCcHeader(cch);
            AurPackages aurPackage = (optVal.isPresent()) ? optVal.get() : new AurPackages();
            if(!optVal.isPresent()) {
              aurPackage.setDisplayName(aurPckg);
              aurPackage.setAurPackage(aurPckg);
              aurPackage.setAurDescription(desc);
              aurSrvc.saveOrUpdate(aurPackage);
            }
            ccd.setAurPackage(aurPackage);
            ccdSrvc.saveOrUpdate(ccd);
            cch.getDetails().add(ccd);
            break;
          }
        }
      });
    } else {
      throw new InstallerException(String.format("CustomCategoryHeader with name: [%s] was NOT found!!!!!!!", cchName));
    }
    log.debug("Exiting............");
  }

  private void addAurPackages() throws IOException, InstallerException {
    Document doc = utilBean.getDocument("appList");
    Elements ulTags = doc.select("ul");
    processTags(ulTags.stream().collect(Collectors.toList()));
  }

  private void processTags(List<Element> elemList) {
    for (Element ulTag : elemList) {
      Element dlTag = ulTag.nextElementSibling();
      if(Objects.nonNull(dlTag) && dlTag.tagName().equalsIgnoreCase("dl")) {
        Element liTag = ulTag.child(0);
        final List<Node> children = liTag.childNodes();
        Element nameNode = (Element) children.get(0);
        String aurName = (nameNode.children().size() > 0) ? nameNode.child(0).text() : nameNode.text();
        String desc = getPackageDescription(children);

        String aurPckg = getPackageName(dlTag.child(0));
        if(aurPckg.equalsIgnoreCase("xfce4-notes-plugin")) {
          aurName="Xfce4 Notes";
        }

        log.debug("aurName: [{}]\n[{}]", aurName, desc);

        Optional<AurPackages> optVal = aurSrvc.findByDisplayName(aurName);
        AurPackages aurPackage = (optVal.isPresent()) ? optVal.get() : new AurPackages();
        if(!optVal.isPresent()) {
          aurPackage.setDisplayName(aurName);
          aurPackage.setAurPackage(aurPckg);
          aurPackage.setAurDescription(desc);
          aurSrvc.saveOrUpdate(aurPackage);
        }
      }
    }
  }

  private String getPackageDescription(List<Node> children) {
    String desc;
    if (children.size() > 2) {
      List<String> textLines = new ArrayList<>();
      List<Node> childNodes = IntStream.range(1, children.size()).mapToObj(i -> children.get(i)).collect(Collectors.toList());
      String text = "";
      for (Node childNode : childNodes) {
        if (childNode instanceof TextNode) {
          text = ((TextNode) childNode).text();
        } else {
          Node node = childNode.childNodes().get(0);
          if (node instanceof TextNode) {
            text = ((TextNode)node).text();
          } else {
            text = ((TextNode)node.childNodes().get(0)).text();
          }
        }
        if (text.equals(".")) {
          int lastIndex = textLines.size() - 1;
          textLines.set(lastIndex, textLines.get(lastIndex) + ".");
        } else {
          textLines.add(text.trim());
        }
      }
      desc = StringUtils.join(textLines.iterator(), ' ').trim();
    } else {
      desc = ((TextNode) children.get(1)).text().trim();
    }
    return desc;
  }

  private String getPackageName(Element elem) {
    List<Node> childNodes = new ArrayList<>(elem.childNodes());
    Collections.reverse(childNodes);
    for (Node node : childNodes) {
      if(node.nodeName().equals("span")) {
        elem = (Element) node.childNode(0);
        break;
      } else if(node.nodeName().equals("a")) {
        elem = (Element) node;
        break;
      }
    }
    String[] splitStr = StringUtils.split(elem.text(), '/');
    return splitStr[splitStr.length - 1];
  }

  private void setCategoryOptions() throws InstallerException {
    readChoicesFile();
    List<MenuLevelOne> list = level1Service.findAll();
    for (MenuLevelOne mlo : list) {
      MenuLevelOne levelOne = (MenuLevelOne) level1Service.findByID(mlo.getId()).get();
      Set<MenuLevelTwo> subCats = levelOne.getSubCats();
      for (MenuLevelTwo mlt : subCats) {
        MenuLevelTwo subCat = (MenuLevelTwo) level2Service.findByID(mlt.getId()).get();
        setCatOptsForLevel2(subCat);
      }
    }

    addOptsForCustCat();
  }

  private void setCatOptsForLevel2(MenuLevelTwo subCat) throws InstallerException {
    Set<MenuLevelThree> level3Cats = subCat.getSubCatsLevel3();
    int numSCs = level3Cats.size();
    if(numSCs > 0) {
      for (MenuLevelThree level3Cat : level3Cats) {
        addMenuOptsForCat(null, level3Cat);
      }
    } else {
      addMenuOptsForCat(subCat, null);
    }
  }

  private void readChoicesFile() throws InstallerException {
    String keyLevel1 = "";
    String fileName = "/config/choices.txt";
    Map<String, String> map;
    try (InputStream is = GenRequiredFilesServiceImpl.class.getResourceAsStream(fileName);
         Scanner scanner = new Scanner(is) ) {
      while (scanner.hasNextLine()) {
        String line = scanner.nextLine();
        String opt = line.trim();
        if(StringUtils.isNotBlank(opt)) {
          int count = line.indexOf(opt);
          int level = (count/2) + 1;
          switch(level) {
            case 1: {
              keyLevel1=opt;
              catOptsMap.put(keyLevel1, new ArrayList<>());
              break;
            }
            case 2: {
              catOptsMap.get(keyLevel1).add(opt);
              break;
            }
            default: {
              log.error("No case defined for Level: [{}] - [{}]", level, opt);
              break;
            }
          }
        }
      }
    } catch (IOException e) {
      log.error("Unable read in text file: [{}]", fileName, e);
      throw new InstallerException(String.format("Unable to read in text file: [%s]", fileName));
    }

    List<String> taskbars = catOptsMap.get("Taskbars");
    taskbars.add(3, "Fbpanel");
  }

  private void addMenuOptsForCat(MenuLevelTwo level2Cat, MenuLevelThree level3Cat) throws InstallerException {
    String catLabel = (Objects.nonNull(level2Cat)) ? level2Cat.getMenuLabel() : level3Cat.getMenuLabel();
    List<String> menuOpts = catOptsMap.remove(catLabel);
    if(CollectionUtils.isNotEmpty(menuOpts)) {
      for (String opt : menuOpts) {
        Optional<AurPackages> optVal = aurSrvc.findByDisplayName(opt);
        if (optVal.isPresent()) {
          CategoryOptions catOpt = new CategoryOptions();
          catOpt.setLevel2(level2Cat);
          catOpt.setLevel3(level3Cat);
          catOpt.setAurPackage(optVal.get());
          catOptSrvc.saveOrUpdate(catOpt);
        } else {
          Optional<CustomCategoryHeader> cchOptVal = cchSrvc.findByName(opt);
          if (cchOptVal.isPresent()) {
            addOptsForCustCat(cchOptVal.get(), opt);
          } else {
            throw new InstallerException(String.format("Neither AUR Package nor Custom Category Header was found using: [%s]!!!!!!!", opt));
          }
        }
      }
    }
  }

  private void addOptsForCustCat() throws InstallerException {
    List<String> custCats = catOptsMap.keySet().stream().collect(Collectors.toList());
    for (String custCat : custCats) {
      Optional<CustomCategoryHeader> cchOptVal = cchSrvc.findByName(custCat);
      if (cchOptVal.isPresent()) {
        addOptsForCustCat(cchOptVal.get(), custCat);
      } else {
        throw new InstallerException(String.format("The Custom Category Header was not found for name: [%s]!!!!!!!", custCat));
      }
    }
    log.debug("done");
  }

  private void addOptsForCustCat(CustomCategoryHeader cch, String custCat) throws InstallerException {
    List<String> menuOpts = catOptsMap.remove(custCat);
    if(CollectionUtils.isNotEmpty(menuOpts)) {
      for (String custOpt : menuOpts) {
        Optional<AurPackages> optVal = aurSrvc.findByDisplayName(custOpt);
        if(optVal.isPresent()) {
          CustomCategoryDetails ccd = new CustomCategoryDetails();
          ccd.setCcHeader(cch);
          ccd.setAurPackage(optVal.get());
          ccdSrvc.saveOrUpdate(ccd);
        } else {
          throw new InstallerException(String.format("The AUR Package was not found for the custom option: [%s] !!!!!!!", custOpt));
        }
      }
    }
  }

  private void createFileForKDE() throws IOException {
    String fileName = SCRIPT_FILE_NAMES.get("KDE");
    Path incFile = Paths.get(incDir.toString(), fileName);
    List<String> kdeCats = List.of("KDE Themes" , "KDE Administer CUPS", "KDE Application Menu Editors"
                                  , "KDE Audio & Video Codecs");
    Map<String, List<String>> arrayConsts = new LinkedHashMap<>();
    Map<String, String> optDescs = new LinkedHashMap<>();
    Map<String, String> optPckgs = new LinkedHashMap<>();

    utilBean.initDirFile(incDir, incFile);

    kdeCats.forEach(cat -> setCustCatData(cat, arrayConsts, optDescs, optPckgs));

    try (FileWriter fileWriter = new FileWriter(incFile.toFile());
         PrintWriter printWriter = new PrintWriter(fileWriter)) {

      utilBean.printHeader(printWriter);

      for (Map.Entry<String, List<String>> me : arrayConsts.entrySet()) {
        log.debug("\nCat: [{}] - {}", me.getKey(), me.getValue());
        utilBean.addMenuOptsArray(me.getKey(), me.getValue(), printWriter);
      }

      if(MapUtils.isNotEmpty(optPckgs)) {
        utilBean.addAURDescsArray(optPckgs, "KDE_AUR_PCKGS", printWriter);
      }
      utilBean.addAURDescsArray(optDescs,"KDE_AUR_DESCS", printWriter);
    } catch (IOException e) {
      log.error("Error trying to append to file: [{}]", fileName, e);
      throw e;
    }
  }

  private void setCustCatData(String custCatName, Map<String, List<String>> arrayConsts, Map<String, String> optDescs
                            , Map<String, String> optPckgs) {
    CustomCategoryHeader cch = cchSrvc.findByName(custCatName).get();
    Set<CustomCategoryDetails> ccdList = cch.getDetails();
    List<String> arrayVals = new ArrayList<>();
    for (CustomCategoryDetails ccd : ccdList) {
      AurPackages aurPackage = ccd.getAurPackage();
      String opt = aurPackage.getDisplayName();
      arrayVals.add(opt);
      if(!custCatName.contentEquals("KDE Themes")) {
        optPckgs.put(opt, aurPackage.getAurPackage());
      }
      optDescs.put(opt, utilBean.getDescription(aurPackage.getAurDescription()));
    }

    arrayConsts.put(cch.getScriptVarName(), arrayVals);
  }

  /**
   * Generate the array constants file for the Video Card Drivers (VCD).
   *
   * @throws IOException
   */
  private void createFileForVCD() throws IOException {
    String fileName = SCRIPT_FILE_NAMES.get("VCD");
    Path incFile = Paths.get(incDir.toString(), fileName);
    List<String> vcdCats = List.of("AMD / ATI", "AMDGPU", "AMD Catalyst", "ATI", "Brands", "Bumblebee", "Intel"
        , "Nouveau", "NVIDIA" , "NVIDIA Proprietary", "VirtualBox", "VMware", "Vulkan");
    Map<String, List<String>> arrayConsts = new LinkedHashMap<>();
    Map<String, String> optDescs = new LinkedHashMap<>();
    Map<String, String> optPckgs = new LinkedHashMap<>();

    utilBean.initDirFile(incDir, incFile);

    vcdCats.forEach(cat -> setCustCatData(cat, arrayConsts, optDescs, optPckgs));

    try (FileWriter fileWriter = new FileWriter(incFile.toFile());
         PrintWriter printWriter = new PrintWriter(fileWriter)) {

      utilBean.printHeader(printWriter);

      for (Map.Entry<String, List<String>> me : arrayConsts.entrySet()) {
        log.debug("\nCat: [{}] - {}", me.getKey(), me.getValue());
        utilBean.addMenuOptsArray(me.getKey(), me.getValue(), printWriter);
      }

      utilBean.addAURDescsArray(optDescs,"VCD_DESCS", printWriter);
    } catch (IOException e) {
      log.error("Error trying to append to file: [{}]", fileName, e);
      throw e;
    }
  }

  private List<MenuLevelOne> createFilesForPostInst() throws IOException {
    List<MenuLevelOne> listLevel1 = level1Service.findAll();
    createFileForSoftCats(listLevel1);

    Map<String,String> menuMap = new LinkedHashMap<>();
    Map<String,String> subCatMap = new LinkedHashMap<>();

    for (MenuLevelOne level1 : listLevel1) {
      MenuLevelOne level1Cat = (MenuLevelOne) level1Service.findByID(level1.getId()).get();
      Set<MenuLevelTwo> level2Cats = level1Cat.getSubCats();
      level2Cats.forEach(subCat -> menuMap.put(subCat.getMenuOption(), subCat.getMenuLabel()));
      List<String> menuOpts = menuMap.keySet().stream().collect(Collectors.toList());
      for (MenuLevelTwo mlt : level2Cats) {
        BigInteger level2ID = mlt.getId();
        MenuLevelTwo level2Cat = (MenuLevelTwo) level2Service.findOne(level2ID).get();
        Set<CategoryOptions> catOpts = level2Cat.getLevel2CatOpts();
        if(CollectionUtils.isEmpty(catOpts)) {
          level2Cat = (MenuLevelTwo) level2Service.findByID(level2ID).get();
          Set<MenuLevelThree> subCatsLevel3 = level2Cat.getSubCatsLevel3();
          addLeve3CatToMap(level2Cat.getMenuLabel(), subCatsLevel3, subCatMap, menuMap);
        }
      }

      createArrayConstsFile(menuOpts, menuMap, subCatMap, level1Cat.getMenuLabel());
      menuMap.clear();
      subCatMap.clear();
    }

    return listLevel1;
  }

  private void addLeve3CatToMap(String menuLabel, Set<MenuLevelThree> subCatsLevel3, Map<String, String> subCatMap
                              , Map<String,String> menuMap) {
    if(CollectionUtils.isNotEmpty(subCatsLevel3)) {
      List<String> menuOpts = new ArrayList<>();
      subCatsLevel3.forEach(subCat -> {
        menuOpts.add(subCat.getMenuOption());
        menuMap.put(subCat.getMenuOption(), subCat.getMenuLabel());
      });
      String arrayVal = StringUtils.join(menuOpts.iterator(), "|||");
      subCatMap.put(menuLabel, arrayVal);
    }
  }

  private void createFileForSoftCats(List<MenuLevelOne> listLevel1) throws IOException {
    Map<String,String> menuMap = new LinkedHashMap<>();
    listLevel1.forEach(mlo -> menuMap.put(mlo.getMenuOption(), mlo.getMenuLabel()));
    List<String> menuOpts = menuMap.keySet().stream().collect(Collectors.toList());
    createArrayConstsFile(menuOpts, menuMap, null, "PostInstMenu");
  }

  /**
   * Create the initial version of the files that will contain the menu options of
   * the categories to display when their software task (i.e. Basic Setup, Desktop Configuration, etc.)
   * is selected.
   *
   * @param menuMap
   * @param subCatMap
   * @param cat
   * @throws IOException
   */
  private void createArrayConstsFile(List<String> menuOpts, Map<String, String> menuMap, Map<String,String> subCatMap
      , String cat) throws IOException {
    Path incFile = Paths.get(incDir.toString(), getFileName(cat));
    utilBean.initDirFile(incDir, incFile);
    String descVarName = (cat.contentEquals("PostInstMenu")) ? "MENU_DESC_ASSOC_ARRAY": "DESC_ASSOC_ARRAY";
    try (FileWriter fileWriter = new FileWriter(incFile.toFile());
         PrintWriter printWriter = new PrintWriter(fileWriter)) {
      utilBean.printHeader(printWriter);
      utilBean.addMenuOptsArray("MENU_OPTS_ARRAY", menuOpts, printWriter);
      utilBean.addAURDescsArray(menuMap,descVarName, printWriter);

      addSubCatsArray(subCatMap, printWriter);
    } catch (IOException e) {
      log.error("Error trying to create file: [{}]", incFile.getFileName().toString(), e);
      throw e;
    }
  }

  private void addSubCatsArray(Map<String, String> subCats, PrintWriter printWriter) {
    if (MapUtils.isNotEmpty(subCats)) {
      printWriter.printf("declare -A %s=(", "SUB_CAT_ASSOC_ARRAY");

      for (Map.Entry<String, String> entry : subCats.entrySet()) {
        printWriter.println(String.format("[\"%s\"]=\"%s\"", entry.getKey(), entry.getValue()));
      }

      printWriter.println(")");
    }
  }

  private String getFileName(String cat) {
    String fileName;
    switch (cat) {
      case "Audio & Video Codecs": {
        fileName = "codecs.sh";
        break;
      }
      case "Desktop & Icon Themes": {
        fileName = "desktop-icon-themes.sh";
        break;
      }
      case "Integrated Development Environments (IDEs)": {
        fileName = "IDEs.sh";
        break;
      }
      case "Readers & Viewers": {
        fileName = "readers-viewers.sh";
        break;
      }
      default: {
        fileName = SCRIPT_FILE_NAMES.get(cat);
        if (StringUtils.isBlank(fileName)) {
          String[] splitStr = StringUtils.split(cat, ' ');
          fileName = StringUtils.join(splitStr, '-').toLowerCase() + ".sh";
        }
        break;
      }
    }
    return fileName;
  }

  private void appendToBasicSetupFile(MenuLevelOne mlo) throws IOException {
    Map<String, List<String>> arrayConsts = new LinkedHashMap<>();
    Map<String, String> optDescs = new LinkedHashMap<>();
    Map<String, String> optPckgs = new LinkedHashMap<>();
    MenuLevelOne catLevel1 = (MenuLevelOne) level1Service.findByID(mlo.getId()).get();
    Set<MenuLevelTwo> subCats = catLevel1.getSubCats();
    for (MenuLevelTwo subCat : subCats) {
      BigInteger level2ID = subCat.getId();
      MenuLevelTwo catLevel2 = (MenuLevelTwo) level2Service.findOne(level2ID).get();
      Set<CategoryOptions> catOpts = catLevel2.getLevel2CatOpts();
      if(CollectionUtils.isNotEmpty(catOpts)) {
        setDataForCat(getVarName(catLevel2), catOpts, arrayConsts, optDescs, optPckgs);
      } else {
        catLevel2 = (MenuLevelTwo) level2Service.findOneWithJoin(subCat.getId()).get();
        Set<MenuLevelThree> subCatsLevel3 = catLevel2.getSubCatsLevel3();
        if(CollectionUtils.isNotEmpty(subCatsLevel3)) {
          subCatsLevel3.forEach(lvl3Cat -> setDataForCat(getVarName(lvl3Cat), lvl3Cat.getLvl3CatOpts()
              , arrayConsts, optDescs, optPckgs));
        }
      }
    }
    appendToFile(getFileName(mlo.getMenuLabel()), arrayConsts, optDescs, optPckgs);
  }

  private String getVarName(MenuLevelTwo lvl2Cat) {
    String arrayVarName = lvl2Cat.getScriptVarName();
    if(StringUtils.isBlank(arrayVarName)) {
      String[] splitStr = StringUtils.split(lvl2Cat.getMenuLabel(), ' ');
      arrayVarName = StringUtils.join(splitStr, '_').toUpperCase();
    }
    return arrayVarName;
  }

  private String getVarName(MenuLevelThree lvl3Cat) {
    String arrayVarName = lvl3Cat.getScriptVarName();
    if(StringUtils.isBlank(arrayVarName)) {
      String[] splitStr = StringUtils.split(lvl3Cat.getMenuLabel(), ' ');
      arrayVarName = StringUtils.join(splitStr, '_').toUpperCase();
    }
    return arrayVarName;
  }

  private void setDataForCat(String arrayVarName, Set<CategoryOptions> catOpts, Map<String, List<String>> arrayConsts
      , Map<String, String> optDescs , Map<String, String> optPckgs) {
    List<String> arrayVals = new ArrayList<>();
    for (CategoryOptions catOpt : catOpts) {
      AurPackages aurPackage = catOpt.getAurPackage();
      String opt = aurPackage.getDisplayName();
      arrayVals.add(opt);
      String optPckg = (StringUtils.isNotBlank(aurPackage.getAurPackage())) ? aurPackage.getAurPackage() : aurPackage.getCustomScript();
      optPckgs.put(opt, optPckg);
      optDescs.put(opt, utilBean.getDescription(aurPackage.getAurDescription()));
    }

    arrayConsts.put(arrayVarName, arrayVals);
  }

  private void appendToFile(String fileName, Map<String, List<String>> arrayConsts
      , Map<String, String> optDescs, Map<String, String> optPckgs) throws IOException {
    Path incFile = Paths.get(incDir.toString(), fileName);

    try (FileWriter fileWriter = new FileWriter(incFile.toFile(), true);
         PrintWriter printWriter = new PrintWriter(fileWriter)) {

      for (Map.Entry<String, List<String>> me : arrayConsts.entrySet()) {
        log.debug("\nCat: [{}] - {}", me.getKey(), me.getValue());
        utilBean.addMenuOptsArray(me.getKey(), me.getValue(), printWriter);
      }

      if(MapUtils.isNotEmpty(optPckgs)) {
        utilBean.addAURDescsArray(optPckgs,"AUR_PCKGS", printWriter);
      }
      utilBean.addAURDescsArray(optDescs,"AUR_PCKG_DESCS", printWriter);
    } catch (IOException e) {
      log.error("Error trying to append to file: [{}]", fileName, e);
      throw e;
    }
  }

  private void appendCatOptsToFile(MenuLevelOne mlo) throws IOException {
    Map<String, List<String>> arrayConsts = new LinkedHashMap<>();
    Map<String, String> optDescs = new LinkedHashMap<>();
    Map<String, String> optPckgs = new LinkedHashMap<>();
    MenuLevelOne catLevel1 = (MenuLevelOne) level1Service.findByID(mlo.getId()).get();
    Set<MenuLevelTwo> subCats = catLevel1.getSubCats();
    String lvl1CatName = mlo.getMenuLabel();

    for (MenuLevelTwo subCat : subCats) {
      if(! subCat.getMenuLabel().contentEquals("Video Card Drivers")) {
        BigInteger level2ID = subCat.getId();
        MenuLevelTwo catLevel2 = (MenuLevelTwo) level2Service.findOne(level2ID).get();
        Set<CategoryOptions> catOpts = catLevel2.getLevel2CatOpts();
        if(CollectionUtils.isNotEmpty(catOpts)) {
          if(catOpts.size() > 9) {
            String fileName = createFileForSubCat(catLevel2);
            if(!lvl1CatName.contentEquals("Desktop Configuration") && StringUtils.isNotBlank(fileName)) {
              arrayConsts.put(catLevel2.getMenuLabel(), List.of(fileName));
            }
          } else {
            setDataForCat(catLevel2.getMenuLabel(), catOpts, arrayConsts, optDescs, optPckgs);
          }
        } else {
          catLevel2 = (MenuLevelTwo) level2Service.findOneWithJoin(subCat.getId()).get();
          Set<MenuLevelThree> subCatsLevel3 = catLevel2.getSubCatsLevel3();
          if(CollectionUtils.isNotEmpty(subCatsLevel3)) {
            if(SUB_CAT_FILES.contains(catLevel2.getMenuLabel())) {
              String fileName = createFileForSubCat(catLevel2, subCatsLevel3);
              arrayConsts.put(catLevel2.getMenuLabel(), List.of(fileName));
            } else {
              for (MenuLevelThree lvl3Cat : subCatsLevel3) {
                String fileName = createFileForSubCat(lvl3Cat);
                if(StringUtils.isNotBlank(fileName)) {
                  arrayConsts.put(lvl3Cat.getMenuLabel(), List.of(fileName));
                }
              }
            }
          }
        }
      }
    }

    if(MapUtils.isNotEmpty(optDescs) && MapUtils.isNotEmpty(optPckgs)) {
      if(lvl1CatName.contentEquals("Desktop Configuration")) {
        Set<String> keys = new LinkedHashSet<>(arrayConsts.keySet());
        for (String key : keys) {
          String newKey = key.toUpperCase().replace(' ', '_');
          List<String> list = arrayConsts.remove(key);
          arrayConsts.put(newKey, list);
        }
        appendToFile(getFileName(lvl1CatName), arrayConsts, optDescs, optPckgs);
      } else {
        appendArrayForSubCatToFile(mlo.getMenuLabel(), arrayConsts, optDescs, optPckgs);
      }

    }
  }

  private String createFileForSubCat(MenuLevelTwo lvl2Cat, Set<MenuLevelThree> subCatsLevel3) throws IOException {
    Map<String, List<String>> arrayConsts = new LinkedHashMap<>();
    Map<String, String> optDescs = new LinkedHashMap<>();
    Map<String, String> optPckgs = new LinkedHashMap<>();
    String fileName = null;

    subCatsLevel3.forEach(subCat -> setDataForCat(subCat.getMenuLabel(), subCat.getLvl3CatOpts()
        , arrayConsts, optDescs, optPckgs));

    if(MapUtils.isNotEmpty(optDescs) && MapUtils.isNotEmpty(optPckgs)) {
      fileName = appendArrayForSubCatToFile(lvl2Cat.getMenuLabel(), arrayConsts, optDescs, optPckgs);
    }

    return fileName;
  }

  private String createFileForSubCat(MenuLevelTwo lvl2Cat) throws IOException {
    Map<String, List<String>> arrayConsts = new LinkedHashMap<>();
    Map<String, String> optDescs = new LinkedHashMap<>();
    Map<String, String> optPckgs = new LinkedHashMap<>();
    String fileName = null;
    setDataForCat(getVarName(lvl2Cat), lvl2Cat.getLevel2CatOpts(), arrayConsts, optDescs, optPckgs);

    if(MapUtils.isNotEmpty(optDescs) && MapUtils.isNotEmpty(optPckgs)) {
      if (lvl2Cat.getMenuLabel().equalsIgnoreCase("System Utils")) {
        for (String dispName : CONKY_PCKGS) {
          Optional<AurPackages> optVal = aurSrvc.findByDisplayName(dispName);
          if (optVal.isPresent()) {
            AurPackages aurPackage = optVal.get();
            String opt = aurPackage.getDisplayName();
            optDescs.put(opt, utilBean.getDescription(aurPackage.getAurDescription()));
            optPckgs.put(opt, aurPackage.getAurPackage() + " conky-manager");
          }
        }
      }
      fileName = appendArrayForSubCatToFile(lvl2Cat.getMenuLabel(), arrayConsts, optDescs, optPckgs);
    }

    return fileName;
  }

  private String createFileForSubCat(MenuLevelThree lvl3Cat) throws IOException {
    Map<String, List<String>> arrayConsts = new LinkedHashMap<>();
    Map<String, String> optDescs = new LinkedHashMap<>();
    Map<String, String> optPckgs = new LinkedHashMap<>();
    String fileName = null;

    setDataForCat(getVarName(lvl3Cat), lvl3Cat.getLvl3CatOpts(), arrayConsts, optDescs, optPckgs);
    if(MapUtils.isNotEmpty(optDescs) && MapUtils.isNotEmpty(optPckgs)) {
      fileName = appendArrayForSubCatToFile(lvl3Cat.getMenuLabel(), arrayConsts, optDescs, optPckgs);
    }

    return fileName;
  }

  private String appendArrayForSubCatToFile(String subCat, Map<String, List<String>> subCatMap
      , Map<String, String> optDescs , Map<String, String> optPckgs) throws IOException {
    String fileName = getFileName(subCat);
    Path incFile = Paths.get(incDir.toString(), fileName);
    boolean addHeader = (incFile.toFile().exists()) ? false : true;
    List<String> varNames = getVarNames(subCat);

    try (FileWriter fileWriter = new FileWriter(incFile.toFile(), true);
         PrintWriter printWriter = new PrintWriter(fileWriter)) {

      if (addHeader) {
        utilBean.printHeader(printWriter);
      }
      if (subCatMap.size() > 1) {
        addAssocArrayForSubCat(subCatMap, printWriter);
      } else {
        utilBean.addMenuOptsArray(varNames.get(0), subCatMap.values().iterator().next(), printWriter);
        if (subCat.equalsIgnoreCase("System Utils")) {
          utilBean.addMenuOptsArray("CONKY_PCKGS", CONKY_PCKGS, printWriter);
        }
      }

      if(MapUtils.isNotEmpty(optPckgs)) {
        utilBean.addAURDescsArray(optPckgs, varNames.get(1), printWriter);
      }
      if(MapUtils.isNotEmpty(optDescs)) {
        utilBean.addAURDescsArray(optDescs, varNames.get(2), printWriter);
      }
    } catch (IOException e) {
      log.error("Error trying to write to file: [{}]", incFile.getFileName().toString(), e);
      throw e;
    }

    return fileName;
  }

  private List<String> getVarNames(String subCat) {
    List<String> varNames;
    switch(subCat) {
      case "Desktop & Icon Themes": {
        varNames = List.of("DESK_ICON_THEMES", "DIT_AUR_PCKGS", "DIT_AUR_DESCS");
        break;
      }
      case "Fonts": {
        varNames = List.of("FONTS", "FONT_AUR_PCKGS", "FONT_AUR_DESCS");
        break;
      }
      case "Taskbars": {
        varNames = List.of("TASKBARS", "TASKBAR_AUR_PCKGS","TASKBAR_AUR_DESCS");
        break;
      }
      default: {
        varNames = List.of("SOFT_CAT_OPTS", "AUR_PCKGS", "AUR_PCKG_DESCS");
        break;
      }
    }

    return varNames;
  }

  private void addAssocArrayForSubCat(Map<String, List<String>> subCatMap, PrintWriter printWriter) {
    printWriter.printf("declare -A %s=(", "CATEGORY_MENU_OPTS");

    subCatMap.forEach((cat,list) -> {
      String val = StringUtils.join(list.iterator(), '|');
      printWriter.println(String.format("[\"%s\"]=\"%s\"", cat, val));
    });

    printWriter.println(")");
  }

}
