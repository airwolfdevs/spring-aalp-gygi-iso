package com.bitbucket.kooshballtb.isocreator.services.commands.impl;

import com.bitbucket.kooshballtb.isocreator.common.CommandStatusEnum;
import com.bitbucket.kooshballtb.isocreator.common.UtilityBean;
import com.bitbucket.kooshballtb.isocreator.config.YAMLConfig;
import com.bitbucket.kooshballtb.isocreator.services.commands.GenBashScriptFileService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class GenRequiredFilesServiceImpl implements GenBashScriptFileService {
  private static final String ISO_ROOT = "/root";
  private static final String PCKG_INST_SCRIPT_NAME = "install-aur-package.sh";
  public static final String SCREEN_DATA_FILE_NAME = "dialog-split-screenrc";
  public static final String TAIL_SCRIPT_NAME = "aalp-gygi-tail.sh";

  private String projRoot;
  private List<Path> generatedFiles = new ArrayList<>();
  private CommandStatusEnum cmdStatus = CommandStatusEnum.PENDING;

  @Autowired
  private UtilityBean utilBean;

  @Autowired
  private YAMLConfig ymlConfig;

  @Override
  public CommandStatusEnum execService() {
    log.debug("Entering...........");

    this.projRoot = String.format("%s/%s", ISO_ROOT, ymlConfig.getBashProj());

    try {
      if(!cmdStatus.equals(CommandStatusEnum.DONE)) {
        createSplitScreenDataFile();
        createTailScriptFile();
        utilBean.setPermissions(generatedFiles);
        cmdStatus = CommandStatusEnum.DONE;
      }
    } catch (IOException e) {
      cmdStatus = CommandStatusEnum.ERROR;
      log.error("Unable to create required files: {}", List.of(SCREEN_DATA_FILE_NAME, TAIL_SCRIPT_NAME), e);
    }

    log.debug("Exiting............");
    return this.cmdStatus;
  }

  @Override
  public CommandStatusEnum getCmdStatus() {
    Path dir = Paths.get(System.getProperty("user.dir"));
    Path dataFile = Paths.get(dir.toString(), "data", SCREEN_DATA_FILE_NAME);
    Path scriptFile = Paths.get(dir.toString(), TAIL_SCRIPT_NAME);
    if(dataFile.toFile().exists() && scriptFile.toFile().exists()) {
      cmdStatus = CommandStatusEnum.DONE;
    } else {
      cmdStatus = CommandStatusEnum.PENDING;
    }
    return this.cmdStatus;
  }

  /**
   * Create the data file to be used by the AUR "screen" package.  This app
   * multiplexes a physical terminal.  It is used by the installer scripts
   * so that a linux "dialog" of type gauge (i.e. progress bar) and tailing
   * of the log is displayed at the same time.
   *
   * @throws IOException
   */
  private void createSplitScreenDataFile() throws IOException {
    log.debug("Entering...........");
    Path dir = Paths.get(System.getProperty("user.dir"), "data");
    Path dataFile = Paths.get(dir.toString(), SCREEN_DATA_FILE_NAME);

    utilBean.initDirFile(dir, dataFile);
    generatedFiles.add(dataFile);

    try (FileWriter fileWriter = new FileWriter(dataFile.toFile());
         PrintWriter printWriter = new PrintWriter(fileWriter)) {
      printWriter.println("startup_message off");
      printWriter.println(String.format("screen -t progBar %s/scripts/%s", projRoot, PCKG_INST_SCRIPT_NAME));
      printWriter.println("split");
      printWriter.println("focus down");
      printWriter.println(String.format("screen -t tailLog %s/scripts/%s", projRoot, TAIL_SCRIPT_NAME));
    } catch (IOException e) {
      log.error("Error trying to create file: [{}]", SCREEN_DATA_FILE_NAME, e);
      throw e;
    }

    log.debug("Exiting............");
  }

  /**
   * This file is used by the AUR "screen" package
   * in order to tail the installer's log file.
   *
   * @throws IOException
   */
  private void createTailScriptFile() throws IOException {
    log.debug("Entering...........");
    Path dir = Paths.get(System.getProperty("user.dir"));
    Path file = Paths.get(dir.toString(), TAIL_SCRIPT_NAME);
    List<String> lines = new ArrayList<>();
    lines.add(String.format("declare LOG_DIR=\"%s/scripts/data\"", projRoot));
    lines.add("");
    lines.add("#===============================================================================");
    lines.add("# functions/methods");
    lines.add("#===============================================================================");
    lines.add("");
    lines.add("#---------------------------------------------------------------------------------------");
    lines.add("#  Main");
    lines.add("#---------------------------------------------------------------------------------------");
    lines.add("main() {");
    lines.add("  tail -f \"$LOG_DIR/aalp-gygi.log\" | ccze -A");
    lines.add("}");
    lines.add("");
    lines.add("main \"$@\"");
    lines.add("exit 0");

    utilBean.initDirFile(dir, file);
    generatedFiles.add(file);

    try (FileWriter fileWriter = new FileWriter(file.toFile());
         PrintWriter printWriter = new PrintWriter(fileWriter)) {
      utilBean.printHeader(printWriter);
      printWriter.println(StringUtils.join(lines.iterator(), '\n'));
    } catch (IOException e) {
      log.error("Error trying to create file: [{}]", TAIL_SCRIPT_NAME, e);
      throw e;
    }
    log.debug("Exiting............");
  }

}
