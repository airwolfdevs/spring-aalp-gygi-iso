package com.bitbucket.kooshballtb.isocreator.services.commands.impl;

import com.bitbucket.kooshballtb.isocreator.common.CommandStatusEnum;
import com.bitbucket.kooshballtb.isocreator.common.InstallerException;
import com.bitbucket.kooshballtb.isocreator.common.UtilityBean;
import com.bitbucket.kooshballtb.isocreator.services.commands.GenBashScriptFileService;
import com.bitbucket.kooshballtb.isocreator.shell.ProgressCounter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.http.impl.client.CloseableHttpClient;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

@Service
@Slf4j
public class HtmlFileDownloadServiceImpl implements GenBashScriptFileService {
  @Autowired
  private UtilityBean utilBean;

  @Autowired
  ProgressCounter progressCounter;

  @Autowired
  private CloseableHttpClient httpClient;

  private CommandStatusEnum cmdStatus = CommandStatusEnum.PENDING;

  public CommandStatusEnum execService() {
    Path htmlFile = null;
    try {
      for(Map.Entry<String, Map<String, String>> me : utilBean.MAP_FILE_URLS.entrySet()) {
        Map<String, String> map = me.getValue();
        htmlFile = Paths.get(map.get("file"));
        if(!htmlFile.toFile().exists()) {
          downloadHTML(map.get("url"), htmlFile, map.get("hrSize"));
        }
        cmdStatus = CommandStatusEnum.DONE;
        progressCounter.reset();
      }
    } catch (IOException | InstallerException e) {
      cmdStatus = CommandStatusEnum.ERROR;
      log.error("Unable to create HTML file: [{}]", htmlFile.toString(), e);
    }

    return this.cmdStatus;
  }

  private void downloadHTML(String url, Path htmlFile, String expSize) throws IOException, InstallerException {
    log.info("Downloading HTML for: [{}]", htmlFile.getFileName().toString());
    progressCounter.display(1, "Connecting to website!");
    HttpComponentsClientHttpRequestFactory requestFactory =
        new HttpComponentsClientHttpRequestFactory(httpClient);

    ResponseEntity<String> response = new RestTemplate(requestFactory)
        .exchange(url, HttpMethod.GET, null, String.class);

    progressCounter.display(2, "Saving HTML!");
    Document doc = Jsoup.parse(response.toString());

    FileUtils.writeStringToFile(htmlFile.toFile(), doc.outerHtml(), "UTF-8");
    progressCounter.display(3, String.format("HTML has been saved to: [%s]",htmlFile.toString()));

    String actualSize = FileUtils.byteCountToDisplaySize(FileUtils.sizeOf(htmlFile.toFile()));
    if(!actualSize.contentEquals(expSize)) {
      Files.delete(htmlFile);
      log.error("Mismatch in size of HTML file: [{}] - Expected: [{}], Actual: [{}]"
          , htmlFile.getFileName().toString(), expSize, actualSize);
      throw new InstallerException(String.format("Mismatch in size of HTML file: [%s] - Expected: [%s], Actual: [%s]"
          , htmlFile.getFileName().toString(), expSize, actualSize));
    }
  }

  @Override
  public CommandStatusEnum getCmdStatus() {
    cmdStatus = CommandStatusEnum.DONE;
    for(Map.Entry<String, Map<String, String>> me : utilBean.MAP_FILE_URLS.entrySet()) {
      Map<String, String> map = me.getValue();
      Path htmlFile = Paths.get(map.get("file"));
      if(!htmlFile.toFile().exists()) {
        cmdStatus = CommandStatusEnum.PENDING;
        break;
      }
    }
    return this.cmdStatus;
  }
}
