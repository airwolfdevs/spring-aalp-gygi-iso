package com.bitbucket.kooshballtb.isocreator.services.commands.impl;

import com.bitbucket.kooshballtb.isocreator.common.CommandStatusEnum;
import com.bitbucket.kooshballtb.isocreator.models.IsoBuildCommands;
import com.bitbucket.kooshballtb.isocreator.models.PostInstallFiles;
import com.bitbucket.kooshballtb.isocreator.services.commands.GenBashScriptFileService;
import com.bitbucket.kooshballtb.isocreator.services.jpa.IsoBuildCommandsService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class ValidateFilesServiceImpl implements GenBashScriptFileService {
  private CommandStatusEnum cmdStatus = CommandStatusEnum.PENDING;

  @Autowired
  @Qualifier("isoBuildCommandsServiceImpl")
  private IsoBuildCommandsService cmdSrvc;

  @Override
  public CommandStatusEnum execService() {
    log.debug("Entering...........");

    cmdStatus = CommandStatusEnum.PASSED;

    List<IsoBuildCommands> cmdList = cmdSrvc.findAll();
    for (IsoBuildCommands isoCmd : cmdList) {
      CommandStatusEnum cse = validateFilesForCmd(isoCmd);
      if(cse.equals(CommandStatusEnum.ERROR)) {
        isoCmd.setStatus(cse.getTextVal());
        cmdSrvc.saveOrUpdate(isoCmd);
        cmdStatus = CommandStatusEnum.ERROR;
      }
    }

    log.debug("Exiting............");
    return this.cmdStatus;
  }

  @Override
  public CommandStatusEnum getCmdStatus() {
    return CommandStatusEnum.PENDING;
  }

  private CommandStatusEnum validateFilesForCmd(IsoBuildCommands isoCmd) {
    CommandStatusEnum cse = CommandStatusEnum.DONE;
    Path baseDir = Paths.get(System.getProperty("user.dir"));
    List<String> errorList = new ArrayList<>();

    for (PostInstallFiles pif : isoCmd.getPostInstFiles()) {
      String outDir = pif.getOutputDir();
      Path destDir = (StringUtils.isNotBlank(outDir)) ? Paths.get(baseDir.toString(), outDir) : baseDir;
      Path incFile = Paths.get(destDir.toString(), pif.getFileName());

      if(!incFile.toFile().exists()) {
        cse = CommandStatusEnum.ERROR;
        errorList.add(String.format("File was NOT found: [%s]", incFile.toString()));
      } else {
        String actualSize = FileUtils.byteCountToDisplaySize(FileUtils.sizeOf(incFile.toFile()));
        String expSize = pif.getHrExpSize();
        if(!actualSize.contentEquals(expSize)) {
          cse = CommandStatusEnum.ERROR;
          errorList.add(String.format("Mismatch in size found for File: [%s] - Expected: [%s], Actual: [%s]"
              , incFile.toString(), expSize, actualSize));
        }
      }
    }

    if(cse.equals(CommandStatusEnum.ERROR)) {
      log.error("Errors found in the command: [{}]\n{}", isoCmd.getCmdName()
          , StringUtils.join(errorList.iterator(), '\n'));
    }

    return cse;
  }
}
