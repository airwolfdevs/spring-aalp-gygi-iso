package com.bitbucket.kooshballtb.isocreator.services.jpa;

import com.bitbucket.kooshballtb.isocreator.models.AurPackages;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

public interface AurPackagesService {
  Optional<AurPackages> findByID(BigInteger id);
  Optional<AurPackages> findByDisplayName(String displayName);
  Optional<AurPackages> findByPackageName(String pckgName);
  List<AurPackages> findAll();
  void saveOrUpdate(AurPackages entity);
  long getNumRecs();
}
