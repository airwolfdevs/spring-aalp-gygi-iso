package com.bitbucket.kooshballtb.isocreator.services.jpa;

import com.bitbucket.kooshballtb.isocreator.models.CustomCategoryDetails;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

public interface CustomCategoryDetailsService {
  Optional<CustomCategoryDetails> findByID(BigInteger id);
  List<CustomCategoryDetails> findAll();
  void saveOrUpdate(CustomCategoryDetails entity);
  long getNumRecs();
}
