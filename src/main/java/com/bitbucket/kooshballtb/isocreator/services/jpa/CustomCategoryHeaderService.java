package com.bitbucket.kooshballtb.isocreator.services.jpa;

import com.bitbucket.kooshballtb.isocreator.models.CustomCategoryHeader;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

public interface CustomCategoryHeaderService {
  Optional<CustomCategoryHeader> findByID(BigInteger id);
  Optional<CustomCategoryHeader> findByName(String name);
  List<CustomCategoryHeader> findAll();
  void saveOrUpdate(CustomCategoryHeader entity);
  long getNumRecs();
}
