package com.bitbucket.kooshballtb.isocreator.services.jpa;

import com.bitbucket.kooshballtb.isocreator.models.IsoBuildCommands;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

public interface IsoBuildCommandsService {
  Optional<IsoBuildCommands> findByID(BigInteger id);
  Optional<IsoBuildCommands> findByName(String cmdName);
  List<IsoBuildCommands> findAll();
  void saveOrUpdate(IsoBuildCommands entity);
}
