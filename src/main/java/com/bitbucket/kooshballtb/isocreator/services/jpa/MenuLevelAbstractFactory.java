package com.bitbucket.kooshballtb.isocreator.services.jpa;

import com.bitbucket.kooshballtb.isocreator.common.InstallerException;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

@Component
public interface MenuLevelAbstractFactory<T> {
  Optional<T> findByID(BigInteger id);
  Optional<T> findOne(BigInteger id);
  Optional<T> findOneWithJoin(BigInteger id);
  List<T> findAll();
  void saveOrUpdate(T entity) throws InstallerException;
  long getNumRecs();
}
