package com.bitbucket.kooshballtb.isocreator.services.jpa;

import com.bitbucket.kooshballtb.isocreator.models.PostInstallFiles;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

public interface PostInstallFilesService {
  Optional<PostInstallFiles> findByID(BigInteger id);
  List<PostInstallFiles> findByCmdName(String cmdName);
  List<PostInstallFiles> findAll();
}
