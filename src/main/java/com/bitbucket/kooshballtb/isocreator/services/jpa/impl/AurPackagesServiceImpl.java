package com.bitbucket.kooshballtb.isocreator.services.jpa.impl;

import com.bitbucket.kooshballtb.isocreator.models.AurPackages;
import com.bitbucket.kooshballtb.isocreator.models.QAurPackages;
import com.bitbucket.kooshballtb.isocreator.repositories.AurPackagesRepository;
import com.bitbucket.kooshballtb.isocreator.services.jpa.AurPackagesService;
import com.querydsl.core.types.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.sql.PreparedStatement;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static com.bitbucket.kooshballtb.isocreator.common.UtilityBean.ID_COL;

@Service
@Transactional
public class AurPackagesServiceImpl implements AurPackagesService {
  private static final String INSERT_SQL = "INSERT INTO aur_packages (id, display_name, aur_package, aur_description) VALUES ("
      + "(NEXT VALUE FOR aur_pckg_seq), ?, ?, ?)";

  @Autowired
  private JdbcTemplate jdbcTemplate;

  private final AurPackagesRepository repo;

  @Autowired
  public AurPackagesServiceImpl(AurPackagesRepository repo) {
    this.repo = repo;
  }

  @Override
  public Optional<AurPackages> findByID(BigInteger id) {
    QAurPackages qObj = QAurPackages.aurPackages;
    Predicate predicate = qObj.id.eq(id);
    return this.repo.findOne(predicate);
  }

  @Override
  public Optional<AurPackages> findByDisplayName(String displayName) {
    QAurPackages qObj = QAurPackages.aurPackages;
    Predicate predicate = qObj.displayName.eq(displayName);
    return this.repo.findOne(predicate);
  }

  @Override
  public Optional<AurPackages> findByPackageName(String pckgName) {
    QAurPackages qObj = QAurPackages.aurPackages;
    Predicate qPredicate = qObj.aurPackage.eq(pckgName);
    return this.repo.findOne(qPredicate);
  }

  @Override
  public List<AurPackages> findAll() {
    return this.repo.findAll();
  }

  @Override
  public void saveOrUpdate(AurPackages entity) {
    if(Objects.isNull(entity.getId())) {
      GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
      jdbcTemplate.update( psc -> {
        PreparedStatement ps = psc.prepareStatement(INSERT_SQL, new String[] {ID_COL});
        ps.setString(1, entity.getDisplayName());
        ps.setString(2, entity.getAurPackage());
        ps.setString(3, entity.getAurDescription());
        return ps;
      } , keyHolder);
      Long bdID = (Long) keyHolder.getKeys().get(ID_COL);
      entity.setId(BigInteger.valueOf(bdID));
    } else {
      this.repo.save(entity);
    }
  }

  @Override
  public long getNumRecs() {
    return this.repo.count();
  }
}
