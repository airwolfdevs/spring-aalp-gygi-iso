package com.bitbucket.kooshballtb.isocreator.services.jpa.impl;

import com.bitbucket.kooshballtb.isocreator.models.CategoryOptions;
import com.bitbucket.kooshballtb.isocreator.models.QCategoryOptions;
import com.bitbucket.kooshballtb.isocreator.repositories.CategoryOptionsRepository;
import com.bitbucket.kooshballtb.isocreator.services.jpa.MenuLevelAbstractFactory;
import com.querydsl.core.types.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.sql.PreparedStatement;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static com.bitbucket.kooshballtb.isocreator.common.UtilityBean.ID_COL;

@Service
@Transactional
public class CategoryOptionsServiceImpl implements MenuLevelAbstractFactory<CategoryOptions> {
  private static final String INSERT_SQL_LVL2 = "INSERT INTO category_options (id, level_two_id, aur_package_id) VALUES ("
      + "(NEXT VALUE FOR category_opts_seq), ?, ?)";
  private static final String INSERT_SQL_LVL3 = "INSERT INTO category_options (id, level_three_id, aur_package_id) VALUES ("
      + "(NEXT VALUE FOR category_opts_seq), ?, ?)";

  @Autowired
  private JdbcTemplate jdbcTemplate;

  private final CategoryOptionsRepository repo;

  @Autowired
  public CategoryOptionsServiceImpl(CategoryOptionsRepository repo) {
    this.repo = repo;
  }

  @Override
  public Optional<CategoryOptions> findByID(BigInteger id) {
    QCategoryOptions qObj = QCategoryOptions.categoryOptions;
    Predicate predicate = qObj.id.eq(id);
    return this.repo.findOne(predicate);
  }

  @Override
  public Optional<CategoryOptions> findOne(BigInteger id) {
    return Optional.empty();
  }

  @Override
  public Optional<CategoryOptions> findOneWithJoin(BigInteger id) {
    return Optional.empty();
  }

  @Override
  public List<CategoryOptions> findAll() {
    return this.repo.findAll();
  }

  @Override
  public void saveOrUpdate(CategoryOptions entity) {
    if(Objects.isNull(entity.getId())) {
      String sql = (Objects.nonNull(entity.getLevel2())) ? INSERT_SQL_LVL2 : INSERT_SQL_LVL3;
      long categoryID = (Objects.nonNull(entity.getLevel2())) ? entity.getLevel2().getId().longValue()
                                                            : entity.getLevel3().getId().longValue();
      GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
      jdbcTemplate.update( psc -> {
        PreparedStatement ps = psc.prepareStatement(sql, new String[] {ID_COL});
        ps.setLong(1, categoryID);
        ps.setLong(2, entity.getAurPackage().getId().longValue());
        return ps;
      } , keyHolder);
      Long bdID = (Long) keyHolder.getKeys().get(ID_COL);
      entity.setId(BigInteger.valueOf(bdID));
    } else {
      this.repo.save(entity);
    }
  }

  @Override
  public long getNumRecs() {
    return this.repo.count();
  }
}
