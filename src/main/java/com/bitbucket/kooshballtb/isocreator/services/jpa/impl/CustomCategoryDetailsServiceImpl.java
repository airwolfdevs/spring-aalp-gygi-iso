package com.bitbucket.kooshballtb.isocreator.services.jpa.impl;

import com.bitbucket.kooshballtb.isocreator.models.CustomCategoryDetails;
import com.bitbucket.kooshballtb.isocreator.models.QCustomCategoryDetails;
import com.bitbucket.kooshballtb.isocreator.repositories.CustomCategoryDetailsRepository;
import com.bitbucket.kooshballtb.isocreator.services.jpa.CustomCategoryDetailsService;
import com.querydsl.core.types.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.sql.PreparedStatement;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static com.bitbucket.kooshballtb.isocreator.common.UtilityBean.ID_COL;

@Service
@Transactional
public class CustomCategoryDetailsServiceImpl implements CustomCategoryDetailsService {
  private static final String INSERT_SQL = "INSERT INTO custom_category_details (id, cc_hdr_id, aur_package_id) VALUES ("
      + "(NEXT VALUE FOR cc_dtl_id_seq), ?, ?)";

  @Autowired
  private JdbcTemplate jdbcTemplate;

  private final CustomCategoryDetailsRepository repo;

  @Autowired
  public CustomCategoryDetailsServiceImpl(CustomCategoryDetailsRepository repo) {
    this.repo = repo;
  }

  @Override
  public Optional<CustomCategoryDetails> findByID(BigInteger id) {
    QCustomCategoryDetails qObj = QCustomCategoryDetails.customCategoryDetails;
    Predicate predicate = qObj.id.eq(id);
    return this.repo.findOne(predicate);
  }

  @Override
  public List<CustomCategoryDetails> findAll() {
    return this.repo.findAll();
  }

  @Override
  public void saveOrUpdate(CustomCategoryDetails entity) {
    if(Objects.isNull(entity.getId())) {
      GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
      jdbcTemplate.update( psc -> {
        PreparedStatement ps = psc.prepareStatement(INSERT_SQL, new String[] {ID_COL});
        ps.setLong(1, entity.getCcHeader().getId().longValue());
        ps.setLong(2, entity.getAurPackage().getId().longValue());
        return ps;
      } , keyHolder);
      Long bdID = (Long) keyHolder.getKeys().get(ID_COL);
      entity.setId(BigInteger.valueOf(bdID));
    } else {
      this.repo.save(entity);
    }
  }

  @Override
  public long getNumRecs() {
    return this.repo.count();
  }
}
