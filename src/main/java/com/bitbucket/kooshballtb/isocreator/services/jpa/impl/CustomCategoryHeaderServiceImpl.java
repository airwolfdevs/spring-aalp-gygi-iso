package com.bitbucket.kooshballtb.isocreator.services.jpa.impl;

import com.bitbucket.kooshballtb.isocreator.models.CustomCategoryHeader;
import com.bitbucket.kooshballtb.isocreator.models.QCustomCategoryHeader;
import com.bitbucket.kooshballtb.isocreator.repositories.CustomCategoryHeaderRepository;
import com.bitbucket.kooshballtb.isocreator.services.jpa.CustomCategoryHeaderService;
import com.querydsl.core.types.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.sql.PreparedStatement;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static com.bitbucket.kooshballtb.isocreator.common.UtilityBean.ID_COL;

@Service
@Transactional
public class CustomCategoryHeaderServiceImpl implements CustomCategoryHeaderService {
  private static final String INSERT_SQL = "INSERT INTO custom_category_header (id, name, script_var_name) VALUES ("
      + "(NEXT VALUE FOR cc_hdr_id_seq), ?, ?)";

  @Autowired
  private JdbcTemplate jdbcTemplate;

  private final CustomCategoryHeaderRepository repo;

  @Autowired
  public CustomCategoryHeaderServiceImpl(CustomCategoryHeaderRepository repo) {
    this.repo = repo;
  }

  @Override
  public Optional<CustomCategoryHeader> findByID(BigInteger id) {
    QCustomCategoryHeader qObj = QCustomCategoryHeader.customCategoryHeader;
    Predicate qPredicate = qObj.id.eq(id);
    return this.repo.findOne(qPredicate);
  }

  @Override
  public Optional<CustomCategoryHeader> findByName(String name) {
    QCustomCategoryHeader qObj = QCustomCategoryHeader.customCategoryHeader;
    Predicate predicate = qObj.name.eq(name);
    return this.repo.findOne(predicate);
  }

  @Override
  public List<CustomCategoryHeader> findAll() {
    return this.repo.findAll();
  }

  @Override
  public void saveOrUpdate(CustomCategoryHeader entity) {
    if(Objects.isNull(entity.getId())) {
      GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
      jdbcTemplate.update( psc -> {
            PreparedStatement ps = psc.prepareStatement(INSERT_SQL, new String[] {ID_COL});
            ps.setString(1, entity.getName());
            ps.setString(2, entity.getScriptVarName());
            return ps;
          } , keyHolder);
      Long bdID = (Long) keyHolder.getKeys().get(ID_COL);
      entity.setId(BigInteger.valueOf(bdID));
    } else {
      this.repo.save(entity);
    }
  }

  @Override
  public long getNumRecs() {
    return this.repo.count();
  }
}
