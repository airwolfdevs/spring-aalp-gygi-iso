package com.bitbucket.kooshballtb.isocreator.services.jpa.impl;

import com.bitbucket.kooshballtb.isocreator.models.IsoBuildCommands;
import com.bitbucket.kooshballtb.isocreator.models.QIsoBuildCommands;
import com.bitbucket.kooshballtb.isocreator.repositories.IsoBuildCommandsRepository;
import com.bitbucket.kooshballtb.isocreator.services.jpa.IsoBuildCommandsService;
import com.querydsl.core.types.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class IsoBuildCommandsServiceImpl implements IsoBuildCommandsService {
  private final IsoBuildCommandsRepository repo;

  @Autowired
  public IsoBuildCommandsServiceImpl(IsoBuildCommandsRepository repo) {
    this.repo = repo;
  }

  @Override
  public Optional<IsoBuildCommands> findByID(BigInteger id) {
    QIsoBuildCommands qObj = QIsoBuildCommands.isoBuildCommands;
    Predicate predicate = qObj.id.eq(id);
    return this.repo.findOne(predicate);
  }

  @Override
  public Optional<IsoBuildCommands> findByName(String cmdName) {
    QIsoBuildCommands qObj = QIsoBuildCommands.isoBuildCommands;
    Predicate predicate = qObj.cmdName.eq(cmdName);
    return this.repo.findOne(predicate);
  }

  @Override
  public List<IsoBuildCommands> findAll() {
    return this.repo.findAll();
  }

  @Override
  public void saveOrUpdate(IsoBuildCommands entity) {
    this.repo.saveAndFlush(entity);
  }
}
