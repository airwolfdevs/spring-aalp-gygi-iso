package com.bitbucket.kooshballtb.isocreator.services.jpa.impl;


import com.bitbucket.kooshballtb.isocreator.common.InstallerException;
import com.bitbucket.kooshballtb.isocreator.models.MenuLevelOne;
import com.bitbucket.kooshballtb.isocreator.models.QMenuLevelOne;
import com.bitbucket.kooshballtb.isocreator.repositories.MenuLevelOneRepository;
import com.bitbucket.kooshballtb.isocreator.services.jpa.MenuLevelAbstractFactory;
import com.querydsl.core.types.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class MenuLevelOneServiceImpl implements MenuLevelAbstractFactory<MenuLevelOne> {
  private final MenuLevelOneRepository repo;

  @Autowired
  public MenuLevelOneServiceImpl(MenuLevelOneRepository repo) {
    this.repo = repo;
  }

  @Override
  public Optional<MenuLevelOne> findByID(BigInteger id) {
    QMenuLevelOne qObj = QMenuLevelOne.menuLevelOne;
    Predicate predicate = qObj.id.eq(id);
    return this.repo.findOne(predicate);
  }

  @Override
  public Optional<MenuLevelOne> findOne(BigInteger id) {
    return Optional.empty();
  }

  @Override
  public Optional<MenuLevelOne> findOneWithJoin(BigInteger id) {
    return Optional.empty();
  }

  @Override
  public List<MenuLevelOne> findAll() {
    return this.repo.findAll();
  }

  @Override
  public void saveOrUpdate(MenuLevelOne entity) throws InstallerException {
    throw new InstallerException("Records for the 'menu_level_one' table are created within the 'data.sql' file");
  }

  @Override
  public long getNumRecs() {
    return this.repo.count();
  }
}
