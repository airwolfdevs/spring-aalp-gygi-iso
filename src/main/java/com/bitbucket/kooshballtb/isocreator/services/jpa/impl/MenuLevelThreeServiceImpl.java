package com.bitbucket.kooshballtb.isocreator.services.jpa.impl;


import com.bitbucket.kooshballtb.isocreator.common.InstallerException;
import com.bitbucket.kooshballtb.isocreator.models.MenuLevelThree;
import com.bitbucket.kooshballtb.isocreator.models.QMenuLevelThree;
import com.bitbucket.kooshballtb.isocreator.repositories.MenuLevelThreeRepository;
import com.bitbucket.kooshballtb.isocreator.services.jpa.MenuLevelAbstractFactory;
import com.querydsl.core.types.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class MenuLevelThreeServiceImpl implements MenuLevelAbstractFactory<MenuLevelThree> {
  private final MenuLevelThreeRepository repo;

  @Autowired
  public MenuLevelThreeServiceImpl(MenuLevelThreeRepository repo) {
    this.repo = repo;
  }

  @Override
  public Optional<MenuLevelThree> findByID(BigInteger id) {
    QMenuLevelThree qObj = QMenuLevelThree.menuLevelThree;
    Predicate predicate = qObj.id.eq(id);
    return this.repo.findOne(predicate);
  }

  @Override
  public Optional<MenuLevelThree> findOne(BigInteger id) {
    return Optional.empty();
  }

  @Override
  public Optional<MenuLevelThree> findOneWithJoin(BigInteger id) {
    return Optional.empty();
  }

  @Override
  public List<MenuLevelThree> findAll() {
    return this.repo.findAll();
  }

  @Override
  public void saveOrUpdate(MenuLevelThree entity) throws InstallerException {
    throw new InstallerException("Records for the 'menu_level_three' table are created within the 'data.sql' file");
  }

  @Override
  public long getNumRecs() {
    return this.repo.count();
  }
}
