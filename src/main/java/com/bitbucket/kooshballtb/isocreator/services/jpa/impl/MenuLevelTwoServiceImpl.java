package com.bitbucket.kooshballtb.isocreator.services.jpa.impl;


import com.bitbucket.kooshballtb.isocreator.common.InstallerException;
import com.bitbucket.kooshballtb.isocreator.models.MenuLevelThree;
import com.bitbucket.kooshballtb.isocreator.models.MenuLevelTwo;
import com.bitbucket.kooshballtb.isocreator.models.QMenuLevelThree;
import com.bitbucket.kooshballtb.isocreator.models.QMenuLevelTwo;
import com.bitbucket.kooshballtb.isocreator.repositories.MenuLevelThreeRepository;
import com.bitbucket.kooshballtb.isocreator.repositories.MenuLevelTwoRepository;
import com.bitbucket.kooshballtb.isocreator.services.jpa.MenuLevelAbstractFactory;
import com.querydsl.core.types.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@Transactional
public class MenuLevelTwoServiceImpl implements MenuLevelAbstractFactory<MenuLevelTwo> {
  private final MenuLevelTwoRepository repo;
  private final MenuLevelThreeRepository repo3;

  @Autowired
  public MenuLevelTwoServiceImpl(MenuLevelTwoRepository repo, MenuLevelThreeRepository repo3) {
    this.repo = repo;
    this.repo3 = repo3;
  }

  @Override
  public Optional<MenuLevelTwo> findByID(BigInteger id) {
    return this.repo.findById(id);
  }

  @Override
  public Optional<MenuLevelTwo> findOne(BigInteger id) {
    QMenuLevelTwo qObj = QMenuLevelTwo.menuLevelTwo;
    Predicate predicate = qObj.id.eq(id);
    return this.repo.findOne(predicate);
  }

  @Override
  public Optional<MenuLevelTwo> findOneWithJoin(BigInteger id) {
    QMenuLevelTwo qObj = QMenuLevelTwo.menuLevelTwo;
    Predicate predicate = qObj.id.eq(id);
    Optional<MenuLevelTwo> optVal = this.repo.findOne(predicate);
    if(optVal.isPresent()) {
      MenuLevelTwo entity = optVal.get();
      Set<MenuLevelThree> subCatsLevel3 = entity.getSubCatsLevel3();
      for (MenuLevelThree lvl3Cat : subCatsLevel3) {
        QMenuLevelThree qLevel3 = QMenuLevelThree.menuLevelThree;
        predicate = qLevel3.id.eq(lvl3Cat.getId());
        MenuLevelThree lvl3 = this.repo3.findOne(predicate).get();
        lvl3Cat.setLvl3CatOpts(lvl3.getLvl3CatOpts());
      }
    }
    return optVal;
  }

  @Override
  public List<MenuLevelTwo> findAll() {
    return this.repo.findAll();
  }

  @Override
  public void saveOrUpdate(MenuLevelTwo entity) throws InstallerException {
    throw new InstallerException("Records for the 'menu_level_two' table are created within the 'data.sql' file");
  }

  @Override
  public long getNumRecs() {
    return this.repo.count();
  }
}
