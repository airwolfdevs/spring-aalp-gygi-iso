package com.bitbucket.kooshballtb.isocreator.shell;

import com.bitbucket.kooshballtb.isocreator.config.YAMLConfig;
import lombok.extern.slf4j.Slf4j;
import org.jline.terminal.Terminal;
import org.jline.utils.AttributedStringBuilder;
import org.jline.utils.AttributedStyle;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
public class ShellHelper {
    @Autowired
    private YAMLConfig ymlConfig;

    private Terminal terminal;

    public ShellHelper(Terminal terminal) {
        this.terminal = terminal;
    }

    /**
     * Construct colored message in the given color.
     *
     * @param message message to return
     * @param color   color to print
     * @return colored message
     */
    public String getColored(String message, PromptColor color) {
        return (new AttributedStringBuilder()).append(message, AttributedStyle.DEFAULT.foreground(color.toJlineAttributedStyle())).toAnsi();
    }

    public String getInfoMessage(String message) {
        return getColored(message, PromptColor.valueOf(ymlConfig.getInfoColor()));
    }

    public String getSuccessMessage(String message) {
        return getColored(message, PromptColor.valueOf(ymlConfig.getSuccessColor()));
    }

    public String getWarningMessage(String message) {
        return getColored(message, PromptColor.valueOf(ymlConfig.getWarningColor()));
    }

    public String getErrorMessage(String message) {
        return getColored(message, PromptColor.valueOf(ymlConfig.getErrorColor()));
    }

    //--- Print methods -------------------------------------------------------

    /**
     * Print message to the console in the default color.
     *
     * @param message message to print
     */
    public void print(String message) {
        print(message, null);
    }

    /**
     * Print message to the console in the success color.
     *
     * @param message message to print
     */
    public void printSuccess(String message) {
        print(message, PromptColor.valueOf(ymlConfig.getSuccessColor()));
    }

    /**
     * Print message to the console in the info color.
     *
     * @param message message to print
     */
    public void printInfo(String message) {
        print(message, PromptColor.valueOf(ymlConfig.getInfoColor()));
    }

    /**
     * Print message to the console in the warning color.
     *
     * @param message message to print
     */
    public void printWarning(String message) {
        print(message, PromptColor.valueOf(ymlConfig.getWarningColor()));
    }

    /**
     * Print message to the console in the error color.
     *
     * @param message message to print
     */
    public void printError(String message) {
        print(message, PromptColor.valueOf(ymlConfig.getErrorColor()));
    }

    /**
     * Generic Print to the console method.
     *
     * @param message message to print
     * @param color   (optional) prompt color
     */
    public void print(String message, PromptColor color) {
        String toPrint = message;
        if (color != null) {
            toPrint = getColored(message, color);
        }
        if(terminal.getType().equalsIgnoreCase("dumb")) {
            log.info("\n{}", toPrint);
        } else {
            terminal.writer().println(toPrint);
            terminal.flush();
        }
    }

    //--- set / get methods ---------------------------------------------------

    public Terminal getTerminal() {
        return terminal;
    }

    public void setTerminal(Terminal terminal) {
        this.terminal = terminal;
    }
}
