#!/bin/bash
#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# globals
#===============================================================================
CUR_DIR="${PWD}"
INC_FILE=$(echo "$CUR_DIR/inc/inst_inc.sh")

source "$INC_FILE"
source "$CUR_DIR/inc/configure-time.sh"
source "$GRAPHICAL_PATH/conf-sys-step-inc.sh"
source "$GRAPHICAL_PATH/pswd-funcs-inc.sh"

declare BLOCK_DEVICE=""
declare DIALOG_BACK_TITLE=""
declare INV_HOST_NAME_MSG=""
declare ROOT_PSWD=""
declare UEFI_FLAG=0
declare DEBUG_FLAG=1
declare LINUX_CMD_KEYS=()
declare -A LINUX_CMD_TITLES=()
declare -A LINUX_CMDS=()
declare TS_KEY_NAME="Time Standard"
declare -A KEY_NAMES=()
declare KAAGI_LOCALE_KEYS=(
    "aa" "ak" "sq" "DZ" "ar"
    "hy" "az" "BY" "BE" "bn"
    "ber" "bho" "bi" "bs" "bg"
    "CA" "ca" "km" "CN" "zh"
    "hr" "cs" "DK" "dv" "nl"
    "dz" "en" "ER" "eo" "et"
    "ET" "fo" "hif" "FI" "FR"
    "fr" "lg" "gez" "ka" "de"
    "DE" "el" "ht" "HK" "hu"
    "is" "IN" "id" "IR" "IE"
    "IL" "it" "IT" "ja" "kl"
    "kk" "rw" "ky" "ko" "lo"
    "lv" "li" "ln" "LT" "nds"
    "LU" "MK" "mai" "mg" "ms"
    "mt" "mfe" "MX" "mn" "MM"
    "NP" "NZ" "NI" "NG" "niu"
    "NO" "om" "pa" "pap" "PG"
    "PE" "PH" "PL" "pt" "ps"
    "ro" "ru" "RU" "sm" "SN"
    "sr" "sk" "sl" "so" "ZA"
    "ES" "es" "LK" "sw" "sv"
    "CH" "TW" "tg" "ta" "th"
    "bo" "ti" "to" "TR" "tr"
    "tk" "UA" "GB" "US" "ur" "UZ")
declare -A KAAGI_LOCALES=(
    ["aa"]="aa_DJ.UTF-8;aa_ER;aa_ER@saaho;aa_ET" ["ak"]="ak_GH" ["sq"]="sq_AL.UTF-8;sq_MK" ["DZ"]="kab_DZ" ["ar"]="ar_AE.UTF-8;ar_BH.UTF-8;ar_DZ.UTF-8;ar_EG.UTF-8;ar_IN;ar_IQ.UTF-8;ar_JO.UTF-8;ar_KW.UTF-8;ar_LB.UTF-8;ar_LY.UTF-8;ar_MA.UTF-8;ar_OM.UTF-8;ar_QA.UTF-8;ar_SA.UTF-8;ar_SD.UTF-8;ar_SS;ar_SY.UTF-8;ar_TN.UTF-8;ar_YE.UTF-8"
    ["hy"]="hy_AM" ["az"]="az_AZ;az_IR" ["BY"]="be_BY.UTF-8;be_BY@latin" ["BE"]="wa_BE.UTF-8" ["bn"]="bn_BD;bn_IN"
    ["ber"]="ber_DZ;ber_MA" ["bho"]="bho_IN;bho_NP" ["bi"]="bi_VU" ["bs"]="bs_BA.UTF-8" ["bg"]="bg_BG.UTF-8"
    ["CA"]="ik_CA;iu_CA;shs_CA" ["ca"]="ca_AD.UTF-8;ca_ES.UTF-8;ca_ES@valencia;ca_FR.UTF-8;ca_IT.UTF-8" ["km"]="km_KH" ["CN"]="ug_CN" ["zh"]="zh_CN.UTF-8;zh_HK.UTF-8;zh_SG.UTF-8;zh_TW.UTF-8"
    ["hr"]="hr_HR.UTF-8" ["cs"]="cs_CZ.UTF-8" ["DK"]="da_DK.UTF-8" ["dv"]="dv_MV" ["nl"]="nl_AW;nl_BE.UTF-8;nl_NL.UTF-8"
    ["dz"]="dz_BT" ["en"]="en_AG;en_AU.UTF-8;en_BW.UTF-8;en_CA.UTF-8;en_DK.UTF-8;en_GB.UTF-8;en_HK.UTF-8;en_IE.UTF-8;en_IL;en_IN;en_NG;en_NZ.UTF-8;en_PH.UTF-8;en_SC.UTF-8;en_SG.UTF-8;en_US.UTF-8;en_ZA.UTF-8;en_ZM;en_ZW.UTF-8" ["ER"]="byn_ER;tig_ER" ["eo"]="eo" ["et"]="et_EE.UTF-8" ["ET"]="am_ET;sid_ET;wal_ET"
    ["fo"]="fo_FO.UTF-8" ["hif"]="hif_FJ" ["FI"]="fi_FI.UTF-8" ["FR"]="br_FR.UTF-8;ia_FR;oc_FR.UTF-8" ["fr"]="fr_BE.UTF-8;fr_CA.UTF-8;fr_CH.UTF-8;fr_FR.UTF-8;fr_LU.UTF-8"
    ["lg"]="lg_UG.UTF-8" ["gez"]="gez_ER;gez_ER@abegede;gez_ET;gez_ET@abegede" ["ka"]="ka_GE.UTF-8" ["de"]="de_AT.UTF-8;de_BE.UTF-8;de_CH.UTF-8;de_DE.UTF-8;de_IT.UTF-8;de_LI.UTF-8;de_LU.UTF-8" ["DE"]="hsb_DE.UTF-8"
    ["el"]="el_GR.UTF-8;el_CY.UTF-8" ["ht"]="ht_HT" ["HK"]="yue_HK" ["hu"]="hu_HU.UTF-8" ["is"]="is_IS.UTF-8"
    ["IN"]="anp_IN;ar_IN;as_IN;bhb_IN.UTF-8;bho_IN;bn_IN;bo_IN;brx_IN;doi_IN;en_IN;gu_IN;hi_IN;hne_IN;kn_IN;kok_IN;ks_IN;ks_IN@devanagari;mag_IN;mai_IN;mjw_IN;ml_IN;mni_IN;mr_IN;or_IN;pa_IN;raj_IN;sa_IN;sat_IN;sd_IN;sd_IN@devanagari;ta_IN;tcy_IN.UTF-8;te_IN;ur_IN" ["id"]="id_ID.UTF-8" ["IR"]="fa_IR" ["IE"]="ga_IE.UTF-8" ["IL"]="he_IL.UTF-8"
    ["it"]="it_CH.UTF-8;it_IT.UTF-8" ["IT"]="fur_IT;lij_IT;sc_IT" ["ja"]="ja_JP.UTF-8" ["kl"]="kl_GL.UTF-8" ["kk"]="kk_KZ.UTF-8"
    ["rw"]="rw_RW" ["ky"]="ky_KG" ["ko"]="ko_KR.UTF-8" ["lo"]="lo_LA" ["lv"]="lv_LV.UTF-8"
    ["li"]="li_BE;li_NL" ["ln"]="ln_CD" ["LT"]="lt_LT.UTF-8;sgs_LT" ["nds"]="nds_DE;nds_NL" ["LU"]="lb_LU"
    ["MK"]="mk_MK.UTF-8" ["mai"]="mai_IN;mai_NP" ["mg"]="mg_MG.UTF-8" ["ms"]="ms_MY.UTF-8" ["mt"]="mt_MT.UTF-8"
    ["mfe"]="mfe_MU" ["MX"]="nhn_MX" ["mn"]="mn_MN" ["MM"]="my_MM;shn_MM" ["NP"]="ne_NP;the_NP"
    ["NZ"]="mi_NZ.UTF-8" ["NI"]="miq_NI" ["NG"]="ha_NG;ig_NG;yo_NG" ["niu"]="niu_NU;niu_NZ" ["NO"]="nb_NO.UTF-8;nn_NO.UTF-8;se_NO"
    ["om"]="om_ET;om_KE.UTF-8" ["pa"]="pa_IN;pa_PK" ["pap"]="pap_AW;pap_CW" ["PG"]="tpi_PG;yuw_PG" ["PE"]="agr_PE;ayc_PE;quz_PE"
    ["PH"]="fil_PH;tl_PH.UTF-8" ["PL"]="csb_PL;pl_PL.UTF-8;szl_PL" ["pt"]="pt_BR.UTF-8;pt_PT.UTF-8" ["ps"]="ps_AF" ["ro"]="ro_RO.UTF-8"
    ["ru"]="ru_RU.UTF-8;ru_UA.UTF-8" ["RU"]="ce_RU;cv_RU;mhr_RU;os_RU;tt_RU;tt_RU@iqtelif" ["sm"]="sm_WS" ["SN"]="ff_SN;wo_SN" ["sr"]="sr_ME;sr_RS;sr_RS@latin"
    ["sk"]="sk_SK.UTF-8" ["sl"]="sl_SI.UTF-8" ["so"]="so_DJ.UTF-8;so_ET;so_KE.UTF-8;so_SO.UTF-8" ["ZA"]="af_ZA.UTF-8;nr_ZA;nso_ZA;ss_ZA;st_ZA.UTF-8;tn_ZA;ts_ZA;ve_ZA;xh_ZA.UTF-8;zu_ZA.UTF-8" ["ES"]="an_ES.UTF-8;ast_ES.UTF-8;eu_ES.UTF-8;gl_ES.UTF-8"
    ["es"]="es_AR.UTF-8;es_BO.UTF-8;es_CL.UTF-8;es_CO.UTF-8;es_CR.UTF-8;es_CU;es_DO.UTF-8;es_EC.UTF-8;es_ES.UTF-8;es_GT.UTF-8;es_HN.UTF-8;es_MX.UTF-8;es_NI.UTF-8;es_PA.UTF-8;es_PE.UTF-8;es_PR.UTF-8;es_PY.UTF-8;es_SV.UTF-8;es_US.UTF-8;es_UY.UTF-8;es_VE.UTF-8" ["LK"]="si_LK" ["sw"]="sw_KE;sw_TZ" ["sv"]="sv_FI.UTF-8;sv_SE.UTF-8" ["CH"]="wae_CH"
    ["TW"]="cmn_TW;hak_TW;lzh_TW;nan_TW;nan_TW@latin" ["tg"]="tg_TJ.UTF-8" ["ta"]="ta_IN;ta_LK" ["th"]="th_TH.UTF-8" ["bo"]="bo_CN;bo_IN"
    ["ti"]="ti_ER;ti_ET" ["to"]="to_TO" ["TR"]="ku_TR.UTF-8" ["tr"]="tr_CY.UTF-8;tr_TR.UTF-8" ["tk"]="tk_TM"
    ["UA"]="crh_UA;uk_UA.UTF-8" ["GB"]="cy_GB.UTF-8;gd_GB.UTF-8;gv_GB.UTF-8;kw_GB.UTF-8" ["US"]="chr_US;unm_US;yi_US.UTF-8" ["ur"]="ur_IN;ur_PK" ["UZ"]="uz_UZ.UTF-8;uz_UZ@cyrillic")
declare -A KAAGI_DIALOG_DESCS=(
    ["aa"]="none|Afar" ["ak"]="Ghana|Akan" ["sq"]="none|Albanian" ["DZ"]="Algeria|Kabyle" ["ar"]="none|Arabic"
    ["hy"]="Armenia|Armenian" ["az"]="none|Azerbaijani" ["BY"]="Belarus|Belarusian" ["BE"]="Belgium|Walloon" ["bn"]="none|Bengali"
    ["ber"]="none|Berber languages" ["bho"]="none|Bhojpuri" ["bi"]="Vanuatu|Bislama" ["bs"]="Bosnia and Herzegovina|Bosnian" ["bg"]="Bulgaria|Bulgarian"
    ["CA"]="Canada|none" ["ca"]="none|Catalan; Valencian" ["km"]="Cambodia|Central Khmer" ["CN"]="China|Uighur; Uyghur" ["zh"]="none|Chinese"
    ["hr"]="Croatia|Croatian" ["cs"]="Czech Republic|Czech" ["DK"]="Denmark|Danish" ["dv"]="Maldives|Divehi; Dhivehi; Maldivian" ["nl"]="none|Dutch; Flemish"
    ["dz"]="Bhutan|Dzongkha" ["en"]="none|English" ["ER"]="Eritrea|none" ["eo"]="none|Esperanto" ["et"]="Estonia|Estonian" ["ET"]="Ethiopia|none"
    ["fo"]="Faroe Islands|Faroese" ["hif"]="Fiji|Fiji Hindi" ["FI"]="Finland|Finnish" ["FR"]="France|none" ["fr"]="none|French"
    ["lg"]="Uganda|Ganda" ["gez"]="none|Geez" ["ka"]="Georgia|Georgian" ["de"]="none|German" ["DE"]="Germany|Upper Sorbian"
    ["el"]="none|Greek, Modern (1453-)" ["ht"]="Haiti|Haitian; Haitian Creole" ["HK"]="Hong Kong|Cantonese" ["hu"]="Hungary|Hungarian" ["is"]="Iceland|Icelandic"
    ["IN"]="India|none" ["id"]="Indonesia|Indonesian" ["IR"]="Iran, Islamic Republic of|Persian" ["IE"]="Ireland|Irish" ["IL"]="Israel|Hebrew"
    ["it"]="none|Italian" ["IT"]="Italy|none" ["ja"]="Japan|Japanese" ["kl"]="Greenland|Kalaallisut; Greenlandic" ["kk"]="Kazakhstan|Kazakh"
    ["rw"]="Rwanda|Kinyarwanda" ["ky"]="Kyrgyzstan|Kirghiz; Kyrgyz" ["ko"]="Korea, Republic of|Korean" ["lo"]="Lao People's Democratic Republic|Lao" ["lv"]="Latvia|Latvian"
    ["li"]="none|Limburgan; Limburger; Limburgish" ["ln"]="Congo, the Democratic Republic of the|Lingala" ["LT"]="Lithuania|none" ["nds"]="none|Low German; Low Saxon; German, Low; Saxon, Low" ["LU"]="Luxembourg|Luxembourgish; Letzeburgesch"
    ["MK"]="Macedonia, the Former Yugoslav Republic of|Macedonian" ["mai"]="none|Maithili" ["mg"]="Madagascar|Malagasy" ["ms"]="Malaysia|Malay" ["mt"]="Malta|Maltese"
    ["mfe"]="Mauritius|Mauritian Creole" ["MX"]="Mexico|Central Azec" ["mn"]="Mongolia|Mongolian" ["MM"]="Myanmar|none" ["NP"]="Nepal|none"
    ["NZ"]="New Zealand|Maori" ["NI"]="Nicaragua|Miskito" ["NG"]="Nigeria|none" ["niu"]="none|Niuean" ["NO"]="Norway|none"
    ["om"]="none|Oromo" ["pa"]="none|Panjabi; Punjabi" ["pap"]="none|Papiamento" ["PG"]="Papua New Guinea|none" ["PE"]="Peru|none"
    ["PH"]="Philippines|none" ["PL"]="Poland|none" ["pt"]="none|Portuguese" ["ps"]="Afghanistan|Pushto; Pashto" ["ro"]="Romania|Romanian; Moldavian; Moldovan"
    ["ru"]="none|Russian" ["RU"]="Russian Federation|none" ["sm"]="Samoa|Samoan" ["SN"]="Senegal|none" ["sr"]="none|Serbian"
    ["sk"]="Slovakia|Slovak" ["sl"]="Slovenia|Slovenian" ["so"]="none|Somali" ["ZA"]="South Africa|none" ["ES"]="Spain|none"
    ["es"]="none|Spanish; Castilian" ["LK"]="Sri Lanka|Sinhala; Sinhalese" ["sw"]="none|Swahili" ["sv"]="none|Swedish" ["CH"]="Switzerland|War-Jaintia"
    ["TW"]="Taiwan, Province of China|none" ["tg"]="Tajikistan|Tajik" ["ta"]="none|Tamil" ["th"]="Thailand|Thai" ["bo"]="none|Tibetan"
    ["ti"]="none|Tigrinya" ["to"]="Tonga|Tonga (Tonga Islands)" ["TR"]="Turkey|Kurdish" ["tr"]="none|Turkish" ["tk"]="Turkmenistan|Turkmen"
    ["UA"]="Ukraine|none" ["GB"]="United Kingdom|none" ["US"]="United States|none" ["ur"]="none|Urdu" ["UZ"]="Uzbekistan|Uzbek")
declare -A ISO_LANG_NAMES=(
    ["aa"]="Afar" ["af"]="Afrikaans" ["agr"]="Aguaruna" ["ak"]="Akan" ["sq"]="Albanian" ["am"]="Amharic"
    ["anp"]="Angika" ["ar"]="Arabic" ["an"]="Aragonese" ["hy"]="Armenian" ["as"]="Assamese"
    ["ast"]="Asturian; Bable; Leonese; Asturleonese" ["az"]="Azerbaijani" ["eu"]="Basque" ["be"]="Belarusian" ["bem"]="Bemba"
    ["bn"]="Bengali" ["ber"]="Berber languages" ["bho"]="Bhojpuri" ["bhb"]="Bhili" ["bi"]="Bislama" ["byn"]="Blin; Bilin" ["brx"]="Bodo"
    ["nb"]="Bokmål, Norwegian; Norwegian Bokmål" ["bs"]="Bosnian" ["br"]="Breton" ["bg"]="Bulgarian" ["my"]="Burmese" ["yue"]="Cantonese"
    ["ca"]="Catalan; Valencian" ["nhn"]="Central Azec" ["km"]="Central Khmer" ["ce"]="Chechen" ["chr"]="Cherokee" ["hne"]="Chhattisgarhi"
    ["zh"]="Chinese" ["the"]="Chitwania Tharu" ["cv"]="Chuvash" ["kw"]="Cornish" ["crh"]="Crimean Tatar; Crimean Turkish" ["hr"]="Croatian"
    ["quz"]="Cusco Quechua" ["cs"]="Czech" ["da"]="Danish" ["dv"]="Divehi; Dhivehi; Maldivian" ["doi"]="Dogri" ["nl"]="Dutch; Flemish"
    ["dz"]="Dzongkha" ["mhr"]="Eastern Mari" ["en"]="English" ["eo"]="Esperanto" ["et"]="Estonian" ["fo"]="Faroese" ["hif"]="Fiji Hindi"
    ["fil"]="Filipino; Pilipino" ["fi"]="Finnish" ["fr"]="French" ["fur"]="Friulian" ["ff"]="Fulah" ["gd"]="Gaelic; Scottish Gaelic"
    ["gl"]="Galician" ["lg"]="Ganda" ["gez"]="Geez" ["ka"]="Georgian" ["de"]="German" ["el"]="Greek, Modern (1453-)" ["gu"]="Gujarati"
    ["ht"]="Haitian; Haitian Creole" ["hak"]="Hakka Chinese" ["ha"]="Hausa" ["he"]="Hebrew" ["hi"]="Hindi" ["hu"]="Hungarian"
    ["is"]="Icelandic" ["ig"]="Igbo" ["id"]="Indonesian" ["ia"]="Interlingua (International Auxiliary Language Association)"
    ["iu"]="Inuktitut" ["ik"]="Inupiaq" ["ga"]="Irish" ["it"]="Italian" ["ja"]="Japanese" ["kab"]="Kabyle" ["kl"]="Kalaallisut; Greenlandic"
    ["kn"]="Kannada" ["mjw"]="Karbi" ["ks"]="Kashmiri" ["csb"]="Kashubian" ["kk"]="Kazakh" ["rw"]="Kinyarwanda" ["ky"]="Kirghiz; Kyrgyz"
    ["kok"]="Konkani" ["ko"]="Korean" ["ku"]="Kurdish" ["lo"]="Lao" ["lv"]="Latvian" ["lij"]="Ligurian" ["li"]="Limburgan; Limburger; Limburgish"
    ["lzh"]="Literary Chinese" ["ln"]="Lingala" ["lt"]="Lithuanian" ["nds"]="Low German; Low Saxon; German, Low; Saxon, Low"
    ["lb"]="Luxembourgish; Letzeburgesch" ["mk"]="Macedonian" ["mag"]="Magahi" ["mai"]="Maithili" ["mg"]="Malagasy" ["ms"]="Malay" ["ml"]="Malayalam"
    ["mt"]="Maltese" ["cmn"]="Mandarin Chinese" ["mni"]="Manipuri" ["gv"]="Manx" ["mi"]="Maori" ["mr"]="Marathi" ["mfe"]="Mauritian Creole"
    ["nan"]="Min Nan Chinese" ["miq"]="Miskito" ["mn"]="Mongolian" ["nr"]="Ndebele, South; South Ndebele" ["ne"]="Nepali" ["niu"]="Niuean"
    ["se"]="Northern Sami" ["sgs"]="Samogitian" ["nn"]="Norwegian Nynorsk; Nynorsk, Norwegian" ["oc"]="Occitan (post 1500); Provençal"
    ["or"]="Oriya" ["om"]="Oromo" ["os"]="Ossetian; Ossetic" ["pa"]="Panjabi; Punjabi" ["pap"]="Papiamento" ["nso"]="Pedi; Sepedi; Northern Sotho"
    ["fa"]="Persian" ["pl"]="Polish" ["pt"]="Portuguese" ["ps"]="Pushto; Pashto" ["raj"]="Rajasthani" ["ro"]="Romanian; Moldavian; Moldovan"
    ["ru"]="Russian" ["sm"]="Samoan" ["sa"]="Sanskrit" ["sat"]="Santali" ["sc"]="Sardinian" ["sr"]="Serbian"
    ["shn"]="Shan" ["shs"]="Shuswap" ["sid"]="Sidamo" ["szl"]="Silesian" ["sd"]="Sindhi" ["si"]="Sinhala; Sinhalese" ["sk"]="Slovak"
    ["sl"]="Slovenian" ["so"]="Somali" ["ayc"]="Southern Aymara" ["st"]="Sotho, Southern" ["es"]="Spanish; Castilian" ["sw"]="Swahili"
    ["ss"]="Swati" ["sv"]="Swedish" ["tl"]="Tagalog" ["tg"]="Tajik" ["ta"]="Tamil"
    ["tt"]="Tatar" ["te"]="Telugu" ["th"]="Thai" ["bo"]="Tibetan" ["tig"]="Tigre"
    ["ti"]="Tigrinya" ["tpi"]="Tok Pisin" ["to"]="Tonga (Tonga Islands)" ["ts"]="Tsonga" ["tn"]="Tswana" ["tcy"]="Tulu"
    ["tr"]="Turkish" ["tk"]="Turkmen" ["ug"]="Uighur; Uyghur" ["uk"]="Ukrainian" ["hsb"]="Upper Sorbian" ["unm"]="Unami"
    ["ur"]="Urdu" ["uz"]="Uzbek" ["ve"]="Venda" ["vi"]="Vietnamese" ["wal"]="Walamo"
    ["wa"]="Walloon" ["wae"]="War-Jaintia" ["cy"]="Welsh" ["fy"]="Western Frisian" ["wo"]="Wolof" ["xh"]="Xhosa"
    ["yuw"]="Yau" ["yi"]="Yiddish" ["yo"]="Yoruba" ["zu"]="Zulu")

declare -A AUR_PCKG_DESCS=(["connman"]="Intel's modular network connection manager"
    ["netctl"]="Profile based systemd network management"
    ["networkmanager"]="Network connection manager and user applications"
    ["systemd-networkd"]="A system daemon that manages network configurations"
    ["wicd"]="Wired and wireless network manager for Linux")

declare -A AUR_PCKGS_TO_INSTALL=(["connman"]="ConnMan"
    ["networkmanager"]="NetworkManager"
    ["wicd"]="Wicd")

declare FREE_DNS_PROVIDERS=("Google" "Cloudflare" "Level 3" "Verisign" "Quad9" "DNS.WATCH" "OpenNIC" "Yandex")
declare -A FREE_DNS_IPS=()
declare -A FREE_DNS_IPV4=(["Google"]="8.8.8.8|||8.8.4.4"
  ["Cloudflare"]="1.1.1.1|||1.0.0.1"
  ["Level 3"]="209.244.0.3|||209.244.0.4"
  ["Verisign"]="64.6.64.6|||64.6.65.6"
  ["Quad9"]="9.9.9.9|||9.9.9.10"
  ["DNS.WATCH"]="84.200.69.80|||84.200.70.40"
  ["OpenNIC"]="185.121.177.177|||169.239.202.202"
  ["Yandex"]="77.88.8.8|||77.88.8.1")
declare -A FREE_DNS_IPV6=(["Google"]="2001:4860:4860::8888|||2001:4860:4860::8844"
  ["Cloudflare"]="2606:4700:4700::1111|||2606:4700:4700::1001"
  ["Verisign"]="2620:74:1b::1:1|||2620:74:1c::2:2"
  ["Quad9"]="2620:fe::fe|||2620:fe::10"
  ["DNS.WATCH"]="2001:1608:10:25::1c04:b12f|||2001:1608:10:25::9249:d69b"
  ["OpenNIC"]="2a05:dfc7:5::53|||2a05:dfc7:5353::53"
  ["Yandex"]="2a02:6b8::feed:0ff|||2a02:6b8:0:1::feed:0ff")

declare HXU_NORMALIZE="${LOCAL_BIN_DIR}/hxnormalize"
declare HXU_SELECT="${LOCAL_BIN_DIR}/hxselect"
declare PHANTOM_JS="${LOCAL_BIN_DIR}/phantomjs"
declare LUKS_FLAG=0
declare LVM_FLAG=0
declare LVM_DEVICE_UUID=""
declare ROOT_DEVICE_UUID=""
declare -A varMap
source -- "$ASSOC_ARRAY_FILE"

#===============================================================================
# functions/methods
#===============================================================================

#---------------------------------------------------------------------------------------
#      METHOD:                          initVars
# DESCRIPTION: Initialize the global variables.
#---------------------------------------------------------------------------------------
initVars() {
  local startPos=$(findPositionForString VAR_KEY_NAMES "step#3")
  local taskNum=0
  for keyName in "${VAR_KEY_NAMES[@]:${startPos}}"; do
    taskNum=$(expr ${taskNum} + 1)
    local taskKey=$(printf "step#3.%d" ${taskNum})
    KEY_NAMES["$taskKey"]="$keyName"
  done

  local msgTextArray=(
    "The entered host name of '%s' fails based on the following:"
    "       a) must be between 1 to 63 characters in length"
    "       b) may contain:"
    "             1) the ASCII letters 'a' through 'z' (in a case-insensitive manner)"
    "             2) the digits '0' through '9"
    "             3) the minus sign ('-'), but NOT start with nor end with"
    "       c) other symbols, punctuation characters, or white space are NOT permitted."
    "$DIALOG_BORDER" " " "Please re-enter a valid name!")
  INV_HOST_NAME_MSG=$(getTextForDialog "${msgTextArray[@]}")
  ROOT_PSWD="/root/.root.pswd"
  BLOCK_DEVICE=$(echo "${varMap["BLOCK-DEVICE"]}")
  DIALOG_BACK_TITLE="Step#3:  Configuration of the System"

  setFlags
  if ${LUKS_FLAG}; then
    setUUIDforLVMDevice
  else
    setUUIDforRootDevice
  fi

  if [ ! ${varMap["IPv6"]+_} ] && [ ! ${varMap["IPv4"]+_} ]; then
    varMap["IPv4"]="true"
  fi
  setIPsForAltDNS
}

#---------------------------------------------------------------------------------------
#      METHOD:                          setFlags
# DESCRIPTION: Sets the global variable flags for EFI, LUKS, etc.
#---------------------------------------------------------------------------------------
setFlags() {
  DEBUG_FLAG=$(echo 'false' && return 1)
  if [[ ! -z "${AALP_GYGI_YAD}" ]] && [ "$AALP_GYGI_YAD" == "debug" ]; then
    DEBUG_FLAG=$(echo 'true' && return 0)
  elif [[ ! -z "${AALP_GYGI_DIALOG}" ]] && [ "$AALP_GYGI_DIALOG" == "debug" ]; then
    DEBUG_FLAG=$(echo 'true' && return 0)
  fi

  if ${DEBUG_FLAG}; then
    ROOT_MOUNTPOINT="/tmp/mnt"
    if [ ! -d "${ROOT_MOUNTPOINT}/etc" ] ; then
      mkdir -p "$ROOT_MOUNTPOINT"
      cp -rf "/etc/" "${ROOT_MOUNTPOINT}/."
    fi
  fi

  UEFI_FLAG=$(echo 'false' && return 1)
  if [ "${varMap["BOOT-MODE"]}" == "UEFI" ]; then
    UEFI_FLAG=$(echo 'true' && return 0)
  fi

  LUKS_FLAG=$(echo 'false' && return 1)
  LVM_FLAG=$(echo 'false' && return 1)
  case "${varMap["PART-LAYOUT"]}" in
    "LVM w/ dm-crypt")
      LUKS_FLAG=$(echo 'true' && return 0)
      LVM_FLAG=$(echo 'true' && return 0)
    ;;
    "LVM")
      LVM_FLAG=$(echo 'true' && return 0)
    ;;
  esac
}

#---------------------------------------------------------------------------------------
#      METHOD:                     setUUIDforLVMDevice
# DESCRIPTION: Sets the UUID of the LVM container
#---------------------------------------------------------------------------------------
setUUIDforLVMDevice() {
  local cmdOutput=$(changeRoot "pvdisplay")
  local lines=()
  readarray -t lines <<< "$cmdOutput"
  local line=$(trimString "${lines[1]}")
  line=$(echo "$line" | sed 's/ \+/\n/g')
  readarray -t lines <<< "$line"
  line=$(trimString "${lines[-1]}")
  cmdOutput=$(changeRoot "blkid \"$line\"")
  readarray -t lines <<< "${cmdOutput// /$'\n'}"
  line=$(trimString "${lines[1]}")
  LVM_DEVICE_UUID=$(echo "$line" | cut -d'"' -f2)
}

#---------------------------------------------------------------------------------------
#      METHOD:                     setUUIDforRootDevice
# DESCRIPTION: Sets the UUID of the LVM container
#---------------------------------------------------------------------------------------
setUUIDforRootDevice() {
  local cmd="blkid -o list"
  local cmdOutput=$(changeRoot "$cmd")
  local rows=()
  local cols=()
  local idx=2

  if ${LVM_FLAG}; then
    idx=1
  fi

  readarray -t rows <<< "$cmdOutput"
  for row in "${rows[@]:3}"; do
    row=$(trimString "$row")
    row=$(echo "$row" | sed "s/ \+/${FORM_FLD_SEP}/g")
    readarray -t cols <<< "${row//$FORM_FLD_SEP/$'\n'}"
    if [ "${cols[$idx]}" == "/" ]; then
      ROOT_DEVICE_UUID=$(echo "${cols[-1]}")
    fi
  done

}

#---------------------------------------------------------------------------------------
#      METHOD:                       setIPsForAltDNS
# DESCRIPTION: Set either the IPv6 or IPv4 addresses for the free DNS Providers
#---------------------------------------------------------------------------------------
setIPsForAltDNS() {
  local providers=("${FREE_DNS_PROVIDERS[@]}")
  FREE_DNS_PROVIDERS=()

  if [ ${varMap["OpenNIC"]+_} ]; then
    local ipAddrs=()
    readarray -t ipAddrs <<< "${varMap["OpenNIC"]//$FORM_FLD_SEP/$'\n'}"
    local concatStr=$(printf "$FORM_FLD_SEP%s" "${ipAddrs[@]:0:2}")
    concatStr=${concatStr:${#FORM_FLD_SEP}}
    FREE_DNS_IPV4["OpenNIC"]="$concatStr"
    concatStr=$(printf "$FORM_FLD_SEP%s" "${ipAddrs[@]:2}")
    concatStr=${concatStr:${#FORM_FLD_SEP}}
    FREE_DNS_IPV6["OpenNIC"]="$concatStr"
  fi

  for dnsProv in "${providers[@]}"; do
    if [ ${varMap["IPv6"]+_} ] && [ ${FREE_DNS_IPV6["$dnsProv"]+_} ]; then
      FREE_DNS_PROVIDERS+=("$dnsProv")
      FREE_DNS_IPS["$dnsProv"]="${FREE_DNS_IPV6["$dnsProv"]}"
    elif [ ${varMap["IPv4"]+_} ] && [ ${FREE_DNS_IPV4["$dnsProv"]+_} ]; then
      FREE_DNS_PROVIDERS+=("$dnsProv")
      FREE_DNS_IPS["$dnsProv"]="${FREE_DNS_IPV4["$dnsProv"]}"
    fi
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                     setFileSystemTypes
# DESCRIPTION: Generate an fstab file based on the selection chose
#  Required Params:
#      1)  keyName - name of the key for global associative array variable "$varMap"
#      2) stepTask - the current task of the step
#---------------------------------------------------------------------------------------
genFstabFile() {
  local keyName="$1"
  local stepTask="$2"
  local cmd=""

  if [ ! ${varMap["$keyName"]+_} ]; then
    local fstabChoices=("#1" "#2" "#3")
    local -A fstabDescs=(["#1"]="Device Names" ["#2"]="File system UUIDs" ["#3"]="File system labels")
    local dialogTile="Fstab Configuration"
    local dialogText="Configure fstab based on:"
    local helpText=$(getHelpTextForFstab)

    selectGenFstab "$DIALOG_BACK_TITLE" "$dialogTile" "$dialogText" "$helpText"
    if [ ${#mbSelVal} -gt 0 ]; then
      varMap["$keyName"]="$mbSelVal"
    fi
    cleanDialogFiles
  else
    mbSelVal="${varMap["$keyName"]}"
  fi

  if [ ${#mbSelVal} -gt 0 ]; then
    case "$mbSelVal" in
      "Device Names")
        cmd=$(echo "genfstab -p ${ROOT_MOUNTPOINT} >> ${ROOT_MOUNTPOINT}/etc/fstab")
      ;;
      "File system UUIDs")
        cmd=$(echo "genfstab -U -p ${ROOT_MOUNTPOINT} >> ${ROOT_MOUNTPOINT}/etc/fstab")
      ;;
      "File system labels")
        cmd=$(echo "genfstab -L -p ${ROOT_MOUNTPOINT} >> ${ROOT_MOUNTPOINT}/etc/fstab")
      ;;
    esac

    LINUX_CMD_KEYS+=("$stepTask")
    LINUX_CMD_TITLES["$stepTask"]=$(echo "Configuring 'fstab' based on '$mbSelVal'....")
    LINUX_CMDS["$stepTask"]="$cmd"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                      configHwSysClocks
# DESCRIPTION: Configure the Time Standard & Zone for the Hardware & System Clocks.
#  Required Params:
#      1)  keyName - name of the key for global associative array variable "$varMap"
#      2) stepTask - the current task of the step
#---------------------------------------------------------------------------------------
configHwSysClocks() {
  local keyName="$1"
  local stepTask="$2"

  if [ ${varMap["$TS_KEY_NAME"]+_} ] && [ ${varMap["$keyName"]+_} ]; then
    setLinuxCommandsForClocks "$stepTask" "${varMap["$TS_KEY_NAME"]}" "${varMap["$keyName"]}"
  else
    configTimeStandard "$TS_KEY_NAME"
    if [ ${varMap["$TS_KEY_NAME"]+_} ]; then
      setTimeZoneDataForDialogs
      readarray -t ZONE_ARRAY < "$ZONE_ARRAY_FILE"
      source -- "$SUB_ZONE_ARRAY_FILE"
      setTimeZone "$keyName"

      if [ ${varMap["$keyName"]+_} ]; then
        setLinuxCommandsForClocks "$stepTask" "${varMap["$TS_KEY_NAME"]}" "${varMap["$keyName"]}"
      fi
    fi
  fi
  cleanDialogFiles
}

#---------------------------------------------------------------------------------------
#      METHOD:                  setLinuxCommandsForClocks
# DESCRIPTION: Set the linux commands for the Time Standard, Time Zone,
#              & Time Synchronization in order to configure the Hardware & System Clocks.
#  Required Params:
#      1)     stepTask - the current task of the step
#      2) timeStandard - local or UTC timescale
#      3)     timeZone - the Zone & Sub-Zone of a Time Zone
#---------------------------------------------------------------------------------------
setLinuxCommandsForClocks() {
  local stepTask="$1"
  local timeStandard="$2"
  local timeZone="$3"
  local pcmnCmd=$(printf "changeRoot \"%s\"" "pacman -S --noconfirm ntp")
  local cmdArray=("$pcmnCmd" "changeRoot \"timedatectl set-ntp true\""
    "changeRoot \"timedatectl set-local-rtc %d\"" "changeRoot \"timedatectl set-timezone %s\"")
  local cmdKey="${stepTask}.1"

  LINUX_CMD_KEYS+=("$cmdKey")
  LINUX_CMD_TITLES["$cmdKey"]=$(echo "Installing the AUR package 'NTP'....")
  LINUX_CMDS["$cmdKey"]="${cmdArray[0]}"

  cmdKey="${stepTask}.2"
  LINUX_CMD_KEYS+=("$cmdKey")
  LINUX_CMD_TITLES["$cmdKey"]=$(echo "Configuring Time Synchronization with the Network Time Protocol (NTP)....")
  LINUX_CMDS["$cmdKey"]="${cmdArray[1]}"

  cmdKey="${stepTask}.3"
  LINUX_CMD_KEYS+=("$cmdKey")
  if [ "$timeStandard" == "localtime" ]; then
    LINUX_CMD_TITLES["$cmdKey"]=$(echo "Setting Time Standard to local timescale....")
    LINUX_CMDS["$cmdKey"]=$(printf "${cmdArray[2]}" 1)
  else
    LINUX_CMD_TITLES["$cmdKey"]=$(echo "Setting Time Standard to UTC timescale....")
    LINUX_CMDS["$cmdKey"]=$(printf "${cmdArray[2]}" 0)
  fi

  cmdKey="${stepTask}.4"
  LINUX_CMD_KEYS+=("$cmdKey")
  LINUX_CMD_TITLES["$cmdKey"]=$(echo "Setting Time Zone to '$timeZone'....")
  LINUX_CMDS["$cmdKey"]=$(printf "${cmdArray[3]}" "$timeZone")
}

#---------------------------------------------------------------------------------------
#      METHOD:                       configureLocalization
# DESCRIPTION: Configure the locale that will be used
#  Required Params:
#      1)  keyName - name of the key for global associative array variable "$varMap"
#      2) stepTask - the current task of the step
#---------------------------------------------------------------------------------------
configureLocalization() {
  local keyName="$1"
  local stepTask="$2"
  local dialogTitle="Localization Configuration"
  local defCtryCode=$($JSON_PROCESSOR -r '.countryCode' ${DATA_DIR}/geoLoc.json)
  local output=$(locale 2> /dev/null)
  local lines=()

  if [ ${#defCtryCode} -lt 1 ] || [ ! ${KAAGI_LOCALES["$defCtryCode"]+_} ]; then
    defCtryCode="US"
  fi

  readarray -t lines <<< "$output"
  output="${lines[0]}"
  readarray -t lines <<< "${output//=/$'\n'}"

  getLocale "$DIALOG_BACK_TITLE" "$dialogTitle" "${lines[1]}" "$defCtryCode"
  
  if [ ${#mbSelVal} -gt 0 ]; then
    local cmdKey="${stepTask}.1"
    LINUX_CMD_KEYS+=("$cmdKey")
    LINUX_CMD_TITLES["$cmdKey"]=$(echo "Setting the locale in /etc/locale.gen...")
    LINUX_CMDS["$cmdKey"]=$(printf "changeRoot \"sed -i 's/#'%s'/'%s'/' /etc/locale.gen\"" "$mbSelVal" "$mbSelVal")

    cmdKey="${stepTask}.2"
    LINUX_CMD_KEYS+=("$cmdKey")
    LINUX_CMD_TITLES["$cmdKey"]=$(echo "Generating the locale...")
    LINUX_CMDS["$cmdKey"]=$(printf "changeRoot \"%s\"" "locale-gen")

    cmdKey="${stepTask}.3"
    LINUX_CMD_KEYS+=("$cmdKey")
    LINUX_CMD_TITLES["$cmdKey"]=$(echo "Setting the system locale...")
    LINUX_CMDS["$cmdKey"]=$(printf "changeRoot \"localectl set-locale LANG=%s\"" "$mbSelVal")

    varMap["$keyName"]="$mbSelVal"
  fi
  cleanDialogFiles
}

#---------------------------------------------------------------------------------------
#      METHOD:                        configureNetwork
# DESCRIPTION: Configure the network connection and hostname
#  Required Params:
#      1)  keyName - name of the key for global associative array variable "$varMap"
#      2) stepTask - the current task of the step
#---------------------------------------------------------------------------------------
configureNetwork() {
  local keyName="$1"
  local stepTask="$2"
  local dialogTitle="Network Configuration:  Hostname"
  local dialogText="Enter a unique host name for the system:"
  local initValue="aalp-gygi"
  local helpText=$(getHelpTextForHostName)
  local aurPckgName=""
  local networkManagers=("${!AUR_PCKG_DESCS[@]}")
  networkManagers=($(printf "%s\n" "${networkManagers[@]}" | sort -u))

  showDlgForHostName "$DIALOG_BACK_TITLE" "$dialogTitle" "$dialogText" "$initValue" "$helpText"
  cleanDialogFiles

  if [ ${#ibEnteredText} -gt 0 ]; then
    varMap["$keyName"]="$ibEnteredText"
    helpText=$(getHelpTextForNetMans)
    dialogText="Choose the solution to manage the network connection settings:"
    dialogTitle="Network Connection Manager"
    showDlgForNetMans "$DIALOG_BACK_TITLE" "$dialogTitle" "$dialogText" "$helpText"
    aurPckgName="$mbSelVal"
    cleanDialogFiles

    setProvidersForDNS
    cleanDialogFiles

    setLinuxCommandsForNetworkConfig "$stepTask" "$aurPckgName"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                    selectProvidersForDNS
# DESCRIPTION: Set the DNS providers to be the preferred and/or alternate DNS servers
#---------------------------------------------------------------------------------------
setProvidersForDNS() {
  local altFLAG=$(echo 'false' && return 1)
  local ipVer="IPv6"
  if [ ! ${varMap["$ipVer"]+_} ]; then
    ipVer="IPv4"
  fi
  local helpText=$(getHelpTextForFormDNS "$ipVer")
  local dialogText="Enter the $ipVer addresses of the ISP's DNS servers"
  local dialogTitle="Domain Name System (DNS)"
  local dnsServers=()
  local dnsServerTypes=("Preferred" "Alternate")

  showDialogForISPsDNS "$DIALOG_BACK_TITLE" "$dialogTitle" "$dialogText" "$helpText"
  cleanDialogFiles

  if [ ${#dnsServers[@]} -gt 0 ]; then
    mbSelVal="ISP"
    appendLinesForDnsmasq "$ipVer" "$dnsType"
    altFLAG=$(echo 'true' && return 0)
    dnsServerTypes=("Alternate")
  fi

  dialogText="Select the $ipVer addresses of a DNS provider:"

  for dnsType in "${dnsServerTypes[@]}"; do
    dialogTitle="$dnsType Free DNS providers"
    helpText=$(getHelpTextForDnsmasq "$ipVer" ${altFLAG})
    showDialogForDnsmasq "$DIALOG_BACK_TITLE" "$dialogTitle" "$dialogText" "$helpText"
    appendLinesForDnsmasq "$ipVer" "$dnsType"
    if ! ${altFLAG}; then
      removeSelectedProvider
      altFLAG=$(echo 'true' && return 0)
    fi
    cleanDialogFiles
  done

  setDnsmasqConfFile
}

#---------------------------------------------------------------------------------------
#      METHOD:                    appendLinesForDnsmasq
# DESCRIPTION: Append the preferred and alternate DNS servers to the array variable
#              "dnsServers" that will be appended to the configuration file for dnsmasq
#  Required Params:
#      1)   ipVer - IPv4 or IPv6
#      2) dnsType - Preferred or Alternate.
#---------------------------------------------------------------------------------------
appendLinesForDnsmasq() {
  local ipAddrs=()

  if [ ${FREE_DNS_IPS["$mbSelVal"]+_} ]; then
    dnsServers+=("# $2 DNS servers ($mbSelVal - $1)")
    readarray -t ipAddrs <<< "${FREE_DNS_IPS["$mbSelVal"]//$FORM_FLD_SEP/$'\n'}"
  else
    ipAddrs=("${dnsServers[@]}")
    dnsServers=("# $2 DNS servers (ISP - $1)")
  fi

  for ip in "${ipAddrs[@]}"; do
    dnsServers+=("server=$ip")
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                    removeSelectedProvider
# DESCRIPTION: Removes the selected preferred provider from the arrays
#---------------------------------------------------------------------------------------
removeSelectedProvider() {
  local maxIdx=$(expr ${#FREE_DNS_PROVIDERS[@]} - 1)
  local pos=$(findPositionForString FREE_DNS_PROVIDERS "$mbSelVal")
  local aryIdx=$(expr ${pos} - 1)

  if [ ${pos} -gt 1 ]; then
    if [ ${aryIdx} -lt ${maxIdx} ]; then
      FREE_DNS_PROVIDERS=("${FREE_DNS_PROVIDERS[@]:0:${aryIdx}}" "${FREE_DNS_PROVIDERS[@]:${pos}}")
    else
      FREE_DNS_PROVIDERS=("${FREE_DNS_PROVIDERS[@]:0:${aryIdx}}")
    fi
  else
    FREE_DNS_PROVIDERS=("${FREE_DNS_PROVIDERS[@]:${pos}}")
  fi

  unset FREE_DNS_IPS["$mbSelVal"]
}

#---------------------------------------------------------------------------------------
#      METHOD:                     setDnsmasqConfFile
# DESCRIPTION: Sets the dynamic values within the "${DATA_DIR}/dnsmasq.conf" file
#---------------------------------------------------------------------------------------
setDnsmasqConfFile() {
  local srcFile="${DATA_DIR}/dnsmasq.conf"
  local destFile="${ROOT_MOUNTPOINT}/tmp/dnsmasq.conf"
  local exampleDomain="example.com"
  local hostDomain="${ibEnteredText}.com"
  local concatStr=$(printf "\n%s" "${dnsServers[@]}")

  cp -rf "$srcFile" "$destFile"
  echo "${concatStr:1}" >> "$destFile"

  sed -i -e 's/'"$exampleDomain"'/'"$hostDomain"'/g' "$destFile"
}

#---------------------------------------------------------------------------------------
#      METHOD:              setLinuxCommandsForNetworkConfig
# DESCRIPTION: Set the linux commands for the installing network tools,
#              DNS server (dnsmasq). network manager, and wireless
#  Required Params:
#      1)    stepTask - the current task of the step
#      2) aurPckgName - AUR name of package for the network manager
#---------------------------------------------------------------------------------------
setLinuxCommandsForNetworkConfig() {
  local stepTask="$1"
  local aurPckgName="$2"
  local cmdArray=()
  local cmdKey="${stepTask}.1"
  local concatStr=""

  LINUX_CMD_KEYS+=("$cmdKey")
  LINUX_CMD_TITLES["$cmdKey"]=$(echo "Configuring the hostname....")
  LINUX_CMDS["$cmdKey"]=$(printf "echo \"%s\" > ${ROOT_MOUNTPOINT}/etc/hostname" "$ibEnteredText")

  cmdKey="${stepTask}.2"
  LINUX_CMD_KEYS+=("$cmdKey")
  LINUX_CMD_TITLES["$cmdKey"]=$(echo "Configuring the /etc/hosts file....")
  LINUX_CMDS["$cmdKey"]="configureHostFile"

  cmdKey="${stepTask}.3"
  LINUX_CMD_KEYS+=("$cmdKey")
  local aurPckgs=("iproute2" "dnsmasq")
  if [ ${AUR_PCKGS_TO_INSTALL["$aurPckgName"]+_} ]; then
    aurPckgs+=("$aurPckgName")
    LINUX_CMD_TITLES["$cmdKey"]="Installing AUR packages for networking tools, DNS server, & network manager...."
  elif [ "$aurPckgName" == "systemd-networkd" ]; then
    LINUX_CMD_TITLES["$cmdKey"]="Installing AUR packages for networking tools & DNS server...."
    aurPckgs+=("bridge-utils" "iproute2")
  fi
  concatStr=$(printf " %s" "${aurPckgs[@]}")
  LINUX_CMDS["$cmdKey"]=$(printf "changeRoot \"%s %s\"" "pacman -S --noconfirm" "${concatStr:1}")

  cmdKey="${stepTask}.4"
  LINUX_CMD_KEYS+=("$cmdKey")
  LINUX_CMD_TITLES["$cmdKey"]=$(echo "Configuring dnsmasq....")
  LINUX_CMDS["$cmdKey"]="configureDnsmasq"

  cmdKey="${stepTask}.5"
  case "$aurPckgName" in
    "networkmanager")
      LINUX_CMD_KEYS+=("$cmdKey")
      LINUX_CMD_TITLES["$cmdKey"]=$(echo "Enabling ${AUR_PCKGS_TO_INSTALL["$aurPckgName"]} service....")
      LINUX_CMDS["$cmdKey"]="configureNetworkManager"
      cmdKey="${stepTask}.6"
    ;;
    "connman"|"wicd")
      LINUX_CMD_KEYS+=("$cmdKey")
      LINUX_CMD_TITLES["$cmdKey"]=$(echo "Enabling ${AUR_PCKGS_TO_INSTALL["$aurPckgName"]} service....")
      concatStr=$(echo "changeRoot \"systemctl enable ${aurPckgName}.service\"")
      LINUX_CMDS["$cmdKey"]="$concatStr"
      cmdKey="${stepTask}.6"
    ;;
  esac

  addCmdsForWireless "$cmdKey"
}

#---------------------------------------------------------------------------------------
#      METHOD:                      configureHostFile
# DESCRIPTION: Configure the /etc/hosts file by copying it from the ${DATA_DIR}/hosts.cfg
#              file.   This should be already configured to use with dnsmasq.
#---------------------------------------------------------------------------------------
configureHostFile() {
  local srcFile="${DATA_DIR}/hosts.cfg"
  local destFile="${ROOT_MOUNTPOINT}/etc/hosts"
  cp -rf "$srcFile" "$destFile"
}

#---------------------------------------------------------------------------------------
#      METHOD:                      configureDnsmasq
# DESCRIPTION: Execute the linux commands to configure dnsmasq and enable the service
#---------------------------------------------------------------------------------------
configureDnsmasq(){
  local srcFile="${DATA_DIR}/resolv.dnsmasq.conf"
  local destFile="${ROOT_MOUNTPOINT}/etc/resolv.conf"
  local origFile="${ROOT_MOUNTPOINT}/etc/resolv.conf.original"

  mv "$destFile" "$origFile"
  cp -rf "$srcFile" "$destFile"

  srcFile="${ROOT_MOUNTPOINT}/tmp/dnsmasq.conf"
  destFile="${ROOT_MOUNTPOINT}/etc/dnsmasq.conf"
  origFile="${ROOT_MOUNTPOINT}/etc/dnsmasq.conf.original"

  mv "$destFile" "$origFile"
  cp -rf "$srcFile" "$destFile"

  changeRoot "systemctl enable dnsmasq.service"
}

#---------------------------------------------------------------------------------------
#      METHOD:                   configureNetworkManager
# DESCRIPTION: Execute the linux commands to tell NetworkManager not to modify
#              the /etc/resolv.conf file and enable the service
#---------------------------------------------------------------------------------------
configureNetworkManager() {
  local lines=("[main]" "dns=none")
  local concatStr=$(printf "\n%s" "${lines[@]}")
  local srcFile="${ROOT_MOUNTPOINT}/etc/NetworkManager/NetworkManager.conf"
  echo "${concatStr:1}" >> "$srcFile"
  changeRoot "systemctl enable NetworkManager.service"
}

#---------------------------------------------------------------------------------------
#      METHOD:                     addCmdsForWireless
# DESCRIPTION: Add the linux commands to install the AUR wireless packages
#  Required Params:
#      1) cmdKey - key to the progress bar title and commands to be executed within the
#                  global array variables for the linux commands
#---------------------------------------------------------------------------------------
addCmdsForWireless() {
  local cmdKey="$1"
  local wirelessConnFlag=`ip link | grep wlp | awk '{print $2}'| sed 's/://' | sed '1!d'`
  local aurPckgs=("iw" "wireless_tools" "wpa_actiond" "wpa_supplicant" "dialog")
  local concatStr=$(printf " %s" "${aurPckgs[@]}")

  if [[ -n $wirelessConnFlag ]]; then
    LINUX_CMD_KEYS+=("$cmdKey")
    LINUX_CMD_TITLES["$cmdKey"]=$(echo "Installing AUR packages for wireless....")
    LINUX_CMDS["$cmdKey"]=$(printf "changeRoot \"%s %s\"" "pacman -S --noconfirm" "${concatStr:1}")
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     isValidHostName
# DESCRIPTION: Validates that the hostname entered starts with a letter)
#      RETURN: 0 - true, 1 - false
#---------------------------------------------------------------------------------------
function isValidHostName() {

  if [[ ! "$ibEnteredText" =~ ^- ]] &&  [[ ! "$ibEnteredText" =~ -$ ]] \
      && [[ "$ibEnteredText" =~ ^[0-9a-zA-Z\-]+$ ]] && [ ${#ibEnteredText} -lt 64 ]; then
    echo 'true' && return 0
  fi

  echo 'false' && return 1
}

#---------------------------------------------------------------------------------------
#      METHOD:                setLinuxCommandsForMkinitcpio
# DESCRIPTION: Set the linux commands for the initializing the RAM disk.
#  Required Params:
#      1)  keyName - name of the key for global associative array variable "$varMap"
#      2) stepTask - the current task of the step
#---------------------------------------------------------------------------------------
setLinuxCommandsForMkinitcpio() {
  local keyName="$1"
  local stepTask="$2"
  local cmdKey="${stepTask}.1"

  LINUX_CMD_KEYS+=("$cmdKey")
  LINUX_CMD_TITLES["$cmdKey"]=$(echo "Initializing RAM Filesystem....")
  LINUX_CMDS["$cmdKey"]="configureRAMDisk"

  varMap["$keyName"]=""
}

#---------------------------------------------------------------------------------------
#      METHOD:                      configureRAMDisk
# DESCRIPTION: Execute the linux commands to configure "mkinitcpio" and initialize
#              the RAM disk.
#---------------------------------------------------------------------------------------
configureRAMDisk() {
  local confFile="${ROOT_MOUNTPOINT}/etc/mkinitcpio.conf"
  local lineNum=$(awk '/^HOOKS/{print NR; exit}' "$confFile")
  local text="## AALP-GYGI:  No changes"

  if ${LUKS_FLAG}; then
    sed -i 's/HOOKS=.*/HOOKS=(base udev autodetect keyboard keymap consolefont modconf block encrypt lvm2 filesystems fsck)/' "$confFile"
    text="## AALP-GYGI:  LVM-on-LUKS"
  elif ${LVM_FLAG}; then
    sed -i 's/HOOKS=.*/HOOKS="base udev autodetect keyboard keymap consolefont modconf block lvm2 filesystems fsck"/' "$confFile"
    text="## AALP-GYGI:  LVM"
  fi

  lineNum=$(expr ${lineNum} - 1)
  sed -i "${lineNum} a $text" "$confFile"

  createRAMDiskEnv
}

#---------------------------------------------------------------------------------------
#      METHOD:                      configureRootPswd
# DESCRIPTION: Configure the root password
#  Required Params:
#      1)  keyName - name of the key for global associative array variable "$varMap"
#      2) stepTask - the current task of the step
#---------------------------------------------------------------------------------------
configureRootPswd() {
  local keyName="$1"
  local stepTask="$2"
  local urlRef="https://wiki.archlinux.org/index.php/Users_and_groups#User_database"
  local dialogTitle="Password for ROOT user"
  local dialogText="Enter the root password:"
  local pswdVals=()

  getPasswordsFromFormDialog "$DIALOG_BACK_TITLE" "$dialogTitle" "$urlRef" "$dialogText"
  cleanDialogFiles

  if [ ${#pswdVals[@]} -gt 0 ]; then
    local concatStr=$(printf "\n%s" "${pswdVals[@]}")
    local cmdKey="${stepTask}.1"
    local cmd=$(printf "changeRoot \"%s < %s >/dev/null\"" "passwd root" "${ROOT_PSWD}")
    echo -e "${concatStr:1}" > "${ROOT_MOUNTPOINT}/$ROOT_PSWD"

    LINUX_CMD_KEYS+=("$cmdKey")
    LINUX_CMD_TITLES["$cmdKey"]=$(echo "Setting root password....")
    LINUX_CMDS["$cmdKey"]="$cmd"

    varMap["$keyName"]="true"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                      installBootLoader
# DESCRIPTION: Set the linux commands to install the boot loader
#  Required Params:
#      1)  keyName - name of the key for global associative array variable "$varMap"
#      2) stepTask - the current task of the step
#---------------------------------------------------------------------------------------
installBootLoader() {
  local keyName="$1"
  local stepTask="$2"
  local cmdKey="${stepTask}.1"
  local pcmnCmd=""

  selectBootLoader

  LINUX_CMD_KEYS+=("$cmdKey")

  case "${varMap["BOOT-LOADER"]}" in
    "Grub2")
      if ${UEFI_FLAG}; then
        pcmnCmd=$(printf "changeRoot \"%s\"" "pacman -S --noconfirm grub efibootmgr os-prober")
      else
        pcmnCmd=$(printf "changeRoot \"%s\"" "pacman -S --noconfirm grub os-prober")
      fi
      LINUX_CMD_TITLES["$cmdKey"]=$(echo "Installing packages for '${varMap["BOOT-LOADER"]}' bootloader....")
      LINUX_CMDS["$cmdKey"]="$pcmnCmd"
      configureGrub2 "$2"
    ;;
    "rEFInd")
      pcmnCmd=$(printf "changeRoot \"%s\"" "pacman -S --noconfirm refind-efi efibootmgr os-prober")
      LINUX_CMD_TITLES["$cmdKey"]=$(echo "Installing packages for '${varMap["BOOT-LOADER"]}' bootloader....")
      LINUX_CMDS["$cmdKey"]="$pcmnCmd"
      configureREFInd "$2"
    ;;
    "Syslinux")
      pcmnCmd=$(printf "changeRoot \"%s\"" "pacman -S --noconfirm syslinux efibootmgr gptfdisk")
      LINUX_CMD_TITLES["$cmdKey"]=$(echo "Installing packages for '${varMap["BOOT-LOADER"]}' bootloader....")
      LINUX_CMDS["$cmdKey"]="$pcmnCmd"
      configureSyslinux "$2"
    ;;
    "systemd-boot")
      LINUX_CMD_TITLES["$cmdKey"]=$(echo "Configuring '${varMap["BOOT-LOADER"]}' bootloader....")
      LINUX_CMDS["$cmdKey"]="configureSystemd"
    ;;
  esac

  varMap["$keyName"]="${varMap["BOOT-LOADER"]}"
  unset varMap["BOOT-LOADER"]
}

#---------------------------------------------------------------------------------------
#      METHOD:                      selectBootLoader
# DESCRIPTION: Select the boot loader that will be used to boot up the system
#---------------------------------------------------------------------------------------
selectBootLoader() {
  local ptt=$(echo "${varMap[PART-TBL-TYPE]}")
  local -A methodParams=(["dialogBackTitle"]="$DIALOG_BACK_TITLE")

  methodParams["partLayout"]="${varMap[PART-LAYOUT]}"
  methodParams["pttDesc"]=$(echo "${PART_TBL_TYPES[$ptt]}")
  methodParams["noCancelFlag"]="true"
  methodParams["skipConf"]="true"

  setBootLoaderOptions methodParams
  selectLinuxCapableBootLoader methodParams
  varMap["BOOT-LOADER"]=$(echo "$mbSelVal")
}

#---------------------------------------------------------------------------------------
#      METHOD:                    setBootLoaderOptions
# DESCRIPTION: Set the options for the dialogs to select the boot loader
#  Required Params:
#      1) assocParamAry - associative array of key/value pairs of parameters
#---------------------------------------------------------------------------------------
setBootLoaderOptions() {
  local -n assocParamAry="$1"
  local bootLoaders=("Grub2" "Syslinux")
  local blChoices=()

  assocParamAry["dialogTitle"]="Linux-capable Boot Loaders"
  if [ ${varMap["BOOT-LOADER"]+_} ]; then
    blChoices=("Grub2")
    assocParamAry["Grub2"]="GRand Unified Bootloader"
  else
    local blFST=$(getBootLoaderFST)
    local blFSTS=()
    local sep="|"
    if ${UEFI_FLAG}; then
      bootLoaders=("Grub2" "rEFInd" "Syslinux" "systemd-boot")
    fi

    for bl in "${bootLoaders[@]}"; do
      readarray -t blFSTS <<< "${BOOT_LOADER_FSTS["$bl"]//$sep/$'\n'}"
      local pos=$(findPositionForString blFSTS "$blFST")
      if [ $pos -gt -1 ]; then
        blChoices+=("$bl")
        case "$bl" in
          "Grub2")
            assocParamAry["$bl"]="GRand Unified Bootloader"
          ;;
          "rEFInd")
            assocParamAry["$bl"]="platform-neutral UEFI boot manager"
          ;;
          "Syslinux")
            assocParamAry["$bl"]="collection of boot loaders"
          ;;
          "systemd-boot")
            assocParamAry["$bl"]="A simple UEFI boot manager installed on Arch by default"
          ;;
        esac
      fi
    done
  fi

  assocParamAry["choices"]=$(printf "\t%s" "${blChoices[@]}")
  assocParamAry["choices"]=${assocParamAry["choices"]:1}
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                      getBootLoaderFST
# DESCRIPTION: Get the file system type for either the '/boot' or '/' directories
#      RETURN: string value of the FST
#---------------------------------------------------------------------------------------
function getBootLoaderFST() {
  local tableRows=()
  local cols=()
  local blFST=""
  readarray -t tableRows < "$PARTITION_TABLE_FILE"

  for row in "${tableRows[@]}"; do
    readarray -t cols <<< "${row//$'\t'/$'\n'}"
    if [ "${cols[5]}" == "/boot" ]; then
      blFST="${cols[4]}"
      break
    elif [[ "${cols[-1]}" =~ "ROOT" ]]; then
      blFST="${cols[4]}"
    fi
  done

  echo "$blFST"
}

#---------------------------------------------------------------------------------------
#      METHOD:                       configureGrub2
# DESCRIPTION: Execute the linux commands to configure the "Grub2" boot loader
#  Required Params:
#      1) stepTask - the current task of the step
#---------------------------------------------------------------------------------------
configureGrub2() {
  local subTask=1
  local pbTitle=$(echo "Installing '${varMap["BOOT-LOADER"]}' bootloader....")
  local cmd=""
  
  if ${UEFI_FLAG}; then
    cmd=$(printf "changeRoot \"%s=%s\"" "grub-install --target=x86_64-efi --efi-directory" "$EFI_MNT_PT")
  else
    cmd=$(printf "changeRoot \"%s\"" "grub-install --target=i386-pc /dev/sda")
  fi

  appendToLinuxCmdArrays "$1" "$pbTitle" "$cmd"

  if ${LUKS_FLAG}; then
    local configFile="${ROOT_MOUNTPOINT}/etc/default/grub"
    local lvmRoot=$(lsblk -l | grep -e "/mnt$" | awk '{print $1}')
    local cryptDevice=$(printf "cryptdevice=UUID=%s:%s root=%s" "${LVM_DEVICE_UUID}" "${LUKS_CONTAINER_NAME}" "/dev/mapper/${lvmRoot}")
    pbTitle=$(echo "Adding LUKS encryption for '${varMap["BOOT-LOADER"]}'....")
    cmd=$(printf "sed -i \"%s=\"%s\"/g\" \"%s\"" "s/GRUB_CMDLINE_LINUX=.*/GRUB_CMDLINE_LINUX" "$cryptDevice" "$configFile")
    appendToLinuxCmdArrays "$1" "$pbTitle" "$cmd"
  fi

  pbTitle=$(echo "Generate config files for '${varMap["BOOT-LOADER"]}'....")
  cmd=$(printf "changeRoot \"%s\"" "grub-mkconfig -o /boot/grub/grub.cfg")
  appendToLinuxCmdArrays "$1" "$pbTitle" "$cmd"

  if ${UEFI_FLAG}; then
    pbTitle=$(echo "Generate EFI config files for '${varMap["BOOT-LOADER"]}'....")
    cmd=$(printf "changeRoot \"%s %s/%s\"" "grub-mkconfig -o" "$EFI_MNT_PT" "EFI/arch/grub.cfg")
    appendToLinuxCmdArrays "$1" "$pbTitle" "$cmd"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                       configureREFInd
# DESCRIPTION: Execute the linux commands to configure the "rEFInd" boot loader
#  Required Params:
#      1) stepTask - the current task of the step
#---------------------------------------------------------------------------------------
configureREFInd() {
  local subTask=1
  local pbTitle=$(echo "Installing '${varMap["BOOT-LOADER"]}' bootloader....")
  local cmd=$(printf "changeRoot \"%s\"" "refind-install")

  appendToLinuxCmdArrays "$1" "$pbTitle" "$cmd"
  
  pbTitle=$(echo "Configuring '${varMap["BOOT-LOADER"]}' bootloader....")
  cmd=$(printf "changeRoot \"%s\"" "mkrlconf")
  appendToLinuxCmdArrays "$1" "$pbTitle" "$cmd"
}

#---------------------------------------------------------------------------------------
#      METHOD:                      configureSyslinux
# DESCRIPTION: Execute the linux commands to configure the "Syslinux" boot loader
#  Required Params:
#      1) stepTask - the current task of the step
#---------------------------------------------------------------------------------------
configureSyslinux() {
  local subTask=1
  local configFile=""
  local cmd=""
  local pbTitle=$(echo "Installing '${varMap["BOOT-LOADER"]}' bootloader....")

  if ${UEFI_FLAG}; then
    local efiDir=$(echo "${ROOT_MOUNTPOINT}${EFI_MNT_PT}/EFI/syslinux")
    cmd=$(printf "mkdir -p %s" "$efiDir")
    appendToLinuxCmdArrays "$1" "$pbTitle" "$cmd"

    cmd=$(printf "cp -r /usr/lib/syslinux/efi64/* %s" "$efiDir")
    appendToLinuxCmdArrays "$1" "$pbTitle" "$cmd"

    local strArray=("efibootmgr --create --disk /dev/sda --part 1"
		    "--loader /EFI/syslinux/syslinux.efi"
		    "--label \\\"Syslinux\\\" --verbose")
    local concatStr=$(printf " %s" "${strArray[@]}")
    pbTitle=$(echo "Setting up boot entry for the '${varMap["BOOT-LOADER"]}' bootloader....")
    cmd=$(printf "changeRoot \"%s\"" "${concatStr:1}")
    appendToLinuxCmdArrays "$1" "$pbTitle" "$cmd"

    configFile="${ROOT_MOUNTPOINT}${EFI_MNT_PT}/EFI/syslinux/syslinux.cfg"
  else
    cmd=$(printf "syslinux-install_update -i -a -m -c %s" "$ROOT_MOUNTPOINT")
    appendToLinuxCmdArrays "$1" "$pbTitle" "$cmd"

    configFile="${ROOT_MOUNTPOINT}/boot/syslinux/syslinux.cfg"
  fi

  setSyslinuxKernel "$1" "$configFile"
  setSyslinuxRootDevice "$1" "$configFile"
}

#---------------------------------------------------------------------------------------
#      METHOD:                     setSyslinuxKernel
# DESCRIPTION: Set the linux commands to configure the Linux kernel for the 
#              "Syslinux" boot loader
#  Required Params:
#      1) stepTask - the current task of the step
#      2) configFile - full path to the "Syslinux" configuration file
#---------------------------------------------------------------------------------------
setSyslinuxKernel() {
  local linuxKernel=$(getNameOfLinuxKernel)
  
  if [ "$linuxKernel" != "linux" ]; then
    local lkFmts=( "s/vmlinuz-linux/vmlinuz-${linuxKernel}/" "s/initramfs-linux.img/initramfs-${linuxKernel}.img/"
		  "s/initramfs-linux-fallback.img/initramfs-${linuxKernel}-fallback.img/")
    local pbTitle=$(echo "Setting Linux kernel for the '${varMap["BOOT-LOADER"]}' bootloader....")
    for fmt in "${lkFmts[@]}"; do
      local cmd=$(printf "sed -i \"%s\" %s" "$fmt" "$2")
      appendToLinuxCmdArrays "$1" "$pbTitle" "$cmd"
    done
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                   setSyslinuxRootDevice
# DESCRIPTION: Set the linux commands to configure the ROOT device for the 
#              "Syslinux" boot loader
#  Required Params:
#      1)   stepTask - the current task of the step
#      2) configFile - full path to the "Syslinux" configuration file
#---------------------------------------------------------------------------------------
setSyslinuxRootDevice() {
  local rootDevice=""
  local pbTitle=$(echo "Setting the ROOT device for the '${varMap["BOOT-LOADER"]}' bootloader....")

  if ${LUKS_FLAG}; then
    rootDevice=$(echo "root=\/dev\/mapper\/${LUKS_CONTAINER_NAME} cryptdevice=\/dev\/${varMap["VG-NAME"]}\/lvm-root:${LUKS_CONTAINER_NAME}")
  else
    rootDevice=$(echo "root=UUID=${ROOT_DEVICE_UUID} rw")
  fi

  local cmd=$(echo "s/APPEND.*/APPEND root=${rootDevice} rw/g")
  cmd=$(printf "sed -i \"%s\" %s" "$cmd" "$2")
  appendToLinuxCmdArrays "$1" "$pbTitle" "$cmd"
}

#---------------------------------------------------------------------------------------
#      METHOD:                  appendToLinuxCmdArrays
# DESCRIPTION: Append to global "LINUX_CMD*" arrays the title & the command
#  Required Params:
#      1) stepTask - the current task of the step
#      2) cmdTitle - the title to be displayed within the dialog for the progress bar
#      3) linuxCmd - the command to be executed
#---------------------------------------------------------------------------------------
appendToLinuxCmdArrays() {
  local stepTask="$1"
  local cmdTitle="$2"
  local linuxCmd="$3"

  subTask=$(expr ${subTask} + 1)

  local cmdKey=$(printf "${stepTask}.%d" ${subTask})
  LINUX_CMD_KEYS+=("$cmdKey")
  LINUX_CMD_TITLES["$cmdKey"]="$cmdTitle"
  LINUX_CMDS["$cmdKey"]="$linuxCmd"
}

#---------------------------------------------------------------------------------------
#      METHOD:                      configureSystemd
# DESCRIPTION: Execute the linux commands to configure the "systemd-boot" boot loader
#---------------------------------------------------------------------------------------
configureSystemd() {
  local linuxKernel=$(getNameOfLinuxKernel)
  local espMount="/boot"
  local configFile="${ROOT_MOUNTPOINT}${espMount}/loader/entries/arch.conf"

  local cmd=$(echo "bootctl --path=${espMount} install")
  changeRoot "$cmd"
  cmd=$(echo "cp /usr/share/systemd/bootctl/loader.conf ${ROOT_MOUNTPOINT}${espMount}/loader/")
  changeRoot "$cmd"
  cmd=$(echo "echo \"timeout 10\" >> ${ROOT_MOUNTPOINT}${espMount}/loader/loader.conf")
  changeRoot "$cmd"

  echo -e "title          Arch Linux\nlinux          /vmlinuz-${linuxKernel}\ninitrd         /initramfs-${linuxKernel}.img" > "$configFile"

  if ${LUKS_FLAG}; then
    rootDevice=$(echo "cryptdevice=\/dev\/${varMap[VG-NAME]}\/lvm-root:${LUKS_CONTAINER_NAME} root=\/dev\/mapper\/${LUKS_CONTAINER_NAME} rw")
  else
    rootDevice=$(echo "root=UUID=${ROOT_DEVICE_UUID} rw")
  fi

  echo "options		$rootDevice" >> "$configFile"
}

#---------------------------------------------------------------------------------------
#  MAIN
#---------------------------------------------------------------------------------------
main() {
  local stepTaskArray=("$@")
  local keyName=""
  initVars

  for stepTask in "${stepTaskArray[@]}"; do
    stepTask=$(trimString "$stepTask")
    keyName="${KEY_NAMES["$stepTask"]}"
    case "$stepTask" in
      "step#3.1")
        genFstabFile "$keyName" "$stepTask"
      ;;
      "step#3.2")
        configHwSysClocks "$keyName" "$stepTask"
      ;;
      "step#3.3")
        configureLocalization "$keyName" "$stepTask"
      ;;
      "step#3.4")
        configureNetwork "$keyName" "$stepTask"
      ;;
      "step#3.5")
        setLinuxCommandsForMkinitcpio "$keyName" "$stepTask"
      ;;
      "step#3.6")
        configureRootPswd "$keyName" "$stepTask"
      ;;
      "step#3.7")
        installBootLoader "$keyName" "$stepTask"
      ;;
      *)
        echo "invalid_option:[$stepTask]"
      ;;
    esac
    if [ ! ${varMap["$keyName"]+_} ]; then
      LINUX_CMD_KEYS=()
      LINUX_CMD_TITLES=()
      LINUX_CMDS=()
      break
    fi
  done

  if [ ${#LINUX_CMD_KEYS[@]} -gt 0 ]; then
    if [ "$DIALOG" == "yad" ]; then
      executeLinuxCommands "$DIALOG_BACK_TITLE" "Configuring the System" LINUX_CMD_KEYS LINUX_CMD_TITLES LINUX_CMDS "--pulsate|"
    else
      executeLinuxCommands "$DIALOG_BACK_TITLE" "Configuing the Systemr" LINUX_CMD_KEYS LINUX_CMD_TITLES LINUX_CMDS
    fi

    local -A updVarMap=()
    for key in "${!varMap[@]}"; do
      updVarMap["$key"]=$(echo "${varMap[$key]}")
    done

    declare -p updVarMap > "${UPD_ASSOC_ARRAY_FILE}"
  fi

  rm -rf "${ROOT_MOUNTPOINT}/$ROOT_PSWD"
}

main "$@"
exit 0
