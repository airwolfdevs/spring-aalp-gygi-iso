#!/bin/bash
#set -e

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# globals
#===============================================================================
MIN_AUTO_SIZE=53687091200
CUR_DIR="${PWD}"
INC_FILE=$(echo "$CUR_DIR/inc/inst_inc.sh")

source "$INC_FILE"
source "$GRAPHICAL_PATH/create-part-inc.sh"
source "$GRAPHICAL_PATH/pswd-funcs-inc.sh"

declare -A varMap
declare BLOCK_DEVICE=""
declare BLOCK_DEVICE_SIZE=""
declare HR_BLOCK_DEVICE_SIZE=""
declare DIALOG_BACK_TITLE=""
declare PHYSICAL_VOLUME=""

source -- "$ASSOC_ARRAY_FILE"

#===============================================================================
# functions
#===============================================================================

#---------------------------------------------------------------------------------------
#      METHOD:                       initVars
# DESCRIPTION: Initialize the global variables.
#---------------------------------------------------------------------------------------
initVars() {
  DIALOG_BACK_TITLE="Create the Partition Layout/Scheme"
  BLOCK_DEVICE=$(echo "${varMap["BLOCK-DEVICE"]}")
  if [ ${varMap["BLOCK_DEVICE_SIZE"]+_} ]; then
    HR_BLOCK_DEVICE_SIZE=$(echo "${varMap["BLOCK_DEVICE_SIZE"]}")
    BLOCK_DEVICE_SIZE=$(humanReadableToBytes "$HR_BLOCK_DEVICE_SIZE")
  else
    BLOCK_DEVICE_SIZE=$(blockdev --getsize64 "$BLOCK_DEVICE")
    HR_BLOCK_DEVICE_SIZE=$(bytesToHumanReadable "$BLOCK_DEVICE_SIZE")
  fi

  if [ ! -f "$PARTITION_TABLE_FILE" ]; then
    touch "$PARTITION_TABLE_FILE"
  fi

  # This suppresses the WARNING "File descriptor 3 (/dev/pts/1) leaked on lvs invocation"
  export LVM_SUPPRESS_FD_WARNINGS=0
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getPartitionProcessType
# DESCRIPTION: Get the option for the type of process to use to partition the disk
#      RETURN: type of partition process in the global "mbSelVal" variable
#---------------------------------------------------------------------------------------
getPartitionProcessType() {
  local dialogTitle="Partition Process Types"
  local helpText=$(getHelpTextForPPT)
  local partProcTypes=("Semi-Automatic Partitioning" "Manual Partitioning")
  getDialogOptionforPPT "$dialogTitle" "$helpText" partProcTypes
}

#---------------------------------------------------------------------------------------
#  partitionBlockDevice
#---------------------------------------------------------------------------------------
partitionBlockDevice() {
  local processType=$(echo "$mbSelVal")

  if [ "$processType" == "${PART_PROC_TYPE_CHOICES[1]}" ]; then
    keepOrZapCurLayout
    if [ "$inputRetVal" != "EXIT" ]; then
      execManualPartitionStep
    fi
  else
    getPartitionLayoutScheme
    if [ ${#mbSelVal} -gt 0 ]; then
      if [ ${varMap["PART-LAYOUT"]+_} ] && [ "$mbSelVal" != "<Keep>" ]; then
        zapPartitionTable
      fi
      case "$mbSelVal" in
        "${PART_LAYOUTS[0]}")
          executeScript "semiAuto"
        ;;
        "${PART_LAYOUTS[1]}")
          executeScript "semiAuto"
          if [ ${varMap["PART-LAYOUT"]+_} ]; then
            unset varMap["BOOT-DEVICE"]
            executeScript "lvm"
          fi
        ;;
        "${PART_LAYOUTS[2]}")
          executeScript "semiAuto"
          if [ ${varMap["PART-LAYOUT"]+_} ]; then
            unset varMap["BOOT-DEVICE"]
            setupLUKS
            executeScript "lvm"
          fi
        ;;
        "<Keep>")
          local menuChoice=$(findPositionForString PART_LAYOUTS "${varMap[PART-LAYOUT]}")
          if [[ "${varMap[PART-LAYOUT]}" =~ "LVM" ]] && [ ${menuChoice} -lt ${#PART_LAYOUTS[@]} ]; then
            modprobe dm-mod
            vgscan &> /dev/null
            vgchange -ay &> /dev/null
          fi
        ;;
      esac
    fi

    local partFlag=$(isPartitioned "$BLOCK_DEVICE")
    if ! ${partFlag}; then
      unset varMap["BLOCK-DEVICE"]
    fi

  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                      executeScript
# DESCRIPTION: Executes the script specified by the parameter
#  Required Params:
#      1) installStep - string that is mapped to the name of the script to execute
#---------------------------------------------------------------------------------------
executeScript() {
  local key="$1"
  local scriptName=""

  case "$key" in
    "lvm")
      scriptName="logical-volume-manager.sh"
    ;;
    "semiAuto")
      scriptName="semi-auto-part.sh "$mbSelVal""
    ;;
    "swap")
      scriptName="create-swap.sh "${PART_PROC_TYPE_CHOICES[1]}" "Partition""
    ;;
  esac

  declare -p varMap > "$ASSOC_ARRAY_FILE"
  eval "./$scriptName"
  source -- "${UPD_ASSOC_ARRAY_FILE}"

  varMap=()
  for key in "${!updVarMap[@]}"; do
    varMap["$key"]=$(echo "${updVarMap[$key]}")
  done

  rm -rf "${UPD_ASSOC_ARRAY_FILE}"
}

#---------------------------------------------------------------------------------------
#      METHOD:                    keepOrZapCurLayout
# DESCRIPTION: If partition layout/scheme already exists, ask to either
#              destroy it or keep it
#---------------------------------------------------------------------------------------
keepOrZapCurLayout() {
  inputRetVal=""
  if [ ${varMap["PART-LAYOUT"]+_} ]; then
    local param=""
    if [ "$DIALOG" == "yad" ]; then
      param=$(getPartitionLayoutForYAD)
    else
      param=$(getPartitionTableText)
    fi
    getKeepOrZapOptForCurLayout "$param"
    if [ "$inputRetVal" == "ZAP" ]; then
      zapPartitionTable
    fi
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                getPartitionTableText
# DESCRIPTION: Displays the partitions to be created for the device
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getPartitionTableText() {
  local partTblType=$(parted -s "$BLOCK_DEVICE" print | grep 'Partition Table' | awk '{print $3}')
  local dialogPartTable=$(getPartitionTableForDialog "$BLOCK_DEVICE" "$HR_BLOCK_DEVICE_SIZE" "${PART_TBL_TYPES[$partTblType]}")
  local textArray=()
  local vgName=""

  if [[ "${varMap[PART-LAYOUT]}" =~ "LVM" ]];then
    if [ ! ${varMap["VG-NAME"]+_} ]; then
      set $(lvs --noheadings -o vg_name)
      varMap["VG-NAME"]=$(echo "$2")
    fi
    textArray=(
    "Logical Volume Group:  [${varMap[VG-NAME]}]"
    "$dialogPartTable")
  else
    textArray=("$dialogPartTable")
  fi

  local partTableText=$(getTextForDialog "${textArray[@]}")

  echo "$partTableText"
}

#---------------------------------------------------------------------------------------
#      METHOD:                   zapPartitionTable
# DESCRIPTION: Zap/destroy the GPT and MBR data structures
#---------------------------------------------------------------------------------------
zapPartitionTable() {
  local pbTitle="Zap/Destroy All Partitions"
  local linuxCmdKeys=("zap" "upd")
  local -A linuxCmdTitles=(["zap"]="Zapping Current Partition Layout/Scheme" ["upd"]="Updating data for Installer")
  local lvNames=""
  local lvscanRows=()
  local lvmNum=1
  local lvmKey=""
  local lvmKeys=()
  local -A linuxCmds=()
  local pvDevice=$(getDeviceNameForPV)

  if [ ${#pvDevice} -gt 0 ]; then
    pbTitle="Zap/Destroy All Logical Volumes & Partitions"
    lvNames=$(getLogicalVolumeNames)
    IFS=$'\t' read -a lvscanRows <<< "$lvNames"
    vgName=$(echo "${lvscanRows[0]}")
    lvscanRows=("${lvscanRows[@]:1}")
    for lv in "${lvscanRows[@]}"; do
      lvmKey=$(printf "lvm#%d" "$lvmNum")
      lvmKeys+=( "$lvmKey" )
      lvmNum=$(expr $lvmNum + 1)
      linuxCmdTitles["$lvmKey"]=$(echo "Removing Logical Volume '$lv'")
      linuxCmds["$lvmKey"]=$(echo "lvremove /dev/$vgName/$lv")
    done
    for num in {1..2}; do
      lvmNum=$(expr $lvmNum + 1)
      lvmKey=$(printf "lvm#%d" "$lvmNum")
      lvmKeys+=( "$lvmKey" )
      if [ ${num} -lt 2 ]; then
        linuxCmdTitles["$lvmKey"]=$(echo "Removing Logical Volume Group '$vgName'")
        linuxCmds["$lvmKey"]=$(echo "vgremove $vgName")
      else
        linuxCmdTitles["$lvmKey"]=$(echo "Removing Physical Volume '$pvDevice'")
        linuxCmds["$lvmKey"]=$(echo "pvremove $pvDevice")
      fi
    done
    linuxCmdKeys=( "${lvmKeys[@]}" "${linuxCmdKeys[@]}")
  fi

  linuxCmds["zap"]=$(printf "sgdisk -Z %s" "$BLOCK_DEVICE")
  linuxCmdTitles["zap"]=$(echo "Destroying the GPT and MBR data structures on '$BLOCK_DEVICE'")
  linuxCmds["upd"]="cleanInstallerData"
  linuxCmdTitles["upd"]="Removing key & value pairs from the Installer's data"

  executeLinuxCommands "$DIALOG_BACK_TITLE" "$pbTitle" linuxCmdKeys linuxCmdTitles linuxCmds
}

#---------------------------------------------------------------------------------------
#      METHOD:                  cleanInstallerData
# DESCRIPTION: Clears key & value pairs from the global associative array "varMap"
#              and removes the file whose name is in the global var  "$PARTITION_TABLE_FILE"
#---------------------------------------------------------------------------------------
cleanInstallerData() {
  unset varMap["PART-LAYOUT"]
  unset varMap["PART-TBL-TYPE"]
  unset varMap["BOOT-LOADER"]
  unset varMap["BOOT-DEVICE"]
  unset varMap["ROOT-PART"]
  unset varMap["SWAP-SIZE"]
  unset varMap["SWAP-TYPE"]
  declare -p varMap > "$ASSOC_ARRAY_FILE"
  rm -rf "$PARTITION_TABLE_FILE"
}

#---------------------------------------------------------------------------------------
#      METHOD:                execManualPartitionStep
# DESCRIPTION: Manually partitions the disk using one of the provided
#              linux partitioning tools.
#---------------------------------------------------------------------------------------
execManualPartitionStep() {
  local partStep=""
  local linuxTool=""
  local debugFlag=""
  while true; do
    partStep=$(getNextMPS "$partStep")
    case "$partStep" in
      "partStep#01") #run tool
        if [ ${#linuxTool} -gt 0 ]; then
          runLinuxPartitionProgram "$BLOCK_DEVICE" "$linuxTool"
        else
          getLinuxPartitionTool
          if [ ${#mbSelVal} -gt 0 ]; then
            linuxTool=$(echo "$mbSelVal")
            runLinuxPartitionProgram "$BLOCK_DEVICE" "$mbSelVal"
          else
            break
          fi
        fi
      ;;
      "partStep#02") #check if partitions exist
        local partFlag=$(isPartitioned "$BLOCK_DEVICE")
        if ! ${partFlag}; then
          showNoPartitionsWarning "$linuxTool"
        else
          inputRetVal=""
        fi
      ;;
      "partStep#03") #check if there is unallocated space
        showUnallocSpaceWarning "$linuxTool"
      ;;
      "partStep#04")
        generateDataFile
        setPartitionLayoutScheme
      ;;
      "partStep#05") #create swap if 'Discrete Partitions' Layout"
        if [[ ! "${varMap[PART-LAYOUT]}" =~ "LVM" ]] && [ ! ${varMap["SWAP-TYPE"]+_} ]; then
          executeScript "swap"
        fi
      ;;
      *)
        break
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     getNextMPS
# DESCRIPTION: Get the next step in the manual partition process
#      RETURN: string containing what the next step should be
#  Required Params:
#      1) lastPartStep - the last Manual Partition Step that was executed
#---------------------------------------------------------------------------------------
function getNextMPS() {
  local lastPartStep="$1"
  local nextStep="partStep#01"

  case "$lastPartStep" in
    "partStep#01") #run tool
      if [ ${#mbSelVal} -gt 0 ]; then
        nextStep="partStep#02"
      fi
    ;;
    "partStep#02") #check if partitions exist
      if [ ${#inputRetVal} -gt 0 ]; then
        if [ "$inputRetVal" == "YES" ]; then
          nextStep="partStep#01"
        else
          nextStep="exit"
        fi
      else
        nextStep="partStep#03"
      fi
    ;;
    "partStep#03") #check if there is unallocated space
      nextStep="partStep#04"
      if [ ${#inputRetVal} -gt 0 ]; then
        if [ "$inputRetVal" == "YES" ]; then
          nextStep="partStep#01"
        fi
      fi
    ;;
    "partStep#04") #create "$PARTITION_TABLE_FILE" and set varMap["PART-LAYOUT"]
      nextStep="partStep#05"
    ;;
    "partStep#05") #create swap if 'Discrete Partitions' Layout
      nextStep="exit"
    ;;
  esac

  echo "$nextStep"
}

#---------------------------------------------------------------------------------------
#      METHOD:                  showNoPartitionsWarning
# DESCRIPTION: Display a warning when no partitions were found.
#      RETURN: In the global variable "inputRetVal":
#              YES) run the linux partition tool again
#               NO) exit the manual partition process
#  Required Params:
#      1) name of the linux partition tool that was previously used
#---------------------------------------------------------------------------------------
showNoPartitionsWarning() {
  local partitionTool="$1"
  local dialogTitle="Warning:    No Partitions found!"
  getChoiceWhenNoParts "$dialogTitle" "$partitionTool"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getChoiceWhenUnallocSpace
# DESCRIPTION: Display a warning when unallocated space is found
#      RETURN: In the global variable "inputRetVal":
#              YES) run the linux partition tool again
#               NO) continue with the manual partition process
#  Required Params:
#      1) name of the linux partition tool that was previously used
#---------------------------------------------------------------------------------------
showUnallocSpaceWarning() {
  local partitionTool="$1"
  local dialogTitle="Warning:    Unallocated Space Found!"
  local unallocSpacePerc=$(parted "$BLOCK_DEVICE" unit '%' print free | grep 'Free Space' | tail -n1 | awk '{print $3}')
  local pos=$(expr ${#unallocSpacePerc} - 1)
  local percAmt=$(echo "${unallocSpacePerc:0:${pos}}")

  percAmt=$(echo "$percAmt/1" | bc)
  if [ ${percAmt} -gt 0 ]; then
    getChoiceWhenUnallocSpace "$dialogTitle" "$partitionTool" "$unallocSpacePerc"
  else
    inputRetVal=""
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                   generateDataFile
# DESCRIPTION: Generate the data file containing the rows of the partition table
#  Required Params:
#---------------------------------------------------------------------------------------
generateDataFile() {
  local dialogBackTitle="Manual Partitioning"
  local filePath=(${PARTITION_TABLE_FILE//\// })
  local methodKey="file"
  local pbKeys=("$methodKey")
  local -A pbTitles=(["$methodKey"]="Generating data file for the configured partition table....")
  local -A methodNames=(["$methodKey"]="writePartitionTableToFile")

  pbKeys+=("done")
  pbTitles["done"]=$(echo "Finished generating data file '${filePath[-2]}/${filePath[-1]}'")

  showProgressBarForDataFile "$dialogBackTitle" "Generating data file"
}

#---------------------------------------------------------------------------------------
#      METHOD:                 setPartitionLayoutScheme
# DESCRIPTION: Set the partition layout scheme based on the partition table created
#              using the manual partitioning process
#---------------------------------------------------------------------------------------
setPartitionLayoutScheme() {
  local rows=()
  local cols=()

  IFS=$'\n' read -d '' -a rows < "$PARTITION_TABLE_FILE"
  for row in "${rows[@]}"; do
    IFS=$'\t' read -a cols <<< "$row"
    case "${cols[3]}" in
      "8200") #swap
        varMap["PART-LAYOUT"]=$(echo "${PART_LAYOUTS[0]}")
        varMap["SWAP-SIZE"]=$(echo "${cols[2]}")
        varMap["SWAP-TYPE"]="partition"
        break
      ;;
      "8E00") #lvm
        getPartitionLayoutScheme "LVM"
        varMap["PART-LAYOUT"]=$(echo "$mbSelVal")
        break
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getPartitionLayout
# DESCRIPTION: Get the type of partition layout/scheme
#      RETURN: partition layout/scheme in the global variable "mbSelVal"
#---------------------------------------------------------------------------------------
getPartitionLayoutScheme() {
  if [ "$#" -gt 0 ]; then
    local lvmChoices=("${PART_LAYOUTS[@]:1}")
    getPartitionLayoutType "$DIALOG_BACK_TITLE" "Partition Layouts/Schemes for LVM" lvmChoices "$1"
  else
    pltDlgChoices=("${PART_LAYOUTS[@]}")
    getPartitionLayoutType "$DIALOG_BACK_TITLE" "Semi-Automatic Partitioning" PART_LAYOUTS
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                   viewURLinElinks
# DESCRIPTION: View the URLs within the "elinks" feature-rich text mode web browser
#---------------------------------------------------------------------------------------
viewURLinElinks() {
  local urls=("Partitioning")
  local -A urlDialogOpts=(["dialogHeight"]=25 ["urlPrefix"]="https://wiki.archlinux.org/index.php")
  local url=""

  if [ "$#" -gt 0 ]; then
    urls+=("Discrete")
  fi

  urls+=("LVM" "dm-crypt" "LVM on LUKS")
  for urlKey in "${urls[@]}"; do
    case "$urlKey" in
      "Discrete")
        url="- /partitioning#Discrete_partitions"
      ;;
      "dm-crypt")
        url="- /Dm-crypt"
      ;;
      "LVM")
        url="- /LVM"
      ;;
      "LVM on LUKS")
        url="- /Dm-crypt/Encrypting_an_entire_system#LVM_on_LUKS"
      ;;
      "Partitioning")
        url="- /partitioning"
      ;;
    esac

    urlDialogOpts["$urlKey"]="$url"
  done

  displayURLinElinks "$DIALOG_BACK_TITLE" urls urlDialogOpts
}

#---------------------------------------------------------------------------------------
#      METHOD:                      setupLUKS
# DESCRIPTION: Setup the 'LVM Partition' to be encrypted with LUKS
#---------------------------------------------------------------------------------------
setupLUKS() {
  setPswdForLUKS
  if [ ${varMap["LVM-on-LUKS"]+_} ]; then
    addEncryptionToLVMPart
    confirmToEncrypt
    if [ "$inputRetVal" == "NO" ]; then
      removeLUKSEncryption
      unset varMap["LVM-on-LUKS"]
      varMap["PART-LAYOUT"]=$(echo "${PART_LAYOUTS[1]}")
    fi
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                    setPswdForLUKS
# DESCRIPTION: Set the password to encrypt & open the LUKS encrypted container
#---------------------------------------------------------------------------------------
setPswdForLUKS() {
  local dialogBackTitle="${PART_LAYOUTS[-1]}"
  local urlRef="https://wiki.archlinux.org/index.php/Security#Passwords"
  local pswdOpt=""

  while true; do
    if [ ${#pswdOpt} -lt 1 ]; then
      getPasswordOptionForLUKS "$dialogBackTitle" "$urlRef"
      if [ ${#mbSelVal} -gt 0 ]; then
        pswdOpt=$(echo "$mbSelVal")
      else
        varMap["PART-LAYOUT"]=$(echo "${PART_LAYOUTS[1]}")
        unset varMap["LVM-on-LUKS"]
        break
      fi
    else
      case "$pswdOpt" in
        "kb")
          setPasswordFromKeyboard "$dialogBackTitle" "$urlRef"
        ;;
        *)
          setPasswordFromAutoGen "$dialogBackTitle" "$urlRef" "$pswdOpt"
        ;;
      esac
      if [ ${varMap["LVM-on-LUKS"]+_} ]; then
        break
      else
        pswdOpt=""
      fi
    fi
  done
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getPasswordOptionForLUKS
# DESCRIPTION: Get the option of how to create a password for the LUKS encrypted container
#      RETURN: option to create a password in the global variable "mbSelVal"
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)          urlRef - the URL that can be referenced for further documentation
#                           from the help dialog
#---------------------------------------------------------------------------------------
getPasswordOptionForLUKS() {
  local dialogBackTitle="$1"
  local urlRef="$2"
  local dialogTitle="Password Create Options for the LUKS encrypted container"
  local pswdOpts=("kb" "apg" "pwgen")
  local -A pswdOptDescs=(["kb"]="Enter/Input password from keyboard" ["apg"]="Automated Password Generator"
  ["pwgen"]="Password generator for creating secure passwords")

  getSelPswdOptForLUKS "$dialogBackTitle" "$dialogTitle" "$urlRef" pswdOpts pswdOptDescs
}

#---------------------------------------------------------------------------------------
#      METHOD:              setPasswordFromKeyboard
# DESCRIPTION: Set the password for the LUKS encrypted container using the keyboard
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)          urlRef - the URL that can be referenced for further documentation
#                           from the help dialog
#---------------------------------------------------------------------------------------
setPasswordFromKeyboard() {
  local dialogBackTitle="$1"
  local urlRef="$2"
  local dialogTitle="Password for LUKS encrypted container"
  local pswdVals=()

  getPasswordsFromFormDialog "$dialogBackTitle" "$dialogTitle" "$urlRef"
  if [ ${#pswdVals[@]} -gt 0 ]; then
    varMap["LVM-on-LUKS"]="${pswdVals[0]}"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:              setPasswordFromKeyboard
# DESCRIPTION: Set the password for the LUKS encrypted container using one of the
#              automated password generation tools
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)          urlRef - the URL that can be referenced for further documentation
#                           from the help dialog
#      3)         aurPckg - name of the AUR password generator package
#---------------------------------------------------------------------------------------
setPasswordFromAutoGen() {
  local dialogBackTitle="$1"
  local urlRef="$2"
  local aurPckg="$3"
  local cmdOutput=""

  getSelAutoGenPswd "$dialogBackTitle" "$urlRef" "$aurPckg"

  if [ ${#mbSelVal} -gt 0 ]; then
    varMap["LVM-on-LUKS"]=$(echo "$mbSelVal")
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                 addEncryptionToLVMPart
# DESCRIPTION: Append a line/row of device type/guid "crypt" that will represent
#              the LVM encrypted container to the "LVM Partition" within the
#              partition table
#---------------------------------------------------------------------------------------
addEncryptionToLVMPart() {
  local row=""
  local rows=()
  local cols=()
  local luksPrefix=$(trimString "$LAST_LVM_PREFIX")

  IFS=$'\n' read -d '' -a rows < "$PARTITION_TABLE_FILE"
  for row in "${rows[@]}"; do
    IFS=$'\t' read -a cols <<< "$row"
    if [ "${cols[3]}" == "8E00" ]; then
      cols[0]=$(printf "%s%s" "$luksPrefix" "$LUKS_CONTAINER_NAME")
      cols[1]="LUKS container"
      cols[3]="crypt"
      cols[-1]="luks"
      break
    fi
  done

  row=$(printf "\t%s" "${cols[@]}")
  row=${row:1}

  appendLogicalVolumeToFile " " "$row"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     confirmToEncrypt
# DESCRIPTION: Confirm to continue with LUKS encryption
#      RETURN: answer to question in the global variable "inputRetVal"
#---------------------------------------------------------------------------------------
confirmToEncrypt() {
  local dialogBackTitle="${PART_LAYOUTS[-1]}"
  local dialogTitle="Confirmation of LUKS encryption"
  local helpText=$(getHelpTextForConfirmation)

  getConfirmationToEncrypt "$dialogBackTitle" "$dialogTitle" "$helpText"
}

#---------------------------------------------------------------------------------------
#      METHOD:                  removeLUKSEncryption
# DESCRIPTION: Remove the line/row from the partition table that has a
#              device type/guid "crypt"
#---------------------------------------------------------------------------------------
removeLUKSEncryption() {
  local fileRows=()
  local rows=()
  local cols=()

  IFS=$'\n' read -d '' -a rows < "$PARTITION_TABLE_FILE"
  for row in "${rows[@]}"; do
    IFS=$'\t' read -a cols <<< "$row"
    if [ "${cols[3]}" != "crypt" ]; then
      fileRows+=("$row")
    fi
  done

  row=$(printf "\n%s" "${fileRows[@]}")
  row=${row:1}
  echo -e "$row" > "$PARTITION_TABLE_FILE"
}

#---------------------------------------------------------------------------------------
#  MAIN
#---------------------------------------------------------------------------------------
main() {
  initVars

  if [ $BLOCK_DEVICE_SIZE -lt $MIN_AUTO_SIZE ]; then
    mbSelVal="${PART_PROC_TYPE_CHOICES[1]}"
    partitionBlockDevice
  else
    while true; do
      local manualFlag=$(echo 'false' && return 1)

      getPartitionProcessType

      if [ "${#mbSelVal}" -gt 0 ]; then
        if [ "$mbSelVal" == "${PART_PROC_TYPE_CHOICES[1]}" ]; then
          manualFlag=$(echo 'true' && return 0)
        fi

        partitionBlockDevice

        if ${manualFlag}; then
          local partFlag=$(isPartitioned "$BLOCK_DEVICE")
          partFlag=$(echo 'false' && return 1)
          if ${partFlag}; then
            break
          fi
        else
          break
        fi
      else
        rm -rf $PARTITION_TABLE_FILE
        break
      fi
    done
  fi

  local -A updVarMap=()
  for key in "${!varMap[@]}"; do
    updVarMap["$key"]=$(echo "${varMap[$key]}")
  done

  declare -p updVarMap > "${UPD_ASSOC_ARRAY_FILE}"
}

main "$@"
exit 0
