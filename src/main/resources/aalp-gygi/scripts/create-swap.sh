#!/bin/bash

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# globals
#===============================================================================
MIN_AUTO_SIZE=53687091200
CUR_DIR="${PWD}"
INC_FILE=$(echo "$CUR_DIR/inc/inst_inc.sh")
MAN_DEV_INC_FILE=$(echo "$CUR_DIR/inc/manage-device-funcs.sh")

source "$INC_FILE"
source "$MAN_DEV_INC_FILE"
source "$GRAPHICAL_PATH/lvm-part-inc.sh"

declare -A varMap
declare PART_TBL_TYPE=""
declare BLOCK_DEVICE=""
declare DEVICE_TYPE=""
declare PHYSICAL_VOLUME=""
declare PHYSICAL_VOL_SIZE=""
declare DIALOG_BACK_TITLE="Swap Space"
declare LV_NAME="lvm-swap"
declare PART_NAME="swap partition"

source -- "$ASSOC_ARRAY_FILE"
declare SWAP_OPTIONS=()

#===============================================================================
# functions
#===============================================================================

#---------------------------------------------------------------------------------------
#      METHOD:                       initVars
# DESCRIPTION: Initialize the global variables.
#---------------------------------------------------------------------------------------
initVars() {
  DEVICE_TYPE="$1"
  if [ "$DEVICE_TYPE" == "Logical Volume" ]; then
    setPhysicalVolume
  fi

  BLOCK_DEVICE=$(echo "${varMap["BLOCK-DEVICE"]}")

  if [ ! ${varMap["PART-TBL-TYPE"]+_} ]; then
    PART_TBL_TYPE=$(parted -s "$BLOCK_DEVICE" print | grep 'Partition Table' | awk '{print $3}')
    varMap["PART-TBL-TYPE"]=$(echo "$PART_TBL_TYPE")
  else
    PART_TBL_TYPE=$(echo "${varMap[PART-TBL-TYPE]}")
  fi

  local partText="partition"
  if [[ "${varMap[PART-LAYOUT]}" =~ "LVM" ]]; then
    partText="LVM partition"
  fi
  SWAP_OPTIONS=("$partText" "file" "systemd-swap" "zramswap")

  if [ ! -f "$DIALOG_IMG_ICON_FILE" ]; then
    echo "${PART_DIALOG_IMAGE},${PART_WINDOW_ICON}" > "$DIALOG_IMG_ICON_FILE"
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  checkIfSwapExists
# DESCRIPTION: If swap already exists, then return its type
#      RETURN: 1) blank
#              2) SWAP_OPTIONS[0] = partition or lvm
#              3) SWAP_OPTIONS[1] = file
#---------------------------------------------------------------------------------------
function checkIfSwapExists() {
  local cmdOutput=$(lsblk "$BLOCK_DEVICE" | egrep 'part|lvm' | awk '{print substr ($1,3)":"$7}')
  local blocklist=()
  local arr=()
  local swapType=""
  local tableRows=()
  local row=""

  for opt in "${SWAP_OPTIONS[@]}"; do
    case "$opt" in
      "${SWAP_OPTIONS[1]}")
        if [ -f "${ROOT_MOUNTPOINT}/swapfile" ]; then
          swapType=$(echo "$opt")
          break
        fi
      ;;
      "${SWAP_OPTIONS[2]}"|"${SWAP_OPTIONS[3]}")
        local existFlag=$(isPackageInstalled "$opt")
        if ${existFlag}; then
          swapType=$(echo "$opt")
          break
        fi
      ;;
      *)
        local partitionTable=$(gdisk "$BLOCK_DEVICE" -l | awk '$1+0' | awk '{print $1":"$4" "$5":"$6}')
        local cols=()
        IFS=$'\n' read -d '' -r -a tableRows <<< $partitionTable
        for row in "${tableRows[@]}"; do
          IFS=':' read -a cols <<< "$row"
          if [ "${cols[-1]}" == "8200" ]; then
            swapType=$(echo "$opt")
            break
          fi
        done
        if [ ${#swapType} -gt 0 ]; then
          break
        fi
      ;;
    esac
  done

  echo "$swapType"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                       getSkipSwapMsg
# DESCRIPTION: Display message that the installer cannot continue creating the
#              swap
#      RETURN: concatenated string
#  Required Params:
#      1) swapType - file, partition, or logical volume
#---------------------------------------------------------------------------------------
function getSkipSwapMsg() {
  local swapType="$1"
  local msg=""
  case "$swapType" in
    "${SWAP_OPTIONS[1]}")
      msg=$(echo "an already existing file with the path \"${ROOT_MOUNTPOINT}/swapfile\"!")
    ;;
    "${SWAP_OPTIONS[2]}"|"${SWAP_OPTIONS[3]}")
      msg=$(echo "the AUR package \"$swapType\" is already installed!!")
    ;;
    *)
      local partitionTable=$(gdisk "$BLOCK_DEVICE" -l | awk '$1+0' | awk '{print $1":"$4" "$5":"$6":"$7" "$8}')
      local cols=()
      local deviceName=""
      IFS=$'\n' read -d '' -r -a tableRows <<< $partitionTable
      for row in "${tableRows[@]}"; do
        IFS=':' read -a cols <<< "$row"
        if [ "${cols[2]}" == "8200" ]; then
          deviceName=$(echo "${cols[-1]}")
          break
        fi
      done
      if [[ "$swapType" =~ "LVM" ]]; then
        msg=$(echo "a Logical Volume with the name \"$deviceName\"already exists!")
      else
        msg=$(echo "a Logical Volume with the name \"$deviceName\"already exists!")
      fi
    ;;
  esac
  local msgTextArray=("The installer cannot continue to create the \"Swap\" because it found" "$msg")
  local msgText=$(printf " %s" "${msgTextArray[@]}")

  msgTextArray=("$msgText" " " " " "The installer will \"Skip\" this process!!!!!")
  msgText=$(getTextForDialog "${msgTextArray[@]}")

  echo "$msgText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getSwapType
# DESCRIPTION: Get the type of swap that will be eventually created by the installer
#      RETURN: selected swap type in the global variable "mbSelVal"
#---------------------------------------------------------------------------------------
getSwapType() {
  local -A methodParams=(["dialogBackTitle"]="Swap Type" ["dialogTitle"]="Swap Type Menu Options" ["deviceType"]="$DEVICE_TYPE")

  setDataForPartitionScheme methodParams

  methodParams["choices"]=$(printf "\t%s" "${SWAP_OPTIONS[@]}")

  showSelectSwapTypeDialog methodParams
}

#---------------------------------------------------------------------------------------
#      METHOD:                    setSwapSize
# DESCRIPTION: Set the size in human-readable format for the amount to be allocated
#              by the installer
#---------------------------------------------------------------------------------------
setSwapSize() {
  local swapSizes=()
  local -A methodParams=(["dialogBackTitle"]="Swap Type" ["dialogTitle"]="Swap Type Menu Options" ["deviceType"]="$DEVICE_TYPE")
  methodParams["deviceName"]=$(getDeviceNameForDialog)

  setDataForPartitionScheme methodParams

  inputRetVal="NO"
  if [ ${varMap["SWAP-SIZE"]+_} ]; then
    methodParams["dialogTitle"]="Keep Current Size?"
    showDlgToKeepSwapSize methodParams
  fi

  if [ "$inputRetVal" == "NO" ]; then
    methodParams["dialogTitle"]="Size of Swap Space Allocation Choices"
    methodParams["deviceSizes"]=$(getCalcSwapSizes)
    getSizeForSwapDevice methodParams
    if [ ${#mbSelVal} -gt 0 ]; then
      varMap["SWAP-SIZE"]=$(echo "$mbSelVal")
      if [ "${varMap["SWAP-TYPE"]}" == "file" ]; then
        setSizeOfROOT
        local flag=$(isFileSizeGreaterThanRoot)
        if ${flag}; then
          performSFSEO methodParams
        fi
      fi
    else
      unset varMap["SWAP-TYPE"]
    fi
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getDeviceNameForDialog
# DESCRIPTION: Get the name of the swap device/type to be displayed on the input
#              dialog (i.e. user hits "Other" on the select size screen)
#      RETURN: string with the name to be displayed
#---------------------------------------------------------------------------------------
function getDeviceNameForDialog() {
  local deviceName=""

  case ${varMap["SWAP-TYPE"]} in
    "file")
      deviceName="'File'"
    ;;
    "systemd-swap")
      deviceName="'systemd-swap' package"
    ;;
    "zramswap")
      deviceName="AUR 'zramswap' package"
    ;;
    *)
      if [[ "${varMap["SWAP-TYPE"]}" =~ "LVM" ]]; then
        deviceName="$LV_NAME"
      else
        deviceName="$PART_NAME"
      fi

      if [ "$DIALOG" != "yad" ]; then
        deviceName=$(echo "'$DEVICE_TYPE' with device name of '$deviceName'")
      fi
    ;;
  esac

  echo "$deviceName"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     getCalcSwapSizes
# DESCRIPTION: Calculates the recommended swap sizes for with & without hibernation
#              The array will be set accordingly:
#                  [0] - amount of RAM installed
#                  [1] - without hibernation:  square root of [0]
#                  [2] - with hibernation:  [0] + [1]
#                  [3] - max recommended:  [0] * 2 (i.e. double of [0])
#---------------------------------------------------------------------------------------
function getCalcSwapSizes() {
  local hrSwapSizes=()
  local mem=`grep MemTotal /proc/meminfo | awk '{print $2}' | sed 's/\..*//'`
  local ram=$(echo "$mem * 1024" | bc)
  local ramCol=$(bytesToHumanReadable $ram)
  IFS=' ' read -a arr <<< "$ramCol"
  local unit=$(echo "${arr[1]}" | tr -d iB)
  local ramSQRT=$(echo "sqrt(${arr[0]})" | bc)

  hrSwapSizes+=($(echo "${arr[0]}$unit"))
  hrSwapSizes+=($(echo "$ramSQRT$unit"))
  local val=$(echo "$ramSQRT + ${arr[0]}" | bc)
  hrSwapSizes+=($(echo "$val$unit"))
  val=$(echo "${arr[0]} * 2" | bc)
  hrSwapSizes+=($(echo "$val$unit"))

  local aryIdx=0
  for size in "${hrSwapSizes[@]}"; do
    val=$(humanReadableToBytes "$size")
    size=$(bytesToHumanReadable ${val})
    hrSwapSizes[$aryIdx]="$size"
    aryIdx=$(expr $aryIdx + 1)
  done

  local swapSizes=$(printf "\t%s" "${hrSwapSizes[@]}")
  swapSizes=${swapSizes:1}
  echo -e "$swapSizes"
}

#---------------------------------------------------------------------------------------
#      METHOD:                     setSizeOfROOT
# DESCRIPTION: Set the size in bytes of the root logical volume or partition
#---------------------------------------------------------------------------------------
setSizeOfROOT() {
  local -A assocAry=()
  local sizeInBytes=0
  local existFlag=$(echo 'false' && return 1)
  local pattern="$LOGICAL_VOL_PREFIX"
  local keys=()
  local partTblRows=()
  local cols=()
  local flagsCol=""
  local rowKey=""

  eval $(typeset -A -p methodParams|sed 's/ methodParams=/ assocAry=/')

  if [ ${assocAry["dialogPartTable"]+_} ]; then
    assocAry["dialogPartTable"]="${methodParams[dialogPartTable]}"
    assocAry["partTableStats"]="${methodParams[partTableStats]}"
  else
    assocAry["partitionLayout"]="${methodParams[partitionLayout]}"
  fi
  if [ "${assocAry[deviceType]}" == "Partition" ]; then
    pattern="/dev/sda"
  fi
  if [ "${varMap[PART-TBL-TYPE]}" == "gpt" ]; then
    if [ ! -f "$PARTITION_TABLE_FILE" ]; then
      writePartitionTableToFile
    fi

    IFS=$'\n' read -d '' -a partTblRows < "$PARTITION_TABLE_FILE"
    local lastIdx=$(expr ${#rows[@]} - 1)
    for row in "${partTblRows[@]}"; do
      rowNum=$(expr $rowNum + 1)
      if [[ "$row" =~ "$pattern" ]]; then
        IFS=$'\t' read -a cols <<< "$row"

        flagsCol=$(trimString "${cols[-1]}")
        if [ ${#flagsCol} -gt 0 ]; then
          existFlag=$(isRootDevice "$flagsCol")
          if ${existFlag}; then
            sizeInBytes=$(humanReadableToBytes "${cols[2]}")
            break
          fi
        else
          rowKey=$(printf "row#%02d" "$rowNum")
          keys+=("$rowKey")
          assocAry["$rowKey"]=$(getDeviceNameForListDialog cols)
        fi
      fi
    done

    if [ $sizeInBytes -lt 1 ]; then
      assocAry["choices"]=$(printf "\t%s" "${keys[@]}")
      assocAry["choices"]=${assocAry["choices"]:1}
      assocAry["dialogTitle"]="Select ROOT device"

      selectRootDevice assocAry
      idx=$(expr $(echo "$mbSelVal" | cut -d"#" -f2) - 1)
      updateRowInPartTable $idx partTblRows -1 "ROOT"

      IFS=$'\t' read -a cols <<< "${partTblRows[$arrayIdx]}"

      sizeInBytes=$(humanReadableToBytes "${cols[2]}")
      varMap["ROOT-DEVICE-SIZE"]=${sizeInBytes}
    fi
  else
    local hrSize=$(getRemainingHRFSize "$BLOCK_DEVICE_SIZE")
    sizeInBytes=$(humanReadableToBytes "$hrSize")
    varMap["ROOT-DEVICE-SIZE"]=${sizeInBytes}
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 isFileSizeGreaterThanRoot
# DESCRIPTION: Check to see if the size of the file will exceed the amount of
#              space in the root partition
#      RETURN: 0 - true, 1 - false
#---------------------------------------------------------------------------------------
function isFileSizeGreaterThanRoot() {
  local rootDeviceSize=${varMap["ROOT-DEVICE-SIZE"]}
  local fileSize=$(humanReadableToBytes "${varMap[SWAP-SIZE]}")
  local minSize=$(humanReadableToBytes "${ROOT_HR_SIZES[0]}")
  local spaceRemain=$(expr $rootDeviceSize - $minSize)

  if [ $fileSize -gt $spaceRemain ]; then
    echo 'true' && return 0
  fi

  echo 'false' && return 1
}

#---------------------------------------------------------------------------------------
#      METHOD:                     performSFSEO
# DESCRIPTION: Perform the operation  selected when the Swap File Size
#              exceeds the amount of buffer space in the root partition or
#              logical volume. (see the function selectSwapFileSizeExceedOpts)
#---------------------------------------------------------------------------------------
performSFSEO() {
  local -n assocArray=$1
  local fields=()

  setSwapFileSizeExceedOpts assocArray

  while true; do
    selectSwapFileSizeExceedOpt assocArray

    case "$mbSelVal" in
      "create")
        varMap["SWAP-TYPE"]=$(echo "${SWAP_OPTIONS[0]}")
        break
      ;;
      "reconf")
        getLinuxPartitionTool
        if [ ${#mbSelVal} -gt 0 ]; then
          runLinuxPartitionProgram "$BLOCK_DEVICE" "$mbSelVal"
          writePartitionTableToFile
          break
        fi
      ;;
      "resize")
        resizeRootDevice
        echo "resizeRootDevice"
        break
      ;;
      "Skip")
        varMap["SWAP-TYPE"]="Skip"
        break
      ;;
      *) # systemd-swap OR zramswap
        varMap["SWAP-TYPE"]=$(echo "$mbSelVal")
        break
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                 setSwapFileSizeExceedOpts
# DESCRIPTION: Set the options available when the Swap File Size exceeds the amount of
#              buffer space in the '/' or root logical volume or partition.
#  Required Params:
#      1) params - associative array of key/value pairs of parameters for the method
#                  and the choices & descriptions will be appended to.
#              "deviceType" - Logical Volume or Partition
# Options Available:
#              If the "partition/LVM partition" is in "SWAP_OPTIONS":
#                 A) resize the root partition or logical volume to +Swap File Size
#                 B) create a swap partition instead of a file
#                 C) choose the "systemd-swap" option
#                 D) choose the "zramswap" option
#              Otherwise:
#                 A) Reconfigure the partition table manually
#                 B) choose the "systemd-swap" option
#                 C) choose the "zramswap" option
#---------------------------------------------------------------------------------------
setSwapFileSizeExceedOpts() {
  local -n params=$1
  local deviceType="${params[deviceType]}"
  local fileSize=$(humanReadableToBytes "${varMap[SWAP-SIZE]}")
  local rootDeviceSize=$(expr ${varMap["ROOT-DEVICE-SIZE"]} + $fileSize)
  local hrSize=$(bytesToHumanReadable $rootDeviceSize)
  local sfseOpts=("create" "resize" "systemd-swap" "zramswap")
  local -A descs=(["resize"]=$(echo "Resize the ROOT '$deviceType' to '$hrSize'")
    ["create"]=$(echo "Create a Swap '$deviceType' instead of a 'File'")
    ["reconf"]="Reconfigure the partition table manually"
    ["systemd-swap"]="Install and use this package"
    ["zramswap"]="Install and use this AUR package"
    )

  if [ ${#SWAP_OPTIONS[@]} -lt 4 ]; then
    sfseOpts=("reconf" "${sfseOpts[@]:2}")
  fi

  for opt in "${sfseOpts[@]}"; do
    params["$opt"]="${descs[$opt]}"
  done

  params["choices"]=$(printf "\t%s" "${sfseOpts[@]}")
  params["choices"]="${params[choices]:1}"
  params["dialogTitle"]="Swap File Size Error"
}

#---------------------------------------------------------------------------------------
#      METHOD:                    resizeRootDevice
# DESCRIPTION: Increase the size of the root partition or logical device by
#              the size of the swap file
#---------------------------------------------------------------------------------------
resizeRootDevice() {
  local fields=()
  local rows=()
  local dialogOptions=()
  local -A dialogOptDecs=()
  local sizeInBytes=0
  local -A idxMap=()
  local key=""
  local row=""
  local optNum=1
  local idx=0
  local fileSize=$(humanReadableToBytes "${varMap[SWAP-SIZE]}")
  local existFlag=$(echo 'false' && return 1)

  IFS=$'\n' read -d '' -a rows < "$PARTITION_TABLE_FILE"
  local lastIdx=$(expr ${#rows[@]} - 1)

  for idx in $(eval echo "{0..$lastIdx}"); do
    row=$(echo -e "${rows[$idx]}")
    IFS=$'\t' read -a fields <<< "$row"

    existFlag=$(isRootDevice "${fields[-1]}")
    if ${existFlag}; then
      sizeInBytes=$(humanReadableToBytes "${fields[2]}")
      break
    fi
  done

  sizeInBytes=$(expr $sizeInBytes + $fileSize)
  local rootDeviceSize=$(bytesToHumanReadable $sizeInBytes)

  updateRowInPartTable $idx rows 2 "$rootDeviceSize"
  varMap["ROOT-DEVICE-SIZE"]=${sizeInBytes}
}

#---------------------------------------------------------------------------------------
#      METHOD:                    createSwapFile
# DESCRIPTION: Create the Swap File by executing the appropriate linux commands
#              and display the progress with a linux "dialog" gauge box (i.e. progress bar)
#  Required Params:
#      1) backTitle - String to be displayed on the backdrop, at the top
#                     of the screen.
#---------------------------------------------------------------------------------------
createSwapFile() {
  local backTitle="$1"
  local pbTitle="Creating Swap File"
  local swapSizeInBytes=$(humanReadableToBytes "${varMap[SWAP-SIZE]}")
  local swapFileCmdKeys=("fallocate" "chmod" "mkswap" "swapon")
  local -A swapFileCmdTitles=()
  local -A swapFileCmds=()
  local linuxCmd=""

  for key in "${swapFileCmdKeys[@]}"
  do
    case "$key" in
      "fallocate")
        linuxCmd=$(echo "fallocate -l ${swapSizeInBytes} ${ROOT_MOUNTPOINT}/swapfile")
      ;;
      "chmod")
        linuxCmd=$(echo "chmod 644 ${ROOT_MOUNTPOINT}/swapfile")
      ;;
      "mkswap")
        linuxCmd=$(echo "mkswap ${ROOT_MOUNTPOINT}/swapfile")
      ;;
      "swapon")
        linuxCmd=$(echo "swapon ${ROOT_MOUNTPOINT}/swapfile")
      ;;
    esac
    swapFileCmds[$key]=$(echo "$linuxCmd")
    swapFileCmdTitles[$key]=$(echo "Executing '$linuxCmd'")
  done

  executeLinuxCommands "$backTitle" "$pbTitle" swapFileCmdKeys swapFileCmdTitles swapFileCmds
}


#---------------------------------------------------------------------------------------
#      METHOD:                    addSwapDevice
# DESCRIPTION: Add the swap partition or logical volume before the ROOT to the
#              global file name "$PARTITION_TABLE_FILE"
#---------------------------------------------------------------------------------------
addSwapDevice() {
  local rows=()
  local cols=()
  local rowNum=1
  local tableRows=()
  local swapCols=()
  local partitionTable=""
  local lvmGroup=""
  local existFlag=$(echo 'false' && return 1)

  IFS=$'\n' read -d '' -r -a rows < "$PARTITION_TABLE_FILE"
  for row in "${rows[@]}"; do
    IFS=$'\t' read -a cols <<< "$row"

    existFlag=$(isRootDevice "${cols[-1]}")
    if ${existFlag}; then
      swapCols=( "${cols[@]:0}" )
      swapCols[2]=$(echo "${varMap[SWAP-SIZE]}")
      swapCols[3]="8200"
      swapCols[-1]="linux-swap(v1)"
      if [[ "${varMap[PART-LAYOUT]}" =~ "LVM" ]]; then
        swapCols[0]="${LVM_DEVICE_PREFIX}${varMap[VG-NAME]}"
        swapCols[1]="$LV_NAME"
      else
        swapCols[0]=$(printf "/dev/sda%d" "$rowNum")
        swapCols[1]="$PART_NAME"
        rowNum=$(expr $rowNum + 1)
        cols[0]=$(printf "/dev/sda%d" "$rowNum")
      fi

      row=$(printf "\t%s" "${swapCols[@]}")
      row=${row:1}
      tableRows+=("$row")

      row=$(printf "\t%s" "${cols[@]}")
      row=${row:1}
      tableRows+=("$row")
    else
      if [[ "${cols[0]}" =~ "dev" ]]; then
        cols[0]=$(printf "/dev/sda%d" "$rowNum")
        row=$(printf "\t%s" "${cols[@]}")
        row=${row:1}
      fi
      tableRows+=("$row")
    fi
    rowNum=$(expr $rowNum + 1)
  done

  partitionTable=$(printf "\n%s" "${tableRows[@]}")
  partitionTable=${partitionTable:1}
  echo -e "$partitionTable" > "$PARTITION_TABLE_FILE"
}

#---------------------------------------------------------------------------------------
#  MAIN
#---------------------------------------------------------------------------------------
main() {
  initVars "$2"
  local processType=$1
  local swapType=""
  local debugFlag=$(echo 'false' && return 1)

  if [[ ! -z "${AALP_GYGI_YAD}" ]] && [ "$AALP_GYGI_YAD" == "debug" ]; then
    debugFlag=$(echo 'true' && return 0)
  elif [[ ! -z "${AALP_GYGI_DIALOG}" ]] && [ "$AALP_GYGI_DIALOG" == "debug" ]; then
    debugFlag=$(echo 'true' && return 0)
  fi

  if ! ${debugFlag}; then
    swapType=$(checkIfSwapExists)
  fi

  if [ ${#swapType} -gt 0 ]; then
    if [ ! ${varMap["SWAP-TYPE"]+_} ]; then
      local msg=$(getSkipSwapMsg "$swapType")
      dispSkipSwapMsg "Can't Create the Swap" "$DIALOG_BACK_TITLE" "$msg"
      unset varMap["SWAP-SIZE"]
      varMap["SWAP-TYPE"]="Skip"
    fi
  else
    if [ "$processType" == "${PART_PROC_TYPE_CHOICES[1]}" ]; then  # if Manual Partitioning
      SWAP_OPTIONS=( "${SWAP_OPTIONS[@]:1}" )
    fi
    while true; do
      if [ ! ${varMap["SWAP-TYPE"]+_} ]; then
        getSwapType
        varMap["SWAP-TYPE"]="$mbSelVal"
      fi

      if [ "${varMap[SWAP-TYPE]}" == "Skip" ]; then
        unset varMap["SWAP-SIZE"]
        break
      else
        setSwapSize
        if [ ${varMap["ROOT-DEVICE-SIZE"]+_} ]; then
          unset varMap["ROOT-DEVICE-SIZE"]
        elif [ ${varMap["SWAP-SIZE"]+_} ]; then
          break
        fi
      fi
    done

    if [ ${varMap["SWAP-SIZE"]+_} ]; then
      addSwapDevice
    fi
  fi

  local -A updVarMap=()
  for key in "${!varMap[@]}"; do
    updVarMap["$key"]=$(echo "${varMap[$key]}")
  done

  declare -p updVarMap > "${UPD_ASSOC_ARRAY_FILE}"
}

main "$@"
exit 0
