#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'

PS1='[\u - \W]\$ '
if [[ $(tty) == "/dev/tty1" ]]; then
  unset KAALP_GYGI_YAD
else
  export KAALP_GYGI_YAD=0
fi

cd /root/KAALP-GYGInstaller/scripts/
./installation-guide.sh "step#5"
