#!/bin/sh
#Some gtk app not wayland ready.
export GDK_BACKEND=x11

#setup input method.
. /etc/X11/xinit/xinputrc
[ -n "$GTK_IM_MODULE" ] && export GTK_IM_MODULE
[ -n "$QT_IM_MODULE" ] && export QT_IM_MODULE
[ -z "$XMODIFIERS" -a -n "$XIM" ] && XMODIFIERS="@im=$XIM"
[ -n "$XMODIFIERS" ] && export XMODIFIERS
#After sway startup, 'exec' in sway config.
[ -n "$XIM_PROGRAM" ] && export XIM_PROGRAM
[ -n "$XIM_ARGS" ] && export XIM_ARGS

#setup DBUS_SESSION_BUS_ADDRESS env.
if [ -n "$DBUS_SESSION_BUS_ADDRESS" ]; then
export DBUS_SESSION_BUS_ADDRESS
eval sway 2>~/.swaysession-errors
else
eval dbus-launch --exit-with-session sway 2>~/.swaysession-errors
fi