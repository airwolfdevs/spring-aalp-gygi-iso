#!/bin/bash
#set -e

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# globals
#===============================================================================
CUR_DIR="${PWD}"
INC_FILE=$(echo "$CUR_DIR/inc/inst_inc.sh")

source "$INC_FILE"
source "$GRAPHICAL_PATH/fmt-mnt-inc.sh"

declare -A varMap
declare EXIT_STATUS=0
declare FMT_DIALOG_IMAGE="format-logo.png"
declare FMT_WINDOW_ICON="format-icon.png"
declare MNT_DIALOG_IMAGE="mount-logo.png"
declare MNT_WINDOW_ICON="mount-icon.png"
declare PART_TABLE_ARRAY=()
declare BLOCK_DEVICE=""
declare DIALOG_BACK_TITLE=""
declare UEFI_FLAG=0
declare PHYSICAL_VOLUME=""
declare PHYSICAL_VOL_SIZE=""
declare -A FMT_HELP_TEXT_OPTS=()
declare FST_SEL_TXT="Choose a supported FST"
declare btrfsHelpTxt=(
"   * 'btrfs' - a modern copy on write (CoW) file system. Its aim is to"
"               implement advanced features while also focusing on fault"
"               tolerance, repair and easy administration."
"               (see warning below if this option is NOT available)")
declare ext3HelpTxt=(
"   * 'ext3' - the Third Extended (\"ext3\") file system that uses"
"             journaling.  It is simple, robust, and extensible.")
declare ext4HelpTxt=(
"   * 'ext4' - the evolution of \"ext3\" that not only uses jounaling, but"
"              also modifies important data structures of the filesystem"
"              such as the ones destined to store the file data.")
declare exfatHelpTxt=(
"   * 'exfat' - (Extended File Allocation Table) is a Microsoft file system"
"               that uses journaling.  It is optimized for flash memory such"
"               as USB flash drives and SD cards.")
declare f2fsHelpTxt=(
"   * 'f2fs' - (Flash-Friendly File System) is a file system that uses"
"              jounaling.  It isintended for NAND-based flash memory"
"              equipped with Flash Transition Layer.  It is supported from"
"              kernel 3.8 onwards.")
declare jfsHelpTxt=(
"   * 'jfs' - The Journaled File System (JFS).  It is claimed that JFS"
"             uses less CPU resources than other GNU/Linux file systems."
"             With certain optimizations, JFS has also been claimed to be"
"             faster for certain file operations, as compared to other"
"             GNU/Linux file systems.")
declare nilfs2HelpTxt=(
"   * 'nilfs2' - New Implementation of a Log-structured File System that uses"
"                journaling.  The storage medium is treated like a circular"
"                buffer and new blocks are always written to the end.  This is"
"                often used for flash media since they will naturally"
"                perform wear-leveling.")
declare ntfsHelpTxt=(
"   * 'ntfs' - (NTFS-3G) is an open source implementation of Microsoft NTFS"
"              that uses journaling.  It includes read and write support"
"              (the Linux kernel only supports reading NTFS).")
declare reiser4HelpTxt=(
"   * 'reiser4' - is an atomic file system that is the successor to ReiserFS."
"                 very efficient for handling small files (often used in"
"                 /var for this purpose) and includes features such as cheap"
"                 transparent compression and block  suballocation")
declare reiserfsHelpTxt=(
"   * 'reiserfs' - (now occasionally referred to as Reiser3) is a"
"                  general-purpose file system that uses journaling.")
declare vfatHelpTxt=(
"   * 'vfat' - File Allocation Table (FAT) that supports creating FAT32."
"             The FAT file system is a legacy file system which is simple"
"             and robust. It offers good performance even in light-weight"
"             implementations, but cannot deliver the same performance,"
"             reliability and scalability as some modern file systems. ")
declare xfsHelpTxt=(
"   * 'xfs' - a high-performance journaling file system created by"
"             Silicon Graphics.  It is particularly proficient at parallel"
"             IO due to its allocation group based design.")
declare FILE_SYSTEMS=("btrfs" "ext3" "ext4" "exfat" "f2fs" "jfs" "nilfs2" "ntfs" "reiser4" "reiserfs" "vfat" "xfs")
declare -A FILE_SYSTEMS_DESCS=(["btrfs"]=" - a modern copy on write (CoW) file system"
["ext3"]=" - the Third Extended (\"ext3\") file system"
["ext4"]=" - the evolution of \"ext3\""
["exfat"]=" - Extended File Allocation Table (exFAT)"
["f2fs"]=" - Flash-Friendly File System"
["jfs"]=" - The Journaled File System (JFS)"
["nilfs2"]=" - New Implementation of a Log-structured File System"
["ntfs"]=" - an open source implementation of Microsoft NTFS"
["reiser4"]=" - the successor FST for ReiserFS"
["reiserfs"]=" - a general-purpose file system"
["vfat"]=" - File Allocation Table (FAT)"
["xfs"]=" - a high-performance journaling FS")
declare FST_DLG_TXT_ARRAY=()

source -- "$ASSOC_ARRAY_FILE"

#===============================================================================
# functions
#===============================================================================

#---------------------------------------------------------------------------------------
#      METHOD:                       initVars
# DESCRIPTION: Initialize the global variables.
#  Required Params:
#      1) installStep - 'Format' or 'Mount'
#---------------------------------------------------------------------------------------
initVars() {
  local installStep="$1"

  BLOCK_DEVICE=$(echo "${varMap["BLOCK-DEVICE"]}")

  readarray -t PART_TABLE_ARRAY < "$PARTITION_TABLE_FILE"

  if [[ "${varMap[PART-LAYOUT]}" =~ "LVM" ]]; then
    FST_DLG_TXT_ARRAY=("Individual Logical Volumes & Partitions can be formatted using one"
      "of the many supported File System Types (FST).")
    DIALOG_BACK_TITLE=$(echo "'$installStep' the Logical Volumes & Partitions")
    setPhysicalVolume
  else
    FST_DLG_TXT_ARRAY=("Individual Partitions can be setup using one of the many different"
      "available File System Types (FST).")
    DIALOG_BACK_TITLE=$(echo "'$installStep' the Partitions")
  fi

  if [ "${varMap[BOOT-MODE]}" == "UEFI" ]; then
    UEFI_FLAG=$(echo 'true' && return 0)
  else
    UEFI_FLAG=$(echo 'false' && return 1)
  fi

  if [ "$installStep" == "Format" ]; then
    formatHelpText
    formatDescsForYAD
    echo "${FMT_DIALOG_IMAGE},${FMT_WINDOW_ICON}" > "$DIALOG_IMG_ICON_FILE"
  else
    echo "${MNT_DIALOG_IMAGE},${MNT_WINDOW_ICON}" > "$DIALOG_IMG_ICON_FILE"
  fi
  # This suppresses the WARNING "File descriptor 3 (/dev/pts/1) leaked on lvs invocation"
  export LVM_SUPPRESS_FD_WARNINGS=0
}

#---------------------------------------------------------------------------------------
#      METHOD:                      formatDescsForYAD
# DESCRIPTION: Remove " - " from the description of the file system types when they
#              are to be displayed within a "yad" dialog
#---------------------------------------------------------------------------------------
formatDescsForYAD() {
  if [ "$DIALOG" == "yad" ]; then
    for fstKey in "${FILE_SYSTEMS[@]}"; do
      FILE_SYSTEMS_DESCS["$fstKey"]="${FILE_SYSTEMS_DESCS["$fstKey"]:3}"
    done
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                      formatHelpText
# DESCRIPTION: Format the text for the help section to be displayed in the either a
#              linux or yad "dialog"
#---------------------------------------------------------------------------------------
formatHelpText() {
  local helpTextArray=()
  local helpText=""

  for fst in "${FILE_SYSTEMS[@]}"; do
    case "$fst" in
      "btrfs")
        helpTextArray=("${btrfsHelpTxt[@]}")
      ;;
      "ext3")
        helpTextArray=("${ext3HelpTxt[@]}")
      ;;
      "ext4")
        helpTextArray=("${ext4HelpTxt[@]}")
      ;;
      "exfat")
        helpTextArray=("${exfatHelpTxt[@]}")
      ;;
      "f2fs")
        helpTextArray=("${f2fsHelpTxt[@]}")
      ;;
      "jfs")
        helpTextArray=("${jfsHelpTxt[@]}")
      ;;
      "nilfs2")
        helpTextArray=("${nilfs2HelpTxt[@]}")
      ;;
      "ntfs")
        helpTextArray=("${ntfsHelpTxt[@]}")
      ;;
      "reiser4")
        helpTextArray=("${reiser4HelpTxt[@]}")
      ;;
      "reiserfs")
        helpTextArray=("${reiserfsHelpTxt[@]}")
      ;;
      "vfat")
        helpTextArray=("${vfatHelpTxt[@]}")
      ;;
      "xfs")
        helpTextArray=("${xfsHelpTxt[@]}")
      ;;
    esac
    if [ "$DIALOG" == "yad" ]; then
      helpText=$(formatTextForYAD helpTextArray)
    else
      helpText=$(getTextForDialog "${helpTextArray[@]}")
    fi
    FMT_HELP_TEXT_OPTS["$fst"]="$helpText"
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                    formatDevices
# DESCRIPTION: Format the logical volumes & partitions with an
#              appropriate file system
#---------------------------------------------------------------------------------------
formatDevices() {
  while true; do
    setNextStep
    manageFSTofDevices

    if [ ! ${varMap["FMT-STEP"]+_} ]; then
      EXIT_STATUS=2
      break
    elif [[ "${varMap[FMT-STEP]}" =~ "#" ]]; then
      local stepNum=$(echo "${varMap[FMT-STEP]}" | cut -d"#" -f2)
      if [ ${stepNum} -lt 0 ]; then
        varMap["FMT-STEP"]="Edit"
        break
      elif [ ${stepNum} -gt 5 ]; then
        unset varMap["FMT-STEP"]
        break
      fi
    fi
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                     setNextStep
# DESCRIPTION: Determine the next step in managing of the file system types for
#              the devices
#---------------------------------------------------------------------------------------
setNextStep() {
  local stepNum=1

  case "${varMap["FMT-STEP"]}" in
    "Edit")
      stepNum=5
    ;;
    "step#01")
      if [ "${varMap[BOOT-DEVICE]}" != "Skip" ]; then
        stepNum=2
      else
        stepNum=3
      fi
    ;;
    "step#02")
      stepNum=3
    ;;
    "step#03")
      stepNum=4
    ;;
    "step#04")
      stepNum=5
    ;;
    "step#05")
      stepNum=-1
    ;;
  esac

  varMap["FMT-STEP"]=$(printf "step#%02d" "$stepNum")
}

#---------------------------------------------------------------------------------------
#      METHOD:                  manageFSTofDevices
# DESCRIPTION: Do all the processing necessary to set the file system type of
#              the devices and format them.
#---------------------------------------------------------------------------------------
manageFSTofDevices() {
  local dialogPartTable=""
  local -A methodParams=(["dialogBackTitle"]="$DIALOG_BACK_TITLE")
  local linuxFmtCmds=()

  setDataForPartitionScheme methodParams

  case "${varMap[FMT-STEP]}" in
    "step#01")
      setRequired
      setFileSystemTypeForBL
    ;;
    "step#02")
      setFSTforBootDevice
    ;;
    "step#03")
      setFSTforRootDevice
    ;;
    "step#04")
      setFileSystemTypeForDevices
    ;;
    "step#05")
      local strCmds=$(getLinuxFmtCmds)
      readarray -t linuxFmtCmds <<< "${strCmds//$'\t'/$'\n'}"

      methodParams["statsTable"]=$(getStatsTable)
      getConfToExecCmds
      case "$inputRetVal" in
        "NO")
          unset varMap["FMT-STEP"]
        ;;
        "YES")
          createFileSystems
        ;;
        *)
          varMap["FMT-STEP"]="Edit"
        ;;
      esac
    ;;
  esac

  cleanDialogFiles
}

#---------------------------------------------------------------------------------------
#      METHOD:                         setRequired
# DESCRIPTION: Set the column with an '*' if it requires either a file system type
#              or a mount point
#---------------------------------------------------------------------------------------
setRequired() {
  local cols=()
  local row=""
  local lastIdx=$(expr ${#PART_TABLE_ARRAY[@]} - 1)
  local valIdx=4
  local colVal=""

  if [[ "$DIALOG_BACK_TITLE" =~ "Mount" ]]; then
    valIdx=5
  fi

  for aryIdx in $(eval echo "{0..$lastIdx}"); do
    row=$(echo -e "${PART_TABLE_ARRAY[$aryIdx]}")

    readarray -t cols <<< "${row//$'\t'/$'\n'}"

    cols[$valIdx]=$(trimString "${cols[$valIdx]}")
    if [[ ! "$row" =~ "swap" ]]; then
      if [ ${valIdx} -gt 4 ]; then
        if [ "${cols[3]}" == "8300" ] || [ "${cols[3]}" == "lvm" ]; then
          cols[$valIdx]="*"
        fi
      elif [ "${cols[3]}" == "8300" ] || [ "${cols[3]}" == "EF00" ] || [ "${cols[3]}" == "lvm" ]; then
        cols[$valIdx]="*"
      fi
    fi

    if [ ${#cols[$valIdx]} -gt 0 ]; then
      row=$(printf "\t%s" "${cols[@]}")
      row=${row:1}
      PART_TABLE_ARRAY[$aryIdx]=$(printf "%s" "$row")
    fi
  done

  updatePartTable PART_TABLE_ARRAY
}

#---------------------------------------------------------------------------------------
#      METHOD:                  setFileSystemTypeForBL
# DESCRIPTION: Set the file system type to "vfat" for the boot loader partition
#              if its GUID is 'EF00'
#---------------------------------------------------------------------------------------
setFileSystemTypeForBL() {
  local cols=()
  local aryIdx=0
  local row="${PART_TABLE_ARRAY[$aryIdx]}"

  readarray -t cols <<< "${row//$'\t'/$'\n'}"

  if [ "${cols[3]}" == "EF00" ] && [ "${cols[3]}" != "vfat" ]; then
    updateRowInPartTable ${aryIdx} PART_TABLE_ARRAY 4 "vfat"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                   setFSTforBootDevice
# DESCRIPTION: Set the file system type for the /boot partition or logical volume
#---------------------------------------------------------------------------------------
setFSTforBootDevice() {
  local row=""
  local cols=()

  setDataForBootDevice methodParams
  local aryIdx=-1
  unset methodParams["aryIdx"]

  if [ ! ${varMap["BOOT-DEVICE"]+_} ]; then
    selectRowNumOfBootDevice methodParams
    aryIdx=$(getArrayIndexOfDevice "$mbSelVal")
    setBootDevice ${aryIdx}
  else
    local joinSrchAry=$(printf "\n%s" "${PART_TABLE_ARRAY[@]}")
    aryIdx=$(echo -e "${joinSrchAry:1}" | grep -n "${varMap["BOOT-DEVICE"]}" | cut -d":" -f 1)
    aryIdx=$(expr $aryIdx - 1)
    row="${PART_TABLE_ARRAY[$aryIdx]}"
    row=$(trimString "$row")

    readarray -t cols <<< "${row//$'\t'/$'\n'}"

    setChoicesForBootDevice "${cols[3]}"
  fi

  if [ ${aryIdx} -gt -1 ]; then
    getFSTforBootDir ${aryIdx} methodParams
    if [ ${#mbSelVal} -gt 0 ]; then
      updateRowInPartTable ${aryIdx} PART_TABLE_ARRAY 4 "$mbSelVal"
    else
      unset varMap["FMT-STEP"]
    fi
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                   setDataForBootDevice
# DESCRIPTION: Set the data necessary to either
#                 1) display the rows from the partition table that could be
#                    the /boot logical volume or partition
#                 2) the array index found to be the row within the partition table
#                    for the /boot device
#  Required Params:
#      1) assocDataArray - associative array where the choices and their descriptions
#                          will be added to
#---------------------------------------------------------------------------------------
setDataForBootDevice() {
  local -n assocDataArray=$1
  local choices=()
  local lastIdx=$(expr ${#PART_TABLE_ARRAY[@]} - 1)
  local rowNum=0
  local aryIdx=-1
  local row=""
  local rowKey=""
  local flagsCol=""
  local cols=()

  for aryIdx in $(eval echo "{0..$lastIdx}"); do
    rowNum=$(expr $rowNum + 1)
    rowKey=$(printf "row#%02d" "$rowNum")
    row=$(echo -e "${PART_TABLE_ARRAY[$aryIdx]}")

    readarray -t cols <<< "${row//$'\t'/$'\n'}"

    flagsCol=$(trimString "${cols[6]}")
    if [ "${cols[5]}" == "/boot" ]; then
      aryIdx=$(getArrayIndexOfDevice "$rowKey")
      setBootDevice ${aryIdx}
      break
    elif [ ${#flagsCol} -lt 1 ]; then
      choices+=("$rowKey")
      assocDataArray["$rowKey"]=$(getDeviceNameForListDialog cols)
    fi
  done

  assocDataArray["aryIdx"]=${aryIdx}
  assocDataArray["choices"]=$(printf "\t%s" "${choices[@]}")
  assocDataArray["choices"]=${assocDataArray["choices"]:1}
  assocDataArray["partLayout"]="${varMap[PART-LAYOUT]}"
  assocDataArray["dialogTitle"]="Select /boot Device"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getArrayIndexOfDevice
# DESCRIPTION: Get the index to the global array 'PART_TABLE_ARRAY' that maps to a
#              logical volume or partition
#  Required Params:
#      1) rowKey - string containing the row number that was either found or
#                  selected
#---------------------------------------------------------------------------------------
function getArrayIndexOfDevice() {
  local rowKey="$1"
  local aryIdx=-1
  if [ ${#rowKey} -gt 0 ]; then
    rowNum=$(echo "$rowKey" | cut -d"#" -f2)
    aryIdx=$(expr $rowNum - 1)
  fi

  echo "${aryIdx}"
}

#---------------------------------------------------------------------------------------
#      METHOD:                      setBootDevice
# DESCRIPTION: Set the name of /boot device with key of "BOOT-DEVICE" within
#              the global associative array 'varMap'
#  Required Params:
#      1) aryIdx - the index within the global array 'PART_TABLE_ARRAY'
#---------------------------------------------------------------------------------------
setBootDevice() {
  local aryIdx=$1
  if [ ${aryIdx} -gt -1 ]; then
    row=$(echo -e "${PART_TABLE_ARRAY[$aryIdx]}")

    readarray -t cols <<< "${row//$'\t'/$'\n'}"

    if [ "${cols[3]}" == "lvm" ]; then
      varMap["BOOT-DEVICE"]=$(echo "${cols[1]}")
    else
      varMap["BOOT-DEVICE"]=$(echo "${cols[0]}")
    fi
  else
    varMap["BOOT-DEVICE"]="Skip"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                   setChoicesForBootDevice
# DESCRIPTION: Set the menu choices of the FSTs available for the '/boot' device
#  Required Params:
#      1) the value of the GUID column within the partition table for the '/boot' device
#---------------------------------------------------------------------------------------
setChoicesForBootDevice() {
  if [ "$1" == "lvm" ]; then
    setDataForGrub2FST "${varMap["BOOT-DEVICE"]}" methodParams
    varMap["BOOT-LOADER"]="Grub2"
  else
    local menuChoices=()
    local blOpts=()
    local sep="|"
    setDataForGrub2FST "${varMap["BOOT-DEVICE"]}" methodParams
    readarray -t menuChoices <<< "${methodParams[choices]//$'\t'/$'\n'}"
    blOpts=("${menuChoices[@]}")

    local keys=("rEFInd" "Syslinux" "systemd-boot")
    for bl in "${keys[@]}"; do
      readarray -t menuChoices <<< "${BOOT_LOADER_FSTS["$bl"]//$sep/$'\n'}"
      blOpts+=("${menuChoices[@]}")
    done

    menuChoices=($(printf "%s\n" "${blOpts[@]}" | sort -u | tr '\n' ' '))
    methodParams["choices"]=$(printf "\t%s" "${menuChoices[@]}")
    methodParams["choices"]=${methodParams["choices"]:1}
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     getFSTforBootDir
# DESCRIPTION: Get the file system type for either the /boot or '/' directory
#              that must be supported by the boot loader.
#      RETURN: string containing the supported file system type
#  Required Params:
#      1) aryIdx - the index within the global array 'PART_TABLE_ARRAY'
#---------------------------------------------------------------------------------------
getFSTforBootDir() {
  local aryIdx=$1
  local -n fstMthdParams=$2
  local fsTypes=()
  local textArray=()
  local deviceName=$(getDeviceName ${aryIdx})

  if [ ${varMap["BOOT-LOADER"]+_} ]; then
    fstMthdParams["dialogTitle"]=$(printf "Supported File Systems for the '%s' boot loader" "${varMap["BOOT-LOADER"]}")
    fstMthdParams["devWarnText"]=$(getTextForDeviceWarning "$deviceName")
  else
    fstMthdParams["dialogTitle"]="Supported File Systems for the boot loaders"
  fi

  selectFileSystemType fstMthdParams
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                      getDeviceName
# DESCRIPTION: Get the name of the device to set the file system type for.
#      RETURN: string containing the name and device type
#  Required Params:
#      1) aryIdx - the index within the global array 'PART_TABLE_ARRAY'
#---------------------------------------------------------------------------------------
function getDeviceName() {
  local aryIdx=$1
  local cols=()
  local row=$(echo -e "${PART_TABLE_ARRAY[$aryIdx]}")
  local deviceName=""

  readarray -t cols <<< "${row//$'\t'/$'\n'}"

  if [[ "$DIALOG_BACK_TITLE" =~ "Mount" ]]; then
    if [[ "${cols[0]}" =~ "sda" ]]; then
      deviceName=$(printf "%s : %s" "${cols[0]}" "${cols[1]}")
    else
      deviceName=$(echo "${cols[1]}")
    fi
  else
    if [ "${cols[3]}" == "lvm" ]; then
      deviceName=$(printf "Logical Volume: %s" "${cols[1]}")
    else
      deviceName=$(printf "Partition: %s" "${cols[1]}")
    fi
  fi

  echo "$deviceName"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getTextForDeviceWarning
# DESCRIPTION: Get the text to display the warning message about the supported
#              file system type that needs to be used for the /boot directory
#      RETURN: concatenated string
#  Required Params:
#      1) name of the device
#---------------------------------------------------------------------------------------
function getTextForDeviceWarning() {
  local deviceWarning=(
  " * The $1 will restrict the choice of file system.  The EFI System Partition"
  "will contain a FAT32 (mkfs.vfat) file system, but the file system containing"
  "the /boot directory must be supported by the '${varMap["BOOT-LOADER"]}' boot loader.")
  local text=$(formatTextForYAD deviceWarning)

  echo "$text"
}

#---------------------------------------------------------------------------------------
#      METHOD:                    setDataForGrub2FS
# DESCRIPTION: Set the data to be displayed in a "dialog" box of type radio list
#              for the Grub2 boot loader supported file system types
#  Required Params:
#      1) deviceName - name of the device
#      2) mthdParams - associative array of key/value pairs where the choices, urlRef,
#                      and dialogText will be added
#---------------------------------------------------------------------------------------
setDataForGrub2FST() {
  local -n mthdParams=$2
  local fstChoices=()
  local textArray=(
  "GRUB (GRand Unified Bootloader) is a multi-boot loader.  The current GRUB"
  "is also referred to as GRUB 2 while GRUB Legacy corresponds to versions"
  "0.9x.")
  mthdParams["urlRef"]="https://wiki.archlinux.org/index.php/GRUB"

  readarray -t fstChoices <<< "${BOOT_LOADER_FSTS["Grub2"]//$sep/$'\n'}"

  setDataForSelFST mthdParams fstChoices textArray "$1"
}

#---------------------------------------------------------------------------------------
#      METHOD:                      setDataForSelFST
# DESCRIPTION: Set the data to be displayed in a "dialog" box of type radio list for
#              selecting a file system type
#  Required Params:
#      1)   aAryParams - associative array of key/value pairs where the choices, dialogText,
#                        and selectStr will be added
#      2)     menuOpts - array of file system types that can be selected
#      3) dlgTextArray - array of strings to be concatenated and displayed in the text
#                        area of the dialog
#   Optional Param:
#      4) string containing the name of a device
#---------------------------------------------------------------------------------------
setDataForSelFST() {
  local -n aAryParams=$1
  local -n menuOpts=$2
  local -n dlgTextArray=$3
  local text=""

  if [ "$#" -gt 3 ]; then
    text=$(echo "${FST_SEL_TXT} for the '$4':")
  else
    text=$(echo "${FST_SEL_TXT}:")
  fi

  if [ "$DIALOG" == "yad" ]; then
    aAryParams["selectStr"]="$text"
    text=$(printf " %s" "${dlgTextArray[@]}")
    text=${text:1}
  elif [ ${aAryParams["partTableStats"]+_} ]; then
    dlgTextArray=("${aAryParams["partTableStats"]}" "$DIALOG_BORDER" " "
      "${dlgTextArray[@]}" "$DIALOG_BORDER" " " "$text")
    text=$(getTextForDialog "${dlgTextArray[@]}")
  else
    dlgTextArray+=("$DIALOG_BORDER" " " "$text")
    text=$(getTextForDialog "${dlgTextArray[@]}")
  fi

  aAryParams["dialogText"]="$text"
  aAryParams["choices"]=$(printf "\t%s" "${menuOpts[@]}")
  aAryParams["choices"]=${aAryParams["choices"]:1}
}

#---------------------------------------------------------------------------------------
#      METHOD:                   setFSTforRootDevice
# DESCRIPTION: Set the file system type for the '/' or root device
#              (i.e. partition or logical volume) when the /boot device DOES NOT EXIST
#---------------------------------------------------------------------------------------
setFSTforRootDevice() {
  local lastIdx=$(expr ${#PART_TABLE_ARRAY[@]} - 1)
  local aryIdx=-1
  local row=""
  local flagsCol=""
  local cols=()

  for aryIdx in $(eval echo "{0..$lastIdx}"); do
    row=$(echo -e "${PART_TABLE_ARRAY[$aryIdx]}")

    readarray -t cols <<< "${row//$'\t'/$'\n'}"

    flagsCol=$(trimString "${cols[6]}")
    if [ "${cols[6]}" == "ROOT" ]; then
      aryIdx=${aryIdx}
      break
    fi
  done

  if [ "${varMap["BOOT-DEVICE"]}" == "Skip" ]; then
    getFSTforBootDir ${aryIdx} methodParams
  else
    getFSTforOneDevice ${aryIdx} methodParams
  fi
  if [ ${#mbSelVal} -gt 0 ]; then
    updateRowInPartTable ${aryIdx} PART_TABLE_ARRAY 4 "$mbSelVal"
  else
    unset varMap["FMT-STEP"]
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     getFSTforOneDevice
# DESCRIPTION: Get the file system type for a single logical volume or partition
#      RETURN: string containing the supported file system type
#  Required Params:
#      1) aryIdx - the index within the global array 'PART_TABLE_ARRAY'
#---------------------------------------------------------------------------------------
getFSTforOneDevice() {
  local aryIdx=$1
  local -n fstMthdParams=$2
  local fsTypes=()
  local textArray=()
  local deviceName=$(getDeviceName ${aryIdx})
  local textArray=("${FST_DLG_TXT_ARRAY}")
  fstMthdParams["dialogTitle"]="Arch Linux Supported File System Types (FST)"

  setDataForSelFST fstMthdParams FILE_SYSTEMS textArray "$deviceName"

  selectFileSystemType fstMthdParams
}

#---------------------------------------------------------------------------------------
#      METHOD:               setFileSystemTypeForDevices
# DESCRIPTION: Set the file system type for the devices
#---------------------------------------------------------------------------------------
setFileSystemTypeForDevices() {
  local -A fstMthdParams=()
  local submittedVals=()
  local choicesStr=""
  local existFlag=$(echo 'false' && return 1)

  setDataForFSTDialogs
  local dialogTitle="${methodParams["dialogTitle"]}"
  if [ "$DIALOG" != "yad" ]; then
    choicesStr="${fstMthdParams["choicesStr"]}"
    unset fstMthdParams["choicesStr"]
  fi

  while true; do
    selectDeviceToAssignFST methodParams
    rowsFST=""
    if [ ${#mbSelVal} -gt 0 ]; then
      IFS=$'\r' read -a submittedVals <<< "$mbSelVal"
      fstMthdParams["dialogPartTable"]="${methodParams["dialogPartTable"]}"

      case "${submittedVals[0]}" in
        "All")
          if [ "$DIALOG" == "yad" ]; then
            rowsFST=$(getSelectedValues "${methodParams[choices]}" "${submittedVals[1]}")
          else
            showSelectFileSystemDialog ${submittedVals[0]}
            if [ ${#mbSelVal} -gt 0 ]; then
              rowsFST=$(printf "%s\r%s" "$choicesStr" "$mbSelVal")
            fi
          fi
        ;;
        "Done"|"Format") # "Format"
          #### NOTE: The method "setNextStep" will move on to the next step
          ####       where the validation and confirmation will be done
          existFlag=$(isUnassigned "${methodParams["statsTable"]}")
          if ${existFlag}; then
            dispUnassignedMsg
            varMap["FMT-STEP"]="Edit"
          else
            break
          fi
        ;;
        "Reset")
          resetFSTs
          setRequired
          setFileSystemTypeForBL
          varMap["FMT-STEP"]="step#01"
          break
        ;;
        "ROOT")
          mbSelVal=$(getFileSystemTypeForRoot)
          if [ "$DIALOG" == "yad" ]; then
            rowsFST=$(getSelectedValues "${methodParams[choices]}" "$mbSelVal")
          else
            rowsFST=$(printf "%s\r%s" "$choicesStr" "$mbSelVal")
          fi
        ;;
        *)
          if [ "$DIALOG" != "yad" ]; then
            showSelectFileSystemDialog "${submittedVals[0]}"
            if [ ${#mbSelVal} -gt 0 ]; then
              rowsFST=$(printf "%s\r%s" "${submittedVals[0]}" "$mbSelVal")
            fi
          elif [ ${#submittedVals[@]} -lt 2 ]; then
            showWarningDialog
          else
            #rowsFST=$(printf "%s\r%s" "${submittedVals[0]}" "${submittedVals[1]}")
            rowsFST=$(getSelectedValues "${submittedVals[0]}" "${submittedVals[1]}")
          fi
        ;;
      esac

      if [ ${#rowsFST} -gt 0 ]; then
        updateFileSystemTypeForRows "$rowsFST"
      fi
    else
      unset varMap["FMT-STEP"]
      break
    fi
    setDataForPartitionScheme methodParams
    methodParams["statsTable"]=$(getStatsTable)
    methodParams["dialogTitle"]="$dialogTitle"

    cleanDialogFiles
  done

}

function getSelectedValues() {
  local rowIDs="$1"
  local fst="$2"
  local selOpts=()

  readarray -t selOpts <<< "${rowIDs//$'\t'/$'\n'}"
  selOpts+=("$fst")
  local concatStr=$(printf "\t%s" "${selOpts[@]}")
  echo "${concatStr:1}"
}

#---------------------------------------------------------------------------------------
#      METHOD:                    setDataForFSTDialogs
# DESCRIPTION: Set the data to be displayed in the dialogs for choosing/selecting
#              the devices & file system type
#---------------------------------------------------------------------------------------
setDataForFSTDialogs() {
  local choicesStr=""
  local rowKeys=()

  if [[ "${varMap["PART-LAYOUT"]}" =~ "LVM" ]]; then
    methodParams["deviceType"]="Logical Volumes"
  else
    methodParams["deviceType"]="Partitions"
  fi

  setChoicesForSelectingDevices

  methodParams["dialogTitle"]=$(echo "'${methodParams[deviceType]}' to format")
  methodParams["statsTable"]=$(getStatsTable)

  if [ "$DIALOG" == "yad" ]; then
    setHelpTextForYAD
  else
    fstMthdParams["dialogBackTitle"]="${methodParams["dialogBackTitle"]}"
    fstMthdParams["dialogTitle"]="Arch Linux Supported File System Types (FST)"
    for fsKey in "${FILE_SYSTEMS[@]}"; do
      fstMthdParams["$fsKey"]=$(echo "${FILE_SYSTEMS_DESCS[$fsKey]}")
    done

    readarray -t rowKeys <<< "${methodParams["choices"]//$'\t'/$'\n'}"

    rowKeys[-1]=$(trimString "${rowKeys[-1]}")
    for opt in "${rowKeys[@]}"; do
      if [[ "$opt" =~ "row" ]]; then
        submittedVals+=("$opt")
      fi
    done
    fstMthdParams["choicesStr"]=$(printf "\t%s" "${submittedVals[@]}")
    fstMthdParams["choicesStr"]=${fstMthdParams["choicesStr"]:1}
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                    setDataForFSTDialogs
# DESCRIPTION: Set text for the help dialog to be displayed on 2 tabs
#              for choosing/selecting:
#                Tab #1) the devices
#                Tab #2) the file system type
#---------------------------------------------------------------------------------------
setHelpTextForYAD() {
  local helpTextKeys=()
  local helpTabKeys=()
  local helpText=""
  local helpTab=""
  local -A helpParamArray=()
  for helpIdx in $(eval echo "{1..2}"); do
    case ${helpIdx} in
      1)
        helpText=$(getHelpTextForDevFST)
        helpTab="Help:  Select &quot;${methodParams["deviceType"]}&quot;"
      ;;
      2)
        helpParamArray["dialogText"]=$(printf " %s" "${FST_DLG_TXT_ARRAY[@]}")
        helpParamArray["dialogText"]=${helpParamArray["dialogText"]:1}
        helpParamArray["choices"]=$(printf "\t%s" "${FILE_SYSTEMS[@]}")
        helpParamArray["choices"]=${helpParamArray["choices"]:1}
        helpText=$(getHelpTextForFST helpParamArray)
        helpTab="Help:  Select &quot;File System Type&quot;"
      ;;
    esac
    helpKey=$(printf "helpText#%02d" ${helpIdx})
    helpTextKeys+=("$helpKey")
    methodParams["$helpKey"]="$helpText"
    helpKey=$(printf "helpTab#%02d" ${helpIdx})
    helpTabKeys+=("$helpKey")
    methodParams["$helpKey"]="$helpTab"
  done
  methodParams["helpTextKeys"]=$(printf "\t%s" "${helpTextKeys[@]}")
  methodParams["helpTextKeys"]=${methodParams["helpTextKeys"]:1}
  methodParams["helpTabKeys"]=$(printf "\t%s" "${helpTabKeys[@]}")
  methodParams["helpTabKeys"]=${methodParams["helpTabKeys"]:1}
}

#---------------------------------------------------------------------------------------
#      METHOD:                 showSelectFileSystemDialog
# DESCRIPTION: Show the linux "dialog" to select the file system type.  The selected
#              value will be in the global variable "mbSelVal"
#  Required Params:
#      1) selRowKeys - Selections chosen in the linux "dialog" to select the devices
#                      to set the file system type to.
#---------------------------------------------------------------------------------------
showSelectFileSystemDialog() {
  local selRowKeys="$1"
  local rowKeys=()
  local rowKey=""
  local selRow=$(printf "'%s' that were selected:" "${methodParams["deviceType"]}")
  local textArray=("$selRow")

  readarray -t rowKeys <<< "${selRowKeys//$'\t'/$'\n'}"

  local len=${#rowKeys[@]}
  local lastIdx=$(expr $len - 1)
  if [ ${len} -gt 7 ]; then
    lastIdx=6
  fi

  for aryIdx in $(eval echo "{0..$lastIdx}"); do
    rowKey="${rowKeys[$aryIdx]}"
    selRow=$(printf "[*] '%s' - %s" "$rowKey" "${methodParams["$rowKey"]}")
    textArray+=("$selRow")
  done

  if [ ${len} -gt 7 ]; then
    textArray+=("           More....")
  fi

  setDataForSelFST fstMthdParams FILE_SYSTEMS textArray
  selectFileSystemType fstMthdParams
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getFileSystemTypeForRoot
# DESCRIPTION: Get the file system type assigned to the '/' or root device
#      RETURN: file system type for the ROOT logical volume or partition
#---------------------------------------------------------------------------------------
function getFileSystemTypeForRoot() {
  local cols=()
  for row in "${PART_TABLE_ARRAY[@]}"; do
    readarray -t cols <<< "${row//$'\t'/$'\n'}"

    flagsCol=$(trimString "${cols[6]}")
    if [ "${cols[6]}" == "ROOT" ]; then
      break
    fi
  done
  echo "${cols[4]}"
}

#---------------------------------------------------------------------------------------
#      METHOD:                 updateFileSystemTypeForRows
# DESCRIPTION: Update the selected rows of devices within the global array "$PART_TABLE_ARRAY"
#              as well as in the global file name "$PARTITION_TABLE_FILE"
#  Required Params:
#      1) rowsAndFST - string containing the selected rows and file system type separated
#                      by '\r'
#---------------------------------------------------------------------------------------
updateFileSystemTypeForRows() {
  local rowsAndFST="$1"
  local rowKeys=()
  local cols=()
  local aryIdx=-1
  local row=""
  local fst=""

  readarray -t rowKeys <<< "${rowsAndFST[0]//$'\t'/$'\n'}"

  fst="${rowKeys[$aryIdx]}"
  unset rowKeys[$aryIdx]

  for rowKey in "${rowKeys[@]}"; do
    aryIdx=$(getArrayIndexOfDevice "$rowKey")
    row="${PART_TABLE_ARRAY[$aryIdx]}"

    readarray -t cols <<< "${row//$'\t'/$'\n'}"

    cols[4]="$fst"
    row=$(printf "\t%s" "${cols[@]}")
    row=${row:1}
    PART_TABLE_ARRAY[$aryIdx]=$(printf "%s" "$row")
  done

  updatePartTable PART_TABLE_ARRAY
}

#---------------------------------------------------------------------------------------
#      METHOD:                   setFSTforSelectedRows
# DESCRIPTION: Set the file system type for the rows that were selected
#  Required Params:
#      1) string containing the rows that were selected separated by '\t'
#---------------------------------------------------------------------------------------
setFSTforSelectedRows() {
  local param="$1"
  local cols=()
  local selRows=()
  local row=""
  local aryIdx=-1

  readarray -t selRows <<< "${param//$'\t'/$'\n'}"

  for opt in "${selRows[@]}"; do
    aryIdx=$(getArrayIndexOfDevice "$opt")
    row=$(echo -e "${PART_TABLE_ARRAY[$aryIdx]}")

    readarray -t cols <<< "${row//$'\t'/$'\n'}"

    cols[4]=$(echo "$mbSelVal")
    row=$(printf "\t%s" "${cols[@]}")
    row=${row:1}
    PART_TABLE_ARRAY[$aryIdx]=$(printf "%s" "$row")
  done

  updatePartTable PART_TABLE_ARRAY
}

#---------------------------------------------------------------------------------------
#    FUNCTION:              getIndexOfSelectedBootDevice
# DESCRIPTION: Check one of the rows selected is either the /boot or '/' device.
#      RETURN: the array index of the device if it will contain the /boot directory
#  Required Params:
#      1) string containing the rows that were selected separated by '\t'
#---------------------------------------------------------------------------------------
function getIndexOfSelectedBootDevice() {
  local selRows=()
  local cols=()
  local row=""
  local aryIdx=-1

  readarray -t selRows <<< "${mbSelVal//$'\t'/$'\n'}"

  for opt in "${selRows[@]}"; do
    aryIdx=$(getArrayIndexOfDevice "$opt")
    row=$(echo -e "${PART_TABLE_ARRAY[$aryIdx]}")

    readarray -t cols <<< "${row//$'\t'/$'\n'}"

    if [ "${cols[0]}" == "${varMap[BOOT-DEVICE]}" ]; then
      break
    elif [ "${varMap[BOOT-DEVICE]}" == "Skip" ]; then
      local existFlag=$(isRootDevice "$row")
      if ${existFlag}; then
        break
      fi
    fi

    aryIdx=-1
  done

  echo ${aryIdx}
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getSelectedRowsTbl
# DESCRIPTION: Get the rows that were selected in a tabular format
#      RETURN: concatenated string
#  Required Params:
#      1) string containing the rows that were selected separated by '\t'
#---------------------------------------------------------------------------------------
function getSelectedRowsTbl() {
  local aryIdx=0
  local cols=()
  local selRows=()
  local tableRows=()
  local row=$(printf "%s;%s;%s" "Selected Row" "Device Name" "FS Type")
  local deviceName=""

  tableRows+=("$row")

  readarray -t selRows <<< "${mbSelVal//$'\t'/$'\n'}"

  for opt in "${selRows[@]}"; do
    aryIdx=$(getArrayIndexOfDevice "$opt")
    row=$(echo -e "${PART_TABLE_ARRAY[$aryIdx]}")

    readarray -t cols <<< "${row//$'\t'/$'\n'}"

    if [ "${cols[3]}" == "lvm" ]; then
      deviceName=$(echo "${cols[1]}")
    else
      deviceName=$(echo "${cols[0]} : ${cols[1]}")
    fi
    row=$(echo "${opt/\#/-};$deviceName;${cols[4]}")
    tableRows+=("$row")
  done

  local tbl=$(printf "\n%s" "${tableRows[@]}")
  tbl=${tbl:1}
  tbl=$(printTable ';' "$tbl" | sed 's/^\-\-\-/   /')

  tbl=$(echo "${tbl//row-/row\#}")
  echo -e "$tbl"
}

#---------------------------------------------------------------------------------------
#      METHOD:                setChoicesForSelectingDevices
# DESCRIPTION: Set the choices and descriptions for the dialog to the rows of
#              logical volumes & partitions required to have a file system type & mount pt
#---------------------------------------------------------------------------------------
setChoicesForSelectingDevices() {
  local choices=()
  local rowNum=1
  local row=""
  local rowKey=""
  local mntPt=""
  local cols=()
  local flagsCol=""
  local deviceType="Partitions"

  if [ ${methodParams["deviceType"]+_} ]; then
    deviceType="${methodParams["deviceType"]}"
  elif [[ "${varMap["PART-LAYOUT"]}" =~ "LVM" ]]; then
    deviceType="Logical Volumes & Partitions"
  else
    deviceType="Partitions"
  fi

  if [ "$#" -gt 0 ]; then
    setChoicesForMPT "$deviceType"
  else
    if [ "$DIALOG" != "yad" ] && [ "$#" -lt 1 ]; then
      choices=("All" "ROOT" )
      methodParams["All"]="set the same file system type to all of the rows"
      methodParams["ROOT"]="set the file system type for '/' or root to all of the rows"
    fi

    for row in "${PART_TABLE_ARRAY[@]}"; do
      rowKey=$(printf "row#%02d" "$rowNum")

      readarray -t cols <<< "${row//$'\t'/$'\n'}"

      mntPt=$(trimString "${cols[5]}")
      flagsCol=$(trimString "${cols[6]}")

      if [ ${#flagsCol} -lt 1 ] && [ "$mntPt" != "/boot" ]; then
        choices+=("$rowKey")
        methodParams["$rowKey"]=$(getDeviceNameForListDialog cols)
      fi
      rowNum=$(expr $rowNum + 1)
    done

    if [ "$DIALOG" != "yad" ]; then
      choices+=("Format" )
      methodParams["Format"]="format the $deviceType with their FST"
    fi
    methodParams["choices"]=$(printf "\t%s" "${choices[@]}")
  fi

  methodParams["choices"]=${methodParams["choices"]:1}
}

#---------------------------------------------------------------------------------------
#      METHOD:                      setChoicesForMPT
# DESCRIPTION: Set the choices and descriptions for the dialog to the rows of
#              logical volumes & partitions required to have a mount pt
#---------------------------------------------------------------------------------------
setChoicesForMPT() {
  local deviceType="$1"
  local rowNum=1
  local row=""
  local rowKey=""
  local mntPt=""
  local cols=()
  local flagsCol=""
  local choiceStr=""
  local -A staticMountPts=(["/boot"]="true" ["/home"]="true" ["/var"]="true")

  for row in "${PART_TABLE_ARRAY[@]}"; do
    rowKey=$(printf "row#%02d" "$rowNum")

    readarray -t cols <<< "${row//$'\t'/$'\n'}"

    mntPt=$(trimString "${cols[5]}")
    flagsCol=$(trimString "${cols[6]}")

    if [ ${#flagsCol} -lt 1 ] && [ ! ${staticMountPts["$mntPt"]+_} ]; then
      if [ "$DIALOG" == "yad" ]; then
        if [[ "${cols[0]}" =~ "sda" ]]; then
          choiceStr=$(printf "%s - %s : %s" "$rowKey" "${cols[0]}" "${cols[1]}")
        else
          choiceStr=$(printf "%s - %s" "$rowKey" "${cols[1]}")
        fi
        choices+=("$choiceStr")
      else
        choices+=("$rowKey")
        methodParams["$rowKey"]=$(getDeviceNameForListDialog cols)
      fi
    fi
    rowNum=$(expr $rowNum + 1)
  done

  if [ "$DIALOG" != "yad" ]; then
    choices+=("Mount" )
    methodParams["Mount"]="mount the $deviceType to their specified directory"
  fi

  if [ "$DIALOG" == "yad" ]; then
    methodParams["choices"]=$(printf ",%s" "${choices[@]}")
  else
    methodParams["choices"]=$(printf "\t%s" "${choices[@]}")
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getLinuxFmtCmds
# DESCRIPTION: Get the linux commands to format the devices with
#              their selected file system type
#      RETURN: concatenated string separated by '\n'
#---------------------------------------------------------------------------------------
function getLinuxFmtCmds() {
  local cmds=()
  local cols=()
  local cmd=""
  local fst=""

  for row in "${PART_TABLE_ARRAY[@]}"; do

    readarray -t cols <<< "${row//$'\t'/$'\n'}"

    fst=$(trimString "${cols[4]}")
    cmd=$(getFmtMntDevName "${cols[0]}" "${cols[1]}")

    if [[ "$row" =~ "swap" ]]; then
      cmd=$(printf "mkswap %s" "$cmd")
      cmds+=("$cmd")
    elif [ ${#fst} -gt 0 ]; then
      case "$fst" in
        "btrfs")
          if [ ${TRIM} -gt 0 ]; then
            cmd=$(printf "mkfs.%s -f -O discard discard %s" "$fst" "$cmd")
          else
            cmd=$(printf "mkfs.%s -f %s" "$fst" "$cmd")
          fi
        ;;
        "ext4")
          if [ ${TRIM} -gt 0 ]; then
            cmd=$(printf "mkfs.%s -E discard %s" "$fst" "$cmd")
          else
            cmd=$(printf "mkfs.%s %s" "$fst" "$cmd")
          fi
        ;;
        "reiser4")
          cmd=$(printf "mkfs.%s -f %s" "$fst" "$cmd")
        ;;
        "reiserfs")
          cmd=$(printf "mkfs.%s -f %s" "$fst" "$cmd")
        ;;
        "vfat")
          cmd=$(printf "mkfs.%s -F32 %s" "$fst" "$cmd")
        ;;
        "xfs")
          cmd=$(printf "mkfs.%s -f %s" "$fst" "$cmd")
        ;;
        *)
          cmd=$(printf "mkfs.%s %s" "$fst" "$cmd")
        ;;
      esac

      cmds+=("$cmd")
    fi
  done

  cmd=$(printf "\t%s" "${cmds[@]}")
  cmd=${cmd:1}

  echo -e "$cmd"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getFmtMntDevName
# DESCRIPTION: Get the name of the device to be either formatted or mounted for a
#              logical volume or partition
#      RETURN: string containing the device name
#  Required Params:
#      1) deviceCol - value of the "Device" column from the partition table
#      2)   nameCol - value of the "Name" column from the partition table
#---------------------------------------------------------------------------------------
function getFmtMntDevName() {
  local deviceCol=$(trimString "$1")
  local nameCol=$(trimString "$2")

  if [[ "$deviceCol" =~ "sda" ]]; then
    deviceName=$(printf "%s" "$deviceCol")
  else
    deviceName=$(printf "/dev/%s/%s" "${varMap["VG-NAME"]}" "$nameCol")
  fi

  echo "$deviceName"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     isUnassigned
# DESCRIPTION: Check if there are any logical volumes or partitions that DO NOT
#              have a file system type or mount pt assigned to them
#      RETURN: 0 - true, 1 - false
#  Required Params:
#      1) statsTbl - statistics of the progress made in tabular format
#---------------------------------------------------------------------------------------
function isUnassigned() {
  local statsTbl="$1"
  local unassigned=$(getNumOfUnassigned "$statsTbl")

  if [ ${unassigned} -gt 0 ]; then
    echo 'true' && return 0
  fi

  echo 'false' && return 1
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     getDialogPartTblText
# DESCRIPTION: Get the text to display in a linux "dialog" message box of the
#              partition table with either:
#              a) the "FS Type" column set to an '*' for the logical volumes and/or
#                 partitions left unassigned that require a file system.
#              b) the "Mount Pt" column set to an '*' for the logical volumes and/or
#                 partitions left unassigned that require a mount point.
#      RETURN: concatenated string
#  Required Params:
#      1) dialogPartTable - string containing the rows of data for the partition table
#---------------------------------------------------------------------------------------
getDialogPartTblText() {
  local dialogPartTable="$1"
  local colIdx=5
  local row=""
  local colVal=""
  local guid=""
  local tableRows=()

  if [ "$#" -gt 1 ]; then
    colIdx=6
  fi

  readarray -t tableRows <<< "$dialogPartTable"
  local lastIdx=$(expr ${#tableRows[@]} - 1)

  for aryIdx in $(eval echo "{4..$lastIdx}"); do
    row=$(echo -e "${tableRows[$aryIdx]}")
    IFS='|' read -d '' -a cols <<< "$row"
    if [ ${#cols[@]} -gt 1 ]; then
      colVal=$(trimString "${cols[$colIdx]}")
      guid=$(trimString "${cols[4]}")
      if [ "$guid" == "8300" ] ||  [ "$guid" == "lvm" ] && [ ${#colVal} -lt 1 ]; then
        colVal=$(echo "${cols[$colIdx]}")
        cols[$colIdx]="${colVal:0:4}*${colVal:5}"
        row=$(printf "|%s" "${cols[@]}")
        row=${row:1}
        tableRows[$aryIdx]=$(printf "%s" "$row")
        dialogPartTable=$(printf "\n%s" "${tableRows[@]}")
        dialogPartTable=${dialogPartTable:1}
      fi
    fi
  done

  echo "$dialogPartTable"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getStatsTable
# DESCRIPTION: Get statistics of the progress made to the required devices either
#                   A) setting to an appropriate file system
#                   B) mount file system to a directory
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getStatsTable() {
  local cols=()
  local len=${#PART_TABLE_ARRAY[@]}
  local numRequired=0
  local numAssigned=0
  local numUnassigned=0
  local row=""
  local colVal=""
  local valIdx=4
  local colHdrs=("Total Devices" "Require FST" "Assigned" "Unassigned")
  local statsTbl=""

  if [[ "$DIALOG_BACK_TITLE" =~ "Mount" ]]; then
    valIdx=5
    colHdrs[1]="Require Mount Pt"
  fi

  for row in "${PART_TABLE_ARRAY[@]}"; do
    readarray -t cols <<< "${row//$'\t'/$'\n'}"

    colVal=$(trimString "${cols[$valIdx]}")
    if [ ${#colVal} -gt 0 ]; then
        numRequired=$(expr $numRequired + 1)
      if [ "$colVal" != "*" ]; then
        numAssigned=$(expr $numAssigned + 1)
      fi
    fi
  done

  numUnassigned=$(expr $numRequired - $numAssigned)

  if [ "$DIALOG" == "yad" ]; then
    statsTbl=$(getStatsForYAD ${len} ${numRequired} ${numAssigned} ${numUnassigned} colHdrs)
  else
    statsTbl=$(getStatsTableForDialog ${len} ${numRequired} ${numAssigned} ${numUnassigned} colHdrs)
  fi

  echo "$statsTbl"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getStatsTableForDialog
# DESCRIPTION: Get the statistics of the progress made for the linux "dialog"
#      RETURN: concatenated string of the statistics in a tabular format
#  Required Params:
#      1)  totalDevices - total number of devices (i.e. length of PART_TABLE_ARRAY)
#      2)   numRequired - number of devices that require a file system type or a
#                         mount point to be assigned/set
#      3)   numAssigned - number of devices that have a value assigned
#      4) numUnassigned - number of devices that need a value to be assigned
#      5)       colHdrs - array of strings for the column headers
#---------------------------------------------------------------------------------------
function getStatsTableForDialog() {
  local totalDevices=$1
  local numRequired=$2
  local numAssigned=$3
  local numUnassigned=$4
  local colHdrs=$5
  local tableRows=()
  local row=""
  local colVal=""
  local valIdx=4
  local colHdrs=("Total Devices" "Require FST" "Assigned" "Unassigned")
  local rowFmt="%8d;%7d;%5d;%6d"

  if [[ "$DIALOG_BACK_TITLE" =~ "Mount" ]]; then
    rowFmt="%8d;%7d;%5d;%6d"
  fi

  row=$(printf ";%s" "${colHdrs[@]}")
  row=${row:1}
  tableRows+=("$row")

  row=$(printf "$rowFmt" ${totalDevices} ${numRequired} ${numAssigned} ${numUnassigned})
  tableRows+=("$row")

  local tbl=$(printf "\n%s" "${tableRows[@]}")
  tbl=${tbl:1}
  tbl=$(printTable ';' "$tbl" | sed 's/^\-\-\-/   /')

  echo -e "$tbl"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                       getStatsForYAD
# DESCRIPTION: Get the statistics of the progress made for the yad "dialog"
#      RETURN: concatenated string separated by '\n'
#  Required Params:
#      1)  totalDevices - total number of devices (i.e. length of PART_TABLE_ARRAY)
#      2)   numRequired - number of devices that require a file system type or a
#                         mount point to be assigned/set
#      3)   numAssigned - number of devices that have a value assigned
#      4) numUnassigned - number of devices that need a value to be assigned
#      5)     statDescs - array of strings for the description column in the summary table
#---------------------------------------------------------------------------------------
function getStatsForYAD() {
  local totalDevices=$1
  local numRequired=$2
  local numAssigned=$3
  local numUnassigned=$4
  local statDescs=$5
  local tableRows=(";")
  local row=";'Format' Process Statistics"
  local statVal=0

  if [[ "$DIALOG_BACK_TITLE" =~ "Mount" ]]; then
    row=" ;'Mount' Process Statistics"
  fi
  tableRows+=(";" "$row")

  for colHdr in "${colHdrs[@]}"; do
    case "$colHdr" in
      "Total Devices")
        statVal=${totalDevices}
      ;;
      "Require FST"|"Require Mount Pt")
        statVal=${numRequired}
      ;;
      "Assigned")
        statVal=${numAssigned}
      ;;
      "Unassigned")
        statVal=${numUnassigned}
      ;;
    esac
    row=$(printf "%d;%s" ${statVal} "$colHdr")
    tableRows+=("$row")
  done

  row=$(printf "\n%s" "${tableRows[@]}")
  row=${row:1}

  echo "$row"
}

#---------------------------------------------------------------------------------------
#      METHOD:                      resetFSTs
# DESCRIPTION: Reset/Clear all the file system types. except for the boot loader
#              device, that were assigned by the user.
#---------------------------------------------------------------------------------------
resetFSTs() {
  local row=""
  local cols=()
  local lastIdx=$(expr ${#PART_TABLE_ARRAY[@]} - 1)

  for aryIdx in $(eval echo "{0..$lastIdx}"); do
    row=$(echo -e "${PART_TABLE_ARRAY[$aryIdx]}")

    readarray -t cols <<< "${row//$'\t'/$'\n'}"

    cols[4]=" "
    row=$(printf "\t%s" "${cols[@]}")
    row=${row:1}
    PART_TABLE_ARRAY[$aryIdx]=$(printf "%s" "$row")
  done

  updatePartTable PART_TABLE_ARRAY
}

#---------------------------------------------------------------------------------------
#      METHOD:                  createFileSystems
# DESCRIPTION: Format the logical volumes and partitions with their selected
#              file system type.
#---------------------------------------------------------------------------------------
createFileSystems() {
  local fsCmdKeys=()
  local -A fsCmdTitles=()
  local -A fsCmds=()
  local cmdSplit=()
  local cmdNum=0
  local key=""
  local cmdTitle=""
  local deviceName=""
  local fst=""
  local pbTitle=""

  if [[ "${varMap["PART-LAYOUT"]}" =~ "LVM" ]]; then
    pbTitle="Formatting the Logical Volumes & Partitions with file system"
  else
    pbTitle="Formatting the Partitions with file system"
  fi

  for cmd in "${linuxFmtCmds[@]:${startIdx}}"; do
    cmdNum=$(expr $cmdNum + 1)
    key=$(printf "fsCmd%02d" "$cmdNum")
    cmdSplit=(${cmd})
    if [[ "${cmdSplit[-1]}" =~ "lvm" ]]; then
      deviceName=$(printf "Logical Volume '%s'" "${cmdSplit[-1]}")
    else
      deviceName=$(printf "Partition with device name %s" "${cmdSplit[-1]}")
    fi
    case "${cmdSplit[0]}" in
      "mkswap")
        cmdTitle=$(printf "Setting up Linux 'swap' area for %s" "$deviceName")
      ;;
      *)
        fst=$(echo "${cmdSplit[0]}" | cut -d"." -f2)
        cmdTitle=$(printf "Formatting %s with file system '%s'" "$deviceName" "$fst")
      ;;
    esac
    fsCmdKeys+=("$key")
    fsCmds["$key"]="$cmd"
    fsCmdTitles["$key"]=$(echo "$cmdTitle")
  done

  executeLinuxCommands "$DIALOG_BACK_TITLE" "$pbTitle" fsCmdKeys fsCmdTitles fsCmds
}

#---------------------------------------------------------------------------------------
#      METHOD:                     mountDevices
# DESCRIPTION: Mount the logical volumes & partitions
#---------------------------------------------------------------------------------------
mountDevices() {
  local -A methodParams=(["dialogBackTitle"]="$DIALOG_BACK_TITLE")
  local linuxMountCmds=()
  local prevStep=""

  if [ ! ${varMap["MOUNT-STEP"]+_} ]; then
    resetMountPts
  fi

  setDataForPartitionScheme methodParams

  if [[ "${varMap["PART-LAYOUT"]}" =~ "LVM" ]]; then
    methodParams["deviceType"]="Logical Volume"
  else
    methodParams["deviceType"]="Partition"
  fi

  while true; do
    case "${varMap["MOUNT-STEP"]}" in
      "confMPTs")
        setLinuxMountCmds

        getConfToExecCmds
        case "$inputRetVal" in
          "Edit"|"NO")
            varMap["MOUNT-STEP"]="editMPTs"
          ;;
          "YES")
            varMap["MOUNT-STEP"]="mounting"
          ;;
          *)
            varMap["MOUNT-STEP"]="exit"
            prevStep=""
            EXIT_STATUS=2
          ;;
        esac
      ;;
      "editMPTs")
        prevStep="confMPTs"
        setMountPtForDevices
      ;;
      "exit")
        if [ ${#prevStep} -gt 0 ]; then
          varMap["MOUNT-STEP"]="$prevStep"
        else
          unset varMap["MOUNT-STEP"]
          break
        fi
      ;;
      "mounting")
        executeLinuxMountCmds
        varMap["MOUNT-STEP"]="exit"
        prevStep=""
      ;;
      *)
        break
      ;;
    esac

    cleanDialogFiles
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                        resetMountPts
# DESCRIPTION: Reset the value of the 'Mount Pt' column in the partition table
#---------------------------------------------------------------------------------------
resetMountPts() {
  setRequired
  setMountPtsAuto

  methodParams["statsTable"]=$(getStatsTable)

  local existFlag=$(isUnassigned "${methodParams["statsTable"]}")
  if ${existFlag}; then
    varMap["MOUNT-STEP"]="editMPTs"
  else
    varMap["MOUNT-STEP"]="confMPTs"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                       setMountPtsAuto
# DESCRIPTION: Automatically set the names of the directories/mount pts to the
#              characters after the '/' or 'lvm-' prefixes in the device names
#              column within the partition table.
#---------------------------------------------------------------------------------------
setMountPtsAuto() {
  local updTblFlag=$(echo 'false' && return 1)
  local cols=()
  local guid=""
  local mountPt=""
  local row=""
  local lastIdx=$(expr ${#PART_TABLE_ARRAY[@]} - 1)

  for aryIdx in $(eval echo "{0..$lastIdx}"); do
    row=$(echo -e "${PART_TABLE_ARRAY[$aryIdx]}")

    readarray -t cols <<< "${row//$'\t'/$'\n'}"

    if [[ "$row" =~ "swap" ]]; then  # when the guid is lvm & swap
      guid="8200"
    else
      guid=$(trimString "${cols[3]}")
    fi

    mountPt=$(getMountPoint "$guid")
    if [ ${#mountPt} -gt 0 ] && [ "$mountPt" != "/etc" ] && [ "$mountPt" != "/usr" ]; then
      cols[5]=$(echo "$mountPt")
      row=$(printf "\t%s" "${cols[@]}")
      row=${row:1}
      PART_TABLE_ARRAY[$aryIdx]=$(printf "%s" "$row")
      updTblFlag=$(echo 'true' && return 0)
    fi
  done

  if ${updTblFlag}; then
    updatePartTable PART_TABLE_ARRAY
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                        getMountPoint
# DESCRIPTION: Get the name of the directory to mount the logical volume or partition to
#      RETURN: path to name of directory
#  Required Params:
#      1) guid - string containing the value of the "GUID" column within the partition table
#---------------------------------------------------------------------------------------
function getMountPoint() {
  local guid="$1"
  local mountPt=$(trimString "${cols[5]}")
  local ary=()

  if [ "$guid" == "8200" ]; then
    mountPt="[SWAP]"
  elif [ "$guid" == "EF00" ]; then
    if [ "${varMap[BOOT-DEVICE]}" == "Skip" ]; then
      mountPt="/boot"
    else
      mountPt="$EFI_MNT_PT"
    fi
  elif [ "${mountPt}" == "*" ]; then
    if [[ "$row" =~ "boot" ]]; then
      mountPt="/boot"
    elif [[ "${cols[6]}" =~ "ROOT" ]]; then
      mountPt="/"
    elif [[ "${cols[0]}" =~ "sda" ]]; then
      ary=(${cols[1]})
      if [ "${cols[1]:0:1}" == "/" ]; then
        mountPt=$(echo "${ary[0]}")
      else
        mountPt=$(echo "/${ary[0]}")
      fi
    else
      ary=(${cols[1]:4})
      mountPt=$(echo "/${ary[0]}")
    fi
  fi

  echo "$mountPt"
}

#---------------------------------------------------------------------------------------
#      METHOD:                  setMountPtForDevices
# DESCRIPTION: Set the mount pt for the devices
#  Required Params:
#      1) dialogPartTable - string containing the rows of data for the partition table
#      2)        statsTbl - statistics of the progress made in tabular format
#---------------------------------------------------------------------------------------
setMountPtForDevices() {
  local existFlag=$(echo 'false' && return 1)
  methodParams["dialogTitle"]=$(echo "Select a ${methodParams["deviceType"]} to mount")

  setChoicesForSelectingDevices "Mount"

  while true; do
    selectDeviceToAssignMPT
    case "${mbSelVal}" in
      "Exit")
        varMap["MOUNT-STEP"]="exit"
        break
      ;;
      "Mount")
        existFlag=$(isUnassigned "${methodParams["statsTable"]}")
        if ${existFlag}; then
          dispUnassignedMsg "mount point" "Mount Pt"
        else
          varMap["MOUNT-STEP"]="confMPTs"
          break
        fi
      ;;
      "Reset")
        resetMountPts
      ;;
      *)
        local aryIdx=$(getArrayIndexOfDevice "$mbSelVal")
        if [ "$DIALOG" != "yad" ]; then
          getDirForMPT ${aryIdx}
        fi
        if [ ${#ibEnteredText} -gt 0 ]; then
          varMap["MOUNT-STEP"]="exit"
          updateRowInPartTable ${aryIdx} PART_TABLE_ARRAY 5 "$ibEnteredText"
        fi
      ;;
    esac

    setDataForPartitionScheme methodParams
    methodParams["statsTable"]=$(getStatsTable)

    cleanDialogFiles
  done
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     isValidDirName
# DESCRIPTION: Validates that the directory name entered DOES NOT match any of the
#              essential directories required for booting and cannot be on a separate
#              partition (i.e. '/etc' & '/usr' must be on the same partition as '/')
#      RETURN: 0 - true, 1 - false
#  Required Params:
#      1) deviceName - name of device that was entered
#---------------------------------------------------------------------------------------
function isValidDirName() {
  local invDirNames=("/etc" "/usr")
  for invDirName in "${invDirNames[@]}"; do
    if [ "$ibEnteredText" == "$invDirName" ]; then
      echo 'false' && return 1
    fi
  done

  echo 'true' && return 0
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   isDuplicateDirName
# DESCRIPTION: Validates that the directory name entered DOES NOT match any of the
#              rows in the partition table
#      RETURN: 0 - true, 1 - false
#  Required Params:
#      1) dialogPartTable - string containing the rows of data for the partition
#                           table delimited by '\n'
#---------------------------------------------------------------------------------------
function isDuplicateDirName() {
  local cols=()
  local dirName=""

  for row in "${PART_TABLE_ARRAY[@]}"; do
    readarray -t cols <<< "${row//$'\t'/$'\n'}"

    dirName=$(trimString "${cols[5]}")
    if [ "$dirName" == "$ibEnteredText" ]; then
      echo 'true' && return 0
    fi
  done

  echo 'false' && return 1
}

#---------------------------------------------------------------------------------------
#      METHOD:                   setLinuxMountCmds
# DESCRIPTION: Set the linux commands to mount the devices to their specified directory
#---------------------------------------------------------------------------------------
setLinuxMountCmds() {
  local cols=()
  local cmd=""
  local guid=""
  local rootFlag=$(echo 'false' && return 1)
  local bootFlag=$(echo 'false' && return 1)

  for row in "${PART_TABLE_ARRAY[@]}"; do
    readarray -t cols <<< "${row//$'\t'/$'\n'}"

    guid=$(trimString "${cols[3]}")

    if [[ "$row" =~ "swap" ]]; then
      cmd=$(getFmtMntDevName "${cols[0]}" "${cols[1]}")
      cmd=$(printf "swapon %s" "$cmd")
      linuxMountCmds+=("$cmd")
    elif [ "$guid" == "EF00" ] || [ "$guid" == "8300" ] ||  [ "$guid" == "lvm" ]; then
      cmd=$(getMountCommands "${cols[0]}" "${cols[1]}" "${cols[4]}" "${cols[5]}")
      if [[ "${cols[-1]}" =~ "ROOT" ]]; then
        if [ ${#linuxMountCmds[@]} -lt 1 ]; then
          linuxMountCmds=("$cmd")
        else
          linuxMountCmds=("$cmd" "${linuxMountCmds[@]:0}")
        fi
        rootFlag=$(echo 'true' && return 0)
      elif [ "${cols[5]}" == "/boot" ]; then
        if [ ${#linuxMountCmds[@]} -lt 1 ]; then
          linuxMountCmds=("$cmd")
        elif ${rootFlag}; then
          linuxMountCmds=("${linuxMountCmds[@]:0:1}" "$cmd" "${linuxMountCmds[@]:1}")
        else
          linuxMountCmds=("$cmd" "${linuxMountCmds[@]:0}")
        fi
        bootFlag=$(echo 'true' && return 0)
      elif [ "$guid" == "EF00" ]; then
        if [ ${#linuxMountCmds[@]} -lt 1 ]; then
          linuxMountCmds=("$cmd")
        elif ${rootFlag} && ${bootFlag}; then
          linuxMountCmds=("${linuxMountCmds[@]:0:3}" "$cmd" "${linuxMountCmds[@]:3}")
        elif ${bootFlag}; then
          linuxMountCmds=("${linuxMountCmds[@]:0:1}" "$cmd" "${linuxMountCmds[@]:1}")
        else
          linuxMountCmds=("$cmd" "${linuxMountCmds[@]:0}")
        fi
      elif [ "$guid" == "8300" ] || [ "$guid" == "lvm" ]; then
        linuxMountCmds+=("$cmd")
      fi
    fi
  done
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getMountCommands
# DESCRIPTION: Get the linux commands to mount the logical volume or partition
#      RETURN: concatenated string separated by '\t'
#  Required Params:
#      1) deviceCol - value of the "Device" column from the partition table
#      2)   nameCol - value of the "Name" column from the partition table
#      3)       fst - linux file system type
#      4)   dirName - name of the specified directory to mount the logical volume
#                     or partition to
#---------------------------------------------------------------------------------------
function getMountCommands() {
  local deviceName=$(getFmtMntDevName "$1" "$2")
  local fst=$(trimString "$3")
  local dirName=$(trimString "$4")
  local mountPt=""
  local cmds=()
  local cmd=""

  if [ "$dirName" == "/" ]; then
    mountPt="/mnt"
  else
    mountPt=$(echo "/mnt${dirName}")
  fi

  if [ ${#mountPt} -gt 4 ]; then
    cmd=$(printf "mkdir -p %s" "$mountPt")
    cmds+=("$cmd")
  fi

  cmd=$(printf "mount -t %s %s %s" "$fst" "$deviceName" "$mountPt")
  cmds+=("$cmd")

  cmd=$(printf "\t%s" "${cmds[@]}")
  cmd=${cmd:1}

  echo "$cmd"
}

#---------------------------------------------------------------------------------------
#      METHOD:                executeLinuxMountCmds
# DESCRIPTION: Mount the logical volumes and partitions to their specified directory
#---------------------------------------------------------------------------------------
executeLinuxMountCmds() {
  local mntCmdKeys=()
  local -A mntCmdTitles=()
  local -A mntCmds=()
  local cmdSplit=()
  local cmdNum=1
  local key=$(printf "mntCmd%02d" "$cmdNum")
  local cmd=$(echo "${linuxMountCmds[0]}")
  local cmdTitle=""
  local pbTitle="Mounting the Partitions"

  if [[ "${varMap["PART-LAYOUT"]}" =~ "LVM" ]]; then
    pbTitle="Mounting the Logical Volumes & Partitions"
  fi

  readarray -t cmdSplit <<< "${cmd//$'\t'/$'\n'}"

  if [[ "${cmdSplit[0]}" =~ "sda" ]]; then
    cmdTitle=$(printf "Mounting '/' or root Partition [%s]" "${cmdSplit[0]:${pos}}")
  else
    str=$(printf "%s/dev/%s/" "$str" "${varMap["VG-NAME"]}")
    pos=${#str}
    cmdTitle=$(printf "Mounting '/' or root Logical Volume [%s]" "${cmdSplit[0]:${pos}}")
  fi

  cmd=$(printf ";%s" "${cmdSplit[@]}")
  mntCmdKeys+=("$key")
  mntCmds["$key"]="${cmd:1}"
  mntCmdTitles["$key"]=$(echo "$cmdTitle")

  for cmd in "${linuxMountCmds[@]:1}"; do
    cmdNum=$(expr $cmdNum + 1)
    key=$(printf "mntCmd%02d" "$cmdNum")
    setLinuxCmdAndTitle "$key" "$cmd"
  done

  executeLinuxCommands "$DIALOG_BACK_TITLE" "$pbTitle" mntCmdKeys mntCmdTitles mntCmds
}

#---------------------------------------------------------------------------------------
#      METHOD:                     setLinuxCmdAndTitle
# DESCRIPTION: Set the linux commands to mount the devices and their titles
#  Required Params:
#      1) cmdKey - key mapped to the command & title
#      2)  mtCmd - mount command to execute
#---------------------------------------------------------------------------------------
setLinuxCmdAndTitle() {
  local cmdKey="$1"
  local mtCmd="$2"
  local mtCmdTitle=""
  local cmdArray=()
  local devType="Logical Volume"
  local chars=""
  local charPos=0

  if [[ "$mtCmd" =~ "swapon" ]]; then
    if [[ "$mtCmd" =~ "sda" ]]; then
      devType="Partition"
      chars="swapon "
    else
      chars=$(printf "swapon /dev/%s/" "${varMap["VG-NAME"]}")
    fi
    charPos=${#chars}
    mtCmdTitle=$(printf "Enabling %s [%s] for paging and swapping" "$devType" "${mtCmd:${charPos}}")
  else
    readarray -t cmdArray <<< "${mtCmd//$'\t'/$'\n'}"
    mtCmd=$(printf ";%s" "${cmdArray[@]}")
    mtCmd="${mtCmd:1}"
    if [[ "${cmdArray[0]}" =~ "sda" ]]; then
      devType="Partition"
    fi
    local device="${cmdArray[0]}"
    readarray -t cmdArray <<< "${cmdArray[-1]// /$'\n'}"
    mtCmdTitle=$(printf "Mounting %s [%s] to [%s]" "$devType" "$device" "${cmdArray[-1]}")
  fi

  mntCmdKeys+=("$cmdKey")
  mntCmds["$cmdKey"]="$mtCmd"
  mntCmdTitles["$cmdKey"]=$(echo "$mtCmdTitle")
}

#---------------------------------------------------------------------------------------
#  MAIN
#---------------------------------------------------------------------------------------
main() {
  initVars "$1"

  case "$1" in
    "Format")
      formatDevices
    ;;
    "Mount")
      mountDevices
    ;;
    *)
      clog_error "No case defined for '$1'!"
      exit 1
    ;;
  esac

  local -A updVarMap=()
  for key in "${!varMap[@]}"; do
    updVarMap["$key"]=$(echo "${varMap[$key]}")
  done

  declare -p updVarMap > "${UPD_ASSOC_ARRAY_FILE}"
}

main "$@"
exit $EXIT_STATUS
