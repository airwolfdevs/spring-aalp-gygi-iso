#!/bin/bash

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================
#===============================================================================
# Shared Globals
#===============================================================================
declare BASE_DIR="${PWD}"
declare DONE_FLAG="      ☒"
declare DATA_DIR="$BASE_DIR/data"
declare ASSOC_ARRAY_FILE="$DATA_DIR/.assoc-array-file.out"
declare UPD_ASSOC_ARRAY_FILE="$DATA_DIR/.assoc-array-file.upd"
declare PARTITION_TABLE_FILE="$DATA_DIR/.partition-table.out"
declare LOGICAL_VOL_PREFIX="lvm-"
declare BOOT_LV_NAME="${LOGICAL_VOL_PREFIX}boot"
declare ROOT_LV_NAME="${LOGICAL_VOL_PREFIX}root"
declare BOOT_PART_NAME="boot-part"
declare LVM_PART_NAME="Linux LVM"
declare LVM_VOLUME_GROUP="SysVolGrp"
declare ROOT_MOUNTPOINT="/mnt"
declare EFI_MNT_PT="/boot/uefi"
declare HOME_PART_SIZE="30 GiB"
declare MIN_PART_SIZE="20 GiB"
declare ROOT_HR_SIZES=("15 GiB" "$MIN_PART_SIZE" "$HOME_PART_SIZE")
declare PART_PROC_TYPE_CHOICES=("semiAuto" "manPart")
declare LVM_DEVICE_PREFIX="  ├─"
declare LAST_LVM_PREFIX="  └─"
declare LUKS_CONTAINER_NAME="cryptLVMonLUKS"
declare INSTALLER_LOG="${DATA_DIR}/aalp-gygi.log"
declare TRIM=0
declare PSEUDO_GRAPHICAL_PATH="$BASE_DIR/inc/pseudo-graphical"
declare YAD_GRAPHICAL_PATH="$BASE_DIR/inc/yad-graphical"
declare GRAPHICAL_PATH=""
declare DIALOG_BORDER=""
declare DIALOG_BORDER_MAX=""
declare URL_REF_BORDER=""
declare NOTE_BORDER=""
declare FORM_FLD_SEP="|||"
declare PART_DIALOG_IMAGE="partition-logo.png"
declare PART_WINDOW_ICON="partition-app-logo.png"
declare DIALOG_IMG_ICON_FILE="/tmp/yad-dialog-images.csv"
declare -A PART_TBL_TYPES=( ["gpt"]="GUID Partition Table (GPT)" ["msdos"]="Master Boot Record (MBR)"  )
declare DEVICE_MANAGEMENT_OPTS=("addCustom" "addSwap" "addVAR" "remove" "remSwapFile")
declare INVALID_DEVICE_NAMES=("boot" "home" "root" "swap" "var")
declare INIT_VOL_GROUP="VolGroup00"
declare -A ISO_CODE_NAMES=(
    ["AF"]="Afghanistan" ["AX"]="Åland Islands" ["AL"]="Albania" ["DZ"]="Algeria"
    ["AS"]="American Samoa" ["AD"]="Andorra" ["AO"]="Angola" ["AI"]="Anguilla"
    ["AQ"]="Antarctica" ["AG"]="Antigua and Barbuda" ["AR"]="Argentina" ["AM"]="Armenia"
    ["AW"]="Aruba" ["AU"]="Australia" ["AT"]="Austria" ["AZ"]="Azerbaijan"
    ["BS"]="Bahamas" ["BH"]="Bahrain" ["BD"]="Bangladesh" ["BB"]="Barbados"
    ["BY"]="Belarus" ["BE"]="Belgium" ["BZ"]="Belize" ["BJ"]="Benin"
    ["BM"]="Bermuda" ["BT"]="Bhutan" ["BO"]="Bolivia, Plurinational State of" ["BQ"]="Bonaire, Sint Eustatius and Saba"
    ["BA"]="Bosnia and Herzegovina" ["BW"]="Botswana" ["BV"]="Bouvet Island" ["BR"]="Brazil"
    ["IO"]="British Indian Ocean Territory" ["BN"]="Brunei Darussalam" ["BG"]="Bulgaria" ["BF"]="Burkina Faso"
    ["BI"]="Burundi" ["KH"]="Cambodia" ["CM"]="Cameroon" ["CA"]="Canada"
    ["CV"]="Cape Verde" ["KY"]="Cayman Islands" ["CF"]="Central African Republic" ["TD"]="Chad"
    ["CL"]="Chile" ["CN"]="China" ["CX"]="Christmas Island" ["CC"]="Cocos (Keeling) Islands"
    ["CO"]="Colombia" ["KM"]="Comoros" ["CG"]="Congo" ["CD"]="Congo, the Democratic Republic of the"
    ["CK"]="Cook Islands" ["CR"]="Costa Rica" ["CI"]="Côte d'Ivoire" ["HR"]="Croatia"
    ["CU"]="Cuba" ["CW"]="Curaçao" ["CY"]="Cyprus" ["CZ"]="Czech Republic"
    ["DK"]="Denmark" ["DJ"]="Djibouti" ["DM"]="Dominica" ["DO"]="Dominican Republic"
    ["EC"]="Ecuador" ["EG"]="Egypt" ["SV"]="El Salvador" ["GQ"]="Equatorial Guinea"
    ["ER"]="Eritrea" ["EE"]="Estonia" ["ET"]="Ethiopia" ["FK"]="Falkland Islands (Malvinas)"
    ["FO"]="Faroe Islands" ["FJ"]="Fiji" ["FI"]="Finland" ["FR"]="France"
    ["GF"]="French Guiana" ["PF"]="French Polynesia" ["TF"]="French Southern Territories" ["GA"]="Gabon"
    ["GM"]="Gambia" ["GE"]="Georgia" ["DE"]="Germany" ["GH"]="Ghana"
    ["GI"]="Gibraltar" ["GR"]="Greece" ["GL"]="Greenland" ["GD"]="Grenada"
    ["GP"]="Guadeloupe" ["GU"]="Guam" ["GT"]="Guatemala" ["GG"]="Guernsey"
    ["GN"]="Guinea" ["GW"]="Guinea-Bissau" ["GY"]="Guyana" ["HT"]="Haiti"
    ["HM"]="Heard Island and McDonald Islands" ["VA"]="Holy See (Vatican City State)" ["HN"]="Honduras" ["HK"]="Hong Kong"
    ["HU"]="Hungary" ["IS"]="Iceland" ["IN"]="India" ["ID"]="Indonesia"
    ["IR"]="Iran, Islamic Republic of" ["IQ"]="Iraq" ["IE"]="Ireland" ["IM"]="Isle of Man"
    ["IL"]="Israel" ["IT"]="Italy" ["JM"]="Jamaica" ["JP"]="Japan"
    ["JE"]="Jersey" ["JO"]="Jordan" ["KZ"]="Kazakhstan" ["KE"]="Kenya"
    ["KI"]="Kiribati" ["KP"]="Korea, Democratic People's Republic of" ["KR"]="Korea, Republic of" ["KW"]="Kuwait"
    ["KG"]="Kyrgyzstan" ["LA"]="Lao People's Democratic Republic" ["LV"]="Latvia" ["LB"]="Lebanon"
    ["LS"]="Lesotho" ["LR"]="Liberia" ["LY"]="Libya" ["LI"]="Liechtenstein"
    ["LT"]="Lithuania" ["LU"]="Luxembourg" ["MO"]="Macao" ["MK"]="Macedonia, the Former Yugoslav Republic of"
    ["MG"]="Madagascar" ["MW"]="Malawi" ["MY"]="Malaysia" ["MV"]="Maldives"
    ["ML"]="Mali" ["MT"]="Malta" ["MH"]="Marshall Islands" ["MQ"]="Martinique"
    ["MR"]="Mauritania" ["MU"]="Mauritius" ["YT"]="Mayotte" ["MX"]="Mexico"
    ["FM"]="Micronesia, Federated States of" ["MD"]="Moldova, Republic of" ["MC"]="Monaco" ["MN"]="Mongolia"
    ["ME"]="Montenegro" ["MS"]="Montserrat" ["MA"]="Morocco" ["MZ"]="Mozambique"
    ["MM"]="Myanmar" ["NA"]="Namibia" ["NR"]="Nauru" ["NP"]="Nepal"
    ["NL"]="Netherlands" ["NC"]="New Caledonia" ["NZ"]="New Zealand" ["NI"]="Nicaragua"
    ["NE"]="Niger" ["NG"]="Nigeria" ["NU"]="Niue" ["NF"]="Norfolk Island"
    ["MP"]="Northern Mariana Islands" ["NO"]="Norway" ["OM"]="Oman" ["PK"]="Pakistan"
    ["PW"]="Palau" ["PS"]="Palestine, State of" ["PA"]="Panama" ["PG"]="Papua New Guinea"
    ["PY"]="Paraguay" ["PE"]="Peru" ["PH"]="Philippines" ["PN"]="Pitcairn"
    ["PL"]="Poland" ["PT"]="Portugal" ["PR"]="Puerto Rico" ["QA"]="Qatar"
    ["RE"]="Réunion" ["RO"]="Romania" ["RU"]="Russian Federation" ["RW"]="Rwanda"
    ["BL"]="Saint Barthélemy" ["SH"]="Saint Helena, Ascension and Tristan da Cunha" ["KN"]="Saint Kitts and Nevis" ["LC"]="Saint Lucia"
    ["MF"]="Saint Martin (French part)" ["PM"]="Saint Pierre and Miquelon" ["VC"]="Saint Vincent and the Grenadines" ["WS"]="Samoa"
    ["SM"]="San Marino" ["ST"]="Sao Tome and Principe" ["SA"]="Saudi Arabia" ["SN"]="Senegal"
    ["RS"]="Serbia" ["SC"]="Seychelles" ["SL"]="Sierra Leone" ["SG"]="Singapore"
    ["SX"]="Sint Maarten (Dutch part)" ["SK"]="Slovakia" ["SI"]="Slovenia" ["SB"]="Solomon Islands"
    ["SO"]="Somalia" ["ZA"]="South Africa" ["GS"]="South Georgia and the South Sandwich Islands" ["SS"]="South Sudan"
    ["ES"]="Spain" ["LK"]="Sri Lanka" ["SD"]="Sudan" ["SR"]="Suriname"
    ["SJ"]="Svalbard and Jan Mayen" ["SZ"]="Swaziland" ["SE"]="Sweden" ["CH"]="Switzerland"
    ["SY"]="Syrian Arab Republic" ["TW"]="Taiwan, Province of China" ["TJ"]="Tajikistan" ["TZ"]="Tanzania, United Republic of"
    ["TH"]="Thailand" ["TL"]="Timor-Leste" ["TG"]="Togo" ["TK"]="Tokelau"
    ["TO"]="Tonga" ["TT"]="Trinidad and Tobago" ["TN"]="Tunisia" ["TR"]="Turkey"
    ["TM"]="Turkmenistan" ["TC"]="Turks and Caicos Islands" ["TV"]="Tuvalu" ["UG"]="Uganda"
    ["UA"]="Ukraine" ["AE"]="United Arab Emirates" ["GB"]="United Kingdom" ["UK"]="United Kingdom"
    ["US"]="United States" ["UM"]="United States Minor Outlying Islands" ["UY"]="Uruguay" ["UZ"]="Uzbekistan"
    ["VU"]="Vanuatu" ["VE"]="Venezuela, Bolivarian Republic of" ["VN"]="Viet Nam" ["VG"]="Virgin Islands, British"
    ["VI"]="Virgin Islands, U.S." ["WF"]="Wallis and Futuna" ["EH"]="Western Sahara" ["YE"]="Yemen"
    ["ZM"]="Zambia" ["ZW"]="Zimbabwe")

declare -A ISO_CTRY_NAME_CODE=(
    ["Georgia"]="GE" ["American Samoa"]="AS" ["Grenada"]="GD" ["Argentina"]="AR" ["United States Minor Outlying Islands"]="UM"
    ["Guernsey"]="GG" ["Antarctica"]="AQ" ["Nicaragua"]="NI" ["French Guiana"]="GF"
    ["Gabon"]="GA" ["Aruba"]="AW" ["Norway"]="NO" ["Netherlands"]="NL" ["Australia"]="AU"
    ["United Kingdom"]="GB" ["Austria"]="AT" ["Uganda"]="UG" ["Gambia"]="GM" ["New Caledonia"]="NC"
    ["Greenland"]="GL" ["Azerbaijan"]="AZ" ["Croatia"]="HR" ["Namibia"]="NA" ["Guinea"]="GN"
    ["Åland Islands"]="AX" ["Norfolk Island"]="NF" ["Haiti"]="HT" ["Gibraltar"]="GI" ["Nigeria"]="NG"
    ["Hungary"]="HU" ["Ghana"]="GH" ["Ukraine"]="UA" ["Niger"]="NE" ["Oman"]="OM"
    ["New Zealand"]="NZ" ["Guam"]="GU" ["Guatemala"]="GT" ["Guinea-Bissau"]="GW" ["Hong Kong"]="HK"
    ["Equatorial Guinea"]="GQ" ["Antigua and Barbuda"]="AG" ["Uzbekistan"]="UZ" ["Heard Island and McDonald Islands"]="HM" ["Guadeloupe"]="GP"
    ["Afghanistan"]="AF" ["Uruguay"]="UY" ["United Arab Emirates"]="AE" ["South Georgia and the South Sandwich Islands"]="GS" ["Honduras"]="HN"
    ["Greece"]="GR" ["Andorra"]="AD" ["Nauru"]="NR" ["Nepal"]="NP" ["Anguilla"]="AI"
    ["United States"]="US" ["Guyana"]="GY" ["Angola"]="AO" ["Armenia"]="AM" ["Niue"]="NU"
    ["Albania"]="AL" ["Cyprus"]="CY" ["Burkina Faso"]="BF" ["Saint Vincent and the Grenadines"]="VC" ["Christmas Island"]="CX"
    ["Bulgaria"]="BG" ["Puerto Rico"]="PR" ["Bangladesh"]="BD" ["Palestine, State of"]="PS" ["Holy See (Vatican City State)"]="VA"
    ["Czech Republic"]="CZ" ["Belgium"]="BE" ["Portugal"]="PT" ["British Indian Ocean Territory"]="IO" ["Barbados"]="BB"
    ["Virgin Islands, British"]="VG" ["India"]="IN" ["Samoa"]="WS" ["Isle of Man"]="IM" ["Venezuela, Bolivarian Republic of"]="VE"
    ["Palau"]="PW" ["Israel"]="IL" ["Bosnia and Herzegovina"]="BA" ["Brunei Darussalam"]="BN" ["Paraguay"]="PY"
    ["Bolivia, Plurinational State of"]="BO" ["Saint Barthélemy"]="BL" ["Virgin Islands, U.S."]="VI" ["Costa Rica"]="CR" ["Bermuda"]="BM"
    ["Viet Nam"]="VN" ["Cuba"]="CU" ["Benin"]="BJ" ["Japan"]="JP" ["Ireland"]="IE"
    ["Curaçao"]="CW" ["Bahrain"]="BH" ["Indonesia"]="ID" ["Cape Verde"]="CV" ["Burundi"]="BI"
    ["Côte d'Ivoire"]="CI" ["Bouvet Island"]="BV" ["Switzerland"]="CH" ["Panama"]="PA" ["Jordan"]="JO"
    ["Botswana"]="BW" ["Cook Islands"]="CK" ["Bhutan"]="BT" ["Wallis and Futuna"]="WF" ["Jamaica"]="JM"
    ["Cameroon"]="CM" ["Brazil"]="BR" ["Peru"]="PE" ["Chile"]="CL" ["Bahamas"]="BS"
    ["French Polynesia"]="PF" ["Colombia"]="CO" ["Vanuatu"]="VU" ["Papua New Guinea"]="PG" ["China"]="CN"
    ["Bonaire, Sint Eustatius and Saba"]="BQ" ["Philippines"]="PH" ["Iceland"]="IS" ["Canada"]="CA" ["Iran, Islamic Republic of"]="IR"
    ["Iraq"]="IQ" ["Cocos (Keeling) Islands"]="CC" ["Pakistan"]="PK" ["Jersey"]="JE" ["Poland"]="PL"
    ["Belize"]="BZ" ["Saint Pierre and Miquelon"]="PM" ["Congo, the Democratic Republic of the"]="CD" ["Pitcairn"]="PN" ["Congo"]="CG"
    ["Italy"]="IT" ["Central African Republic"]="CF" ["Belarus"]="BY" ["Rwanda"]="RW" ["Korea, Democratic People's Republic of"]="KP"
    ["Dominica"]="DM" ["Qatar"]="QA" ["Russian Federation"]="RU" ["Korea, Republic of"]="KR" ["Dominican Republic"]="DO"
    ["Serbia"]="RS" ["Kuwait"]="KW" ["Djibouti"]="DJ" ["Denmark"]="DK" ["Cayman Islands"]="KY"
    ["Germany"]="DE" ["Kazakhstan"]="KZ" ["Ethiopia"]="ET" ["Spain"]="ES" ["Eritrea"]="ER"
    ["Mayotte"]="YT" ["Réunion"]="RE" ["Kenya"]="KE" ["Kyrgyzstan"]="KG" ["Algeria"]="DZ"
    ["Western Sahara"]="EH" ["Kiribati"]="KI" ["Egypt"]="EG" ["Romania"]="RO" ["Cambodia"]="KH"
    ["Estonia"]="EE" ["Ecuador"]="EC" ["Comoros"]="KM" ["Yemen"]="YE" ["Saint Kitts and Nevis"]="KN"
    ["Slovenia"]="SI" ["Malawi"]="MW" ["France"]="FR" ["Saint Helena, Ascension and Tristan da Cunha"]="SH" ["Maldives"]="MV"
    ["Slovakia"]="SK" ["Mauritius"]="MU" ["Zambia"]="ZM" ["Svalbard and Jan Mayen"]="SJ" ["Malta"]="MT"
    ["San Marino"]="SM" ["Montserrat"]="MS" ["Sierra Leone"]="SL" ["Mauritania"]="MR" ["Lao People's Democratic Republic"]="LA"
    ["Tanzania, United Republic of"]="TZ" ["Somalia"]="SO" ["Martinique"]="MQ" ["Lebanon"]="LB" ["Senegal"]="SN"
    ["Saint Lucia"]="LC" ["Northern Mariana Islands"]="MP" ["Trinidad and Tobago"]="TT" ["Saudi Arabia"]="SA" ["Tuvalu"]="TV"
    ["Seychelles"]="SC" ["Taiwan, Province of China"]="TW" ["Solomon Islands"]="SB" ["Sweden"]="SE" ["Sudan"]="SD"
    ["Mozambique"]="MZ" ["Liechtenstein"]="LI" ["Turkey"]="TR" ["Singapore"]="SG" ["Malaysia"]="MY"
    ["Sri Lanka"]="LK" ["South Africa"]="ZA" ["Mexico"]="MX" ["Timor-Leste"]="TL" ["Syrian Arab Republic"]="SY"
    ["Madagascar"]="MG" ["Lithuania"]="LT" ["Turkmenistan"]="TM" ["Sint Maarten (Dutch part)"]="SX" ["Saint Martin (French part)"]="MF"
    ["Luxembourg"]="LU" ["Tunisia"]="TN" ["Montenegro"]="ME" ["Latvia"]="LV" ["Tonga"]="TO"
    ["Swaziland"]="SZ" ["Moldova, Republic of"]="MD" ["Thailand"]="TH" ["Monaco"]="MC" ["Tajikistan"]="TJ"
    ["Morocco"]="MA" ["Liberia"]="LR" ["Tokelau"]="TK" ["Lesotho"]="LS" ["Macao"]="MO"
    ["Fiji"]="FJ" ["Chad"]="TD" ["Zimbabwe"]="ZW" ["Mongolia"]="MN" ["Falkland Islands (Malvinas)"]="FK"
    ["South Sudan"]="SS" ["Myanmar"]="MM" ["French Southern Territories"]="TF" ["Togo"]="TG" ["Suriname"]="SR"
    ["Mali"]="ML" ["Finland"]="FI" ["Macedonia, the Former Yugoslav Republic of"]="MK" ["Sao Tome and Principe"]="ST" ["Libya"]="LY"
    ["Faroe Islands"]="FO" ["Turks and Caicos Islands"]="TC" ["Micronesia, Federated States of"]="FM" ["Marshall Islands"]="MH" ["El Salvador"]="SV" )

declare JSON_PROCESSOR="jq-linux64"
declare IP_KEY_NAME="IP Address"
declare CTRY_KEY_NAME="Country Code"
declare TZ_FILE_NAME="${DATA_DIR}/timeZone.out"
declare PART_LAYOUTS=("Discrete Partitions" "LVM" "LVM w/ dm-crypt")
declare PART_LAYOUT_TEXT_FILE="${DATA_DIR}/partitionScheme.txt"
declare -A LINUX_KERNEL_NAMES=(["linux"]="Stable" ["linux-hardened"]="Hardened"
  ["linux-lts"]="Long-term support" ["linux-zen"]="ZEN Kernel")
declare VAR_KEY_NAMES=(
"step#1" "KEYMAP" "BOOT-MODE" "CONNECTION-TYPE" "SYS-CLOCK" "PART-LAYOUT" "FMT-FS" "MNT-FS"
"step#2" "INST-BASE-SYS" "CONFIG-MLIST" 
"step#3" "CONFIG-FSTAB" "Time Zone" "CONFIG-LOCALE" "CONFIG-HOST" "INIT-RAM-FS" "SET-ROOT-PSWD" "INST-BOOT-LOADER")

declare TRIZEN_USER="trizenuser"
declare SEL_AUR_PCKGS="${DATA_DIR}/software/selectedPackages.txt"
declare POST_INST_CMD_FILE="${DATA_DIR}/software/execCommands.txt"
declare POST_ASSOC_ARRAY_FILE="$DATA_DIR/.post-assoc-array-file.out"
declare EXPECTED_NAMES=("./software-install/software-installer.sh" "./install-aur-package.sh")
declare POST_INST_DIALOG_BACK_TITLE="Step #4:  Post-installation"
declare POST_INST_YAD_TABS="${DATA_DIR}/software/post-inst-yad.tabs"
declare POST_INST_CHKL_KEY="POST_INST_CHECKLIST"
declare AUR_INSTALLED_PCKGS="${DATA_DIR}/software/aurInstalledPackages.txt"

declare -A BOOT_LOADER_FSTS=(
  ["Grub2"]="btrfs|ext4|reiserfs|vfat|xfs"
  ["rEFInd"]="btrfs|ext4|reiserfs|vfat"
  ["Syslinux"]="btrfs|ext3|ext4|reiserfs|vfat|xfs"
  ["systemd-boot"]="vfat"
)

#### DO NOT REMOVE.  Will cause reference issues!!!!
declare EMPTY_CHKL=()

#### Set within the .bashrc file

[[ -z "${AALP_GYGI_YAD}" ]] && GRAPHICAL_PATH="$PSEUDO_GRAPHICAL_PATH" || GRAPHICAL_PATH="$YAD_GRAPHICAL_PATH"

if [[ -z "${AALP_GYGI_YAD}" ]]; then
  source "$GRAPHICAL_PATH/dialog-funcs.sh"
else
  source "$GRAPHICAL_PATH/yad-funcs.sh"
fi

if [[ -z $DASH_LINE_WIDTH_80 ]]; then
  DIALOG_BORDER="$BORDER_WIDTH_600"
  DIALOG_BORDER_MAX="$BORDER_WIDTH_MAX"
  URL_REF_BORDER="---------------------"
  NOTE_BORDER="---------"
else
  DIALOG_BORDER="$DASH_LINE_WIDTH_80"
  DIALOG_BORDER_MAX="$DASH_LINE_FULL_WIDTH"
  URL_REF_BORDER="--------------"
  NOTE_BORDER="------"
fi
