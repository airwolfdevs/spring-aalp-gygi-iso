#!/bin/bash

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# globals
#===============================================================================
declare REG_EXP_PATTERNS=('^\/*boot$' '^\/*boot[[:space:]]+' '^(lvm-)*boot$' '^(lvm-)*boot[[:space:]]+'
'^\/*home$' '^\/*home[[:space:]]+' '^(lvm-)*home$' '^(lvm-)*home[[:space:]]+'
'^\/*root$' '^\/*root[[:space:]]+' '^(lvm-)*root$' '^(lvm-)*root[[:space:]]+'
'^\/*swap$' '^\/*swap[[:space:]]+' '^(lvm-)*swap$' '^(lvm-)*swap[[:space:]]+'
'^\/*var$' '^\/*var[[:space:]]+' '^(lvm-)*var$' '^(lvm-)*var[[:space:]]+'
)

#===============================================================================
# functions / methods
#===============================================================================

#---------------------------------------------------------------------------------------
#    FUNCTION:                 appendDeviceNameToPrefix
# DESCRIPTION: Append the name of the device to either '/' or "lvm-"
#      RETURN: concatenated string with $prefix$deviceName
#  Required Params:
#      1)  deviceName - name of device that was entered
#      2)  deviceType - Logical Volume or Partition
#---------------------------------------------------------------------------------------
function appendDeviceNameToPrefix() {
  local deviceName="$1"
  local deviceType="$2"
  local prefix="$LOGICAL_VOL_PREFIX"
  local str="$deviceName"
  local startsWith="${deviceName:0:${#prefix}}"

  if [ "$deviceType" == "Partition" ]; then
    prefix="/"
  fi

  if [ "$prefix" != "$startsWith" ]; then
    str=$(echo "${prefix}$deviceName")
  fi

  echo "$str"
}

#---------------------------------------------------------------------------------------
#      METHOD:                         changeRoot
# DESCRIPTION: Execute the Arch installer script "arch-chroot" to change root and execute
#              a command on the newly installed system
#  Required Params:
#      1)  command to run
#---------------------------------------------------------------------------------------
changeRoot() {
  local debugFlag=$(isDebugOn)
  if ${debugFlag}; then
    eval "$1"
  else
    arch-chroot "$ROOT_MOUNTPOINT" /bin/bash -c "${1}"
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                findPositionForString
# DESCRIPTION: Find the position of the string within the array of dialog choices
#        NOTE: position - 1 is equivalent to the index of the array
#      RETURN: integer containing the default choice
#  Required Params:
#      1) searchArray - array of choices for the dialog
#      2)   searchStr - string value to search within the array of choices
#---------------------------------------------------------------------------------------
function findPositionForString() {
  local -n searchArray=$1
  local searchStr="$2"
  local joinSrchAry=$(printf "\n%s" "${searchArray[@]}")
  joinSrchAry=${joinSrchAry:1}

  local cmdOutput=$(echo -e "$joinSrchAry" | grep -Fxn "$searchStr" | cut -d":" -f 1)

  if [ ${#cmdOutput} -lt 1 ]; then
    cmdOutput="-1"
  fi

  echo ${cmdOutput}
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getTextForDialog
# DESCRIPTION: Concatenates all the string elements into one string and preserves
# the carriage return
#      RETURN: concatenated string
#  Required Params:
#      1) lines - array of lines\rows to be printed within a dialog
#---------------------------------------------------------------------------------------
function getTextForDialog() {
  local lines=("$@")
  local txt=$(printf "\n%s" "${lines[@]}")
  txt=${txt:1}

  printf "%s" "$txt"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    isValidDeviceName
# DESCRIPTION: Validates that the device name entered DOES NOT match any of the
#              regular expressions in the global array "$REG_EXP_PATTERNS"
#      RETURN: 0 - true, 1 - false
#  Required Params:
#      1) deviceName - name of device that was entered
#---------------------------------------------------------------------------------------
function isValidDeviceName() {
  local -r deviceName="${1}"
  local flag=""

  if [ ${#deviceName} -gt 0 ]; then
    for regExp in "${REG_EXP_PATTERNS[@]}"; do
      if [[ $deviceName =~ $regExp ]]; then
        flag="error"
        break
      fi
    done
  else
    flag="error"
  fi

  if [ ${#flag} -lt 1 ]; then
    echo 'true' && return 0
  fi

  echo 'false' && return 1
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  isDuplicateDeviceName
# DESCRIPTION: Validates that the device name entered DOES NOT match any of the
#              rows in the partition table
#      RETURN: 0 - true, 1 - false
#  Required Params:
#      1) deviceNames - names of the current logical volumes or partitions
#                       that exist in the partition table separated by '\'
#---------------------------------------------------------------------------------------
function isDuplicateDeviceName() {
  local deviceNames="$1"
  local rows=()

  readarray -t rows <<< "${deviceNames//$'\t'/$'\n'}"
  for row in "${rows[@]}"; do
    if [ "$row" == "$ibEnteredText" ]; then
      echo 'true' && return 0
    fi
  done

  echo 'false' && return 1
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                getTextForNetworkConfig
# DESCRIPTION: Get the text to display in the linux "dialog" for the network configuration
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getTextForNetworkConfig() {
  local warnText=$(getTextForWarning)
  local textArray=("$warnText" "Connection not Found."
  "Ethernet/Wired Device Name:  [$ETHERNET_DEVICE_NAME]"
  "      Wireless Device Name:  [$WIRELESS_DEVICE_NAME]"
  "$DASH_LINE_WIDTH_80" " "
  "Choose how the connection should be configured:"
  )

  local dialogText=$(getTextForDialog "${textArray[@]}")

  echo "$dialogText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getSixDigitRandomNum
# DESCRIPTION: Generates a six digit random number
#      RETURN: integer
#---------------------------------------------------------------------------------------
function getSixDigitRandomNum() {
  local keyPlugin=$((RANDOM * 27 + 100000))
  echo ${keyPlugin}
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                      execAPG
# DESCRIPTION: Create 10 secure random passwords ranging in size between 8-20
#              using the AUR "apg" package
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function execAPG() {
  local rows=()
  local pswdArray=()

  mapfile -t pswdArray < <( apg -a1 -n10 -m8 -x20 -E"\n\t" -M sncl )

  local randomPswds=$(printf "\t%s" "${pswdArray[@]}")
  randomPswds=${randomPswds:1}
  echo "$randomPswds"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     execPWGEN
# DESCRIPTION: Create 10 secure random passwords ranging in size between 8-20
#              using the AUR "pwgen" package
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function execPWGEN() {
  local rows=()
  local pswdArray=()

  for num in {8..20..3}; do
    mapfile -t rows < <( pwgen -1sy --remove-chars="\n" ${num} 2 )
    pswdArray+=("${rows[@]:0}")
  done

  pswdArray=($(printf "%s\n" "${pswdArray[@]}" | shuf))
  local randomPswds=$(printf "\t%s" "${pswdArray[@]}")
  randomPswds=${randomPswds:1}
  echo "$randomPswds"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     formatTextForYAD
# DESCRIPTION: Format the text within the array for a yad "dialog" so that it wraps
#              correctly.
#      RETURN: concatenated string
#  Required Params:
#      1) lines - array of strings
#---------------------------------------------------------------------------------------
function formatTextForYAD() {
  local -n lines=$1
  local textArray=("${lines[0]}")

  for line in "${lines[@]:1}"; do
    textArray+=($(trimString "$line"))
  done
  local text=$(printf " %s" "${textArray[@]}")
  text=${text:1}

  echo "$text"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                         validateIP
# DESCRIPTION: Validate that the ip address is in the correct format and integer boundaries
#      RETURN: 0 - true, 1 -false
#  Required Params:
#      1) ip - the IP address to be validated
#---------------------------------------------------------------------------------------
function validateIP() {
    local  ip=$1
    local  stat=1

    if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
      OIFS=$IFS
      IFS='.'
      ip=($ip)
      IFS=$OIFS
      [[ ${ip[0]} -le 255 && ${ip[1]} -le 255 \
          && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
      stat=$?
    fi

    if [ ${stat} -gt 0 ]; then
      echo 'false' && return 1
    fi

    echo 'true' && return 0
}

#---------------------------------------------------------------------------------------
#      METHOD:                      cleanDialogFiles
# DESCRIPTION: Remove the files generated by the dialog frameworks
#---------------------------------------------------------------------------------------
cleanDialogFiles() {
  if typeset -f removeFilesForYAD > /dev/null; then
    removeFilesForYAD
  else
    rm -rf /tmp/inst-menu*
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                      createRAMDiskEnv
# DESCRIPTION: Execute the linux commands to configure "mkinitcpio" and initialize
#              the RAM disk.
#---------------------------------------------------------------------------------------
createRAMDiskEnv() {
  local linuxKernel=$(getNameOfLinuxKernel)
  local cmd="mkinitcpio -p linux"

  changeRoot "$cmd"
  if [ "$linuxKernel" != "linux" ]; then
    cmd=$(echo "mkinitcpio -p $linuxKernel")
    changeRoot "$cmd"
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                         isRootUser
# DESCRIPTION: Check if the current user is the 'root' user
#      RETURN: 0 - true, 1 -false
#---------------------------------------------------------------------------------------
function isRootUser() {
  if [[ "$(id -u)" != "0" ]]; then
    echo 'false' && return 1
  fi

  echo 'true' && return 0
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     getNameOfNormalUser
# DESCRIPTION: Get the name of the normal user that was created during the post-install
#              step of the AALP-GYGInstaller
#      RETURN: name of normal user
#---------------------------------------------------------------------------------------
function getNameOfNormalUser() {
  local cmd="awk -F: '{print $1\"|||\"$6}' /etc/passwd | grep '/home'"
  local cmdOut=$(changeRoot "$cmd")
  local users=()
  local userName=""

  readarray -t users <<< "$cmdOut"
  for usr in "${users[@]}"; do
    readarray -t path <<< "${usr//$FORM_FLD_SEP/$'\n'}"
    if [ "${path[0]}" != "$TRIZEN_USER" ]; then
      userName="${path[0]}"
      break
    fi
  done

  echo "$userName"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     isVersionInstalled
# DESCRIPTION: Check if the current version of a package matches the version to be installed.
#      RETURN: 0 - true, 1 -false
#  Required Params:
#      1) aurPckg - name of the AUR package
#      1) version - the version to be installed
#---------------------------------------------------------------------------------------
function isVersionInstalled() {
  local version="$2"
  local curVer=$(changeRoot "pacman -Q $1 2>/dev/null")
  local verArray=()
  local sep="."
  readarray -t verArray <<< "${version//$sep/$'\n'}"

  version=$(printf "_%s" "${verArray[@]}")
  version="${version:1}"

  readarray -t verArray <<< "${curVer//$sep/$'\n'}"

  curVer=$(printf "_%s" "${verArray[@]}")
  curVer="${curVer:1}"

  if [[ "$curVer" =~ .*"${version}".* ]]; then
    echo 'true' && return 0
  fi

  echo 'false' && return 1
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     isPackageInstalled
# DESCRIPTION: Check if an AUR package is installed.
#      RETURN: 0 - true, 1 - false
#  Required Params:
#      1) aurPckgName - name of the AUR package
#---------------------------------------------------------------------------------------
function isPackageInstalled() {
  local cmd=$(echo "pacman -Qi $1")

  if changeRoot "$cmd" &> /dev/null; then
    echo 'true' && return 0
  fi
  echo 'false' && return 1
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                          isValZero
# DESCRIPTION: Check if the value is 0
#      RETURN: 0 - true, 1 - false
#  Required Params:
#      1) val - an integer
#---------------------------------------------------------------------------------------
function isValZero() {
  local val=$1
  if [ ${val} -gt -1 ] && [ ${val} -lt 1 ]; then
    echo 'true' && return 0
  fi
  echo 'false' && return 1
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     isSoftwareTaskDone
# DESCRIPTION: Check if the index of the current post installation category/software task
#              is NOT 0.
#      RETURN: 0 - true, 1 - false
#  Required Params:
#      1) checklistRef - name of the variable for the checklist array to reference
#      2)      menuOpt - string containing the category/software task
#---------------------------------------------------------------------------------------
function isSoftwareTaskDone() {
  local -n checklistRef="$1"
  local menuOpt="$2"
  local sep="."

  readarray -t chklistAry <<< "${menuOpt[0]//$sep/$'\n'}"
  local taskIdx=${chklistAry[-1]}

  if isValZero ${checklistRef[$taskIdx]} &> /dev/null; then
    echo 'false' && return 1
  fi
  echo 'true' && return 0
}

#---------------------------------------------------------------------------------------
#      METHOD:                 updateSoftwareTaskChecklist
# DESCRIPTION: Update the associative array "POST_ASSOC_ARRAY" declared in
#              the Post Installation script
#  Required Params:
#      1) checklistRef - name of the variable for the checklist array to update
#      2)          val - value to set the flag to
#                        a) -1 - Skipped
#                        b)  1 - Completed
#   Optional Param:
#      3)      menuOpt - string containing the category/software task
#---------------------------------------------------------------------------------------
updateSoftwareTaskChecklist() {
  local -n checklistRef="$1"
  local menuOpt="${SEL_CHKLIST_OPT[0]}"
  local sep="."

  if [ "$#" -gt 2 ]; then
    menuOpt="$3"
  fi

  readarray -t chklistAry <<< "${menuOpt//$sep/$'\n'}"
  local taskIdx=${chklistAry[-1]}

  checklistRef[$taskIdx]=$2
}

#---------------------------------------------------------------------------------------
#      METHOD:                  updatePostInstAssocArray
# DESCRIPTION: Update the associative array "POST_ASSOC_ARRAY" declared in
#              the Post Installation script
#  Required Params:
#      1) checklistRef - name of the variable for the checklist array to update
#   Optional Param:
#      2) checklistKey - name of the checklist to update in the associative array
#---------------------------------------------------------------------------------------
updatePostInstAssocArray() {
  local -n checklistRef="$1"
  local checklistKey="$1"
  local -A updAssocArray=()
  local isTaskDone=$(echo 'true' && return 0)

  if [ "$#" -gt 1 ]; then
    checklistKey="$2"
  fi

  for chklVal in "${checklistRef[@]:1}"; do
    if isValZero ${chklVal} &> /dev/null; then
      isTaskDone=$(echo 'false' && return 1)
      break
    fi
  done

  if ${isTaskDone}; then
    checklistRef[0]=1
    updatePostInstallationChecklist 1
  fi

  local concatStr=$(printf "${FORM_FLD_SEP}%d" "${checklistRef[@]}")
  POST_ASSOC_ARRAY["$checklistKey"]=$(echo "${concatStr:${#FORM_FLD_SEP}}")

  for key in "${!POST_ASSOC_ARRAY[@]}"; do
    updAssocArray["$key"]=$(echo "${POST_ASSOC_ARRAY[$key]}")
  done

  declare -p updAssocArray > "${UPD_ASSOC_ARRAY_FILE}"
}

#---------------------------------------------------------------------------------------
#      METHOD:               updatePostInstallationChecklist
# DESCRIPTION: Update the current task, as well as the overall step, in the checklist
#              for the post installation step (i.e POST_ASSOC_ARRAY["POST_INST_CHECKLIST"])
#  Required Params:
#      1) piTaskVal - value for the current task where:
#                     a)  1:  task is done
#                     b) -1:  task was skipped
#---------------------------------------------------------------------------------------
updatePostInstallationChecklist() {
  local postInstChecklist=()
  local chklistAry=()
  local sep="."
  local isStepDone=$(echo 'true' && return 0)

  readarray -t chklistAry <<< "${SEL_CHKLIST_OPT[0]//$sep/$'\n'}"
  local taskIdx=${chklistAry[-1]}

  readarray -t postInstChecklist <<< "${POST_ASSOC_ARRAY["$POST_INST_CHKL_KEY"]//$FORM_FLD_SEP/$'\n'}"
  postInstChecklist[$taskIdx]=$1

  for chklVal in "${postInstChecklist[@]}"; do
    if isValZero ${chklVal} &> /dev/null; then
      isStepDone=$(echo 'false' && return 1)
      break
    fi
  done

  if ${isStepDone}; then
    postInstChecklist[0]=1
  fi

  local concatStr=$(printf "${FORM_FLD_SEP}%d" "${postInstChecklist[@]}")
  POST_ASSOC_ARRAY["$POST_INST_CHKL_KEY"]=$(echo "${concatStr:${#FORM_FLD_SEP}}")
}

#---------------------------------------------------------------------------------------
#      METHOD:                     saveCurrentDataSet
# DESCRIPTION: Save the current key & value pairs in the global associative array
#              "piAssocArray" to the associative array "localAssocArray".
#        NOTE: The associative array "localAssocArray" must be declared within
#              the calling method.
#---------------------------------------------------------------------------------------
saveCurrentDataSet() {
  local srcFile=$(printf "${NB_TAB_DATA_FNS["tabData"]}" ${piAssocArray["tabNum"]})
  local path=()
  local sep="/"
  readarray -t path <<< "${srcFile//$sep/$'\n'}"
  path[1]="${DATA_DIR}/software"
  local destFile=$(printf "/%s" "${path[@]:1}")
  destFile=${destFile:1}

  for key in "${!piAssocArray[@]}"; do
    localAssocArray["$key"]=$(echo "${piAssocArray[$key]}")
  done

  setDataForPostInstTabNB
  mv "$srcFile" "$destFile"
}

#---------------------------------------------------------------------------------------
#      METHOD:                     resetToPrevDataSet
# DESCRIPTION: Reset the key & value pairs in the global associative array "piAssocArray"
#              back to what is currently in the associative array "localAssocArray".
#        NOTE: The associative array "localAssocArray" must be declared within
#              the calling method.
#---------------------------------------------------------------------------------------
resetToPrevDataSet() {
  local srcFile=$(printf "${NB_TAB_DATA_FNS["tabData"]}" ${localAssocArray["tabNum"]})
  local path=()
  local sep="/"
  readarray -t path <<< "${srcFile//$sep/$'\n'}"
  path[1]="${DATA_DIR}/software"
  local destFile=$(printf "/%s" "${path[@]:1}")
  destFile=${destFile:1}

  piAssocArray=()

  for key in "${!localAssocArray[@]}"; do
    piAssocArray["$key"]=$(echo "${localAssocArray[$key]}")
  done

  rm -rf "$destFile"
  sed -i '$d' "$POST_INST_YAD_TABS"
}

#---------------------------------------------------------------------------------------
#      METHOD:                          runAsUser
# DESCRIPTION: Run a command as the user
#  Required Params:
#      1)  userName - name of the user
#      2)       cmd -the command to run
#---------------------------------------------------------------------------------------
runAsUser() {
  sudo -u ${1} ${2}
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                       isAURPckgInst
# DESCRIPTION: Use "trizen" to check if an AUR package is installed
#      RETURN: 0 - true, 1 - false
#  Required Params:
#      1) aurPckgName - name of the AUR package
#---------------------------------------------------------------------------------------
function isAURPckgInst() {
  local cmd=$(echo "sudo -u $TRIZEN_USER trizen -Qi $1")

  if changeRoot "$cmd" &> /dev/null; then
    echo 'true' && return 0
  fi
  echo 'false' && return 1
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                      getDefaultOption
# DESCRIPTION: Get the default option to be checked for the task that is neither done
#              nor been skipped.
#      RETURN: string containing the value of the menu option
#---------------------------------------------------------------------------------------
function getDefaultOption() {
  local -n chklArray="${piAssocArray["chklArray"]}"
  local -n optsArray="${piAssocArray["optsArray"]}"
  local lastIdx=$(expr ${#chklArray[@]} - 1)
  local defaultVal="done"

  for aryIdx in $(eval echo "{1..${lastIdx}}"); do
    local idxVal=${chklArray[${aryIdx}]}
    if isValZero ${idxVal} &> /dev/null; then
      aryIdx=$(expr ${aryIdx} - 1)
      defaultVal="${optsArray[${aryIdx}]}"
      break
    fi
  done
  echo "$defaultVal"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                       isReqStepsDone
# DESCRIPTION: Check if the 1st two required steps are done (i.e. "Basic Setup" &
#              "Desktop Configuration")
#      RETURN: 0 - true, 1 -false
#   Optional Param:
#      1) delta - the amount to subtract from the current step # (usually 1)
#---------------------------------------------------------------------------------------
function isReqStepsDone() {
  local chklistAry=()
  local postInstChecklist=()
  local sep="."
  local taskIdx=0

  readarray -t postInstChecklist <<< "${POST_ASSOC_ARRAY["$POST_INST_CHKL_KEY"]//$FORM_FLD_SEP/$'\n'}"
  readarray -t chklistAry <<< "${SEL_CHKLIST_OPT[0]//$sep/$'\n'}"
  if [ ${chklistAry[-1]} -gt 2 ]; then
    taskIdx=2
  elif [ "$#" -gt 0 ]; then
    taskIdx=$(expr ${chklistAry[-1]} - $1)
  else
    taskIdx=${chklistAry[-1]}
  fi

  for aryIdx in $(eval echo "{0..${taskIdx}}"); do
    if [ ${postInstChecklist[${aryIdx}]} -lt 1 ]; then
      echo 'false' && return 1
    fi
  done

  echo 'true' && return 0
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getNameOfLinuxKernel
# DESCRIPTION: Get the name of the Linux kernel that was installed
#      RETURN: string of the Linux kernel
#---------------------------------------------------------------------------------------
function getNameOfLinuxKernel() {
  local linuxBase=()
  readarray -t linuxBase <<< "${varMap["INST-BASE-SYS"]//:/$'\n'}"
  local kernel=$(trimString "${linuxBase[1]}")

  echo "$kernel"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                        isDebugOn
# DESCRIPTION: Checks if debugging has been turned on
#      RETURN: 0 - true, 1 - false
#---------------------------------------------------------------------------------------
function isDebugOn() {
  if [ "$AALP_GYGI_DIALOG" == "debug" ]; then
    echo 'true' && return 0
  elif [ "$AALP_GYGI_YAD" == "debug" ]; then
    echo 'true' && return 0
  fi

  echo 'false' && return 1
}
