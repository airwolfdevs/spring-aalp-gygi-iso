#!/bin/bash
#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================
source "$GRAPHICAL_PATH/conf-time-inc.sh"

#===============================================================================
# globals
#===============================================================================
declare TMP_DIR="/mnt/tmp"
declare ZONE_ARRAY=()
declare SUB_ZONE_ARRAY_FILE="/tmp/subZoneArray.out"
declare ZONE_ARRAY_FILE="/tmp/zoneArray.out"

#===============================================================================
# functions/methods
#===============================================================================

#---------------------------------------------------------------------------------------
#      METHOD:                     configTimeStandard
# DESCRIPTION: Set the time standard to be used by the hardware clock
#  Required Params:
#      1)  keyName - name of the key for global associative array variable "$varMap"
#---------------------------------------------------------------------------------------
configTimeStandard() {
  local keyName="$1"
  local timeStandard=""
  local dialogText=$(getTextForTimeStandardDlg)
  local dialogTitle="Time Standard for Hardware Clock"
  local helpText=$(getHelpTextForTimeStandard)

  showTimeStandardDialog "$DIALOG_BACK_TITLE" "$dialogTitle" "$helpText" "$dialogText"
  if [ ${#inputRetVal} -gt 0 ]; then
    timeStandard="$inputRetVal"
    if [ "$timeStandard" == "localtime" ]; then
      cleanDialogFiles
      dialogTitle="Local Time vs UTC"
      dialogText=$(getTextForLocalTimeWarning)
      showLocalTimeWarning "$DIALOG_BACK_TITLE" "$dialogTitle" "$dialogText"
      if ! ${yesNoFlag}; then
        timeStandard="UTC"
      fi
    fi
    varMap["$keyName"]="$timeStandard"
  fi
  cleanDialogFiles
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getTextForTimeStandardDlg
# DESCRIPTION: Get the text to display in the text area of a dialog when setting the
#              time standard for the hardware clock.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getTextForTimeStandardDlg() {
  local extraDashes=""

  if [ "$DIALOG" == "yad" ]; then
    extraDashes="--------------"
  fi

  local lclTextArray=(  "The installer will install the AUR package 'ntp'.  This uses the Network Time Protocol (NTP) daemon"
  "to synchronize the software/system clock with internet time servers.")
  local dialogText=$(printf " %s" "${lclTextArray[@]}")

  lclTextArray=("In an operating system, the time (clock) is determined by four parts:"
  "    1) Time value" "    2) Time standard" "    3) Time Zone"
  "    4) If applicable, Daylight Savings Time (DST)" "$DIALOG_BORDER" " " "Hardware clock"
  "--------------$extraDashes" "Also known as the Real Time Clock (RTC), it stores the values of:"
  "     1)  Year" "     2)  Month" "     3)  Day" "     4)  Hour" "     5)  Minute" "     6)  Seconds"
  "It isn't capable of storing the time standard, nor if DST is being used." " "
  "System clock" "------------$extraDashes" "Also known as the software clock, it keeps track of:"
  "    1) Time" "    2) Time Zone" "    3) DST if applicable" "$DIALOG_BORDER" " "
  "Time synchronization" "--------------------$extraDashes" "${dialogText:1}" "$DIALOG_BORDER" " "
  )

  if [ "$DIALOG" == "yad" ]; then
    lclTextArray+=("Click either the 'Local Time' or 'UTC' button to set the Time Standard:")
  else
    lclTextArray=("----------------------------------------------------------"
    "           Use 'j' and 'k' keys to scroll"
    "----------------------------------------------------------" " " "${lclTextArray[@]}"
    "Select either <Local> or < UTC > to set the Time Standard:")
  fi

  dialogText=$(getTextForDialog "${lclTextArray[@]}")

  echo "$dialogText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getTextForLocalTimeWarning
# DESCRIPTION: Get the text to display a warning message when the local timescale is
#              is going to be used as the time standard.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getTextForLocalTimeWarning() {
  local lclTextArray=("The Hardware Clock has no concept of a timezone nor DST (Daylight"
  "Savings Time).  Arch Linux handles DST changes transparently ONLY when the Hardware "
  "Clock is kept in the UTC timescale.  A local timescale will cause inconsistent DST"
  "results.  If Arch Linux is:")
  local dialogText=$(printf " %s" "${lclTextArray[@]}")

  if [ "$DIALOG" == "yad" ]; then
    lclTextArray=("${dialogText:1}")
  else
    local warnText=$(getTextForWarning)
    lclTextArray=("$warnText" " " " " "${dialogText:1}")
  fi

  local textArray=("running during a DST change, the time written to the Hardware Clock"
  "will be adjusted for the change.")
  dialogText=$(printf " %s" "${textArray[@]}")
  lclTextArray+=("     * ${dialogText}")

  textArray=("NOT running during a DST change, the time read from the"
  "Hardware Clock will NOT be adjusted for the change.")
  dialogText=$(printf " %s" "${textArray[@]}")
  lclTextArray+=("     * ${dialogText}" "$DIALOG_BORDER" " "
    "Are you sure you want to continue to use the local timescale?" " "
  )
  if [ "$DIALOG" == "yad" ]; then
    lclTextArray+=("Click the:"
      "        \"Use Local\" button to confirm to use the local timescale"
      "                                         OR"
      "        \"Use UTC\" button to switch to use the UTC timescale")
  else
    lclTextArray+=("Select:"
      "        <Use Local> to confirm to use the local timescale"
      "                              OR"
      "        < Use UTC > to switch to use the UTC timescale")
  fi

  dialogText=$(getTextForDialog "${lclTextArray[@]}")

  echo "$dialogText"
}

#---------------------------------------------------------------------------------------
#      METHOD:                         setTimeZone
# DESCRIPTION: Sets the time zone using either the value that automatically determined
#              or entered manually using the dialogs
#  Required Params:
#      1)  keyName - name of the key for global associative array variable "$varMap"
#---------------------------------------------------------------------------------------
setTimeZone() {
  local keyName="$1"
  local autoFlag=$(echo 'true' && return 0)
  local timeZone=""
  local helpText=""
  local dialogTitle=""
  local subZones=()
  local stepNum=1

  yesNoFlag=$(echo 'false' && return 1)

  if [ -f "$TZ_FILE_NAME" ]; then
    timeZone=$(cat "$TZ_FILE_NAME")
    if [ ${#timeZone} -gt 0 ]; then
      stepNum=3
    fi
  fi

  while true; do
    case ${stepNum} in
      1)
        dialogTitle="Time Zone Selection"
        helpText=$(getHelpTextForZone)
        showZoneDialog "$DIALOG_BACK_TITLE" "$dialogTitle" "$helpText"
        if [ ${#mbSelVal} -gt 0 ]; then
          timeZone="$mbSelVal"
          if [ ${SUB_ZONE_ARRAY["$timeZone"]+_} ]; then
            stepNum=$(expr $stepNum + 1)
          else
            stepNum=$(expr $stepNum + 2)
          fi
        else
          stepNum=0
        fi
      ;;
      2)
        readarray -t subZones <<< "${SUB_ZONE_ARRAY["$timeZone"]}"
        if [ ${#subZones[@]} -gt 0 ]; then
          if [ ${#subZones[@]} -lt 2 ]; then
            timeZone=$(printf "%s/%s" "$timeZone" "${subZones[0]}")
            stepNum=$(expr $stepNum + 1)
          else
            dialogTitle="Sub-Zone Selection"
            helpText=$(getHelpTextForSubZone)
            showSubZoneDialog "$DIALOG_BACK_TITLE" "$dialogTitle" "$timeZone" "$helpText"
            if [ ${#mbSelVal} -gt 0 ]; then
              timeZone=$(printf "%s/%s" "$timeZone" "$mbSelVal")
              stepNum=$(expr $stepNum + 1)
            else
              timeZone=""
              stepNum=1
            fi
          fi
        else
          stepNum=$(expr $stepNum + 1)
        fi
      ;;
      3)
        dialogTitle="Confirmation of Time Zone (Zone/Sub-Zone)"
        helpText=$(getHelpTextForTimeZoneConf ${autoFlag} "$timeZone")
        showTimeZoneConf "$DIALOG_BACK_TITLE" "$dialogTitle" "$timeZone" "$helpText" ${autoFlag}
        if ${yesNoFlag}; then
          varMap["$keyName"]="$timeZone"
          stepNum=$(expr $stepNum + 1)
        else
          timeZone=""
          autoFlag=$(echo 'false' && return 1)
          stepNum=1
        fi
      ;;
      *)
        cleanDialogFiles
        break
      ;;
    esac
    cleanDialogFiles
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                  setTimeZoneDataForDialogs
# DESCRIPTION: Set the arrays for zones & their sub-zones based on the options that can
#              be set in the operating system.
#---------------------------------------------------------------------------------------
setTimeZoneDataForDialogs() {
  local zoneArray=()
  local -A SUB_ZONE_ARRAY=()
  local zoneLocArray=()
  local concatStr=""
  local cmdOutput=$(changeRoot "find -L /usr/share/zoneinfo/posix -mindepth 2 -type f -printf \"%P\n\" | sort")
  local tz=""
  local loc=""
  local tzSubZone=()

  while IFS=$'\n' read -r line; do
    IFS='/' read -a tzSubZone <<< "$line"
    prevTZ="${tzSubZone[0]}"
    break
  done <<< "$cmdOutput"

  while IFS=$'\n' read -r line; do
    IFS='/' read -a tzSubZone <<< "$line"
    tz="${tzSubZone[0]}"

    if [ ${#tzSubZone[0]} -gt 2 ]; then
      concatStr=$(printf "/%s" "${tzSubZone[@]:1}")
      tzSubZone=("${tzSubZone[0]}" "${concatStr:1}")
    fi

    if [ "$prevTZ" == "$tz" ]; then
      zoneLocArray+=("${tzSubZone[1]}")
    else
      concatStr=$(printf "\n%s" "${zoneLocArray[@]}")
      SUB_ZONE_ARRAY["$prevTZ"]=$(echo "${concatStr:1}")
      prevTZ="$tz"
      zoneLocArray=("${tzSubZone[1]}")
    fi
  done <<< "$cmdOutput"
  concatStr=$(printf "\n%s" "${zoneLocArray[@]}")
  SUB_ZONE_ARRAY["$prevTZ"]=$(echo "${concatStr:1}")

  zoneArray=($(find -L /usr/share/zoneinfo/posix -maxdepth 1 -type f -printf "%P\n" | sort -n))
  zoneArray+=("${!SUB_ZONE_ARRAY[@]}")
  zoneArray=($(echo "${zoneArray[@]}" | tr ' ' '\n' | sort -u | tr '\n' ' '))

  concatStr=$(printf "\n%s" "${zoneArray[@]}")
  echo "${concatStr:1}" > "${ZONE_ARRAY_FILE}"
  declare -p SUB_ZONE_ARRAY > "${SUB_ZONE_ARRAY_FILE}"
}
