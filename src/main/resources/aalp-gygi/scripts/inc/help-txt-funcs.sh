#!/bin/bash

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getHelpTextForHRF
# DESCRIPTION: Text to display on the help dialog when entering a size of a device
#              in human-readable format
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForHRF() {
  local helpTextArray=(
  "Valid human-readable format is of the form:"
  "[0-9]+[:space:]*[?(B)|(iB)]"
  "WHERE"
  "       [0-9]+ is a number (i.e. 1 or more digits)"
  "       [:space:]* is 0 or more spaces (usually just 1)"
  "       [?(B)|(iB)] is the size unit"
  "             WHERE"
  "                    ? is either K, M, G, or T appended by a 'B' or \"iB\""
  "$DIALOG_BORDER" " "
  "Examples:"
  "---------"
  "10G, 10GB, 10GiB, 10 G, 10 GB, 10 GiB"
  )
  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getHelpTextForBL
# DESCRIPTION: Text to display on the help/more dialog when selecting the
#              type of boot loader.
#      RETURN: concatenated string
#  Required Params:
#      1) pttDesc - description of partition table type
#   Optional Param:
#      2) string containing the rows of data for the partition table
#---------------------------------------------------------------------------------------
function getHelpTextForBL() {
  local pttDesc="$1"
  local helpTextArray=()
  local warnText=$(getTextForWarning)

  if [ "$#" -gt 1 ]; then
    helpTextArray=("$2" " ")
  fi

  helpTextArray+=("URL Reference:" "$URL_REF_BORDER"
  "https://wiki.archlinux.org/index.php/Category:Boot_loaders"
  "$DIALOG_BORDER" " "
  "In order to boot Arch Linux, a Linux-capable boot loader must be installed"
  "to the \"$pttDesc\".  It is the first piece of software"
  "started by the \"${varMap[BOOT-MODE]}\" mode.  Before initiating the boot"
  "process, the boot loader is responsible for loading:"
  "     A)  The kernel with the wanted kernel parameters"
  "     B)  Initial RAM disk."
  "$DIALOG_BORDER" " "
  "Menu Choices:"
  "-----------------------"
  "\"GRUB\" - (GRand Unified Bootloader) is a multi-boot loader."
  "         NOTE:  ONLY one supported with 'LVM'."
  "\"Syslinux\" - is a collection of boot loaders capable of booting from"
  "             drives, CDs, and over the network via PXE (see WARNING below)"
  )

  if ${UEFI_FLAG}; then
    helpTextArray+=("\"systemd-boot\" - previously called gummiboot, is a simple UEFI boot")
    helpTextArray+=("                 manager which executes configured EFI images.")
    helpTextArray+=("\"rEFInd\" - a UEFI boot manager capable of launching EFISTUB kernels.")
    helpTextArray+=("             Platform-neutral designed and simplify booting multiple OSes.")
  fi

  helpTextArray+=(" " "$warnText")
  helpTextArray+=("As of Syslinux 6.03, some of the features of the supported file systems")
  helpTextArray+=("are not supported by the bootloader. For more information, see")
  helpTextArray+=("https://www.syslinux.org/wiki/index.php?title=Filesystem")

  if [ "$DIALOG" == "yad" ]; then
    helpTextArray[2]="https://wiki.archlinux.org/index.php/Category%3ABoot_loaders"
  else
    helpTextArray+=(
      "$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER"
      "  * Select the <View URL> button below to visit the URL mentioned above"
    )
  fi

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:               getHelpTextForConnConfig
# DESCRIPTION: Text to display on the help/more dialog for the network configuration
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForNetworkConfig() {
  local urlRef="https://wiki.archlinux.org/index.php/Network_configuration"
  local lclTextArray=("This process is to setup a temporary connection to the internet so"
    "that a network manager may be installed and automatically configure"
    "the internet connection.")
  local concatStr=$(printf " %s" "${lclTextArray[@]}")
  concatStr=${concatStr:1}
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef"
    "$DIALOG_BORDER" " " "$concatStr" "$DIALOG_BORDER" " "
    "Description of menu options:")
  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("---------------------------------------")
    local lastNum=3
    if [ ${#WIRELESS_DEVICE_NAME} -gt 0 ]; then
      lastNum=4
    fi
    for optNum in $(eval echo "{1..${lastNum}}"); do
      case ${optNum} in
        1)
          lclTextArray=("   * \"Configure Proxy\" - Prompts for the address of the proxy and"
            "configures the connection through the proxy."
          )
        ;;
        2)
          lclTextArray=("   * \"Wired Automatic\" - Starts the \"dhcpcd\" service using the configured"
            "name of the network device")
        ;;
        3)
          lclTextArray=("   * \"Wired Manual\" - Manually enter the IP Address, Submask, and Gateway"
            "and the installer will then enable the connection.")
        ;;
        4)
          lclTextArray=("   * \"Wireless\" - Executes the 'wifi-menu' command if a wireless"
            "connection was found")
        ;;
      esac
      concatStr=$(printf " %s" "${lclTextArray[@]}")
      concatStr=${concatStr:1}
      helpTextArray+=("$concatStr")
    done

  else
    helpTextArray+=("----------------------------"
      "   * \"Wired Automatic\" - Starts the \\\"dhcpcd\\\" service using the configured"
      "                         name of the network device"
      "   *    \"Wired Manual\" - Manually enter the IP Address, Submask, and Gateway"
      "                         and the installer will then enable the connection.")

    if [ ${#WIRELESS_DEVICE_NAME} -gt 0 ]; then
      helpTextArray+=(
        "   *        \"Wireless\" - Executes the 'wifi-menu' command if a wireless"
        "                         connection was found")
    fi

    helpTextArray+=(
      "   * \"Configure Proxy\" - Prompts for the address of the proxy and"
      "                         configures the connection through the proxy."
      "$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER"
      "  * Select the <View URL> button below to visit the URL mentioned above"
    )
  fi


  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:             getHelpTextForSelDeviceSize
# DESCRIPTION: Text to display on the help/more dialog when selecting a
#              size to allocate a partition or logical volume
#      RETURN: concatenated string
#  Required Params:
#      1) partTable - string containing the rows of data for the partition table
#---------------------------------------------------------------------------------------
function getHelpTextForSelDeviceSize() {
  local partTable="$1"
  local minMaxTbl=()
  IFS=$'\n' read -d '' -a minMaxTbl <<< "$2"

  local helpTextArray=(
  "$partTable" " "
  "Selecting the <Other > button allows you to enter a size between the"
  "Minimum & Maximum values!"
  "${minMaxTbl[@]:0:5}"
  )
  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getHelpTextForTool
# DESCRIPTION: Text to display on the help/more dialog when selecting a
#              linux partition program
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForTool() {
  local helpTextArray=()
  local lines=""
  local gdiskLine=""

  if [ "$DIALOG" == "yad" ]; then
    helpTextArray=(
      "\"fdisk\" - Dialog-driven program for creation and manipulation of partition tables."
      "\"gdisk\" - Interactive GUID partition table (GPT) manipulator.")
  else
    helpTextArray=(
      " \"fdisk\" - Dialog-driven program for creation and"
      "           manipulation of partition tables."
      " \"gdisk\" - Interactive GUID partition table (GPT)"
      "           manipulator.")
  fi
  lines=$(getTextForDialog "${helpTextArray[@]}")

  helpTextArray=("URL Reference:" "$URL_REF_BORDER"
  "https://wiki.archlinux.org/index.php/partitioning#Partitioning_tools"
  "$DIALOG_BORDER" " "
  "These Linux Partitioning Tools are used to create and/or manipulate the"
  "device partition tables and partitions:"
  "----------------------------------------------------"
  "\"cfdisk\" - Curses-based variant of fdisk."
  "\"cgdisk\" - Curses-based variant of gdisk."
  "$lines"
  "\"parted\" - (GNU Parted) Terminal partitioning tool."
  "\"sfdisk\" - Scriptable variant of fdisk."
  "\"sgdisk\" - Scriptable variant of gdisk."
  )

  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("\"GParted\" – GTK+ partition editor for graphically managing disk partitions.")
  else
    helpTextArray+=(
      "$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER"
      "  * Select the <View URL> button below to visit the URL mentioned above"
    )
  fi
  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getHelpTextForInstGuide
# DESCRIPTION: Text to display on the help/more dialog for the installation guide
#      RETURN: concatenated string
#   Optional Param:
#      1) label for the extra button on the linux "dialog"
#---------------------------------------------------------------------------------------
function getHelpTextForInstGuide() {
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER"
  "https://wiki.archlinux.org/index.php/installation_guide"
  "$DIALOG_BORDER" " ")

  if [ "$DIALOG" == "yad" ]; then
    setInstGuideTextForYAD
  else
    local lclTextArray=("This is a step and its tasks that needs to be completed.  These"
      "are part of the Installation Guide for installing ArchLinux.")
    local concatStr=$(printf " %s" "${lclTextArray[@]}")
    helpTextArray+=("${concatStr:1}" "$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER"
      "   * ")
    lclTextArray=("Selecting the installation/main steps (i.e. step#1, step#2, or"
      "step#3) will automatically run the tasks below the step that are not checked.")
    concatStr=$(printf " %s" "${lclTextArray[@]}")
    helpTextArray[-1]+="${concatStr:1}"
    helpTextArray+=("   * Select <View URL> below to visit the official installation guide using \"Elinks\"")
    if [ "$1" == "View CHKL" ]; then
      lclTextArray=("Select <View CHKL> on previous screen to view all the steps of the Installation"
        "Guide.  It for the most part follows the same steps as the official guide on the Arch Linux"
        "site (see URL).  The differences between this and the official guide are:")
      concatStr=$(printf " %s" "${lclTextArray[@]}")
      helpTextArray+=("   * ${concatStr:1}" "       A)  " "       B)  ")

      lclTextArray=("The \"Select Editor\" task was added to the installation"
        "step \"Pre-Installation\".")
      concatStr=$(printf " %s" "${lclTextArray[@]}")
      helpTextArray[-2]+="${concatStr:1}"

      lclTextArray=("The \"Chroot\" task was was removed from the installation"
        "step \"Configure the system\" as this is done automatically"
        "by the installer.")
      concatStr=$(printf " %s" "${lclTextArray[@]}")
      helpTextArray[-1]+="${concatStr:1}"
    else
      helpTextArray+=("   * Select <View Opts> on previous screen to view all:"
        "       A)  the steps of the Installation Guide."
        "       B)  the rows configured for the Partition Layout/Scheme")
    fi
  fi

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#      METHOD:                   setInstGuideTextForYAD
# DESCRIPTION: Text to display within the YAD help dialog for the installation guide
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
setInstGuideTextForYAD() {
  local lclTxtArray=("This is the Installation Guide for installing Arch Linux.  It"
    "for the most part follows the same steps as the official guide on the Arch Linux"
    "site (see URL).  The differences between this and the official guide are:")
  local concatStr=$(printf " %s" "${lclTxtArray[@]}")
  helpTextArray+=("${concatStr:1}" "  A)  " "  B)  ")

  lclTxtArray=("The order of the \"Reboot\" step and the \"Post-installation\""
    "step were swapped.")
  concatStr=$(printf " %s" "${lclTxtArray[@]}")
  helpTextArray[-2]+="${concatStr:1}"

  lclTxtArray=("The \"Chroot\" task was was removed from the installation"
    "step \"Configure the system\" as this is done automatically"
    "by the installer.")
  concatStr=$(printf " %s" "${lclTxtArray[@]}")
  helpTextArray[-1]+="${concatStr:1}"

  helpTextArray+=( "$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER"
    "   * " "   * ")

  lclTxtArray=("Selecting the installation/main steps (i.e. step#1, step#2, or"
    "step#3) will automatically run the tasks below the step that are not checked.")
  concatStr=$(printf " %s" "${lclTxtArray[@]}")
  helpTextArray[-2]+="${concatStr:1}"

  lclTxtArray=("Except for the installation/main step \"Pre-Installation\", none"
    "of the tasks for an installation/main step can be executed/run"
    "until the ALL the tasks for the previous installation/ main step have"
    "ran.  As an example, the tasks of the \"Installation\" step can't"
    "run until ALL the tasks of the \"Pre-Installation\" step have ran.")
  concatStr=$(printf " %s" "${lclTxtArray[@]}")
  helpTextArray[-1]+="${concatStr:1}"

  if [ ${INST_GUIDE_CHECKLIST[0]} -gt 0 ]; then
    lclTxtArray=("Click the \"View Part. Tbl\" button to view all the rows configured in the"
    "Partition Table")
    concatStr=$(printf " %s" "${lclTxtArray[@]}")
    helpTextArray+=("   * ${concatStr:1}")
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getHelpTextForKeyboard
# DESCRIPTION: Text to display on the help/more dialog when selecting a
#              keyboard layout
#      RETURN: concatenated string
#  Required Params:
#      1) the default keyboard layout
#      2) dialogNum - the number of the specific dialog that will be rendered
#---------------------------------------------------------------------------------------
function getHelpTextForKeyboard() {
  local dialogNum=$2
  local hdrText=$(getHdrTextForKeyboard)
  local helpTextArray=("$hdrText")
  local lclTextArray=("will display a radio list dialog of keyboard layouts"
    "that are not related to any particular country or language.")
  local otherDisp=$(printf " %s" "${lclTextArray[@]}")

  lclTextArray=("will display a radio list dialog with a list of countries that"
    "have keyboard layouts related to them.")
  local dispText=$(printf " %s" "${lclTextArray[@]}")

  otherDisp="${otherDisp:1}"
  dispText="${dispText:1}"

  case ${dialogNum} in
    1)
      lclTextArray=(
          "       a) A confirmation dialog if there is ONLY 1 keyboard layout that is"
          "       b) A radio list dialog of keyboard layouts that are")
      hdrText=" related to the country chosen"
      lclTextArray[-2]+="$hdrText"
      lclTextArray[-1]+="$hdrText"
      hdrText=$(getTextForDialog "${lclTextArray[@]}")
      if [ "$DIALOG" == "yad" ]; then
        helpTextArray+=("   * Clicking the \"Ok\" button will display either:" "$hdrText"
          "   * Clicking the \"Other\" button " "   * Clicking the \"Exit\" button ")
      else
        helpTextArray+=("   * Selecting < Ok  > will display either:" "$hdrText"
          "   * Selecting <Other> " "   * Selecting <Exit > ")
      fi
      helpTextArray[-2]+="$otherDisp"
      helpTextArray[-1]+="keeps the keyboard layout as '$1'"
    ;;
    2)
      if [ "$DIALOG" == "yad" ]; then
        helpTextArray+=("   * Clicking the \"Ok\" button " "   * Clicking the \"Cancel\" button "
          "   * Clicking the \"Other\" button ")
      else
        helpTextArray+=("   * Selecting <  Ok  > " "   * Selecting <Cancel> "
          "   * Selecting <Other > ")
      fi
      helpTextArray[-3]+="will display a dialog asking to confirm the selection made."
      helpTextArray[-2]+="$dispText"
      helpTextArray[-1]+="$otherDisp"
    ;;
    3)
      if [ "$DIALOG" == "yad" ]; then
        helpTextArray+=("   * Clicking the \"Ok\" button " "   * Clicking the \"Cancel\" button ")
      else
        helpTextArray+=("   * Selecting <  Ok  > " "   * Selecting <Cancel> ")
      fi
      helpTextArray[-2]+="will display a dialog asking to confirm the selection made."
      helpTextArray[-1]+="$dispText"
    ;;
    4)
      if [ "$DIALOG" == "yad" ]; then
        helpTextArray+=("   * Clicking the \"Yes\" button " "   * Clicking the \"No\" button ")
      else
        helpTextArray+=("   * Selecting <Yes> " "   * Selecting <No > ")
      fi
      helpTextArray[-2]+="will save/set the keyboard layout to the value displayed between []."
      helpTextArray[-1]+="$dispText"
    ;;
  esac

  if [ "$DIALOG" != "yad" ]; then
    setTextForELinks
  fi

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getHdrTextForKeyboard
# DESCRIPTION: Text to display in the header of the help/more dialog when selecting a
#              keyboard layout
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHdrTextForKeyboard() {
  local helpTxtArray=("URL Reference:" "$URL_REF_BORDER"
  "https://wiki.archlinux.org/index.php/Keyboard_configuration_in_console"
  "$DIALOG_BORDER" " ")
  local lclTxtArray=("Keyboard mappings (keymaps) for the virtual terminal, console fonts and"
  "console maps are provided by the AUR kbd (Keytable files and keyboard"
  "utilities) package.  This is a dependency of systemd and also provides many"
  "low-level tools for managing the virtual terminal.")
  local concatStr=$(printf " %s" "${lclTxtArray[@]}")

  helpTxtArray+=("${concatStr:1}" "$DIALOG_BORDER" " "
  "The naming conventions of console keymaps are somewhat arbitrary, but usually they are based on: "
  "     >   Language codes: where the language code is the same as its country code "
  "     >    Country codes: where variations of the same language are used in different "
  "     > Keyboard layouts: where the layout is not related to a particular country or ")
  helpTxtArray[-3]+="(e.g. de for German, or fr for French)."
  helpTxtArray[-2]+="countries (e.g.uk for United Kingdom English, or us for United States English)"
  helpTxtArray[-1]+="language (e.g. dvorak for the Dvorak keyboard layout)."
  helpTxtArray+=("$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER")

  local helpText=$(getTextForDialog "${helpTxtArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#      METHOD:                      getTextForELinks
# DESCRIPTION: Text to append to the array variable "helpTextArray" for selecting
#              <View URL> within a linux "dialog"
#---------------------------------------------------------------------------------------
setTextForELinks() {
  local lclTxtArray=("Selecting <View URL> below will display the URL Ref. "
  "in the \"elinks\" feature-rich text mode web browser.")
  local concatStr=$(printf " %s" "${lclTxtArray[@]}")
  helpTextArray+=("   * ${concatStr:1}")
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getHelpTextForEspDevice
# DESCRIPTION: Text to display on the help/more dialog when selecting the size
#              of the EFI system partition to be allocated.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForEspDevice() {
  local helpTextArray=()
  local concatArray=("The EFI system partition (also called ESP) is an OS independent partition that"
                  "acts as the storage place for the EFI bootloaders, applications and drivers to be"
                  "launched by the UEFI firmware.  It is mandatory for UEFI boot.")
  local concatStr=$(printf " %s" "${concatArray[@]}")
  local warnText=$(getTextForWarning)

  helpTextArray+=("URL Reference:" "$URL_REF_BORDER"
  "https://wiki.archlinux.org/index.php/EFI_system_partition"
  "$DIALOG_BORDER" " " "${concatStr:1}" "$DIALOG_BORDER" " " "$warnText")
  concatArray=("When dual-booting, avoid reformatting the ESP, as it may contain files"
              "required to boot other operating systems")
  concatStr=$(printf " %s" "${concatArray[@]}")
  helpTextArray+=("   * ${concatStr:1}")
  concatArray=("The EFI system partition must be a physical partition in the main partition"
              "table of the disk, not under LVM or software RAID etc.")
  concatStr=$(printf " %s" "${concatArray[@]}")
  helpTextArray+=("   * ${concatStr:1}")

  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=( "$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER"
      "   * The human-readable sizes are based on calculations made by the installer."
      "   * Click the \"Form\" button to enter a different size other than the choices being displayed.")
  else
    helpTextArray+=("$DIALOG_BORDER" " " "NOTE:" "-----"
      "  * Select the <View URL> button below to visit the URL mentioned above")
  fi
  helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                getHelpTextForBootDevice
# DESCRIPTION: Text to display on the help/more dialog when selecting the size
#              of the /boot partition or logical volume to be allocated.
#      RETURN: concatenated string
#   Optional Param:
#      1) string containing the rows of data for the partition table
#---------------------------------------------------------------------------------------
function getHelpTextForBootDevice() {
  local helpTextArray=()
  local helpText=$(getTextForBootDevice)

  if [ "$#" -gt 0 ]; then
    helpTextArray=("$1" " ")
  fi

  helpTextArray+=("$helpText")

  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=( "$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER"
      "   * The human-readable sizes are based on calculations made by the installer."
      "   * Click the \"Form\" button to enter a different size other than the choices being displayed.")
  else
    helpTextArray+=("$DIALOG_BORDER" " " "NOTE:" "-----"
      "  * Select the <View URL> button below to visit the URL mentioned above")
  fi
  helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getTextForBootDevice
# DESCRIPTION: Text to display on the help/more dialog when just selecting the
#              /boot logical volume or partition to be formatted.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getTextForBootDevice() {
  local helpTextArray=()
  local warnText=$(getTextForWarning)

  helpTextArray+=("URL Reference:" "$URL_REF_BORDER"
  "https://wiki.archlinux.org/index.php/partitioning#.2Fboot"
  "$DIALOG_BORDER" " "
  "The /boot directory contains the kernel and RAM disk images as well as the"
  "bootloader configuration file and bootloader stages. It also stores data"
  "that is used before the kernel begins executing user-space programs."
  "/boot is not required for normal system operation, but only during boot"
  "and kernel upgrades (when regenerating the initial RAM disk."
  "$DIALOG_BORDER" " " "$warnText"
  "'/boot' cannot reside in LVM when using a boot loader which"
  "does not support LVM; you must create a separate /boot partition and "
  "format it directly. Only 'GRUB' (i.e Grub2) is known to support LVM."
  )

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getHelpTextForRoot
# DESCRIPTION: Text to display on the help/more dialog when dealing with the
#              '/' (i.e. root) partition or logical volume
#      RETURN: concatenated string
#  Required Params:
#      1) string set to either Logical Volume or Partition
#   Optional Param:
#      2) string containing the rows of data for the partition table
#---------------------------------------------------------------------------------------
function getHelpTextForRoot() {
  local helpTextArray=()
  
  if [ "$#" -gt 1 ]; then
    helpTextArray=("$2" " ")
  fi

  helpTextArray+=("URL Reference:" "$URL_REF_BORDER"
  "https://wiki.archlinux.org/index.php/partitioning#.2F"
  "$DIALOG_BORDER" " "
  "The root directory is the top of the hierarchy, the point where the"
  "primary filesystem is mounted and from which all other filesystems stem."
  "All files and directories appear under the root directory /, even if they"
  "are stored on different physical devices. The contents of the root file"
  "system must be adequate to boot, restore, recover, and/or repair the"
  "system. Therefore, certain directories under '/' are not candidates for"
  "separate $1s." " "
  "It traditionally contains the '/usr' directory, which can grow"
  "significantly depending upon how much software is installed. 15–20 GiB"
  "should be sufficient for most users with modern hard disks. If you plan to"
  "use a swap file, the installer will put it here.  You might need to"
  "increase its size."
  )

  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=( "$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER"
      "   * The human-readable sizes are based on calculations made by the installer."
      "   * Click the \"Form\" button to enter a different size other than the choices being displayed.")
  else
    helpTextArray+=("$DIALOG_BORDER" " " "NOTE:" "-----"
      "  * Select the <View URL> button below to visit the URL mentioned above")
  fi

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:             getHelpTextForRootWithWarning
# DESCRIPTION: Text to display on the help/more dialog when dealing with the
#              '/' (i.e. root) partition or logical volume.  This will also
#              contain a warning message in regards to mounting of certain directories.
#      RETURN: concatenated string
#  Required Params:
#      1) string set to either Logical Volume or Partition
#   Optional Param:
#      2) string containing the rows of data for the partition table
#---------------------------------------------------------------------------------------
function getHelpTextForRootWithWarning() {
  local helpText=""
  local warnText=$(getTextForWarning)

  if [ "$#" -gt 1 ]; then
    helpText=$(getHelpTextForRoot "$1" "$2")
  else
    helpText=$(getHelpTextForRoot "$1")
  fi

  local textArray=( "$helpText" " " "$warnText"
  "Directories essential for booting (except for /boot) must be on the same"
  "$1 as '/' or mounted in early userspace by the initramfs."
  "These essential directories are: /etc and /usr"
  )
  local msgText=$(getTextForDialog "${textArray[@]}")

  echo "$msgText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getHelpTextForVarDevice
# DESCRIPTION: Text to display on the help/more dialog when selecting the size
#              of the /var partition or logical volume to be allocated.
#      RETURN: concatenated string
#  Required Params:
#      1) logical volume or partition
#      2) string concatenation of the global array var "varDeviceTextArray" in "partition_funcs.sh"
#   Optional Param:
#      3) string containing the rows of data for the partition table
#---------------------------------------------------------------------------------------
function getHelpTextForVarDevice() {
  local textArray=()
  local helpTextArray=()
  
  if [ "$#" -gt 2 ]; then
    helpTextArray=("$3" " ")
  fi
  
  IFS=$'\n' read -d '' -a textArray <<< "$2"

  textArray[-1]=$(printf "%s %s" "$1" "${textArray[-1]}")
  
  helpTextArray+=("URL Reference:" "$URL_REF_BORDER"
    "https://wiki.archlinux.org/index.php/partitioning#.2Fvar"
    "$DIALOG_BORDER" " " "${textArray[@]:0}"
  )

  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=( "$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER"
      "   * The human-readable sizes are based on calculations made by the installer."
      "   * Click the \"Form\" button to enter a different size other than the choices being displayed.")
  else
    helpTextArray+=("$DIALOG_BORDER" " " "NOTE:" "-----"
      "  * Select the <View URL> button below to visit the URL mentioned above")
  fi

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getHelpTextForPPT
# DESCRIPTION: Text to display on the help/more dialog when selecting a
#              partition process type
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForPPT() {
  local helpTextArray=( "URL Reference:" "$URL_REF_BORDER"
  "https://wiki.archlinux.org/index.php/installation_guide#Partition_the_disks"
  "$DIALOG_BORDER" " "
  "Partitioning a hard drive divides the available space into sections"
  "that can be accessed independently. An entire drive may be allocated"
  "to a single partition, or multiple ones for cases such as "
  "dual-booting, maintaining a swap partition, or to logically separate"
  "data such as audio and video files."
  "$DIALOG_BORDER" " " "Example Layout/Scheme:" "---------------------------------"
  "NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT"
  "sda      8:0    0   500G  0 disk"
  "├─sda1   8:1    0     1M  0 part        <--- GRUB bootloader"
  "├─sda2   8:2    0   250M  0 part /boot"
  "├─sda3   8:3    0    54G  0 part [SWAP]"
  "├─sda4   8:4    0    30G  0 part /      <--- ROOT"
  "├─sda5   8:5    0   100G  0 part /dbhome"
  "├─sda6   8:6    0   100G  0 part /nexus-oss"
  "└─sda7   8:7    0 215.8G  0 part /home"
  "$DIALOG_BORDER" " " "Description of menu options:" "----------------------------------------"
  "\"Semi-Automatic Partitioning\" - This automatically creates certain"
  "       partitions/logical volumes (i.e.root, home, etc.), as well as"
  "       other ones required based on the configuration settings of the"
  "       machine/VM.  It also gives you the flexibility to create other"
  "       partitions as you see fit."
  " "
  "\"Manual Partitioning\" - Allows you the opportunity to partition the"
  "                disk as to your liking by using the following Linux"
  "                partition apps/tools:"
  "     - fdisk" "     - cfdisk" "     - sfdisk" "     - gdisk"
  "     - cgdisk" "     - sgdisk" "     - parted"
  )

  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("     - GParted")
  else
    helpTextArray+=(
      "$DIALOG_BORDER" " " "NOTE:" "-----"
      "  * Select the <View URL> button below to visit the URL mentioned above"
    )
  fi

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:               getHelpTextForAutoPswd
# DESCRIPTION: Text to display on the help/more dialog when selecting the
#              generated password.
#      RETURN: concatenated string
#  Required Params:
#      1) aurPckg - name of the AUR password generator package
#      2)  urlRef - the URL that can be referenced for further documentation
#---------------------------------------------------------------------------------------
getHelpTextForAutoPswd() {
  local aurPckg="$1"
  local urlRef="$2"
  local helpTextArray=(" " "URL Reference:" "$URL_REF_BORDER" "$urlRef"
  "$DIALOG_BORDER" " "
  "The passwords were generated with the AUR package '$aurPckg'"
  "using the following options:")

  case "$aurPckg" in
    "apg")
      helpTextArray+=(
      "    # random character password generation"
      "    # number of passwords = 10"
      "    # minimum length = 8"
      "    # maximum length = 20"
      "    # exclude characters = carriage return character"
      "    # symbolsets specified with mode where:"
      "      > s - use special symbol set"
      "      > n - use numeral symbol set"
      "      > c - use uppercase symbol set "
      "      > l - use lowercase symbol set")
    ;;
    "pwgen")
      helpTextArray+=(
      "    # remove-chars - exclude carriage return character"
      "                 and use the random password generator"
      "    # secure - Generate completely random, hard-to-memorize passwords"
      "    # symbols - Include at least one special character in the password")
    ;;
  esac

  if [ "$DIALOG" = "yad" ]; then
    helpTextArray+=("$DIALOG_BORDER" " "
    "Click the \"Cancel\" button to choose a different option OR"
    "to generate a new set of passwords to choose from"
    )
  else
    helpTextArray+=("$DIALOG_BORDER" " "
    "Select <Cancel > to choose a different option OR"
    "to generate a new set of passwords to choose from"
    "$DIALOG_BORDER" " " "NOTE:" "-----"
    "  * Select the <View URL> button below to visit the URL mentioned above")
  fi

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:             getHelpTextForConfirmation
# DESCRIPTION: Text to display on the help/more dialog when confirming the data
#              for the LUKS encrypted container
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForConfirmation() {
  local helpTextArray=()
  local confirmLine=""
  local cancelLine=""

  if [ "$DIALOG" == "yad" ]; then
    helpTextArray=(
      "Clicking the \"Confirm\" button will save the data for creating the LUKS encrypted"
      "container and move on to the next step, which is running the LVM script.")
    confirmLine=$(getTextForDialog "${helpTextArray[@]}")
    helpTextArray=(
      "Clicking the \"Cancel\" button will remove the data to create the LUKS encrypted"
      "container, set the partition layout/scheme to 'LVM', and move on to the next step,"
      "which is running the LVM script.")
    cancelLine=$(getTextForDialog "${helpTextArray[@]}")
  else
    helpTextArray=(
      "<Confirm> - save the data for creating the LUKS encrypted container and"
      "            move on to the next step, which is running the LVM script.")
    confirmLine=$(getTextForDialog "${helpTextArray[@]}")
    helpTextArray=(
      "<Cancel > - removes the data to create the LUKS encrypted container, sets"
      "            the partition layout/scheme to 'LVM', and move on to the next"
      "            step, which is running the LVM script.")
    cancelLine=$(getTextForDialog "${helpTextArray[@]}")
  fi

  helpTextArray=("The name for the LUKS container is set by the Installer"
    "$DIALOG_BORDER" " " "Options:" "-----------------"
    "$confirmLine" " " "$cancelLine"
  )
  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:              getHelpTextForCurLayout
# DESCRIPTION: Text to display on the help/more dialog when given the choice of
#              keeping or zapping the current partition layout/scheme.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForCurLayout() {
  local helpTextArray=()
  local keepLine=""
  local zapLine=""

  if [ "$DIALOG" == "yad" ]; then
    keepLine="Clicking the \"Keep\" button will keep the current partition layout/scheme intact"
    helpTextArray=("Clicking the \"Zap\" button destroys the GPT and MBR data structures and"
    "the disk can be repartitioned by using one of the following partitioning tools:")
    zapLine=$(getTextForDialog "${helpTextArray[@]}")
  else
    helpTextArray=(
      "< Keep > - the current layout/scheme will remain and the installer moves on"
      "         to next step")
    keepLine=$(getTextForDialog "${helpTextArray[@]}")
    helpTextArray=(
      "< Zap  > - destroy the GPT and MBR data structures and the disk can be "
      "--------------------------------------------------------------------------"
      "           repartitioned by using one of the following partitioning tools:")
    zapLine=$(getTextForDialog "${helpTextArray[@]}")
  fi

  helpTextArray=()
  if [ "$#" -gt 0 ]; then
    helpTextArray+=(" " "$1" " ")
  fi

  helpTextArray+=("Options:" "------------------"
    "$keepLine" " " "$zapLine"
    "     - fdisk" "     - cfdisk" "     - sfdisk" "     - gdisk"
    "     - cgdisk" "     - sgdisk" "     - parted"
  )

  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("     - GParted")
  fi

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                getHelpTextForPswdKB
# DESCRIPTION: Text to display on the help/more dialog when entering a
#              password to encrypt the LVM device
#      RETURN: concatenated string
#  Required Params:
#      1) urlRef - the URL that can be referenced for further documentation
#---------------------------------------------------------------------------------------
function getHelpTextForPswdKB() {
  local urlRef="$1"
  local spacer=""
  local extraDashes=""
  if [ "$DIALOG" == "yad" ]; then
    extraDashes="-------------"
  else
    spacer="    "
  fi
  local helpTextArray=(" " "URL Reference:" "$URL_REF_BORDER" "$urlRef"
  "${DIALOG_BORDER}${extraDashes}" " "
  "Insecure passwords include those containing:"
  "  > Personally identifiable information (e.g., your dog's name, date of"
  "${spacer}birth, area code, favorite video game)" " "
  "  > Simple character substitutions on words (e.g., k1araj0hns0n)" " "
  "  > Root "words" or common strings followed or preceded by added numbers,"
  "${spacer}symbols, or characters (e.g., DG091101%)" " "
  "  > Common phrases or short phrases of grammatically related words"
  "${spacer}(e.g. all of the lights), and even with character substitution."
  "${DIALOG_BORDER}${extraDashes}" " "
  "The right choice for a password is something long (8-20 characters,"
  "depending on importance) and seemingly completely random. A good technique"
  "for building secure, seemingly random passwords is to base them on"
  "characters from every word in a sentence. Take for instance “the girl is"
  "walking down the rainy street” could be translated to t6!WdtR5 or, less"
  "simply, t&6!RrlW@dtR,57. This approach could make it easier to remember a"
  "password, but note that the various letters have very different"
  "probabilities of being found at the start of words.")

  if [ "$DIALOG" != "yad" ]; then
    helpTextArray+=(
      "$DIALOG_BORDER" " " "NOTE:" "-----"
      "  * Select the <View URL> button below to visit the URL mentioned above"
    )
  fi

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:            getHelpTextForSelPartLayout
# DESCRIPTION: Text to display on the help/more dialog when selecting the
#              partition layout/scheme.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForSelPartLayout() {
  local warnText=$(getTextForWarning)
  local helpTextArray=("URL References:" "$URL_REF_BORDER"
  "https://wiki.archlinux.org/index.php/partitioning"
  )

  if [ "$DIALOG" != "yad" ] && [ ${varMap["PART-LAYOUT"]+_} ]; then
    local dialogPartTable=$(getPartitionTableText)
    helpTextArray=("$dialogPartTable" " " "${helpTextArray[@]}")
  fi

  if [ "$#" -gt 0 ]; then
    helpTextArray+=("https://wiki.archlinux.org/index.php/partitioning#Discrete_partitions"
    "https://wiki.archlinux.org/index.php/LVM"
    "https://wiki.archlinux.org/index.php/Dm-crypt"
    "https://wiki.archlinux.org/index.php/Encrypting_an_entire_system#LVM_on_LUKS"
    "$DIALOG_BORDER" " "
    "\"Discrete Partitions\" - separates out a path as a partition to allow for"
    "a different choice of a file system and mount options.  It puts certain"
    "directories on their own separate logical volume or partition under the "
    "'/' or root device." "$warnText"
    "The '/etc' and '/usr [1]' directories are essential for booting.  These"
    "must be on the same partition as '/' or 'root' partition.")
  else
    helpTextArray+=("https://wiki.archlinux.org/index.php/LVM"
    "https://wiki.archlinux.org/index.php/Dm-crypt"
    "https://wiki.archlinux.org/index.php/Encrypting_an_entire_system#LVM_on_LUKS")
  fi

  helpTextArray+=("$DIALOG_BORDER" " "
  "\"Logical Volume Manager\" - (LVM) is a device mapper target that provides"
  "logical volume management for the Linux kernel.  It utilizes the kernel's"
  "device-mapper feature to provide a system of partitions independent of"
  "underlying disk layout.  With LVM you abstract your storage and have"
  "'virtual partitions', making extending/shrinking easier." "$warnText"
  "'/boot' cannot reside in LVM when using a boot loader which does not"
  "support LVM.  A separate /boot partition must be created and format it"
  "directly. Only 'GRUB' (i.e Grub2) is known to support LVM."
  "$DIALOG_BORDER" " "
  "\"Device-Mapper Crypt\" - (dm-crypt) is a transparent disk encryption"
  "subsystem that is the standard encryption functionality provided by the"
  "Linux kernel.  The installer will use the straight-forward approach by:"
  "    1) encrypting the LVM partition first with 'LUKS'"
  "    2) creating the logical volumes on top of the encrypted partition"
  "'LUKS' (Linux Unified Key Setup) is the preferred way to setup"
  "disk encryption with 'dm-crypt'."
  )

  if [ "$DIALOG" != "yad" ]; then
    helpTextArray+=(
      "$DIALOG_BORDER" " " "NOTE:" "-----"
      "  * Select the <View URL> button below to visit the URLs mentioned above"
    )
  fi
  
  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:            getHelpTextForPasswordOption
# DESCRIPTION: Text to display on the help/more dialog when selecting the option
#              to create the password in order to encrypt the LVM device
#      RETURN: concatenated string
#  Required Params:
#      1) urlRef - the URL that can be referenced for further documentation
#---------------------------------------------------------------------------------------
getHelpTextForPasswordOption() {
  local urlRef="$1"
  local helpTextArray=(" " "URL References:" "$URL_REF_BORDER" "$urlRef"
  "$DIALOG_BORDER" " "
  )
  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("Options" "----------------------------------"
    "There aren't any requirements in choosing a password.  The recommendation"
    "is 8-20 characters in length, and seemingly completely random."
    "The Installer will allow you to create one or use one of the automated"
    "tools based on the recommendations"
    "$DIALOG_BORDER-------------" " "
    "\"kb\" - manually enter a password between 8-20 characters" " "
    "\"apg\" - generates 10 random passwords between 8-20 characters"
    "using the 'random character password generator'." " "
    "\"pwgen\" - generates 10 completely random, hard-to-memorize"
    "passwords (secure type) that are between 8-20 characters."
    "$DIALOG_BORDER-------------" " "
    "Clicking \"Cancel\" will change the partition layout/scheme")
  else
    helpTextArray+=("Options" "-------------------"
    "   'kb' - manually enter a password between 8-20 characters"
    "  'apg' - generates 10 random passwords between 8-20 characters using the"
    "          'random character password generator'."
    "'pwgen' - generates 10 completely random, hard-to-memorize passwords"
    "          (secure type) that are between 8-20 characters."
    "$DIALOG_BORDER" " "
    "Selecting <Cancel> will change the partition layout/scheme")
  fi
  helpTextArray+=("From [${PART_LAYOUTS[2]}] To [${PART_LAYOUTS[1]}]")
  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getHelpTextForPTT
# DESCRIPTION: Text to display on the help/more dialog when selecting the
#              Partition Table Type.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForPTT() {
  local extraDashes=""
  if [ "$DIALOG" == "yad" ]; then
    extraDashes="------------"
  fi
  local helpTextArray=( "URL Reference:"  "$URL_REF_BORDER"
  "https://wiki.archlinux.org/index.php/partitioning#Partition_table"
  "$DIALOG_BORDER"
  "\"${PART_TBL_TYPES[gpt]}\""
  "An alternative, contemporary, partitioning style that is intended to"
  "replace MBR."
  "Some advantages:"
  "  - partitioning and boot data is more robust and can be recovered if the"
  "    data becomes corrupted"
  "  - supports up to 128 partitions"
  "  - size of hard drive can be supported up to 2 ZiB"
  " "
  "\"${PART_TBL_TYPES[msdos]}\""
  "Limited by:"
  "   - hard drive size of up to 2 TB"
  "   - supports ONLY 4 primary partitions"
  "   - can't recover partitioning and boot data because the data is stored"
  "     in one place"
  "Use only if dual-boot with Windows using Legacy BIOS or installing on"
  "on older hardware, especially on old laptops"
  "$DIALOG_BORDER"
  "Other references/resources:"
  "---------------------------$extraDashes"
  "\"How-to Geek\":"
  "https://www.howtogeek.com/193669/whats-the-difference-between-gpt-and-mbr"
  "-when-partitioning-a-drive/"
  )
  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                getHelpTextForDeviceManagement
# DESCRIPTION: Text to display on the help/more dialog when selecting an operation
#              to manage the partition table
#      RETURN: concatenated string
#  Required Params:
#      1) urlRef - the URL that can be referenced for further documentation
#      2) dlgOpts - associative array of key/value pairs of parameters for the function:
#                 "choices" - concatenated string of choices to be displayed in the
#                             dialog separated by '\t'
#              "deviceType" - Logical Volume or Partition
#         "dialogPartTable" - string containing the rows of data for the partition table
#---------------------------------------------------------------------------------------
function getHelpTextForDeviceManagement() {
  local urlRef="$1"
  local -n dlgOpts=$2
  local dlgChoices=()
  local choiceNum=1
  local key=""
  local dashBorder="----------------------------"
  local helpTextArray=()

  if [ ${dlgOpts["dialogPartTable"]+_} ]; then
    helpTextArray+=("${dlgOpts[dialogPartTable]}" " " "${helpTextArray[@]}")
  elif [ "$DIALOG" == "yad" ]; then
    dashBorder="---------------------------------------"
  fi
  helpTextArray+=("URL Reference:" "$URL_REF_BORDER" "$urlRef" "$DIALOG_BORDER" " "
  "Description of menu options:" "$dashBorder")

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  IFS=$'\t' read -a dlgChoices <<< "${dlgOpts[choices]}"

  for opt in "${dlgChoices[@]}"; do
    key=$(echo "${opt}Text")
    if [ ${dlgOpts["$key"]+_} ]; then
      helpText+=$(printf "\n%d) \"%s\":" "$choiceNum" "${dlgOpts[$opt]}")
      helpText+=$(printf "\n%s" "${dlgOpts[$key]}")
    fi
    choiceNum=$(expr $choiceNum + 1)
  done

  helpText+=$(printf "\n%s\n\n%s\n%s" "$DIALOG_BORDER" "NOTE:" "$NOTE_BORDER")

  if [ "$DIALOG" == "yad" ]; then
    helpText+=$(printf "\n%s\n%s" "     *  Clicking the \"Cancel\" button will remove 'ALL' the partitions & table" \
    "     *  Clicking the \"Done\" button will ask for confirmation to create the ${dlgOpts[deviceType]}s")
  else
    helpText+=$(printf "\n%s\n%s" "   * <Cancel> - removes 'ALL' the partitions & table" \
    "   * <Done > - will ask for confirmation to create the ${dlgOpts[deviceType]}s")
  fi

  echo -e "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:              getHelpTextForCustomDeviceName
# DESCRIPTION: Text to display on the help/more dialog creating a custom device
#      RETURN: concatenated string
#  Required Params:
#      1) deviceType - Logical Volume or Partition
#---------------------------------------------------------------------------------------
function getHelpTextForCustomDeviceName() {
  local deviceType="$1"
  local prefix="$LOGICAL_VOL_PREFIX"
  if [ "$deviceType" == "Partition" ]; then
    prefix="/"
  fi

  local helpTextArray=(
  "If the name does NOT begin with '$prefix', then the installer will append the name"
  "to it.  For example, \"nexus-oss\" would become \"${prefix}nexus-oss\"."
  " " "The following \"$deviceType\" Names are reserved for use by the installer"
  "and may NOT be used:" "----------------------------------------"
  )
  for invName in "${INVALID_DEVICE_NAMES[@]}"; do
    line=$(printf "    # \"%s\" or \"%s%s\"" "$invName" "$prefix" "$invName")
    helpTextArray+=("$line")
  done
  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getHelpTextForRemDevice
# DESCRIPTION: Text to display on the help/more dialog when selecting either a
#              logical volume or partition to be removed
#      RETURN: concatenated string
#  Required Params:
#      1) deviceType - Logical Volume or Partition
#---------------------------------------------------------------------------------------
function getHelpTextForRemDevice() {
  local urlRef="https://wiki.archlinux.org/index.php/partitioning#Discrete_partitions"
  local line=$(echo "Only the eligible \"$1s\" that are NOT required by the system or ")
  line+="essential for booting can be selected to be removed from the partition table."
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef" "$DIALOG_BORDER" " " "$line")
  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getHelpTextToSaveDevices
# DESCRIPTION: Text to display on the help/more dialog when confirming to write
#              the data to disk for the logical volumes or partitions.
#      RETURN: concatenated string
#  Required Params:
#      1) deviceType - Logical Volume or Partition
#      2)      steps - array of the linux commands that will be executed for each step
#   Optional Param:
#      3) string containing the rows of data for the partition table
#---------------------------------------------------------------------------------------
function getHelpTextToSaveDevices() {
  local -n steps=$2
  local textArray=()
  local nextStep=""
  local btnText=$(getHelpTextForButtons "$1")
  local extraDashes=""

  if [ "$1" == "Partition" ]; then
    if [[ "${varMap[PART-LAYOUT]}" =~ "LVM" ]]; then
      textArray=("https://wiki.archlinux.org/index.php/LVM#Create_partitions"
            "  # After saving the Partition Layout/Scheme, The NEXT STEP will then be to create the Logical Volumes!"
            "\"LVM article at Gentoo wiki\" - https://wiki.gentoo.org/wiki/LVM")
    else
      textArray=("https://wiki.archlinux.org/index.php/partitioning#Example_layouts"
            "  # After saving the Partition Layout/Scheme, The NEXT STEP will then be to format the partitions!"
            "\"Easiest Arch Linux Manual\" - http://www.tfir.io/always-updated-arch-linux-tutorial/4")
    fi
  else
    if [[ "${steps[0]}" =~ "pvcreate" ]]; then
      textArray=("https://wiki.archlinux.org/index.php/LVM#Create_physical_volumes")
    else
      textArray=("https://wiki.archlinux.org/index.php/Dm-crypt/Encrypting_an_entire_system#LVM_on_LUKS")
    fi
      textArray+=(
        "  # After saving the logical volumes, The NEXT STEP will then be to format & mount the logical volumes & partitions!")
  fi

  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "${textArray[0]}" "$DIALOG_BORDER" " ")
  if [ "$#" -gt 2 ]; then
    helpTextArray=("$3" " " "${helpTextArray[@]}")
  fi

  if [ "$DIALOG" == "yad" ]; then
    extraDashes="------------"
  fi

  if [ ${#textArray[@]} -gt 2 ]; then
    helpTextArray+=("$btnText" "${steps[@]}" " " "${textArray[-2]}" "$DIALOG_BORDER" " "
     "Other references/resources:" "---------------------------$extraDashes" "${textArray[-1]}")
  else
    helpTextArray+=("$btnText" "${steps[@]}" " " "${textArray[-1]}")
  fi

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getHelpTextForButtons
# DESCRIPTION: Text to display on the help/more dialog for the buttons on the
#              save confirmation dialog
#      RETURN: concatenated string
#  Required Params:
#      1) deviceType - Logical Volume or Partition
#---------------------------------------------------------------------------------------
function getHelpTextForButtons() {
  local btnTextArray+=(
  "  # <Cancel>  removes 'ALL' the '$1s' from the Partition Layout/Scheme"
  "  # < Exit >  stores the data in the Installer's 'data' directory and"
  "              returns to the installation guide"
  "  # < Save >  commits the rows of '$1s' in the Partition Layout/Scheme to the"
  "              disk '$BLOCK_DEVICE' using the following commands:")
  local textArray=()
  local aryIdx=0
  local btnText=""
  local pos=0

  if [ "$1" == "Partition" ]; then
    btnTextArray[0]="  # <Cancel>  removes 'ALL' the '$1s' and the partition table"
  fi

  if [ "$DIALOG" == "yad" ]; then
    for btnNum in $(eval echo "{1..3}"); do
      btnText="${btnTextArray[$aryIdx]}"
      pos=$(expr index "$btnText" \>)
      case ${btnNum} in
        1)
          textArray+=("  # Clicking the \"Cancel\" button")
          textArray[-1]+=$(echo "${btnText:${pos}}")
        ;;
        2)
          textArray+=("  # Clicking the \"Exit\" button")
          textArray[-1]+=$(echo "${btnText:${pos}}")
          aryIdx=$(expr $aryIdx + 1)
          textArray[-1]+=" "
          textArray[-1]+=$(trimString "${btnTextArray[$aryIdx]}")
        ;;
        3)
          textArray+=("  # Clicking the \"Save\" button")
          textArray[-1]+=$(echo "${btnText:${pos}}")
          aryIdx=$(expr $aryIdx + 1)
          textArray[-1]+=" "
          textArray[-1]+=$(trimString "${btnTextArray[$aryIdx]}")
        ;;
      esac
      aryIdx=$(expr $aryIdx + 1)
    done
  else
    textArray=("${btnTextArray[@]}")
  fi
  btnText=$(getTextForDialog "${textArray[@]}")

  echo "$btnText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getHelpTextForVolumeGroup
# DESCRIPTION: Text to display on the help/more dialog setting the name of a Volume Group.
#      RETURN: concatenated string
#   Optional Param:
#      1) string containing the rows of data for the partition table
#---------------------------------------------------------------------------------------
function getHelpTextForVolumeGroup() {
  local extraDashes=""
  local url="https://www.digitalocean.com/community/tutorials/an-introduction-to-lvm-concepts-terminology-and-operations"
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER"
    "https://wiki.archlinux.org/index.php/LVM#Create_volume_group" "$DIALOG_BORDER" " ")
  if [ "$#" -gt 0 ]; then
    helpTextArray=("$1" " " "${helpTextArray[@]}")
  else
    helpTextArray+=("The Volume Group is the highest level abstraction used within the LVM.  It"
    "gathers together a collection of Logical Volumes and Physical Volumes into"
    "one administrative unit." "$DIALOG_BORDER" " ")
  fi

  if [ "$DIALOG" == "yad" ]; then
    extraDashes="------------"
  fi

  local line="Physical volumes are combined into volume groups (VGs).  "
  line+="This creates a pool of disk space out of which volumes can be allocated."
  helpTextArray+=("$line" "$DIALOG_BORDER" " ")

  line="Within a volume group, the disk space available for allocation is divided "
  line+="into units of a fixed-size called extents.  An extent is the smallest "
  line+="unit of space that can be allocated.  Within a physical volume, extents"
  line+="are referred to as physical extents."
  helpTextArray+=("$line" "$DIALOG_BORDER" " ")

  line="A logical volume is allocated into logical extents of the same size as the "
  line+="physical extents. The extent size is thus the same for all logical volumes "
  line+="in the volume group. The volume group maps the logical extents to physical extents."

  helpTextArray+=("$line" "$DIALOG_BORDER" " "
  "Other references/resources:" "---------------------------$extraDashes"
  "\"DigitalOcean's Introduction To LVM\" - $url")

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getHelpTextForSwapSize
# DESCRIPTION: Text to display on the help dialog when selecting the size
#              of the swap to be allocated.
#      RETURN: concatenated string
#   Optional Param:
#      1) string containing the rows of data for the partition table
#---------------------------------------------------------------------------------------
function getHelpTextForSwapSize() {
  local extraDashes=""
  local paragraph1=(
  "There has always been a lot of confusion around swap size.  Historically,"
  "the general rule for swap partition size was to allocate twice the amount"
  "of physical RAM. As computers have gained ever larger memory capacities,"
  "this rule is outdated. For example, on average desktop machines with up to"
  "512 MiB RAM, the 2× rule is usually adequate; if a sufficient amount of"
  "RAM (more than 1024 MiB) is available, it may be possible to have a"
  "smaller swap partition.")
  local concatStr=$(printf " %s" "${paragraph1[@]}")
  local paragraph2=("There is no definite answer to this swap size question. Even the major"
  "Linux distributions don’t have the same swap size guideline. There are"
  "just recommendations and various opinions on the ideal swap size.  Especially"
  "when it comes to using hibernation (a.k.a suspend to disk). The"
  "recommendations vary from the size of RAM to 1.5 times the size of RAM.  Although"
  "the kernel will try to compress the suspend-to-disk image to fit"
  "the swap space there is no guarantee it will succeed if the used swap"
  "space is significantly smaller than RAM.")
  local helpTextArray=()

  if [ "$#" -gt 0 ]; then
    helpTextArray=("$1" " ")
  fi
  if [ "$DIALOG" == "yad" ]; then
    extraDashes="------------"
  fi

  concatStr=${concatStr:1}

  helpTextArray+=("URL Reference:" "$URL_REF_BORDER"
  "https://wiki.archlinux.org/index.php/partitioning#Swap" "$DIALOG_BORDER" " "
  "$concatStr" " ")

  concatStr=$(printf " %s" "${paragraph2[@]}")
  concatStr=${concatStr:1}

  helpTextArray+=("$concatStr" "$DIALOG_BORDER" " ")

  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("Abbreviations"
      "---------------------------$extraDashes"
      "     * W/O - Without"
      "     *    W/ - With")
  else
    helpTextArray+=("Column Header Abbreviations"
      "---------------------------$extraDashes"
      "     * W/O - Without"
      "     *  W/ - With")
  fi

  helpTextArray+=("$DIALOG_BORDER" " "
  "The \"Recommended Swap Sizes\" are based on the following calculations:")

  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("     * Without Hibernation - square root of the RAM size"
      "     *      With Hibernation - (a.k.a suspend to disk) size of RAM plus the square root of the RAM size"
      "     *                 Maximum - 2 * the RAM installed")
  else
    helpTextArray+=("     * Without Hibernation - square root of the RAM size"
      "     *    With Hibernation - (a.k.a suspend to disk) size of RAM plus the square root of the RAM size"
      "     *             Maximum - 2 * the RAM installed")
  fi

  helpTextArray+=("$DIALOG_BORDER" " "
  "Other references/resources:"
  "---------------------------$extraDashes"
  "\"RHEL Swap Space and recommended sizes\" - http://fibrevillage.com/storage/582-rhel-swap-space"
  " "
  "\"It's FOSS\" - https://itsfoss.com/swap-size/"
  )
  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getHelpTextForSwapType
# DESCRIPTION: Text to display on the help/more dialog when selecting a
#              swap type
#      RETURN: concatenated string
#  Required Params:
#      1) dlgOptions - array of options to be displayed in the dialog
#   Optional Param:
#      2) string containing the rows of data for the partition table
#---------------------------------------------------------------------------------------
function getHelpTextForSwapType() {
  local -n dlgOptions=$1
  local extraDashes=""
  local spacing=""
  local concatStr=""
  local helpTextArray=()

  if [ "$#" -gt 1 ]; then
    helpTextArray=("$2" " ")
  fi

  if [ "$DIALOG" == "yad" ]; then
    extraDashes="------------"
  else
    spacing="         "
  fi

  helpTextArray+=("URL Reference:" "$URL_REF_BORDER"
      "https://wiki.archlinux.org/index.php/Swap" "$DIALOG_BORDER" " ")
  concatStr=$(getTextForSwapReasons "$spacing")
  helpTextArray+=("$concatStr")

  concatStr=$(getTextForSwapOptions dlgOptions)

  helpTextArray+=("$DIALOG_BORDER" " "
  "Description of menu options:" "----------------------------$extraDashes"
  "$concatStr" "$DIALOG_BORDER" " " "Other references/resources:"
  "---------------------------$extraDashes"
  "https://wiki.archlinux.org/index.php/Swap#systemd-swap"
  "https://wiki.archlinux.org/index.php/improving_performance#Zram_or_zswap")

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getTextForSwapReasons
# DESCRIPTION: Text to display on the help/more dialog for the reasons behind why swap
#              should be added
#      RETURN: concatenated string
#  Required Params:
#      1) spacing - a string of spaces to format the lines in a linux "dialog"
#---------------------------------------------------------------------------------------
function getTextForSwapReasons() {
  local spacing="$1"
  local concatStr=""
  local textArray=("Linux divides its physical RAM (random access memory) into chunks of"
        "memory called pages. Swapping is the process whereby a page of memory is"
        "copied to the preconfigured space on the hard disk, called swap space, to"
        "free up that page of memory. The combined sizes of the physical memory and"
        "the swap space is the amount of virtual memory available.")
  local first=("    1st) When the system requires more memory than is physically available,"
        "${spacing}the kernel swaps out less used pages and gives memory to the"
        "${spacing}current application (process) that needs the memory immediately.")
  local second=("    2nd) A significant number of the pages used by an application during"
        "${spacing}its startup phase may only be used for initialization and then"
        "${spacing}never used again. The system can swap out those pages and free"
        "${spacing}the memory for other applications or even for the disk cache.")

  if [ "$DIALOG" == "yad" ]; then
    concatStr=$(printf " %s" "${textArray[@]}")
    concatStr=${concatStr:1}
    textArray=("$concatStr" "$DIALOG_BORDER" " "
      "Swapping is necessary for two important reasons:")
    concatStr=$(printf " %s" "${first[@]}")
    concatStr=${concatStr:1}
    textArray+=("$concatStr")
    concatStr=$(printf " %s" "${second[@]}")
    concatStr=${concatStr:1}
    textArray+=("$concatStr")
  else
    textArray+=("$DIALOG_BORDER" " " "Swapping is necessary for two important reasons:"
      "${first[@]}" "${second[@]}")
  fi

  concatStr=$(getTextForDialog "${textArray[@]}")

  echo "$concatStr"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getTextForSwapOptions
# DESCRIPTION: Text to display on the help/more dialog for the description of the
#              options available
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getTextForSwapOptions() {
  local -n optArray=$1
  local textArray=()
  local optTextArray=()
  local concatStr=""

  for swapType in "${optArray[@]}"; do
    local txt="     # \"$swapType\" - "
    case "$swapType" in
      "partition")
        textArray=("the installer will automatically create a partition based"
        "on the size you will select next.")
      ;;
      "LVM partition")
        textArray=("the installer will automatically create a logical"
        "volume based on the size you will select next.")
      ;;
      "file")
        textArray=("offers the ability to vary its size on-the-fly, and is more"
        "easily removed. This may be especially desirable if disk space is at"
        "a premium (e.g. a modestly-sized SSD)")
      ;;
      "systemd-swap")
        textArray=("Package to install containing a script to automate creation and"
        "use of a swap file (visit URL below)")
      ;;
      "zramswap")
        textArray=("AUR Package to install that sets up a zram-based swap device on boot by"
        "creating a device in RAM and compresses it (visit URL below)")
      ;;
    esac

    concatStr=$(printf " %s" "${textArray[@]}")
    local txt=$(printf "     # \"%s\" - %s" "$swapType" "${concatStr:1}")
    optTextArray+=("$txt")
  done

  concatStr=$(getTextForDialog "${optTextArray[@]}")

  echo "$concatStr"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getHelpTextForButtons
# DESCRIPTION: Text to display on the help/more dialog for the buttons on the
#              save confirmation dialog
#      RETURN: concatenated string
#  Required Params:
#      1) dlgOpts - associative array of key/value pairs of parameters for the function:
#            "deviceType" - Logical Volume or Partition
#            "deviceName" - string containing the name of the logical volume or partition
#            "valDevName" - flag to determine if the name of the logical volume or partition
#                           needs to be validated
#---------------------------------------------------------------------------------------
function getHelpTextForCreateDeviceForm() {
  local -n dlgOpts=$1
  local textArray=()
  local flag=${dlgOpts["valDevName"]}
  local helpText=""

  if ${flag}; then
    helpText=$(getHelpTextForCustomDeviceName "${dlgOpts[deviceType]}")
    textArray=("$helpText" "$DIALOG_BORDER--------------" " ")
  fi

  for itemNum in $(eval echo "{1..5}"); do
    case ${itemNum} in
      1)
        textArray+=("  # Clicking the \"Cancel\" button will display the previous dialog where the size can be selected")
        textArray+=("$DIALOG_BORDER--------------" " ")
      ;;
      2)
        textArray+=("  # Clicking the next set of buttons will create the \"${dlgOpts[deviceType]}\" with the ")
        textArray[-1]+="name of '${dlgOpts[deviceName]}' and size depending on which of the following was clicked:"
      ;;
      3)
        textArray+=("       * Clicking the \"Min\" button will set the Allocation Size to the value in the simple text entry field ")
        textArray[-1]+="next to the label \"Minimum\"."
      ;;
      4)
        textArray+=("       * Clicking the \"Max\" button will set the Allocation Size to the value in the Simple Text Entry (STE) field ")
        textArray[-1]+="next to the label \"Maximum\"."
      ;;
      5)
        textArray+=("       * Clicking the \"Other\" button will set the Allocation Size to the value in the numeric field ")
        textArray[-1]+="next to the label \"Other\"."
        textArray+=("            > This numeric field can be set by either using the '-' or '+' buttons, or by entering the value")
        textArray[-1]+=" manually.  Entering a value greater than the \"Maximum\" will set this field to the value that is"
        textArray[-1]+=" shown in the maximum STE field.  Likewise, entering a value less than the \"Minimum\" will set this"
        textArray[-1]+=" field to the value that is shown in the minimum STE field."
      ;;
    esac
  done

  helpText=$(getTextForDialog "${textArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:              getHelpTextForCustomDevice
# DESCRIPTION: Text to display on the help/more dialog creating a custom device
#      RETURN: concatenated string
#  Required Params:
#      1) deviceType - Logical Volume or Partition
#---------------------------------------------------------------------------------------
function getHelpTextForCustomDevice() {
  local tabBorder="-----------------------------------------------"
  local helpTextArray=("Tab Name: \"Select Size to Allocate\"" "$tabBorder")
  local textArray=("   * The human-readable sizes are based on calculations made by the installer."
    "  Click the \"Form\" button to enter a different size other than the choices being displayed.")
  local helpText=$(printf " %s" "${textArray[@]}")
  helpText=${helpText:1}

  helpTextArray+=("$helpText" "$DIALOG_BORDER--------------" " " "Tab Name: \"Enter a Name\"" "$tabBorder")
  helpText=$(getHelpTextForCustomDeviceName "$1")

  helpTextArray+=("$helpText" "$DIALOG_BORDER--------------" " " "NOTE:" "$NOTE_BORDER"
    "   * Click the \"Form\" button to enter a different size other than the choices being displayed.")
  textArray=(
    "   * Clicking the \"Ok\" button will submit both the human-readable size selected on the "
    "\"Select Size to Allocate\" tab as well as the name entered on the \"Enter a Name\" tab.")
  helpText=$(printf " %s" "${textArray[@]}")
  helpText=${helpText:1}
  helpTextArray+=("$helpText")
  helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getHelpTextForFST
# DESCRIPTION: Text to display on the help/more dialog when selecting a
#              file system type
#      RETURN: concatenated string
#  Required Params:
#      1) helpParams - an associative array of key/value pairs to be displayed
#                      on the main & help dialogs
#---------------------------------------------------------------------------------------
function getHelpTextForFST() {
  local -n helpParams=$1
  local fstChoices=()
  local urlRef="${helpParams[urlRef]}"
  local devWarnText="${helpParams["devWarnText"]}"
  local helpTextArray=(  " * 'btrfs' does not support swap files. Failure to heed this warning may"
  "result in file system corruption. While a swap file may be used on"
  "'btrfs' when mounted through a loop device, this will result in severely degraded swap performance.")
  local addDashes=""
  local dlgBorder="$DIALOG_BORDER"
  local warnText=$(getTextForWarning)
  local concatStr=$(printf " %s" "${helpTextArray[@]}")
  concatStr=${concatStr:1}

  if [ "$DIALOG" == "yad" ]; then
    addDashes="-----------"
    dlgBorder=$(echo "${DIALOG_BORDER}--------------")
  fi

  if [ ${helpParams["dialogPartTable"]+_} ]; then
    helpTextArray=("${helpParams[dialogPartTable]}" " ")
  else
    helpTextArray=("${helpParams[dialogText]}" "$dlgBorder" " ")
  fi

  if [ ${#urlRef} -gt 0 ]; then
    helpTextArray+=("URL Reference:" "$URL_REF_BORDER" "$urlRef" "$dlgBorder" " ")
  else
    appendHelpTextForFST "$dlgBorder"
  fi
  helpTextArray+=("Description of menu options:" "----------------------------$addDashes")

  IFS=$'\t' read -d '' -a fstChoices <<< "${helpParams[choices]}"
  fstChoices[-1]=$(trimString "${fstChoices[-1]}")
  for fst in "${fstChoices[@]}"; do
    helpTextArray+=("${FMT_HELP_TEXT_OPTS[$fst]}")
  done

  if [ ${#fstChoices[@]} -gt 1 ]; then
    helpTextArray+=(" " "$warnText" "$concatStr" " " "$devWarnText")
  else
    helpTextArray+=(" " "$warnText" "$devWarnText")
  fi
  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#      METHOD:                 appendHelpTextForFST
# DESCRIPTION: Append lines of text to the helpTextArray variable that was initialized
#              in the calling method
#  Required Params:
#      1) dlgBorder - line of dashes to separate the sections
#---------------------------------------------------------------------------------------
appendHelpTextForFST() {
  local dlgBorder="$1"
  local lclTextArray=()
  local text=""

  helpTextArray+=("URL Reference:" "$URL_REF_BORDER"
    "https://wiki.archlinux.org/index.php/file_systems" "$dlgBorder" " ")
  lclTextArray=("A file system (or filesystem) is used to control how data is stored and")
  lclTextArray+=("retrieved.  Without a file system, information placed in a storage medium")
  lclTextArray+=("would be one large body of data with no way to tell where one piece of")
  lclTextArray+=("information stops and the next begins." " ")
  lclTextArray+=("Individual drive partitions can be setup using one of the many different")
  lclTextArray+=("available file systems.  Each has its own advantages, disadvantages, and")
  lclTextArray+=("unique idiosyncrasies.  Below is a brief overview of the supported FSTs.")

  if [ "$DIALOG" == "yad" ]; then
    text=$(formatTextForYAD lclTextArray)
  else
    text=$(printf " %s" "${lclTextArray[@]}")
    text=${text:1}
  fi

  helpTextArray+=("$text")
  helpTextArray+=("$dlgBorder" " ")
}

#---------------------------------------------------------------------------------------
#      METHOD:                 appendHelpTextForFSTOpts
# DESCRIPTION: Append lines of text dealing with the button options for setting the
#              format types to the helpTextArray variable that was initialized
#              in the calling method
#  Required Params:
#      1)   dlgBorder - line of dashes to separate the sections
#      2) deviceTypes - string containing the type of devices to be mounted
#---------------------------------------------------------------------------------------
appendHelpTextForFSTOpts() {
  local dlgBorder="$1"
  local deviceTypes="$2"
  local text=""

  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("Description of options:" "---------------------------------"
    "  # Clicking the \"Edit\" will edit/change the file system type that was assigned to each device")
    textArray=(
    "  # Clicking the \"Exit\" stores the data in the Installer's 'data' directory and returns to"
    "the installation guide")
    text=$(formatTextForYAD textArray)
    helpTextArray+=("$text")
    textArray=("  # Clicking the \"Confirm\" will format the $deviceTypes with their assigned"
    "file system type using the commands below:")
    text=$(formatTextForYAD textArray)
    helpTextArray+=("$text")
  else
    helpTextArray+=("Description of options:" "-----------------------"
    "  # < Edit  >  edit/change the file system type that was assigned to each device"
    "  # < Exit  >  stores the data in the Installer's 'data' directory and returns to"
    "               the installation guide"
    "  # <Confirm>  formats the $deviceTypes with their assigned file system type"
    "               using the commands below:")
    text=$(getTextForDialog "${textArray[@]}")
    helpTextArray+=("$text")
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                 appendHelpTextForMPT
# DESCRIPTION: Append lines of text to the helpTextArray variable that was initialized
#              in the calling method
#  Required Params:
#      1) dlgBorder - line of dashes to separate the sections
#---------------------------------------------------------------------------------------
appendHelpTextForMPT() {
  local dlgBorder="$1"
  local lclTextArray=()
  local text=""

  helpTextArray+=("https://wiki.archlinux.org/index.php/file_systems#Mount_a_file_system")
  helpTextArray+=("$dlgBorder" " ")

  if [[ "${varMap[PART-LAYOUT]}" =~ "LVM" ]]; then
    lclTextArray=("On Arch Linux non-system devices are not mounted automatically.  Logical Volumes")
    lclTextArray+=("& Partitions will be mounted by the installer in fstab.  Mounting in fstab is the")
    lclTextArray+=("more traditional method and is still used because of it’s simplicity and")
  else
    lclTextArray=("On Arch Linux non-system paritions are not mounted automatically.  Partitions")
    lclTextArray+=("will be mounted by the installer in fstab.  Mounting in fstab is the more")
    lclTextArray+=("traditional method and is still used because of it’s simplicity and")
  fi
  lclTextArray+=("convenience.  Mounts in fstab are converted to native systemd mounts.")

  if [ "$DIALOG" == "yad" ]; then
    text=$(formatTextForYAD lclTextArray)
  else
    text=$(printf " %s" "${lclTextArray[@]}")
    text=${text:1}
  fi

  helpTextArray+=("$text")
  helpTextArray+=("$dlgBorder" " ")
}

#---------------------------------------------------------------------------------------
#      METHOD:                 appendHelpTextForMPTOpts
# DESCRIPTION: Append lines of text dealing with the button options for mounting
#              to the helpTextArray variable that was initialized
#              in the calling method
#  Required Params:
#      1)   dlgBorder - line of dashes to separate the sections
#      2) deviceTypes - string containing the type of devices to be mounted
#---------------------------------------------------------------------------------------
appendHelpTextForMPTOpts() {
  local dlgBorder="$1"
  local deviceTypes="$2"
  local text=""

  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("Description of options:" "---------------------------------"
    "  # Clicking the \"Edit\" will edit/change the directory that was assigned to each device")
    textArray=(
    "  # Clicking the \"Exit\" stores the data in the Installer's 'data' directory"
    "and returns to the installation guide")
    text=$(formatTextForYAD textArray)
    helpTextArray+=("$text")
    textArray=("  # Clicking the \"Confirm\" will execute the process of associating the"
    "$deviceTypes to their specified directory using the commands below:")
    text=$(formatTextForYAD textArray)
    helpTextArray+=("$text")

  else
    textArray=("Description of options:" "-----------------------"
    "  # < Edit  >  edit/change the directory that was assigned to each device"
    "  # < Exit  >  stores the data in the Installer's 'data' directory and returns to"
    "               the installation guide"
    "  # <Confirm>  executes the process of associating the $deviceTypes to their"
    "               specified directory using the commands below:")
    text=$(getTextForDialog "${textArray[@]}")
    helpTextArray+=("$text")
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                      getTextForWarning
# DESCRIPTION: Get the text to display in the dialog to signify a warning message.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getTextForWarning() {
  local dashBorder="----------------------------------"
  local textArray=("$dashBorder" "          WARNING!!!!!!!" "$dashBorder")
  local text=$(getTextForDialog "${textArray[@]}")

  echo "$text"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getHelpTextForDevFST
# DESCRIPTION: Text to display on the help/more dialog when selecting the devices
#              to assign a file system to
#      RETURN: concatenated string
#   Optional Param:
#      1) string containing the rows of data for the partition table
#---------------------------------------------------------------------------------------
function getHelpTextForDevFST() {
  local helpTextArray=()
  local warnText=$(getTextForWarning)
  local textArray=("A file system (or filesystem) is used to control how data is stored and retrieved. Without a"
  "file system, information placed in a storage medium would be one large body of data with no"
  "way to tell where one piece of information stops and the next begins. By separating the data"
  "into pieces and giving each piece a name, the information is easily isolated and identified.")
  local concatStr=$(printf " %s" "${textArray[@]}")
  concatStr=${concatStr:1}

  if [ "$#" -gt 0 ]; then
    helpTextArray=("$1" " ")
  fi

  helpTextArray+=("URL Reference:" "$URL_REF_BORDER"
  "https://wiki.archlinux.org/index.php/installation_guide#Format_the_partitions"
  "$DIALOG_BORDER" " " "$concatStr" "$DIALOG_BORDER" " ")

  addHelpTextOptsForFST

  helpTextArray+=("$DIALOG_BORDER" " " "$warnText")
  textArray=("The file system containing the /boot directory must be supported by"
  "the '${varMap["BOOT-LOADER"]}' boot loader.")
  concatStr=$(printf " %s" "${textArray[@]}")
  concatStr=${concatStr:1}
  helpTextArray+=("$concatStr")

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#      METHOD:                    addHelpTextOptsForFST
# DESCRIPTION: Append lines of text to the helpTextArray that was initialized in the
#              calling method
#---------------------------------------------------------------------------------------
addHelpTextOptsForFST() {
  local textArray=()
  local deviceType="partition"
  if [[ "${varMap[PART-LAYOUT]}" =~ "LVM" ]]; then
    deviceType="logical volume"
  fi

  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("Description of options:" "---------------------------------")
    for idx in $(eval echo "{1..6}"); do
      case ${idx} in
        1)
          textArray=("     >>  Selecting the \"row#*\" option will assign the row in the partition table"
          "to the file system type that will be selected on the next screen")
        ;;
        2)
          textArray=("     >>  Clicking the \"All\" button will set all of the row#* selections and the '/' or root to"
          "the same file system type")
        ;;
        3)
          textArray=("     >>  Clicking the \"Reset\" button will remove the file system types from the ${deviceType}s that were"
          "assigned and restarts the process from the beginning")
        ;;
        4)
          textArray=("     >>  Clicking the \"ROOT\" button will set all of the row#* selections to the same"
          "file system type as the '/' or root")
        ;;
        5)
          textArray=("     >>  Clicking the \"Cancel\" button will store the data in the Installer's"
          " 'data' directory and return to the installation guide")
        ;;
        6)
          textArray=("     >>  Clicking the \"Format\" button will verify that the required ${deviceType}s"
          " have a file system type and then ask for confirmation to format")
        ;;
      esac
      concatStr=$(printf " %s" "${textArray[@]}")
      concatStr=${concatStr:1}
      helpTextArray+=("$concatStr")
    done
  else
    helpTextArray+=("Description of options:" "-----------------------"
      "  #    \"All\" - sets all of the row#* selections and the '/' or root to"
      "               the same file system type"
      "  #   \"ROOT\" - sets all of the row#* selections to the same"
      "               file system type as the '/' or root"
      "  #  \"row#*\" - row in the partition table to have its file system type set"
      "               that will be selected on the next screen"
      "  # \"Format\" - verifies that the required ${deviceType}s have a file system type"
      "               and then ask for confirmation to format"
      "  # <Cancel>  stores the data in the Installer's 'data' directory and"
      "              returns to the installation guide"
      "  # <Reset >  removes the file system types from the ${deviceType}s that were"
      "              assigned and restarts the process from the beginning"
    )
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getHelpTextForConf
# DESCRIPTION: Text to display on the help/more dialog when either:
#              a) confirming the file system types to format the devices with
#              b) confirming the directories to mount the devices to
#      RETURN: concatenated string
#  Required Params:
#      1) linuxCmds - array of linux commands to be executed
#   Optional Param:
#      2) string containing the rows of data for the partition table
#---------------------------------------------------------------------------------------
function getHelpTextForConf() {
  local -n linuxCmds=$1
  local stepNum=1
  local helpTextArray=()
  local deviceTypes="partitions"
  local steps=()
  local dlgBorder="$DIALOG_BORDER"

  if [ "$DIALOG" == "yad" ]; then
    addDashes="-----------"
    dlgBorder=$(echo "${DIALOG_BORDER}--------------")
  else
    dlgBorder="$DIALOG_BORDER_MAX"
  fi

  if [[ "${varMap[PART-LAYOUT]}" =~ "LVM" ]]; then
    deviceTypes="logical volumes & partitions"
  fi

  if [ "$#" -gt 1 ]; then
    helpTextArray=("$2" " " "${helpTextArray[@]}")
  fi

  if [[ "$DIALOG_BACK_TITLE" =~ "Mount" ]]; then
    appendHelpTextForMPT "$dlgBorder"
    appendHelpTextForMPTOpts helpTextArray "$deviceTypes"
  else
    appendHelpTextForFST "$dlgBorder"
    appendHelpTextForFSTOpts "$dlgBorder" "$deviceTypes"
  fi

  for cmd in "${linuxCmds[@]}"; do
    IFS=$'\t' read -a steps <<< "$cmd"
    cmd=$(printf "%7s step#%02d) %s" " " ${stepNum} "${steps[0]}")
    helpTextArray+=("$cmd")
    for cmd in "${steps[@]:1}"; do
      cmd=$(printf "%16s %s" " " "$cmd")
      helpTextArray+=("$cmd")
    done
    stepNum=$(expr $stepNum + 1)
  done

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo -e "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getHelpTextForDevMPT
# DESCRIPTION: Text to display on the help/more dialog when selecting the device
#              to assign a mount pt to
#      RETURN: concatenated string
#  Required Params:
#      1) string containing the rows of data for the partition table
#---------------------------------------------------------------------------------------
function getHelpTextForDevMPT() {
  local helpTextArray=()
  local warnText=$(getTextForWarning)
  local deviceType="partition"
  if [[ "${varMap[PART-LAYOUT]}" =~ "LVM" ]]; then
    deviceType="logical volume"
  fi
  local textArray=("A $deviceType is used to form part of the storage necessary to support a "
  "single set of files and directories.  Mounting is the process of associating a logical volume"
  "or partition to a specified directory.  The mount point is the specified directory in a file"
  "system where additional information is logically connected through the root logical volume"
  "or partition.")
  local concatStr=$(printf " %s" "${textArray[@]}")
  concatStr=${concatStr:1}
  local dlgBorder="$DIALOG_BORDER_MAX"
  if [ "$DIALOG" == "yad" ]; then
    dlgBorder="$DIALOG_BORDER"
  fi

  if [ "$#" -gt 0 ]; then
    helpTextArray=("$1" " ")
  fi

  helpTextArray+=("URL Reference:" "$URL_REF_BORDER"
  "https://wiki.archlinux.org/index.php/File_systems#Mount_a_file_system"
  "$dlgBorder" " " "$concatStr" "$dlgBorder" " ")

  addHelpTextOptsForMPT

  helpTextArray+=("$dlgBorder" " " "$warnText"
  "  *  Except for the /boot directory, directories essential for booting must be on the same"
  "     partition as '/' or mounted in early userspace by the initramfs. These essential"
  "     directories are: /etc and /usr "
  )
  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#      METHOD:                    addHelpTextOptsForMPT
# DESCRIPTION: Append lines of text to the helpTextArray that was initialized in the
#              calling method
#---------------------------------------------------------------------------------------
addHelpTextOptsForMPT() {
  local textArray=()
  local deviceType="partition"
  if [[ "${varMap[PART-LAYOUT]}" =~ "LVM" ]]; then
    deviceType="logical volume"
  fi

  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("Description of options:" "---------------------------------")
    for idx in $(eval echo "{1..3}"); do
      case ${idx} in
        1)
          textArray=("     >>  Clicking the \"Reset\" button will remove the file system types from the ${deviceType}s that were"
          "assigned and restarts the process from the beginning")
        ;;
        2)
          textArray=("     >>  Clicking the \"Cancel\" button will store the data in the Installer's"
          " 'data' directory and return to the installation guide")
        ;;
        3)
          textArray=("     >>  Clicking the \"Mount\" button will verify that the required ${deviceType}s"
          " have a specified directory to mount to and then ask for confirmation to mount")
        ;;
      esac
      concatStr=$(printf " %s" "${textArray[@]}")
      concatStr=${concatStr:1}
      helpTextArray+=("$concatStr")
    done
  else
    helpTextArray+=("Description of options:" "-----------------------"
      "  # \"row#*\" - row in the partition table to assign/edit a specified directory"
      "              that will be entered on the next screen"
      "  # \"Mount\" - verifies that the required ${deviceType}s have a specified directory"
      "              and then ask for confirmation to mount"
      "  # <Cancel>  stores the data in the Installer's 'data' directory and"
      "              returns to the installation guide"
      "  # <Reset >  removes the specified directory from the ${deviceType}s that were"
      "              assigned and restarts the process from the beginning"
    )
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getHelpTextForMirroList
# DESCRIPTION: Text to display on the help/more dialog for setting up the mirrors for
#              the primary.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForMirroList() {
  local extraDashes=""
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER"
    "https://wiki.archlinux.org/index.php/Mirrors" "$DIALOG_BORDER" " ")
  local lclTextArray=("This option is a guide to selecting and configuring the mirrors, which are used by the package"
                      "manager to download and install software.  The KAAGI installer will prompt to choose a primary"
                      "country, and the installer will then use a program called reflector.  Reflector will select the"
                      "200 most up-to-date mirrors synchronized HTTP or HTTPS mirrors from the Arch Linux -"
                      "Mirror Status page, and sort them by speed within the primary country, and save the list to a"
                      "temporary file.")
  local concatStr=$(printf " %s" "${lclTextArray[@]}")

  if [ "$DIALOG" == "yad" ]; then
    extraDashes="------------"
  fi

  helpTextArray+=("${concatStr:1}" " ")
  lclTextArray=("The installer will then prompt for a secondary set of countries to use.  Reflector will then"
                "choose the best 200 mirrors from the countries selected and do the same as in the previous"
                "paragraph.  These mirror sites will be appended after the list of mirrors that were found"
                "for the primary country.  For example, if Canada, Mexico, Germany, and France were chosen,"
                "reflector will find the best mirrors between all 4 of these countries.")
  concatStr=$(printf " %s" "${lclTextArray[@]}")
  helpTextArray+=("${concatStr:1}" " ")

  lclTextArray=("After confirming to save, the installer will create a backup of the original list that was"
                "installed, and then overwrite the the /ect/pacman.d/mirrorlist file with list of mirrors"
                "that were generated.")
  concatStr=$(printf " %s" "${lclTextArray[@]}")
  helpTextArray+=("${concatStr:1}" "$DIALOG_BORDER" " "
  "Other references/resources:" "---------------------------$extraDashes"
  "\"Mirror Status\" - https://www.archlinux.org/mirrors/status"
  "\"Reflector\" - https://wiki.archlinux.org/index.php/reflector")

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getHelpTextForSecondary
# DESCRIPTION: Text to display on the help/more dialog for setting up the secondary
#              set of mirrors.
#      RETURN: concatenated string
#  Required Params:
#      1) primaryCtryCode - the primary country that the mirrors will be searched first
#---------------------------------------------------------------------------------------
function getHelpTextForSecondary() {
  local primaryCtryName="${ISO_CODE_NAMES["$1"]}"
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER"
    "https://wiki.archlinux.org/index.php/Mirrors" "$DIALOG_BORDER" " ")
  local opt=""
  local extraDashes=""

  if [ "$#" -gt 1 ]; then
    if [ "$DIALOG" == "yad" ]; then
      extraDashes="------------"
    fi
    opt="Select 1 or more countries from the check list of available"
  else
    if [ "$DIALOG" == "yad" ]; then
      opt="Clicking the \"Yes\" button "
    else
      opt="Selecting \"YES\" "
    fi
    opt+="will then display a check list of available"
  fi

  local lclTextArray=("Sometimes it is good to have a backup, and the AUR package that the \"pacman\" package manager"
                      ", or a particular version of the package, will try to install may not be in the list of mirrors"
                      "for the \"primary\" country '$primaryCtryName'.  $opt"
                      "countries that \"pacman\" will search after all the primary mirrors.")
  local concatStr=$(printf " %s" "${lclTextArray[@]}")
  helpTextArray+=("${concatStr:1}" " ")

  lclTextArray=("The installer will use a program called reflector to find the most up-to-date mirrors"
                "synchronized HTTP or HTTPS mirrors from the Arch Linux - Mirror Status page, and sort them by speed"
                "between the countries that were selected.  For example, if Canada, Mexico, Germany, and France were"
                "chosen, reflector will find the best 200 mirrors between all 4 of these countries.  These sites will"
                "be appended to the list of sites found in the primary.")
  concatStr=$(printf " %s" "${lclTextArray[@]}")

  if [ "$#" -gt 1 ]; then
    helpTextArray+=("${concatStr:1}" "$DIALOG_BORDER" " "
      "Other references/resources:" "---------------------------$extraDashes"
      "\"Mirror Status\" - https://www.archlinux.org/mirrors/status"
      "\"Reflector\" - https://wiki.archlinux.org/index.php/reflector")
  else
    helpTextArray+=("${concatStr:1}")
  fi

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getHelpTextForBaseInstall
# DESCRIPTION: Text to display on the help/more dialog for installing the base system
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForBaseInstall() {
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER"
    "https://wiki.archlinux.org/index.php/Kernel" "$DIALOG_BORDER" " ")
  local extraDashes=""

  if [ "$DIALOG" == "yad" ]; then
    extraDashes="----------------------"
  fi

  local lclTextArray=("Arch Linux is based on the Linux kernel.  There are "
    "various alternative Linux kernels available for Arch Linux in addition"
    "to the stable Linux kernel.")
  local concatStr=$(printf " %s" "${lclTextArray[@]}")
  helpTextArray+=("${concatStr:1}" " " "Description of Officially supported kernels:"
    "--------------------------------------------$extraDashes"
    "     * \"linux\":  Stable — Vanilla Linux kernel and modules, with a few patches applied."
    "     * \"linux-hardened\":  "
    "     * \"linux-lts\":  Longterm — Long-term support (LTS) Linux kernel and modules."
    "     * \"linux-zen\":  "
    )

  lclTextArray=("Hardened — A security-focused Linux kernel applying a set of hardening patches to mitigate kernel and userspace"
                "exploits. It also enables more upstream kernel hardening features than linux along AppArmor and SELinux.")
  concatStr=$(printf " %s" "${lclTextArray[@]}")
  helpTextArray[10]+="${concatStr:1}"
  lclTextArray=("ZEN Kernel — Result of a collaborative effort of kernel hackers to provide the best Linux kernel"
                "possible for everyday systems.")
  concatStr=$(printf " %s" "${lclTextArray[@]}")
  helpTextArray[12]+="${concatStr:1}"
  helpTextArray+=("$DIALOG_BORDER" " "
      "Other references/resources:" "---------------------------$extraDashes"
      "\"LTS & Stable\" - https://www.kernel.org/category/releases.html"
      "\"Hardened\" - https://kernsec.org/wiki/index.php/Kernel_Self_Protection_Project"
      "\"ZEN Kernel\" - https://github.com/zen-kernel/zen-kernel")

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getHelpTextForInstConf
# DESCRIPTION: Text to display on the help/more dialog when installing the base,
#              linux kernel, python, etc.
#      RETURN: concatenated string
#  Required Params:
#      1) linuxCmds - array of linux commands to be executed
#---------------------------------------------------------------------------------------
function getHelpTextForInstConf() {
  local -n linuxCmds=$1
  local stepNum=1
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER"
    "https://wiki.archlinux.org/index.php/installation_guide#Install_the_base_packages"
    "$DIALOG_BORDER" " ")
  local deviceTypes="partitions"
  local instCmdArray=()

  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("  # Clicking the \"Cancel\" button will ")
    helpTextArray+=("  # Clicking the \"Confirm\" button will ")
  else
    helpTextArray+=("  # Selecting <Cancel > will ")
    helpTextArray+=("  # Selecting <Confirm> will ")
  fi
  helpTextArray[-2]+="display the previous dialog to select a Linux kernel."
  helpTextArray[-1]+="execute the following:"
  helpTextArray+=("$DIALOG_BORDER")

  for cmd in "${linuxCmds[@]}"; do
    IFS=$' ' read -a instCmdArray <<< "$cmd"
    cmd=$(printf "%7s step#%02d) %s" " " ${stepNum} "$cmd")
    helpTextArray+=(" " "$cmd")
    for pckg in "${instCmdArray[@]:2}"; do
      helpTextArray+=("${HELP_TEXT_ARRAY["$pckg"]}")
    done
    stepNum=$(expr $stepNum + 1)
  done

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo -e "$helpText"
}


#---------------------------------------------------------------------------------------
#    FUNCTION:                     getHelpTextForFstab
# DESCRIPTION: Text to display on the help/more dialog when creating the fstab file.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForFstab() {
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER"
    "https://wiki.archlinux.org/index.php/Fstab" "$DIALOG_BORDER" " ")

  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("  # Clicking the \"Cancel\" button will ")
  else
    helpTextArray+=("  # Selecting <Cancel > will ")
  fi
  helpTextArray[-1]+="store the data in the Installer's 'data' directory and return to the installation guide"
  helpTextArray+=("$DIALOG_BORDER" " "
    "The /etc/fstab file contains static file system information. It tells"
    "the system how the devices should be mounted and integrated into the overall system at startup."
    " " "Each filesystem is described in a separate line.  These lines contain a configurable identifier"
    "at the beginning and may be set by using one of the following options:"
    "$DIALOG_BORDER" " "
    "     #1)  Name of device"
    "             # Logical Volume:  /dev/mapper/[Volume Grooup]-[Logical Volume Name]"
    "             # Partition:  /dev/sda* where star is the number of the mounted device"
    "     #2)  File system UUID"
    "             # The Universal Unique Identifier of the mounted logical volume or partition"
    "     #3)  File system label"
    "             # Label assigned to a device, which currently can only be on devices with file system type Ext 3/4."
    "             # If the label is blank, then the device name is used."
    )

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo -e "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     getHelpTextForZone
# DESCRIPTION: Text to display on the help/more dialog when selecting the zone
#              for the time zone.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForZone() {
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER"
    "https://wiki.archlinux.org/index.php/Time#Time_zone" "$DIALOG_BORDER" " "
    )
  local concatStr=$(printf " %s" "${lclTextArray[@]}")
  local text="          b) If the zone has NO sub-zones (see below), confirm the zone that was selected."

  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("Click the \"Select Zone\" tab and choose the appropriate zone." " "
      "  # Clicking the \"Ok\" button will either:"
      "          a) Display the \"Select Sub-Zone\" tab to select a sub-zone for the zone that was selected."
      "$text" " " "  # Clicking the \"Exit\" button ")
  else
    helpTextArray+=("The installer will display a radio list of Zones.  Choose the appropriate zone." " "
      "  # Selecting < Ok > will either:"
      "          a) Display a radio list of Sub-Zones to select a sub-zone for the zone that was selected."
      "$text" "  # Selecting <Exit> ")
  fi

  helpTextArray[-1]+="will store the data in the Installer's 'data' directory and return to the installation guide"
  helpTextArray+=("$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER"
    "The following Zones DO NOT have any Sub-Zones:")
  local lclTextArray=($(find -L /usr/share/zoneinfo/posix -maxdepth 1 -type f -printf "%P\n" | sort -n))
  local concatStr=$(printf "       *  %s\n" "${lclTextArray[@]}")
  helpTextArray+=("$concatStr")

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo -e "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getHelpTextForSubZone
# DESCRIPTION: Text to display on the help/more dialog when selecting the sub-zone
#              of a time zone.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForSubZone() {
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER"
    "https://wiki.archlinux.org/index.php/Time#Time_zone" "$DIALOG_BORDER" " ")
  local lclTextArray=("Choose from the radio list of sub-zones for the"
      "time zone that was previously selected.")
  local concatStr=$(printf " %s" "${lclTextArray[@]}")

  helpTextArray+=("${concatStr:1}")

  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("  # Clicking the \"Ok\" button will ")
    helpTextArray+=("  # Clicking the \"Cancel\" button will ")
  else
    helpTextArray+=("  # Selecting <  Ok  > will ")
    helpTextArray+=("  # Selecting <Cancel> will ")
  fi

  helpTextArray[-2]+="ask for confirmation to set the time zone based on the values selected."
  helpTextArray[-1]+="return to the previous dialog to select a different time zone."

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo -e "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getHelpTextForTimeZoneConf
# DESCRIPTION: Text to display on the help/more dialog when confirming the time zone.
#      RETURN: concatenated string
#  Required Params:
#      1) autoFlag - flag to indicate if the time zone & location/sub-zone were determined
#                    by the installer, or if they were entered manually.
#      2) timeZone - time zone & location/sub-zone
#---------------------------------------------------------------------------------------
function getHelpTextForTimeZoneConf() {
  local autoFlag=$1
  local timeZone="$2"
  local tzArray=()
  local concatStr=""
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER"
  "https://wiki.archlinux.org/index.php/Time#Time_zone" "$DIALOG_BORDER" " ")

  IFS='/' read -a tzArray <<< "$timeZone"

  helpTextArray+=("The following Time Zone ")
  if ${autoFlag}; then
    helpTextArray[-1]+="was detected by the installer:"
  else
    helpTextArray[-1]+="was selected:"
  fi

  if [ ${#tzArray[@]} -gt 1 ]; then
    concatStr=$(printf "/%s" "${tzArray[@]:1}")
    local zoneTitle=""
    if [ "$DIALOG" == "yad" ]; then
      zoneTitle=$(printf "%18sZone:%2s'%s'" " " " " "${tzArray[0]}")
    else
      zoneTitle=$(printf "%15sZone:%2s'%s'" " " " " "${tzArray[0]}")
    fi
    helpTextArray+=("$zoneTitle" "           Sub-Zone:  '${concatStr:1}'")
  else
    helpTextArray+=("               Time Zone:  '$timeZone'")
  fi

  helpTextArray+=("$DIALOG_BORDER" " ")

  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("  # Clicking the \"Confirm\" button will ")
    helpTextArray+=("  # Clicking the \"Cancel\" button will ")
  else
    helpTextArray+=("  # Selecting <Confirm> will ")
    helpTextArray+=("  # Selecting <Cancel > will ")
  fi

  if [ ${#tzArray[@]} -gt 1 ]; then
    helpTextArray[-2]+="set the timezone based on the values displayed in the Zone & Sub-Zone fields."
  else
    helpTextArray[-2]+="set the timezone based on the value displayed in the Time Zone field."
  fi
  helpTextArray[-1]+="display the dialogs with a radio list of Zones & Sub-Zones to choose from."

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo -e "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getHelpTextForTimeStandard
# DESCRIPTION: Text to display on the help/more dialog when setting the time standard.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForTimeStandard() {
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER"
    "https://wiki.archlinux.org/index.php/Time#Time_standard" "$DIALOG_BORDER" " ")

  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("  # Clicking the \"Local Time\" button ")
    helpTextArray+=("  # Clicking the \"UTC\" button ")
    helpTextArray+=("  # Clicking the \"Exit\" button ")
  else
    helpTextArray+=("  # Selecting <Local> ")
    helpTextArray+=("  # Selecting < UTC > ")
    helpTextArray+=("  # Selecting <Exit > ")
  fi

  helpTextArray[-3]+="will set the time standard to the local timescale."
  helpTextArray[-2]+="will set the time standard to the Universal Time Coordinated timescale."
  helpTextArray[-1]+="will store the data in the Installer's 'data' directory and return to the installation guide"

  helpTextArray+=("$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER"
  "Choosing the local timescale could cause daylight savings time to be inconsistent!")

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo -e "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getHelpTextForLocalization
# DESCRIPTION: Text to display on the help/more dialog when setting the locale.
#      RETURN: concatenated string
#  Required Params:
#      1) the default locale
#      2) dialogNum - the number of the specific dialog that will be rendered
#---------------------------------------------------------------------------------------
function getHelpTextForLocalization() {
  local dialogNum=$2
  local hdrText=$(getHdrTextForLocalization)
  local helpTextArray=("$hdrText")

  setDlgTextForLocalization "$1" ${dialogNum}

  if [ "$DIALOG" != "yad" ]; then
    setTextForELinks
  fi

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo -e "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getHdrTextForLocalization
# DESCRIPTION: Text to display in the header of the help/more dialog
#              when setting the locale.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHdrTextForLocalization() {
  local helpTxtArray=("URL Reference:" "$URL_REF_BORDER"
    "https://wiki.archlinux.org/index.php/Locale" "$DIALOG_BORDER" " ")
  local lclTxtArray=("Locales are used in Linux by the GNU C Library (glibc),"
    "as well as other locale-aware programs or libraries.  They use the locales"
    "to render the text, correctly display the regional monetary values, time and"
    "date formats, alphabetic idiosyncrasies, and other locale-specific standards.")
  local concatStr=$(printf " %s" "${lclTxtArray[@]}")

  helpTxtArray+=("${concatStr:1}")

  lclTxtArray=("Locales define the character sets to be used.  Setting up the"
      "right locale is especially important when the language the language"
      "contains non-ASCII characters.")
  concatStr=$(printf " %s" "${lclTxtArray[@]}")
  helpTxtArray+=(" " "${concatStr:1}" "$DIALOG_BORDER" " ")

  local helpText=$(getTextForDialog "${helpTxtArray[@]}")

  echo -e "$helpText"
}

#---------------------------------------------------------------------------------------
#      METHOD:                  setDlgTextForLocalization
# DESCRIPTION: Text to display the help/more dialog when setting the locale
#  Required Params:
#      1) the default locale
#      2) dialogNum - the number of the specific dialog that will be rendered
#---------------------------------------------------------------------------------------
setDlgTextForLocalization() {
  local dialogNum=$2
  local concatStr=""
  local lclTextArray=("will display the dialog with a list of languages and/or countries"
      "that have locales associated to them.")
  local dispText=$(printf " %s" "${lclTextArray[@]}")
  dispText=("${dispText:1}")

  case ${dialogNum} in
    1)
      lclTextArray=("This dialog lists languages and/or countries that have one"
          "or more locales associated with them.  The list is sorted by the name"
          "of the language and then by the name of the country.")
      concatStr=$(printf " %s" "${lclTextArray[@]}")
      helpTextArray+=("${concatStr:1}" "$DIALOG_BORDER" " ")

      lclTextArray=(
          "       a) A confirmation dialog if there is ONLY 1 locale that is"
          "       b) A radio list dialog of locales that are")
      concatStr=" associated with the language and/or country chosen"
      lclTextArray[-2]+="$concatStr"
      lclTextArray[-1]+="$concatStr"
      concatStr=$(getTextForDialog "${lclTextArray[@]}")
      if [ "$DIALOG" == "yad" ]; then
        helpTextArray+=("   * Clicking the \"Ok\" button will display:" "$concatStr"
        "   * Clicking the \"Exit\" button ")
      else
        helpTextArray+=("   * Selecting < OK > will display:" "$concatStr"
        "   * Selecting <Exit> ")
      fi
      helpTextArray[-1]+="will set the locale and LANG environmental variable to '$1'"
    ;;
    2)
      lclTextArray=("This dialog lists the locales that"
          "are associated with the language and/or country chosen.")
      concatStr=$(printf " %s" "${lclTextArray[@]}")
      helpTextArray+=("${concatStr:1}" "$DIALOG_BORDER" " ")
      if [ "$DIALOG" == "yad" ]; then
        helpTextArray+=("   * Clicking the \"OK\" button " "   * Clicking the \"Cancel\" button ")
      else
        helpTextArray+=("   * Selecting <  Ok  > " "   * Selecting <Cancel> ")
      fi
      helpTextArray[-2]+="will display a dialog asking to confirm the selection made."
      helpTextArray[-1]+="$dispText"
    ;;
    3)
      if [ "$DIALOG" == "yad" ]; then
        helpTextArray+=("   * Clicking the \"Yes\" button " "   * Clicking the \"No\" button ")
      else
        helpTextArray+=("   * Selecting <Yes> " "   * Selecting <No > ")
      fi
      helpTextArray[-2]+="will save/set the locale to the value displayed between []."
      helpTextArray[-1]+="$dispText"
    ;;
  esac
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getHelpTextForHostName
# DESCRIPTION: Text to display on the help/more dialog when setting the host name.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForHostName() {
  local extraDashes=""
  local helpTextArray=("A host name is a unique name created to identify a machine on a network.  This"
    "will be configured in '/etc/hostname'.  Based on the specification \"RFC 1123\", a valid host name:")
  local concatStr=$(printf " %s" "${helpTextArray[@]}")

  if [ "$DIALOG" == "yad" ]; then
    extraDashes="------------"
  fi

  helpTextArray=("URL Reference:" "$URL_REF_BORDER"
    "https://wiki.archlinux.org/index.php/Network_configuration#Set_the_hostname"
    "$DIALOG_BORDER" " " "${concatStr:1}"
    "     1) Is a free-form string up to 64 characters in length"
    "     2) May contain:"
    "         a) only the ASCII case-insensitive letters 'a' through 'z'"
    "         b) the digits '0' through '9'"
    "         c) the hyphen (-), but CANNOT be the first nor the last character"
    "     3) No other symbols, punctuation characters, or white space are permitted."
    "$DIALOG_BORDER" "Other references/resources:"
    "---------------------------$extraDashes" "\"Restrictions on valid hostnames\":"
    "https://en.wikipedia.org/wiki/Hostname#Restrictions_on_valid_hostnames"
    "RFC 1123:" "https://tools.ietf.org/html/rfc1123" "$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER")

  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("  # Clicking the \"Exit\" button ")
  else
    helpTextArray+=("  # Selecting <Exit> ")
  fi

  helpTextArray[-1]+="will store the data in the Installer's 'data' directory and return to the installation guide"

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo -e "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getHelpTextForHostName
# DESCRIPTION: Text to display on the help/more dialog when selecting a network manager.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForNetMans() {
  local extraDashes=""
  local helpTextArray=()
  local concatStr=""
  local menuOptsText=()
  local numOpts=5

  if [ "$DIALOG" == "yad" ]; then
    extraDashes="------------"
  fi

  for optNum in $(eval echo "{1..${numOpts}}"); do
    case ${optNum} in
      1)
        helpTextArray=("#1) ConnMan — Daemon for managing internet connections within embedded devices running the Linux"
            "operating system.  Comes with a command-line client, plus Enlightenment, ncurses, GTK and Dmenu clients"
            "are available.")
      ;;
      2)
        helpTextArray=("#2) netctl — Simple and robust tool to manage network connections via"
            "profiles.  Intended for use with systemd.")
      ;;
      3)
        helpTextArray=("#3) NetworkManager — Manager that provides wired, wireless, mobile broadband and"
            "OpenVPN detection with configuration and automatic connection.")
      ;;
      4)
        helpTextArray=("#4) systemd-networkd — Native systemd daemon that manages network configuration. It"
            "includes support for basic network configuration through udev, which is a device manager"
            "for the Linux kernel.")
      ;;
      5)
        helpTextArray=("#5) Wicd — Wireless and wired connection manager with few"
            "dependencies.  Comes with ncurses and GTK+ interfaces.")
      ;;
    esac
    concatStr=$(printf " %s" "${helpTextArray[@]}")
    menuOptsText+=("${concatStr:1}")
  done

  helpTextArray=("A network manager lets you manage network connection settings in"
    "so called network profiles to facilitate switching networks. ")
  concatStr=$(printf " %s" "${helpTextArray[@]}")

  helpTextArray=("URL Reference:" "$URL_REF_BORDER"
    "https://wiki.archlinux.org/index.php/Network_configuration#Network_managers"
    "${DIALOG_BORDER}${extraDashes}" " " "${concatStr:1}" "${DIALOG_BORDER}${extraDashes}"
    " " "Description of menu options:" "----------------------------$extraDashes")

  for opt in "${menuOptsText[@]}"; do
    helpTextArray+=("       $opt")
  done

  helpTextArray+=("${DIALOG_BORDER}${extraDashes}" " " "NOTE:" "$NOTE_BORDER")

  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("  # Clicking the \"Cancel\" button ")
  else
    helpTextArray+=("  # Selecting <Cancel> ")
  fi

  helpTextArray[-1]+="will default to \"#2) netctl\" as this should already be installed as part of the base installation"

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo -e "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getHelpTextForFormDNS
# DESCRIPTION: Text to display on the help/more dialog when entering the ISP's DNS servers.
#      RETURN: concatenated string
#  Required Params:
#      1) ipVer - IPv4 or IPv6
#---------------------------------------------------------------------------------------
function getHelpTextForFormDNS() {
  local ipVer="$1"
  local helpTextArray=("Setting up a DNS server can potentially help resolve domain"
    "names.  As a refresher, DNS is the Internet’s master phone book. It turns"
    "machine-usable IP addresses into human-readable domain names.  To make this"
    "process faster and easier to manage, the AUR package \"dnsmasq\" will be installed.")
  local concatStr=$(printf " %s" "${helpTextArray[@]}")
  local extraDashes=""
  if [ "$DIALOG" == "yad" ]; then
    extraDashes="------------"
  fi

  helpTextArray=("URL Reference:" "$URL_REF_BORDER" "https://wiki.archlinux.org/index.php/Domain_name_resolution"
    "${DIALOG_BORDER}$extraDashes" " " "${concatStr:1}" "${DIALOG_BORDER}$extraDashes" " ")

  local lclTextArray=("The purpose of this dialog is to get the \"$1\" addresses of the ISP's"
    "DNS servers so that they will be used by \"dnsmasq\" as the preferred servers"
    "to search for a domain name.")

  if [ "$DIALOG" == "yad" ]; then
    lclTextArray+=("  Click the \"Skip\" button")
  else
    lclTextArray+=("  Select < Skip >")
  fi

  lclTextArray+=("to bypass the ISP's DNS servers and select from a list of free servers.  Using"
  "Alternative DNS services are a quick and easy way to increase the"
  "overall Internet experience. Security, privacy and even browsing speed are made better"
  "when people switch their DNS providers to something more robust.  ")
  concatStr=$(printf " %s" "${lclTextArray[@]}")
  helpTextArray+=("${concatStr:1}")

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo -e "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getHelpTextForDnsmasq
# DESCRIPTION: Text to display on the help/more dialog when selecting the preferred
#              and/or Alternate DNS servers for Dnsmasq.
#      RETURN: concatenated string
#  Required Params:
#      1)   ipVer - IPv4 or IPv6
#      2) altFlag - flag to indicate if the help text is either for the preferred
#                   or alternate DNS servers.
#---------------------------------------------------------------------------------------
function getHelpTextForDnsmasq() {
  local ipVer="$1"
  local altFlag=$2
  local serverDesc="preferred"
  local lclTextArray=()
  local helpTextArray=("'dnsmasq' provides a DNS server, a DHCP server with support for DHCPv6 and PXE, and a TFTP"
      "server.  It is designed to be lightweight and have a small footprint.  The AALP-GYGI installer will"
      "configure 'dnsmasq' to cache DNS queries for improved DNS lookup speeds to previously visited"
      "sites.  Not only that, it will also be configured to enable Domain Name System Security"
      "Extensions (DNSSEC) validation.")
  local concatStr=$(printf " %s" "${helpTextArray[@]}")
  local extraDashes=""
  if [ "$DIALOG" == "yad" ]; then
    extraDashes="------------"
  fi

  helpTextArray=("URL Reference:" "$URL_REF_BORDER" "https://wiki.archlinux.org/index.php/dnsmasq"
  "${DIALOG_BORDER}$extraDashes" " " "${concatStr:1}" "${DIALOG_BORDER}$extraDashes" " " )

  if ${altFlag}; then
    serverDesc="alternate"
    lclTextArray=("Alternative DNS services are a quick and easy way to increase the"
        "overall Internet experience. Security, privacy and even browsing speed are made better"
        "when people switch their DNS providers to something more robust.  However, people shouldn’t"
        "rely on these providers alone, as they’re not perfect.  At best, DNS alternatives should be"
        "an addition to your setup, not the sole solution.")
    concatStr=$(printf " %s" "${lclTextArray[@]}")
    helpTextArray+=("${concatStr:1}" "${DIALOG_BORDER}$extraDashes" " ")
  fi

  lclTextArray=("The radio list of DNS providers was generated from Wikipedia's \"Public recursive"
      "name server\" and the Public DNS's \"Best Free Public DNS Server List (Valid October 2018)\" pages"
      "(see links below).  All of them are FREE and support DNSSEC (confirmed by using the \"dig\" command"
      "to send a DNS query with the 'do' (request DNSSEC) flag set).")
  concatStr=$(printf " %s" "${lclTextArray[@]}")

  helpTextArray+=("${concatStr:1}" "${DIALOG_BORDER}$extraDashes" " " "NOTE:" "$NOTE_BORDER"
    "      * The \"$ipVer\" addresses for the 'OpenNIC' option are the closest servers generated by their website.")
  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("      * Clicking the \"Cancel\" button ")
  else
    helpTextArray+=("      * Selecting <Cancel> ")
  fi

  helpTextArray[-1]+="will set the first option in the list to be the '$serverDesc' DNS servers."

  helpTextArray+=("${DIALOG_BORDER}$extraDashes" " "
      "Other references/resources:" "---------------------------$extraDashes"
      "\"Wikipedia\" - https://wikipedia.org/wiki/Public_recursive_name_server"
      "\"Public DNS\" - https://www.publicdns.xyz/")

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo -e "$helpText"
}
