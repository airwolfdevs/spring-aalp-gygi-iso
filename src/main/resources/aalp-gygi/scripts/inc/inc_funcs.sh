#!/bin/bash

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# functions
#===============================================================================


#---------------------------------------------------------------------------------------
#      METHOD:               installDependencyFromFile
# DESCRIPTION: Install the dependency of a package by downloading the file
#              from an Arch repository.
#  Required Params:
#      1)  package - name of the AUR package
#      2) pckgName - full name/description of the AUR package
#      3) repoArch - name of the Arch repository
#---------------------------------------------------------------------------------------
installDependencyFromFile() { 
  local package=$1
  local pckgName=$2
  local repoArch=$3
  local existFlag=$(isPackageInstalled "yaourt" "$package")

  if ${existFlag}; then
    clog_info " "
    clog_info " "
    clog_info "################################################################"
    clog_info "################## $pckgName installed successfully"
    clog_info "################################################################"
  else
    clog_info " "
    clog_info " "
    clog_info "################################################################"
    clog_info "################## Downloading from mirror "
    clog_info "################################################################"
    wget -O /tmp/$package.pkg.tar.xz https://www.archlinux.org/packages/$repoArch/$package/download/ 2>&1

    clog_info " "
    clog_info " "
    clog_info "################################################################"
    clog_info "################## Installing from file $package.pkg.tar.xz"
    clog_info "################################################################"
    yaourt -U --noconfirm /tmp/$package.pkg.tar.xz 2>&1
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     installPackage
# DESCRIPTION: Install the AUR package
#      RETURN: 0 - true, 1 - false because package was already installed
#  Required Params:
#      1)  package - name of the AUR package
#      2) pckgName - full name/description of the AUR package
#---------------------------------------------------------------------------------------
function installPackage() {
  local package=$1
  local pckgName=$2
  local existFlag=$(isPackageInstalled "yaourt" "$package")

  if ${existFlag}; then
    clog_info " "
    clog_info " "
    clog_info "################################################################"
    clog_info "################ $pckgName is already installed"
    clog_info "################################################################"
  else
    clog_info " "
    clog_info " "
    clog_info "################################################################"
    clog_info "################ Installing $pckgName ........"
    clog_info "################################################################"
    yaourt --m-arg "--skipinteg --skipchecksums --skippgpcheck" -S --noconfirm $package 2>&1
    echo 'true' && return 0
  fi

  echo 'false' && return 1
}

#---------------------------------------------------------------------------------------
#      METHOD:                  installPckgFromScript
# DESCRIPTION: Install the AUR package using a custom script
#  Required Params:
#      1)    package - name of the AUR package
#      2)   pckgName - full name/description of the AUR package
#      3)       path - directory path to script
#      4) scriptName - file name of script
#---------------------------------------------------------------------------------------
installPckgFromScript() {
  local package=$1
  local pckgName=$2
  local scriptName="$3/$4"
  local existFlag=$(isPackageInstalled "yaourt" "$package")
  
  if ${existFlag}; then
    clog_info " "
    clog_info " "
    clog_info "################################################################"
    clog_info "################ $pckgName is already installed"
    clog_info "################################################################"
  else
   eval "./$scriptName"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                   installPackageNames
# DESCRIPTION: Install all the AUR packages
#  Required Params:
#      1) pckgNames - array of AUR package names
#---------------------------------------------------------------------------------------
installPackageNames() {
  local pckgNames=("$@")

  for pckg in "${pckgNames[@]}"
  do
    IFS='|' read -r -a splitArray <<< "$pckg"
    len=${#splitArray[@]}
    if [ $len -gt 3 ]; then
      installPckgFromScript "${splitArray[0]}" "${splitArray[1]}" "${splitArray[2]}" "${splitArray[3]}" 2>&1 >> "$INSTALLER_LOG"
    elif [ $len -gt 2 ]; then
      installDependencyFromFile "${splitArray[0]}" "${splitArray[1]}" "${splitArray[2]}" 2>&1 >> "$INSTALLER_LOG"
    else
      installPackage "${splitArray[0]}" "${splitArray[1]}" 2>&1 >> "$INSTALLER_LOG"
    fi
  done
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     verifyPackages
# DESCRIPTION: Verify if all the AUR package names installed
#      RETURN: 0 - true, 1 - false
#  Required Params:
#      1) pckgNames - array of AUR package names
#---------------------------------------------------------------------------------------
function verifyPackages() {
  local pckgNames=("$@")
  local result=$TRUE
  local existFlag=0

  clog_info " "
  clog_info " "
  clog_info "################################################################"
  clog_info "############     Verifying all packages installed   ############"
  clog_info "################################################################"
  for pckg in "${pckgNames[@]}"
  do
    IFS='|' read -r -a splitArray <<< "$pckg"
    existFlag=$(isPackageInstalled "yaourt" "${splitArray[0]}")
    if ${existFlag}; then
      pckgInfo=$(yaourt -Q ${splitArray[0]})
      pckgArray=(`echo $pckgInfo | sed 's/ / /g'`)
      pckgVer=$(echo ${pckgArray[1]})
      echo "############  ${splitArray[1]}:  $pckgVer"
    else
      logf_error "package '${splitArray[0]}' was not found"
      result=$FALSE
    fi
  done

  if [ $result == $TRUE ]; then
    echo 'true' && return 0
  fi
  echo 'false' && return 1
}
