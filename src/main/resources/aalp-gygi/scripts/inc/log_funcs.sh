#!/bin/bash
#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# globals
#===============================================================================
FALSE=1
TRUE=0
HOME_DIR=$(cd $(dirname $0); pwd)

#-----------------------------------------------
#    Control Sequences for Formatting text     #
#-----------------------------------------------
RESET="\e[0m"
BLINK="5;"
BOLD="1;"
DIM="2;"
REVERSE="7;"
UNDERLINE="4;"

#-----------------------------------------------
#               Background Colors              #
#-----------------------------------------------
BCKND_BLUE="\e[48;5;27m"
BCKND_BRIGHT_GREEN="\e[48;5;10m"
BCKND_BRIGHT_BROWN="\e[48;5;202m"
BCKND_BRIGHT_YELLOW="\e[48;5;11m"
BCKND_BROWN="\e[48;5;208m"
BCKND_CYAN="\e[48;5;6m"
BCKND_DARK_BLUE="\e[48;5;21m"
BCKND_DARK_GREEN="\e[48;5;2m"
BCKND_DARK_RED="\e[48;5;1m"
BCKND_DARK_YELLOW="\e[48;5;220m"
BCKND_GREEN="\e[48;5;40m"
BCKND_GREY="\e[48;5;8m"
BCKND_RED="\e[48;5;196m"
BCKND_SILVER="\e[48;5;7m"
BCKND_TEAL="\e[48;5;23mTeal"
BCKND_YELLOW="\e[48;5;226m"

#-----------------------------------------------
#                    Colors                    #
#-----------------------------------------------
BLUE="\e[38;5;27m"
BRIGHT_GREEN="\e[38;5;10m"
BRIGHT_BROWN="\e[38;5;202m"
BRIGHT_YELLOW="\e[38;5;11m"
BROWN="\e[38;5;208m"
CYAN="\e[38;5;6m"
DARK_BLUE="\e[38;5;21m"
DARK_GREEN="\e[38;5;2m"
DARK_RED="\e[38;5;1m"
DARK_YELLOW="\e[38;5;220m"
GREEN="\e[38;5;40m"
GREY="\e[38;5;8m"
RED="\e[38;5;196m"
SILVER="\e[38;5;7m"
TEAL="\e[38;5;23mTeal"
YELLOW="\e[38;5;226m"

ERROR_LEVEL="\e[1;48;5;196m [ERROR] "
INFO_LEVEL="\e[1;48;5;2m [INFO]  "
WARN_LEVEL="\e[1;7;38;5;226m [WARN]  "
FATAL_LEVEL="\e[1;5;48;5;1m [FATAL] "

#===============================================================================
# functions/methods
#===============================================================================

#---------------------------------------------------------------------------------------
#    FUNCTION:                       log
# DESCRIPTION: Inserts the date & time before the text to be printed
#      RETURN: string contenated with the data & time appended with
#              the text passed in
#  Required Params:
#      1) string containing the text to be logged/printed
#---------------------------------------------------------------------------------------
log()       {
  echo -e "$(date +%Y-%m-%d\ %T) <---> $1";
}

#---------------------------------------------------------------------------------------
#   FUNCTIONS:       log_error,log_fatal,log_info,log_warn
# DESCRIPTION: Logs/Prints the text without and ANSI colors and folding
#      RETURN: string contenated with the log level appended with
#              the text passed in
#  Required Params:
#      1) string containing the text to be logged/printed
#---------------------------------------------------------------------------------------
log_error() { 
  echo "$(log "[ERROR]  - $1")"; 
}
log_fatal() { 
  echo "$(log "[FATAL]  - $1")"; 
}
log_info() { 
  echo "$(log "[INFO]   - $1")"; 
}
log_warn() { 
  echo "$(log "[WARN]   - $1")"; 
}


#---------------------------------------------------------------------------------------
#   FUNCTIONS:       clogf_error,clogf_fatal,clogf_info,clogf_warn
# DESCRIPTION: Logs/Prints the text with ANSI colors and folding
#      RETURN: string contenated with the log level appended with
#              the text passed in
#  Required Params:
#      1) string containing the text to be logged/printed
#---------------------------------------------------------------------------------------
clogf_error() {
  #Console width number
  T_COLS=`tput cols`
  echo -e "$(log "${ERROR_LEVEL}${RESET}${RED} - $1${RESET}")" | fold -sw $(( $T_COLS - 1 ));
}
clogf_fatal() {
  #Console width number
  T_COLS=`tput cols`
  echo -e "$(log "${FATAL_LEVEL}${RESET}${DARK_RED} - $1${RESET}")" | fold -sw $(( $T_COLS - 1 ));
}
clogf_info()  {
  #Console width number
  T_COLS=`tput cols`
  echo -e "$(log "${INFO_LEVEL}${RESET}${GREEN} - $1${RESET}")" | fold -sw $(( $T_COLS - 18 ));
}
clogf_warn()  {
  #Console width number
  T_COLS=`tput cols`
  echo -e "$(log "${WARN_LEVEL}${RESET}${YELLOW} - $1${RESET}")" | fold -sw $(( $T_COLS - 1 ));
}

#---------------------------------------------------------------------------------------
#   FUNCTIONS:       clog_error,clog_fatal,clog_info,clog_warn
# DESCRIPTION: Logs/Prints the text with just ANSI colors 
#      RETURN: string contenated with the log level appended with
#              the text passed in
#  Required Params:
#      1) string containing the text to be logged/printed
#---------------------------------------------------------------------------------------
clog_error() {
  echo -e "$(log "${ERROR_LEVEL}${RESET}${RED} - $1${RESET}")";
}
clog_fatal() { 
  echo -e "$(log "${FATAL_LEVEL}${RESET}${DARK_RED} - $1${RESET}")";
}
clog_info()  {
  echo -e "$(log "${INFO_LEVEL}${RESET}${GREEN} - $1${RESET}")";
}
clog_warn()  {
  echo -e "$(log "${WARN_LEVEL}${RESET}${YELLOW} - $1${RESET}")";
}
