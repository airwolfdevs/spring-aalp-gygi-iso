#!/bin/bash

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# functions/methods
#===============================================================================

#---------------------------------------------------------------------------------------
#      METHOD:                       addEspDevice
# DESCRIPTION: Add a line/row in the "$PARTITION_TABLE_FILE" that represents the device
#              for 'EFI system' partition
#  Required Params:
#      1) methodParams - associative array of key/value pairs of parameters for the function:
#              "deviceName" - Name of the partition
#              "deviceType" - Partition
#         "dialogBackTitle" - Specifies a backtitle string to be displayed on the
#                             backdrop, at the top of the screen.
#          "statsBlockSize" - BLOCK_DEVICE_SIZE
#---------------------------------------------------------------------------------------
addEspDevice() {
  local -n methodParams=$1
  local hrSizes=("550 MiB" "1 GiB")
  local row=$(printf "\t%s" "${hrSizes[@]}")

  methodParams["deviceSizes"]=${row:1}

  setDataForPartitionScheme methodParams

  getSizeForEspDevice methodParams
  if [ ${#mbSelVal} -gt 0 ]; then
    appendPartitionToFile "${methodParams[deviceName]}"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                       addBootDevice
# DESCRIPTION: Add a line/row in the "$PARTITION_TABLE_FILE" that represents either
#              the logical volume or partition for /boot
#  Required Params:
#      1) methodParams - associative array of key/value pairs of parameters for the function:
#              "deviceName" - Name of a partition or logical volume
#              "deviceType" - Logical Volume or Partition
#         "dialogBackTitle" - Specifies a backtitle string to be displayed on the
#                             backdrop, at the top of the screen.
#          "statsBlockSize" - either BLOCK_DEVICE_SIZE or PHYSICAL_VOL_SIZE
#---------------------------------------------------------------------------------------
addBootDevice() {
  local -n methodParams=$1
  local hrSizes=("200 MiB" "1 GiB")

  if ${UEFI_FLAG}; then
    hrSizes[0]="550 MiB"
  fi

  local row=$(printf "\t%s" "${hrSizes[@]}")

  methodParams["deviceSizes"]=${row:1}

  setDataForPartitionScheme methodParams

  getSizeForBootDevice methodParams

  if [ "${methodParams[deviceType]}" == "Logical Volume" ] &&  [ ${#mbSelVal} -gt 0 ]; then
    local warnMsg=("/boot cannot reside in LVM when using a boot loader which does not support LVM;"
                    "you must create a separate /boot partition and format it directly.  Only GRUB"
                    "is known to support LVM and this will be the ONLY choice available when it"
                    "is time to install a bootloader!!!")
    local concatStr=$(printf " %s" "${warnMsg[@]}")
    showWarningDialogForBootLV "Adding a '/boot' Logical Volume" "${concatStr:1}"
    if ! ${yesNoFlag}; then
      mbSelVal=""
    fi
  fi

  if [ ${#mbSelVal} -gt 0 ]; then
    if [ "${methodParams[deviceType]}" == "Partition" ]; then
      appendPartitionToFile "${methodParams[deviceName]}"
    else
      appendLogicalVolumeToFile "${methodParams[deviceName]}"
    fi
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                       addRootDevice
# DESCRIPTION: Add a line/row in the "$PARTITION_TABLE_FILE" that represents either
#              the '/' (i.e. root) logical volume or partition
#  Required Params:
#      1) methodParams - associative array of key/value pairs of parameters for the function:
#              "deviceName" - Name of a partition or logical volume
#              "deviceType" - Logical Volume or Partition
#         "dialogBackTitle" - Specifies a backtitle string to be displayed on the
#                             backdrop, at the top of the screen.
#          "statsBlockSize" - either BLOCK_DEVICE_SIZE or PHYSICAL_VOL_SIZE
#---------------------------------------------------------------------------------------
addRootDevice() {
  local -n methodParams=$1
  local row=$(printf "\t%s" "${ROOT_HR_SIZES[@]}")

  methodParams["deviceSizes"]=${row:1}

  setDataForPartitionScheme methodParams

  getSizeForRootDevice methodParams

  if [ "${methodParams[deviceType]}" == "Partition" ]; then
    appendPartitionToFile "${methodParams[deviceName]}"
  else
    appendLogicalVolumeToFile "${methodParams[deviceName]}"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                        runDevManOp
# DESCRIPTION: Run the selected operation chosen to manage the logical volumes
#              or partitions.  The operations that can be run are:
#                      "addCustom" - add a custom logical volume or partition
#                        "addSwap" - add a swap file, or a logical volume or partition
#                         "addVAR" - add a /var logical volume or partition
#                         "remove" - remove a logical volume or partition
#                    "remSwapFile" - remove the swap file option that was chosen
#  Required Params:
#      1) varDeviceName - name of the  logical volume or partition for /var
#      2)  runMethodParams - associative array of key/value pairs of parameters for the method
#---------------------------------------------------------------------------------------
runDevManOp() {
  local varDeviceName="$1"
  local -n runMethodParams=$2
  local key=""
  local lastDevice="${runMethodParams[lastDevice]}"
  local hrMaxAllocSize=$(getMaxAllocSize "$lastDevice")
  local urlRef="https://wiki.archlinux.org/index.php/partitioning#Discrete_partitions"

  if [ "$DIALOG" == "yad" ]; then
    key="partitionLayout"
  else
    key="dialogPartTable"
  fi

  setDataForPartitionScheme runMethodParams

  runMethodParams["dialogTitle"]=$(printf "Available Operations for '%s'" "${runMethodParams[dialogBackTitle]}")
  runMethodParams["maxAllocSize"]="$hrMaxAllocSize"
  setOptionsForManagingDevices "$key" "$varDeviceName" runMethodParams

  getSelectedOperation "$urlRef" runMethodParams

  case "$mbSelVal" in
    "addCustom")
      addCustomDevice "$key" runMethodParams
    ;;
    "addSwap")
      addSwapSpace "${runMethodParams[deviceType]}"
    ;;
    "addVAR")
      runMethodParams["deviceName"]="$varDeviceName"
      addVarDevice runMethodParams
    ;;
    "Cancel")
      cleanKeyAndVals
    ;;
    "Done"|"Extra")
      addLastDevice "$lastDevice"
    ;;
    "remove")
      removeDevice runMethodParams
    ;;
    "remSwapFile")
      unset varMap["SWAP-SIZE"]
      unset varMap["SWAP-TYPE"]
    ;;
  esac
}

#---------------------------------------------------------------------------------------
#      METHOD:               setOptionsForManagingDevices
# DESCRIPTION: Set the choices of the device management operations, which also their
#              descriptions and text for the help dialog.
#  Required Params:
#      1)     devTableKey - "dialogPartTable" or "partitionLayout"
#      2)   varDeviceName - logical volume or partition name for the /var device
#      3)   assocParamAry - associative array of key/value pairs of parameters that the
#                           values described in the description will be added to.  This
#                           will also contain the following key/value pairs:
#              "deviceType" - Logical Volume or Partition
#         "dialogPartTable" - string containing the rows of data for the
#                             partition table in a textual table format
#         "partitionLayout" - string containing header, logical volumes and partitions
#---------------------------------------------------------------------------------------
setOptionsForManagingDevices() {
  local devTableKey="$1"
  local varDeviceName="$2"
  local -n assocParamAry=$3
  local deviceType="${assocParamAry[deviceType]}"
  local deviceTable="${assocParamAry[$devTableKey]}"
  local maxAllocSize=$(humanReadableToBytes "${assocParamAry[maxAllocSize]}")
  local options=("${DEVICE_MANAGEMENT_OPTS[@]}")
  local -A titles=(["addCustom"]="Add Custom $deviceType" ["addSwap"]="Add 'Swap' Space" ["addVAR"]="Add '/var' $deviceType"
    ["remove"]="Remove ${deviceType}s" ["remSwapFile"]="Remove 'Swap' File")
  local dialogChoices=()
  local ary=()
  local deviceCount=0
  local addFlag=$(echo 'false' && return 1)
  local key=""

  if [ "${varMap[PART-TBL-TYPE]}" != "gpt" ]; then
    options=("${DEVICE_MANAGEMENT_OPTS[1]}" "${DEVICE_MANAGEMENT_OPTS[-1]}")
  fi

  for opt in "${options[@]}"; do
    addFlag=$(echo 'false' && return 1)
    case "$opt" in
      "addCustom")
        if [ $maxAllocSize -gt 0 ]; then
          addFlag=$(echo 'true' && return 0)
        fi
      ;;
      "addSwap")
        if [ ! ${varMap["SWAP-TYPE"]+_} ]; then
          addFlag=$(echo 'true' && return 0)
        fi
      ;;
      "addVAR")
        if [ $maxAllocSize -gt 0 ] && [[ ! "$deviceTable" =~ "$varDeviceName" ]]; then
          addFlag=$(echo 'true' && return 0)
        fi
      ;;
      "remove")

        if [[ "${varMap[PART-LAYOUT]}" =~ "LVM" ]]; then
          deviceCount=$(echo "$deviceTable" | grep "lvm-" | wc -l)
          if [ ${deviceCount} -gt 1 ]; then
            addFlag=$(echo 'true' && return 0)
          fi
        else
          deviceCount=$(echo "$deviceTable" | awk '{print $1}' | grep "dev" | wc -l)
          if [ ${deviceCount} -gt 2 ]; then
            addFlag=$(echo 'true' && return 0)
          fi
        fi
      ;;
      "remSwapFile")
        if [ ${varMap["SWAP-TYPE"]+_} ] && [ "${varMap[SWAP-TYPE]}" == "file" ]; then
          addFlag=$(echo 'true' && return 0)
        fi
      ;;
    esac
    if ${addFlag}; then
      key=$(echo "${opt}Text")
      dialogChoices+=("$opt")

      if [ "$DIALOG" == "yad" ]; then
        assocParamAry["$opt"]=$(escapeSpecialCharacters "${titles[$opt]}")
      else
        assocParamAry["$opt"]=$(echo " - ${titles[$opt]}")
      fi
      if [ ${HELP_TEXT_OPTS["$key"]+_} ]; then
        assocParamAry["$key"]="${HELP_TEXT_OPTS[$key]}"
      fi
    fi
  done

  assocParamAry["choices"]=$(printf "\t%s" "${dialogChoices[@]}")
  assocParamAry["choices"]=${assocParamAry["choices"]:1}
}

#---------------------------------------------------------------------------------------
#      METHOD:                       addCustomDevice
# DESCRIPTION: Add the line/row in the "$PARTITION_TABLE_FILE" that represents
#              the custom logical volume or partition (i.e. custom name and size entered)
#  Required Params:
#      1)     devTableKey - "dialogPartTable" or "partitionLayout"
#      2)  methodParams - associative array of key/value pairs of parameters for the method:
#              "deviceName" - Name of a partition or logical volume
#              "deviceType" - Logical Volume or Partition
#         "dialogBackTitle" - Specifies a backtitle string to be displayed on the
#                             backdrop, at the top of the screen.
#              "lastDevice" - the last logical volume or partition (i.e. /home) to be created
#         A) "partitionLayout" - string containing header, logical volumes and partitions
#                                        OR
#         B) "dialogPartTable" - string containing the rows of data for the
#                                partition table in a textual table format
#         C)  "partTableStats" - summary table of the progress in the partitioning process
#---------------------------------------------------------------------------------------
addCustomDevice() {
  local devTableKey="$1"
  local -n methodParams=$2
  local deviceTable="${methodParams[$devTableKey]}"
  local deviceType="${methodParams[deviceType]}"

  methodParams["deviceNames"]=$(getNamesOfExistingDevices "$deviceTable" "$deviceType")

  getCustomDevice methodParams

  if [ ${#mbSelVal} -gt 0 ] && [ ${#ibEnteredText} -gt 0 ]; then
    if [ "$deviceType" == "Partition" ]; then
      appendPartitionToFile "$ibEnteredText"
    else
      appendLogicalVolumeToFile "$ibEnteredText"
    fi
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                       addSwapSpace
# DESCRIPTION: Call the "create-swap.sh" script to either:
#                  A) Create a swap partition
#                  B) Create a swap file
#  Required Params:
#      1) deviceType - Logical Volume or Partition
#---------------------------------------------------------------------------------------
addSwapSpace() {
  declare -p varMap > "$ASSOC_ARRAY_FILE"
  ./create-swap.sh "semiAuto" "$1"
  if [ -f "$UPD_ASSOC_ARRAY_FILE" ]; then
    source -- "${UPD_ASSOC_ARRAY_FILE}"

    varMap=()
    for key in "${!updVarMap[@]}"; do
      varMap["$key"]=$(echo "${updVarMap[$key]}")
    done

    rm -rf "${UPD_ASSOC_ARRAY_FILE}"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                       addVarDevice
# DESCRIPTION: Add the line/row in the "$PARTITION_TABLE_FILE" that represents
#              the logical volume or partition for /var
#  Required Params:
#      1) methodParams - associative array of key/value pairs of parameters for the method:
#              "deviceName" - Name of a partition or logical volume
#              "deviceType" - Logical Volume or Partition
#         "dialogBackTitle" - Specifies a backtitle string to be displayed on the
#                             backdrop, at the top of the screen.
#              "lastDevice" - the last logical volume or partition (i.e. /home) to be created
#         A) "partitionLayout" - string containing header, logical volumes and partitions
#                                        OR
#         B) "dialogPartTable" - string containing the rows of data for the
#                                partition table in a textual table format
#         C)  "partTableStats" - summary table of the progress in the partitioning process
#---------------------------------------------------------------------------------------
addVarDevice() {
  local -n methodParams=$1
  local deviceName="${methodParams[deviceName]}"
  local deviceType="${methodParams[deviceType]}"
  local hrSizes=("$MIN_PART_SIZE" "${methodParams[maxAllocSize]}")
  local row=$(printf "\t%s" "${hrSizes[@]}")

  funcParams["deviceSizes"]=${row:1}

  getSizeForVarDevice methodParams

  if [ ${#mbSelVal} -gt 0 ]; then
    if [ "$deviceType" == "Partition" ]; then
      appendPartitionToFile "$deviceName"
    else
      appendLogicalVolumeToFile "$deviceName"
    fi
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                      cleanKeyAndVals
# DESCRIPTION: Clears key & value pairs from the global associative array "varMap"
#              and removes the file whose name is in the global var "$PARTITION_TABLE_FILE"
#---------------------------------------------------------------------------------------
cleanKeyAndVals() {
  unset varMap["PART-LAYOUT"]
  unset varMap["PART-TBL-TYPE"]
  unset varMap["BOOT-DEVICE"]
  unset varMap["BOOT-LOADER"]
  unset varMap["SWAP-SIZE"]
  unset varMap["SWAP-TYPE"]
  unset varMap["LVM-on-LUKS"]
  unset varMap["VG-NAME"]
  rm -rf "$PARTITION_TABLE_FILE"
}

#---------------------------------------------------------------------------------------
#      METHOD:                       addLastDevice
# DESCRIPTION: Add the line/row in the "$PARTITION_TABLE_FILE" that represents either
#                 A) the logical volume for /home
#                 B) the partition for /home or if MBR, /root
#  Required Params:
#      1) lastDevice - the last logical volume or partition (i.e. /home) to be created
#---------------------------------------------------------------------------------------
addLastDevice() {
  local lastDevice="$1"
  local ary=()
  IFS=$'\t' read -a ary <<< "$lastDevice"
  mbSelVal="${ary[2]}"

  if [[ "${ary[0]}" =~ "dev" ]]; then
    appendPartitionToFile "${ary[1]}"
  else
    appendLogicalVolumeToFile "${ary[1]}"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                      removeDevice
# DESCRIPTION: Remove the line/row in the "$PARTITION_TABLE_FILE" for the partition
#              or logical volume that is NOT to be created
#  Required Params:
#  Required Params:
#      1)     devTableKey - "dialogPartTable" or "partitionLayout"
#      2)  methodParams - associative array of key/value pairs of parameters for the method:
#              "deviceName" - Name of a partition or logical volume
#              "deviceType" - Logical Volume or Partition
#         "dialogBackTitle" - Specifies a backtitle string to be displayed on the
#                             backdrop, at the top of the screen.
#              "lastDevice" - the last logical volume or partition (i.e. /home) to be created
#         A) "partitionLayout" - string containing header, logical volumes and partitions
#                                        OR
#         B) "dialogPartTable" - string containing the rows of data for the
#                                partition table in a textual table format
#         C)  "partTableStats" - summary table of the progress in the partitioning process
#---------------------------------------------------------------------------------------
removeDevice() {
  local -n methodParams=$1
  local tableRows=()

  IFS=$'\n' read -d '' -a tableRows < "$PARTITION_TABLE_FILE"

  setDataForPartitionScheme methodParams
  setChoicesToRemoveDevice tableRows methodParams

  selectDeviceToRemove methodParams

  if [ ${#mbSelVal} -gt 0 ]; then
    removeDeviceFromFile tableRows
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                  setChoicesToRemovePart
# DESCRIPTION: Set the choices & descriptions for the logical volumes or partitions that
#              are eligible to be removed from the partition table
#  Required Params:
#      1)    partTblRows - array containing the rows of data from the partition table
#      2) assocDataArray - associative array where the choices & descriptions will be
#                          appended to.
#---------------------------------------------------------------------------------------
setChoicesToRemoveDevice() {
  local -n partTblRows=$1
  local -n assocDataArray=$2
  local dlgChoices=()
  local rowNum=0
  local cols=()
  local flagsCol=""
  local rowKey=""
  local row=""
  local remFlag=$(echo 'true' && return 0)
  local reqFlag=$(isBootPartReq)
  local devPrefix="$LOGICAL_VOL_PREFIX"

  if [ "${funcParams[deviceType]}" == "Partition" ]; then
    devPrefix="/dev/sda"
  fi

  for row in "${partTblRows[@]}"; do
    rowNum=$(expr $rowNum + 1)
    if [[ "$row" =~ "$devPrefix" ]]; then
      IFS=$'\t' read -a cols <<< "$row"
      flagsCol=$(trimString "${cols[-1]}")
      if [ ${#flagsCol} -lt 1 ] || [[ "$flagsCol" =~ "swap" ]]; then
        remFlag=$(echo 'true' && return 0)
        if [[ "${cols[1]}" =~ "boot" ]] && ${reqFlag}; then
          remFlag=$(echo 'false' && return 1)
        fi
        if ${remFlag}; then
          rowKey=$(printf "row#%02d" "$rowNum")
          dlgChoices+=("$rowKey")
          assocDataArray["$rowKey"]=$(getDeviceNameForListDialog cols)
        fi
      fi
    fi
  done

  assocDataArray["choices"]=$(printf "\t%s" "${dlgChoices[@]}")
  assocDataArray["choices"]=${assocDataArray["choices"]:1}
}

#---------------------------------------------------------------------------------------
#      METHOD:                   setStepsForCmds
# DESCRIPTION: Set each of the lines of commands into a step to be displayed
#              in the help dialog.
#
#  Requirements:  THESE MUST BE DECLARED WITHIN THE CALLING METHOD
#      1) linuxCmdKeys - array of key values to the title & command arrays
#      2)    linuxCmds - associative array of the commands to execute
#      3)     cmdSteps - the formatted lines to display in the help dialog/tab
#---------------------------------------------------------------------------------------
setStepsForCmds() {
  local steps=()
  local len=${#cmds[@]}
  local stepNum=0
  local fmt="%13s step#%02d) %s"
  local splitArray=()
  local addNoteFlag=$(echo 'false' && return 1)
  local sep="|"

  if [ "$DIALOG" == "yad" ]; then
    fmt="%6s step#%02d) %s"
  fi

  for key in "${linuxCmdKeys[@]}"; do
    stepNum=$(expr $stepNum + 1)
    local cmd=$(printf "$fmt" " " ${stepNum} "${linuxCmds["$key"]}")
    if [[ "$cmd" =~ "cryptsetup" ]]; then
      readarray -t splitArray <<< "${cmd//$sep/$'\n'}"
      cmd=$(printf "echo 'passphrase' |%s" "${splitArray[-1]}")
      cmd=$(printf "$fmt" " " ${stepNum} "$cmd")
      addNoteFlag=$(echo 'true' && return 0)
    fi
    cmdSteps+=("$cmd")
  done

  if ${addNoteFlag}; then
    local passPhraseNote=$(printf "\n%s %s" \
        "       NOTE:  The 'passphrase' is the password that was entered in the process of setting up the" \
        " 'LVM Partition' to be encrypted with 'LUKS'")
    cmdSteps+=("$passPhraseNote")
  fi
}
