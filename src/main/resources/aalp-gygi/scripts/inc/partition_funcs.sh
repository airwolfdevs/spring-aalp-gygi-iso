#!/bin/bash
#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# globals
#===============================================================================
declare varDeviceTextArray=(
"The '/var' directory stores variable data such as spool directories"
"and files, administrative and logging data, pacman's (package manager)"
"cache, the Arch Build System(ABS) tree, etc. Keeping it in a separate"
"avoids running out of disk space due to flunky logs, etc."
)
declare -A UNIT_BLOCK_SIZES=(["GiB"]=2097152 ["MiB"]=2048)

#===============================================================================
# functions
#===============================================================================

#---------------------------------------------------------------------------------------
#    FUNCTION:                 bytesToHumanReadable
# DESCRIPTION: Converts the bytes passed in to human-readable format
#      RETURN: string containing human-readable value
#  Required Params:
#      1) sizeInBytes - bytes to be converted
#---------------------------------------------------------------------------------------
function bytesToHumanReadable() {
  local sizeInBytes=$1
  local units="B KiB MiB GiB TiB PiB"
  local unit=""
  for unit in $units; do
    test ${sizeInBytes%.*} -lt 1024 && break;
    sizeInBytes=$(echo "$sizeInBytes / 1024" | bc -l)
  done

  local bytesToHR=$(printf "%.1f %s\n" "$sizeInBytes" "$unit")

  echo "$bytesToHR"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  humanReadableToBytes
# DESCRIPTION: Converts human-readable format to the number of bytes it represents
#      RETURN: integer of the number of bytes
#  Required Params:
#      1) string - human-readable format
#---------------------------------------------------------------------------------------
function humanReadableToBytes() {
  local hrToBytes=$(echo $1 | awk \
      'BEGIN{IGNORECASE = 1}
       function printpower(n,b,p) {printf "%u\n", n*b^p; next}
       /[0-9]$/{print $1;next};
       /K(iB)?$/{printpower($1,  2, 10)};
       /M(iB)?$/{printpower($1,  2, 20)};
       /G(iB)?$/{printpower($1,  2, 30)};
       /T(iB)?$/{printpower($1,  2, 40)};
       /B$/{     printpower($1, 10,  3)};
       /KB$/{    printpower($1, 10,  3)};
       /MB$/{    printpower($1, 10,  6)};
       /GB$/{    printpower($1, 10,  9)};
       /TB$/{    printpower($1, 10, 12)}')
  echo "$hrToBytes"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  humanReadableToBlocks
# DESCRIPTION: Converts human-readable format to the number of blocks it represents
#      RETURN: integer of the number of bytes
#  Required Params:
#      1) string - human-readable format
#---------------------------------------------------------------------------------------
function humanReadableToBlocks() {
  local hrSize="$1"
  local sizeUnit=(${hrSize// / })
  local blockSize=${UNIT_BLOCK_SIZES["${sizeUnit[1]}"]}
  blockSize=$(echo "scale=3; ${sizeUnit[0]}*${blockSize}" | bc)

  echo "$blockSize"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     isValidSize
# DESCRIPTION: Check if the size entered for a partition is a valid
#              human-readable format and falls within the minimum and maximum
#      RETURN: 0 - true, 1 - false
#  Required Params:
#      1) inputSize - value entered in human-readable format
#      2)   minSize - minimum value in human-readable format
#      3)   maxSize - maximum value in human-readable format
#---------------------------------------------------------------------------------------
function isValidSize() {
  local inputSize=$(echo "$1")
  local minSize=$(humanReadableToBytes "$2")
  local maxSize=$(humanReadableToBytes "$3")
  local hrToBytes=$(humanReadableToBytes "$inputSize")

  if [ ${hrToBytes} -lt ${minSize} ] || [ ${hrToBytes} -gt ${maxSize} ]; then
    echo 'false' && return 1
  fi

  echo 'true' && return 0
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     isPartitioned
# DESCRIPTION: Check if a device has been partitioned
#      RETURN: 0 - true, 1 - false
#  Required Params:
#      1) blockDevice - string value of the device (i.e. "/dev/sda")
#---------------------------------------------------------------------------------------
function isPartitioned() {
  local blockDevice=$1
  local cmdOutput=$(parted ${blockDevice} print -s 2>&1)
  grep "unknown" <<< "${cmdOutput}" >/dev/null 2>&1

  if [ "${?}" -eq 0 ]; then
    echo 'false' && return 1
  else
    cmdOutput=$(lsblk "$blockDevice" | grep 'part\|lvm' | wc -l)
    if [ ${cmdOutput} -lt 1 ]; then
      echo 'false' && return 1
    fi
  fi

  echo 'true' && return 0
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getPartFmtMntCounts
# DESCRIPTION: Gets the counts of
#               1) the number of partitions and/or logical volumes
#               2) those that have been formatted
#               3) those that have been mounted
#      RETURN: Comma separated values of the counts
#  Required Params:
#      1) blockDevice - string value of the device (i.e. "/dev/sda")
#---------------------------------------------------------------------------------------
function getPartFmtMntCounts() {
  local cmdOutput=$(lsblk "$1" | grep 'part\|lvm' | awk '{print substr($1,3)":"$4":"$6":"$7}')
  local lines=()
  local fields=()
  local blockDevices=()
  local blockDevice=""
  local mntPt=""
  local line=""
  local retVal=""
  local fsCnt=0
  local mntCnt=0
  local existFlag=0
  local sep=":"

  if [[ "${varMap["PART-LAYOUT"]}" =~ "LVM" ]]; then
    fsCnt=1
  fi

  readarray -t lines <<< $cmdOutput

  for line in "${lines[@]}"; do
    readarray -t fields <<< "${line//$sep/$'\n'}"
    if [ "${fields[2]}" == "lvm" ]; then
      fields[0]=$(echo "/dev/mapper/${fields[0]}")
    else
      fields[0]=$(echo "/dev/${fields[0]}")
    fi
    line=$(printf ":%s" "${fields[@]}")
    line=${line:1}
    blockDevices+=("$line")
  done

  for line in "${blockDevices[@]}"; do
    readarray -t fields <<< "${line//$sep/$'\n'}"
    blockDevice=$(echo "${fields[0]}")

    existFlag=$(isFormatted "$blockDevice")
    if ${existFlag}; then
      fsCnt=$(expr $fsCnt + 1)
    fi

    existFlag=$(isMounted "$blockDevice")
    if ${existFlag}; then
      mntCnt=$(expr $mntCnt + 1)
    fi
  done

  echo "${#blockDevices[@]},$fsCnt,$mntCnt"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     isFormatted
# DESCRIPTION: Check if the partition or logical volume has been formatted with
#              a file system type
#      RETURN: 0 - true, 1 - false
#  Required Params:
#      1) blockDevice - a partition or logical device
#---------------------------------------------------------------------------------------
function isFormatted() {
  local blockDevice=$1
  local output=$(file -L -s "${blockDevice}")
  grep filesystem <<< "${output}" > /dev/null 2>&1

  if [ "${?}" -eq 0 ]; then
    echo 'true' && return 0
  fi

  echo 'false' && return 1
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                       isMounted
# DESCRIPTION: Check if the partition or logical volume has been attached to a
#              directory on the file system
#      RETURN: 0 - true, 1 - false
#  Required Params:
#      1) blockDevice - a partition or logical device
#---------------------------------------------------------------------------------------
function isMounted() {
  local blockDevice=$1
  local lineCnt=$(findmnt --source "${blockDevice}" -n | wc -l 2>&1)

  if [ ${lineCnt} -gt 0 ]; then
    echo 'true' && return 0
  fi

  echo 'false' && return 1
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                      isRootDevice
# DESCRIPTION: Check if the partition or logical volume is the root (i.e. '/')
#      RETURN: 0 - true, 1 - false
#  Required Params:
#      1) str - assumption is that this is either:
#                  A) a line from the "gdisk" command
#                  B) a line from 'lsblk' command that contains the
#                     "$MOUNTPOINT" column
#                  C) name of a partition or logical volume
#---------------------------------------------------------------------------------------
function isRootDevice() {
  local str=$1
  local flag=$FALSE

  shopt -s nocasematch
  if [[ "$str" =~ "root" ]] || [ "$str" == "/" ]; then
    flag=$TRUE
  fi
  shopt -u nocasematch

  if [ ${flag} == $TRUE ]; then
    echo 'true' && return 0
  else
    echo 'false' && return 1
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getMaxForDevice
# DESCRIPTION: Calculate the maximum number of bytes that the device can be
#              allocated to.  This calculation makes sure that the /home partition
#              will have at least "30 GiB" and that the device is at least "20 GiB"
#      RETURN: human-readable format of the max size
#  Required Params:
#      1) str - human-readable format of the total number of bytes remaining
#---------------------------------------------------------------------------------------
function getMaxForDevice() {
  local hrTotBytesRem=$1
  local totBytesRem=$(humanReadableToBytes "$hrTotBytesRem")
  local minBytesForHome=$(humanReadableToBytes "$HOME_PART_SIZE")
  local minBytesForPart=$(humanReadableToBytes "$MIN_PART_SIZE")
  local oneGig=$(humanReadableToBytes "1 GiB")
  local bytesForPart=$(expr $totBytesRem - $minBytesForHome)
  local bytesToHR=""

  minBytesForPart=$(expr $minBytesForPart - $oneGig)
  if [ $bytesForPart -gt $minBytesForPart ]; then
    bytesToHR=$(bytesToHumanReadable "$bytesForPart")
  fi

  echo "$bytesToHR"
}

#---------------------------------------------------------------------------------------
#      METHOD:               writePartitionTableToFile
# DESCRIPTION: Creates a text file that contains information about each partition
#              and/or logical volume.  It merges the output of the 'gdisk' and
#              'parted' linux commands.
#---------------------------------------------------------------------------------------
writePartitionTableToFile() {
  local lines=()
  local fields=()
  local assocKey=""
  local str=""
  local line=""
  local -A flagMap=()
  local -A fsMap=()
  local flagsCol=""
  local row=""
  local fileRows=()
  local partitionTable=$(gdisk "$BLOCK_DEVICE" -l | awk '$1+0' | sed -e 's/^ *//g' | sed -e 's/  /:/g' | sed -e 's/:::*/:/g')
  local partedOutput=$(parted -s "$BLOCK_DEVICE" print | awk '$1+0' | sed -e 's/^ *//g' | sed -e 's/  /:/g' | sed -e 's/: */:/g' | sed -e 's/::*/:/g')

  local cmdOut=$(lsblk -l /dev/sda1 | awk '{print $7}')
  readarray -t lines <<< "$cmdOut"
  local len=${#lines[@]}

  readarray -t lines <<< "$partedOutput"

  #####  Assuming first line is bootloader, which doesn't have a mount point
  if [ ${len} -lt 2 ]; then
    readarray -t fields <<< "${lines[0]//$':'/$'\n'}"
    str=$(trimString "${fields[-1]}")
    assocKey=$(printf "/dev/sda%d" "${fields[0]}")
    flagMap["$assocKey"]="$str"
  fi

  for line in "${lines[@]:1}"; do
    readarray -t fields <<< "${line//$':'/$'\n'}"
    local pos=$(expr ${#fields[@]} - 2)
    str=$(trimString "${fields[$pos]}")
    str=(${str// / })
    assocKey=$(printf "/dev/sda%d" "${fields[0]}")

    if [[ "$str" =~ "swap" ]]; then
      flagMap["$assocKey"]="$str"
    elif [ ${FILE_SYSTEMS_DESCS["$str"]+_} ]; then
      fsMap["$assocKey"]="$str"
    elif [[ "$line" =~ "lvm" ]]; then
      str=$(trimString "${fields[-1]}")
      flagMap["$assocKey"]="$str"
    fi
  done

  if [ -f "$PARTITION_TABLE_FILE" ]; then
    rm -rf "$PARTITION_TABLE_FILE"
  fi

  readarray -t lines <<< "$partitionTable"
  for line in "${lines[@]}"; do
    row=$(getColumnData "$line")
    flagsCol=$(getFlagsForPartition "$row")
    row+=$(appendFileSystemAndDir "$row" "$flagsCol")
    line=$(printf "\t%s" "$row" "$flagsCol")
    line=${line:1}
    fileRows+=("$line")
  done

  str=$(printf "\n%s" "${fileRows[@]}")
  str=${str:1}
  echo -e "$str" > "$PARTITION_TABLE_FILE"

  export LVM_SUPPRESS_FD_WARNINGS=0
  local existFlag=$(isLVM "$BLOCK_DEVICE")
  if ${existFlag}; then
    appendLogicalVolumes
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                      getColumnData
# DESCRIPTION: Gets the data for the columns
#                 1) Partition Number
#                 2) Partition Name
#                 3) Partition Size
#                 4) GUID
#      RETURN: Concatenated string of the values for the columns delimited by '\t'
#  Required Params:
#      1) outputLine - string from a row of the partition table from
#                      the 'gdisk' command
#---------------------------------------------------------------------------------------
function getColumnData() {
  local outputLine="$1"
  local line=$(echo "$outputLine" | sed -e 's/: /:/g')
  local fields=()
  local fieldNum=0
  local hrToBytes=0
  local bytesToHR=""
  local device=""
  local partName=""
  local guid=""

  IFS=':' read -a fields <<< "$line"
  fieldNum=1
  for field in "${fields[@]}"; do
    case "$fieldNum" in
      1) #partition number
        device=$(printf "%s%d" "$BLOCK_DEVICE" "$field")
      ;;
      4) #size
        hrToBytes=$(humanReadableToBytes "$field")
        bytesToHR=$(bytesToHumanReadable "$hrToBytes")
      ;;
      5) #GUID
        guid=${field^^}
     ;;
      6) #name
        case "$guid" in
          "EF00")
            partName="EFI System"
          ;;
          "EF02")
            partName="BIOS Boot"
          ;;
          "8E00")
            partName="Linux LVM"
          ;;
          *)
            partName=$(printf "%s" "$field")
        esac
      ;;
    esac
    fieldNum=$(expr $fieldNum + 1)
  done

  local row=$(printf "%s\t%s\t%s\t%s" "$device" "$partName" "$bytesToHR" "$guid")
  echo -e "$row"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getFlagsForPartition
# DESCRIPTION: Get the flags for the current row that will be added to the global
#              file name "$PARTITION_TABLE_FILE"
#      RETURN: Concatenated string of the values
#  Required Params:
#      1) row - contains the data for the columns from the function getColumnData()
#---------------------------------------------------------------------------------------
function getFlagsForPartition() {
  local row="$1"
  local fields=()
  local flagsCol=" "

  IFS=$'\t' read -a fields <<< "$row"
  local device=$(echo "${fields[0]}")
  local partName=$(echo "${fields[1]}")
  local guid=$(echo "${fields[3]}")
  local existFlag=$(isRootDevice "$row")

  if [ ${flagMap["$device"]+_} ]; then
    if ${existFlag}; then
      flagsCol=$(printf "%s, ROOT" "${flagMap[$device]}")
    else
      flagsCol=$(echo "${flagMap[$device]}")
    fi
  elif ${existFlag}; then
    flagsCol="ROOT"
  elif [ "$guid" == "8200" ]; then
    flagsCol="linux-swap(v1)"
  fi

  echo -e "$flagsCol"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  appendFileSystemAndDir
# DESCRIPTION: Append the file system type and directory name/path to the current
#              row that will be added to the global file name "$PARTITION_TABLE_FILE"
#      RETURN: Concatenated string of the values for the columns delimited by '\t'
#  Required Params:
#      1) row - contains the data for the columns from the function getColumnData()
#      2) flagsCol - the flags for the current row
#                    (see function getFlagsForPartition())
#---------------------------------------------------------------------------------------
function appendFileSystemAndDir() {
  local row="$1"
  local flagsCol="$2"
  local fileSystem=" "
  local dirName=" "

  IFS=$'\t' read -a fields <<< "$row"
  local device=$(echo "${fields[0]}")
  local partName=$(echo "${fields[1]}")
  local guid=$(echo "${fields[3]}")

  case "$guid" in
    "EF00")
      fileSystem="vfat"
    ;;
    "EF02")
      fileSystem=" "
      dirName=" "
    ;;
    *)
      if [[ "$flagsCol" =~ "ROOT" ]]; then
        dirName="/"
      else
        fields=(${partName// / })
        dirName=${fields[0]}
        shopt -s nocasematch
        if [[ "$dirName" =~ "Linux" ]]; then
          dirName=" "
        elif [[ "$dirName" != /* ]]; then
          dirName=$(echo "/$dirName")
        fi
        shopt -u nocasematch
      fi
      if [ ${fsMap["$device"]+_} ]; then
        fileSystem=$(echo "${fsMap[$device]}")
      fi
    ;;
  esac

  local data=$(printf "\t%s\t%s" "$fileSystem" "$dirName")
  echo -e "$data"
}

#---------------------------------------------------------------------------------------
#      METHOD:                 appendLogicalVolumes
# DESCRIPTION: Append the logical volumes to the global file name "$PARTITION_TABLE_FILE"
#---------------------------------------------------------------------------------------
appendLogicalVolumes() {
  local rows=()
  local lsblkOutput=$(lsblk "$BLOCK_DEVICE" | grep 'lvm' | awk '{print substr ($1,3)":"$4":"$7}')
  local row=""
  local aryIdx=0
  local vgName=""
  local lvNames=$(getLogicalVolumeNames)
  local lvName=""
  local lvscanRows=()
  local lvRows=()

  IFS=$'\t' read -a lvscanRows <<< "$lvNames"
  vgName=$(echo "${lvscanRows[0]}")
  lvscanRows=("${lvscanRows[@]:1}")

  IFS=$'\n' read -d '' -r -a rows <<< "$lsblkOutput"
  for row in "${rows[@]}"; do
    lvName=$(echo "${lvscanRows[$aryIdx]}")
    lvRow=$(getLogicalVolume "$vgName" "$lvName" "$row")
    lvRows+=("$lvRow")
    aryIdx=$(expr $aryIdx + 1)
  done

  lvRows[-1]=$(setPrefixForLastRow "${lvRows[-1]}")

  local lvs=$(printf "\n%s" "${lvRows[@]}")
  lvs=${lvs:1}
  echo -e "$lvs" >> "$PARTITION_TABLE_FILE"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getLogicalVolumeNames
# DESCRIPTION: Get the name of the volume group and the logical volumes inside
#              the group using the linux 'lvscan' command
#      RETURN: Concatenated string delimited by '\t' where the first position
#              is the name of the volume group
#---------------------------------------------------------------------------------------
getLogicalVolumeNames() {
  local lvscanOutput=$(lvscan | awk '{print $2":"$3" "$4}')
  local names=()
  local rows=()
  local len=0
  local vgName=""

  IFS=$'\n' read -d '' -r -a rows <<< "$lvscanOutput"

  for row in "${rows[@]}"; do
    IFS=$':' read -a cols <<< "$row"
    len=$(expr ${#cols[0]} - 2)
    lvName=$(echo "${cols[0]:1:${len}}")
    IFS=$'/' read -a cols <<< "$lvName"
    vgName=$(echo "${cols[2]}")
    lvName=$(echo "${cols[-1]}")
    names+=($(echo "$lvName"))
  done

  local lvNames=$(printf "\t%s" "$vgName" "${names[@]}")
  lvNames=${lvNames:1}
  echo -e "$lvNames"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getLogicalVolume
# DESCRIPTION: Get the logical volume
#      RETURN: Concatenated string delimited by '\t'
#  Required Params:
#      1)  vgGrp - the name of the volume group
#      2) lvName - the name of the logical volume
#      3)    row - a row/line from output of the linux "lsblk" command of type lvm
#---------------------------------------------------------------------------------------
getLogicalVolume() {
  local vgName="$1"
  local lvName="$2"
  local row="$3"
  local colNum=1
  local col=""
  local cols=()
  local hrToBytes=0
  local lvCols=()
  local rows=()
  local lvRow=""
  local cmdOut=""
  local lvDevice=$(printf "%s%s" "$LVM_DEVICE_PREFIX" "$vgName")

  IFS=$':' read -d '' -r -a cols <<< "$row"

  for col in "${cols[@]}"; do
    case "$colNum" in
      1) #logical volume device & name
        lvRow=$(printf "%s\t%s" "$lvDevice" "$lvName")
      ;;
      2) #size
        hrToBytes=$(humanReadableToBytes "$col")
        col=$(bytesToHumanReadable "$hrToBytes")
        lvRow+=$(printf "\t%s" "$col")
      ;;
      3) #guid, file system, mount pt
        cmdOut=$(blkid "/dev/$vgName/$lvName" | awk '{print $3}')
        if [ ${#cmdOut} -gt 0 ]; then
          cmdOut=$(echo "$cmdOut" | cut -d"=" -f2)
          local len=$(expr ${#cmdOut} - 2)
          if [ ${len} -gt 0 ]; then
            cmdOut=${cmdOut:1:${len}}
          else
            cmdOut=" "
          fi
        else
          cmdOut=" "
        fi
        #eval "$cmdOut"
        lvRow+=$(printf "\tlvm\t%s\t%s" "$cmdOut" "$col")
      ;;
    esac
    colNum=$(expr $colNum + 1)
  done
  col=$(getLogicalVolumeFlags "$row")
  lvRow+=$(printf "\t%s" "$col")

  echo -e "$lvRow"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getLogicalVolumeFlags
# DESCRIPTION: Get the flags associated with a logical volume
#              (i.e. "ROOT" for '/' or root logical volume)
#      RETURN: string containing the names of the flags
#---------------------------------------------------------------------------------------
function getLogicalVolumeFlags() {
  local lv="$1"
  local lvFlags=" "
  local existFlag=$(isRootDevice "$lv")

  if ${existFlag}; then
    lvFlags="ROOT"
  else
    existFlag=$(isSwapPart "$lv")
    if ${existFlag}; then
      lvFlags="linux-swap(v1)"
    fi
  fi

  echo "$lvFlags"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  setPrefixForLastRow
# DESCRIPTION: Set prefix in the device column from "$LVM_DEVICE_PREFIX"
#              to "$LAST_LVM_PREFIX" for last row of logical volumes
#      RETURN: concatenated string for the last logical volume
#---------------------------------------------------------------------------------------
function setPrefixForLastRow() {
  local row="$1"
  local cols=()

  IFS=$'\t' read -a cols <<< "$row"
  cols[0]=$(printf "%s%s" "$LAST_LVM_PREFIX" "$vgName")
  row=$(printf "\t%s" "${cols[@]}")
  row=${row:1}
  echo -e "$row"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                      isSwapPart
# DESCRIPTION: Check if the partition or logical volume is the swap
#      RETURN: 0 - true, 1 - false
#  Required Params:
#      1) str - assumption is that this is either:
#                  A) a line from the "gdisk" command
#                  B) a line from "lsblk" command that contains the
#                     'MOUNTPOINT' column
#                  C) name of a partition or logical volume
#---------------------------------------------------------------------------------------
function isSwapPart() {
  local str=$1
  local flag=$FALSE

  shopt -s nocasematch
  if [[ "$str" =~ "swap" ]]; then
    flag=$TRUE
  fi
  shopt -u nocasematch

  if [ ${flag} == $TRUE ]; then
    echo 'true' && return 0
  else
    echo 'false' && return 1
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:               isValidHumanReadableFmt
# DESCRIPTION: Check if the value is a valid human-readable format
#      RETURN: 0 - true, 1 - false
#  Required Params:
#      1) hrVal - human-readable format value to be checked
#---------------------------------------------------------------------------------------
function isValidHumanReadableFmt() {
  local hrVal=$(echo "$1")
  local hrToBytes=$(humanReadableToBytes "$hrVal")

  if [ "${#hrToBytes}" -gt 0 ]; then
    echo 'true' && return 0
  fi

  echo 'false' && return 1
}

#---------------------------------------------------------------------------------------
#      METHOD:               runLinuxPartitionProgram
# DESCRIPTION: Run the linux partition program
#      1) blockDevice - string value of the device (i.e. "/dev/sda")
#      2) linuxProg - linux program to run
#---------------------------------------------------------------------------------------
runLinuxPartitionProgram() {
  local blockDevice="$1"
  local linuxProg=$(echo "$2" | awk '{print tolower($0)}')
  local runCmd=""

  case $linuxProg in
    parted)
      runCmd="parted -a opt ${blockDevice}"
    ;;
    *)
      runCmd="$linuxProg ${blockDevice}"
    ;;
  esac

  if [ "$DIALOG" == "yad" ]; then
    case $linuxProg in
      gparted)
        ${runCmd} > /dev/null 2>&1
      ;;
      *)
        xfce4-terminal --maximize --color-text=lightgreen --hide-menubar -e "$runCmd" > /dev/null 2>&1
      ;;
    esac
  else
    ${runCmd}
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    isBootPartReq
# DESCRIPTION: Check if the /boot partition is required
#      RETURN: 0 - true, 1 - false
#---------------------------------------------------------------------------------------
function isBootPartReq() {
  local rows=()

  readarray -t rows < "$PARTITION_TABLE_FILE"

  local len=${#rows[@]}

  if [ $len -lt 1 ]; then
    echo 'true' && return 0
  fi

  echo 'false' && return 1
}

#---------------------------------------------------------------------------------------
#      METHOD:                  setChoicesToRemovePart
# DESCRIPTION: Set the choices & descriptions for the partitions that are eligible to be
#              removed from the partition table
#  Required Params:
#      1)     rowChoices - array of eligible partitions that will be appended to
#      2) rowChoiceDescs - associative array of key/value pairs of choices & their
#                          descriptions that will be appended to
#---------------------------------------------------------------------------------------
setChoicesToRemovePart() {
  local -n partTblRows=$1
  local -n partChoices=$2
  local -n partChoiceDescs=$3
  local rowNum=0
  local cols=()
  local flagsCol=""
  local rowKey=""
  local row=""
  local remFlag=$(echo 'true' && return 0)
  local reqFlag=$(isBootPartReq)

  for row in "${partTblRows[@]}"; do
    rowNum=$(expr $rowNum + 1)
    IFS=$'\t' read -a cols <<< "$row"
    flagsCol=$(trimString "${cols[-1]}")
    if [ ${#flagsCol} -lt 1 ] || [[ "$flagsCol" =~ "swap" ]]; then
      remFlag=$(echo 'true' && return 0)
      if [[ "${cols[1]}" =~ "boot" ]] && ${reqFlag}; then
        remFlag=$(echo 'false' && return 1)
      fi
      if ${remFlag} && [[ "${cols[0]}" =~ "/dev/sda" ]]; then
        rowKey=$(printf "row#%02d" "$rowNum")
        partChoices+=("$rowKey")
        partChoiceDescs["$rowKey"]=$(printf "'%s'--'%s': %s" "${cols[0]}" "${cols[1]}" "${cols[2]}")
      fi
    fi
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                   removeDeviceFromFile
# DESCRIPTION: Remove the row number contained in the global variable "$mbSelVal"
#              from the file whose name is stored in the global variable "$PARTITION_TABLE_FILE"
#  Required Params:
#      1) partTblRows - array containing the current set of rows in the file
#---------------------------------------------------------------------------------------
removeDeviceFromFile() {
    local -n partTblRows=$1
    local rowNum=$(echo "$mbSelVal" | cut -d"#" -f2)
    rowNum=$(expr $rowNum - 1)
    local fileRows=("${partTblRows[@]:0:${rowNum}}")
    local row=$(echo "${partTblRows[$rowNum]}")

    if [[ "$row" =~ "swap" ]]; then
      unset varMap["SWAP-SIZE"]
      unset varMap["SWAP-TYPE"]
    fi

    unset partTblRows[$rowNum]
    for row in "${partTblRows[@]:${rowNum}}"; do
      if [[ "$row" =~ "/dev/sda" ]]; then
        IFS=$'\t' read -a cols <<< "$row"
        rowNum=$(expr $rowNum + 1)
        cols[0]=$(printf "/dev/sda%d" "$rowNum")
        row=$(printf "\t%s" "${cols[@]}")
        row=${row:1}
      fi
      fileRows+=("$row")
    done

    local allRows=$(printf "\n%s" "${fileRows[@]}")
    allRows=${allRows:1}
    echo -e "$allRows" > "$PARTITION_TABLE_FILE"
}

#---------------------------------------------------------------------------------------
#      METHOD:                     findEligibleDevices
# DESCRIPTION: Find the devices that can be partitioned and where Arch Linux can be
#              installed.  Set the names within the array "devices" that was declared
#              in the calling function.
#---------------------------------------------------------------------------------------
findEligibleDevices() {
  local cmdOutput=$(lsblk -d | awk '{print "/dev/" $1}' | grep 'sd\|hd\|vd\|nvme\|mmcblk');
  readarray -t devices <<< "$cmdOutput"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getAttachedDevices
# DESCRIPTION: Text in tabular format of all the devices attached to the
#              machine/VM
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getAttachedDevices() {
  local attachedDevices=$(lsblk -lnp -I 2,3,8,9,22,34,56,57,58,65,66,67,68,69,70,71,72,91,128,129,130,131,132,133,134,135,259 | awk '{print $1,$4,$6,$7}'| column -t | sed -e 's/^ *//g' | sed -e 's/  /:/g' | sed -e 's/:::*/:/g')
  local lines=()
  local fields=()
  local cols=()
  local line=""
  local hdrDtl=$(printf "%s;%s;%s;%s" "Device Name" "Size" "Type" "Mount Pt")
  local tableRows=()
  local tbl=""

  tableRows+=("$hdrDtl")
  IFS=$'\n' read -d '' -r -a lines <<< "$attachedDevices"
  for line in "${lines[@]}"; do
    IFS=$':' read -a fields <<< "$line"

    cols=()
    for field in "${fields[@]}"; do
      cols+=($(trimString "$field"))
    done

    if [ ${#cols[@]} -lt 4 ]; then
      cols+=(" ")
    fi
    line=$(printf ";%s" "${cols[@]}")
    line=${line:1}
    tableRows+=("$line")
  done

  attachedDevices=$(printf "\n%s" "${tableRows[@]}")
  attachedDevices=${attachedDevices:1}
  tbl=$(printTable ';' "$attachedDevices" | sed 's/^\-\-\-/   /')
  printf "Attached Devices:\n%s" "$tbl"
}


#---------------------------------------------------------------------------------------
#    FUNCTION:                         isLVM
# DESCRIPTION: Check if the partition layout/scheme contains logical volumes
#      RETURN: 0 - true, 1 - false
#  Required Params:
#      1) blockDevice - string value of the device (i.e. "/dev/sda")
#---------------------------------------------------------------------------------------
function isLVM() {
  local lineCnt=$(lsblk "$1" | grep 'lvm' | wc -l 2>&1)

  if [ "$lineCnt" -lt 1 ]; then
    echo 'false' && return 1
  fi

  echo 'true' && return 0
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                          isLUKS
# DESCRIPTION: Check if the partition layout/scheme contains a LUKS encrypted container
#      RETURN: 0 - true, 1 - false
#  Required Params:
#      1) blockDevice - string value of the device (i.e. "/dev/sda")
#---------------------------------------------------------------------------------------
function isLUKS() {
  local lineCnt=$(lsblk "$1" | grep 'crypt' | wc -l 2>&1)

  if [ "$lineCnt" -lt 1 ]; then
    echo 'false' && return 1
  fi

  echo 'true' && return 0
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getDeviceNameForPV
# DESCRIPTION: Find the name of the device for the physical volume
#      RETURN: name of the device (i.e. /dev/sda#)
#---------------------------------------------------------------------------------------
function getDeviceNameForPV() {
  local pvDevice=""
  local rows=()
  local cols=()

  mapfile -t rows < <( lvmdiskscan )

  local row=$(trimString "${rows[-1]}")
  local cols=$(echo $row | cut -d' ' -f1)
  if [ ${cols[0]} -gt 0 ]; then
    for row in "${rows[@]}"; do
      if [[ "$row" =~ "LVM physical volume" ]]; then
        row=$(trimString "$row")
        pvDevice=$(echo $row | cut -d' ' -f1)
        break
      fi
    done
  fi

  echo "$pvDevice"
}

#---------------------------------------------------------------------------------------
#      METHOD:              appendLogicalVolumeToFile
# DESCRIPTION: Append a line/row to the "LVM Partition", LUKS encrypted container,
#              or the last logical volume within the file "$PARTITION_TABLE_FILE"
#  Required Params:
#      1) lvName - name of logical volume
#---------------------------------------------------------------------------------------
appendLogicalVolumeToFile() {
  local lvName="$1"
  local row=""
  local fileRows=()
  local partTblRows=()
  local cols=()
  local lvSize=$(trimString "$mbSelVal")

  IFS=$'\n' read -d '' -a partTblRows < "$PARTITION_TABLE_FILE"

  local rowNum=$(findRowNumForLVM partTblRows)
  local len=${#partTblRows[@]}

  if [ ${rowNum} -lt ${len} ]; then
    fileRows=("${partTblRows[@]:0:${rowNum}}")
    row="${partTblRows[$rowNum]}"
    IFS=$'\t' read -a cols <<< "$row"
    if [ "${cols[3]}" == "lvm" ]; then
      cols[0]="${LVM_DEVICE_PREFIX}${varMap[VG-NAME]}"
      row=$(printf "\t%s" "${cols[@]}")
      row=${row:1}
      fileRows+=("$row")
      rowNum=$(expr $rowNum + 1)
    fi
  else
    fileRows=("${partTblRows[@]:0}")
  fi

  if [ "$#" -gt 1 ]; then
    fileRows+=("$2")
  else
    cols=("${LAST_LVM_PREFIX}${varMap[VG-NAME]}" "$lvName" "$lvSize" "lvm" " " " " " ")
    if [ "$lvName" == "$ROOT_LV_NAME" ]; then
      cols[-1]="ROOT"
    elif [ "$lvName" == "$BOOT_LV_NAME" ]; then
      cols[5]="/boot"
    fi
    row=$(printf "\t%s" "${cols[@]}")
    row=${row:1}
    fileRows+=("$row")
  fi

  if [ ${rowNum} -lt ${len} ]; then
    fileRows+=("${partTblRows[@]:${rowNum}}")
  fi

  row=$(printf "\n%s" "${fileRows[@]}")
  row=${row:1}
  echo -e "$row" > "$PARTITION_TABLE_FILE"
}

#---------------------------------------------------------------------------------------
#      METHOD:                appendPartitionToFile
# DESCRIPTION: Append a line/row to the end of "$PARTITION_TABLE_FILE" that
#              represents the partition to be created
#  Required Params:
#      1) partName - name of partition
#---------------------------------------------------------------------------------------
appendPartitionToFile() {
  local partName="$1"
  local partSize=$(trimString "$mbSelVal")
  local cols=("/dev/sda" "$partName" "$partSize" "8300" " " " " " ")

  case "$partName" in
    "$BOOT_PART_NAME")
      cols[5]="/boot"
    ;;
    "$EFI_PART_NAME")
      cols[3]="EF00"
      cols[-1]="boot, esp"
    ;;
    "$LVM_PART_NAME")
      cols[3]="8E00"
      cols[-1]="lvm"
    ;;
    "$ROOT_PART_NAME")
      cols[-1]="ROOT"
    ;;
  esac

  if [ "$#" -gt 1 ]; then
    cols[4]="$2"
    if [ "$#" -gt 2 ]; then
      cols[-1]="$3"
    fi
  fi

  local lineNum=$(wc -l < "$PARTITION_TABLE_FILE")
  lineNum=$(expr $lineNum + 1)
  cols[0]=$(printf "%s%d" "${cols[0]}" "$lineNum")
  local row=$(printf "\t%s" "${cols[@]}")
  row=${row:1}
  echo -e "$row" >> "$PARTITION_TABLE_FILE"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  findRowNumForLVM
# DESCRIPTION: Find the row number in the partition table that is either
#              A) a logical volume
#              B) the 'LUKS' encrypted container
#              C) the 'LVM Partition'
#      RETURN: integer for the row number
#  Required Params:
#      1) rows - array containing all the rows of partitions and logical volumes
#                from the partition table
#---------------------------------------------------------------------------------------
function findRowNumForLVM() {
  local -n rows=$1
  local cols=()
  local rowNum=0
  local len=${#rows[@]}

  for (( aryIdx = len-1; aryIdx > -1; aryIdx-- ))
  do
    IFS=$'\t' read -a cols <<< "${rows[$aryIdx]}"
    case "${cols[3]}" in
      "8E00")
        rowNum=$(expr $aryIdx + 1)
        break
      ;;
      "crypt")
        rowNum=$(expr $aryIdx + 1)
        break
      ;;
      "lvm")
        rowNum=$aryIdx
        break
      ;;
    esac
  done

  echo ${rowNum}
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getMaxAllocSize
# DESCRIPTION: Get the maximum size that a partition or logical volume may be
#              allocated to
#      RETURN: human-readable format of size
#  Required Params:
#      1) lastDevice - data for either the /home or /root partition or
#                      logical volume
#---------------------------------------------------------------------------------------
function getMaxAllocSize() {
  local lastDevice="$1"
  local ary=()
  IFS=$'\t' read -a ary <<< "$lastDevice"
  local maxSize=$(getMaxForDevice "${ary[2]}")
  echo "$maxSize"
}

#---------------------------------------------------------------------------------------
#      METHOD:                 updateRowInPartTable
# DESCRIPTION: Update a row in the partition table
#  Required Params:
#      1)    aryIdx - the index within the array of rows from the partition table
#      2) tableRows - array containing all the rows of partitions and logical volumes
#                     from the partition table
#      3)    colIdx - the column within the row to update
#      4)    colVal - the value to update the column with
#---------------------------------------------------------------------------------------
updateRowInPartTable() {
  local aryIdx=$1
  local -n tableRows=$2
  local colIdx=$3
  local colVal="$4"
  local row=$(echo -e "${tableRows[$aryIdx]}")
  local cols=()

  IFS=$'\t' read -a cols <<< "$row"
  cols[$colIdx]="$colVal"
  row=$(printf "\t%s" "${cols[@]}")
  row=${row:1}
  tableRows[$aryIdx]=$(printf "%s" "$row")
  updatePartTable tableRows
}

#---------------------------------------------------------------------------------------
#      METHOD:                   updatePartTable
# DESCRIPTION: Update the partition table in the global file name "$PARTITION_TABLE_FILE"
#  Required Params:
#      1) rows - array containing all the rows of partitions and logical volumes
#                from the partition table
#---------------------------------------------------------------------------------------
updatePartTable() {
  local -n rows=$1
  local partTable=$(printf "\n%s" "${rows[@]}")
  partTable=${partTable:1}

  echo -e "$partTable" > "$PARTITION_TABLE_FILE"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                       isFmtOrMnt
# DESCRIPTION: Check to see if all the devices are either formatted or mounted
#      RETURN: 0 - true, 1 - false
#  Required Params:
#      1) checkFmtOrMnt - flag where value
#           > 1 - compares the number of devices to the number that are mounted
#           < 2 - compares the number of devices to the number that are formatted
#      2)   blockDevice - string value of the device (i.e. "/dev/sda")
#---------------------------------------------------------------------------------------
function isFmtOrMnt() {
  local checkFmtOrMnt=$1
  local fmtFlag=$FALSE
  local fsCnt=0
  local mntCnt=0
  local deviceCnt=0
  local counts=$(getPartFmtMntCounts "$2")
  local cntArray=()
  local sep=","
  local diff=0

  readarray -t cntArray <<< "${counts//$sep/$'\n'}"

  deviceCnt=${cntArray[0]}
  fsCnt=${cntArray[1]}
  mntCnt=$(trimString "${cntArray[2]}")

  if [ "$checkFmtOrMnt" -gt 1 ]; then
    diff=$(expr $deviceCnt - $mntCnt)
  else
    diff=$(expr $deviceCnt - $fsCnt)
  fi

  if [ $diff -gt 2 ]; then
    echo 'false' && return 1
  else
    echo 'true' && return 0
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                  setPhysicalVolume
# DESCRIPTION: Set the name of the Physical Volume and its size
#---------------------------------------------------------------------------------------
setPhysicalVolume() {
  local rows=()
  local cols=()

  IFS=$'\n' read -d '' -a rows < "$PARTITION_TABLE_FILE"
  for row in "${rows[@]}"; do
    IFS=$'\t' read -a cols <<< "$row"
    if [ "${cols[3]}" == "8E00" ]; then
      PHYSICAL_VOLUME=$(echo "${cols[0]}")
      PHYSICAL_VOL_SIZE=$(echo "${cols[2]}")
      break
    fi
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                   setSummaryOfCurLayout
# DESCRIPTION: Get a summary of the statistics for the current partition layout/scheme
#              in tabular format.
#  Required Params:
#      1) statsArray - array to append the statistical rows to
#---------------------------------------------------------------------------------------
setSummaryOfCurLayout() {
  local -n statsArray=$1
  local -A keyValPairs=()
  local rowKeys=("bootLoader" "bootDevice" "swapDevice" "lvmRow" "cryptRow" "lvmCnt" "partCnt")
  local key=""
  local row=""
  local cnt=0

  setRowsForSummarizedTable keyValPairs
  for key in "${rowKeys[@]}"; do
    if [ ${keyValPairs["$key"]+_} ]; then
      case "$key" in
        "lvmCnt")
          cnt=${keyValPairs[$key]}
          row="Logical Volume"
        ;;
        "partCnt")
          cnt=${keyValPairs[$key]}
          row="Partition"
        ;;
        *)
          statsArray+=("${keyValPairs[$key]}")
        ;;
      esac
      if [ ${cnt} -gt 0 ]; then
        if [ ${cnt} -gt 1 ]; then
          row=$(echo "${row}s")
        fi
        row=$(printf "%d;%s" ${cnt} "$row")
        statsArray+=("$row")
      fi
    fi

    cnt=0
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                 setRowsForSummarizedTable
# DESCRIPTION: Set the rows of data to display in the summarized table of the
#              current partition layout/scheme
#  Required Params:
#      1) summaryRows - associative array where the key/value pairs of the rows
#                       will be added to
#---------------------------------------------------------------------------------------
setRowsForSummarizedTable() {
  local -n summaryRows=$1
  local row=""
  local partTblRows=()
  local cols=()
  local lvmCnt=0
  local partCnt=0

  IFS=$'\n' read -d '' -a partTblRows < "$PARTITION_TABLE_FILE"
  local len=${#partTblRows[@]}

  for (( aryIdx = len-1; aryIdx > -1; aryIdx-- )); do
    row="${partTblRows[$aryIdx]}"
    IFS=$'\t' read -a cols <<< "$row"

    case "${cols[3]}" in
      "8300")
        partCnt=$(expr $partCnt + 1)
        if [[ "$row" =~ "/boot" ]]; then
          summaryRows["bootDevice"]="1;/boot Partition"
        elif [[ "$row" =~ "swap" ]]; then
          summaryRows["swapDevice"]="1;[SWAP] Partition"
        fi
      ;;
      "8E00")
        summaryRows["lvmRow"]="1;Partition/LVM physical volume"
      ;;
      "EF00")
        summaryRows["bootLoader"]="1;EFI Boot loader"
      ;;
      "EF02")
        summaryRows["bootLoader"]="1;BIOS Boot loader"
      ;;
      "crypt")
        summaryRows["cryptRow"]="1;LUKS encrypted container"
      ;;
      "lvm")
        lvmCnt=$(expr $lvmCnt + 1)
        if [[ "$row" =~ "/boot" ]]; then
          summaryRows["bootDevice"]="1;/boot Logical Volume"
        elif [[ "$row" =~ "swap" ]]; then
          summaryRows["swapDevice"]="1;[SWAP] Logical Volume"
        fi
      ;;
    esac
  done
  summaryRows["lvmCnt"]=${lvmCnt}
  summaryRows["partCnt"]=${partCnt}
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getSelectableSizes
# DESCRIPTION: Get the sizes that can be selected within either a linux or yad "dialog".
#      RETURN: concatenated string delimited by a '\t'
#  Required Params:
#      1) hrSizes - minimum & maximum sizes in human-readable format
#---------------------------------------------------------------------------------------
function getSelectableSizes() {
  local -n hrSizes=$1
  local hrSizeChoices=()
  local hrMinSize=$(echo "${hrSizes[0]}")
  local minSize=$(humanReadableToBytes "$hrMinSize")
  local hrMaxSize=$(echo "${hrSizes[-1]}")
  local maxSize=$(humanReadableToBytes "$hrMaxSize")
  local hrSizeChoice=$(echo "100 GiB")
  local sizeChoice=$(humanReadableToBytes "$hrSizeChoice")
  local sizeInBytes=0
  local bytesToHR=""

  hrSizeChoices+=("$hrMinSize")
  if [ $maxSize -gt $sizeChoice ]; then
    sizeInBytes=$(( ($minSize + $sizeChoice)  / 2 ))
    bytesToHR=$(bytesToHumanReadable "$sizeInBytes")
    hrSizeChoices+=("$bytesToHR")
    hrSizeChoices+=("$hrSizeChoice")
  else
    sizeInBytes=$(( ($minSize + $maxSize)  / 2 ))
    bytesToHR=$(bytesToHumanReadable "$sizeInBytes")
    hrSizeChoices+=("$bytesToHR")
  fi
  hrSizeChoices+=("$hrMaxSize")

  local row=$(printf "\t%s" "${hrSizeChoices[@]}")
  row=${row:1}
  echo -e "$row"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getLinuxPartitionTool
# DESCRIPTION: Get the linux tool that will be used to create and/or manipulate
#              the partition table
#      RETURN: selected linux tool in the global variable "mbSelVal"
#---------------------------------------------------------------------------------------
getLinuxPartitionTool() {
  local dialogBackTitle="Manual Partitioning"
  local dialogTitle="Linux Partitioning Tools"
  local partitionTools=("cfdisk" "cgdisk" "fdisk" "gdisk" "parted" "sfdisk" "sgdisk")
  local -A partToolDescs=(["cfdisk"]="Pseudo-graphical" ["cgdisk"]="Pseudo-graphical"
  ["fdisk"]="Dialog-driven" ["gdisk"]="Dialog-driven" ["parted"]="Dialog-driven & Non-interactive"
  ["sfdisk"]="Non-interactive" ["sgdisk"]="Non-interactive")

  if [ "$DIALOG" == "yad" ]; then
    partitionTools+=("GParted")
    partToolDescs["GParted"]="Graphical using GTK+"
  fi

  getSelectedLinuxPartitionTool "$dialogBackTitle" "$dialogTitle" partitionTools partToolDescs
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                      getNumOfUnassigned
# DESCRIPTION: Get the number of logical volumes and/or partitions that DO NOT
#              have a "file system type" or "mount pt" assigned to them
#      RETURN: integer value
#  Required Params:
#      1) statsTable - statistics of the progress made in tabular format
#---------------------------------------------------------------------------------------
function getNumOfUnassigned() {
  local statsTable="$1"
  local rows=()
  local row=""
  local unassigned=-1

  IFS=$'\n' read -d '' -a rows <<< "$statsTable"

  if [ "$DIALOG" == "yad" ]; then
    row="${rows[-1]}"
    IFS=$';' read -d '' -a rows <<< "$row"
    unassigned=${rows[0]}
  else
    row="${rows[-2]}"
    unassigned=$(trimString $(echo "$row" | cut -d"|" -f5))
  fi

  echo ${unassigned}
}
