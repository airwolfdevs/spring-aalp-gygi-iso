#!/bin/bash

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# globals
#===============================================================================
declare TEXT_SKIP_TASK="return to the \"Basic Setup\" screen and mark this task as skipped."

#===============================================================================
# functions/methods
#===============================================================================

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getTextForBootDevice
# DESCRIPTION: Text to display on the help/more dialog when just selecting the
#              /boot logical volume or partition to be formatted.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getTextForPostInstStep() {
  local txtArray=("Typically, this step is done after rebooting, but then this step cannot be run with any"
  "kind of GUI until the drivers and such are installed.  Arch Linux does not come with any"
  "kind of GUI, nor additional software.  To do so, this has to be done manually, and you"
  "can skip this step and reboot if you want to do so.")
  local concatStr=$(printf " %s" "${txtArray[@]}")

  local helpTextArray=("URL Reference:" "$URL_REF_BORDER"
  "https://itsfoss.com/things-to-do-after-installing-arch-linux"
  "$DIALOG_BORDER" " " "${concatStr:1}")

  txtArray=("It’s not enough to make your Desktop as user friendly.  This step aims to make the"
  "necessary tweaks & adjustments to some of the settings so that the Desktop is easier to use"
  "as well as better overall performance.")
  concatStr=$(printf " %s" "${txtArray[@]}")

  helpTextArray+=(" " "${concatStr:1}" "$DIALOG_BORDER")

  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("  # Clicking the \"Continue\" button will ")
    helpTextArray+=("  # Clicking the \"Skip\" button will ")
  else
    helpTextArray+=("  # Selecting <Continue> will ")
    helpTextArray+=("  # Selecting <  Skip  > will ")
  fi

  helpTextArray[-2]+="start the Post-installation step."
  helpTextArray[-1]+="will mark this step as \"Skipped\" and returns back to the installation guide"

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getHelpTextForChecklist
# DESCRIPTION: Text to display on the help dialog when the "Post Installation" checklist
#              is displaying.
#        NOTE:  The calling method must declare an associative array variable
#               with the name of "piAssocArray":
#---------------------------------------------------------------------------------------
function getHelpTextForChecklist() {
  local dlgBorder=""

  if [ "$DIALOG" == "yad" ]; then
    dlgBorder="${DIALOG_BORDER_MAX}-----"
  else
    dlgBorder="${DIALOG_BORDER}"
  fi

  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "https://wiki.archlinux.org/index.php/list_of_applications"
    "$dlgBorder" " "
    "   >  Basic Setup - required 1st step that executes some of the common tasks such as:"
    "          -  enabling the multilib repository,"
    "          -  add a new user and assigning sudo privileges,"
    "          -  font configuration, etc."
    "   >  Desktop Configuration - required 2nd step to make the Desktop environment as user friendly as possible by installing:"
    "          -  drivers necessary for the installed video card,"
    "          -  desktop environment or window manager & display manager,"
    "          -  themes, etc."
    "   >  Accessory Software - install AUR packages (i.e. software applications) that are in the accessory/other category such as:"
    "          -  Calculators,"
    "          -  Sticky Notes,"
    "          -  Time Tracking, etc."
    "   >  Database Software - install AUR packages that will help with storing data & managing database such as:"
    "          -  MongoDB,"
    "          -  PostgreSQL,"
    "          -  DBeaver, etc."
    "   >  Development Software - install AUR packages that will help with developing software such as:"
    "          -  Build Automation,"
    "          -  IDEs,"
    "          -  Java, etc."
    "   >  Documents & Texts Software - install AUR packages that will be able to create & read documents such as:"
    "          -  Office Suites,"
    "          -  PDF,"
    "          -  Text Editors, etc."
    "   >  Internet Software - install AUR packages that will enable to connect and interact with the Internet such as:"
    "          -  File Sharing,"
    "          -  Network Managers,"
    "          -  Web Browsers, etc."
    "   >  Multimedia Software - install AUR packages that are in the multimedia category such as:"
    "          -  Audio,"
    "          -  Codecs,"
    "          -  Image."
    "          -  Video, etc."
    "   >  System & Utility Software - install AUR packages that are the tools to configure & monitor the system such as:"
    "          -  Archiving & Compression Tools,"
    "          -  File Managers,"
    "          -  Terminal Emulators, etc."
    "   >  Security Software - install AUR packages for hardening the Arch Linux system such as:"
    "          -  Firewalls,"
    "          -  Network,"
    "          -  Password Managers, etc."
    "   >  Software Extras - install AUR packages that don't necessarily belong to one of the above categories such as:"
    "          -  Auto Background Changer,"
    "          -  KStars,"
    "          -  Profile Sync Daemon, etc." " " " " "NOTE:" "$NOTE_BORDER"
    "The \"Basic Setup\" & \"Desktop Configuration\" steps must be first two to run.  The rest of the steps can be done in any order."
    "$dlgBorder" " "
  )

  local okText="will execute the step that was selected!"
  local btnTextArray=("will: "
     "     1)  Store the data for the Post Installation step into the Installer's 'data' directory."
     "     2)  Return back to the installation guide.")
  local exitText=$(getTextForDialog "${btnTextArray[@]}")
  local skipText=$(getSkipText)
  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("   * Clicking the \"Ok\" button $okText"
      " " "   * Clicking the \"Exit\" button $exitText")
    if [ ${piAssocArray["addSkipBtn"]+_} ]; then
      helpTextArray+=(" " "   * Clicking the \"Skip\" button $skipText")
    fi

  else
    helpTextArray+=("   * Selecting < Ok > $okText")
    if [ ${piAssocArray["addSkipBtn"]+_} ]; then
      helpTextArray+=(" " "   * Selecting <Skip> $skipText")
    fi
    helpTextArray+=(" " "   * Selecting <Exit> $exitText" " " " " "NOTE:" "$NOTE_BORDER"
      "   * Select <View URL> below to visit the URL mentioned above")
  fi

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                         getSkipText
# DESCRIPTION: Get the text for the 'Skip' button on the "Post Installation" checklist.
#---------------------------------------------------------------------------------------
function getSkipText() {
  local appendTxt=""

  if [ "$DIALOG" == "yad" ]; then
    appendTxt="that have a blank value in the 'Is Done' column to 'Skipped'."
  else
    appendTxt="that have a blank value between the braces in the last column to '[S]'."
  fi

  local sbtnTextArray=("will: "
     "     1)  Mark/set the software tasks $appendTxt"
     "     2)  Store the data for the Post Installation step into the Installer's 'data' directory."
     "     3)  Mark/set '${POST_INST_NB_OPTS["title"]}' in the Installation Guide's checklist as DONE."
     "     4)  Return back to the installation guide.")
  local text=$(getTextForDialog "${sbtnTextArray[@]}")

  echo "$text"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                getHelpTextForPswdKB
# DESCRIPTION: Text to display on the help/more dialog when entering a
#              password to encrypt the LVM device
#      RETURN: concatenated string
#  Required Params:
#      1) urlRef - the URL that can be referenced for further documentation
#---------------------------------------------------------------------------------------
function getHelpTextForPswdKB() {
  local urlRef="$1"
  local userNameReqs=$(getTextForUserName)
  local spacer=""
  local extraDashes=""
  local helpTextArray=(" " "URL Reference:" "$URL_REF_BORDER" "$urlRef")
  local okText=""
  local cancelText=""

  if [ "$DIALOG" == "yad" ]; then
    extraDashes="-------------"
  else
    spacer="    "
  fi

  if [[ "$urlRef" =~ "Adding_a_user" ]]; then
    local grpNameReqs=$(getTextForGroupName)
    helpTextArray+=("${DIALOG_BORDER}${extraDashes}"
                    " " "Group Name:" "---------${extraDashes}" "$grpNameReqs")
    local okTxtAry=("will add the group name to the system, & the username and password within Samba!"
      "   NOTE:  If the \"UserName\" is not an existing Linux user account, then a new one will be created!")
    okText=$(printf "\n%s" "${okTxtAry[@]}")
    okText="${okText:1}"
    cancelText="will return to the \"4.1.12 - Install Samba\" screen."
  else
    okText="will add the username and password to the system!"
    cancelText="will return to the \"Basic Setup\" screen."
  fi

   helpTextArray+=("${DIALOG_BORDER}${extraDashes}"
   " " "Username:" "---------${extraDashes}"
  "$userNameReqs" "${DIALOG_BORDER}${extraDashes}" " " "Password:"
  "---------${extraDashes}" "Insecure passwords include those containing:"
  "  > Personally identifiable information (e.g., your dog's name, date of"
  "${spacer}birth, area code, favorite video game)" " "
  "  > Simple character substitutions on words (e.g., k1araj0hns0n)" " "
  "  > Root "words" or common strings followed or preceded by added numbers,"
  "${spacer}symbols, or characters (e.g., DG091101%)" " "
  "  > Common phrases or short phrases of grammatically related words"
  "${spacer}(e.g. all of the lights), and even with character substitution." " "
  "The right choice for a password is something long (8-20 characters,"
  "depending on importance) and seemingly completely random. A good technique"
  "for building secure, seemingly random passwords is to base them on"
  "characters from every word in a sentence. Take for instance “the girl is"
  "walking down the rainy street” could be translated to t6!WdtR5 or, less"
  "simply, t&6!RrlW@dtR,57. This approach could make it easier to remember a"
  "password, but note that the various letters have very different"
  "probabilities of being found at the start of words."
  "${DIALOG_BORDER}${extraDashes}" " ")

  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("   * Clicking the \"Ok\" button $okText"
      "   * Clicking the \"Cancel\" button $cancelText")
  else
    helpTextArray+=("   * Selecting <  Ok  > $okText"
      "   * Selecting <Cancel> $cancelText" "$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER"
      "   * Select <View URL> below to visit the URL mentioned above")
  fi

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     getTextForUserName
# DESCRIPTION: Text to display what the requirements are for a valid username.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getTextForUserName() {
  local textArray=(
    "Usernames can ONLY be up to 32 characters long.  The installer requires that:"
    "     A)  The first character is either a lowercase letter or an underscore."
    "     B)  The next 31 characters can be either:"
    "            1) letters, numbers, underscores, and/or hyphens"
    "                        OR"
    "            2) 30 characters of 1) above plus a '$' at the end"
    "     C)  The username can end with a dollar sign (i.e. '$' can be the last character)."
    "In regular expression terms:  ^[a-z_]([a-z0-9_-]{0,31}|[a-z0-9_-]{0,30}\\$)$"
  )

  local dlgText=$(getTextForDialog "${textArray[@]}")
  echo "$dlgText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getHelpForBasicSetup
# DESCRIPTION: Text to display on the help/more dialog for current task selected for the
#              "Basic Setup" step
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpForBasicSetup() {
  local curTaskName="$1"
  local urlRef=""
  local helpText=""
  case "$curTaskName" in
    "Add Unofficial User Repositories")
      urlRef="https://wiki.archlinux.org/index.php/Unofficial_user_repositories"
      helpText=$(getHelpForAddUURTask)
    ;;
    "Check MultiLib")
      urlRef="https://wiki.archlinux.org/index.php/official_repositories#multilib"
      helpText=$(getHelpForCheckMultiLibTask)
    ;;
    "Configure SUDO")
      urlRef="https://wiki.archlinux.org/index.php/Sudo"
      helpText=$(getHelpForSUDOTask)
    ;;
    "Choose AUR Helper")
      urlRef="https://wiki.archlinux.org/index.php/AUR_helpers"
      helpText=$(getHelpForAURHelperTask)
    ;;
    "Create User")
      urlRef="https://wiki.archlinux.org/index.php/users_and_groups"
      helpText=$(getHelpForCreateUserTask)
    ;;
    "Install CUPS")
      urlRef="https://wiki.archlinux.org/index.php/CUPS"
      helpText=$(getHelpForCUPSTask)
    ;;
    "Install Font Config")
      urlRef="https://wiki.archlinux.org/index.php/Font_configuration"
      helpText=$(getHelpForFontConfigTask)
    ;;
    "Install NFS")
      urlRef="https://wiki.archlinux.org/index.php/NFS"
      helpText=$(getHelpForNetFileSysTask)
    ;;
    "Install Samba")
      urlRef="https://wiki.archlinux.org/index.php/Samba"
      helpText=$(getHelpForSambaTask)
    ;;
    "Install SSH")
      urlRef="https://wiki.archlinux.org/index.php/Secure_Shell"
      helpText=$(getHelpForSecShellTask)
    ;;
    "Install TLP")
      urlRef="https://wiki.archlinux.org/index.php/TLP"
      helpText=$(getHelpForAdvPowerManTask)
    ;;
    "Install ZRam")
      urlRef="https://wiki.archlinux.org/index.php/Improving_performance#Zram_or_zswap"
      helpText=$(getHelpForZRamSwapTask)
    ;;
    "Set Default Shell")
      urlRef="https://wiki.archlinux.org/index.php/Command-line_shell"
      helpText=$(getHelpForSetShellTask)
    ;;
    "Set Default Text Editor")
      urlRef="https://wiki.archlinux.org/index.php/environment_variables#Default_programs"
      helpText=$(getHelpForDefaultEditorTask)
    ;;
    "Update System")
      urlRef="https://wiki.archlinux.org/index.php/System_maintenance#Upgrading_the_system"
      helpText=$(getHelpForUpdateSystemTask)
    ;;
    *)
      clog_error "No case statement defined for curTaskName: [$curTaskName]"
      exit 1
    ;;
  esac

  local extraDashes=""
  if [ "$DIALOG" == "yad" ]; then
    extraDashes="-------------"
  fi

  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef" "${DIALOG_BORDER}${extraDashes}" " " "$helpText"
  "${DIALOG_BORDER}${extraDashes}" " " )

  local okText="will"
  local cancelText="will store the data for the \"Basic Setup\" step into the Installer's 'data' directory and return to the Post Installation Checklist dialog."
  if [ "$DIALOG" == "yad" ]; then
    local lines=("will either: "
         "     A)  Execute/Run the other selected 'Software Task' in the \"Checklist\" tab OR"
         "     B)  Execute/Run the selected task in the \"Basic Setup\" tab")
    okText=$(getTextForDialog "${lines[@]}")
    helpTextArray+=("   * Clicking the \"Ok\" button $okText" " "
      "   * Clicking the \"Run\" button will execute the selected task in the \"Basic Setup\" tab" " "
      "   * Clicking the \"Cancel\" button $cancelText")
  else
    okText="will execute/run the selected task!"
    helpTextArray+=("   * Selecting < Ok > $okText"
      "   * Selecting <Cancel> $cancelText" "$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER"
      "   * Select <View URL> below to visit the URL mentioned above")
  fi

  helpText=$(getTextForDialog "${helpTextArray[@]}")
  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getHelpForCheckMultiLibTask
# DESCRIPTION: Text to display on the help/more dialog when the "Check MultiLib" task is
#              selected for the "Basic Setup" step.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpForCheckMultiLibTask() {
  local textArray=("Enables the 'multilib' repository so that 32-bit software and libraries"
  "can be used to run and build 32-bit applications on 64-bit installs.  This"
  "repository can be found in '.../multilib/os/' within the mirrors that will be"
  "configured later.  The 32-bit compatible libraries are located under '/usr/lib32/'. ")
  local concatStr=$(printf " %s" "${textArray[@]}")

  echo "${concatStr:1}"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getHelpForAddUURTask
# DESCRIPTION: Text to display on the help/more dialog when the
#              "Add Unofficial User Repositories" task is selected for the
#              "Basic Setup" step.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpForAddUURTask() {
  local textArray=("Add custom binary repositories freely created and shared"
  "by the community, often providing pre-built versions of PKGBUILDS found"
  "in the AUR (Arch User Repository)")
  local concatStr=$(printf " %s" "${textArray[@]}")

  echo "${concatStr:1}"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                      getTextForAddUUR
# DESCRIPTION: Text to display on the help/more dialog when the selecting the unofficial
#              user repositories to add.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getTextForAddUUR() {
  local urlRef="https://wiki.archlinux.org/index.php/Unofficial_user_repositories"
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef")
  local textArray=("The Arch User Repository (AUR) provides user-made PKGBUILD scripts"
   "for packages not included in the repositories. These PKGBUILD scripts simplify building"
   "from source by explicitly listing and checking for dependencies and configuring the"
   "install to match the Arch architecture.  Arch User Repository helper programs can"
   "further streamline the downloading of PKGBUILD scripts and associated building"
   "process.  However, this comes at the cost of executing PKGBUILDs not validated by a"
   "trusted person; as a result, Arch developers have stated that the utilities for"
   "automatic finding, downloading and executing of PKGBUILDs will never be included in the"
   "official repositories.")
  local concatStr=$(printf " %s" "${textArray[@]}")
  local extraDashes=""
  local wikiURL="https://en.wikipedia.org/wiki/Arch_Linux#Arch_User_Repository_(AUR)"

  if [ "$DIALOG" == "yad" ]; then
    extraDashes="---------"
    wikiURL="https://en.wikipedia.org/wiki/Arch_Linux#Arch_User_Repository_%28AUR%29"
  fi

  helpTextArray+=("${DIALOG_BORDER}${extraDashes}" " " " "
  "Taken from the Wikipedia Article:  $wikiURL"
  "${DIALOG_BORDER}${extraDashes}" " " "${concatStr:1}" "${DIALOG_BORDER}${extraDashes}"
  "The installer will:"
  "     1)  Add the selected repositories to the \"/etc/pacman.conf\" file"
  "            with SigLevel of 'TrustedOnly'"
  "     2)  If any of the selected repositories are \"signed\", their associated key will be locally signed."
  )
  
  helpTextArray+=("${DIALOG_BORDER}${extraDashes}" " ")

  local okText="will commit the selected values!"
  local skipText="will $TEXT_SKIP_TASK"
  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("   * Clicking the \"Ok\" button $okText"
      "   * Clicking the \"Skip\" button $skipText")
  else
    helpTextArray+=("   * Selecting < Ok > will display the list of repositories for the selected value."
      "   * Selecting <Done> $okText"
      "   * Selecting <Skip> $skipText" "$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER"
      "   * Select <View URL> below to visit the URLs mentioned above")
  fi

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getHelpForUpdateSystemTask
# DESCRIPTION: Text to display on the help/more dialog when the "Update System" task is
#              selected for the "Basic Setup" step.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpForUpdateSystemTask() {
  local textArray=("Performs full system upgrades to Arch Linux.  It"
  "synchronizes the repository databases and updates the system's packages.")
  local concatStr=$(printf " %s" "${textArray[@]}")

  echo "${concatStr:1}"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     getHelpForSUDOTask
# DESCRIPTION: Text to display on the help/more dialog when the "Configure SUDO" task is
#              selected for the "Basic Setup" step.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpForSUDOTask() {
  local textArray=("Configure the \"sudo\" command that will allow a system administrator"
  "to delegate authority to give certain users—or groups of users—the ability to run"
  "commands as root or another user while providing an audit trail of the commands and"
  "their arguments.")
  local concatStr=$(printf " %s" "${textArray[@]}")

  echo "${concatStr:1}"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                       getTextForSUDO
# DESCRIPTION: Text to display on the help/more dialog when the "Configure SUDO" dialog
#              is being displayed.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getTextForSUDO() {
  local urlRef="https://wiki.archlinux.org/index.php/Sudo"
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef")
  local lines=("Sudo allows a system administrator to delegate authority to give certain"
               "users—or groups of users—the ability to run commands as root or another"
               "user while providing an audit trail of the commands and their arguments.")
  local concatStr=$(printf " %s" "${lines[@]}")
  local extraDashes=""
  if [ "$DIALOG" == "yad" ]; then
    extraDashes="---------"
  fi

  helpTextArray+=("${DIALOG_BORDER}${extraDashes}" " " "${concatStr:1}")

  lines=("Sudo is an alternative to 'su' for running commands as root. Unlike 'su',"
         "which launches a root shell that allows all further commands root access, sudo"
         "instead grants temporary privilege escalation to a single command. By enabling"
         "root privileges only when needed, sudo usage reduces the likelihood that a typo"
         "or a bug in an invoked command will ruin the system.")
  concatStr=$(printf " %s" "${lines[@]}")
  helpTextArray+=(" " "${concatStr:1}")

  lines=("Sudo can also be used to run commands as other users; additionally, sudo logs"
         "all commands and failed access attempts for security auditing.")
  concatStr=$(printf " %s" "${lines[@]}")
  helpTextArray+=(" " "${concatStr:1}" "${DIALOG_BORDER}${extraDashes}" " ")

  local okText="will do the selected configuration!"
  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("   * Clicking the \"Ok\" button $okText")
  else
    helpTextArray+=("   * Selecting < Ok > $okText"
      "   * Select <View URL> below to visit the URL mentioned above")
  fi

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getHelpForFontConfigTask
# DESCRIPTION: Text to display on the help/more dialog when the "Install Font Config"
#              task is selected for the "Basic Setup" step.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpForFontConfigTask() {
  local textArray=("Configuration of the fonts is done with the \"fontconfig\""
  "library.  It is a library designed to provide a list of available fonts to"
  "applications, and also for configuration for how fonts get rendered."
  )
  local concatStr=$(printf " %s" "${textArray[@]}")

  echo "${concatStr:1}"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getTextForFontConfig
# DESCRIPTION: Text to display on the help/more dialog when the "Font Config" dialog
#              is displaying.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getTextForFontConfig() {
  local urlRef="https://wiki.archlinux.org/index.php/Font_configuration"
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef")
  local extraDashes=""
  if [ "$DIALOG" == "yad" ]; then
    concatStr=$(getTextForFontconfigLib)
    extraDashes="-------------"
    helpTextArray+=("${DIALOG_BORDER}${extraDashes}" " " "$concatStr")
  else
    helpTextArray+=("${DIALOG_BORDER}")
  fi

  local textArray=("Though Fontconfig is used often in modern Unix and Unix-like operating systems,"
    "some applications rely on the original method of font selection and display, the"
    "X Logical Font Description.")
  local concatStr=$(printf " %s" "${textArray[@]}")
  helpTextArray+=(" " "${concatStr:1}" " "
    "Fontconfig can:")
  textArray=("discover new fonts when installed automatically, removing a common source"
    "of configuration problems.")
  concatStr=$(printf " %s" "${textArray[@]}")
  helpTextArray+=("     *  ${concatStr:1}")
  textArray=("perform font name substitution, so that appropriate alternative fonts"
    "can be selected if fonts are missing.")
  concatStr=$(printf " %s" "${textArray[@]}")
  helpTextArray+=("     *  ${concatStr:1}"
    "     *  identify the set of fonts required to completely cover a set of languages.")
  textArray=("have GUI configuration tools built as it uses an XML-based configuration"
  "file (though with autodiscovery, we believe this need is minimized).")
  concatStr=$(printf " %s" "${textArray[@]}")
  helpTextArray+=("     *  ${concatStr:1}")
  textArray=("efficiently and quickly find the fonts you need among the set of"
    "fonts you have installed, even if you have installed thousands of fonts,"
    "while minimizing memory usage.")
  concatStr=$(printf " %s" "${textArray[@]}")
  helpTextArray+=("     *  ${concatStr:1}")
  textArray=("be used in concert with the X Render Extension and FreeType to"
    "implement high quality, anti-aliased and subpixel rendered text on a display.")
  concatStr=$(printf " %s" "${textArray[@]}")
  helpTextArray+=("     *  ${concatStr:1}" " " " " "Fontconfig does not:")
  textArray=("render the fonts themselves (this is left to FreeType"
    "or other rendering mechanisms)")
  concatStr=$(printf " %s" "${textArray[@]}")
  helpTextArray+=("     *  ${concatStr:1}")
  textArray=("depend on the X Window System in any fashion, so that printer"
    "only applications do not have such dependencies")
  concatStr=$(printf " %s" "${textArray[@]}")
  helpTextArray+=("     *  ${concatStr:1}" "${DIALOG_BORDER}${extraDashes}" " ")

  local okText="will install the library and make your Arch fonts beautiful!"
  local skipText="will $TEXT_SKIP_TASK"
  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("   * Clicking the \"Ok\" button $okText"
      "   * Clicking the \"Skip\" button $skipText")
  else
    helpTextArray+=("   * Selecting < Ok > $okText"
      "   * Selecting <Skip> $skipText" "$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER"
      "   * Select <View URL> below to visit the URLs mentioned above")
  fi

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getTextForFontconfigLib
# DESCRIPTION: Text to display at the beginning of the help/more dialog and the linux
#              "dialog" when the "Font Config" dialog is rendered.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getTextForFontconfigLib() {
  local txtArray=("Fontconfig is a library designed to provide a list of available fonts"
    "to applications, and also for configuration for how fonts get rendered.  The FreeType"
    "library renders the fonts, based on this configuration.  The freetype2 font rendering"
    "packages on Arch Linux include the bytecode interpreter (BCI) enabled for better font"
    "rendering especially with an LCD monitor.")
  local txt=$(printf " %s" "${txtArray[@]}")

  echo "${txt:1}"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getHelpForCreateUserTask
# DESCRIPTION: Text to display on the help/more dialog when the "Create User" task is
#              selected for the "Basic Setup" step.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpForCreateUserTask() {
  local textArray=("Create a separate user to manage the systems with sudo privilege"
  "under the wheel group.  To perform all the tasks as the ROOT user will lead to potential"
  "damages on the system.  One cannot also build and install custom packages as the ROOT user!"
  )
  local concatStr=$(printf " %s" "${textArray[@]}")

  echo "${concatStr:1}"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getHelpForAURHelperTask
# DESCRIPTION: Text to display on the help/more dialog when the "Choose AUR Helper" task
#              is selected for the "Basic Setup" step.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpForAURHelperTask() {
  local textArray=("Install an AUR Helper application that can automate certain usage of"
  "the Arch User Repository(AUR).  The 'pacman' package manager can only handle updates"
  "for pre-built packages in its repositories."
  )
  local concatStr=$(printf " %s" "${textArray[@]}")

  echo "${concatStr:1}"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     getTextForAURHelper
# DESCRIPTION: Text to display on the help/more dialog when selecting an AUR helper.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getTextForAURHelper() {
  local urlRef="https://wiki.archlinux.org/index.php/AUR_helpers"
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef" "$DIALOG_BORDER")
  local lines=()
  local concatStr=""

  if [ "$DIALOG" == "yad" ]; then
    lines=("AUR helpers automate certain usage of the Arch User Repository. Most AUR helpers can"
           "search for packages in the AUR and retrieve their PKGBUILDs – others additionally"
           "assist with the build and install process.")
    concatStr=$(printf " %s" "${lines[@]}")
    helpTextArray+=(" " "${concatStr:1}")
  fi

  lines=("Pacman only handles updates for pre-built packages in its repositories. AUR"
      "packages are redistributed in form of PKGBUILDs and need an AUR helper to automate"
      "the re-build process. However, keep in mind that a rebuild of a package may be required"
      "when its shared library dependencies are updated, not only when the package itself is updated.")
  concatStr=$(printf " %s" "${lines[@]}")
  helpTextArray+=(" " "${concatStr:1}" " "
                "Since AUR helpers are unsupported, they are not present in the official repositories.")

  local okText="will install either the command line or graphical helper!"
  local skipText="will $TEXT_SKIP_TASK"
  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("   * Clicking the \"Ok\" button $okText"
      "   * Clicking the \"Skip\" button $skipText")
  else
    helpTextArray+=("   * Selecting < Ok > $okText"
      "   * Selecting <Skip> $skipText" "$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER"
      "   * Select <View URL> below to visit the URL mentioned above")
  fi

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getHelpForDefaultEditorTask
# DESCRIPTION: Text to display on the help/more dialog when the
#              "Set Default Text Editor" task is selected for the "Basic Setup" step.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpForDefaultEditorTask() {
  local textArray=("Change the default command-line text editor used by various"
  "programs, such as crontab.  The current default is 'vi'.")
  local concatStr=$(printf " %s" "${textArray[@]}")

  echo "${concatStr:1}"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getTextForDefaultEditor
# DESCRIPTION: Text to display on the help/more dialog when selecting the
#              default console text editor
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getTextForDefaultEditor() {
  local helpText=$(getHelpForDefaultEditorTask)
  local urlRef="https://wiki.archlinux.org/index.php/environment_variables#Default_programs"
  local extraDashes=""
  if [ "$DIALOG" == "yad" ]; then
    extraDashes="---------"
  fi
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef" "${DIALOG_BORDER}${extraDashes}" " " "$helpText"
  "${DIALOG_BORDER}${extraDashes}" " " "Categories/Types of Command-Line Text Editors"
  "---------------------------------------------${extraDashes}" "   * Console" "     - DTE" "     - Micro" "     - Nano"
  "   * Emacs-style" "     - Emacs" "     - Vile" "     - Zile"
  "   * Vi-style" "     - Amp" "     - Neovim" "     - Vim" "${DIALOG_BORDER}${extraDashes}")
  local okText="will set the selected value as the default command line text editor and install if it isn't already!"
  local skipText="will keep the default editor as 'vi', $TEXT_SKIP_TASK"
  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("   * Clicking the \"Ok\" button $okText"
      "   * Clicking the \"Skip\" button $skipText")
  else
    helpTextArray+=("   * Selecting < Ok > $okText"
      "   * Selecting <Skip> $skipText" "$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER"
      "   * Select <View URL> below to visit the URL mentioned above")
  fi

  helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getHelpForSetShellTask
# DESCRIPTION: Text to display on the help/more dialog when the "Set Default Shell"
#              task is selected for the "Basic Setup" step.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpForSetShellTask() {
  local textArray=("Set and configure the default to be served"
  "when logging into the Arch Linux system.  The current default is 'Bash'.")
  local concatStr=$(printf " %s" "${textArray[@]}")

  echo "${concatStr:1}"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getTextForCmdLineShells
# DESCRIPTION: Text to display on the help/more dialog when selecting the
#              default POSIX compliant shell
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getTextForCmdLineShells() {
  local urlRef="https://wiki.archlinux.org/index.php/Command-line_shell"
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef")
  local extraDashes=""
  if [ "$DIALOG" == "yad" ]; then
    extraDashes="---------"
  fi
  helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef" "${DIALOG_BORDER}${extraDashes}" " "
                "POSIX compliant shells" "----------------------${extraDashes}"
                "     * Bash - https://wiki.archlinux.org/index.php/Bash"
                "     * Dash - https://wiki.archlinux.org/index.php/Dash"
                "     * MirBSD™ Korn Shell - https://wiki.archlinux.org/index.php/KornShell"
                "     * Zsh - https://wiki.archlinux.org/index.php/Zsh" " "
                "Alternative shells" "----------------------${extraDashes}"
                "     * C shell(tcsh) - https://en.wikipedia.org/wiki/C_shell"
                "     * Fish - http://fishshell.com/"
                "     * Oh - https://github.com/michaelmacinnis/oh"
                "     * PowerShell - https://github.com/PowerShell/PowerShell"
                "${DIALOG_BORDER}${extraDashes}" " ")
  local okText="will set the selected value as the default shell, install and configure if it isn't already!"
  local skipText="will keep the default editor as 'bash', $TEXT_SKIP_TASK"
  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("   * Clicking the \"Ok\" button $okText"
      "   * Clicking the \"Skip\" button $skipText")
  else
    helpTextArray+=("   * Selecting < Ok > $okText"
      "   * Selecting <Skip> $skipText" "$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER"
      "   * Select <View URL> below to visit the URLs mentioned above")
  fi

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     getHelpForCUPSTask
# DESCRIPTION: Text to display on the help/more dialog when the "Install CUPS" task is
#              selected for the "Basic Setup" step.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpForCUPSTask() {
  local textArray=("Install the necessary AUR packages to support sending documents to a"
  "printer, \"print\" into a PDF document, etc.")
  local concatStr=$(printf " %s" "${textArray[@]}")

  echo "${concatStr:1}"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                       getTextForCUPS
# DESCRIPTION: Text to display on the help/more dialog when the "Install CUPS" dialog
#              is displaying.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getTextForCUPS() {
  local urlRef="https://wiki.archlinux.org/index.php/CUPS"
  local extraDashes=""
  local concatStr=""
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef")

  if [ "$DIALOG" == "yad" ]; then
    concatStr=$(getHdrTxtForCUPS)
    extraDashes="---------"
    helpTextArray+=("${DIALOG_BORDER}${extraDashes}" " " "$concatStr")
  else
    helpTextArray+=("${DIALOG_BORDER}" " ")
  fi

  local textArray=("CUPS can be fully controlled using the lp* and cups* CLI tools.  Alternatively,"
  "CUPS can be fully administered through:")
  concatStr=$(printf " %s" "${textArray[@]}")
  helpTextArray+=("${concatStr:1}" "     * the Web interface available on http://localhost:631/"
  "     * one of the following GUI applications:"
  "         > GtkLP — GTK+ interface for CUPS."
  "         > Print Manager — Tool for managing print jobs and printers (if KDE will be installed)."
  "         > System Config Printer — GTK+ interface for CUPS."
  "         NOTE:" "         $NOTE_BORDER"
  "         These will be made available within the step \"4.2 - Desktop Environment\"."
  "${DIALOG_BORDER}${extraDashes}" " ")

  local okText="will install the necessary AUR packages!"
  local skipText="will $TEXT_SKIP_TASK"

  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("   * Clicking the \"Ok\" button $okText"
      "   * Clicking the \"Skip\" button $skipText")
  else
    helpTextArray+=("   * Selecting < Ok > $okText"
      "   * Selecting <Skip> $skipText" "$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER"
      "   * Select <View URL> below to visit the URLs mentioned above")
  fi

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                      getHdrTxtForCUPS
# DESCRIPTION: Text to display at the beginning of the help/more dialog and the linux
#              "dialog" when the "Install CUPS" dialog is rendered.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHdrTxtForCUPS() {
  local txtArray=("CUPS (aka Common Unix Printing System) is the standards-based, open source printing"
  "system developed by Apple Inc. for Mac OS X and other UNIX-like operating systems.")
  local txt=$(printf " %s" "${txtArray[@]}")

  echo "${txt:1}"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getHelpForAdvPowerManTask
# DESCRIPTION: Text to display on the help/more dialog when the "Install TLP" task is
#              selected for the "Basic Setup" step.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpForAdvPowerManTask() {
  local textArray=("Install advanced power management tool for Linux and configure"
  "its services.  TLP can be configured either through the command-line or a Python UI.")
  local concatStr=$(printf " %s" "${textArray[@]}")

  echo "${concatStr:1}"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                        getTextForTLP
# DESCRIPTION: Text to display on the help/more dialog when the "Install TLP" dialog
#              is displaying.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getTextForTLP() {
  local urlRef="https://wiki.archlinux.org/index.php/TLP"
  local extraDashes=""
  if [ "$DIALOG" == "yad" ]; then
    extraDashes="---------"
  fi
  local textArray=("TLP brings the benefits of advanced power management for Linux"
      "without the need to understand every technical detail. TLP comes with a default"
      "configuration already optimized for battery life, so you may just install and forget"
      "it. Nevertheless TLP is highly customizable to fulfill your specific requirements."
  )
  local concatStr=$(printf " %s" "${textArray[@]}")
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef"
  "${DIALOG_BORDER}${extraDashes}" " " "${concatStr:1}" " "
  "NOTE:" "$NOTE_BORDER" "The GUI package \"tlpui-git\" will also be installed."
  "${DIALOG_BORDER}${extraDashes}" " ")

  local okText="will install TLP and configure its services!"
  local skipText="will $TEXT_SKIP_TASK"
  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("   * Clicking the \"Ok\" button $okText"
      "   * Clicking the \"Skip\" button $skipText")
  else
    helpTextArray+=("   * Selecting < Ok > $okText"
      "   * Selecting <Skip> $skipText" "$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER"
      "   * Select <View URL> below to visit the URLs mentioned above")
  fi

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getHelpForSecShellTask
# DESCRIPTION: Text to display on the help/more dialog when the "Install SSH" task is
#              selected for the "Basic Setup" step.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpForSecShellTask() {
  local textArray=("Install this network protocol in order be able to control the"
  "Arch Linux system remotely and transfer data between networked computers."
  )
  local concatStr=$(printf " %s" "${textArray[@]}")

  echo "${concatStr:1}"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                        getTextForSSH
# DESCRIPTION: Text to display on the help/more dialog when the "Install SSH" dialog
#              is displaying.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getTextForSSH() {
  local urlRef="https://wiki.archlinux.org/index.php/Secure_Shell"
  local extraDashes=""
  if [ "$DIALOG" == "yad" ]; then
    extraDashes="---------"
  fi
  local textArray=("Secure Shell (SSH) is a cryptographic network protocol for operating"
  "network services securely over an unsecured network. Typical applications include remote"
  "command-line login and remote command execution, but any network service can be secured with SSH."
  )
  local concatStr=$(printf " %s" "${textArray[@]}")
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef"
  "${DIALOG_BORDER}${extraDashes}" " " "${concatStr:1}" " ")

  textArray=("Services that always use SSH are the Secure Copy Protocol(SCP) and the Secure File"
  "Transfer Protocol(SFTP).  Other services that can use SSH are Git, rsync and X11 forwarding.")
  concatStr=$(printf " %s" "${textArray[@]}")
  helpTextArray+=("${concatStr:1}" " ")

  textArray=("An SSH server, by default, listens on the standard TCP port 22. An SSH client"
  "program is typically used for establishing connections to an sshd daemon accepting remote"
  "connections. Both are commonly present on most modern operating systems, including Mac OS X,"
  "GNU/Linux, Solaris and OpenVMS. Proprietary, freeware and open source versions of various"
  "levels of complexity and completeness exist.")
  concatStr=$(printf " %s" "${textArray[@]}")
  helpTextArray+=("${concatStr:1}"
  "${DIALOG_BORDER}${extraDashes}" " ")

  local okText="will install the selected implementation!"
  local skipText="will $TEXT_SKIP_TASK"
  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("   * Clicking the \"Ok\" button $okText"
      "   * Clicking the \"Skip\" button $skipText")
  else
    helpTextArray+=("   * Selecting < Ok > $okText"
      "   * Selecting <Skip> $skipText" "$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER"
      "   * Select <View URL> below to visit the URLs mentioned above")
  fi

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getHelpForNetFileSysTask
# DESCRIPTION: Text to display on the help/more dialog when the "Install NFS" task is
#              selected for the "Basic Setup" step.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpForNetFileSysTask() {
  local textArray=("Install this distributed file system protocol to allow a user on"
  "a client computer to access files over a network.")
  local concatStr=$(printf " %s" "${textArray[@]}")

  echo "${concatStr:1}"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                        getTextForNFS
# DESCRIPTION: Text to display on the help/more dialog when the "Install NFS" dialog
#              is displaying.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getTextForNFS() {
  local urlRef="https://wiki.archlinux.org/index.php/NFS"
  local extraDashes=""
  if [ "$DIALOG" == "yad" ]; then
    extraDashes="---------"
  fi
  local textArray=("Network File System (NFS) is an Internet Standard/distributed file system protocol"
      "created by Sun Microsystems in 1984.  NFS was developed to allow file sharing between"
      "systems residing on a local area network in a manner similar to how local storage is"
      "accessed.  The Linux NFS client supports three versions of the NFS protocol:"
  )
  local concatStr=$(printf " %s" "${textArray[@]}")
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef"
      "${DIALOG_BORDER}${extraDashes}" " " "${concatStr:1}"
      "    > NFS version 2 [RFC1094]" "    > NFS version 3 [RFC1813]"
      "    > NFS version 4 [RFC3530]." " " "NOTE:" "$NOTE_BORDER"
      "If a Linux File System is to be mounted  on a Windows machine, Samba/CIFS should be used instead!"
      "${DIALOG_BORDER}${extraDashes}" " ")

  local okText="will install the client and server, and configure its services!"
  local skipText="will $TEXT_SKIP_TASK"
  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("   * Clicking the \"Ok\" button $okText"
      "   * Clicking the \"Skip\" button $skipText")
  else
    helpTextArray+=("   * Selecting < Ok > $okText"
      "   * Selecting <Skip> $skipText" "$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER"
      "   * Select <View URL> below to visit the URLs mentioned above")
  fi

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     getHelpForSambaTask
# DESCRIPTION: Text to display on the help/more dialog when the "Install Samba" task is
#              selected for the "Basic Setup" step.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpForSambaTask() {
  local textArray=("Install this re-implementation of the Server Message Block (SMB)"
  "network protocol to facilitate file and printer sharing among the Arch Linux"
  "system and Windows systems.")
  local concatStr=$(printf " %s" "${textArray[@]}")

  echo "${concatStr:1}"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                       getTextForSamba
# DESCRIPTION: Text to display on the help/more dialog when the "Install Samba" dialog
#              is displaying.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getTextForSamba() {
  local urlRef="https://wiki.archlinux.org/index.php/Samba"
  local extraDashes=""
  if [ "$DIALOG" == "yad" ]; then
    extraDashes="---------"
  fi
  local textArray=("Samba is a re-implementation of the Server Message Block (SMB)"
  "networking protocol.  As an alternative to NFS, Samba provides secure, stable and"
  "fast file and print services for all clients using the SMB/CIFS protocol.  It is"
  "an important component to seamlessly integrate Linux/Unix Servers and Desktops"
  "into Active Directory environments."
  )

  local concatStr=$(printf " %s" "${textArray[@]}")
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef"
  "${DIALOG_BORDER}${extraDashes}" " " "${concatStr:1}"
  "${DIALOG_BORDER}${extraDashes}" " ")

  local okText="will install Samba, enable its services and usershares!"
  local skipText="will $TEXT_SKIP_TASK"
  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("   * Clicking the \"Ok\" button $okText"
      "   * Clicking the \"Skip\" button $skipText")
  else
    helpTextArray+=("   * Selecting < Ok > $okText"
      "   * Selecting <Skip> $skipText" "$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER"
      "   * Select <View URL> below to visit the URLs mentioned above")
  fi

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getHelpForZRamSwapTask
# DESCRIPTION: Text to display on the help/more dialog when the "Install ZRam" task is
#              selected for the "Basic Setup" step.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpForZRamSwapTask() {
  local textArray=("Install this Linux kernel module for creating a compressed block"
  "device in RAM (i.e. a RAM disk), but with on-the-fly \"disk\" compression"
  )
  local concatStr=$(printf " %s" "${textArray[@]}")

  echo "${concatStr:1}"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     getTextForZRamSwap
# DESCRIPTION: Text to display on the help/more dialog when the "Install ZRam" dialog
#              is displaying.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getTextForZRamSwap() {
  local urlRef="https://wiki.archlinux.org/index.php/Improving_performance#Zram_or_zswap"
  local extraDashes=""
  if [ "$DIALOG" == "yad" ]; then
    extraDashes="---------"
  fi
  local textArray=("The ZRam kernel module (previously called compcache) provides a"
  "compressed block device in RAM.  It will be used as swap device and the RAM will"
  "be able to hold much more information, but uses more CPU. Still, it is much quicker"
  "than swapping to a hard drive.  If a system often falls back to swap, this could"
  "improve responsiveness.  Using this is also a good way to reduce disk read/write"
  "cycles due to swap on SSDs.")

  local concatStr=$(printf " %s" "${textArray[@]}")
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef"
  "${DIALOG_BORDER}${extraDashes}" " " "${concatStr:1}"
  "${DIALOG_BORDER}${extraDashes}" " ")

  local okText="will install the AUR package and enable its service!"
  local skipText="will $TEXT_SKIP_TASK"
  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("   * Clicking the \"Ok\" button $okText"
      "   * Clicking the \"Skip\" button $skipText")
  else
    helpTextArray+=("   * Selecting < Ok > $okText"
      "   * Selecting <Skip> $skipText" "$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER"
      "   * Select <View URL> below to visit the URLs mentioned above")
  fi

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     getTextForGroupName
# DESCRIPTION: Text to display what the requirements are for a valid group name.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getTextForGroupName() {
  local textArray=(
    "Group Names can ONLY be up to 16 characters long.  The installer requires that:"
    "     A)  The first character is either a lowercase letter or an underscore."
    "     B)  The next 15 characters can be either letters, numbers, underscores, and/or hyphens"
    "In regular expression terms:  ^[a-z_]([a-z0-9_-]{0,15})$"
  )

  local dlgText=$(getTextForDialog "${textArray[@]}")
  echo "$dlgText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getHelpTextForBrandOfVC
# DESCRIPTION: Text to display on the help/more dialog when the "Brands of Video Cards"
#              dialog is displaying.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForBrandOfVC() {
  local urlRef="https://wiki.archlinux.org/index.php/xorg#Driver_installation"
  local extraDashes=""
  local btnText=""
  if [ "$DIALOG" == "yad" ]; then
    extraDashes="---------"
    btnText="click the \"Skip\" button "
  else
    btnText="select <Skip> "
  fi

  local textArray=("The installer was unable to determine the brand for the installed video"
        "card.  Determine the brand name for the video card and select the option that"
        "matches it.  If none of the options is a match, $btnText and install the drivers"
        "manually in order for video acceleration to work!")
  local concatStr=$(printf " %s" "${textArray[@]}")
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef"
      "${DIALOG_BORDER}${extraDashes}" " " "${concatStr:1}" " "
      "${DIALOG_BORDER}${extraDashes}" " ")
  local okText="will display a dialog to either select a type or confirm the selection!"
  local skipText="will $TEXT_SKIP_TASK"
  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("   * Clicking the \"Ok\" button $okText"
      "   * Clicking the \"Skip\" button $skipText")
  else
    helpTextArray+=("   * Selecting < Ok > $okText"
      "   * Selecting <Skip> $skipText" "$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER"
      "   * Select <View URL> below to visit the URLs mentioned above")
  fi

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     getHelpTextForIntel
# DESCRIPTION: Text to display on the help/more dialog when the confirmation dialog
#              for the "Intel" video drivers is displaying.
#      RETURN: concatenated string
#  Required Params:
#      1) brandName - the name of the brand for the video card installed
#---------------------------------------------------------------------------------------
function getHelpTextForIntel() {
  local urlRef="https://wiki.archlinux.org/index.php/Intel_graphics"
  local extraDashes=""
  if [ "$DIALOG" == "yad" ]; then
    extraDashes="---------"
  fi

  local textArray=("Intel graphics are essentially plug-and-play because"
      "Intel provides and supports open source drivers.")
  if [ "$1" != "unknown" ]; then
      textArray=("The installer has identified that the brand of video card"
      "is \"Intel\".  ${textArray[0]}" "${textArray[@]:1}")
  fi
  local concatStr=$(printf " %s" "${textArray[@]}")
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef"
      "${DIALOG_BORDER}${extraDashes}" " " "${concatStr:1}")

  textArray=("After confirmation, the installer will also display an additional"
    "dialog to get confirmation if Vulkan will also need to be supported.")
  concatStr=$(printf " %s" "${textArray[@]}")
  helpTextArray+=(" " "${concatStr:1}" " " "${DIALOG_BORDER}${extraDashes}" " ")

  addTextForButtons "$1" "will install the Driver & OpenGL packages listed!"

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#      METHOD:                      addTextForButtons
# DESCRIPTION: Append the help text for the buttons of the dialog to the array variable
#              "helpTextArray" that was declared in the calling function.
#  Required Params:
#      1) brandName - the name of the brand for the video card installed
#   Optional Param:
#      2) override for the confirm button
#      3) override for the cancel button
# ---------------------------------------------------------------------------------------
addTextForButtons() {
  local okText="will display the Driver & OpenGL packages for the selected 'Type'!"
  local cancelText="will return back to the \"Brands\" selection dialog."
  local skipText="will $TEXT_SKIP_TASK"

  if [ "$#" -gt 1 ]; then
    okText="$1"
  fi

  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("   * Clicking the \"Confirm\" button $okText")
    if [ "$1" == "unknown" ]; then  # coming from "Brands" dialog
      helpTextArray+=("   * Clicking the \"Cancel\" button $cancelText")
    else
      helpTextArray+=("   * Clicking the \"Skip\" button $skipText")
    fi
  else
    helpTextArray+=("   * Selecting <Confirm> $okText")
    if [ "$1" == "unknown" ]; then  # coming from "Brands" dialog
      helpTextArray+=("   * Selecting <Cancel > $cancelText")
    else
      helpTextArray+=("   * Selecting < Skip  > $skipText")
    fi
    helpTextArray+=("$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER"
      "   * Select <View URL> below to visit the URLs mentioned above")
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getHelpTextForVirtualBox
# DESCRIPTION: Text to display on the help/more dialog when the confirmation dialog
#              for the "VirtualBox" video drivers is displaying.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForVirtualBox() {
  local urlRef="https://wiki.archlinux.org/index.php/VirtualBox#Install_the_Guest_Additions"
  local extraDashes=""
  if [ "$DIALOG" == "yad" ]; then
    extraDashes="---------"
  fi

  local textArray=("The installer has identified that Arch Linux will be installed within"
      "VirtualBox.  The packages listed are the same as those seen within the URL.")
  local concatStr=$(printf " %s" "${textArray[@]}")
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef"
      "${DIALOG_BORDER}${extraDashes}" " " "${concatStr:1}" " "
      "${DIALOG_BORDER}${extraDashes}" " ")
  local okText="will install the Driver & OpenGL packages listed!"
  local skipText="will $TEXT_SKIP_TASK"
  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("   * Clicking the \"Confirm\" button $okText"
      "   * Clicking the \"Skip\" button $skipText"
      )
  else
    helpTextArray+=("   * Selecting <Confirm> $okText" "   * Selecting < Skip  > $skipText"
      "$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER"
      "   * Select <View URL> below to visit the URLs mentioned above")
  fi

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getHelpTextForVMware
# DESCRIPTION: Text to display on the help/more dialog when the confirmation dialog
#              for the "VMware" video drivers is displaying.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForVMware() {
  local urlRef="https://wiki.archlinux.org/index.php/VMware/Installing_Arch_as_a_guest"
  local extraDashes=""
  if [ "$DIALOG" == "yad" ]; then
    extraDashes="---------"
  fi

  local textArray=("The installer has identified that Arch Linux will be installed in a VMware product,"
      "such as Player (Plus), Fusion or Workstation.  The reasons that the installer will use the"
      "open source Open-VM-Tools over the proprietary VMware Tools is:")
  local concatStr=$(printf " %s" "${textArray[@]}")
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef"
      "${DIALOG_BORDER}${extraDashes}" " " "${concatStr:1}")

  textArray=("VMware Tools use to provide the best drivers for network and storage, combined"
             "with the functionality for other features such as time synchronization.  These"
             "are now part of the Linux kernel.")
  concatStr=$(printf " %s" "${textArray[@]}")
  helpTextArray+=("     1) ${concatStr:1}")

  textArray=("VMware Tools used to have the advantage of being able to use the Unity mode feature, but"
             "has been removed due to lack of use and developer difficulties in maintaining the feature.")
  concatStr=$(printf " %s" "${textArray[@]}")
  helpTextArray+=("     2) ${concatStr:1}")

  textArray=("Open-VM-Tools is the open source implementation of VMware Tools and consists of a suite"
  "of virtualization utilities that improves the functionality, administration, and management of"
  "virtual machines within a VMware environment.")
  concatStr=$(printf " %s" "${textArray[@]}")
  helpTextArray+=(" " "${concatStr:1}" " " "The benefits of using Open-VM-Tools are:")

  textArray=("It provides the best out-of-box experience to efficiently deploy virtual machines"
            "on VMware's virtual infrastructure.")
  concatStr=$(printf " %s" "${textArray[@]}")
  helpTextArray+=("     * ${concatStr:1}")

  textArray=("Open-VM-Tools eliminates separate maintenance cycles for VMware Tools updates.  Updates"
            "to the packages are provided with OS maintenance updates and patches.  This then reduces"
            "operational expenses and virtual machine downtime.")
  concatStr=$(printf " %s" "${textArray[@]}")
  helpTextArray+=("     * ${concatStr:1}")

  textArray=("Open-VM-Tools adheres to the VMware Compatibility Matrix for the guest OS"
            "release.  Therefore, a compatibility matrix check is NOT required.")
  concatStr=$(printf " %s" "${textArray[@]}")
  helpTextArray+=("     * ${concatStr:1}")

  textArray=("Since Open-VM-Tools is bundled with the operating system, it provides"
            "a compact footprint optimized for each OS release.")
  concatStr=$(printf " %s" "${textArray[@]}")
  helpTextArray+=("     * ${concatStr:1}" "${DIALOG_BORDER}${extraDashes}" " ")

  local okText="will install the packages listed!"
  local skipText="will $TEXT_SKIP_TASK"
  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("   * Clicking the \"Confirm\" button $okText"
      "   * Clicking the \"Skip\" button $skipText"
      )
  else
    helpTextArray+=("   * Selecting <Confirm> $okText"
      "   * Selecting < Skip  > $skipText"
      "$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER"
      "   * Select <View URL> below to visit the URLs mentioned above")
  fi

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getHelpTextForVirtualBox
# DESCRIPTION: Text to display on the help/more dialog when the confirmation dialog
#              for the "VirtualBox" video drivers is displaying.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForVulkan() {
  local brandName="$1"
  local urlRef="https://wiki.archlinux.org/index.php/Vulkan"
  local extraDashes=""
  if [ "$DIALOG" == "yad" ]; then
    extraDashes="---------"
  fi

  local textArray=("Vulkan is a low-overhead, cross-platform 3D graphics and compute"
                  "API. Vulkan targets high-performance realtime 3D graphics applications.  When"
                  "compared with OpenGL and Direct3D 11, Vulkan offers:")
  local concatStr=$(printf " %s" "${textArray[@]}")
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef"
      "${DIALOG_BORDER}${extraDashes}" " " "${concatStr:1}"
      "     1) higher performance and more balanced CPU/GPU usage."
      "     2) considered to be a lower level API and offers parallel tasking."
      "     3) better at distributing work amongst multiple CPU cores." "${DIALOG_BORDER}${extraDashes}" " ")

  local okText="will install the packages listed along with the Driver & OpenGL packages for \"$brandName\"!"
  local skipText="will continue with the install of the Driver & OpenGL packages for \"$brandName\"."
  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("   * Clicking the \"Confirm\" button $okText"
      "   * Clicking the \"Skip\" button $skipText"
      )
  else
    helpTextArray+=("   * Selecting <Confirm> $okText" "   * Selecting < Skip  > $skipText"
      "$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER"
      "   * Select <View URL> below to visit the URLs mentioned above")
  fi

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                      getHelpTextForAMD
# DESCRIPTION: Text to display on the help/more dialog when the type selection dialog
#              for the "AMD / ATI" video drivers is displaying.
#      RETURN: concatenated string
#  Required Params:
#      1) brandName - the name of the brand for the video card installed
#---------------------------------------------------------------------------------------
function getHelpTextForAMD() {
  local urlRef="https://wiki.archlinux.org/index.php/xorg#AMD"
  local extraDashes=""
  if [ "$DIALOG" == "yad" ]; then
    extraDashes="---------"
  fi

  local textArray=("AMD develops computer processors and related technologies for business"
      "and consumer markets.  ATI was acquired by AMD in 2006.")
  if [ "$1" != "unknown" ]; then
      textArray=("The installer has identified that the brand of video card"
      "is \"Advanced Micro Devices (AMD)\".  ${textArray[0]}" "${textArray[@]:1}")
  fi
  local concatStr=$(printf " %s" "${textArray[@]}")
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef"
      "${DIALOG_BORDER}${extraDashes}" " " "${concatStr:1}")

  textArray=("Select the appropriate type based on the column \"GPU architecture\" or"
  "\"Radeon cards\" using the table displayed within the URL above.")
  concatStr=$(printf " %s" "${textArray[@]}")
  helpTextArray+=(" " "${concatStr:1}" " " "${DIALOG_BORDER}${extraDashes}" " ")

  addTextForButtons "$1"

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getHelpTextForNvidiaOpts
# DESCRIPTION: Text to display on the help/more dialog when the type selection dialog
#              for the "NVIDIA" video drivers is displaying.
#      RETURN: concatenated string
#  Required Params:
#      1) brandName - the name of the brand for the video card installed
#---------------------------------------------------------------------------------------
function getHelpTextForNvidiaOpts() {
  local urlRef="https://wiki.archlinux.org/index.php/NVIDIA"
  local extraDashes=""
  if [ "$DIALOG" == "yad" ]; then
    extraDashes="---------"
  fi

  local textArray=("If the video card is for a laptop with hybrid Intel/NVIDIA graphics, then"
    "it is recommended to select \"Bumblebee\" for the type.  Otherwise, the installer will"
    "base the Driver & OpenGL packages on the GeForce 600-900 and Quadro/Tesla/Tegra K-series"
    "cards and newer [NVE0, NV110 and NV130 family cards from around 2010-2019]!")
  if [ "$1" != "unknown" ]; then
      textArray=("The installer has identified that the brand of video card"
      "is \"NVIDIA\".  ${textArray[0]}" "${textArray[@]:1}")
  fi
  local concatStr=$(printf " %s" "${textArray[@]}")
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef"
      "${DIALOG_BORDER}${extraDashes}" " " "${concatStr:1}")

  textArray=("For older versions of the video cards, visit the URL above to determine which"
    "Driver & OpenGL packages will need to be installed, as well as the commands that need"
    "to be executed to configure them.")
  concatStr=$(printf " %s" "${textArray[@]}")
  helpTextArray+=(" " "${concatStr:1}" " " "${DIALOG_BORDER}${extraDashes}" " ")

  addTextForButtons "$1"

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getHelpTextForAMDGPU
# DESCRIPTION: Text to display on the help/more dialog when the confirmation dialog
#              for the "AMDGPU" video drivers is displaying.
#      RETURN: concatenated string
#  Required Params:
#      1) brandName - the name of the brand for the video card installed
#---------------------------------------------------------------------------------------
function getHelpTextForAMDGPU() {
  local urlRef="https://wiki.archlinux.org/index.php/AMDGPU"
  local extraDashes=""
  if [ "$DIALOG" == "yad" ]; then
    extraDashes="---------"
  fi

  local textArray=("AMDGPU is the open source graphics driver for the latest AMD"
      "Radeon graphics cards.  Xorg will automatically load the driver and it will"
      "use the monitor's EDID to set the native resolution.  Configuration is only"
      "required for tuning the driver.")
  local concatStr=$(printf " %s" "${textArray[@]}")
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef"
      "${DIALOG_BORDER}${extraDashes}" " " "${concatStr:1}")

  textArray=("After confirmation, the installer will also display an additional"
    "dialog to get confirmation if Vulkan will also need to be supported.")
  concatStr=$(printf " %s" "${textArray[@]}")
  helpTextArray+=(" " "${concatStr:1}" " " "${DIALOG_BORDER}${extraDashes}" " ")

  addTextForSelectedType "$1"

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#      METHOD:                   addTextForSelectedType
# DESCRIPTION: Append the help text for the buttons of the selected type dialog to the
#              array variable "helpTextArray" that was declared in the calling function.
#  Required Params:
#      1) brandName - the name of the brand for the selected type
# ---------------------------------------------------------------------------------------
addTextForSelectedType() {
  local okText="will install the Driver & OpenGL packages listed!"
  local cancelText="will return back to the \"$1\" selection dialog."

  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("   * Clicking the \"Confirm\" button $okText"
        "   * Clicking the \"Cancel\" button $cancelText")
  else
    helpTextArray+=("   * Selecting <Confirm> $okText"
        "   * Selecting <Cancel > $cancelText"
        "$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER"
        "   * Select <View URL> below to visit the URLs mentioned above")
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                      getHelpTextForATI
# DESCRIPTION: Text to display on the help/more dialog when the confirmation dialog
#              for the "ATI" video drivers is displaying.
#      RETURN: concatenated string
#  Required Params:
#      1) brandName - the name of the brand for the video card installed
#---------------------------------------------------------------------------------------
function getHelpTextForATI() {
  local urlRef="https://wiki.archlinux.org/index.php/ATI"
  local extraDashes=""
  if [ "$DIALOG" == "yad" ]; then
    extraDashes="---------"
  fi

  local textArray=("ATI is the radeon open source driver which supports the majority"
      "of AMD (previously ATI) GPUs.  Xorg will automatically load the driver and it will"
      "use the monitor's EDID to set the native resolution.  Configuration is only"
      "required for tuning the driver.")
  local concatStr=$(printf " %s" "${textArray[@]}")
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef"
      "${DIALOG_BORDER}${extraDashes}" " " "${concatStr:1}" " "
      "${DIALOG_BORDER}${extraDashes}" " ")

  addTextForSelectedType "$1"

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getHelpTextForCatalyst
# DESCRIPTION: Text to display on the help/more dialog when the confirmation dialog
#              for the "AMD Catalyst" video drivers is displaying.
#      RETURN: concatenated string
#  Required Params:
#      1) brandName - the name of the brand for the video card installed
#---------------------------------------------------------------------------------------
function getHelpTextForCatalyst() {
  local urlRef="https://wiki.archlinux.org/index.php/AMD_Catalyst"
  local extraDashes=""
  if [ "$DIALOG" == "yad" ]; then
    extraDashes="---------"
  fi

  local textArray=("Catalyst is now the legacy AMD proprietary driver.  Catalyst packages"
    "are no longer offered in the official repositories.  It is no longer updated by AMD"
    "and does not support the latest Xorg.  Installing an old Xorg version is required.")
  local concatStr=$(printf " %s" "${textArray[@]}")
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef"
      "${DIALOG_BORDER}${extraDashes}" " " "${concatStr:1}" " "
      "${DIALOG_BORDER}${extraDashes}" " ")

  addTextForSelectedType "$1"

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getHelpTextForBumblebee
# DESCRIPTION: Text to display on the help/more dialog when the confirmation dialog
#              for the "Bumblebee" video drivers is displaying.
#      RETURN: concatenated string
#  Required Params:
#      1) brandName - the name of the brand for the video card installed
#---------------------------------------------------------------------------------------
function getHelpTextForBumblebee() {
  local urlRef="https://wiki.archlinux.org/index.php/Bumblebee"
  local extraDashes=""
  if [ "$DIALOG" == "yad" ]; then
    extraDashes="---------"
  fi

  local textArray=("Bumblebee is an effort to make NVIDIA Optimus enabled laptops"
    "work in GNU/Linux systems.  NVIDIA Optimus is a technology that allows an Intel"
    "integrated GPU and discrete NVIDIA GPU to be built into and accessed by a laptop.  This"
    "involves two graphics cards with two different power consumption profiles plugged in a"
    "layered way sharing a single framebuffer.")
  local concatStr=$(printf " %s" "${textArray[@]}")
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef"
      "${DIALOG_BORDER}${extraDashes}" " " "${concatStr:1}" " "
      "${DIALOG_BORDER}${extraDashes}" " ")

  addTextForSelectedType "$1"

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getHelpTextForNouveau
# DESCRIPTION: Text to display on the help/more dialog when the confirmation dialog
#              for the "Nouveau" video drivers is displaying.
#      RETURN: concatenated string
#  Required Params:
#      1) brandName - the name of the brand for the video card installed
#---------------------------------------------------------------------------------------
function getHelpTextForNouveau() {
  local urlRef="https://wiki.archlinux.org/index.php/Nouveau"
  local extraDashes=""
  if [ "$DIALOG" == "yad" ]; then
    extraDashes="---------"
  fi

  local textArray=("Nouveau is the open-source driver for the NVIDIA graphics cards.  Visit"
    "the URL above to be directed toward finding the video card's code name, and comparing"
    "it with the feature matrix for supported features")
  local concatStr=$(printf " %s" "${textArray[@]}")
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef"
      "${DIALOG_BORDER}${extraDashes}" " " "${concatStr:1}" " "
      "${DIALOG_BORDER}${extraDashes}" " ")

  addTextForSelectedType "$1"

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getHelpTextForNVIDIA
# DESCRIPTION: Text to display on the help/more dialog when the confirmation dialog
#              for the "NVIDIA proprietary" video drivers is displaying.
#      RETURN: concatenated string
#  Required Params:
#      1) brandName - the name of the brand for the video card installed
#---------------------------------------------------------------------------------------
function getHelpTextForNVIDIA() {
  local urlRef="https://wiki.archlinux.org/index.php/NVIDIA"
  local extraDashes=""
  if [ "$DIALOG" == "yad" ]; then
    extraDashes="---------"
  fi

  local textArray=("The Driver & OpenGL packages listed are for NVIDIA's proprietary graphics"
    "card driver.  These can be used with the GeForce 600-900 and Quadro/Tesla/Tegra K-series"
    "cards and newer [NVE0, NV110 and NV130 family cards from around 2010-2019]!")
  local concatStr=$(printf " %s" "${textArray[@]}")
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef"
      "${DIALOG_BORDER}${extraDashes}" " " "${concatStr:1}" " "
      "${DIALOG_BORDER}${extraDashes}" " ")

  addTextForSelectedType "$1"

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getHelpTextForDesktopEnv
# DESCRIPTION: Text to display on the help/more dialog for current task selected for the
#              "Desktop Configuration" step
#      RETURN: concatenated string
#  Required Params:
#      1) curTaskName - the name of the task being executed.
#---------------------------------------------------------------------------------------
function getHelpTextForDesktopConf() {
  local curTaskName="$1"
  local urlRef=""
  local helpText=""
  case "$curTaskName" in
    "Administer CUPS")
      urlRef="https://wiki.archlinux.org/index.php/CUPS#Usage"
      helpText=$(getHelpTextForAdminCupsTask)
    ;;
    "Application Launchers")
      urlRef="https://www.slant.co/topics/3945/~best-linux-app-launchers"
      helpText=$(getHelpTextForAppLaunchTask)
    ;;
    "Application Menu Editors")
      urlRef="https://wiki.archlinux.org/index.php/list_of_applications#Application_menu_editors"
      helpText=$(getHelpTextForAppMenuEdTask)
    ;;
    "Composite Manager")
      urlRef="https://en.wikipedia.org/wiki/Compositing_window_manager"
      helpText=$(getHelpTextForCompManTask)
    ;;
    "DE or WM")
      urlRef="https://www.slant.co/topics/14976/~desktop-environments-for-arch-linux"
      helpText=$(getHelpTextForDEorWMTask)
    ;;
    "Desktop & Icon Themes")
      urlRef="https://www.slant.co/topics/2086/~best-gtk3-themes"
      helpText=$(getHelpTextForThemesTask)
    ;;
    "Display Manager")
      urlRef="https://wiki.archlinux.org/index.php/display_manager"
      helpText=$(getHelpTextForDispManTask)
    ;;
    "Fonts")
      urlRef="https://wiki.archlinux.org/index.php/fonts"
      helpText=$(getHelpTextForFontsTask)
    ;;
    "Taskbars")
      urlRef=""
      helpText=$(getHelpTextForTaskbarsTask)
    ;;
    "Video Card Drivers")
      urlRef="https://wiki.archlinux.org/index.php/Xorg#Driver_installation"
      helpText=$(getHelpTextForVCDTask)
    ;;
    "Xorg")
      urlRef="https://wiki.archlinux.org/index.php/Xorg"
      helpText=$(getHelpTextForXorgTask)
    ;;
    *)
      clog_error "No case statement defined for curTaskName: [$curTaskName]"
      exit 1
    ;;
  esac

  local extraDashes=""
  if [ "$DIALOG" == "yad" ]; then
    extraDashes="-------------"
  fi

  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef" "${DIALOG_BORDER}${extraDashes}" " " "$helpText"
  "${DIALOG_BORDER}${extraDashes}" " " )

  local okText="will"
  local cancelText="will store the data for the \"Desktop Configuration\" step into the Installer's"
  cancelText+=" 'data' directory and return to the Post Installation Checklist dialog."
  if [ "$DIALOG" == "yad" ]; then
    local lines=("will either: "
         "     A)  Execute/Run the other selected 'Software Task' in the \"Checklist\" tab OR"
         "     B)  Execute/Run the selected task in the \"Desktop Configuration\" tab")
    okText=$(getTextForDialog "${lines[@]}")
    helpTextArray+=("   * Clicking the \"Ok\" button $okText" " "
      "   * Clicking the \"Run\" button will execute the selected task in the \"Desktop Configuration\" tab" " "
      "   * Clicking the \"Cancel\" button $cancelText")
  else
    okText="will execute/run the selected task!"
    helpTextArray+=("   * Selecting <  Ok  > $okText"
      "   * Selecting <Cancel> $cancelText" "$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER"
      "   * Select <View URL> below to visit the URL mentioned above")
  fi

  helpText=$(getTextForDialog "${helpTextArray[@]}")
  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getHelpTextForAdminCupsTask
# DESCRIPTION: Text to display on the help/more dialog when the "Administer CUPS"
#              task is selected for the "Desktop Configuration" step.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForAdminCupsTask() {
  local textArray=("Install one of several GUI applications can be used to fully control"
  "CUPS (aka Common Unix Printing System) and/or give printer administration privileges"
  "to the user '${POST_ASSOC_ARRAY["UserName"]}'."
  )
  local concatStr=$(printf " %s" "${textArray[@]}")

  echo "${concatStr:1}"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                getHelpTextForAdministerCUPS
# DESCRIPTION: Text to display on the help/more dialog when the
#              "Administer CUPS" dialog is being shown.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForAdministerCUPS() {
  local urlRef="https://wiki.archlinux.org/index.php/CUPS#Usage"
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef")
  local textArray=("The CUPS server can be fully administered either through the"
   "web interface, available on http://localhost:631/, or by using one of the GUI"
   "applications that can be selected to install.")
  local concatStr=$(printf " %s" "${textArray[@]}")
  local extraDashes=""

  if [ "$DIALOG" == "yad" ]; then
    extraDashes="---------"
  fi

  helpTextArray+=("${DIALOG_BORDER}${extraDashes}" " " "${concatStr:1}")

  textArray=("The installer will also ask for confirmation if printer administration"
    "privileges should be given to the user '${POST_ASSOC_ARRAY["UserName"]}'.")
  concatStr=$(printf " %s" "${textArray[@]}")
  helpTextArray+=( " " "${concatStr:1}" "${DIALOG_BORDER}${extraDashes}" " ")

  local confirmText="get confirmation to add printer administration privileges to the user"
  local okText="will install the selected packages and ${confirmText}!"
  local skipText="will first get ${confirmText}.  Then $TEXT_SKIP_TASK"
  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("   * Clicking the \"Ok\" button $okText"
      "   * Clicking the \"Skip\" button $skipText")
  else
    helpTextArray+=("   * Selecting < Ok > $okText"
      "   * Selecting <Skip> $skipText" "$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER"
      "   * Select <View URL> below to visit the URLs mentioned above")
  fi

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getHelpTextForCompManTask
# DESCRIPTION: Text to display on the help/more dialog when the "Composite Manager"
#              task is selected for the "Desktop Configuration" step.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForCompManTask() {
  local textArray=("Install a compositing window manager to allow for advanced visual"
    "effects, such as transparency, and avoid issues unpleasant visual issues such as"
    "screen-tearing.")
  local concatStr=$(printf " %s" "${textArray[@]}")

  echo "${concatStr:1}"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:               getHelpTextForCompositeManager
# DESCRIPTION: Text to display on the help/more dialog when the
#              "Composite Manager" dialog is being shown.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForCompositeManager() {
  local urlRef="https://en.wikipedia.org/wiki/Compositing_window_manager"
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef")
  local textArray=("The installed '${POST_ASSOC_ARRAY["WINDOW-MANAGER"]}' window manager"
      "does not do compositing.  A compositing window manager is a component of a computer's"
      "graphical user interface that draws windows and their borders.  It also controls how"
      "they display and interact with each other, and with the rest of the desktop"
      "environment . The main difference between a compositing window manager and other window"
      "managers is that instead of outputting to a common screen, programs each output first"
      "to a separate and independent buffer, or temporary location inside the computer, where"
      "they can be manipulated before they are shown.")
  local concatStr=$(printf " %s" "${textArray[@]}")
  local extraDashes=""

  if [ "$DIALOG" == "yad" ]; then
    extraDashes="---------"
  fi

  helpTextArray+=("${DIALOG_BORDER}${extraDashes}" " " "${concatStr:1}")

  textArray=("The window manager then processes and combines, or composites, output from these"
      "separate buffers onto a common desktop. The result is that the programs now behave as"
      "independent 2D or 3D objects.[1] Compositing allows for advanced visual effects, such as"
      "transparency, fading, scaling, duplicating, bending and contorting, shuffling, and redirecting"
      "applications. The addition of a virtual third dimension allows for features such as realistic"
      "shadows beneath windows, the appearance of distance and depth, live thumbnail versions of windows,"
      "and complex animations, to name just a few.[3][4] Because the programs draw to the off-screen"
      "buffer, all graphics are naturally double buffered and thus do not flicker as they are updated.")
  concatStr=$(printf " %s" "${textArray[@]}")
  helpTextArray+=( " " "${concatStr:1}")

  textArray=("The most commonly-used compositing window managers include the Desktop Window"
      "Manager in Microsoft Windows, the Quartz Compositor in Mac OS X, and Compiz, Metacity"
      "and KWin for Linux, FreeBSD and OpenSolaris systems.")
  concatStr=$(printf " %s" "${textArray[@]}")
  helpTextArray+=( " " "${concatStr:1}" "${DIALOG_BORDER}${extraDashes}" " ")

  local okText="will install the selected composite manager!"
  local skipText="will $TEXT_SKIP_TASK"
  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("   * Clicking the \"Ok\" button $okText"
      "   * Clicking the \"Skip\" button $skipText")
  else
    helpTextArray+=("   * Selecting < Ok > $okText"
      "   * Selecting <Skip> $skipText" "$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER"
      "   * Select <View URL> below to visit the URLs mentioned above")
  fi

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getHelpTextForAppLaunchTask
# DESCRIPTION: Text to display on the help/more dialog when the "Application Launchers"
#              task is selected for the "Desktop Configuration" step.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForAppLaunchTask() {
  local textArray=("Choose and install small utility applications that help make the"
    "desktop a more productive environment to work and play.")
  local concatStr=$(printf " %s" "${textArray[@]}")

  echo "${concatStr:1}"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getHelpTextForAppLaunchers
# DESCRIPTION: Text to display on the help/more dialog when the "Application Launchers"
#              dialog is being shown.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForAppLaunchers() {
  local urlRef="https://www.slant.co/topics/3945/~best-linux-app-launchers"
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef")
  local textArray=("Application launchers play an integral part in making the Linux desktop"
    "a more productive environment to work and play.  They represent small utilities which"
    "offers the desktop user a convenient access point for application software and can"
    "make a real boost to users’ efficiency.")
  local concatStr=$(printf " %s" "${textArray[@]}")
  local extraDashes=""

  if [ "$DIALOG" == "yad" ]; then
    extraDashes="---------"
  fi

  helpTextArray+=("${DIALOG_BORDER}${extraDashes}" " " "${concatStr:1}")

  textArray=("An application launcher helps to reduce start up times for applications by"
    "indexing shortcuts in the menu.  Furthermore, this type of software allows users to"
    "search for documents and other files quicker by indexing different file formats. "
    "This makes them useful for launching almost anything on a computer including multimedia"
    "files, games, and the internet.  Application launchers often support plug-ins, adding to their versatility.")
  concatStr=$(printf " %s" "${textArray[@]}")
  helpTextArray+=( " " "${concatStr:1}" "${DIALOG_BORDER}${extraDashes}" " ")

  addTextForOkSkipButtons

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#      METHOD:                   addTextForOkSkipButtons
# DESCRIPTION: Append the help text for the Ok & Skip buttons of the dialog to the
#              array variable "helpTextArray" that was declared in the calling function.
#   Optional Param:
#      1) override text for the Ok button
# ---------------------------------------------------------------------------------------
addTextForOkSkipButtons() {
  local okText="will install the packages for the options that were selected."
  local skipText="will $TEXT_SKIP_TASK"

  if [ "$#" -gt 0 ]; then
    okText="$1"
  fi

  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("   * Clicking the \"Ok\" button $okText"
      "   * Clicking the \"Skip\" button $skipText")
  else
    helpTextArray+=("   * Selecting < Ok > $okText"
      "   * Selecting <Skip> $skipText" "$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER"
      "   * Select <View URL> below to visit the URLs mentioned above")
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getHelpTextForAppMenuEdTask
# DESCRIPTION: Text to display on the help/more dialog when the "Application Menu Editors"
#              task is selected for the "Desktop Configuration" step.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForAppMenuEdTask() {
  local textArray=("Choose and install packages that can manage application entries"
    "in the application menu.")
  local concatStr=$(printf " %s" "${textArray[@]}")

  echo "${concatStr:1}"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                getHelpTextForAppMenuEditors
# DESCRIPTION: Text to display on the help/more dialog when the "Application Menu Editors"
#              dialog is being shown.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForAppMenuEditors() {
  local urlRef="https://www.freedesktop.org/wiki/Specifications/menu-spec/"
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef")
  local textArray=("Linux desktops usually follow the freedesktop.org menu specification"
    "for their hierarchical menus with desktop applications.  This allows a user to edit"
    "the application menu manually via files stored in directories defined by the freedesktop"
    "specification (visit URL above).  However, the more easier and comfortable approach is"
    "to use a GUI application.")
  local concatStr=$(printf " %s" "${textArray[@]}")
  local extraDashes=""

  if [ "$DIALOG" == "yad" ]; then
    extraDashes="---------"
  fi

  helpTextArray+=("${DIALOG_BORDER}${extraDashes}" " " "${concatStr:1}" "${DIALOG_BORDER}${extraDashes}" " ")

  addTextForOkSkipButtons

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getHelpTextForDispManTask
# DESCRIPTION: Text to display on the help/more dialog when the "Display Manager"
#              task is selected for the "Desktop Configuration" step.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForDispManTask() {
  local textArray=("Choose and install the display manager package to provide a way of"
    "logging into the system and prevent unauthorised access.")
  local concatStr=$(printf " %s" "${textArray[@]}")

  echo "${concatStr:1}"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                getHelpTextForDisplayManager
# DESCRIPTION: Text to display on the help/more dialog when the "Display Manager" dialog
#              is being shown.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForDisplayManager() {
  local urlRef="https://wiki.archlinux.org/index.php/display_manager"
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef")
  local textArray=("\"Display Managers\" (DMs) are also referred to as Login Managers. "
    "Typically, they are a graphical user interface (GUI) that is displayed at the end"
    "of the boot process in place of the default shell.  There are various implementations"
    "of DMs, just as there for a \"Desktop Environment\" (DE) or standalone \"Window Manager\""
    "(WM).  There is usually a certain amount of customization and themeability available with each one.")
  local concatStr=$(printf " %s" "${textArray[@]}")
  local extraDashes=""

  if [ "$DIALOG" == "yad" ]; then
    extraDashes="---------"
  fi

  helpTextArray+=("${DIALOG_BORDER}${extraDashes}" " " "${concatStr:1}" "${DIALOG_BORDER}${extraDashes}" " ")

  addTextForOkSkipButtons "will install and configure the selected display manager!"

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getHelpTextForThemesTask
# DESCRIPTION: Text to display on the help/more dialog when the "Desktop & Icon Themes"
#              task is selected for the "Desktop Configuration" step.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForThemesTask() {
  local textArray=("Choose and install desktop & icon theme packages in order to"
    "style the dash, launcher, and panel.")
  local concatStr=$(printf " %s" "${textArray[@]}")

  echo "${concatStr:1}"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getHelpTextForThemes
# DESCRIPTION: Text to display on the help/more dialog when the "Desktop & Icon Themes"
#              dialog is being shown.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForThemes() {
  local urlRef="https://wiki.archlinux.org/index.php/GTK+#Themes"
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef")
  local textArray=("Themes for desktops are packages with CSS code to style the dash, launcher, and"
    "panel.  They are widely in Desktop Environments or Window Managers used as standalone.")
  local concatStr=$(printf " %s" "${textArray[@]}")
  local extraDashes=""

  if [ "$DIALOG" == "yad" ]; then
    extraDashes="---------"
  fi

  helpTextArray+=("${DIALOG_BORDER}${extraDashes}" " " "${concatStr:1}")

  textArray=("The freedesktop project provides the Icon Theme Specification, which applies to most"
    "Linux DEs and tries to unify the look of a whole bunch of icons in an icon-theme.  Freedesktop"
    "also provides the Icon Naming Specification, which defines a standard naming scheme for icons"
    "believed to be installed on any system.  The default theme hicolor should include them all.")
  concatStr=$(printf " %s" "${textArray[@]}")
  helpTextArray+=( " " "${concatStr:1}" "${DIALOG_BORDER}${extraDashes}" " ")

  addTextForOkSkipButtons

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getHelpTextForFontsTask
# DESCRIPTION: Text to display on the help/more dialog when the "Fonts"
#              task is selected for the "Desktop Configuration" step.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForFontsTask() {
  local textArray=("Choose and install font packages from Adobe, Apple, Microsoft, etc.")
  local concatStr=$(printf " %s" "${textArray[@]}")

  echo "${concatStr:1}"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     getHelpTextForFonts
# DESCRIPTION: Text to display on the help/more dialog when the "Fonts" dialog
#              is being shown.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForFonts() {
  local urlRef="https://wiki.archlinux.org/index.php/general_recommendations#Fonts"
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef")
  local textArray=("Fonts are electronic data files containing a set of glyphs, characters,"
    "or symbols such as dingbats.")
  local concatStr=$(printf " %s" "${textArray[@]}")
  local extraDashes=""

  if [ "$DIALOG" == "yad" ]; then
    extraDashes="---------"
  fi

  helpTextArray+=("${DIALOG_BORDER}${extraDashes}" " " "${concatStr:1}")

  textArray=("It is advised to install a set of TrueType fonts, as only unscalable bitmap"
    "fonts are included in a basic Arch system.  There are several general-purpose font"
    "families providing large Unicode coverage and even metric compatibility with fonts"
    "from other OSs.")
  concatStr=$(printf " %s" "${textArray[@]}")
  helpTextArray+=(" " "${concatStr:1}" " "
    "More information on the subject can be found by visiting the URL above."
    "${DIALOG_BORDER}${extraDashes}" " ")

  addTextForOkSkipButtons

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getHelpTextForTaskbarsTask
# DESCRIPTION: Text to display on the help/more dialog when the "Taskbars"
#              task is selected for the "Desktop Configuration" step.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForTaskbarsTask() {
  local textArray=("Choose and install the packages for a taskbar/dock that displays the"
    "the applications that are currently running, switch to other desktops, and launch applications.")
  local concatStr=$(printf " %s" "${textArray[@]}")

  echo "${concatStr:1}"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getHelpTextForTaskbars
# DESCRIPTION: Text to display on the help/more dialog when the "Taskbars" dialog
#              is being shown.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForTaskbars() {
  local urlRef="https://www.slant.co/topics/7875/~panel-dock-for-linux"
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef")
  local textArray=("There are various purposes for using a taskbar.  Typically, they"
    "show the applications that are currently running.")
  local concatStr=$(printf " %s" "${textArray[@]}")
  local extraDashes=""

  if [ "$DIALOG" == "yad" ]; then
    extraDashes="---------"
  fi

  helpTextArray+=("${DIALOG_BORDER}${extraDashes}" " " "${concatStr:1}")

  textArray=("A taskbar assumes the form of a strip located along one edge of the"
    "screen.  On this strip are various icons which correspond to the windows open"
    "within an application.  Clicking these icons makes it possible to easily switch"
    "between applications or windows, as well as other desktops, with the currently"
    "active application or window usually appearing differently from the rest.  The"
    "taskbar usually has a notification area, which uses interactive icons to display"
    "real-time information about the state of the computer system and some of the"
    "applications active on it. ")
  concatStr=$(printf " %s" "${textArray[@]}")
  helpTextArray+=(" " "${concatStr:1}" "${DIALOG_BORDER}${extraDashes}" " ")

  addTextForOkSkipButtons

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getHelpTextForVCDTask
# DESCRIPTION: Text to display on the help/more dialog when the "Video Card Drivers"
#              task is selected for the "Desktop Configuration" step.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForVCDTask() {
  local textArray=("Install the Driver & OpenGL packages based on the brand name of"
    "the video card installed in the system.  The installer will try and determine"
    "this.  Otherwise, the installer will display a dialog with a list of brand names.")
  local concatStr=$(printf " %s" "${textArray[@]}")

  echo "${concatStr:1}"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getHelpTextForDEorWMTask
# DESCRIPTION: Text to display on the help/more dialog when the "DE or WM"
#              task is selected for the "Desktop Configuration" step.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForDEorWMTask() {
  local textArray=("Install either:"
    "     A) \"Desktop Environment\" (DE) containing a window manager and bundle of applications."
    "     B) \"Window Manager\" (WM) designed to be used as standalone.")
  local concatStr=$(getTextForDialog "${textArray[@]}")

  echo "$concatStr"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getHelpTextForDEorWM
# DESCRIPTION: Text to display on the help/more dialog when the "DE or WM" dialog
#              is being shown.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForDEorWM() {
  local urlRef="https://www.slant.co/topics/14976/~desktop-environments-for-arch-linux"
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef")
  local textArray=("The \"Desktop Environment\" (DE) choices displayed by the installer"
      "come bundled with a variety of components to provide a common graphical user"
      "interface.  These DMs provide their own window manager and allow the other provided"
      "applications a better way of interacting with each other.  This gives a more"
      "consistent experience, complete with features like desktop icons, toolbars,"
      "wallpapers, and desktop widgets.  Additionally, most of the DEs include a set of"
      "integrated applications and utilities.")
  local concatStr=$(printf " %s" "${textArray[@]}")
  local extraDashes=""

  if [ "$DIALOG" == "yad" ]; then
    extraDashes="---------"
  fi

  helpTextArray+=("${DIALOG_BORDER}${extraDashes}" " " "${concatStr:1}")

  concatStr=$(getHelpTextForWM)
  helpTextArray+=("${DIALOG_BORDER}${extraDashes}" " " "$concatStr" "${DIALOG_BORDER}${extraDashes}" " ")

  addTextForOkSkipButtons "will display the options that are available."

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getHelpTextForDeskEnv
# DESCRIPTION: Text to display on the help/more dialog when the "Desktop Environment"
#              dialog is being shown.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForDeskEnv() {
  local urlRef="https://wiki.archlinux.org/index.php/desktop_environment"
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef")
  local textArray=("A \"Desktop Environment\" (DE) bundles together a variety of"
      "components to provide common graphical user interface elements such as icons,"
      "toolbars, wallpapers, and desktop widgets.  Additionally, most DEs include a"
      "set of integrated applications and utilities.  Most importantly, DEs provide"
      "their own window manager.")
  local concatStr=$(printf " %s" "${textArray[@]}")
  local extraDashes=""

  if [ "$DIALOG" == "yad" ]; then
    extraDashes="---------"
  fi

  helpTextArray+=("${DIALOG_BORDER}${extraDashes}" " " "${concatStr:1}")

  textArray=("The GUI environment can be configured in any number of ways. Desktop Environments"
      "simply provide a complete and convenient means of accomplishing this task.  One is free to"
      "mix-and-match applications from multiple desktop environments.  For example, one may wish to"
      "install and run GNOME applications such as the Epiphany web browser within KDE Plasma instead"
      "of using the Konqueror web browser.  However, one drawback of this approach is that many"
      "applications provided by a DE rely heavily upon their DE's respective underlying libraries.  This"
      "could require installation of a larger number of dependencies.  Such mixed environments should be"
      "avoided to conserve disk space.  Often there are other alternatives which do depend on only a"
      "few external libraries.")
  concatStr=$(printf " %s" "${textArray[@]}")
  helpTextArray+=(" " "${concatStr:1}")

  textArray=("Applications provided by a DE tend to integrate better with their native environments.  Mixing"
      "environments with different widget toolkits will result in visual discrepancies (that is, interfaces"
      "will use different icons and widget styles).  In terms of usability, mixed environments may not behave"
      "similarly.  This can lead to confusion or unexpected behavior.")
  concatStr=$(printf " %s" "${textArray[@]}")
  helpTextArray+=(" " "${concatStr:1}" " " "NOTE:" "$NOTE_BORDER")

  concatStr=$(getTextForTasksToSkip "DE_SKIP_COMP_MAN" "Desktop Environments" "Composite Manager")
  helpTextArray+=("$concatStr")

  concatStr=$(getTextForTasksToSkip "DE_SKIP_DISP_MAN" "Desktop Environments" "Display Manager")

  helpTextArray+=("$concatStr" " " "${DIALOG_BORDER}${extraDashes}" " ")

  addTextForOkSkipButtons "will install & configure the selected DE!"

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getHelpTextForWinMan
# DESCRIPTION: Text to display on the help/more dialog when the "Window Manager" dialog
#              is being shown.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForWinMan() {
  local urlRef="https://wiki.archlinux.org/index.php/Window_manager"
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef")
  local textArray=("A window manager (WM) is system software that controls the placement"
      "and appearance of windows within a windowing system in a graphical user interface"
      "(GUI).  It can be part of a desktop environment (DE) or be used as standalone.")
  local concatStr=$(printf " %s" "${textArray[@]}")
  local extraDashes=""

  if [ "$DIALOG" == "yad" ]; then
    extraDashes="---------"
  fi

  helpTextArray+=("${DIALOG_BORDER}${extraDashes}" " " "${concatStr:1}")

  concatStr=$(getHelpTextForWM)
  helpTextArray+=("$concatStr" " " "NOTE:" "$NOTE_BORDER")

  concatStr=$(getTextForTasksToSkip "WM_SKIP_COMP_MAN" "Window Managers" "Composite Manager")

  helpTextArray+=("$concatStr" "${DIALOG_BORDER}${extraDashes}" " ")

  local okText=""
  local skipText="will $TEXT_SKIP_TASK"
  if [ "$DIALOG" == "yad" ]; then
    local lines=("will either: "
         "     A)  Display the available WMs for the other selected type chosen."
         "     B)  Display confirmation to install & configure the selected WM.")
    okText=$(getTextForDialog "${lines[@]}")
    helpTextArray+=("   * Clicking the \"Ok\" button $okText" " "
      "   * Clicking the \"Run\" button will execute the selected task in the \"Basic Setup\" tab" " "
      "   * Clicking the \"Cancel\" button $cancelText")
  else
    okText="will display the available WMs for the selected type!"
    helpTextArray+=("   * Selecting < Ok > $okText"
      "   * Selecting <Skip> $cancelText" "$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER"
      "   * Select <View URL> below to visit the URL mentioned above")
  fi

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getTextForTasksToSkip
# DESCRIPTION: Text to display on the help/more dialog when either the "DE or WM" or
#              the "Window Manager" dialog is being shown.
#      RETURN: concatenated string
#  Required Params:
#      1) namesArray - the name of the array containing the values that if selected
#                      will cause a task to be skipped
#      2)  optsTitle - the name of the options
#      3)   taskName - name of the task that will be skipped
#---------------------------------------------------------------------------------------
function getTextForTasksToSkip() {
  local -n namesArray="$1"
  local deWM=$(echo "The following $2 will skip the task of installing a \"$3\" ")
  deWM+="since they already include one:"

  local skipTextArray=("$deWM")

  for deWM in "${namesArray[@]}"; do
    deWM=$(printf "     > %s" "$deWM")
    skipTextArray+=("$deWM")
  done

  local skipText=$(getTextForDialog "${skipTextArray[@]}")

  echo "$skipText"

}

#---------------------------------------------------------------------------------------
#    FUNCTION:                      getHelpTextForWM
# DESCRIPTION: Text to display on the help/more dialog when either the "DE or WM" or
#              the "Window Manager" dialog is being shown.
#      RETURN: concatenated string
#   Optional Param:
#      1) override for the first sentence
#---------------------------------------------------------------------------------------
function getHelpTextForWM() {
  local textArray=("The options of \"Window Managers\" displayed by the installer are"
      "designed to be used as standalone, which gives complete freedom over the"
      "choice of applications to be used.  This allows the ability to create a"
      "more lightweight and customized environment, tailored to a user's own specific"
      "needs.  For Arch Linux, window managers are grouped into the following types:")
  local idxArray=(-1 -2 -3)

  if [ "$#" -gt 0 ]; then
    textArray[0]="The options of \"Window Managers\" (WMs) displayed by the installer are"
  fi

  local concatStr=$(printf " %s" "${textArray[@]}")
  local wmTextArray+=(" " "${concatStr:1}" "     * Dynamic" "     * Stacking" "     * Tiling")
  if [ "$#" -gt 0 ]; then
    wmTextArray+=("${DIALOG_BORDER}${extraDashes}" " ")
    idxArray=(-3 -4 -5)
  fi

  textArray=("— window managers that \"tile\" the windows so that none are overlapping.  They"
             "usually make very extensive use of key-bindings and have less (or no) reliance on the"
             " mouse.  Tiling window managers may be manual, offer predefined layouts, or both.")
  concatStr=$(printf " %s" "${textArray[@]}")
  wmTextArray[${idxArray[0]}]+="$concatStr"

  textArray=("— (aka floating) window managers that provide the traditional desktop metaphor used in"
             "commercial operating systems like Windows and OS X.  Windows act like pieces of paper on a"
             "desk, and can be stacked on top of each other.")
  concatStr=$(printf " %s" "${textArray[@]}")
  wmTextArray[${idxArray[1]}]+="$concatStr"
  wmTextArray[${idxArray[2]}]+=" — window managers that can dynamically switch between tiling or floating window layout."

  local concatStr=$(getTextForDialog "${wmTextArray[@]}")

  echo "$concatStr"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getHelpTextForXorgTask
# DESCRIPTION: Text to display on the help/more dialog when the "Xorg"
#              task is selected for the "Desktop Configuration" step.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForXorgTask() {
  local textArray=("Install the xorg group of packages, which includes the Xorg server"
      "packages.  This task is required for the Desktop Environment, GPU Drivers,"
      "Keyboard Layout, etc.")
  local concatStr=$(printf " %s" "${textArray[@]}")
  local strArray=("${concatStr:1}" " " "NOTE:" "$NOTE_BORDER")

  textArray=("Skipping this task will set the rest of the remaining tasks of"
      "\"Step#4:  Post Installation\" as \"Skipped\" and returns back to the installation guide.")
  concatStr=$(printf " %s" "${textArray[@]}")
  strArray+=("${concatStr:1}")

  concatStr=$(getTextForDialog "${strArray[@]}")

  echo "${concatStr}"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     getHelpTextForXorg
# DESCRIPTION: Text to display on the help/more dialog when the "Xorg" dialog
#              is being shown.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForXorg() {
  local urlRef="https://wiki.archlinux.org/index.php/Xorg"
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef")
  local textArray=("Xorg (commonly referred as simply X) is the most popular display server"
      "among Linux users.  It is an ever-present requisite for GUI applications, resulting"
      "in massive adoption from most distributions of Linux.")
  local concatStr=$(printf " %s" "${textArray[@]}")
  local extraDashes=""

  if [ "$DIALOG" == "yad" ]; then
    extraDashes="---------"
  fi

  helpTextArray+=("${DIALOG_BORDER}${extraDashes}" " " "${concatStr:1}")

  textArray=("The X.Org project provides an open source implementation of the X Window"
      "System.  The development work is being done in conjunction with the freedesktop.org"
      "community.  The X.Org Foundation is the educational non-profit corporation whose"
      "Board serves this effort, and whose Members lead this work.")
  concatStr=$(printf " %s" "${textArray[@]}")
  helpTextArray+=(" " "${concatStr:1}" "${DIALOG_BORDER}${extraDashes}" " ")

  local okText="will install the packages listed!"
  textArray=("will mark this task, the remaining tasks for \"${SEL_CHKLIST_OPT[0]} - ${SEL_CHKLIST_OPT[1]}\""
      "and the remaining software tasks of the \"Step#4:  Post Installation\" step.  The installer will"
      "then return back to the installation guide.")
  local skipText=$(printf " %s" "${textArray[@]}")
  skipText="${skipText:1}"
  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("   * Clicking the \"Confirm\" button $okText"
      "   * Clicking the \"Skip\" button $skipText"
      )
  else
    helpTextArray+=("   * Selecting <Confirm> $okText" "   * Selecting < Skip  > $skipText"
      "$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER"
      "   * Select <View URL> below to visit the URLs mentioned above")
  fi

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                      getTextForWarning
# DESCRIPTION: Get the text to display in the dialog to signify a warning message.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getTextForWarning() {
  local dashBorder="----------------------------------"
  local textArray=("$dashBorder" "          WARNING!!!!!!!" "$dashBorder" " " " ")
  local text=$(getTextForDialog "${textArray[@]}")

  echo "$text"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getHelpTextForFlashbackDE
# DESCRIPTION: Text to display on the help/more dialog when the "GNOME Flashback"
#              dialog is being shown.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForFlashbackDE() {
  local urlRef="https://wiki.archlinux.org/index.php/GNOME/Flashback"
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef")
  local textArray=("GNOME Flashback (previously called GNOME fallback mode) is a shell for"
      "GNOME 3.  The desktop layout and the underlying technology is similar to GNOME 2.  It"
      "doesn't use 3D acceleration at all, so it's generally faster and less CPU intensive"
      "than GNOME Shell with llvmpipe.")
  local concatStr=$(printf " %s" "${textArray[@]}")
  local extraDashes=""

  if [ "$DIALOG" == "yad" ]; then
    extraDashes="---------"
  fi

  helpTextArray+=("${DIALOG_BORDER}${extraDashes}" " " "${concatStr:1}"
      "There are 2 session/window manager options to choose from:"
      "     1)  Gnome Flashback (Metacity)" "     2)  Gnome Flashback (Compiz)"
      "Metacity is lighter and faster, whereas Compiz provides fancier desktop effects."
      "${DIALOG_BORDER}${extraDashes}" " ")

  local okText="will install & configure the 'GNOME Flashback' DE!"
  local cancelText="will to the dialog to choose a Desktop Environment"
  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("   * Clicking the \"Ok\" button $okText"
      "   * Clicking the \"Cancel\" button $cancelText")
  else
    helpTextArray+=("   * Selecting <  Ok  > $okText"
      "   * Selecting <Cancel> $cancelText" "$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER"
      "   * Select <View URL> below to visit the URLs mentioned above")
  fi

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getHelpTextForDeWmConf
# DESCRIPTION: Text to display on the help/more dialog when the confirmation dialog
#              for installing either a "Desktop Environment" or standalone
#              "Window Manager" is being shown.
#      RETURN: concatenated string
#  Required Params:
#    1) urlRef - the URL to visit for further documentation
#    2) deWMName - name of either the "Desktop Environment" or standalone "Window Manager"
#    3) deWMKey - "Desktop Environment" or type of window manager
#    4) title - post installation step title
#---------------------------------------------------------------------------------------
function getHelpTextForDeWmConf() {
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$1")
  local textArray=("The packages/software listed to install & configuration steps are either"
    "required and/or suggested within the Arch Wiki page for \"$2\" $3.  Visit the URL"
    "specified above and then click on \"$2\" for further reading.")
  local concatStr=$(printf " %s" "${textArray[@]}")
  local extraDashes=""

  if [ "$DIALOG" == "yad" ]; then
    extraDashes="---------"
  fi

  helpTextArray+=("${DIALOG_BORDER}${extraDashes}" " " "${concatStr:1}"
    "${DIALOG_BORDER}${extraDashes}" " ")

  textArray=("will:"
    "     A)  Install the AUR packages listed"
    "     B)  Create the configuration files to fire up the \"$2\" $3")
  if [ ${keyDescs["C)"]+_} ]; then
    textArray+=("     C)  Skip the tasks listed for \"$4\"")
  fi
  local okText=$(getTextForDialog "${textArray[@]}")
  local cancelText="will return back to the dialog to select a \"$2\""
  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("   * Clicking the \"Confirm\" button $okText"
      "   * Clicking the \"Cancel\" button $cancelText"
      )
  else
    helpTextArray+=("   * Selecting <Confirm> $okText" "   * Selecting <Cancel > $cancelText"
      "$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER"
      "   * Select <View URL> below to visit the URLs mentioned above")
  fi

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getHelpTextForDispManConf
# DESCRIPTION: Text to display on the help/more dialog when the confirmation dialog
#              for installing a "Display Manager" is being shown.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForDispManConf() {
  local urlRef="https://wiki.archlinux.org/index.php/display_manager"
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef")
  local dmName="${POST_ASSOC_ARRAY["Display-Manager"]}"
  local textArray=("The packages/software listed to install & configuration steps are either"
    "required and/or suggested within the Arch Wiki page for \"$dmName\" Display Manager. "
    "Visit the URL specified above and then click on \"$dmName\" for further reading.")
  local concatStr=$(printf " %s" "${textArray[@]}")
  local extraDashes=""

  if [ "$DIALOG" == "yad" ]; then
    extraDashes="---------"
  fi

  helpTextArray+=("${DIALOG_BORDER}${extraDashes}" " " "${concatStr:1}"
    "${DIALOG_BORDER}${extraDashes}" " ")

  textArray=("will:"
    "     A)  Install the AUR packages listed"
    "     B)  Create/Edit the conf. files & Start/Fire Up the \"$dmName\" Display Manager")

  local okText=$(getTextForDialog "${textArray[@]}")
  local cancelText="will return back to the dialog to select a \"Display Manager\""
  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("   * Clicking the \"Confirm\" button $okText"
      "   * Clicking the \"Cancel\" button $cancelText"
      )
  else
    helpTextArray+=("   * Selecting <Confirm> $okText" "   * Selecting <Cancel > $cancelText"
      "$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER"
      "   * Select <View URL> below to visit the URLs mentioned above")
  fi

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getHelpTextForCatSoftTask
# DESCRIPTION: Text to display on the help dialog when a task for a Software Category
#              dialog/tab is displaying.
#  Required Params:
#      1) catSoftTask - the name of the task being executed.
#---------------------------------------------------------------------------------------
function getHelpTextForCatSoftTask() {
  local catSoftTask="$1"
  local urlRef="https://wiki.archlinux.org/index.php/List_of_applications"
  local dlgBorder=""
  local helpText=""
  local helpTextArray=()
  local staticText="Install AUR packages (i.e. software applications)"
  if [ "$DIALOG" == "yad" ]; then
    dlgBorder="${DIALOG_BORDER_MAX:0:112}"
  else
    dlgBorder="${DIALOG_BORDER}"
  fi

  case "$catSoftTask" in
    "Accessory Software")
      urlRef+="#Others"
      helpTextArray+=("${staticText} that are in the accessory/other category such as:"
                      "     *  Calculators," "     *  Sticky Notes," "     *  Time Tracking, etc.")
    ;;
    "Application & Web Servers")
      urlRef="https://wiki.archlinux.org/index.php/server"
      helpTextArray=("Install AUR packages (i.e. server software) to run web applications locally"
                      "and/or serve HTML web pages and other files via HTTP to clients like web browsers")
      helpText=$(printf " %s" "${helpTextArray[@]}")
      helpTextArray=("${helpText:1} such as:"
                      "     *  Apache Tomcat," "     *  WildFly," "     *  Apache," "     *  nginx, etc.")
    ;;
    "Database Software")
      if [ "$DIALOG" == "yad" ]; then
        urlRef="https://wiki.archlinux.org/index.php/Category%3ADatabase_management_systems"
      else
        urlRef="https://wiki.archlinux.org/index.php/Category:Database_management_systems"
      fi
      helpTextArray+=("${staticText} that are in the 'Database management systems' category from sub-categories such as:"
          "     *  NoSQL - 'Apache Cassandra', 'MongoDB'" "     *  Relational DBMSs - 'PostgreSQL', 'SQL Server'"
          "     *  Tools - 'pgAdmin', 'DBeaver'" "     *  etc.")
    ;;
    "Development Software")
      urlRef+="#Development"
      helpTextArray=("${staticText} that will help with developing software such as:"
                      "     *  Build Automation," "     *  IDEs," "     *  Java, etc.")
    ;;
    "Documents & Texts Software")
      urlRef+="#Documents_and_texts"
      helpTextArray=("${staticText} that will be able to create & read documents such as:"
                      "     *  Office Suites," "     *  PDF," "     *  Text Editors, etc.")
    ;;
    "Internet Software")
      urlRef+="#Internet"
      helpTextArray=("${staticText} that will enable to connect and interact with the Internet such as:"
                      "     *  File Sharing," "     *  Network Managers," "     *  Web Browsers, etc.")
    ;;
    "Multimedia Software")
      urlRef+="#Multimedia"
      helpTextArray=("${staticText} that are in the multimedia category such as:"
                      "     *  Audio," "     *  Codecs," "     *  Image." "     *  Video, etc.")
    ;;
    "System & Utility Software")
      urlRef+="#Utilities"
      helpTextArray=("${staticText} that are the tools to configure & monitor the system such as:"
                      "     *  Archiving & Compression Tools," "     *  File Managers,"
                      "     *  Terminal Emulators, etc.")
    ;;
    "Security Software")
      urlRef+="#Security"
      helpTextArray=("${staticText} for hardening the Arch Linux system such as:"
                      "     *  Firewalls," "     *  Network," "     *  Password Managers, etc.")
    ;;
    "Software Extras")
      helpTextArray=("${staticText} that either can be installed using a customized script or"
                    "don't necessarily belong to any one of the other categories such as:")
      helpText=$(printf " %s" "${helpTextArray[@]}")
      helpTextArray=("${helpText}" "     *  Auto Background Changer," "     *  KStars,"
                    "     *  Profile Sync Daemon, etc." " ")
    ;;
    *)
      clog_error "No case statement defined for catSoftTask: [$catSoftTask]"
      exit 1
    ;;
  esac

  helpText=$(getHelpTextForButtons "$catSoftTask")
  helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef" "$dlgBorder" " " "${helpTextArray[@]}" "$dlgBorder" " "
                "$helpText")

  helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getHelpTextForButtons
# DESCRIPTION: Get the text for all the buttons within the dialog for a category
#              Software Task.
#  Required Params:
#      1) catSoftTask - the name of the task being executed.
#---------------------------------------------------------------------------------------
function getHelpTextForButtons() {
  local btnHelpTextArray=()
  local btnTextArray=()
  local skipText=$(getHelpTextForSkipBtn "$1")
  local concatStr=""
  if [ "$DIALOG" == "yad" ]; then
    concatStr=$(getHelpTextForOkBtn "$1")
    btnHelpTextArray=("   * Clicking the \"Ok\" button $concatStr" " ")
    btnTextArray=("will close the '$1' tab and return back to the Post Installation step"
                  "displaying the 'Checklist' tab.")
    concatStr=$(printf " %s" "${btnTextArray[@]}")
    btnHelpTextArray+=("   * Clicking the \"Cancel\" button ${concatStr:1}" " ")
    btnTextArray=("will append a new tab to display the available AUR packages that can be installed"
                  "and/or sub-caterogies for the category that was selected within the '$1' tab.")
    concatStr=$(printf " %s" "${btnTextArray[@]}")
    btnHelpTextArray+=("   * Clicking the \"Select\" button ${concatStr:1}" " ")
    btnHelpTextArray+=("   * Clicking the \"Skip\" button $skipText" " ")
  else
    concatStr="will display a dialog with the options that can be selected for the chosen category."
    btnHelpTextArray=("   * Selecting <  Ok  > $concatStr")
    btnHelpTextArray+=("   * Selecting < Skip > $skipText")
    btnTextArray=("will close the dialog for the '$1' task and return back to the"
                  "dialog for the checklist of the Post Installation step.")
    concatStr=$(printf " %s" "${btnTextArray[@]}")
    btnHelpTextArray+=("   * Selecting <Cancel> ${concatStr:1}")
  fi

  local btnHelpText=$(getTextForDialog "${btnHelpTextArray[@]}")

  echo "$btnHelpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     getHelpTextForOkBtn
# DESCRIPTION: Get the text for the ok button on a YAD notebook dialog.
#  Required Params:
#      1) catSoftTask - the name of the task being executed.
#   Optional Param:
#      2) name of category currently selected.
#---------------------------------------------------------------------------------------
function getHelpTextForOkBtn() {
  local okBtnTextArray=(
    "      NOTE:  If the name of the Software Task selected within the 'Checklist' tab does"
    "not match '$1', then the installer will:"
  )
  local concatStr=$(printf " %s" "${okBtnTextArray[@]}")

  okBtnTextArray=("will process all the tabs from left to right starting with the 'Checklist' tab." "${concatStr:1}"
        "            a)  Store the data for this software category tasks task into the Installer's 'data' directory. ")

  if [ "$#" -gt 1 ]; then
    okBtnTextArray+=("            b)  Close the '$1' & '$2' tabs.")
  else
    okBtnTextArray+=("            b)  Close the '$1' tab.")
  fi

  okBtnTextArray+=("            c)  Return back to the Post Installation step displaying the 'Checklist' tab.")

  local okBtnHelpText=$(getTextForDialog "${okBtnTextArray[@]}")

  echo "$okBtnHelpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getHelpTextForSkipBtn
# DESCRIPTION: Get the text for the 'Skip' button within the dialog for a category
#              Software Task.
#  Required Params:
#      1) catSoftTask - the name of the task being executed.
#---------------------------------------------------------------------------------------
function getHelpTextForSkipBtn() {
  local appendTxt=""

  if [ "$DIALOG" == "yad" ]; then
    appendTxt="that have a blank value in the 'Is Done' column to 'Skipped'."
  else
    appendTxt="that have a blank value between the braces in the last column to '[S]'."
  fi

  local sbtnTextArray=("will: "
     "     1)  Mark/set the software category tasks $appendTxt"
     "     2)  Store the data for this software category task into the Installer's 'data' directory."
     "     3)  Mark/set '$1' in the checklist for the Post Installation step as DONE."
     "     4)  Return back to the Post Installation step and display its 'Checklist' dialog.")
  local text=$(getTextForDialog "${sbtnTextArray[@]}")

  echo "$text"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                getHelpTextForCategoryDialog
# DESCRIPTION: Text to display on the help dialog when the options for a category
#              is displaying.
#  Required Params:
#      1) catSoftTask - the name of the task being executed.
#      2)     softCat - the name of the category that was selected.
#---------------------------------------------------------------------------------------
function getHelpTextForCategoryDialog() {
  local helpTextArray=()
  local btnTextArray=()
  local skipText=$(getHelpTextForSkipCatBtn "$1" "$2")
  local concatStr=""


  if [ "$2" == "Custom Scripts" ]; then
    local userName=$(getNameOfNormalUser)
    concatStr=$(getHelpTextForAlacritty)
    helpTextArray+=("$concatStr")
    concatStr=$(getHelpTextForTomcat "$userName")
    helpTextArray+=("$concatStr")
    concatStr=$(getHelpTextForAutoBckgndChanger "$userName")
    helpTextArray+=("$concatStr")
    concatStr=$(getHelpTextForNexusOSS)
    helpTextArray+=("$concatStr")
    concatStr=$(getHelpTextForPostgreSQL "$userName")
    helpTextArray+=("$concatStr" "${DIALOG_BORDER_MAX:0:112}" " ")
  fi

  if [ "$DIALOG" == "yad" ]; then
    concatStr=$(getHelpTextForOkBtn "$1" "$2")
    helpTextArray+=("   * Clicking the \"Ok\" button $concatStr" " ")
    concatStr="will close the '$2' tab and return back to the '$1' tab."
    helpTextArray+=("   * Clicking the \"Cancel\" button $concatStr" " ")
    concatStr=$(getHelpTextForInstCatBtn "$1" "$2")
    helpTextArray+=("   * Clicking the \"Install\" button $concatStr" " ")
    helpTextArray+=("   * Clicking the \"Skip\" button $skipText" " ")
  else
    concatStr="will display a dialog with the options that can be selected for the chosen category."
    helpTextArray+=("   * Selecting <  Ok  > $concatStr")
    helpTextArray+=("   * Selecting < Skip > $skipText")
    concatStr="will close the dialog for the '$2' category and return back to the '$1' dialog."
    helpTextArray+=("   * Selecting <Cancel> $concatStr")
  fi

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getHelpTextForSkipCatBtn
# DESCRIPTION: Get the text for the 'Skip' button within the dialog for a category
#              is being shown.
#  Required Params:
#      1) catSoftTask - the name of the task being executed.
#      2)     softCat - the name of the category that was selected.
#---------------------------------------------------------------------------------------
function getHelpTextForSkipCatBtn() {
  local appendTxt=""
  local winType=""

  if [ "$DIALOG" == "yad" ]; then
    appendTxt="in the 'Is Done' column to 'Skipped'."
    winType="tab"
  else
    appendTxt="in the last column to '[S]'."
    winType="dialog"
  fi

  local sbtnTextArray=("will: "
     "     1)  Mark/set the '$2' category $appendTxt"
     "     2)  Return back to the '$1' $winType.")
  local text=$(getTextForDialog "${sbtnTextArray[@]}")

  echo "$text"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getHelpTextForInstCatBtn
# DESCRIPTION: Get the text for when either the "Install" button or <  Ok  > is clicked
#              within the category tab/dialog.
#  Required Params:
#      1) catSoftTask - the name of the task being executed.
#      2)     softCat - the name of the category that was selected.
#---------------------------------------------------------------------------------------
function getHelpTextForInstCatBtn() {
  local appendTxt=""
  local winType=""

  if [ "$DIALOG" == "yad" ]; then
    winType="tab"
  else
    winType="dialog"
  fi

  local sbtnTextArray=("will: "
     "     1)  Install the AUR packages selected in the '$2' category $winType"
     "     2)  Save the selected packages into the Installer's 'data' directory."
     "     3)  Close the $winType for the '$2' category and return back to the '$1' $winType.")
  local text=$(getTextForDialog "${sbtnTextArray[@]}")

  echo "$text"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:               getHelpTextForSubCatDialog
# DESCRIPTION: Text to display on the help dialog when the options available in the
#              dialog/tab are sub-categories.
#  Required Params:
#      1) catSoftTask - the name of the task being executed.
#      2)     softCat - the name of the category that was selected.
#---------------------------------------------------------------------------------------
function getHelpTextForSubCatDialog() {
  local helpTextArray=()
  local btnTextArray=()
  local skipText=$(getHelpTextForSkipCatBtn "$1" "$2")
  local concatStr=""
  if [ "$DIALOG" == "yad" ]; then
    concatStr=$(getHelpTextForOkBtn "$1" "$2")
    helpTextArray=("   * Clicking the \"Ok\" button $concatStr" " ")
    concatStr="will close the '$2' tab and return back to the '$1' tab."
    helpTextArray+=("   * Clicking the \"Cancel\" button $concatStr" " ")
    concatStr=$(getHelpTextForCommitCatBtn "$1" "$2")
    helpTextArray+=("   * Clicking the \"Commit\" button $concatStr" " ")
    btnTextArray=("will append a new tab to display the available AUR packages that can be installed"
                  "for the sub-category that was selected.")
    concatStr=$(printf " %s" "${btnTextArray[@]}")
    helpTextArray+=("   * Clicking the \"Select\" button ${concatStr:1}" " ")
    helpTextArray+=("   * Clicking the \"Skip\" button $skipText" " ")
  else
    concatStr="will display a dialog with the options that can be selected for the chosen sub-category."
    helpTextArray=("   * Selecting <  Ok  > $concatStr")
    helpTextArray+=("   * Selecting < Skip > $skipText")
    concatStr=$(getHelpTextForCommitCatBtn "$1" "$2")
    helpTextArray+=("   * Selecting <Commit> $concatStr")
  fi

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getHelpTextForCommitCatBtn
# DESCRIPTION: Get the text for when either the "Commit" button or <Commit> is clicked
#              within the category tab/dialog that has sub-categories as the options.
#  Required Params:
#      1) catSoftTask - the name of the task being executed.
#      2)     softCat - the name of the category that was selected.
#---------------------------------------------------------------------------------------
function getHelpTextForCommitCatBtn() {
  local appendTxt=""
  local winType=""

  if [ "$DIALOG" == "yad" ]; then
    winType="tab"
  else
    winType="dialog"
  fi

  local sbtnTextArray=("will: "
     "     1)  Install the AUR packages selected in the sub-category ${winType}s"
     "     2)  Save the selected packages into the Installer's 'data' directory."
     "     3)  Close the $winType for the '$2' category and return back to the '$1' $winType.")
  local text=$(getTextForDialog "${sbtnTextArray[@]}")

  echo "$text"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getHelpTextForSubCatBtns
# DESCRIPTION: Get the text for all buttons within the dialog to the display the options
#              for a sub-category.
#   Optional Param:
#      1) name of the sub-category that has a dialog of type "radiolist"
#         (i.e. Eclipse, IntelliJ, etc.)
#---------------------------------------------------------------------------------------
function getHelpTextForSubCatBtns() {
  local btnHelpTextArray=()
  local saveStr=""
  local cancelStr=""

  if [ "$#" -gt 0 ]; then
    saveStr="will store the selected AUR package for '$1' in memory."
    cancelStr="will remove the previously selected AUR package for '$1' from memory."
  else
    saveStr="will store the AUR package(s) selected in memory."
    #### Text is not wrapping in dialog
    btnHelpTextArray=("will remove the AUR package(s) previously" "selected from memory.")
    cancelStr=$(getTextForDialog "${btnHelpTextArray[@]}")
    cancelStr="will remove the AUR package(s) previously selected from memory."
  fi

  if [ "$DIALOG" == "yad" ]; then
    btnHelpTextArray=("   * Clicking the \"Save\" button $saveStr" " ")
    btnHelpTextArray+=("   * Clicking the \"Cancel\" button $cancelStr" " ")
  else
    btnHelpTextArray=("   * Selecting < Save > $saveStr")
    btnHelpTextArray+=("   * Selecting <Cancel> $cancelStr")
  fi

  local btnHelpText=$(getTextForDialog "${btnHelpTextArray[@]}")

  echo "$btnHelpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getHelpTextForEclipse
# DESCRIPTION: Text to display on the help dialog when the Eclipse sub-category
#              dialog/tab is displaying.
#---------------------------------------------------------------------------------------
function getHelpTextForEclipse() {
  local urlRef="https://wiki.archlinux.org/index.php/Eclipse"
  local stsRef="https://spring.io/tools"
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef" " "
    "Spring Tool Suite:" "${URL_REF_BORDER}---" "$stsRef")
  local textArray=("Eclipse is an open source community project, which aims to provide a universal"
    "development platform.  The Eclipse project is most widely known for its cross-platform integrated"
    "development environment (IDE).")
  local concatStr=$(printf " %s" "${textArray[@]}")
  local dlgBorder=""
  if [ "$DIALOG" == "yad" ]; then
    dlgBorder="${DIALOG_BORDER_MAX:0:112}"
  else
    dlgBorder="${DIALOG_BORDER}"
  fi

  helpTextArray+=("$dlgBorder" " " "${concatStr:1}")

  textArray=("Installing multiple instances of Eclipse at the same time is not only unnecessary,"
    "but will also cause them to conflict with one-another.  Choose the package which most immediately"
    "fulfills your needs.  Support for any additionally required languages can be added either through"
    "the Eclipse Marketplace or the IDE's plugin manager")
  concatStr=$(printf " %s" "${textArray[@]}")
  helpTextArray+=(" " "${concatStr:1}")
  concatStr=$(getHelpTextForSubCatBtns "Eclipse")
  helpTextArray+=("$dlgBorder" " " "$concatStr")

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getHelpTextForIntelliJ
# DESCRIPTION: Text to display on the help dialog when the Eclipse sub-category
#              dialog/tab is displaying.
#---------------------------------------------------------------------------------------
function getHelpTextForIntelliJ() {
  local urlRef="https://en.wikipedia.org/wiki/IntelliJ_IDEA"
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef")
  local textArray=("IntelliJ IDEA is a Java integrated development environment (IDE) for developing"
      "computer software.  It is developed by JetBrains (formerly known as IntelliJ), and is available as:")
  local concatStr=$(printf " %s" "${textArray[@]}")
  local dlgBorder=""
  if [ "$DIALOG" == "yad" ]; then
    dlgBorder="${DIALOG_BORDER_MAX:0:112}"
  else
    dlgBorder="${DIALOG_BORDER}"
  fi

  helpTextArray+=("$dlgBorder" " " "${concatStr:1}" "     - Community Edition (CE)"
    "     - Ultimate Edition (UE), which is the proprietary commercial edition" "NOTE:" "$NOTE_BORDER"
    "The Ultimate Edition requires the purchase of a license after the evaluation period expires.")

  textArray=("Both can be used for commercial development.  However, both cannot be installed at the same time"
    "because they will conflict with one-another.")
  concatStr=$(printf " %s" "${textArray[@]}")
  helpTextArray+=(" " "${concatStr:1}")
  concatStr=$(getHelpTextForSubCatBtns "IntelliJ")
  helpTextArray+=("$dlgBorder" " " "$concatStr")

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getHelpTextForAlacritty
# DESCRIPTION: Text to describe the custom script for installing the Alacritty terminal.
#---------------------------------------------------------------------------------------
function getHelpTextForAlacritty() {
  local urlRef="https://github.com/jwilm/alacritty"
  local helpTextArray=("In addition to installing the AUR package 'alacritty', "
      "the installer will also install the following AUR packages:")
  local concatStr=$(printf " %s" "${helpTextArray[@]}")
  helpTextArray=("${DIALOG_BORDER_MAX:0:112}" " " "URL Reference:"
    "$URL_REF_BORDER" "$urlRef" "${DIALOG_BORDER_MAX:0:112}" " " "${concatStr:1}" "     > xclip"
    "     > ranger" "     > adobe-source-code-pro-fonts" "     > awesome-terminal-fonts")
  local helpText=$(getTextForDialog "${helpTextArray[@]}")
  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getHelpTextForTomcat
# DESCRIPTION: Text to describe the custom script for installing the Apache Tomcat.
#  Required Params:
#      1) userName - the name of the normal user.
#---------------------------------------------------------------------------------------
function getHelpTextForTomcat() {
  local userName="$1"
  local urlRef="https://wiki.archlinux.org/index.php/tomcat"
  local helpTextArray=("Sets the permissions and ownership on the files and sub-directories"
      "within the installed Apache Tomcat directory to give the user '$userName' read & write"
      "access in order to allow them to make changes.")
  local permText=$(printf " %s" "${helpTextArray[@]}")
  local customBorder="------------------------------"

  if [ "$DIALOG" == "yad" ]; then
    customBorder+="------------------------------"
  fi

  helpTextArray=("${DIALOG_BORDER_MAX:0:112}" " " "Reference for 'Apache Tomcat':"
    "$customBorder" "$urlRef" "${DIALOG_BORDER_MAX:0:112}" " "
    "The advantage of using the customized script to install 'Apache Tomcat' are:"
    "     1)  Installs the latest build version for any of the Tomcat versions selected."
    "     2)  Sets up an administrative user to access Tomcat's 'admin' & 'manager' web applications!"
    "     3)  Creates a 'setenv.sh' to:"
    "         a) set instance specific env. variables such as JAVA_HOME & CATALINA_HOME for each version installed"
    "         b) easily run Tomcat with different Java runtime arguments (i.e. Heap)"
    "         c) enable JMX Remote in order to monitor Tomcat remotely"
    "     4)  Creates a service file so that Tomcat can be started/stopped using 'systemctl'."
    "     5)  Creates the service wrapper script '/usr/bin/tomcat-srvc' so that the user '$userName'"
    "         a) can start & stop Tomcat using 'systemctl'"
    "         b) run Tomcat in Remote Debug mode"
    "     6)  ${permText:1}")
  local helpText=$(getTextForDialog "${helpTextArray[@]}")
  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:               getHelpTextForAutoBckgndChanger
# DESCRIPTION: Text to describe the custom script for installing the Auto Background
#              Changer.
#---------------------------------------------------------------------------------------
function getHelpTextForAutoBckgndChanger() {
  local userName="$1"
  local urlRef="https://github.com/AlvinJian/auto_background_changer"
  local helpTextArray=("The installer adds support of Nitrogen and a randomization script"
      "that randomizes the image displayed on the desktop using the files located in"
      "the directory /home/${userName}/Pictures/Wallpapers.")
  local concatStr=$(printf " %s" "${helpTextArray[@]}")
  local customBorder="-----------------------------------------"

  if [ "$DIALOG" == "yad" ]; then
    customBorder+="-----------------------------------------"
  fi

  helpTextArray=("${DIALOG_BORDER_MAX:0:112}" " " "Reference for 'Auto Background Changer':"
    "$customBorder" "$urlRef" "${DIALOG_BORDER_MAX:0:112}" " " "${concatStr:1}")
  local helpText=$(getTextForDialog "${helpTextArray[@]}")
  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getHelpTextForNexusOSS
# DESCRIPTION: Text to describe the custom script for installing the Nexus OSS
#              repository manager.
#  Required Params:
#      1) userName - the name of the normal user.
#---------------------------------------------------------------------------------------
function getHelpTextForNexusOSS() {
  local userName="$1"
  local urlRef="https://help.sonatype.com/repomanager3"
  local textArray=("If you’re doing software development, you’re more than likely"
    "deploying to a local Maven repository. The Nexus repository manager tends to be"
    "one of the more popular options, and with both and open source version (Nexus OSS)"
    "and one that comes with paid support (Nexus Pro), there really aren’t any articulable barriers to adoption.")
  local concatStr=$(printf " %s" "${textArray[@]}")
  local customBorder="------------------------------"

  if [ "$DIALOG" == "yad" ]; then
    customBorder+="------------------------------"
  fi

  local helpTextArray=("${DIALOG_BORDER_MAX:0:112}" " " "Reference for 'Nexus OSS v3':"
    "$customBorder" "$urlRef" "${DIALOG_BORDER_MAX:0:112}" " " "${concatStr:1}" " "
    "The advantage of using the customized script to install 'Nexus OSS v3' are:"
    "     1)  Installs the latest build version."
    "     2)  Configures the necessary property files so that:"
    "         a) the server runs on port 9091 and shouldn't conflict with other servers running.")

  textArray=("will use either 'jdk8' or 'jdk8-openjdk' if already installed.  Otherwise, the script"
    "will install the AUR package 'jdk8-openjdk'.")
  concatStr=$(printf " %s" "${textArray[@]}")
  helpTextArray+=("         b) ${concatStr:1}"
    "     3)  Creates the service wrapper script '/usr/bin/nexus-oss' so that the user '$userName'"
    "         a) can start & stop nexus using 'systemctl'"
    "         b) tail the log file")

  textArray=("Installs the application to a separate partition.  The default name of the partition is '/nexus-oss'. "
    "This will allow space to NOT be consumed with '/var' & '/usr' that other applications might need.")
  concatStr=$(printf " %s" "${textArray[@]}")
  helpTextArray+=("     4)  ${concatStr:1}")

  textArray=("NOTE:  This script can be run after the system has been rebooted.  A different"
    "partition name can then be used and passed into the script using the '-part' option.")
  concatStr=$(printf " %s" "${textArray[@]}")
  helpTextArray+=("         ${concatStr:1}")

  textArray=("Tells pacman to skip the upgrade.  This ensures that 'Nexus OSS v3' is not accidentally upgraded"
    "to a potentially incompatible version as well as keeping the internal databases intact.")
  concatStr=$(printf " %s" "${textArray[@]}")
  helpTextArray+=("     5)  ${concatStr:1}")

  local helpText=$(getTextForDialog "${helpTextArray[@]}")
  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getHelpTextForPostgreSQL
# DESCRIPTION: Text to describe the custom script for installing PostgreSQL.
#  Required Params:
#      1) userName - the name of the normal user.
#---------------------------------------------------------------------------------------
function getHelpTextForPostgreSQL() {
  local userName="$1"
  local urlRef="https://wiki.archlinux.org/index.php/PostgreSQL"
  local textArray=("Builds & Installs the latest build version from the source code.  This can potentially"
    "fix issues that the precompiled version from the AUR repository might be causing.")
  local concatStr=$(printf " %s" "${textArray[@]}")
  local customBorder="---------------------------"

  if [ "$DIALOG" == "yad" ]; then
    customBorder+="---------------------------"
  fi

  local helpTextArray=("${DIALOG_BORDER_MAX:0:112}" " " "Reference for 'PostgreSQL':"
    "$customBorder" "$urlRef" "${DIALOG_BORDER_MAX:0:112}" " "
    "The advantage of using the customized script to install 'PostgreSQL' are:"
    "     1)  ${concatStr:1}")
  textArray=("Installs the application to a separate partition.  The default name of the partition is"
    "'/dbhome'.  This will allow space to NOT be consumed with '/var' & '/usr' that other applications might need.")
  concatStr=$(printf " %s" "${textArray[@]}")

  helpTextArray+=("     2)  ${concatStr:1}")

  textArray=("NOTE:  This script can be run after the system has been rebooted.  A different"
    "partition name can then be used and passed into the script using the '-part' option.")
  concatStr=$(printf " %s" "${textArray[@]}")

  helpTextArray+=("         ${concatStr:1}"
    "     3)  Initializes the database cluster."
    "     4)  Edits the configuration file so that it can perform better."
    "     5)  Creates a service file so that it can be started/stopped using 'systemctl'."
    "     6)  Creates the service wrapper script '/usr/bin/pgsql-srvc' so that the user '$userName'"
    "         a) can start & stop postgres using 'systemctl'"
    "         b) tail the log file")

  textArray=("Tells pacman to skip the upgrade.  This ensures that 'PostgreSQL' is not accidentally upgraded"
    "to a potentially incompatible version.")
  concatStr=$(printf " %s" "${textArray[@]}")
  helpTextArray+=( "     7)  ${concatStr:1}")

  local helpText=$(getTextForDialog "${helpTextArray[@]}")
  echo "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     getHelpTextForConky
# DESCRIPTION: Text to display on the help/more dialog when the dialog to select a
#              'Conky' package is shown.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getHelpTextForConky() {
  local urlRef="https://wiki.archlinux.org/index.php/conky"
  local helpTextArray=("URL Reference:" "$URL_REF_BORDER" "$urlRef")
  local lines=("Conky is a system monitor software for the X Window System. It is"
  "available for GNU/Linux and FreeBSD. It is free software released under"
  "the terms of the GPL license. Conky is able to monitor many system variables"
  "including CPU, memory, swap, disk space, temperature, top, upload, download,"
  "system messages, and much more. It is extremely configurable, however, the"
  "configuration can be a little hard to understand.")
  local concatStr=$(printf " %s" "${lines[@]}")
  local extraDashes=""
  if [ "$DIALOG" == "yad" ]; then
    extraDashes="---------"
  fi

  helpTextArray+=("${DIALOG_BORDER}${extraDashes}" " " "${concatStr:1}")

  lines=("In addition to the Conky package selected, the 'Conky Manager' package will also be"
  "installed. This is the theme manager for the Conky widgets. It provides options to"
  "start/stop, browse and edit Conky themes installed on the system.")
  concatStr=$(printf " %s" "${lines[@]}")
  helpTextArray+=(" " "${concatStr:1}")

  lines=("Sudo can also be used to run commands as other users; additionally, sudo logs"
  "all commands and failed access attempts for security auditing.")
  concatStr=$(printf " %s" "${lines[@]}")
  helpTextArray+=(" " "${concatStr:1}" "${DIALOG_BORDER}${extraDashes}" " ")

  local okText="will install the selected Conky package & the AUR 'conky-manager' package!"
  if [ "$DIALOG" == "yad" ]; then
    helpTextArray+=("   * Clicking the \"Ok\" button $okText")
  else
    helpTextArray+=("   * Selecting < Ok > $okText"
    "   * Select <View URL> below to visit the URL mentioned above")
  fi

  local helpText=$(getTextForDialog "${helpTextArray[@]}")

  echo "$helpText"
}
