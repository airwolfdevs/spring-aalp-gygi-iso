#!/bin/bash

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# globals
#===============================================================================
CUR_DIR="${PWD}"
INC_FILE=$(echo "$CUR_DIR/inc/aalp-gygi-consts.sh")

source "$INC_FILE"

#### Load all the include files
declare INCLUDE_FILES=("common-funcs.sh" "post-inst-help-text.sh" "inc_funcs.sh" "log_funcs.sh" "string_funcs.sh")
for fileName in "${INCLUDE_FILES[@]}"; do
  source "$BASE_DIR/inc/$fileName"
done

