#!/bin/bash

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# globals
#===============================================================================
declare -A TAB_SEL_VALS=()

#===============================================================================
# functions/methods
#===============================================================================

#---------------------------------------------------------------------------------------
#      METHOD:                    selectUnoffUserRepos
# DESCRIPTION: Show a linux "checklist" dialog to select the unofficial signed & unsigned
#              user repositories.  The selections will be set within the global
#              associative array variable "TAB_SEL_VALS".  The keys will be:
#                      - "Signed" for the selected signed repository names
#                      - "Unsigned" for the selected unsigned repository names
#  Required Params:
#      1) dialogTitle - the title to be displayed at the top of the window
#      2)    helpText - Text to display on the help/more dialog
#---------------------------------------------------------------------------------------
selectUnoffUserRepos() {
  local dialogText="Select the type of user repositories to add:"
  local dialogTitle="$1"
  local -A typeDialogParams=(["dialogHeight"]=20 ["dialogWidth"]=80 ["extra-label"]="Done" ["cancel-label"]="Skip"
    ["help-label"]="Help" ["help-text"]="$2" ["help-title"]="Help:  $1" ["Signed"]="User Repository"
    ["Unsigned"]="User Repository")
  local -A signedDlgParams=(["defaultChoice"]="archlinuxcn" ["dialogHeight"]=30 ["dialogWidth"]=80)
  local -A unsignedDlgParams=(["defaultChoice"]="archlinuxfr" ["dialogHeight"]=30 ["dialogWidth"]=80)
  local -n dialogParms="typeDialogParams"
  local repoTypes=("Signed" "Unsigned")
  local -n signedOpts="SIGNED_MENU_OPTS"
  local -n unsignedOpts="UNSIGNED_MENU_OPTS"
  local urls=()
  local repoType=""
  local sep=" "

  setELinksURLs "$2"

  addDescsToDlgParams "SIGNED_MENU_OPTS" "SIGNED_REPO_DESCS" "signedDlgParams"
  addDescsToDlgParams "UNSIGNED_MENU_OPTS" "UNSIGNED_REPO_DESCS" "unsignedDlgParams"

  while true; do
    exec 3>&1
      case "$repoType" in
        "Signed")
          showListOfChoicesDialogBox "$dialogTitle" "$repoType User Repository" "$dialogText" signedOpts "checklist" signedDlgParams 2>&1 1>&3
        ;;
        "Unsigned")
          showListOfChoicesDialogBox "$dialogTitle" "$repoType User Repository" "$dialogText" unsignedOpts "checklist" unsignedDlgParams 2>&1 1>&3
        ;;
        *)
          showListOfChoicesDialogBox "$POST_INST_DIALOG_BACK_TITLE" "$dialogTitle" "$dialogText" repoTypes "radiolist" typeDialogParams 2>&1 1>&3
        ;;
      esac      
    exec 3>&-
    
    IFS=',' read -a fields <<< "$mbSelVal"
    case ${fields[0]} in
      $DIALOG_OK)
        case "$repoType" in
          "Signed"|"Unsigned")
            local concatStr=$(echo "${fields[1]//\"/}")
            local rows=()
            readarray -t rows <<< "${concatStr//$sep/$'\n'}"
            concatStr=$(printf "\n%s" "${rows[@]}")
            TAB_SEL_VALS["$repoType"]="${concatStr:1}"
            repoType=""
          ;;
          *)
            repoType=$(echo "${fields[1]//\"/}")
          ;;
        esac
      ;;
      $DIALOG_EXTRA)
        break
      ;;
      $DIALOG_HELP)
        if [ "${fields[1]}" == "View-URL" ]; then
          elinks "${urls[0]}"
        fi
      ;;
      *)
        case "$repoType" in
          "Signed"|"Unsigned")
            unset TAB_SEL_VALS["$repoType"]
            repoType=""
          ;;
          *)
            TAB_SEL_VALS=()
            break
          ;;
        esac
      ;;
   esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                     selectOptionForSUDO
# DESCRIPTION: Select the option to use when issuing the "sudo" command using a linux
#              "radiolist" dialog.  Set the value within the global variable "mbSelVal".
#  Required Params:
#      1) paramAssocArray - the name of the associative array with the required
#                           key/value pairs:
#      "dialogBackTitle" - String to be displayed at the top of the window.
#      "dialogTitle"     - String to be displayed at the top of the dialog window.
#      "dialogText"      - Text to display inside the dialog before the list of menu options
#      "helpText"        - Text to display on the help/more dialog
#      "menuOptsRef"     - name of the array containing the menu options
#      "optDescsRef"     - name of the associative array containing the description
#                          of the menu options
#---------------------------------------------------------------------------------------
selectOptionForSUDO() {
  local -n paramAssocArray="$1"
  paramAssocArray["nocancel"]="true"

  showLinuxListDialog "paramAssocArray"
}

#---------------------------------------------------------------------------------------
#      METHOD:                       selectAURHelper
# DESCRIPTION: Show a linux "radiolist" dialog to select either a command-line or gui
#              AUR helper package.  Set the selected values in the global associative
#              array "TAB_SEL_VALS".
#  Required Params:
#      1) dialogTitle - the title to be displayed at the top of the window
#      2)    helpText - Text to display on the help/more dialog
#---------------------------------------------------------------------------------------
selectAURHelper() {
  local -n paramAssocArray="$1"
  local dialogTitle="${paramAssocArray["dialogTitle"]}"
  local windowTitle="${paramAssocArray["dialogBackTitle"]}"
  local helpText="${paramAssocArray["helpText"]}"
  local -A catDialogParams=(["dialogHeight"]=15 ["dialogWidth"]=80 ["cancel-label"]="Skip"
    ["help-label"]="Help" ["help-text"]="$helpText" ["help-title"]="Help:  $dialogTitle")
  local -A cliDlgParams=(["defaultChoice"]="Trizen" ["dialogHeight"]=20 ["dialogWidth"]=80)
  local -A guiDlgParams=(["defaultChoice"]="Octopi" ["dialogHeight"]=20 ["dialogWidth"]=80)
  local urls=()
  local aurType=""
  local sep=" "
  local -A dlgTextArray=(["type"]="Select the type of AUR helper to install:"
    ["Command Line"]="Select the name of the 'Command Line' AUR helper to install:"
    ["Graphical"]="Select the name of the 'Graphical' AUR helper to install:")
  local dialogText=""

  setELinksURLs "$helpText"

  addDescsToDlgParams "CAT_MENU_OPTS" "DESC_ASSOC_ARRAY" "catDialogParams"
  addDescsToDlgParams "CLI_AUR_HELPERS" "AUR_PCKG_DESCS" "cliDlgParams"
  addDescsToDlgParams "GUI_AUR_HELPERS" "AUR_PCKG_DESCS" "guiDlgParams"

  while true; do
    if [ ${dlgTextArray["$aurType"]+_} ]; then
      dialogText="${dlgTextArray["$aurType"]}"
    else
      dialogText="${dlgTextArray["type"]}"
    fi
    exec 3>&1
      case "$aurType" in
        "Command Line")
          showListOfChoicesDialogBox "$dialogTitle" "$aurType AUR Helpers" "$dialogText" CLI_AUR_HELPERS "radiolist" cliDlgParams 2>&1 1>&3
        ;;
        "Graphical")
          showListOfChoicesDialogBox "$dialogTitle" "$aurType AUR Helpers" "$dialogText" GUI_AUR_HELPERS "radiolist" guiDlgParams 2>&1 1>&3
        ;;
        *)
          showListOfChoicesDialogBox "$windowTitle" "$dialogTitle" "$dialogText" CAT_MENU_OPTS "radiolist" catDialogParams 2>&1 1>&3
        ;;
      esac
    exec 3>&-

    IFS=',' read -a fields <<< "$mbSelVal"
    case ${fields[0]} in
      $DIALOG_OK)
        case "$aurType" in
          "Command Line"|"Graphical")
            TAB_SEL_VALS["$aurType"]=$(echo "${fields[1]//\"/}")
            break
          ;;
          *)
            aurType=$(echo "${fields[1]//\"/}")
            aurType="${DESC_ASSOC_ARRAY["$aurType"]}"
            TAB_SEL_VALS["AUR_HELPER_TYPE"]="$aurType"
          ;;
        esac
      ;;
      $DIALOG_HELP)
        if [ "${fields[1]}" == "View-URL" ]; then
          elinks "${urls[0]}"
        fi
      ;;
      *)
        case "$aurType" in
          "Command Line"|"Graphical")
            unset TAB_SEL_VALS["$aurType"]
            aurType=""
          ;;
          *)
            TAB_SEL_VALS=()
            break
          ;;
        esac
      ;;
   esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                     selectDefaultShell
# DESCRIPTION: Select the name of the shell to install using a linux "radiolist" dialog.
#              Set the value within the global array variable "TAB_SEL_VALS".
#  Required Params:
#      1) paramAssocArray - the name of the associative array with the required
#                           key/value pairs:
#      "dialogTitle"   - String to be displayed at the top of the dialog window.
#      "helpText"      - Text to display on the help/more dialog
#      "menuOptsRef"   - name of the array containing the menu options
#      "optDescsRef"   - name of the associative array containing the description
#                        of the menu options
#---------------------------------------------------------------------------------------
selectDefaultShell() {
  local -n paramAssocArray="$1"
  local dialogTitle="${paramAssocArray["dialogTitle"]}"
  local windowTitle="${paramAssocArray["dialogBackTitle"]}"
  local helpText="${paramAssocArray["helpText"]}"
  local -A catDialogParams=(["dialogHeight"]=15 ["dialogWidth"]=80 ["cancel-label"]="Skip"
    ["help-label"]="Help" ["help-text"]="$helpText" ["help-title"]="Help:  $dialogTitle")
  local -A posixDlgParams=(["defaultChoice"]="Bash" ["dialogHeight"]=20 ["dialogWidth"]=80)
  local -A altDlgParams=(["defaultChoice"]="PowerShell" ["dialogHeight"]=20 ["dialogWidth"]=80)
  local urls=()
  local shellType=""
  local sep=" "
  local catSel=""
  local -A dlgTextArray=(["type"]="Choose the type of Command-line shell to install:")
  local dialogText=""

  setELinksURLs "$helpText"

  addDescsToDlgParams "SHELL_TYPES" "DESC_ASSOC_ARRAY" "catDialogParams"

  for catSel in "${SHELL_TYPES[@]}"; do
    case "$catSel" in
      "${SHELL_TYPES[0]}")
        dlgTextArray["$catSel"]="Select the name of the 'POSIX compliant shell' to install:"
        addDescsToDlgParams "${paramAssocArray["$catSel"]}" "AUR_PCKG_DESCS" "posixDlgParams"
      ;;
      *)
        dlgTextArray["$catSel"]="Select the name of the 'Alternative shell' to install:"
        addDescsToDlgParams "${paramAssocArray["$catSel"]}" "AUR_PCKG_DESCS" "altDlgParams"
        catSel=""
      ;;
    esac
  done

  while true; do
    if [ ${dlgTextArray["$catSel"]+_} ]; then
      dialogText="${dlgTextArray["$shellType"]}"
    else
      dialogText="${dlgTextArray["type"]}"
    fi
    exec 3>&1
      case "$catSel" in
        "${SHELL_TYPES[0]}")
          showListOfChoicesDialogBox "$dialogTitle" "$shellType" "$dialogText" POSIX_COMPLIANT_SHELLS "radiolist" posixDlgParams 2>&1 1>&3
        ;;
        "${SHELL_TYPES[1]}")
          showListOfChoicesDialogBox "$dialogTitle" "$shellType" "$dialogText" ALTERNATIVE_SHELLS "radiolist" altDlgParams 2>&1 1>&3
        ;;
        *)
          showListOfChoicesDialogBox "$windowTitle" "$dialogTitle" "$dialogText" SHELL_TYPES "radiolist" catDialogParams 2>&1 1>&3
        ;;
      esac
    exec 3>&-

    IFS=',' read -a fields <<< "$mbSelVal"
    case ${fields[0]} in
      $DIALOG_OK)
        case "$catSel" in
          "${SHELL_TYPES[0]}"|"${SHELL_TYPES[1]}")
            TAB_SEL_VALS["SHELL_NAME"]=$(echo "${fields[1]//\"/}")
            break
          ;;
          *)
            catSel=$(echo "${fields[1]//\"/}")
            shellType="${DESC_ASSOC_ARRAY["$catSel"]}"
            TAB_SEL_VALS["SHELL_TYPE"]="$shellType"
          ;;
        esac
      ;;
      $DIALOG_HELP)
        if [ "${fields[1]}" == "View-URL" ]; then
          elinks "${urls[0]}"
        fi
      ;;
      *)
        case "$catSel" in
          "${SHELL_TYPES[0]}"|"${SHELL_TYPES[1]}")
            unset TAB_SEL_VALS["SHELL_NAME"]
            unset TAB_SEL_VALS["SHELL_TYPE"]
            catSel=""
          ;;
          *)
            TAB_SEL_VALS=()
            break
          ;;
        esac
      ;;
   esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                     selectDefTextEditor
# DESCRIPTION: Select the option using a linux "radiolist" dialog for the default text
#              editor.  Set the value within the global variable "mbSelVal".
#  Required Params:
#      1) paramAssocArray - the name of the associative array with the required
#                           key/value pairs:
#      "dialogTitle"   - String to be displayed at the top of the dialog window.
#      "dialogText"    - Text to display inside the dialog before the list of menu options
#      "dialogTextHdr" - String to be displayed at the top of the text area.
#      "helpText"      - Text to display on the help/more dialog
#      "menuOptsRef"   - name of the array containing the menu options
#      "optDescsRef"   - name of the associative array containing the description
#                        of the menu options
#---------------------------------------------------------------------------------------
selectDefTextEditor() {
  local -n paramAssocArray="$1"

  paramAssocArray["cancel-label"]="Skip"
  showLinuxListDialog "paramAssocArray"
}

#---------------------------------------------------------------------------------------
#      METHOD:                  showInstallFontConfigStep
# DESCRIPTION: Show a linux "dialog" of type 'menu' that will display the packages to be
#              installed for font configuration.
#  Required Params:
#    1)  optSel - the value of the option that was selected for this task
#    2) optDesc - the description/name of this task
#---------------------------------------------------------------------------------------
showInstallFontConfigStep() {
  local -A funcParams=(["menuOptsRef"]="aurPckgs" ["optDescsRef"]="aurPckgDescs")
  local windowTitle=$(printf "%s - %s" "${SEL_CHKLIST_OPT[0]}" "${SEL_CHKLIST_OPT[1]}")
  local dialogTitle=$(printf "%s - %s" "$1" "$2")

  funcParams["dialogText"]=$(getTextForFontconfigLib)
  funcParams["helpText"]=$(getTextForFontConfig)
  funcParams["dialogBackTitle"]="$windowTitle"
  funcParams["dialogTitle"]="$dialogTitle"

  showConfirmationDialog "funcParams"
}

#---------------------------------------------------------------------------------------
#      METHOD:                     selectPackageForSSH
# DESCRIPTION: Select the option using a linux "radiolist" dialog for the secure shell.
#              Set the value within the global variable "mbSelVal".
#  Required Params:
#      1) paramAssocArray - the name of the associative array with the required
#                           key/value pairs:
#      "dialogTitle"   - String to be displayed at the top of the dialog window.
#      "dialogText"    - Text to display inside the dialog before the list of menu options
#      "dialogTextHdr" - String to be displayed at the top of the text area.
#      "helpText"      - Text to display on the help/more dialog
#      "menuOptsRef"   - name of the array containing the menu options
#      "optDescsRef"   - name of the associative array containing the description
#                        of the menu options
#---------------------------------------------------------------------------------------
selectPackageForSSH() {
  local -n paramAssocArray="$1"

  paramAssocArray["cancel-label"]="Skip"
  showLinuxListDialog "paramAssocArray"
}
