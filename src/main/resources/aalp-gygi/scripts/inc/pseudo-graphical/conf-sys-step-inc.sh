#!/bin/bash
#set -e

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# globals
#===============================================================================

#===============================================================================
# functions/methods
#===============================================================================


#---------------------------------------------------------------------------------------
#      METHOD:                       selectGenFstab
# DESCRIPTION: Show a linux "radiolist" dialog to select the type of identifier to use
#              in the fstab file.  Set the value within the global variable "$mbSelVal"
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)     dialogTitle - String to be displayed at the top of the dialog box.
#      3)      dialogText - Text to display inside the dialog before the
#                           list of menu options
#      4)        helpText - Text to display on the help/more dialog
#---------------------------------------------------------------------------------------
selectGenFstab() {
  local -A dialogParms=(["dialogHeight"]=20 ["help-button"]="true" ["helpWidth"]=85
      ["help-text"]="$4" ["help-title"]="Help:  $2" ["helpURL"]="true")
  local urls=()

  setELinksURLs "$4"

  for opt in "${fstabChoices[@]}"; do
    dialogParms["$opt"]="${fstabDescs["$opt"]}"
  done

  while true; do
    exec 3>&1
      showListOfChoicesDialogBox "$1" "$2" "$3" fstabChoices "radiolist" dialogParms 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$mbSelVal"
    case ${fields[0]} in
      ${DIALOG_OK})
        mbSelVal=$(echo "${fields[1]//\"/}")
        mbSelVal="${fstabDescs["$mbSelVal"]}"
        break
      ;;
      ${DIALOG_CANCEL})
        mbSelVal=""
        break
      ;;
      ${DIALOG_HELP})
        if [ "${fields[1]}" == "View-URL" ]; then
          elinks "${urls[0]}"
        fi
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                          getLocale
# DESCRIPTION: Get the locale.  Set the select keyboard layout in the global
#              variable "mbSelVal".
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)     dialogTitle - String to be displayed at the top of the dialog box.
#      3)   defaultLocale - the default locale
#      4)     defCtryCode - the default country code based on the geographical location
#---------------------------------------------------------------------------------------
getLocale() {
  local numDialogs=3
  local dialogNum=0
  local selData=()
  local -A dialogOpts1=()
  local -A dialogOpts2=()
  local -A dialogOpts3=()
  local localeOptions=()
  local selCode="$3"
  local aryIdx=0
  local -A dialogParms=(["dialogHeight"]=30 ["help-button"]="true" ["help-text"]=""
      ["help-title"]="Help: $2" ["helpURL"]=true)
  local urls=()
  local showConfFlag=$(echo 'false' && return 1)
  local dlgName=""

  readarray -t selData <<< "${selCode//_/$'\n'}"
  if [ ${KAAGI_LOCALES["${selData[0]}"]+_} ]; then
    selCode="${selData[0]}"
  else
    selCode="${selData[1]:0:2}"
  fi

  for dialogNum in $(eval echo "{1..${numDialogs}}"); do
    setDataForDialog ${dialogNum} "$3" "$selCode"
  done

  setELinksURLs "${dialogOpts1["help-text"]}"

  mbSelVal="$3"
  dialogNum=3
  while true; do
    exec 3>&1
      showDialogForLocale ${dialogNum} "$1" "$2" "$3" "$selCode" 2>&1 1>&3
    exec 3>&-

    if [ ${dialogNum} -gt 2 ]; then
      IFS=',' read -a selData <<< "$inputRetVal"
      selData[1]="$mbSelVal"
    else
      IFS=',' read -a selData <<< "$mbSelVal"
    fi

    case ${selData[0]} in
      ${DIALOG_OK})
        case ${dialogNum} in
          1)
            selCode="${selData[1]}"
            showConfFlag=$(showConfDialog "$selCode")
            if ${showConfFlag}; then
              mbSelVal="${KAAGI_LOCALES["$selCode"]}"
              mbSelVal="${mbSelVal}"
              selData[1]="$mbSelVal"
              dialogNum=3
            else
              dialogNum=2
            fi
          ;;
          2)
            mbSelVal="${selData[1]}"
            dialogNum=3
          ;;
          3)
            mbSelVal="${selData[1]}"
            break
          ;;
        esac
      ;;
      ${DIALOG_CANCEL})
        if [ ${dialogNum} -gt 1 ]; then
          dialogNum=1
        else
          mbSelVal="$3"
          break
        fi
      ;;
      ${DIALOG_HELP})
        if [ "${selData[1]}" == "View-URL" ]; then
          elinks "${urls[0]}"
        fi
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                      setDataForDialog
# DESCRIPTION: Set the help and the menu options for the dialog to be displayed:
#                 #1)  Languages and/or Countries that have locales associated to them
#                 #2)  Locales associated with the language and/or country chosen
#                 #3)  Confirmation of Locale Selection
#  Required Params:
#      1)     dialogNum - the number for the dialog to display
#      2) defaultLocale - the default locale
#      3)       selCode - language or country code
#---------------------------------------------------------------------------------------
setDataForDialog() {
  local dialogNum=$1
  local helpText=$(getHelpTextForLocalization "$2" ${dialogNum})
  local ctryLang=()
  local langNames=()
  local defFlag=$(echo 'false' && return 1)

  dialogParms["help-text"]="$helpText"
  case ${dialogNum} in
    1)
      for opt in "${!dialogParms[@]}"; do
        dialogOpts1["$opt"]="${dialogParms[$opt]}"
      done
      for key in "${KAAGI_LOCALE_KEYS[@]}"; do
        readarray -t ctryLang <<< "${KAAGI_DIALOG_DESCS["$key"]//|/$'\n'}"
        if [ "${ctryLang[1]}" == "none" ]; then
          ctryLang[1]=" "
        else
          if [ "${ctryLang[0]}" == "none" ]; then
            ctryLang[0]=" "
          fi
          readarray -t langNames <<< "${ctryLang[1]//;/$'\n'}"
          if [ ${#langNames[@]} -gt 2 ]; then
            ctryLang[1]=$(printf "; %s" "${langNames[@]:0:2}")
            ctryLang[1]="${ctryLang[1]:2}"
          fi
        fi

        dialogOpts1["$key"]=$(printf "%s | %s" "${ctryLang[1]}" "${ctryLang[0]}")

        defFlag=$(isDefaultChoice "$key" "$2")
        if ${defFlag}; then
          dialogOpts1["defaultChoice"]="$key"
        fi
      done

      dialogOpts1["cancel-label"]="Exit"
      dialogOpts1["column-separator"]="|"
      dialogOpts1["dialogWidth"]=90
    ;;
    2)
      dialogOpts2["help-text"]="$helpText"
    ;;
    3)
      dialogOpts3=(["dialogHeight"]=20 ["help-button"]=true ["help-text"]="$helpText"
          ["help-title"]="${dialogParms["help-title"]}")
    ;;
  esac
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                       isDefaultChoice
# DESCRIPTION: Check if the language or country code contains the default locale
#      RETURN: 0 - true, 1 - false
#  Required Params:
#      1)     localeKey - the language or country code
#      2) defaultLocale - the default locale
#---------------------------------------------------------------------------------------
function isDefaultChoice() {
  local localeData=()
  local concatStr="${KAAGI_LOCALES["$1"]}"

  readarray -t localeData <<< "${concatStr//;/$'\n'}"

  local pos=$(findPositionForString localeData "$2")
  if [ ${pos} -gt -1 ]; then
    echo 'true' && return 0
  fi

  echo 'false' && return 1
}

#---------------------------------------------------------------------------------------
#      METHOD:                    setOptionsForSelCode
# DESCRIPTION: Set the menu options for the dialog to choose the locale associated with
#              either a language or country code that was selected.
#  Required Params:
#      1) defaultLocale - the default locale
#      2)       selCode - the selected language or country code
#---------------------------------------------------------------------------------------
setOptionsForSelCode() {
  local selCode=$(trimString "$2")
  local concatStr=""
  local localeData=()
  local optNum=0
  local key=""
  local defChoice=""
  local helpText="${dialogOpts2["help-text"]}"
  local idx=0

  dialogOpts2=()
  for opt in "${!dialogParms[@]}"; do
    dialogOpts2["$opt"]="${dialogParms[$opt]}"
  done
  dialogOpts2["help-text"]="$helpText"
  dialogOpts2["dialogHeight"]=20

  if [ ${ISO_CODE_NAMES["$selCode"]+_} ]; then
    concatStr=$(printf "country '%s':" "${ISO_CODE_NAMES["$selCode"]}")
  else
    concatStr=$(printf "language '%s':" "${ISO_LANG_NAMES["$selCode"]}")
    idx=1
  fi
  dialogOpts2["dialogText"]=$(printf "Select the locale that is associated with the %s" "$concatStr")

  concatStr="${KAAGI_LOCALES["$selCode"]}"
  localeOptions=()
  readarray -t localeOptions <<< "${concatStr//;/$'\n'}"
  for lcl in "${localeOptions[@]}"; do
    readarray -t localeData <<< "${lcl//./$'\n'}"
    if [ ${#localeData[@]} -lt 2 ]; then
      readarray -t localeData <<< "${lcl//@/$'\n'}"
    fi
    concatStr="${localeData[0]}"

    readarray -t localeData <<< "${concatStr//_/$'\n'}"
    concatStr="${localeData[$idx]}"
    if [ ${idx} -gt 0 ]; then
      concatStr="${ISO_CODE_NAMES["$concatStr"]}"
    else
      concatStr="${ISO_LANG_NAMES["$concatStr"]}"
    fi

    dialogOpts2["$lcl"]="$concatStr"

    if [ "$lcl" == "$1" ]; then
      defChoice="$lcl"
    fi
  done

  if [ ${#defChoice} -gt 0 ]; then
    dialogOpts2["defaultChoice"]="$defChoice"
  else
    unset dialogOpts2["defaultChoice"]
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                     showDialogForLocale
# DESCRIPTION: Show the following linux dialog:
#                 #1)  Languages and/or Countries that have locales associated with them
#                 #2)  Locales associated with the language and/or country chosen
#                 #3)  Confirmation of Locale Selection
#  Required Params:
#      1)       dialogNum - the number for the dialog to display
#      2) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      3)     dialogTitle - String to be displayed at the top of the dialog box.
#      4)   defaultLocale - the default locale
#      5)         selCode - the selected language or country code
#---------------------------------------------------------------------------------------
showDialogForLocale() {
  local dialogNum=$1
  local dialogText=""
  local textArray=("In Linux, locales define which language and the character sets to be used." " "
  "The Default Locale is:  ["$4"]" "$DASH_LINE_WIDTH_80" " ")

  case ${dialogNum} in
    1)
      textArray+=("Select the language or country code that is associated with a locale:")
      dialogText=$(getTextForDialog "${textArray[@]}")
      showListOfChoicesDialogBox "$2" "$3" "$dialogText" KAAGI_LOCALE_KEYS "radiolist" dialogOpts1
    ;;
    2)
      setOptionsForSelCode "$4" "$5"
      dialogText="${dialogOpts2["dialogText"]}"
      unset dialogOpts2["dialogText"]
      textArray+=("$dialogText")
      dialogText=$(getTextForDialog "${textArray[@]}")
      showListOfChoicesDialogBox "$2" "$3" "$dialogText" localeOptions "radiolist" dialogOpts2
    ;;
    3)
      setTextForConfDlg
      dialogText=$(getTextForDialog "${textArray[@]}")
      showYesNoBox "$2" "$dialogText" "Confirmation of Locale Selection" dialogOpts3
    ;;
  esac
}

#---------------------------------------------------------------------------------------
#      METHOD:                      setTextForConfDlg
# DESCRIPTION: Set the text to be displayed in the confirmation dialog.
#---------------------------------------------------------------------------------------
setTextForConfDlg() {
  local selData=()
  local dialogText=$(printf "Confirm to set the Locale to:  [%s]" "$mbSelVal")
  textArray=("$dialogText")

  readarray -t selData <<< "${mbSelVal//_/$'\n'}"

  dialogText="${selData[0]}"
  dialogText=$(printf "%15sLanguage [%s]:  '%s'" " " "$dialogText" "${ISO_LANG_NAMES["$dialogText"]}")
  textArray+=("$dialogText")

  if [ ${#selData[@]} -gt 1 ]; then
    dialogText="${selData[1]:0:2}"
    dialogText=$(printf "%16sCountry [%s]:  '%s'" " " "$dialogText" "${ISO_CODE_NAMES["$dialogText"]}")
    textArray+=("$dialogText")
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                       showConfDialog
# DESCRIPTION: Check if the selected country code has only one choice for a
#              keyboard layout
#      RETURN: 0 - true, 1 - false
#  Required Params:
#      1) selCode - the selected language or country code
#---------------------------------------------------------------------------------------
function showConfDialog() {
  local localeData=()
  local concatStr="${KAAGI_LOCALES["$1"]}"
  concatStr="${concatStr:1}"

  readarray -t localeData <<< "${concatStr//;/$'\n'}"

  if [ ${#localeData[@]} -lt 2 ]; then
    echo 'true' && return 0
  fi

  echo 'false' && return 1
}

#---------------------------------------------------------------------------------------
#      METHOD:                     showDlgForHostName
# DESCRIPTION: Uses a "dialog" inputbox to get the name of the host that the computer
#              will be configured with.  Sets the value that was entered in the
#              global variable "ibEnteredText"
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)     dialogTitle - String to be displayed at the top of the dialog box.
#      3)      dialogText - Text to display inside the dialog before the
#                           list of menu options
#      4)       initValue - the initial value to be displayed
#      5)        helpText - Text to display on the help/more dialog
#---------------------------------------------------------------------------------------
showDlgForHostName() {
  local -A dialogOpts=(["dialogHeight"]=17  ["cancel-label"]="Exit" ["no-collapse"]=true
    ["help-button"]=true ["help-text"]="$5" ["help-title"]="Help:  $2" ["initValue"]="$4")
  local inpFields=()
  local isValid=$(echo 'false' && return 1)
  local urls=()
  local urlRefChoices=("Arch Wiki" "Restrictions" "RFC 1123")
  local -A urlDialogOpts=(["dialogHeight"]=20 )

  setELinksURLs "$5"

  while true; do
    exec 3>&1
      showDialogInputBox "$1" "$3" "$2" dialogOpts 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a inpFields <<< "$ibEnteredText"
    case ${inpFields[0]} in
      ${DIALOG_OK})
        ibEnteredText=$(trimString "${inpFields[1]}")
        isValid=$(isValidHostName)

        if ! ${isValid}; then
          dispErrMsgDlg "$1"
        else
          break
        fi
      ;;
      ${DIALOG_CANCEL})
        ibEnteredText=""
        break
      ;;
      ${DIALOG_HELP})
        if [ "${inpFields[1]}" == "View-URL" ]; then
          displayURLinElinks "$1" urlRefChoices urlDialogOpts
        fi
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                        dispErrMsgDlg
# DESCRIPTION: Displays the appropriate error message
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#---------------------------------------------------------------------------------------
dispErrMsgDlg() {
  local dialogBackTitle="$1"
  local dialogTitle=$(echo "Error:  Invalid Name for Host")
  local errTxt=$(getTextForError)
  local msgText=$(printf "$INV_HOST_NAME_MSG" "$ibEnteredText")
  local msgTextArray=("$errTxt" " " "$msgText")

  msgText=$(getTextForDialog "${msgTextArray[@]}")

  $DIALOG --clear --no-collapse --backtitle "$1" --title "$dialogTitle" --msgbox "$msgText" 25 80 3>&1 1>&2 2>&3
}

#---------------------------------------------------------------------------------------
#      METHOD:                      showDlgForNetMans
# DESCRIPTION: Show a linux "radiolist" dialog to select the type of network manager to
#              manage network connection settings.  Set the value within the global
#              variable "ibEnteredText"
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)     dialogTitle - String to be displayed at the top of the dialog box.
#      3)      dialogText - Text to display inside the dialog before the
#                           list of menu options
#      4)        helpText - Text to display on the help/more dialog
#---------------------------------------------------------------------------------------
showDlgForNetMans() {
  local -A dialogParms=(["dialogHeight"]=25 ["help-button"]="true" ["helpWidth"]=85
      ["help-text"]="$4" ["help-title"]="Help:  $dialogTitle" ["helpURL"]="true")
  local urls=()
  local optNum=0
  local key=""
  local menuOpts=()
  local selVals=()

  setELinksURLs "$4"

  for opt in "${!AUR_PCKG_DESCS[@]}"; do
    dialogParms["$opt"]="${AUR_PCKG_DESCS["$opt"]}"
  done

  dialogParms["defaultChoice"]="${networkManagers[1]}"

  while true; do
    exec 3>&1
      showListOfChoicesDialogBox "$1" "$2" "$3" networkManagers "radiolist" dialogParms 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a selVals <<< "$mbSelVal"
    case ${selVals[0]} in
      ${DIALOG_OK})
        mbSelVal=$(echo "${selVals[1]//\"/}")
        break
      ;;
      ${DIALOG_CANCEL})
        mbSelVal="${dialogParms["defaultChoice"]}"
        break
      ;;
      ${DIALOG_HELP})
        if [ "${fields[1]}" == "View-URL" ]; then
          elinks "${urls[0]}"
        fi
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                    showDialogForISPsDNS
# DESCRIPTION: Uses a linux "dialog" form box of type "--form" to get the ISP's domain
#              name systems
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)     dialogTitle - String to be displayed at the top of the dialog box.
#      3)      dialogText - Text to display inside the dialog before the form
#      4)        helpText - Text to display on the help/more dialog
#---------------------------------------------------------------------------------------
showDialogForISPsDNS() {
  local -A dlgCmdOpts=(["dialogHeight"]=20 ["help-button"]=true
    ["help-text"]="$4" ["help-title"]="Help:  $2" ["helpURL"]=true
    ["initValue"]=0 ["ok-label"]="Submit" ["cancel-label"]="Skip")
  local fields=()
  local validPswd=$(echo 'false' && return 1)
  local urls=()

  setELinksURLs "$4"

  while true; do
    exec 3>&1
      showFormDialogForDNS "$1" "$2" "$3"
    exec 3>&-

    IFS=$'\n' read -d '' -a fields <<< "${formVals//$FORM_FLD_SEP/$'\n'}"

    case ${fields[0]} in
      ${DIALOG_OK})
        dnsServers=("${fields[@]:1}")
        break
      ;;
      ${DIALOG_CANCEL})
        dnsServers=()
        break
      ;;
      ${DIALOG_HELP})
        exec 3>&1
          showHelpDialog "$1" dlgCmdOpts
        exec 3>&-
        IFS=',' read -d '' -a fields <<< "$mbSelVal"
        local retVal=$(trimString "${fields[1]}")
        if [ "$retVal" == "View-URL" ]; then
          elinks "${urls[0]}"
        fi
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                    showFormDialogForDNS
# DESCRIPTION: Displays a linux "dialog" box of type "--form" for the ISP's DNS Servers
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)     dialogTitle - String to be displayed at the top of the dialog box.
#      3)      dialogText - Text to display in dialog before the menu options
#---------------------------------------------------------------------------------------
showFormDialogForDNS() {
  formDlgCmd=$(getCmdForDialogBoxType "form" "$1" "$3" "$2" dlgCmdOpts)

  formDlgCmd+=' "  Primary :" 1 15 "" 1 35 20 20'
  formDlgCmd+=' "Secondary :" 2 15 "" 2 35 20 20 2> $instInput'

  eval "$formDlgCmd"
  cmdRetVal=$?

  case $cmdRetVal in
    ${DIALOG_OK})
      formVals=$(cat "$instInput")
      formVals=$(echo "${DIALOG_OK}${FORM_FLD_SEP}$formVals")
    ;;
    ${DIALOG_CANCEL})
      formVals=$(echo "${DIALOG_CANCEL}${FORM_FLD_SEP}Cancel")
    ;;
    ${DIALOG_HELP})
      formVals=$(echo "${DIALOG_HELP}${FORM_FLD_SEP}Help")
    ;;
    ${DIALOG_EXTRA})
      formVals=$(echo "${DIALOG_EXTRA}${FORM_FLD_SEP}Extra")
    ;;
    ${DIALOG_ESC})
      formVals=$(echo "${DIALOG_CANCEL}${FORM_FLD_SEP}ESC")
    ;;
  esac
}

#---------------------------------------------------------------------------------------
#      METHOD:                    showDialogForDnsmasq
# DESCRIPTION: Show a linux "radiolist" dialog to select the free DNS provider.
#              Set the value within the global variable "$mbSelVal".
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)     dialogTitle - String to be displayed at the top of the dialog box.
#      3)      dialogText - Text to display inside the dialog before the
#                           list of menu options
#      4)        helpText - Text to display on the help/more dialog
#---------------------------------------------------------------------------------------
showDialogForDnsmasq() {
  local -A dialogParms=(["dialogHeight"]=25 ["help-button"]="true" ["helpWidth"]=85
    ["help-text"]="$4" ["help-title"]="Help:  $2" ["helpURL"]="true"
    ["column-separator"]="$FORM_FLD_SEP")
  local urls=()
  local urlRefChoices=("Arch Wiki" "Restrictions" "RFC 1123")
  local -A urlDialogOpts=(["dialogHeight"]=20 )

  setELinksURLs "$4"

  for opt in "${FREE_DNS_PROVIDERS[@]}"; do
    dialogParms["$opt"]="${FREE_DNS_IPS["$opt"]}"
  done

  while true; do
    exec 3>&1
      showListOfChoicesDialogBox "$1" "$2" "$3" FREE_DNS_PROVIDERS "radiolist" dialogParms 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$mbSelVal"
    case ${fields[0]} in
      ${DIALOG_OK})
        mbSelVal=$(echo "${fields[1]//\"/}")
        break
      ;;
      ${DIALOG_CANCEL})
        mbSelVal="${FREE_DNS_PROVIDERS[0]}"
        break
      ;;
      ${DIALOG_HELP})
        if [ "${fields[1]}" == "View-URL" ]; then
          displayURLinElinks "$1" urlRefChoices urlDialogOpts
        fi
      ;;
    esac
  done
}
