#!/bin/bash
#set -e

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# globals
#===============================================================================

#===============================================================================
# functions/methods
#===============================================================================

#---------------------------------------------------------------------------------------
#      METHOD:                      showTimeZoneConf
# DESCRIPTION: Show a linux "dialog" box of type "yes no" to get confirmation to
#              set the time zone to the value being displayed.
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)     dialogTitle - String to be displayed at the top of the dialog box.
#      3)        timeZone - The Time Zone that was either determined or manually entered
#      4)        helpText - Text to display on the help/more dialog
#---------------------------------------------------------------------------------------
showTimeZoneConf() {
  local dialogBackTitle="$1"
  local dialogTitle="$2"
  local timeZone="$3"
  local helpText="$4"
  local -A cmdOpts=(["yes-label"]="Confirm" ["no-label"]="Cancel" ["dialogHeight"]=20 ["helpWidth"]=85
    ["help-button"]=true ["help-text"]="$helpText" ["help-title"]="Help:  $dialogTitle")
  local urls=()
  local dialogText=""

  setELinksURLs "$helpText"

  yesNoFlag=$(echo 'false' && return 1)
  while true; do
    dialogText=$(getTextForTimeZoneDialog "$timeZone")
    exec 3>&1
      showYesNoBox "$DIALOG_BACK_TITLE" "$dialogText" "$dialogTitle" cmdOpts 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$inputRetVal"

    case ${fields[0]} in
      $DIALOG_OK)
        yesNoFlag=$(echo 'true' && return 0)
        break
      ;;
      $DIALOG_CANCEL)
        break
      ;;
      $DIALOG_HELP)
        if [ "${fields[1]}" == "View-URL" ]; then
          elinks "${urls[0]}"
        fi
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getTextForTimeZoneDialog
# DESCRIPTION: Get the text to display in the text area of a linux "dialog".
#      RETURN: concatenated string
#  Required Params:
#      1) timeZone - The Time Zone that was either determined or manually entered
#---------------------------------------------------------------------------------------
function getTextForTimeZoneDialog() {
  local timeZone=""
  local dialogText=$(timedatectl status)
  local tzSZ=()
  local lclTextArray=()
  local concatStr=""

  local extraDashes=""
  if [ "$DIALOG" == "yad" ]; then
    extraDashes="--------------------"
  fi
  lclTextArray+=("Current settings of the Hardware & System Clocks:"
  "-------------------------------------------------$extraDashes" " " "$dialogText")

  if [ "$#" -gt 0 ]; then
    lclTextArray+=("$DIALOG_BORDER" " " "Confirm to set the Time Zone to?")
    timeZone="$1"
    IFS='/' read -a tzSZ <<< "$timeZone"

    if [ ${#tzSZ[@]} -gt 1 ]; then
      concatStr=$(printf "/%s" "${tzSZ[@]:1}")
      lclTextArray+=("         Zone:  '${tzSZ[0]}'" "     Sub-Zone:  '${concatStr:1}'")
    else
      lclTextArray+=("         Zone:  '$timeZone'")
    fi
  fi

  dialogText=$(getTextForDialog "${lclTextArray[@]}")

  echo "$dialogText"
}

#---------------------------------------------------------------------------------------
#      METHOD:                       showZoneDialog
# DESCRIPTION: Show a linux "radiolist" dialog to select the zone section of a time zone.
#              Set the value within the global variable "$mbSelVal"
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)     dialogTitle - String to be displayed at the top of the dialog box.
#      3)        helpText - Text to display on the help/more dialog
#---------------------------------------------------------------------------------------
showZoneDialog() {
  local dialogBackTitle="$1"
  local dialogTitle="$2"
  local helpText="$3"
  local dialogText=""
  local -A dialogParms=(["cancel-label"]="Exit" ["no-collapse"]="true" ["dialogHeight"]=25 ["help-button"]="true"
      ["helpWidth"]=85 ["help-text"]="$helpText" ["help-title"]="Help:  $dialogTitle" ["helpURL"]="true")
  local urls=()
  local optNum=0
  local lclTextArray=()
  local zoneOpts=()
  local key=""

  setELinksURLs "$helpText"

  for zone in "${ZONE_ARRAY[@]}"; do
    optNum=$(expr ${optNum} + 1)
    key=$(printf "#%d" ${optNum})
    zoneOpts+=("$key")
    dialogParms["$key"]="$zone"
  done

  while true; do
    dialogText=$(timedatectl status)
    lclTextArray=("Current settings of the Hardware & System Clocks:"
      "-------------------------------------------------" " "
      "$dialogText" "$DIALOG_BORDER" " " "Select the Zone:")
    dialogText=$(getTextForDialog "${lclTextArray[@]}")
    exec 3>&1
      showListOfChoicesDialogBox "$dialogBackTitle" "$dialogTitle" "$dialogText" zoneOpts "radiolist" dialogParms 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$mbSelVal"
    case ${fields[0]} in
      $DIALOG_OK)
        mbSelVal=$(echo "${fields[1]//\"/}")
        mbSelVal="${dialogParms["$mbSelVal"]}"
        break
      ;;
      $DIALOG_CANCEL)
        mbSelVal=""
        break
      ;;
      $DIALOG_HELP)
        if [ "${fields[1]}" == "View-URL" ]; then
          elinks "${urls[0]}"
        fi
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                      showSubZoneDialog
# DESCRIPTION: Show a linux "radiolist" dialog to select the sub-zone of a time zone.
#              Set the value within the global variable "$mbSelVal".
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)     dialogTitle - String to be displayed at the top of the dialog box.
#      3)            zone - The Zone of a time zone that was selected
#      4)        helpText - Text to display on the help/more dialog
#---------------------------------------------------------------------------------------
showSubZoneDialog() {
  local dialogBackTitle="$1"
  local dialogTitle="$2"
  local zone="$3"
  local helpText="$4"
  local -A dialogParms=(["no-collapse"]="true" ["dialogHeight"]=25 ["help-button"]="true" ["helpWidth"]=85
      ["help-text"]="$helpText" ["help-title"]="Help:  $dialogTitle" ["helpURL"]="true")
  local urls=()
  local optNum=0
  local dialogText="Select the Sub-Zone for Zone '$zone':"
  local subZoneOpts=()
  local key=""

  setELinksURLs "$helpText"

  for locSubZone in "${subZones[@]}"; do
    optNum=$(expr ${optNum} + 1)
    key=$(printf "#%d" ${optNum})
    subZoneOpts+=("$key")
    dialogParms["$key"]="$locSubZone"
  done

  while true; do
    exec 3>&1
      showListOfChoicesDialogBox "$dialogBackTitle" "$dialogTitle" "$dialogText" subZoneOpts "radiolist" dialogParms 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$mbSelVal"
    case ${fields[0]} in
      $DIALOG_OK)
        mbSelVal=$(echo "${fields[1]//\"/}")
        mbSelVal="${dialogParms["$mbSelVal"]}"
        break
      ;;
      $DIALOG_CANCEL)
        mbSelVal=""
        break
      ;;
      $DIALOG_HELP)
        if [ "${fields[1]}" == "View-URL" ]; then
          elinks "${urls[0]}"
        fi
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                       showTimeStandardDialog
# DESCRIPTION: Show a linux "radiolist" dialog to select the time standard for the
#              hardware clock to be set to.  Set the value within the global
#              variable "$inputRetVal"
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)     dialogTitle - String to be displayed at the top of the dialog box.
#      3)        helpText - Text to display on the help/more dialog
#      4)      dialogText - Text to display in the text area of the dialog
#---------------------------------------------------------------------------------------
showTimeStandardDialog() {
  local dialogBackTitle="$1"
  local dialogTitle="$2"
  local helpText="$3"
  local dialogText="$4"
  local -A dialogOpts=(["yes-label"]="Local" ["no-label"]="Exit" ["extra-label"]="UTC"
    ["no-collapse"]="true" ["dialogHeight"]=30 ["help-button"]="true" ["help-text"]="$helpText"
    ["help-title"]="Help: $dialogTitle" ["helpURL"]="true")
  local urls=()

  setELinksURLs "$helpText"

  while true; do
    exec 3>&1
      showYesNoBox "$dialogBackTitle" "$dialogText" "$dialogTitle" dialogOpts 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$inputRetVal"
    case ${fields[0]} in
      $DIALOG_OK)
        inputRetVal="localtime"
        break
      ;;
      $DIALOG_CANCEL)
        inputRetVal=""
        break
      ;;
      $DIALOG_EXTRA)
        inputRetVal="UTC"
        break
      ;;
      $DIALOG_HELP)
        if [ "${fields[1]}" == "View-URL" ]; then
          elinks "${urls[0]}"
        fi
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                    showLocalTimeWarning
# DESCRIPTION: Show a linux "dialog" box of type "yes no" to get confirmation to
#              use the local timescale for the time standard.
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)     dialogTitle - String to be displayed at the top of the dialog box.
#      3)      dialogText - Text to display in the text area of the dialog
#---------------------------------------------------------------------------------------
showLocalTimeWarning() {
  local dialogBackTitle="$1"
  local dialogTitle="$2"
  local dialogText="$3"
  local -A cmdOpts=(["yes-label"]="Use Local" ["no-label"]="Use UTC" ["dialogHeight"]=25)

  exec 3>&1
    showYesNoBox "$DIALOG_BACK_TITLE" "$dialogText" "$dialogTitle" cmdOpts 2>&1 1>&3
  exec 3>&-
  IFS=',' read -a fields <<< "$inputRetVal"

  case ${fields[0]} in
    $DIALOG_OK)
      yesNoFlag=$(echo 'true' && return 0)
    ;;
    $DIALOG_CANCEL)
      yesNoFlag=$(echo 'false' && return 1)
    ;;
  esac
}
