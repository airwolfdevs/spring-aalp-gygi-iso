#!/bin/bash
#set -e

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# functions/methods
#===============================================================================

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getDialogOptionforPPT
# DESCRIPTION: Get the option for the type of process to use to partition the disk
#      RETURN: type of partition process in the global "mbSelVal" variable
#  Required Params:
#      1) dialogTitle - String to be displayed at the top of the dialog box.
#      2)    helpText - Text to display on the help/more dialog
#      3)  pptChoices - array of options that can be selected in the dialog
#---------------------------------------------------------------------------------------
getDialogOptionforPPT() {
  local dialogTitle="$1"
  local helpText="$2"
  local -n pptChoices=$3
  while true; do
    exec 3>&1
      selectPartitionProcessType "$dialogTitle" "$helpText" pptChoices 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$mbSelVal"
    case ${fields[0]} in
      $DIALOG_OK)
        local idx=$(expr ${fields[1]} - 1)
        mbSelVal=$(echo "${PART_PROC_TYPE_CHOICES[idx]}")
        break
      ;;
      $DIALOG_CANCEL)
        mbSelVal=""
        break
      ;;
      $DIALOG_HELP)
        if [ "${fields[1]}" == "View-URL" ]; then
          elinks "https://wiki.archlinux.org/index.php/installation_guide#Partition_the_disks"
        fi
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:              selectPartitionProcessType
# DESCRIPTION: Select the option for the type of process to use to partition the disk
#      RETURN: selected choice in the global "mbSelVal" variable
#  Required Params:
#      1) dialogTitle - String to be displayed at the top of the dialog box.
#      2)    helpText - Text to display on the help/more dialog
#      3) menuOptions - array of options that can be selected in the dialog
#---------------------------------------------------------------------------------------
selectPartitionProcessType() {
  local dialogTitle="$1"
  local helpText="$2"
  local -n menuOptions=$3
  local textArray=(
  "Partitioning a hard drive allows one to logically divide the available"
  "space into sections that can be accessed independently of one"
  "another.  (More...)"
  "$DASH_LINE_WIDTH_80" " "
  "Choose which process you want to use:"
  )
  local dialogText=$(getTextForDialog "${textArray[@]}")

  local -A dialogParms=(["help-label"]="More..." ["help-text"]="$helpText"
      ["help-title"]="Help:  Partition Process Choices" ["helpURL"]=true)
  local fields=()

  showListOfChoicesDialogBox "$DIALOG_BACK_TITLE" "$dialogTitle" "$dialogText" menuOptions "radiolist" dialogParms
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                getKeepOrZapOptForCurLayout
# DESCRIPTION: Get the option to either keep or destroy (i.e. zap) the current
#              partition layout/scheme.
#      RETURN: option chosen in the global variable "inputRetVal"
#  Required Params:
#      1) dialogPartTable - string containing the rows of data for the partition table
#---------------------------------------------------------------------------------------
getKeepOrZapOptForCurLayout() {
  local dialogPartTable="$1"
  local dialogText=$(getDialogTextForKeepOrZap "$dialogPartTable")
  local helpText=$(getHelpTextForCurLayout "$dialogPartTable")
  local dialogTitle="Keep OR Zap Partition Layout/Scheme"
  local -A cmdOpts=(["yes-label"]="Keep" ["no-label"]="Zap" ["dialogHeight"]=27 ["helpWidth"]=98
    ["help-button"]=true ["help-text"]="$helpText" ["help-title"]="Help:  $dialogTitle")

  while true; do
    exec 3>&1
      showYesNoBox "$DIALOG_BACK_TITLE" "$dialogText" "$dialogTitle" cmdOpts 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$inputRetVal"

    case ${fields[0]} in
      $DIALOG_OK)
        inputRetVal="KEEP"
        break
      ;;
      $DIALOG_CANCEL)
        if [ "${fields[1]}" == "ESC" ]; then
          inputRetVal="EXIT"
        else
          inputRetVal="ZAP"
        fi
        break
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getDialogTextForKeepOrZap
# DESCRIPTION: Get the text to display in the Keep Or Zap linux "dialog"
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getDialogTextForKeepOrZap() {
  local sumTbl=$(getSummarizedTableOfStats)
  local textArray=()

  textArray+=("$sumTbl" " " "Select the < Help > button to view the configured partition table."
    "$DASH_LINE_WIDTH_80" " "
    "Do you want to Keep OR Zap the current Partition Layout/Scheme?"
  )

  local dialogText=$(getTextForDialog "${textArray[@]}")

  echo "$dialogText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getSummarizedTableOfStats
# DESCRIPTION: Get the header and rows of the summarized table of statistics for
#              for the current partition layout/scheme.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getSummarizedTableOfStats() {
  local hdr=$(printf "Partition Table: %s%3sDisk '%s'" "${PART_TBL_TYPES[${varMap[PART-TBL-TYPE]}]}" " " "$BLOCK_DEVICE")
  local lvm=""
  local textArray=("$hdr")
  local rows=()

  if [ ${varMap["VG-NAME"]+_} ]; then
    setPhysicalVolume
    lvm=$(printf "Physical Volume:  [%s]%3sVolume Group:  [%s]" "$PHYSICAL_VOLUME" " " "${varMap[VG-NAME]}")
    textArray+=("$lvm")
  fi

  setSummaryOfCurLayout rows
  local sumTbl=$(printf "\n%s" "${rows[@]}")
  sumTbl=${sumTbl:1}
  sumTbl=$(printTable ';' "$sumTbl" | sed 's/^\-\-\-/   /')

  textArray+=("$sumTbl")
  local dialogText=$(getTextForDialog "${textArray[@]}")
  echo "$dialogText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getChoiceWhenNoParts
# DESCRIPTION: Get choice when no partitions were found.
#      RETURN: In the global variable "inputRetVal":
#              YES) run the linux partition tool again
#               NO) exit the manual partition process
#  Required Params:
#      1)   dialogTitle - String to be displayed at the top of the dialog box.
#      2) partitionTool - name of the linux partition tool that was previously used
#---------------------------------------------------------------------------------------
getChoiceWhenNoParts() {
  local dialogTitle="$1"
  local partitionTool="$2"
  local warnText=$(getTextForWarning)
  local textArray=(" " "$warnText"
  "The Partition Table does not contain any partitions."
  "$DASH_LINE_WIDTH_80" " "
  "Do you want to <Run > the linux partition tool '$partitionTool' again OR"
  "<Cancel> the Manual Partition Process?"
  )
  local dialogText=$(getTextForDialog "${textArray[@]}")
  local -A cmdOpts=(["yes-label"]="Run" ["no-label"]="Cancel")

  exec 3>&1
    showYesNoBox "$DIALOG_BACK_TITLE" "$dialogText" "$dialogTitle" cmdOpts 2>&1 1>&3
  exec 3>&-
  IFS=',' read -a fields <<< "$inputRetVal"
  case ${fields[0]} in
    $DIALOG_OK)
      inputRetVal="YES"
    ;;
    $DIALOG_CANCEL)
      inputRetVal="NO"
    ;;
  esac
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getChoiceWhenUnallocSpace
# DESCRIPTION: Get choice when unallocated space is found
#      RETURN: In the global variable "inputRetVal":
#              YES) run the linux partition tool again
#               NO) continue with the manual partition process
#  Required Params:
#      1)      dialogTitle - String to be displayed at the top of the dialog box.
#      2)    partitionTool - name of the linux partition tool that was previously used
#      3) unallocSpacePerc - percentage of unallocated space
#---------------------------------------------------------------------------------------
getChoiceWhenUnallocSpace() {
  local dialogTitle="$1"
  local partitionTool="$2"
  local unallocSpacePerc="$3"
  local warnText=$(getTextForWarning)
  local textArray=(" " "$warnText"
  "There is '$unallocSpacePerc' of Unallocated Space."
  "$DASH_LINE_WIDTH_80" " "
  "Do you want to <Run > the linux partition tool '$partitionTool' again OR"
  "<Continue> with the Manual Partition Process?"
  )
  local dialogText=$(getTextForDialog "${textArray[@]}")
  local -A cmdOpts=(["yes-label"]="Run" ["no-label"]="Continue")

  exec 3>&1
    showYesNoBox "$DIALOG_BACK_TITLE" "$dialogText" "$dialogTitle" cmdOpts 2>&1 1>&3
  exec 3>&-
  IFS=',' read -a fields <<< "$inputRetVal"
  case ${fields[0]} in
    $DIALOG_OK)
      inputRetVal="YES"
    ;;
    $DIALOG_CANCEL)
      inputRetVal="NO"
    ;;
  esac
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getPartitionLayoutType
# DESCRIPTION: Get the type of partition layout/scheme
#      RETURN: selected value in the global variable "mbSelVal"
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)     dialogTitle - String to be displayed at the top of the dialog box.
#      3)      pltChoices - array of strings for the choices of
#                           partition layout/scheme types
#  Optional Params:
#      4) flag to indicate if this is just for "LVM"
#---------------------------------------------------------------------------------------
getPartitionLayoutType() {
  local dialogBackTitle="$1"
  local dialogTitle="$2"
  local -n pltChoices=$3
  local -A pltOpts=()
  local dialogText=""
  local helpText=""

  if [ "$#" -gt 3 ]; then
    dialogText=$(getTextForSelPartLayout)
    helpText=$(getHelpTextForSelPartLayout)
    pltOpts["nocancel"]=true
  else
    dialogText=$(getTextForSelPartLayout "all")
    helpText=$(getHelpTextForSelPartLayout "all")
  fi

  if [ ${varMap["PART-LAYOUT"]+_} ]; then
    pltOpts["dialogHeight"]=35
    pltOpts["defaultChoice"]=$(findPositionForString pltChoices "${varMap[PART-LAYOUT]}")
    pltOpts["extra-label"]="Keep"
    pltOpts["helpHeight"]=35
    pltOpts["helpWidth"]=98
  fi

  while true; do
    exec 3>&1
      selectPartitionLayoutType "$dialogBackTitle" "$dialogText" "$dialogTitle" "$helpText" pltChoices pltOpts 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$mbSelVal"
    case ${fields[0]} in
      $DIALOG_OK)
        local aryIdx=$(expr ${fields[1]} - 1)
        mbSelVal=$(echo "${pltChoices[$aryIdx]}")
        break
      ;;
      $DIALOG_CANCEL)
        mbSelVal=""
        if [ ! ${pltOpts["nocancel"]+_} ]; then
          break
        fi
      ;;
      $DIALOG_EXTRA)
        mbSelVal="<Keep>"
        break
      ;;
      $DIALOG_HELP)
        if [ "${fields[1]}" == "View-URL" ]; then
          viewURLinElinks "all"
        fi
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#    FUNCTION:              getTextForSelPartLayout
# DESCRIPTION: Text to display in the radio list dialog box when selecting the
#              partition layout/scheme.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
getTextForSelPartLayout() {
  local textArray=()
  local helpBtnText="Select the <More...> button below for an explanation of the options"

  if [ ${varMap["PART-LAYOUT"]+_} ]; then
    local sumTbl=$(getSummarizedTableOfStats)
    textArray=( "Current Partition Layout/Scheme:  [${varMap[PART-LAYOUT]}]"
    " " "$sumTbl" " "
    "$helpBtnText and"
    "to view the configured partition table.")
    helpBtnText=$(getTextForDialog "${textArray[@]}")
  else
    helpBtnText="${helpBtnText}."
  fi

  if [ "$#" -gt 0 ]; then
    textArray=(" " "$helpBtnText" "$DASH_LINE_WIDTH_80" " "
    "Choose the partition layout/scheme:")
  else
    textArray=(" " "$helpBtnText" "$DASH_LINE_WIDTH_80" " "
    "Choose the partition layout/scheme for LVM:")
  fi

  local dialogText=$(getTextForDialog "${textArray[@]}")
  echo "$dialogText"
}

#---------------------------------------------------------------------------------------
#      METHOD:                 selectPartitionLayoutType
# DESCRIPTION: Select the partition layout/scheme within a linux "dialog" box
#              of type radio list
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)      dialogText - Text to display inside the dialog before the
#                           list of menu options
#      3)     dialogTitle - String to be displayed at the top of the dialog box.
#      4)        helpText - text to display inside the help/more dialog
#      5)   layoutChoices - array of strings for the choices of
#                           partition layout/scheme types
#      6)   addDialogOpts - options for the linux "dialog" command to either override or add
#---------------------------------------------------------------------------------------
selectPartitionLayoutType() {
  local dialogBackTitle="$1"
  local dialogText="$2"
  local dialogTitle="$3"
  local helpText="$4"
  local -n layoutChoices=$5
  local -n addDialogOpts=$6
  local key=""
  local -A dialogParms=( ["no-collapse"]=true ["dialogHeight"]=25 ["help-label"]="More..."
    ["help-text"]="$helpText" ["help-title"]="Help: $dialogTitle" ["helpURL"]=true)

  for key in "${!addDialogOpts[@]}"; do
    dialogParms[$key]=$(echo "${addDialogOpts[$key]}")
  done

  showListOfChoicesDialogBox "$dialogBackTitle" "$dialogTitle" "$dialogText" layoutChoices "radiolist" dialogParms
}

#---------------------------------------------------------------------------------------
#    FUNCTION:               getConfirmationToEncrypt
# DESCRIPTION: Get confirmation of whether or not to continue with LUKS encryption
#      RETURN: value selected in the global variable "inputRetVal"
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)     dialogTitle - String to be displayed at the top of the dialog box.
#      3)        helpText - Text to display on the help/more dialog
#---------------------------------------------------------------------------------------
getConfirmationToEncrypt() {
  local dialogBackTitle="$1"
  local dialogTitle="$2"
  local helpText="$3"
  local dialogText=$(getTextForConfirmation)
  local -A cmdOpts=(["yes-label"]="Confirm" ["no-label"]="Cancel" ["dialogHeight"]=33 ["dialogWidth"]=98
  ["help-button"]=true ["help-text"]="$helpText" ["help-title"]="Help:  $dialogTitle")

  while true; do
    exec 3>&1
      showYesNoBox "$dialogBackTitle" "$dialogText" "$dialogTitle" cmdOpts 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$inputRetVal"
    case ${fields[0]} in
      $DIALOG_OK)
        inputRetVal="YES"
        break
      ;;
      $DIALOG_CANCEL)
        inputRetVal="NO"
        break
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                getTextForConfirmation
# DESCRIPTION: Get the text to display in a linux "dialog" command for the
#              confirmation of the LUKS encrypted container
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getTextForConfirmation() {
  local dialogPartTable=$(getPartitionTableText)
  local pvDevice=$(getDeviceNameForPV)
  local warnText=$(getTextForWarning)
  local textArray=(" "
  "'LUKS' (Linux Unified Key Setup) is the preferred way to setup"
  "disk encryption with 'dm-crypt'."
  "$DASH_LINE_WIDTH_80"
  "'Device-Mapper Crypt' (dm-crypt) - The installer will use the straight-"
  "forward approach by:"
  "    1) first encrypting the device '$pvDevice' with 'LUKS'"
  "    2) creating the logical volumes on top of the encrypted device"
  " " "$dialogPartTable" " "
  "Name of the LUKS encrypted container:  $LUKS_CONTAINER_NAME"
  "Password to encrypt & open container:  ${varMap[LVM-on-LUKS]}" " " "$warnText"
  "If you are NOT going to be able to remember the Password,"
  "then write it down and store it in a safe place!"
  )
  local dialogText=$(getTextForDialog "${textArray[@]}")

  echo "$dialogText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getSelPswdOptForLUKS
# DESCRIPTION: Get the option to create a password for the LUKS encrypted container
#      RETURN: option of how to create a password in the global variable "mbSelVal"
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)     dialogTitle - String to be displayed at the top of the dialog box.
#      3)          urlRef - the URL that can be referenced for further documentation
#                           from the help dialog
#      4)     dlgPswdOpts - array of options to choose from
#      5)     dlgOptDescs - associative array of key/value pairs for the
#                           description of each option
#---------------------------------------------------------------------------------------
getSelPswdOptForLUKS() {
  local dialogBackTitle="$1"
  local dialogTitle="$2"
  local urlRef="$3"
  local -n dlgPswdOpts=$4
  local -n dlgOptDescs=$5
  local dialogText=$(getDialogTextForLUKS)
  local helpText=$(getHelpTextForPasswordOption "$urlRef")

  while true; do
    exec 3>&1
      selectPasswordOptionForLUKS "$dialogBackTitle" "$dialogTitle" "$dialogText" \
        "$helpText" dlgPswdOpts dlgOptDescs 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$mbSelVal"
    case ${fields[0]} in
      $DIALOG_OK)
        mbSelVal=$(echo "${fields[1]}")
        break
      ;;
      $DIALOG_CANCEL)
        mbSelVal=""
        break
      ;;
      $DIALOG_HELP)
        if [ "${fields[1]}" == "View-URL" ]; then
          elinks "$urlRef"
        fi
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getDialogTextForLUKS
# DESCRIPTION: Get the text to display in a linux "dialog" command when selecting
#              the option of creating a password for the LUKS encrypted container
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getDialogTextForLUKS() {
  local textArray=(" "
  "'LUKS' (Linux Unified Key Setup) is the preferred way to setup"
  "disk encryption with 'dm-crypt'."
  "$DASH_LINE_WIDTH_80" " "
  "There aren't any requirements in choosing a password.  The recommendation"
  "is 8-20 characters in length, and seemingly completely random."
  "The Installer will allow you to create one or use one of the automated"
  "tools based on the recommendations"
  "(More...)"
  "$DASH_LINE_WIDTH_80" " "
  "Choose the option to create the password to"
  "encrypt & open the LUKS encrypted container:"
  )
  local dialogText=$(getTextForDialog "${textArray[@]}")
  echo "$dialogText"
}

#---------------------------------------------------------------------------------------
#      METHOD:              selectPasswordOptionForLUKS
# DESCRIPTION: Select the option to create a password for the LUKS encrypted container
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)     dialogTitle - String to be displayed at the top of the dialog box.
#      3)      dialogText - Text to display inside the dialog before the
#                           list of menu options
#      4)        helpText - Text to display on the help/more dialog
#      5)     dlgPswdOpts - array of options to choose from
#      6)     dlgOptDescs - associative array of key/value pairs for the
#                           description of each option
#---------------------------------------------------------------------------------------
selectPasswordOptionForLUKS() {
  local dialogBackTitle="$1"
  local dialogTitle="$2"
  local dialogText="$3"
  local helpText="$4"
  local -n menuOptions=$5
  local -n optDescs=$6
  local -A dialogParms=(["dialogHeight"]=25 ["help-label"]="More..." ["help-text"]="$helpText"
    ["help-title"]="Help:  Password Create Options" ["helpURL"]=true)

  for key in "${!optDescs[@]}"; do
    dialogParms[$key]=$(echo "- ${optDescs[$key]}")
  done

  showListOfChoicesDialogBox "$dialogBackTitle" "$dialogTitle" "$dialogText" menuOptions "radiolist" dialogParms
}

#---------------------------------------------------------------------------------------
#      METHOD:                 showProgressBarForDataFile
# DESCRIPTION: Display a linux "dialog" box of type gauge (i.e. progress bar) while
#              executing the methods to create the data file for the partition table.
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)   pbDialogTitle - String to be displayed at the top of the dialog box.
#---------------------------------------------------------------------------------------
showProgressBarForDataFile() {
  (
    local methodNum=1
    local pbPerc=0
    local methodName=""
    local pbTitle=""
    local totalMethods=${#pbKeys[@]}

    sleep 0.5
    for pbKey in "${pbKeys[@]}"; do
      pbTitle="${pbTitles[$pbKey]}"
      echo $pbPerc
      echo "XXX"
      echo "$pbTitle"
      echo "XXX"

      case "$pbKey" in
        "done")
          sleep 0.5
        ;;
        *)
          methodName="${methodNames[$pbKey]}"
          clog_info "Executing method [$methodName]" >> "$INSTALLER_LOG"
          ${methodName}
          clog_info "Method [$methodName] is DONE" >> "$INSTALLER_LOG"
          methodNum=$(expr $methodNum + 1)
          pbPerc=$(printf "%.0f" $(echo "scale=3; $methodNum / $totalMethods * 100" | bc))
        ;;
      esac

      sleep 0.5
    done
  ) | $DIALOG --clear --backtitle "$1" --title "$2" --gauge "Please Wait..." 20 80 0
}
