#!/bin/bash

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# globals
#===============================================================================
declare -A TAB_SEL_VALS=()

#===============================================================================
# functions/methods
#===============================================================================

#---------------------------------------------------------------------------------------
#      METHOD:                       dispStepInitPB
# DESCRIPTION: Display a linux "dialog" box of type gauge (i.e. progress bar) while
#              executing the methods to initialize the data for a step/task.
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)         pbTitle - String to be displayed at the top of the dialog box.
#---------------------------------------------------------------------------------------
dispStepInitPB() {
  (
    local methodNum=1
    local pbPerc=0
    local methodName=""
    local pbTitle=""
    local totalMethods=${#pbKeys[@]}
    local continueFlag=$(echo 'true' && return 0)

    sleep 0.5
    for pbKey in "${pbKeys[@]}"; do
      pbTitle="${pbTitles[$pbKey]}"
      echo $pbPerc
      echo "XXX"
      echo "$pbTitle"
      echo "XXX"
      methodName="${methodNames[$pbKey]}"

      callPostInstMethod
      if ! ${continueFlag}; then
        break
      fi

      sleep 0.5
    done
  ) | $DIALOG --clear --backtitle "$1" --title "$2" --gauge "Please Wait..." 20 80 0
}

#---------------------------------------------------------------------------------------
#      METHOD:                   showDesktopConfWarning
# DESCRIPTION: Show a linux "dialog" box of type "yes no" to get confirmation of whether
#              or not to continue with the user's choice.  Some examples are:
#                 > use the "AMD Catalyst" type Driver & OpenGL packages
#                 > skipping installation of Xorg
#  Required Params:
#      1) paramAssocArray - the name of the associative array with the required
#                           key/value pairs:
#      "dialogBackTitle" - The string to be displayed on the backdrop, at the top
#                          of the screen.
#      "dialogTitle"     - String to be displayed at the top of the dialog box.
#      "dialogText"      - Text to display in the text area of the dialog
#---------------------------------------------------------------------------------------
showDesktopConfWarning() {
  local -n paramAssocArray="$1"
  local dialogBackTitle="${paramAssocArray["dialogBackTitle"]}"
  local dialogTitle="${paramAssocArray["dialogTitle"]}"
  local warnText=$(getTextForWarning)
  local dialogText=$(echo "$warnText ${paramAssocArray["dialogText"]}")
  local -A cmdOpts=(["yes-label"]="Continue" ["no-label"]="Cancel" ["dialogHeight"]=25)

  exec 3>&1
    showYesNoBox "$dialogBackTitle" "$dialogText" "$dialogTitle" cmdOpts 2>&1 1>&3
  exec 3>&-
  IFS=',' read -a fields <<< "$inputRetVal"

  case ${fields[0]} in
    $DIALOG_OK)
      yesNoFlag=$(echo 'true' && return 0)
    ;;
    $DIALOG_CANCEL)
      yesNoFlag=$(echo 'false' && return 1)
    ;;
  esac
}

#---------------------------------------------------------------------------------------
#      METHOD:                     showRadioListDialog
# DESCRIPTION: This is a wrapper for calling a linux "dialog" of type "radiolist" to
#              display the options available for a specific task.  Sets the value
#              selected within the global variable "mbSelVal".
#  Required Params:
#      1) paramAssocArray - the name of the associative array with the required
#                           key/value pairs:
#      "cancel-label"    - Skip or Cancel.
#      "dialogBackTitle" - The string to be displayed on the backdrop, at the top
#                          of the screen.
#      "dialogTitle"     - String to be displayed at the top of the dialog box.
#      "dialogText"      - Text to display in the text area of the dialog
#      "helpText"        - Text to display on the help/more dialog
#      "menuOptsRef"     - name of the array containing the menu options
#      "optDescsRef"     - name of the associative array containing the description
#                          of the menu options
#---------------------------------------------------------------------------------------
showRadioListDialog() {
  local -n paramAssocArray="$1"

  showLinuxListDialog "paramAssocArray"
}


#---------------------------------------------------------------------------------------
#      METHOD:                      showWinManDialog
# DESCRIPTION: Select the type of Window Manger and the name of the WM to install
#              using a linux "radiolist" dialog.  Set the values within the global
#              array variable "TAB_SEL_VALS".
#  Required Params:
#      1) paramAssocArray - the name of the associative array with the required
#                           key/value pairs:
#      "dialogTitle"   - String to be displayed at the top of the dialog window.
#      "helpText"      - Text to display on the help/more dialog
#      "optDescsRef"   - name of the associative array containing the description
#                        of the menu options
#---------------------------------------------------------------------------------------
showWinManDialog() {
  local -n paramAssocArray="$1"
  local dialogTitle=""
  local windowTitle=""
  local helpText="${paramAssocArray["helpText"]}"
  local -A wmTypesParams=(["defaultChoice"]="${WINDOW_MANAGERS[1]}" ["dialogHeight"]=15 ["dialogWidth"]=80
    ["help-label"]="Help" ["help-text"]="$helpText" ["help-title"]="Help:  $dialogTitle")
  local -A wmDlgParams=(["dialogHeight"]=20 ["dialogWidth"]=80)
  local urls=()
  local wmName=""
  local sep=" "
  local wmType=""
  local dialogText=""
  local descsRef="${paramAssocArray["optDescsRef"]}"
  setELinksURLs "$helpText"

  addDescsToDlgParams "WINDOW_MANAGERS" "$descsRef" "wmTypesParams"

  for wmType in "${WINDOW_MANAGERS[@]}"; do
    addDescsToDlgParams "${paramAssocArray["$wmType"]}" "$descsRef" "wmDlgParams"
  done

  wmType=""
  while true; do
    if [ ${wmType} -gt 0 ]; then
      dialogText=$(echo "Select the standalone Window Manager of type '$wmType' to install:")
      windowTitle="$dialogTitle"
      dialogTitle=$(echo "'$wmType' types")
    else
      dialogText="Choose the type of Window Manager to install:"
      dialogTitle="${paramAssocArray["dialogTitle"]}"
      windowTitle="${paramAssocArray["dialogBackTitle"]}"
    fi
    exec 3>&1
      case "$wmType" in
        "${WINDOW_MANAGERS[0]}")
          wmDlgParams["defaultChoice"]="dwm"
          showListOfChoicesDialogBox "$windowTitle" "$dialogTitle" "$dialogText" DYNAMIC_WINDOW_MANAGERS "radiolist" wmDlgParams 2>&1 1>&3
        ;;
        "${WINDOW_MANAGERS[1]}")
          wmDlgParams["defaultChoice"]="Openbox"
          showListOfChoicesDialogBox "$windowTitle" "$dialogTitle" "$dialogText" STACKING_WINDOW_MANAGERS "radiolist" wmDlgParams 2>&1 1>&3
        ;;
        "${WINDOW_MANAGERS[2]}")
          wmDlgParams["defaultChoice"]="i3"
          showListOfChoicesDialogBox "$windowTitle" "$dialogTitle" "$dialogText" TILING_WINDOW_MANAGERS "radiolist" wmDlgParams 2>&1 1>&3
        ;;
        *)
          showListOfChoicesDialogBox "$windowTitle" "$dialogTitle" "$dialogText" WINDOW_MANAGERS "radiolist" wmTypesParams 2>&1 1>&3
        ;;
      esac
    exec 3>&-

    IFS=',' read -a fields <<< "$mbSelVal"
    cleanDialogFiles

    case ${fields[0]} in
      $DIALOG_OK)
        if [ ${TAB_SEL_VALS["WM_TYPE"]+_} ]; then
          TAB_SEL_VALS["WM_NAME"]=$(echo "${fields[1]//\"/}")
          break
        else
          wmType=$(echo "${fields[1]//\"/}")
          TAB_SEL_VALS["WM_TYPE"]="$wmType"
        fi
      ;;
      $DIALOG_HELP)
        if [ "${fields[1]}" == "View-URL" ]; then
          elinks "${urls[0]}"
        fi
      ;;
      *)
        if [ ${TAB_SEL_VALS["WM_NAME"]+_} ]; then
          unset TAB_SEL_VALS["WM_NAME"]
          unset TAB_SEL_VALS["WM_TYPE"]
          wmType=""
        else
          TAB_SEL_VALS=()
          break
        fi
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                     showCheckListDialog
# DESCRIPTION: This is a wrapper for calling a linux "dialog" of type "checklist" to
#              display the options available for a specific task.  Sets the value
#              selected within the global variable "mbSelVal".
#  Required Params:
#      1) paramAssocArray - the name of the associative array with the required
#                           key/value pairs:
#      "cancel-label"    - Skip or Cancel.
#      "dialogBackTitle" - The string to be displayed on the backdrop, at the top
#                          of the screen.
#      "dialogTitle"     - String to be displayed at the top of the dialog box.
#      "dialogText"      - Text to display in the text area of the dialog
#      "helpText"        - Text to display on the help/more dialog
#      "menuOptsRef"     - name of the array containing the menu options
#      "optDescsRef"     - name of the associative array containing the description
#                          of the menu options
#---------------------------------------------------------------------------------------
showCheckListDialog() {
  local -n paramAssocArray="$1"

  paramAssocArray["listType"]="checklist"
  paramAssocArray["output-separator"]="|"
  if [ "${paramAssocArray["menuOptsRef"]}" == "KDE_THEMES" ]; then
    paramAssocArray["dialogWidth"]=98
  fi

  showLinuxListDialog "paramAssocArray"
}
