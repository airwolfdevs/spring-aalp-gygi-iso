#!/bin/bash

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# globals
#===============================================================================

declare DASH_LINE_WIDTH_80="--------------------------------------------------------------------------"
declare DASH_LINE_FULL_WIDTH="----------------------------------------------------------------------------------------------"

: ${DIALOG=dialog}

# Define the dialog exit status codes
: ${DIALOG_OK=0}
: ${DIALOG_CANCEL=1}
: ${DIALOG_HELP=2}
: ${DIALOG_EXTRA=3}
: ${DIALOG_ESC=255}
: ${DIALOG_INPUT_ERR0R=409}
: ${DIALOG_VALUE_ERR0R=406}

instMenu=`instMenu 2>/dev/null` || instMenu=/tmp/inst-menu$$
trap "rm -f $instMenu" 0 1 2 5 15
instInput=`tempfile 2>/dev/null` || instInput=/tmp/test$$
trap "rm -f $instInput" 0 1 2 5 15

helpTextFile="/tmp/help-textbox.txt"

#===============================================================================
# functions / methods
#===============================================================================

#---------------------------------------------------------------------------------------
#      METHOD:             showListOfChoicesDialogBox
# DESCRIPTION: Displays a list of choices using the linux "dialog" command's
#              check list, menu, or radio list dialog box
#
#  Required Params:
#      1) backTitle - Specifies a backtitle string to be displayed on the
#                     backdrop, at the top of the screen.
#      2) dialogTitle - String to be displayed at the top of the dialog box.
#      3) dialogText - Text to display in dialog before the menu options
#      4) menuOpts - List of options that can be selected
#      5) listType - can be "checklist", "menu", or "radiolist"
#      6) cmdOpts - associative array that may contain:
#                     A) key/value pair of options for the linux "dialog" command
#                        (see function getCmdForDialogBoxType)
#                     B) key/value pair of choices where the key is the choice
#                        and the value is its description
#                     C) the default choice where key = "defaultChoice" and the
#                        value can either be numeric or a string
#---------------------------------------------------------------------------------------
showListOfChoicesDialogBox() {
  local backTitle=$1
  local dialogTitle=$2
  local dialogText=$3
  local -n choicesParam=$4
  local menuBoxType="$5"
  local -n cmdOpts=$6
  local listHeight=${#choicesParam[@]}

  if [ ${cmdOpts["listHeight"]+_} ]; then
    listHeight=${cmdOpts["listHeight"]}
  elif [ "$listHeight" -lt 28 ]; then
    listHeight=$(expr $listHeight + 1)
  elif [ "$listHeight" -gt 28 ]; then
    listHeight=28
  fi

  mbDialogCmd=$(getCmdForDialogBoxType "$menuBoxType" "$backTitle" "$dialogText" "$dialogTitle" cmdOpts)

  local row="${choicesParam[0]}"
  local cmdSplit=(${row})
  if [ ${#cmdSplit[@]} -gt 2 ]; then
    mbDialogCmd+=' $listHeight'
    for row in "${choicesParam[@]}"; do
      mbDialogCmd+=$(printf " %s" "$row")
    done
    mbDialogCmd+=' 2> $instMenu'
  else
    getChoicesForDialog choicesParam cmdOpts "$menuBoxType"
    mbDialogCmd+=' $listHeight "${dialogChoices[@]}" 2> $instMenu'
  fi

  eval "$mbDialogCmd"

  dialogCmdRetVal=$?

  case $dialogCmdRetVal in
    $DIALOG_OK)
      mbSelVal=`cat $instMenu`
      mbSelVal=$(echo "$DIALOG_OK,$mbSelVal")
    ;;
    $DIALOG_CANCEL)
      mbSelVal=$(echo "$DIALOG_CANCEL,Cancel")
    ;;
    $DIALOG_HELP)
      showHelpDialog "$backTitle" cmdOpts
    ;;
    $DIALOG_EXTRA)
      mbSelVal=$(echo "$DIALOG_EXTRA,Extra")
    ;;
    $DIALOG_ESC)
      mbSelVal=$(echo "$DIALOG_CANCEL,ESC")
    ;;
  esac
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getChoicesForDialog
# DESCRIPTION: Converts the array of choices to an array of selectable choices
#              for the linux "dialog" command
#      RETURN: array of selectable choices in the global variable "dialogChoices"
#
#  Required Params:
#      1) choicesArray - array of list/menu choices that can be selected
#      2) dialogCmdOpts - associative array that may contain:
#                     A) key/value pair of choice descriptions where the
#                        key is the choice and the value is its description
#                     B) the default choice where key = "defaultChoice" and the
#                        value can either be numeric or a string
#---------------------------------------------------------------------------------------
function getChoicesForDialog() {
  local -n choicesArray=$1
  local -n dialogCmdOpts=$2
  local menuBoxType="$3"
  local numRegEx='^[0-9]+$'
  local idx=1
  local defaultChoice=1
  local checklistVals=()
  local chkSep="|"
  dialogChoices=()

  if [ ${dialogCmdOpts["defaultChoice"]+_} ]; then
    defaultChoice=$(echo "${dialogCmdOpts[defaultChoice]}")
  elif [ ${dialogCmdOpts["checklistVals"]+_} ]; then
    readarray -t checklistVals <<< "${dialogCmdOpts["checklistVals"]//$chkSep/$'\n'}"
  fi

  for choice in "${choicesArray[@]}"; do
    if [ ${dialogCmdOpts["$choice"]+_} ]; then
      dialogChoices+=("$choice")
      dialogChoices+=("${dialogCmdOpts[$choice]}")
    else
      dialogChoices+=("$idx")
      dialogChoices+=("$choice")
    fi

    if [ "$menuBoxType" != "menu" ]; then
      if [ ${#checklistVals[@]} -gt 0 ]; then
        local pos=$(findPositionForString checklistVals "$choice")
        if [ ${pos} -gt -1 ]; then
          dialogChoices+=(ON)
        else
          dialogChoices+=(OFF)
        fi
      elif [[ $defaultChoice =~ $numRegEx ]]; then
        if [ "$idx" -eq $defaultChoice ]; then
          dialogChoices+=(ON)
        else
          dialogChoices+=(OFF)
        fi
      else
        if [ "$choice" == "$defaultChoice" ]; then
          dialogChoices+=(ON)
        else
          dialogChoices+=(OFF)
        fi
      fi
    fi
    idx=$(expr $idx + 1)
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                      showHelpDialog
# DESCRIPTION: Displays the text for the help section in a linux "dialog" box of type:
#                   A) "msgbox" if the helpText contains a link/URL
#                   B) "textbox"
#      RETURN: "Help" or "View-URL" in the global string var "mbSelVal"
#  Required Params:
#      1)   backTitle - Specifies a backtitle string to be displayed on the
#                       backdrop, at the top of the screen.
#      2) helpCmdOpts - associative array that may contain the following:
#           "helpHeight" - height of help dialog
#            "helpWidth" - width of help dialog
#            "help-text" - text to display
#           "help-title" - String to be displayed at the top of the help dialog box
#---------------------------------------------------------------------------------------
showHelpDialog() {
  local backTitle=$1
  local -n helpCmdOpts=$2
  local helpText="NO HELP TEXT FOUND!"
  local helpTitle="Help"
  local key=""
  local helpDialogHeight=25
  local helpDialogWidth=80
  local cmdKeys=("help-text" "help-title" "helpHeight" "helpWidth")

  for key in "${cmdKeys[@]}"; do
    if [ ${helpCmdOpts["$key"]+_} ]; then
      case $key in
        "help-text")
          local helpTextArray+=(
          "----------------------------------------------------------"
          "           Use 'j' and 'k' keys to scroll"
          "----------------------------------------------------------" " "
          "${helpCmdOpts[$key]}")
          helpText=$(getTextForDialog "${helpTextArray[@]}")
        ;;
         "help-title")
          helpTitle=$(echo "${helpCmdOpts[$key]}")
        ;;
         "helpHeight")
          helpDialogHeight=${helpCmdOpts[$key]}
        ;;
         "helpWidth")
          helpDialogWidth=${helpCmdOpts[$key]}
        ;;
      esac
    fi
  done

  if [[ "$helpText" =~ "http" ]]; then
    $DIALOG --clear --no-collapse --backtitle "$backTitle" --title "${helpTitle}" --ok-label "Exit" \
    --extra-button --extra-label "View URL" --msgbox "$helpText" ${helpDialogHeight} ${helpDialogWidth} 3>&1 1>&2 2>&3
  else
    $DIALOG --keep-window --backtitle "$backTitle" --title "$helpTitle" --ok-label "Exit" \
            --msgbox "$helpText" ${helpDialogHeight} ${helpDialogWidth} 3>&1 1>&2 2>&3
  fi
  dialogHelpRetVal=$?
  case $dialogHelpRetVal in
    $DIALOG_OK)
      mbSelVal=$(echo "$DIALOG_HELP,Help")
    ;;
    $DIALOG_EXTRA)
      mbSelVal=$(echo "$DIALOG_HELP,View-URL")
    ;;
  esac

}

#---------------------------------------------------------------------------------------
#    FUNCTION:                getCmdForDialogBoxType
# DESCRIPTION: Concatenates the linux "dialog" command and options to a string
#  Required Params:
#      1) dialogBoxType - the type of dialog box (i.e. check list, input box, etc.)
#      2) backTitle - Specifies a backtitle string to be displayed on the
#                     backdrop, at the top of the screen.
#      3) dialogText - Text to display in dialog before the menu options
#      4) dialogTitle - String to be displayed at the top of the dialog box.
#      5) dialogCmdOpts - associative array of options for the linux "dialog"command
#                         (see below)
#  Options for the linux "dialog" command:
#      "cancel-label" - Show the "Cancel" button and override the default label
#      "defaultChoice" - default value that will be automatically set/selected
#      "dialogHeight" - height of dialog
#      "dialogWidth" - width of dialog
#      "extra-button" - Show an extra button, between "OK" and "Cancel" buttons.
#      "extra-label" - Show an extra button, between "OK" and "Cancel" buttons
#                      and override the default label "Extra"
#      "help-button" - Show a help-button after "OK" and "Cancel" buttons.
#      "help-label" - Show a help-button after "OK" and "Cancel" buttons and
#                     override the default label "Help".
#      "help-text" - text to display on the Help dialog (i.e. dialog of type text box)
#      "nocancel" - Hides cancel button
#                      multiple spaces to a single space
#      "no-collapse" - Disables conversion of tabs to spaces and reduction of
#                      multiple spaces to a single space
#---------------------------------------------------------------------------------------
function getCmdForDialogBoxType() {
  local dialogBoxType="$1"
  local dialogBackTitle="$2"
  local dialogText="$3"
  local dialogTitle=$4
  local -n dialogCmdOpts=$5
  local dialogHeight=15
  local dialogWidth=80
  local initValue=""
  local cmd=$(echo "$DIALOG --clear --backtitle \"$dialogBackTitle\" --title \"$dialogTitle\"")
  local cmdKeys=("no-collapse" "insecure" "nocancel" "nook" "max-input" "ok-label" "cancel-label" "extra-label"
    "extra-button" "help-label" "help-button" "dialogHeight" "dialogWidth" "initValue" "output-separator"
    "column-separator")

  for key in "${cmdKeys[@]}"; do
    if [ ${dialogCmdOpts["$key"]+_} ]; then
      case $key in
        "cancel-label")
          if [ ! ${dialogCmdOpts["nocancel"]+_} ]; then
            cmd+=$(echo " --$key \"${dialogCmdOpts[$key]}\"")
          fi
        ;;
        "column-separator")
          cmd+=$(echo " --$key \"${dialogCmdOpts[$key]}\"")
        ;;
        "dialogHeight")
          dialogHeight=${dialogCmdOpts[$key]}
        ;;
        "dialogWidth")
          dialogWidth=${dialogCmdOpts[$key]}
        ;;
        "extra-button")
          cmd+=$(echo " --$key")
        ;;
        "extra-label")
          cmd+=$(echo " --extra-button --$key \"${dialogCmdOpts[$key]}\"")
        ;;
        "help-button")
          cmd+=$(echo " --$key")
        ;;
        "help-label")
          cmd+=$(echo " --help-button --$key \"${dialogCmdOpts[$key]}\"")
        ;;
        "initValue")
          initValue=$(echo "${dialogCmdOpts[$key]}")
        ;;
        "insecure")
          cmd+=$(echo " --$key")
        ;;
        "max-input")
          cmd+=$(echo " --$key ${dialogCmdOpts[$key]}")
        ;;
        "nocancel")
          cmd+=$(echo " --$key")
        ;;
        "nook")
          cmd+=$(echo " --$key")
        ;;
        "no-collapse")
          cmd+=$(echo " --$key")
        ;;
        "ok-label")
          cmd+=$(echo " --$key \"${dialogCmdOpts[$key]}\"")
        ;;
        "output-separator")
          cmd+=$(echo " --$key \"${dialogCmdOpts[$key]}\"")
        ;;
      esac
    fi
  done

  if [ ${#initValue} -gt 0 ]; then
    cmd+=$(echo " --$dialogBoxType \"$dialogText\" $dialogHeight $dialogWidth $initValue")
  else
    cmd+=$(echo " --$dialogBoxType \"$dialogText\" $dialogHeight $dialogWidth")
  fi

  echo "$cmd"
}

#---------------------------------------------------------------------------------------
#      METHOD:                  executeLinuxCommands
# DESCRIPTION: Display a linux "dialog" box of type gauge (i.e. progress bar) while
#              executing a set of linux commands and logging the results.
#  Required Params:
#      1)     backTitle - Specifies a backtitle string to be displayed on the
#                         backdrop, at the top of the screen.
#      2)       pbTitle - String to be displayed at the top of the dialog box.
#      3)   cmdKeyArray - Array of keys to the associative arrays
#      4) cmdTitleArray - Associative array containing the text to be displayed
#                         for the commands in the progress bar
#      5)      cmdArray - Associative array containing the linux commands to be executed
#  Optional Params:
#      6) flag to indicate if in "DEBUG" mode
#---------------------------------------------------------------------------------------
executeLinuxCommands() {
  local backTitle="$1"
  local pbTitle="$2"
  local -n cmdKeyArray=$3
  local -n cmdTitleArray=$4
  local -n cmdArray=$5
  local cmdNum=1
  local pbPerc=0
  local cmd=""
  local evalCmds=()
  local debugFlag=$(echo 'false' && return 1)

  cmdKeyArray+=("done")
  cmdTitleArray["done"]="All commands have been executed!!!!"

  if [ "$AALP_GYGI_DIALOG" == "debug" ]; then
    debugFlag=$(echo 'true' && return 0)
  fi

  local totCmds=${#cmdKeyArray[@]}
  (
    sleep 0.5
    for pbKey in "${cmdKeyArray[@]}"; do
      echo $pbPerc
      echo "XXX"
      echo "${cmdTitleArray[$pbKey]}..."
      echo "XXX"
      cmd="${cmdArray[$pbKey]}"

      case $pbKey in
        "done")
          if [ ${#cmd} -gt 0 ]; then
            clog_info "${cmdTitleArray[$pbKey]}\nResult of command [$cmd]" >> "$INSTALLER_LOG"
            eval ${cmd} &>> "$INSTALLER_LOG"
          else
            clog_warn "${cmdTitleArray[$pbKey]}" >> "$INSTALLER_LOG"
          fi
        ;;
        *)
          IFS=$'\t' read -a evalCmds <<< "$cmd"
          for cmd in "${evalCmds[@]}"; do
            clog_info "Executing command [$cmd]" >> "$INSTALLER_LOG"
            if ${debugFlag}; then
              clog_warn "debugFlag=[$debugFlag] - sleep 5" >> "$INSTALLER_LOG"
              sleep 5
            else
              eval ${cmd} &>> "$INSTALLER_LOG"
            fi
            clog_info "Command [$cmd] is DONE" >> "$INSTALLER_LOG"
          done
          cmdNum=$(expr $cmdNum + 1)
          pbPerc=$(printf "%.0f" $(echo "scale=3; $cmdNum / $totCmds * 100" | bc))
        ;;
      esac

      sleep 0.5
    done
  ) | $DIALOG --clear --backtitle "$backTitle" --title "$pbTitle" --gauge "Please Wait..." 20 80 0
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getLinuxFileSystemType
# DESCRIPTION: Get the type of file system that will be used to format the
#              partition/logical volume
#      RETURN: selected file system type in the global "mbSelVal" variable
#  Required Params:
#      1) partName - Name of the partition to assign the file system type to.
#      2) partSize - human readable size of partition
#---------------------------------------------------------------------------------------
function getLinuxFileSystemType() {
  local partName=$1
  local partSize=$2
  local fields=()

  while true; do
    exec 3>&1
      selectFileSystemType "$partName" "$partSize" 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$mbSelVal"
    case ${fields[0]} in
      $DIALOG_OK)
        mbSelVal=$(echo "${fields[1]}")
        break
      ;;
      $DIALOG_CANCEL)
        mbSelVal=""
        break
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                  showDialogInputBox
# DESCRIPTION: Displays an input box for the linux "dialog" command
#      RETURN: value that was entered in the "ibEnteredText" variable
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)      dialogText - Text to display in dialog before the menu options
#      3)     dialogTitle - String to be displayed at the top of the dialog box.
#      4)   dialogCmdOpts - options for the linux "dialog" command
#  Optional Params:
#      5) dialogType - if passed in, will set the type of dialog to a password box
#---------------------------------------------------------------------------------------
showDialogInputBox() {
  local dialogBackTitle="$1"
  local dialogText="$2"
  local dialogTitle="$3"
  local -n cmdOpts=$4
  local enteredText=""
  local dialogType="inputbox"

  if [ "$#" -gt 4 ]; then
    dialogType="passwordbox"
  fi

  ibDialogCmd=$(getCmdForDialogBoxType "$dialogType" "$dialogBackTitle" "$dialogText" "$dialogTitle" cmdOpts)
  ibDialogCmd+=' 2> $instInput'
  eval "$ibDialogCmd"

  cmdRetVal=$?

  case $cmdRetVal in
    $DIALOG_OK)
      ibEnteredText=$(cat "$instInput")
      ibEnteredText=$(echo "$DIALOG_OK,$ibEnteredText")
    ;;
    $DIALOG_CANCEL)
      ibEnteredText=$(echo "$DIALOG_CANCEL,Cancel")
    ;;
    $DIALOG_HELP)
      showHelpDialog "$dialogBackTitle" cmdOpts
      ibEnteredText="$mbSelVal"
    ;;
    $DIALOG_EXTRA)
      ibEnteredText=$(echo "$DIALOG_EXTRA,Extra")
    ;;
    $DIALOG_ESC)
      ibEnteredText=$(echo "$DIALOG_CANCEL,ESC")
    ;;
  esac
}

#---------------------------------------------------------------------------------------
#      METHOD:                   showYesNoBox
# DESCRIPTION: Displays a "dialog" yes/no box
#      RETURN: "YES" or "NO" in the "inputRetVal" variable
#  Required Params:
#      1) backTitle - Specifies a backtitle string to be displayed on the
#                     backdrop, at the top of the screen.
#      2) dialogText - Text to display in dialog before the menu options
#      3) dialogTitle - String to be displayed at the top of the dialog box.
#      4) dialogCmdOpts - associative array that may contain the following:
#---------------------------------------------------------------------------------------
showYesNoBox() {
  local backTitle="$1"
  local dialogText="$2"
  local dialogTitle="$3"
  local -n dialogCmdOpts=$4

  dialogCmd=$(getDialogCmdForYesNo "$backTitle" "$dialogText" "$dialogTitle" dialogCmdOpts)

  eval "$dialogCmd"

  dialogRetVal=$?

  case $dialogRetVal in
    $DIALOG_OK)
      inputRetVal=$(echo "$DIALOG_OK,YES")
    ;;
    $DIALOG_CANCEL)
      inputRetVal=$(echo "$DIALOG_CANCEL,NO")
    ;;
    $DIALOG_EXTRA)
      inputRetVal=$(echo "$DIALOG_EXTRA,Extra")
    ;;
    $DIALOG_HELP)
      showHelpDialog "$backTitle" dialogCmdOpts
      inputRetVal="$mbSelVal"
    ;;
    $DIALOG_ESC)
      inputRetVal=$(echo "$DIALOG_CANCEL,ESC")
    ;;
  esac
}

#---------------------------------------------------------------------------------------
#    FUNCTION:               getDialogCmdForYesNo
# DESCRIPTION: Get the command that will display a linux "dialog" box of type yes/no
#      RETURN: concatenated string
#  Required Params:
#      1) backTitle - Specifies a backtitle string to be displayed on the
#                     backdrop, at the top of the screen.
#      2) dialogText - Text to display in dialog before the menu options
#      3) dialogTitle - String to be displayed at the top of the dialog box.
#      4) yesNoCmdOpts - associative array that may contain the following:
#  Options for the linux "dialog" command:
#      "yes-label" - override the label used for the "Yes" button
#      "no-label" - override the label used for the "No" button
#      "dialogHeight" - height of dialog
#      "dialogWidth" - width of dialog
#      "extra-button" - Show an extra button, between "OK" and "Cancel" buttons.
#      "extra-label" - Show an extra button, between "OK" and "Cancel" buttons
#                      and override the default label "Extra"
#      "helpHeight" - height of help dialog
#      "helpWidth" - width of help dialog
#      "help-button" - Show a help-button after "OK" and "Cancel" buttons.
#      "help-label" - Show a help-button after "OK" and "Cancel" buttons and
#                     override the default label "Help".
#      "help-text" - text to display on the Help dialog (i.e. dialog of type text box)
#      "help-title" - String to be displayed at the top of the help dialog box
#---------------------------------------------------------------------------------------
function getDialogCmdForYesNo() {
  local backTitle="$1"
  local dialogText="$2"
  local dialogTitle="$3"
  local -n yesNoCmdOpts=$4
  local dialogHeight=15
  local dialogWidth=80
  local helpDialogHeight=25
  local helpDialogWidth=80
  local keyArray=("yes-label" "no-label" "dialogHeight" "dialogWidth" "extra-button" "extra-label"
   "helpHeight" "helpWidth" "help-button" "help-label" "help-text" "help-title")
  local helpText="NO HELP TEXT FOUND!"
  local helpTitle="Help"
  local cmd=$(echo "$DIALOG --clear --no-collapse --backtitle \"$backTitle\" --title \"$dialogTitle\"")

  for key in "${keyArray[@]}"; do
    if [ ${yesNoCmdOpts["$key"]+_} ]; then
      case $key in
        "dialogHeight")
          dialogHeight=${yesNoCmdOpts[$key]}
        ;;
        "dialogWidth")
          dialogWidth=${yesNoCmdOpts[$key]}
        ;;
        "extra-button")
          cmd+=$(echo " --$key --ok-label \"${yesNoCmdOpts[yes-label]}\"")
        ;;
        "extra-label")
          cmd+=$(echo " --extra-button --$key \"${yesNoCmdOpts[$key]}\" --ok-label \"${yesNoCmdOpts[yes-label]}\" --cancel-label \"${yesNoCmdOpts[no-label]}\"")
        ;;
        "no-label")
          cmd+=$(echo " --$key \"${yesNoCmdOpts[$key]}\"")
        ;;
        "yes-label")
          cmd+=$(echo " --$key \"${yesNoCmdOpts[$key]}\"")
        ;;
        "helpHeight")
          helpDialogHeight=${yesNoCmdOpts[$key]}
        ;;
        "helpWidth")
          helpDialogWidth=${yesNoCmdOpts[$key]}
        ;;
        "help-button")
          cmd+=$(echo " --$key")
        ;;
        "help-label")
          cmd+=$(echo " --help-button --$key \"${yesNoCmdOpts[$key]}\"")
        ;;
        "help-text")
          helpText=$(echo "${yesNoCmdOpts[$key]}")
        ;;
        "help-title")
          helpTitle=$(echo "${yesNoCmdOpts[$key]}")
        ;;
      esac
    fi
  done

  cmd+=$(echo " --yesno \"$dialogText\" $dialogHeight $dialogWidth")

  echo "$cmd"
}

#---------------------------------------------------------------------------------------
#      METHOD:               getSelectedLinuxPartitionTool
# DESCRIPTION: Get the linux tool that will be used to create and/or manipulate
#              the partition table
#      RETURN: selected linux tool in the global variable "mbSelVal"
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)     dialogTitle - String to be displayed at the top of the dialog box.
#      3)      linuxTools - array of linux partition tool names
#      4)  linuxToolDescs - associative array of key/value pairs that describe the type
#                           of interaction for each tool
#---------------------------------------------------------------------------------------
getSelectedLinuxPartitionTool() {
  local dialogBackTitle="$1"
  local dialogTitle="$2"
  local -n linuxTools=$3
  local -n linuxToolDescs=$4
  local helpText=$(getHelpTextForTool)
  local textArray=(" " "Partitioning tools that are used to create and/or manipulate the device"
    "partition tables and partitions. (More...)" " " "$DASH_LINE_WIDTH_80" " "
    "Select a partition tool:"
  )
  local dialogText=$(getTextForDialog "${textArray[@]}")

  while true; do
    exec 3>&1
      selectLinuxPartitionTool "$dialogBackTitle" "$dialogText" "$dialogTitle" "$helpText" linuxTools linuxToolDescs 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$mbSelVal"
    case ${fields[0]} in
      $DIALOG_OK)
        mbSelVal=$(echo "${fields[1]}")
        break
      ;;
      $DIALOG_CANCEL)
        mbSelVal=""
        break
      ;;
      $DIALOG_HELP)
        if [ "${fields[1]}" == "View-URL" ]; then
          elinks "https://wiki.archlinux.org/index.php/partitioning#Partitioning_tools"
        fi
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                 selectLinuxPartitionTool
# DESCRIPTION: Select the Linux Partition Tool within the linux "dialog" box of
#              type "radio list"
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)      dialogText - Text to display in dialog before the menu options
#      3)     dialogTitle - String to be displayed at the top of the dialog box.
#      4)        helpText - Text to display on the help/more dialog
#      5)     toolChoices - array of tool names to choose from
#      6)        dlgDescs - associative array of interaction types for each tool
#---------------------------------------------------------------------------------------
selectLinuxPartitionTool() {
  local dialogBackTitle="$1"
  local dialogText="$2"
  local dialogTitle="$3"
  local helpText="$4"
  local -n toolChoices=$5
  local -n dlgDescs=$6
  local -A dialogParms=(["dialogHeight"]=25 ["help-label"]="More..." ["help-text"]="$helpText"
      ["help-title"]="Help:  $dialogTitle" ["helpURL"]=true)
  local key=""

  for key in "${!dlgDescs[@]}"; do
      dialogParms["$key"]=${dlgDescs["$key"]}
  done

  showListOfChoicesDialogBox "$backTitle" "$dialogTitle" "$dialogText" toolChoices "radiolist" dialogParms
}

#---------------------------------------------------------------------------------------
#      METHOD:                       pbSpinner
# DESCRIPTION: Dialog version of a spinning progress bar
#  Required Params:
#      1)       progressID - process ID of the progress bar
#      2)        pckgName - String to be displayed at the top of the dialog box.
#      3) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#---------------------------------------------------------------------------------------
pbSpinner() {
  local progressID=$1
  local pckgName="$2"
  local dialogBackTitle="$3"
  local pbPerc=0
  local totSteps=10
  local stepNum=11
  local pbTitle="Installing '$pckgName'..."
  (
  while true; do
    echo $pbPerc
    echo "XXX"
    echo "$pbTitle"
    echo "XXX"
    if [ ${pbPerc} -lt 100 ]; then
      sleep 0.25
    else
      break
    fi
    kill -0 $progressID &> /dev/null;
    if [[ $? == 0 ]]; then
      if [ ${stepNum} -lt 9 ]; then
        stepNum=$(expr $stepNum + 1)
      else
        stepNum=1
      fi
      if [ ${stepNum} -gt 8 ]; then
        pbPerc=99
      else
        pbPerc=$(printf "%.0f" $(echo "scale=3; $stepNum / $totSteps * 100" | bc))
      fi
    else
      pbPerc=100
      pbTitle="Process of Installing '$pckgName'is done!!!!!!!"
#      local existFlag=$(isPackageInstalled "pacman" "$pckgName")
#      if ${existFlag}; then
#        pbTitle="Successfully installed '$pckgName'!"
#      else
#        pbTitle="Installation of '$pckgName' FAILED!!!!!!!"
#      fi
    fi
  done
  sleep 2
  ) | $DIALOG --clear --begin 3 3 --backtitle "$dialogBackTitle" --title "$pbTitle" --gauge "Please Wait..." 15 80 0
}

#---------------------------------------------------------------------------------------
#      METHOD:                showTypeOfConnectionConfigDlg
# DESCRIPTION: Get the type of connection configuration to set up.
#              Set value within the global variable "mbSelVal".
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)        skipFlag - flag to indicate whether skip button is available
#---------------------------------------------------------------------------------------
showTypeOfConnectionConfigDlg() {
  local helpText=$(getHelpTextForNetworkConfig)
  local urlRefs=()

  setRefLinks "$helpText" urlRefs

  while true; do
    exec 3>&1
      selectTypeOfConnectionConfig "$1" "$helpText" ${2} 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$mbSelVal"
    case ${fields[0]} in
      $DIALOG_OK)
        local aryIdx=$(expr ${fields[1]} - 1)
        mbSelVal=$(echo "${connConfigOpts[$aryIdx]}")
        break
      ;;
      $DIALOG_CANCEL)
        mbSelVal="Skip"
        break
      ;;
      $DIALOG_HELP)
        if [ "${fields[1]}" == "View-URL" ]; then
          elinks "${urlRefs[0]}"
        fi
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                         setRefLinks
# DESCRIPTION: Extract the URLs from the text for the help dialog and set them within
#              the array
#  Required Params:
#      1) helpText - string containing the URLs within the text to be displayed
#                      in the help dialog
#      2) refLinks - array to add the URLs that were found
#---------------------------------------------------------------------------------------
setRefLinks() {
  local helpText="$1"
  local -n refLinks=$2
  IFS=$'\n' read -d '' -a textArray <<< "$helpText"
  for line in "${textArray[@]}"; do
    if [[ "$line" =~  "http" ]]; then
      pos=$(expr index "$line" -)
      if [ ${pos} -gt 0 ]; then
        pos=$(expr $pos + 1)
      fi
      refLinks+=($(echo "${line:${pos}}"))
    fi
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                selectInstallationSteps
# DESCRIPTION: Select the installation steps to perform within a linux "dialog" box
#              of type check list
#  Required Params:
#      1)   dialogBackTitle - Specifies a backtitle string to be displayed on the
#                             backdrop, at the top of the screen.
#      2)            urlRef - the URL to be displayed in help dialog
#      3)          skipFlag - flag to indicate whether skip button is available
#---------------------------------------------------------------------------------------
selectTypeOfConnectionConfig() {
  local dialogBackTitle="$1"
  local helpText="$2"
  local skipFlag=$3
  local dialogTitle="Network Configuration"
  local -A dialogParms=(["no-collapse"]=true ["dialogHeight"]=20
  ["help-button"]=true ["helpHeight"]=28 ["help-text"]="$helpText" ["help-title"]="Help:  $dialogTitle" ["helpURL"]=true)
  local dialogText=$(getTextForNetworkConfig)

  if ${skipFlag}; then
    dialogParms["cancel-label"]="Skip"
  else
    dialogParms["nocancel"]=true
  fi

  showListOfChoicesDialogBox "$dialogBackTitle" "$dialogTitle" "$dialogText" connConfigOpts "radiolist" dialogParms
}

#---------------------------------------------------------------------------------------
#      METHOD:                 getValueFromDialogInputBox
# DESCRIPTION: Uses a "dialog" input box to get a value from the keyboard and sets
#              the text that was entered in the "ibEnteredText" variable
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)     dialogTitle - String to be displayed at the top of the dialog box.
#      3)      dialogText - Text to display in dialog before the menu options
#---------------------------------------------------------------------------------------
getValueFromDialogInputBox() {
  local dialogBackTitle="$1"
  local dialogTitle="$2"
  local dialogText="$3"
  local -A dialogOpts=(["dialogHeight"]=20)
  local fields=()
  local validFlag=$(echo 'false' && return 1)

  exec 3>&1
    showDialogInputBox "$dialogBackTitle" "$dialogText" "$dialogTitle" dialogOpts 2>&1 1>&3
  exec 3>&-
  IFS=',' read -a fields <<< "$ibEnteredText"
  case ${fields[0]} in
    $DIALOG_OK)
      ibEnteredText=$(trimString "${fields[1]}")

      if [ ${#ibEnteredText} -gt 0 ]; then
        if typeset -f validateProxy > /dev/null; then
          validFlag=$(validateProxy)
          if ${validFlag}; then
            ibEnteredText=$(echo "$DIALOG_OK,$ibEnteredText")
          else
            ibEnteredText=$(echo "$DIALOG_VALUE_ERR0R,$ibEnteredText")
          fi
        else
          ibEnteredText=$(echo "$DIALOG_OK,$ibEnteredText")
        fi
      else
        ibEnteredText=$(echo "$DIALOG_INPUT_ERR0R,No Value")
      fi
    ;;
    ${DIALOG_CANCEL})
      ibEnteredText=$(echo "$DIALOG_CANCEL,${fields[1]}")
    ;;
    ${DIALOG_ESC})
      ibEnteredText=$(echo "$DIALOG_CANCEL,${fields[1]}")
    ;;
  esac
}

#---------------------------------------------------------------------------------------
#      METHOD:                dispInvValueEntered
# DESCRIPTION: Displays the appropriate error message
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)        errorMsg - the error message to display
#      3)     dialogTitle - String to be displayed at the top of the dialog box.
#
#---------------------------------------------------------------------------------------
dispInvValueEntered() {
  local dialogBackTitle="$1"
  local errorMsg="$2"
  local dialogTitle=$(echo "Error:  Invalid $3")
  local errTxt=$(getTextForError)
  local endTxt="You must hit OK to continue!"

  if [ "$#" -gt 3 ]; then
    endTxt="$4"
  fi

  local msgTextArray=("$errTxt" " " "$errorMsg" "$DASH_LINE_WIDTH_80" " " "$endTxt")

  local msgText=$(getTextForDialog "${msgTextArray[@]}")

  $DIALOG --clear --no-collapse --backtitle "$dialogBackTitle" --title "$dialogTitle" --msgbox "$msgText" 25 80 3>&1 1>&2 2>&3
}

#---------------------------------------------------------------------------------------
#      METHOD:                  displayURLinElinks
# DESCRIPTION: Display the selected URL within the Elinks web browser
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)     menuChoices - array of choices to display in the linux "dialog"
#      3)        menuOpts - associative array of options for the linux "dialog" command
#                           as well as the URLs that map to the "urlChoices"
#---------------------------------------------------------------------------------------
displayURLinElinks() {
  local dialogBackTitle="$1"
  local -n menuChoices=$2
  local -n menuOpts=$3
  local fields=()

  while true; do
    exec 3>&1
      selectURLtoViewInElinks "$dialogBackTitle" menuChoices menuOpts 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$mbSelVal"
    case ${fields[0]} in
      $DIALOG_OK)
        if [ ${menuOpts["urlPrefix"]+_} ]; then
          mbSelVal=$(echo "${menuOpts[urlPrefix]}${menuOpts[${fields[1]}]:2}")
        else
          mbSelVal=$(echo "${menuOpts[${fields[1]}]:2}")
        fi
        elinks "$mbSelVal"
      ;;
      $DIALOG_CANCEL)
        mbSelVal=""
        break
      ;;
    esac
  done

}

#---------------------------------------------------------------------------------------
#      METHOD:                selectURLtoViewInElinks
# DESCRIPTION: Select the URL to view in Elinks using a linux "dialog" box of type
#              radio list
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)      urlChoices - array of choices to display in the linux "dialog"
#      3)     dialogParms - associative array of options for the linux "dialog" command
#                           as well as the URLs that map to the "urlChoices"
#---------------------------------------------------------------------------------------
selectURLtoViewInElinks() {
  local dialogBackTitle="$1"
  local -n urlChoices=$2
  local -n dialogParms=$3
  local textArray=(
  "ELinks is an advanced and well-established feature-rich text mode web"
  "(HTTP/FTP/...) browser."
  "$DASH_LINE_WIDTH_80" " "
  )

  if [ ${dialogParms["urlPrefix"]+_} ]; then
    textArray+=(
    "URL Prefix" "----------" "${dialogParms[urlPrefix]}" " "
    "Select the Path to be appended to the \\\"URL Prefix\\\" to form the URL"
    "to be viewed within \\\"elinks\\\":"
    )
  else
    textArray+=("Select the URL to view within \\\"elinks\\\":")
  fi

  local dialogText=$(getTextForDialog "${textArray[@]}")
  local dialogTitle="Elinks Web Browser Wrapper"

  showListOfChoicesDialogBox "$dialogBackTitle" "$dialogTitle" "$dialogText" urlChoices "radiolist" dialogParms
}

#---------------------------------------------------------------------------------------
#      METHOD:               getSelectedDeviceToPartition
# DESCRIPTION: Get the device to be partitioned and where Arch Linux will be installed.
#      RETURN: name of device in the global "mbSelVal" variable
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#---------------------------------------------------------------------------------------
getSelectedDeviceToPartition() {
  local dialogBackTitle="$1"

  mbSelVal=""
  exec 3>&1
    showDialogForDeviceSel "$dialogBackTitle" 2>&1 1>&3
  exec 3>&-

  IFS=',' read -a fields <<< "$mbSelVal"
  case ${fields[0]} in
    $DIALOG_OK)
      local aryIdx=$(expr ${fields[1]} - 1)
      mbSelVal=$(echo "${devices[$aryIdx]}")
    ;;
    $DIALOG_CANCEL)
      mbSelVal=""
    ;;
  esac
}

#---------------------------------------------------------------------------------------
#      METHOD:                showDialogForDeviceSel
# DESCRIPTION: Show a linux "dialog" of type radio list that will display the
#              available devices that can be selected
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#---------------------------------------------------------------------------------------
showDialogForDeviceSel() {
  local dialogBackTitle="$1"
  local deviceArray=()
  local attachedDevices=$(getAttachedDevices)
  local dialogTitle="Primary Device Selection"
  local textArray=(" " "$attachedDevices" "$DASH_LINE_WIDTH_80" " "
    "Select the device to be partitioned and where"
    "Arch Linux will be installed:"
  )
  local dialogText=$(getTextForDialog "${textArray[@]}")
  local -A dialogParms=(["dialogHeight"]=20 ["no-collapse"]=true)

  IFS=$'\n' read -d '' -r -a deviceArray <<< "$attachedDevices"
  if [ ${#deviceArray[@]} -gt 4 ]; then
    dialogParms["dialogHeight"]=30
  fi

  showListOfChoicesDialogBox "$DIALOG_BACK_TITLE" "$dialogTitle" "$dialogText" devices "radiolist" dialogParms
}

#---------------------------------------------------------------------------------------
#      METHOD:                  showYesNoSaveConfDialog
# DESCRIPTION: Show the linux "yes/no" dialog for confirmation to save the data and set
#              the response in the global variable "inputRetVal"
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)      dialogText - text to be displayed in the dialog before the menu options
#      3)     dialogTitle - Title for dialog
#      4)   addDialogOpts - options for the linux "dialog" command
#      5)   elinksChoices - array of choices to display in the linux "dialog"
#      6)      elinksOpts - associative array of options for the linux "dialog" command
#                           as well as the URLs that map to the "urlChoices"
#---------------------------------------------------------------------------------------
showYesNoSaveConfDialog() {
  local dialogBackTitle="$1"
  local dialogText="$2"
  local dialogTitle="$3"
  local -n addDialogOpts=$4
  local -n elinksChoices=$5
  local -n elinksOpts=$6
  local -A dialogOpts=(["yes-label"]="Save" ["no-label"]="Cancel")

  for key in "${!addDialogOpts[@]}"; do
    dialogOpts[$key]=$(echo "${addDialogOpts[$key]}")
  done

  if [ ${dialogOpts["help-text"]+_} ]; then
    dialogOpts["help-title"]=$(echo "Help:  $dialogTitle")
  fi

  while true; do
    exec 3>&1
      showYesNoBox "$dialogBackTitle" "$dialogText" "$dialogTitle" dialogOpts 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$inputRetVal"
    case ${fields[0]} in
      $DIALOG_OK)
        inputRetVal="YES"
        break
      ;;
      $DIALOG_CANCEL)
        inputRetVal="NO"
        break
      ;;
      $DIALOG_EXTRA)
        inputRetVal=$(echo "${fields[1]}")
        break
      ;;
      $DIALOG_HELP)
        if [ "${fields[1]}" == "View-URL" ]; then
          if [ ${#elinksChoices[@]} -gt 1 ]; then
            displayURLinElinks "$dialogBackTitle" elinksChoices elinksOpts
          else
            elinks "${elinksChoices[0]}"
          fi
        fi
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                  selectLinuxCapableBootLoader
# DESCRIPTION: Select the boot loader that will be used to boot up the system.   Set the
#              name of the boot loader in the global variable "mbSelVal"
#  Required Params:
#      1) dlgFuncParams - associative array of key/value pairs of parameters for the method
#                         (see selectBootLoader method)
#---------------------------------------------------------------------------------------
selectLinuxCapableBootLoader() {
  local -n dlgFuncParams=$1
  local dialogBackTitle="${dlgFuncParams[dialogBackTitle]}"
  local partLayout="${dlgFuncParams[partLayout]}"
  local skipConf=$(echo 'false' && return 1)
  local fields=()
  local urlRefChoices=("Boot Loaders" "File System")
  local -A urlDialogOpts=(["dialogHeight"]=20 ["dialogWidth"]=90
  ["Boot Loaders"]="https://wiki.archlinux.org/index.php/Category:Boot_loaders"
  ["File System"]="https://www.syslinux.org/wiki/index.php?title=Filesystem")

  if [ ${dlgFuncParams["skipConf"]+_} ]; then
    skipConf=$(echo 'true' && return 0)
  fi

  while true; do
    exec 3>&1
      selectBootLoader dlgFuncParams 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$mbSelVal"
    case ${fields[0]} in
      $DIALOG_OK)
        mbSelVal=$(echo "${fields[1]}")
        if ! ${skipConf} && [[ "$partLayout" =~ "LVM" ]] && [ "$mbSelVal" != "Grub2" ]; then
          confirmChoiceOfBL "$mbSelVal" "$dialogBackTitle"
          IFS=',' read -a fields <<< "$inputRetVal"
          inputRetVal=$(echo "${fields[1]}")
          if [ "$inputRetVal" == "YES" ]; then
            break
          else
            mbSelVal=""
          fi
        else
          break
        fi
      ;;
      $DIALOG_CANCEL)
        mbSelVal=""
        if [ ! ${dlgFuncParams["noCancelFlag"]+_} ]; then
          break
        fi
      ;;
      $DIALOG_HELP)
        if [ "${fields[1]}" == "View-URL" ]; then
          displayURLinElinks "$dialogBackTitle" urlRefChoices urlDialogOpts
        fi
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                       selectBootLoader
# DESCRIPTION: Select type of boot loader that will be used to boot up the system
#              within the linux "dialog" program using the radio list type of box
#  Required Params:
#      1) selFuncParams - associative array of key/value pairs of parameters for the method:
#                      "choices" - choices of names for the available boot loaders
#              "dialogBackTitle" - Specifies a backtitle string to be displayed on the
#                                  backdrop, at the top of the screen.
#                  "dialogTitle" - String to be displayed at the top of the dialog box
#                      "pttDesc" - partition table type
#                 "noCancelFlag" - NO Cancel button will be shown
#---------------------------------------------------------------------------------------
selectBootLoader() {
  local -n selFuncParams=$1
  local blChoices=()
  local dialogBackTitle="${selFuncParams[dialogBackTitle]}"
  local dialogTitle="${selFuncParams[dialogTitle]}"
  local pttDesc="${selFuncParams[pttDesc]}"
  local textArray=(
  "In order to boot Arch Linux, a Linux-capable boot loader must be installed"
  "(More...)" "$DASH_LINE_WIDTH_80" " "
  "Choose the boot loader available for the '$pttDesc':")
  local helpText=""

  if [ ${selFuncParams["dialogPartTable"]+_} ]; then
    helpText=$(getHelpTextForBL "$pttDesc" "${selFuncParams[dialogPartTable]}")
  else
    helpText=$(getHelpTextForBL "$pttDesc")
  fi

  local -A dialogParms=(["dialogHeight"]=20 ["help-label"]="More..." ["help-text"]="$helpText"
    ["help-title"]="Help:  $dialogTitle" ["helpURL"]=true)

  if [ ${selFuncParams["noCancelFlag"]+_} ]; then
    dialogParms["nocancel"]=true
  fi
  if [ ${selFuncParams["partTableStats"]+_} ]; then
    textArray=("${selFuncParams[partTableStats]}" "$DASH_LINE_WIDTH_80" " " "${textArray[@]}")
    dialogParms["dialogHeight"]=33
    dialogParms["helpHeight"]=33
    dialogParms["helpWidth"]=98
  fi
  local dialogText=$(getTextForDialog "${textArray[@]}")

  IFS=$'\t' read -a blChoices <<< "${selFuncParams["choices"]}"
  for opt in "${blChoices[@]}"; do
    dialogParms["$opt"]="${selFuncParams[$opt]}"
  done
  showListOfChoicesDialogBox "$dialogBackTitle" "$dialogTitle" "$dialogText" blChoices "radiolist" dialogParms
}

#---------------------------------------------------------------------------------------
#      METHOD:                     confirmChoiceOfBL
# DESCRIPTION: Confirm choice of boot loader when it is not Grub2 and the
#              partition layout =~ LVM.  Set "YES" or "NO" within the
#              global variable "inputRetVal"
#  Required Params:
#      1) name of Boot Loader
#      2) backtitle string to be displayed on the backdrop, at the top of the screen.
#---------------------------------------------------------------------------------------
confirmChoiceOfBL() {
  local dialogTitle="LVM DOES NOT support '$1'"
  local warnText=$(getTextForWarning)
  local textArray=("$warnText"
  "'/boot' cannot reside in a Logical Volume when using a boot loader"
  "which does not support LVM; you must create a separate /boot partition"
  "and format it directly. Only 'GRUB' (i.e Grub2) is known to support LVM."
  "$DASH_LINE_WIDTH_80"
  " "
  "Do you want to continue?"
  )
  local dialogText=$(getTextForDialog "${textArray[@]}")
  local -A cmdOpts=(["yes-label"]="Continue" ["no-label"]="Cancel")

  showYesNoBox "$2" "$dialogText" "$dialogTitle" cmdOpts
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                       getTextForError
# DESCRIPTION: Get the text to display in the dialog to signify an error message.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getTextForError() {
  local dashBorder="----------------------------------"
  local textArray=("$dashBorder" "          ERROR!!!!!!!" "$dashBorder")
  local text=$(getTextForDialog "${textArray[@]}")

  echo "$text"
}

#---------------------------------------------------------------------------------------
#      METHOD:                        setELinksURLs
# DESCRIPTION: Set the URLs that will be able to be viewed in the ELinks web browser
#  Required Params:
#      1)   helpText - string containing the URLs within the text to be displayed
#                      in the help dialog
#---------------------------------------------------------------------------------------
setELinksURLs() {
  local helpText="$1"
  local pos=0
  local textArray=()
  local lastIdx=0

  IFS=$'\n' read -d '' -a textArray <<< "$helpText"
  for line in "${textArray[@]}"; do
    if [[ "$line" =~ "http" ]]; then
      if [[ "$line" =~ "systemd" ]] || [[ "$line" =~ "itsfoss" ]]; then
        urls+=($(echo "${line}"))
      else
        pos=$(expr index "$line" -)
        if [ ${pos} -gt 0 ]; then
          pos=$(expr ${pos} + 1)
        fi
        urls+=($(echo "${line:${pos}}"))
      fi
    fi
  done
  if [[ -v urlRefChoices ]] && [ ${#urls[@]} -gt 0 ]; then
    lastIdx=$(expr ${#urls[@]} - 1)
    for idx in $(eval echo "{0..${lastIdx}}"); do
      urlDialogOpts["${urlRefChoices[${idx}]}"]=$(echo "- ${urls[${idx}]}")
    done
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                       showErrorDialog
# DESCRIPTION: Displays the appropriate error message
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)     dialogTitle - String to be displayed at the top of the dialog box.
#      3)        errorMsg - the error message to display
#
#---------------------------------------------------------------------------------------
showErrorDialog() {
  local dialogBackTitle="$1"
  local dialogTitle=$(echo "Error:  $2")
  local errTxt=$(getTextForError)

  local msgTextArray=("$errTxt" " " "$3" "$DASH_LINE_WIDTH_80")

  local msgText=$(getTextForDialog "${msgTextArray[@]}")

  $DIALOG --clear --no-collapse --backtitle "$1" --title "$dialogTitle" --msgbox "$msgText" 25 80 3>&1 1>&2 2>&3
}

#---------------------------------------------------------------------------------------
#      METHOD:                     showProgBarAndTail
# DESCRIPTION: Split the terminal to show a linux "dialog" box of type gauge
#              (i.e. progress bar) in the top half and the tail of the log file in the
#              bottom half.  This is to display the progress of the AUR packages getting
#              installed and/or the commands getting executed.
#---------------------------------------------------------------------------------------
showProgBarAndTail() {
  screen -S aalp-gygi -c "${DATA_DIR}/dialog-split-screenrc"
}
