#!/bin/bash
#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# functions/methods
#===============================================================================

#---------------------------------------------------------------------------------------
#      METHOD:                    selectRowNumOfBootDevice
# DESCRIPTION: Select the row number that maps to the /boot logical volume or partition
#              and set the value in the global variable "mbSelVal"
#  Required Params:
#      1) assocParamArray - associative array of key/value pairs of parameters for the method
#                 "choices" - contains the rows of logical volumes and partitions that
#                             could be the /boot device
#         "dialogBackTitle" - Specifies a backtitle string to be displayed on the
#                             backdrop, at the top of the screen.
#             "dialogTitle" - String to be displayed at the top of the dialog box.
#         "dialogPartTable" - string containing the rows of data for the
#                             partition table in a textual table format
#          "partTableStats" - summary table of the progress in the partitioning process
#                             in a textual table format
#---------------------------------------------------------------------------------------
selectRowNumOfBootDevice() {
  local -n assocParamArray=$1
  local menuOptions=()
  local urls=()
  local dialogText=$(getTextForSelBootDev "${assocParamArray[partLayout]}" "${assocParamArray[partTableStats]}")
  local dialogTitle="${assocParamArray[dialogTitle]}"
  local helpText=$(getHelpTextForBootDevice "${assocParamArray[dialogPartTable]}")
  local -A dialogParms=(["cancel-label"]="Skip" ["dialogHeight"]=33 ["dialogTitle"]="$dialogTitle"
    ["help-button"]=true ["helpHeight"]=30 ["helpWidth"]=98 ["help-text"]="$helpText"
    ["help-title"]="Help:  $dialogTitle" ["no-collapse"]=true)

  setRefLinks "$helpText" urls

  IFS=$'\t' read -a menuOptions <<< "${assocParamArray[choices]}"

  for opt in "${menuOptions[@]}"; do
    dialogParms["$opt"]="${assocParamArray[$opt]}"
  done
  while true; do
    exec 3>&1
    showListOfChoicesDialogBox "${assocParamArray[dialogBackTitle]}" "$dialogTitle" "$dialogText" \
        menuOptions "radiolist" dialogParms 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$mbSelVal"
    case ${fields[0]} in
      $DIALOG_OK)
        mbSelVal=$(echo "${fields[1]}")
        break
      ;;
      $DIALOG_CANCEL)
        mbSelVal=""
        break
      ;;
      $DIALOG_HELP)
        if [ "${fields[1]}" == "View-URL" ]; then
          elinks "${urls[0]}"
        fi
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getTextForSelBootDev
# DESCRIPTION: Get the text to display in a linux "dialog" command when selecting
#              the /boot device
#      RETURN: concatenated string
#  Required Params:
#      1)      partLayout - the partition layout/scheme
#      2) partTableStats" - summary table of the progress in the partitioning process
#                           in a textual table format
#---------------------------------------------------------------------------------------
function getTextForSelBootDev() {
  local partLayout="$1"
  local deviceType="Partition"

  if [[ "$partLayout" =~ "LVM" ]]; then
    deviceType="Logical Volume or Partition"
  fi

  local textArray=("$2" "$DIALOG_BORDER" " "
  "The /boot directory contains the kernel and ramdisk images as well as the"
  "bootloader configuration file and bootloader stages."
  " " "Select < Help > to view the partition table"
  "$DIALOG_BORDER" " " "Select the '/boot' $deviceType:")
  local dialogText=$(getTextForDialog "${textArray[@]}")
  echo "$dialogText"
}

#---------------------------------------------------------------------------------------
#      METHOD:                    selectFileSystemType
# DESCRIPTION: Get the file system type and set the selected value within
#              the global variable "mbSelVal"
#  Required Params:
#      1) assocParamArray - associative array of key/value pairs of parameters for the method
#                 "choices" - string of the options for the file system types that can
#                             be selected and separated by '\t'
#         "dialogBackTitle" - Specifies a backtitle string to be displayed on the
#                             backdrop, at the top of the screen.
#              "dialogText" - String to be displayed inside the dialog box on top of
#                             the menu of choices.
#             "dialogTitle" - String to be displayed at the top of the dialog box.
#         "dialogPartTable" - string containing the rows of data for the
#                             partition table in a textual table format
#          "partTableStats" - summary table of the progress in the partitioning process
#                             in a textual table format
#                  "urlRef" - the URL that can be referenced for further documentation
#---------------------------------------------------------------------------------------
selectFileSystemType() {
  local -n assocArray=$1
  local urlRef=$(echo "${assocArray[urlRef]}")
  local menuChoices=()
  local dialogTitle=$(echo "${assocArray[dialogTitle]}")
  local helpText=$(getHelpTextForFST assocArray)
  local -A dialogParms=(["defaultChoice"]="ext4" ["dialogHeight"]=33 ["no-collapse"]=true ["help-button"]=true
  ["helpHeight"]=30 ["helpWidth"]=98 ["help-text"]="$helpText" ["help-title"]="Help:  $dialogTitle"
  ["helpURL"]=true)

  if [[ "$dialogTitle" =~ "systemd" ]]; then
    unset dialogParms["defaultChoice"]
  fi

  IFS=$'\t' read -d '' -a menuChoices <<< "${assocArray[choices]}"
  menuChoices[-1]=$(trimString "${menuChoices[-1]}")
  for fsKey in "${menuChoices[@]}"; do
    dialogParms["$fsKey"]=$(echo "${FILE_SYSTEMS_DESCS[$fsKey]}")
  done

  while true; do
    exec 3>&1
      showListOfChoicesDialogBox "${assocArray[dialogBackTitle]}" "$dialogTitle" "${assocArray[dialogText]}" \
          menuChoices "radiolist" dialogParms 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$mbSelVal"
    case ${fields[0]} in
      $DIALOG_OK)
        mbSelVal=$(echo "${fields[1]}")
        break
      ;;
      $DIALOG_CANCEL)
        mbSelVal=""
        break
      ;;
      $DIALOG_HELP)
        if [ "${fields[1]}" == "View-URL" ]; then
          elinks "${assocArray[urlRef]}"
        fi
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                   selectDeviceToAssignFST
# DESCRIPTION: Select the row numbers of the devices to set the file system types for
#
#      1) assocParamArray - associative array of key/value pairs of parameters for the method
#                 "choices" - string of the options for the file system types that can
#                             be selected and separated by '\t'
#         "dialogBackTitle" - Specifies a backtitle string to be displayed on the
#                             backdrop, at the top of the screen.
#             "dialogTitle" - String to be displayed at the top of the dialog box.
#         "dialogPartTable" - string containing the rows of data for the
#                             partition table in a textual table format
#          "partTableStats" - summary table of the progress in the partitioning process
#                             in a textual table format
#              "statsTable" - statistics of the progress in the formatting process
#---------------------------------------------------------------------------------------
selectDeviceToAssignFST() {
  local -n assocArray=$1
  local menuOptions=()
  local helpText=$(getHelpTextForDevFST "${assocArray[dialogPartTable]}")
  local dialogTitle="${assocArray[dialogTitle]}"
  local -A dialogParms=(["dialogHeight"]=33 ["no-collapse"]=true ["helpHeight"]=30 ["extra-label"]="Reset"
      ["helpWidth"]=98 ["help-button"]=true ["help-text"]="$helpText" ["helpURL"]=true
      ["help-title"]="Help: $dialogTitle" ["output-separator"]="$FORM_FLD_SEP")
  local dialogText=$(getTextForCheckList "${assocArray[statsTable]}" "${assocArray[partTableStats]}")
  local urls=()
  local rows=()

  setRefLinks "$helpText" urls

  IFS=$'\t' read -d '' -a menuOptions <<< "${assocArray["choices"]}"
  menuOptions[-1]=$(trimString "${menuOptions[-1]}")
  for opt in "${menuOptions[@]}"; do
    dialogParms["$opt"]=$(echo "${assocArray[$opt]}")
  done

  while true; do
    exec 3>&1
      showListOfChoicesDialogBox "${assocArray[dialogBackTitle]}" "$dialogTitle" "$dialogText" \
        menuOptions "checklist" dialogParms 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$mbSelVal"
    case ${fields[0]} in
      $DIALOG_OK)
        mbSelVal=$(echo "${fields[1]//\"/}")
        if [ ${#mbSelVal} -gt 0 ]; then
          IFS=$'\t' read -d '' -a fields <<< "${mbSelVal//$FORM_FLD_SEP/$'\t'}"
          case "${fields[0]}" in
            "All")
              mbSelVal="${fields[0]}"
            ;;
            "Format"|"ROOT")
              mbSelVal="${fields[0]}"
            ;;
            *)
              mbSelVal=$(printf "\t%s" "${fields[@]}")
              mbSelVal=${mbSelVal:1}
            ;;
          esac
          break
        else
          showWarningDialog
        fi
      ;;
      $DIALOG_CANCEL)
        mbSelVal=""
        break
      ;;
      $DIALOG_EXTRA)
        mbSelVal="Reset"
        break
      ;;
      $DIALOG_HELP)
        if [ "${fields[1]}" == "View-URL" ]; then
          elinks "${urls[0]}"
        fi
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getTextForCheckList
# DESCRIPTION: Text to display on the linux "dialog" box of type check list
#              when selecting the devices to assign a file system to
#      RETURN: concatenated string
#  Required Params:
#      1)     statsTbl - statistics of the progress made in tabular format
#      2) partTblStats - statistical summary of the partition table
#---------------------------------------------------------------------------------------
function getTextForCheckList() {
  local deviceType="Partition"
  if [[ "${varMap[PART-LAYOUT]}" =~ "LVM" ]]; then
    deviceType="Logical Volume"
  fi
  local textArray=("Select the ${deviceType}s to assign a"
    "file system type to be formatted with:")
  local concatStr=$(printf " %s" "${textArray[@]}")
  concatStr=${concatStr:1}

  textArray=("$2" " " "$1" " " "$concatStr")

  local dialogText=$(getTextForDialog "${textArray[@]}")

  echo "$dialogText"
}

#---------------------------------------------------------------------------------------
#      METHOD:                 showWarningDialog
# DESCRIPTION: Show a warning dialog when no logical volumes or partitions were selected
#              and the Ok button was clicked.
#---------------------------------------------------------------------------------------
showWarningDialog() {
  local deviceType="Partition"
  if [[ "${varMap[PART-LAYOUT]}" =~ "LVM" ]]; then
    deviceType="Logical Volume"
  fi
  local warnText=$(getTextForWarning)
  local msgTextArray=("$warnText" "No '${deviceType}s' were selected."
  "-----------------------------------" "Either:"
  "   1) Select at least one '${deviceType}' before clicking the <  OK  > button"
  "   2) Select the 'All' option if you want to set them all to the same file system type"
  )
  local msgText=$(getTextForDialog "${msgTextArray[@]}")

  $DIALOG --clear  --no-collapse --title "WARNING!!!!!!!" --msgbox "$msgText" 25 80 3>&1 1>&2 2>&3
}

#---------------------------------------------------------------------------------------
#      METHOD:                   dispUnassignedMsg
# DESCRIPTION: Display a linux "dialog" of type message box when there are required
#              logical volumes and/or partitions that do not have a file system
#  Required Params:
#      1)  fstMPT - "file system type" or "mount point"
#      2) colName - name of a column in the partition table representing the "$fstMPT"
#---------------------------------------------------------------------------------------
dispUnassignedMsg(){
  local fstMPT="$1"
  local colName="$2"
  local dialogPartTable="${methodParams["dialogPartTable"]}"
  local statsTbl="${methodParams["statsTable"]}"
  local unassigned=$(getNumOfUnassigned "$statsTbl")
  local dialogText=""
  local reqText=""
  if [ ${unassigned} -gt 1 ]; then
    dialogText=$(echo "There are $unassigned '${methodParams["deviceType"]}s'")
    reqText="require"
  else
    dialogText=$(echo "There is $unassigned '${methodParams["deviceType"]}'")
    reqText="requires"
  fi
  local textArray=("$dialogText that $reqText a $fstMPT.  Scroll through the partition table below"
    "(using the 'j' and 'k' keys) to see which '${methodParams["deviceType"]}' $reqText a $fstMPT to"
    "be assigned. These have an '*' within their '$colName' column.")
  local concatStr=$(printf " %s" "${textArray[@]}")
  concatStr=${concatStr:1}
  textArray=("$statsTbl" " " "$concatStr" "$DIALOG_BORDER_MAX" " "
    "$dialogPartTable" " " "Click <Ok> to continue!")

  local msgBoxTxt=$(getTextForDialog "${textArray[@]}")
  $DIALOG --clear --no-collapse --backtitle "$DIALOG_BACK_TITLE" --title "Unassigned '${methodParams["deviceType"]}'" \
    --msgbox "$msgBoxTxt" 30 98 3>&1 1>&2 2>&3
}

#---------------------------------------------------------------------------------------
#      METHOD:                      getConfToExecCmds
# DESCRIPTION: Get confirmation of whether or not to either:
#                 A)  format the devices with their selected file system type
#                 B)  mount the devices to their specified directory
#              Set the value chosen in the global variable "inputRetVal"
#---------------------------------------------------------------------------------------
getConfToExecCmds() {
  local statsTbl="${methodParams["statsTable"]}"
  local dialogTitle=""
  local helpText=""
  local textArray=("$statsTbl" " "
    "Select < Help > to see all rows of the partition table and"
    "description of options." "$DIALOG_BORDER" " ")
  local deviceType="Partitions"
  local urlRefChoices=()
  local -A urlDialogOpts=()

  if [[ "${varMap[PART-LAYOUT]}" =~ "LVM" ]]; then
    deviceType="Logical Volumes & Partitions"
  fi

  if [[ "$DIALOG_BACK_TITLE" =~ "Mount" ]]; then
    dialogTitle="Confirmation to 'Mount' $deviceType"
    helpText=$(getHelpTextForConf linuxMountCmds "${methodParams["dialogPartTable"]}")
    textArray+=("Mount the $deviceType to their specified directory?")
    urlRefChoices=("https://wiki.archlinux.org/index.php/file_systems#Mount_a_file_system")
  else
    dialogTitle="Confirmation to 'Format' $deviceType"
    helpText=$(getHelpTextForConf linuxFmtCmds "${methodParams["dialogPartTable"]}")
    textArray+=("Format the $deviceType with their assigned file system type?")
    urlRefChoices=("https://wiki.archlinux.org/index.php/file_systems")
  fi

  local dialogText=$(getTextForDialog "${textArray[@]}")

  local -A addOpts=(["yes-label"]="Confirm" ["no-label"]="Edit" ["dialogHeight"]=20 ["helpHeight"]=30 ["helpWidth"]=98
    ["help-button"]=true ["extra-label"]="Exit" ["help-text"]="$helpText" ["helpURL"]=true)

  showYesNoSaveConfDialog "$DIALOG_BACK_TITLE" "$dialogText" "$dialogTitle" addOpts urlRefChoices urlDialogOpts
}

#---------------------------------------------------------------------------------------
#      METHOD:                   selectDeviceToAssignMPT
# DESCRIPTION: Select the logical volume or partition to mount to a specified directory
#---------------------------------------------------------------------------------------
selectDeviceToAssignMPT() {
  local menuOptions=()
  local helpText=$(getHelpTextForDevMPT "${methodParams[dialogPartTable]}")
  local dialogTitle="${methodParams[dialogTitle]}"
  local -A dialogParms=(["dialogHeight"]=33 ["no-collapse"]=true ["helpHeight"]=30 ["extra-label"]="Reset"
      ["helpWidth"]=98 ["help-button"]=true ["help-text"]="$helpText" ["helpURL"]=true
      ["help-title"]="Help: $dialogTitle")
  local dialogText=$(getTextForMountDialog "${methodParams[statsTable]}" "${methodParams[partTableStats]}")
  local urls=()
  local rows=()

  setRefLinks "$helpText" urls

  IFS=$'\t' read -d '' -a menuOptions <<< "${methodParams["choices"]}"
  menuOptions[-1]=$(trimString "${menuOptions[-1]}")
  for opt in "${menuOptions[@]}"; do
    dialogParms["$opt"]=$(echo "${methodParams[$opt]}")
  done

  while true; do
    exec 3>&1
      showListOfChoicesDialogBox "${methodParams[dialogBackTitle]}" "$dialogTitle" "$dialogText" \
        menuOptions "radiolist" dialogParms 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$mbSelVal"
    case ${fields[0]} in
      $DIALOG_OK)
        mbSelVal=$(echo "${fields[1]//\"/}")
        if [ ${#mbSelVal} -gt 0 ]; then
          IFS=$'\t' read -d '' -a fields <<< "${mbSelVal//$FORM_FLD_SEP/$'\t'}"
          case "${fields[0]}" in
            "All")
              mbSelVal="${fields[0]}"
            ;;
            "Format"|"ROOT")
              mbSelVal="${fields[0]}"
            ;;
            *)
              mbSelVal=$(printf "\t%s" "${fields[@]}")
              mbSelVal=${mbSelVal:1}
            ;;
          esac
          break
        else
          showWarningDialog
        fi
      ;;
      $DIALOG_CANCEL)
        mbSelVal="Exit"
        break
      ;;
      $DIALOG_EXTRA)
        mbSelVal="Reset"
        break
      ;;
      $DIALOG_HELP)
        if [ "${fields[1]}" == "View-URL" ]; then
          elinks "${urls[0]}"
        fi
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getTextForMountDialog
# DESCRIPTION: Text to display on the linux "dialog" box of type radio list
#              when selecting a logical volume or partition to mount to a specified directory
#      RETURN: concatenated string
#  Required Params:
#      1)     statsTbl - statistics of the progress made in tabular format
#      2) partTblStats - statistical summary of the partition table
#---------------------------------------------------------------------------------------
function getTextForMountDialog() {
  local deviceType="Partition"
  if [[ "${varMap[PART-LAYOUT]}" =~ "LVM" ]]; then
    deviceType="Logical Volume"
  fi
  local textArray=("Select the ${deviceType} to mount to a"
    "specified directory:")
  local concatStr=$(printf " %s" "${textArray[@]}")
  concatStr=${concatStr:1}

  textArray=("$2" " " "$1" " " "$concatStr")

  local dialogText=$(getTextForDialog "${textArray[@]}")

  echo "$dialogText"
}

#-----------------------------------------------------------------------------------------------
#      METHOD:                            getDirForMPT
# DESCRIPTION: Get the name of a directory for the mount pt of a logical volume or partition.
#              Set the value that was entered in the global variable "ibEnteredText"
#  Required Params:
#      1) aryIdx - the index within the global array 'PART_TABLE_ARRAY'
#-----------------------------------------------------------------------------------------------
getDirForMPT() {
  local aryIdx=$1
  local deviceName=$(getDeviceName ${aryIdx})
  local fields=()

  ibEnteredText=""
  while true; do
    getValidatedMountPt "${methodParams[dialogPartTable]}" "$deviceName"
    IFS=',' read -a fields <<< "$ibEnteredText"
    case ${fields[0]} in
      $DIALOG_OK)
        ibEnteredText=$(echo "${fields[1]}")
        break
      ;;
      $DIALOG_CANCEL)
        ibEnteredText=""
        break
      ;;
      *)
        ibEnteredText=""
        dispDirNameErrorMsg "${fields[0]}" "${fields[1]}" "${methodParams[dialogPartTable]}"
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                getValidatedDeviceName
# DESCRIPTION: Uses a "dialog" inputbox to get the name of a directory for the
#              mount pt and validates that the directory:
#                   A) is NOT /etc or /usr
#                   B) is NOT a duplicate
#              Sets the value that was entered in the global variable "ibEnteredText"
#  Required Params:
#      1) dialogPartTable - string containing the rows of data for the partition table
#      2) deviceName - name of a Logical Volume or Partition
#---------------------------------------------------------------------------------------
getValidatedMountPt() {
  local dialogPartTable="$1"
  local deviceName="$2"
  local dialogText=$(getTextForInputBox "$deviceName")
  local dialogTitle=$(echo "Directory Name Entry Dialog")
  local -A dialogOpts=(["dialogHeight"]=20)
  local fields=()
  local isValid=$(echo 'false' && return 1)
  local isDuplicate=$(echo 'false' && return 1)

  dialogOpts["initValue"]=$(getInitialValue "$deviceName")

  exec 3>&1
    showDialogInputBox "$DIALOG_BACK_TITLE" "$dialogText" "$dialogTitle" dialogOpts 2>&1 1>&3
  exec 3>&-
  IFS=',' read -a fields <<< "$ibEnteredText"
  case ${fields[0]} in
    $DIALOG_OK)
      ibEnteredText=$(trimString "${fields[1]}")
      isValid=$(isValidDirName)
      isDuplicate=$(isDuplicateDirName)

      if ! ${isValid}; then
        ibEnteredText=$(echo "$DIALOG_INPUT_ERR0R,$ibEnteredText")
      elif ${isDuplicate}; then
        ibEnteredText=$(echo "$DIALOG_VALUE_ERR0R,$ibEnteredText")
      else
        ibEnteredText=$(echo "$DIALOG_OK,$ibEnteredText")
      fi
    ;;
    $DIALOG_CANCEL)
      ibEnteredText=$(echo "$DIALOG_CANCEL,${fields[1]}")
    ;;
  esac
}

#---------------------------------------------------------------------------------------
#    FUNCTION:              getDialogTextForInputBox
# DESCRIPTION: Get the text to display within a linux "dialog" input box
#      RETURN: Concatenated string
#  Required Params:
#      1) deviceName - name of a Logical Volume or Partition
#---------------------------------------------------------------------------------------
function getTextForInputBox() {
  local deviceName="$1"
  local invalidNames=("etc" "usr")
  local deviceType="Partition"
  local prefix="${deviceName:0:4}"

  if [ "$prefix" == "lvm-" ]; then
    deviceType="Logical Volume"
  fi

  prefix="/"

  local textArray=(
  "NOTE:  If the directory name does NOT begin with a '${prefix}', then the installer"
  "       will append the name to it (i.e. nexus-oss would become ${prefix}nexus-oss)"
  "$DIALOG_BORDER" " "
  "NOTE:  The following directories are essential for booting and must be on"
  "       the same partition as '/' or root:"
  "       #  ${prefix}etc"
  "       #  ${prefix}usr"
  "$DIALOG_BORDER" " "
  "Enter the name of the directory for the $deviceType '$deviceName'"
  "[ex: ${prefix}dbhome, ${prefix}nexus-oss]:")

  local dialogText=$(getTextForDialog "${textArray[@]}")

  echo "$dialogText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getInitialValue
# DESCRIPTION: Get the initial value of the directory name to display within a
#              linux "dialog" input box
#      RETURN: string prefixed with '/'
#  Required Params:
#      1) deviceName - name of a Logical Volume or Partition
#---------------------------------------------------------------------------------------
function getInitialValue() {
  local deviceName="$1"
  local nameAry=()
  local initVal=""
  IFS=$':' read -a nameAry <<< "$deviceName"

  if [ ${#nameAry[@]} -gt 1 ]; then
    nameAry=(${nameAry[1]})
    if [ "${nameAry[0]:0:1}" == "/" ]; then
      initVal=$(echo "${nameAry[0]}")
    else
      initVal=$(echo "/${nameAry[0]}")
    fi
  else
    nameAry=(${deviceName:4})
    initVal=$(echo "/${nameAry[0]}")
  fi

  echo "$initVal"
}

#---------------------------------------------------------------------------------------
#      METHOD:                dispDirNameErrorMsg
# DESCRIPTION: Displays within a "dialog" message box that the entered directory name
#              is either a duplicate or an essential directory.
#  Required Params:
#      1)       errorCode - $DIALOG_INPUT_ERR0R:  reserved name
#                           $DIALOG_VALUE_ERR0R:  duplicate
#      2)        inputVal - the directory/mount pt. that was entered
#      3) dialogPartTable - string containing the rows of data for the partition table
#---------------------------------------------------------------------------------------
dispDirNameErrorMsg() {
  local errorCode=$(echo "$1")
  local inputVal=$(echo "$2")
  local dialogPartTable="$3"
  local errorMsg=""
  local msgTextArray=("ERROR!!!" "--------")

  case ${errorCode} in
    ${DIALOG_INPUT_ERR0R})
      msgTextArray+=(
      "     Except for the /boot directory, the '$inputVal' directory is essential for booting"
      "     and must be on the same partition as '/' or root.")
    ;;
    ${DIALOG_VALUE_ERR0R})
      msgTextArray+=(
      "     The '$inputVal' directory already exists as a mount point within the partition table."
      "     (see below)")
    ;;
  esac
  msgTextArray+=("$DIALOG_BORDER_MAX" " " "----------------------------------------------------------"
    "           Use 'j' and 'k' keys to scroll"
    "----------------------------------------------------------" " "
    "$dialogPartTable" " " "You must hit OK to continue & re-enter a valid value")

  local msgText=$(getTextForDialog "${msgTextArray[@]}")

  $DIALOG --clear --no-collapse --title "Invalid Directory Name" --msgbox "$msgText" 30 98 3>&1 1>&2 2>&3
}
