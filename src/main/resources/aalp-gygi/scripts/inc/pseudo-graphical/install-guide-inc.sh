#!/bin/bash
#set -e

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# functions/methods
#===============================================================================

#---------------------------------------------------------------------------------------
#      METHOD:                     dispWelcome
# DESCRIPTION: Displays the "Welcome" linux "dialog" box of type message box
#---------------------------------------------------------------------------------------
dispWelcome() {
  local msgText=$(getWelcomeText)

  $DIALOG --clear --no-collapse --backtitle "Airwolf Arch Linux Pseudo-Graphical & YAD Graphical Installer" --title "Welcome (Use 'j' and 'k' keys to scroll)" \
  --msgbox "$msgText" 35 98 3>&1 1>&2 2>&3
  dialogCmdRetVal=$?
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                       getWelcomeText
# DESCRIPTION: Get the text to display on the Welcome screen
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getWelcomeText() {
  local msgTextArray=(
  " "
  "                                .:dOko'      '        ,,,xxxxxxxxxxxxxxxxxxddddddddddddddd"
  "                .        ..    xWWNNNNWX'  ..'.       .. 'ololooloolloololoooooooooooooooo"
  "              .''        .;oOc;0XNNNNNXKc;Od;.        ,''. .c:c::::cc:c:,,;ccccccccccccccc"
  "             ..,OO;.     ....';clkOOk0d::;...'.    .:kKd''. .::::::::c:,'',;::cccccccccccc"
  "            '.,dkkO0Odl::;....;OoKO0kKxxx'....:odxOOkkxxo''. .llllllc:;'''',:;ccllllllllll"
  "           .''oxdxxxddOx:......:XddK0oOk.......:xdooodddxl.'.  loooo:,;,,'''';;coooooooooo"
  "           ..:d:;'''...';.....,..:k.;x..''..'.':.......''c''.  llcc;'''..''''';::lllllllll"
  "           '...........:;.....,'.':ll;'.:;.'...;:'..........'  llc,'''''''''''',;;clllcllc"
  "           '..........,,'...,xKKcxo .xdlK0x....'''...........  ::,'''''''',''''',;;:cccccc"
  "           ........    ,.',.l;':,:o,;l;':'cc';.':    ......'. .;'''''''cl;ll;''''';;:cc:::"
  "            ......     00l....0l,,'..';,lO'...xWO     ...... l;,''''.,xXx::xx:''''';;;c;;;"
  "             .....     dWNWk..c'. .... ',:'.0WWWc     .... .:,''''''.:NNxocdOx'''''.'',;;;"
  "                ..      xWNMx...''',''''...OWWWo      .. .c:,''''''',cxxxookKk;,''..''''c:"
  "                         'kNWXc;,.''''.':cXWNx.        'll,''',;codxdxkkOOOOOdddoc:;;'';,,"
  "                           dWWK','.......kWWk         'ol,,;coxkO0OxddOOOkxxxddxxoollod::;"
  "                            OMWo        cNMO.         ddlldxO0XX0kxxxxkOkkdddddxxkkk00dolc"
  "                             lXNKxooooxKNKl           xxO0KXXXNXOxxxxxkOOkdddddxkkkOXNNK0O"
  "$DASH_LINE_FULL_WIDTH" " "
  "                              Welcome to the Airwolf Arch Linux"
  "                                       Pseudo-Graphical"
  "                                             And"
  "                                   YAD Graphical Installer" " "
  "               https://bitbucket.org/kooshballtb/spring-aalp-gygi-iso/src/master"
  "$DASH_LINE_FULL_WIDTH" " "
  "The purpose of this installer is to simplify the process of installing and configuring "
  "Arch Linux in order to create a killer customized installation."
  "$DASH_LINE_FULL_WIDTH" " "
  "Requirements for installing Arch Linux:"
  "     - A x86_64 (i.e. 64 bit) compatible machine"
  "     - Minimum 512 MB of RAM (recommended 2 GB)"
  "     - At least 1 GB of free disk space (recommended 20 GB for basic usage)"
  "     - An active internet connection"
  "     - Familiarity with the Linux command line"
  "     - The following binaries/AUR packages installed within the ISO:"
  )

  for binary in "${REQ_BINARIES[@]}"; do
    binary=$(printf "%7s%s  '%s':  %s" " " "$LVM_DEVICE_PREFIX" "$binary" "${BINARY_DESCS["$binary"]}")
    msgTextArray+=("$binary")
  done

  msgTextArray+=("$DASH_LINE_FULL_WIDTH" " "
  "Select <  OK  > to continue to the installation guide."
  "Hit the ESC key to cancel.")

  local msgText=$(getTextForDialog "${msgTextArray[@]}")

  echo "$msgText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getInstallationSteps
# DESCRIPTION: Get the installation steps to execute/run
#      RETURN: step #s in the global "mbSelVal" variable
#---------------------------------------------------------------------------------------
getInstallationSteps() {
  local installSteps=()
  local -A assocParamAry=()
  local instChecklist=$(getInstallGuideChecklist)
  local partLayoutText=""
  local dialogText=""
  local extraLabel="View CHKL"

  if [ ${INST_GUIDE_CHECKLIST[0]} -gt 0 ]; then
    partLayoutText=$(getTextForPartLayoutDlg)
    dialogText=$(getTextForViewOptsDlg)
    extraLabel="View Opts"
  fi

  setStepsForInstallGuideChecklist

  while true; do
    exec 3>&1
      selectInstallationSteps "$extraLabel" 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$mbSelVal"
    case ${fields[0]} in
      $DIALOG_OK)
        mbSelVal=$(echo "${fields[1]//\"/}")
        break
      ;;
      $DIALOG_CANCEL)
        mbSelVal=""
        break
      ;;
      $DIALOG_EXTRA)
        showOptionsDialog "$instChecklist" "$partLayoutText" "$dialogText"
      ;;
      $DIALOG_HELP)
        if [ "${fields[1]}" == "View-URL" ]; then
          elinks "https://wiki.archlinux.org/index.php/installation_guide"
        fi
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getTextForPartLayoutDlg
# DESCRIPTION: Get the text to display in a linux "dialog" command of type "msgbox"
#              to display the partition layout/scheme
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getTextForPartLayoutDlg() {
  local -A assocParamAry=()
  local dialogPartTable=""
  local startRow=1
  local tableRows=()

  setDataForPartitionScheme assocParamAry
  dialogPartTable="${assocParamAry["dialogPartTable"]}"
  if [[ "$dialogPartTable" =~ "Volume Group" ]]; then
    startRow=$(expr $startRow + 1)
  fi

  readarray -t tableRows <<< "$dialogPartTable"
  dialogPartTable=$(printf "\n%s" "${tableRows[@]:${startRow}}")
  dialogPartTable=${dialogPartTable:1}

  tableRows=("----------------------------------------------------------"
    "           Use 'j' and 'k' keys to scroll"
    "----------------------------------------------------------" " "
    "${assocParamAry["partTableStats"]}" " " "$dialogPartTable")
  local dialogText=$(getTextForDialog "${tableRows[@]}")
  echo "$dialogText" | sed 's/^--+/+/g'
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getTextForViewOptsDlg
# DESCRIPTION: Get the text to display in a linux "dialog" command of type "msgbox"
#              to display the options to view either the partition layout/scheme or
#              installation guid checklist
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getTextForViewOptsDlg() {
  local dlgTxtArray=("Select:" "-------")
  local textArray=("<Part. Tbl> to view all the rows configured for"
  "the Partition Layout/Scheme")
  local concatStr=$(printf " %s" "${textArray[@]}")

  dlgTxtArray+=("      #  ${concatStr:1}"
    "      #  <  Exit   > to return to the previous screen")

  textArray=("<Checklist> to view the steps of the Installation Guide that have"
    "been completed to install Arch Linux.  It for the most part follows the same steps"
    "as the official guide on the Arch Linux site.  The differences between this and"
    "the official guide are:")
  concatStr=$(printf " %s" "${textArray[@]}")
  dlgTxtArray+=("      #  ${concatStr:1}")
  concatStr=$(printf "%11sA)  " " ")
  dlgTxtArray+=("$concatStr")
  concatStr=$(printf "%11sB)  " " ")
  dlgTxtArray+=("$concatStr")

  textArray=("The 'Select Editor' task was added to the installation"
    "step 'Pre-Installation'.")
  concatStr=$(printf " %s" "${textArray[@]}")
  dlgTxtArray[-2]+="${concatStr:1}"

  textArray=("The 'Chroot' task was was removed from the installation"
    "step 'Configure the system' as this is done automatically"
    "by the installer.")
  concatStr=$(printf " %s" "${textArray[@]}")
  dlgTxtArray[-1]+="${concatStr:1}"

  local dialogText=$(getTextForDialog "${dlgTxtArray[@]}")
  echo "$dialogText"
}

#---------------------------------------------------------------------------------------
#      METHOD:            setStepsForInstallGuideChecklist
# DESCRIPTION: Set the choices of the steps and tasks that will displayed in the
#              linux "dialog" box of type check list
#---------------------------------------------------------------------------------------
setStepsForInstallGuideChecklist() {
  local instStepNum=0
  local numInstSteps=$(expr ${#stepTotals[@]} - 1)
  local stepArray=()
  local instStepCat=""
  local stepDesc=""
  local doneFlag=$(echo 'false' && return 1)

  for instStepNum in $(eval echo "{1..${numInstSteps}}"); do
    doneFlag=$(isInstStepDone ${instStepNum})
    if ! ${doneFlag}; then
      break
    fi
  done
  instStepCat=$(printf "step#%d" "$instStepNum")
  stepArray=("$instStepCat")
  stepDesc=$(printf '"%s"' "${checkListSteps[$instStepCat]}")
  stepArray+=("$stepDesc")
  stepArray+=(OFF)

  stepDesc=$(printf " %s" "${stepArray[@]}")
  stepDesc=${stepDesc:1}
  installSteps+=("$stepDesc")

  if [ ${instStepNum} -lt 4 ]; then
    setTasksForMainStep "$instStepCat" ${instStepNum}
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                  setTasksForMainStep
# DESCRIPTION: Set the tasks to execute/run under the installation/main step
#              (i.e. Pre-Installation, etc.)
#  Required Params:
#      1) instStepCat - step key of the installation/main step
#      2) instStepNum - number of installation/main step
#---------------------------------------------------------------------------------------
setTasksForMainStep() {
  local instStepCat="$1"
  local instStepNum=$2
  local totSteps=${stepTotals[$instStepNum]}
  local stepArray=()
  local instStep=""
  local stepDesc=""
  local taskNum=0
  local doneFlag=$(echo 'false' && return 1)

  for taskNum in $(eval echo "{1..${totSteps}}"); do
    instStep=$(printf "$instStepCat.%d" "$taskNum")
    stepArray=("--$instStep")
    doneFlag=$(isTaskForStepDone ${instStepNum} ${taskNum})
    if ${doneFlag}; then
      stepDesc=$(appendValueToTaskDesc "$instStepNum" "$taskNum")
      stepDesc=$(printf '"%s"' "$stepDesc")
      stepArray+=("$stepDesc")
      stepArray+=(ON)
    else
      stepDesc=$(printf '"%s"' "${checkListSteps[$instStep]}")
      if [ "$instStep" == "step#4" ] && [ "${varMap["POST-INST"]}" == "Skipped" ]; then
        stepDesc+="     [Skipped]"
      fi
      stepArray+=("$stepDesc")
      stepArray+=(OFF)
    fi

    instStep=$(printf " %s" "${stepArray[@]}")
    instStep=${instStep:1}
    installSteps+=("$instStep")
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                selectInstallationSteps
# DESCRIPTION: Select the installation steps to perform within a linux "dialog" box
#              of type check list
#  Required Params:
#      1) extraLabel - label for the extra button
#---------------------------------------------------------------------------------------
selectInstallationSteps() {
  local dialogTitle="AALP-GYGI Installation Guide Menu"
  local helpText=$(getHelpTextForInstGuide "$1")
  local -A dialogParms=(["cancel-label"]="Exit" ["dialogHeight"]=35 ["dialogWidth"]=98 ["listHeight"]=15 ["no-collapse"]=true
      ["help-button"]=true ["helpHeight"]=28 ["help-text"]="$helpText" ["help-title"]="Help:  $dialogTitle" ["helpURL"]=true)
  local dialogText=$(getTextForInstallGuide)

  dialogParms["extra-label"]="$1"

  showListOfChoicesDialogBox "$DIALOG_BACK_TITLE" "$dialogTitle" "$dialogText" installSteps "checklist" dialogParms
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                getTextForInstallGuide
# DESCRIPTION: Get the text to display in the linux "dialog" for the installation guide
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getTextForInstallGuide() {
  local textArray=(
  "                      .:dOko'      '        ,,,xxxxxxxxxxxxxxxxxxddddddddddddddd"
  "      .        ..    xWWNNNNWX'  ..'.       .. 'ololooloolloololoooooooooooooooo"
  "    .''        .;oOc;0XNNNNNXKc;Od;.        ,''. .c:c::::cc:c:,,;ccccccccccccccc"
  "   ..,OO;.     ....';clkOOk0d::;...'.    .:kKd''. .::::::::c:,'',;::cccccccccccc"
  "  '.,dkkO0Odl::;....;OoKO0kKxxx'....:odxOOkkxxo''. .llllllc:;'''',:;ccllllllllll"
  " .''oxdxxxddOx:......:XddK0oOk.......:xdooodddxl.'.  loooo:,;,,'''';;coooooooooo"
  " ..:d:;'''...';.....,..:k.;x..''..'.':.......''c''.  llcc;'''..''''';::lllllllll"
  " '...........:;.....,'.':ll;'.:;.'...;:'..........'  llc,'''''''''''',;;clllcllc"
  " '..........,,'...,xKKcxo .xdlK0x....'''...........  ::,'''''''',''''',;;:cccccc"
  " ........    ,.',.l;':,:o,;l;':'cc';.':    ......'. .;'''''''cl;ll;''''';;:cc:::"
  "  ......     00l....0l,,'..';,lO'...xWO     ...... l;,''''.,xXx::xx:''''';;;c;;;"
  "   .....     dWNWk..c'. .... ',:'.0WWWc     .... .:,''''''.:NNxocdOx'''''.'',;;;"
  "      ..      xWNMx...''',''''...OWWWo      .. .c:,''''''',cxxxookKk;,''..''''c:"
  "               'kNWXc;,.''''.':cXWNx.        'll,''',;codxdxkkOOOOOdddoc:;;'';,,"
  "                 dWWK','.......kWWk         'ol,,;coxkO0OxddOOOkxxxddxxoollod::;"
  "                  OMWo        cNMO.         ddlldxO0XX0kxxxxkOkkdddddxxkkk00dolc"
  "                   lXNKxooooxKNKl           xxO0KXXXNXOxxxxxkOOkdddddxkkkOXNNK0O"
  "$DASH_LINE_FULL_WIDTH"
  "Select the step or tasks you want to execute/run:"
  )

  local dialogText=$(getTextForDialog "${textArray[@]}")

  echo "$dialogText"
}

#---------------------------------------------------------------------------------------
#      METHOD:                     showOptionsDialog
# DESCRIPTION: Show a linux "dialog" box of type "yes no" to:
#  Required Params:
#      1)  instChecklist - concatenated string of the Installation Guide Checklist
#      2) partLayoutText - concatenated string of the Partition Layout/Scheme
#      3)     dialogText - Text to display in dialog before the menu options
#---------------------------------------------------------------------------------------
showOptionsDialog() {
  local instChecklist="$1"
  local partLayoutText="$2"
  local -A dialogOpts=(["yes-label"]="Checklist" ["no-label"]="Exit" ["extra-label"]="Part. Tbl"
    ["no-collapse"]="true" ["dialogHeight"]=20)

  if [[ -n "$partLayoutText" ]]; then
    while true; do
      exec 3>&1
        showYesNoBox "$DIALOG_BACK_TITLE" "$3" "View of Partition Table & Checklist" dialogOpts 2>&1 1>&3
      exec 3>&-
      IFS=',' read -a fields <<< "$inputRetVal"
      case ${fields[0]} in
        $DIALOG_OK)
          $DIALOG --clear --no-collapse --backtitle "$DIALOG_BACK_TITLE" --title "Installation Guide Checklist" \
            --ok-label "Exit" --msgbox "$instChecklist" 33 97 3>&1 1>&2 2>&3
        ;;
        $DIALOG_CANCEL)
          break
        ;;
        $DIALOG_EXTRA)
          $DIALOG --clear --no-collapse --backtitle "$DIALOG_BACK_TITLE" --title "Partition Layout/Scheme" \
            --ok-label "Exit" --msgbox "$partLayoutText" 33 97 3>&1 1>&2 2>&3
        ;;
      esac
    done
  else
    $DIALOG --clear --no-collapse --backtitle "$DIALOG_BACK_TITLE" --title "Installation Guide Checklist" \
      --ok-label "Exit" --msgbox "$instChecklist" 33 97 3>&1 1>&2 2>&3
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:              getDataFromPartitionTable
# DESCRIPTION: Get the rows of data from the partition table to be displayed within
#              a linux "dialog" box
#      RETURN: concatenated string
#  Required Params:
#      1)  blockDevice - string value of the device (i.e. "/dev/sda")
#      2) blockDevSize - size in human-readable format of "$blockDevice"
#      3)      pttDesc - value from global associative array variable "$PART_TBL_TYPES"
#      4)      lvmDesc - contains the name of the Physical Volume and the Volume Group
#   Optional Param:
#      5) line/row of data representing a partition or logical volume delimited by '\t'
#      6) last line/row number to print
#---------------------------------------------------------------------------------------
function getDataFromPartitionTable() {
  local blockDevice="$1"
  local blockDevSize="$2"
  local pttDesc="$3"
  local lvmDesc="$4"
  local dialogPartTable=""

  if [ "$#" -gt 5 ]; then
    dialogPartTable=$(getPartitionTableForDialog "$5" $6)
  elif [ "$#" -gt 4 ]; then
    dialogPartTable=$(getPartitionTableForDialog "$5")
  else
    dialogPartTable=$(getPartitionTableForDialog)
  fi

  local textArray=(
  "$lvmDesc"
  "$dialogPartTable")

  dialogPartTable=$(getTextForDialog "${textArray[@]}")

  echo "$dialogPartTable"
}

#---------------------------------------------------------------------------------------
#      METHOD:                      dispLoadingDataPB
# DESCRIPTION: Display a linux "dialog" box of type gauge (i.e. progress bar) while
#              executing the methods to initialize the data for the installation guide.
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)         pbTitle - String to be displayed at the top of the dialog box.
#---------------------------------------------------------------------------------------
dispLoadingDataPB() {
  (
    local methodNum=1
    local pbPerc=0
    local methodName=""
    local pbTitle=""
    local totalMethods=${#pbKeys[@]}
    local continueFlag=$(echo 'true' && return 0)

    sleep 0.5
    for pbKey in "${pbKeys[@]}"; do
      pbTitle="${pbTitles[$pbKey]}"
      echo $pbPerc
      echo "XXX"
      echo "$pbTitle"
      echo "XXX"
      methodName="${methodNames[$pbKey]}"

      callInstMethod
      if ! ${continueFlag}; then
        break
      fi

      sleep 0.5
    done
  ) | $DIALOG --clear --backtitle "$1" --title "$2" --gauge "Please Wait..." 20 80 0
}

#---------------------------------------------------------------------------------------
#      METHOD:                  getInstallGuideChecklist
# DESCRIPTION: Get the checklist for the Installation Guide.  This will include the steps
#              and tasks that are complete/done.
#      RETURN: Concatenated string
#---------------------------------------------------------------------------------------
function getInstallGuideChecklist() {
  local instStepNum=0
  local numInstSteps=$(expr ${#stepTotals[@]} - 1)
  local stepArray=()
  local instStepCat=""
  local checkMark=" "
  local instStep=""
  local doneFlag=$(echo 'false' && return 1)

  for instStepNum in $(eval echo "{1..${numInstSteps}}"); do
    instStepCat=$(printf "step#%d" "$instStepNum")
    doneFlag=$(isInstStepDone ${instStepNum})
    if ${doneFlag}; then
      checkMark="X"
    else
      checkMark=" "
    fi
    instStep=$(printf "[%s] %s%5s\"%s\"" "$checkMark" "$instStepCat" " " "${checkListSteps[$instStepCat]}")
    stepArray+=("$instStep")
    if [ ${instStepNum} -lt 4 ]; then
      addTasksForMainStep "$instStepCat" ${instStepNum}
      stepArray+=(" ")
    elif [ ${instStepNum} -lt 5 ]; then
      stepArray+=(" ")
    fi
  done
  local concatStr=$(printf "\n%s" "${stepArray[@]}")
  concatStr=${concatStr:1}
  echo "$concatStr"
}

#---------------------------------------------------------------------------------------
#      METHOD:                  addTasksForMainStep
# DESCRIPTION: Add the tasks of the installation/main step (i.e. Pre-Installation, etc.)
#  Required Params:
#      1) instStepCat - step key of the installation/main step
#      2) instStepNum - number of installation/main step
#---------------------------------------------------------------------------------------
addTasksForMainStep() {
  local instStepCat="$1"
  local instStepNum=$2
  local totSteps=${stepTotals[$instStepNum]}
  local taskOfStep=""
  local stepDesc=""
  local taskNum=0
  local doneFlag=$(echo 'false' && return 1)
  local checkMark=" "

  for taskNum in $(eval echo "{1..${totSteps}}"); do
    taskOfStep=$(printf "$instStepCat.%d" "$taskNum")
    doneFlag=$(isTaskForStepDone ${instStepNum} ${taskNum})
    if ${doneFlag}; then
      checkMark="X"
      stepDesc=$(appendValueToTaskDesc "$instStepNum" "$taskNum")
      stepDesc=$(trimString "$stepDesc")
      stepDesc=$(printf '"%s"' "$stepDesc")
    else
      checkMark=" "
      stepDesc=$(trimString "${checkListSteps[$taskOfStep]}")
      stepDesc=$(printf '"%s"' "$stepDesc")
    fi
    if [ ${taskNum} -lt ${totSteps} ]; then
      taskOfStep=$(printf "%4s%s[%s] %s%5s%s" " " "$LVM_DEVICE_PREFIX" "$checkMark" "$taskOfStep" " " "$stepDesc")
    else
      taskOfStep=$(printf "%4s%s[%s] %s%5s%s" " " "$LAST_LVM_PREFIX" "$checkMark" "$taskOfStep" " " "$stepDesc")
    fi
    stepArray+=("$taskOfStep")
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                   dispMissingRequirements
# DESCRIPTION: Display a linux "dialog" box of type "menu" to display the requirements
#              and which ones are missing.
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)     dialogTitle - String to be displayed at the top of the dialog box.
#      3)        errorMsg - Error Message
#---------------------------------------------------------------------------------------
dispMissingRequirements() {
  local errTxt=$(getTextForError)
  local msgTextArray=("$errTxt" " " "$3" "$DIALOG_BORDER_MAX")
  local msgText=$(getTextForDialog "${msgTextArray[@]}")
  local -A dialogParms+=(["nook"]="" ["cancel-label"]="Exit" ["dialogHeight"]=25 ["dialogWidth"]=98 ["listHeight"]=15
      ["no-collapse"]=true ["column-separator"]="$FORM_FLD_SEP" )

  showListOfChoicesDialogBox "$1" "$2" "$msgText" chkList "checklist" dialogParms
}

#---------------------------------------------------------------------------------------
#      METHOD:                      showRebootConfDlg
# DESCRIPTION: Show a linux "dialog" box of type "yes no" to get confirmation to
#              save the data and reboot the system.
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)     dialogTitle - String to be displayed at the top of the dialog box.
#      3)      dialogText - Text to display inside the dialog before the
#                           list of menu options
#---------------------------------------------------------------------------------------
showRebootConfDlg() {
  local -A cmdOpts=(["yes-label"]="Continue" ["no-label"]="Cancel" ["dialogHeight"]=20)

  exec 3>&1
    showYesNoBox "$1" "$3" "$2" cmdOpts 2>&1 1>&3
  exec 3>&-
  IFS=',' read -a fields <<< "$inputRetVal"

  case ${fields[0]} in
    $DIALOG_OK)
      yesNoFlag=$(echo 'true' && return 0)
    ;;
    $DIALOG_CANCEL)
      yesNoFlag=$(echo 'false' && return 1)
    ;;
  esac
}
