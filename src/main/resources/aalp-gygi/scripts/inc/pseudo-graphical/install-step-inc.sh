#!/bin/bash
#set -e

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# globals
#===============================================================================

#===============================================================================
# functions/methods
#===============================================================================

#---------------------------------------------------------------------------------------
#      METHOD:                    selectPrimaryCountry
# DESCRIPTION: Show a linux "radiolist" dialog to select a country to be the primary
#              for configuration of the mirrors.  Set the value within the global
#              variable "$mbSelVal"
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)     dialogTitle - String to be displayed at the top of the dialog box.
#      3)      dialogText - Text to display inside the dialog before the
#                           list of menu options
#      4)        helpText - Text to display on the help/more dialog
#---------------------------------------------------------------------------------------
selectPrimaryCountry() {
  local dialogBackTitle="$1"
  local dialogTitle="$2"
  local dialogText="$3"
  local helpText="$4"
  local -A dialogParms=(["defaultChoice"]="US" ["dialogHeight"]=25 ["cancel-label"]="Skip" ["help-button"]="true"
      ["help-text"]="$helpText" ["help-title"]="Help:  $dialogTitle" ["helpURL"]="true")
  local urls=()
  local urlRefChoices=("Arch Wiki" "Mirror Status" "Reflector")
  local -A urlDialogOpts=(["dialogHeight"]=20)

  setELinksURLs "$helpText"

  for ctryCode in "${MIRROR_CTRY_CODES[@]}"; do
    dialogParms["$ctryCode"]="${MIRROR_CTRY_NAMES["$ctryCode"]}"
  done

  while true; do
    exec 3>&1
      showListOfChoicesDialogBox "$dialogBackTitle" "$dialogTitle" "$dialogText" MIRROR_CTRY_CODES "radiolist" dialogParms 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$mbSelVal"
    case ${fields[0]} in
      $DIALOG_OK)
        mbSelVal=$(echo "${fields[1]//\"/}")
        break
      ;;
      $DIALOG_CANCEL)
        mbSelVal=""
        break
      ;;
      $DIALOG_HELP)
        if [ "${fields[1]}" == "View-URL" ]; then
          displayURLinElinks "$dialogBackTitle" urlRefChoices urlDialogOpts
        fi
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                     showSecondaryOption
# DESCRIPTION: Show a linux "dialog" box of type "yes no" to determine if more repo sites
#              should be added to the list of mirrors.  Set the global variable "yesNoFlag"
#              based on the option chosen.
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)     dialogTitle - String to be displayed at the top of the dialog box.
#      3)      dialogText - Text to display inside the dialog before the
#                           list of menu options
#      4)        helpText - Text to display on the help/more dialog
#---------------------------------------------------------------------------------------
showSecondaryOption() {
  local dialogBackTitle="$1"
  local dialogTitle="$2"
  local dialogText="$3"
  local helpText="$4"
  local -A cmdOpts=(["yes-label"]="YES" ["no-label"]="NO" ["dialogHeight"]=20
    ["help-button"]=true ["help-text"]="$helpText" ["help-title"]="Help:  $dialogTitle")
  local urls=()

  setELinksURLs "$helpText"

  while true; do
    yesNoFlag=$(echo 'false' && return 1)
    exec 3>&1
      showYesNoBox "$DIALOG_BACK_TITLE" "$dialogText" "$dialogTitle" cmdOpts 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$inputRetVal"

    case ${fields[0]} in
      $DIALOG_OK)
        yesNoFlag=$(echo 'true' && return 0)
        break
      ;;
      $DIALOG_CANCEL)
        break
      ;;
      $DIALOG_HELP)
        if [ "${fields[1]}" == "View-URL" ]; then
          elinks "${urls[0]}"
        fi
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                  selectSecondaryCountries
# DESCRIPTION: Show a linux "checklist" dialog to select country codes to be the secondary
#              for configuration of the mirrors.  Add the values selected to the
#              variable "ctryCodes".
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)     dialogTitle - String to be displayed at the top of the dialog box.
#      3)      dialogText - Text to display inside the dialog before the
#                           list of menu options
#      4)        helpText - Text to display on the help/more dialog
#---------------------------------------------------------------------------------------
selectSecondaryCountries() {
  local dialogBackTitle="$1"
  local dialogTitle="$2"
  local dialogText="$3"
  local helpText="$4"
  local -A dialogParms=(["dialogHeight"]=25 ["cancel-label"]="Skip" ["help-button"]="true" ["help-text"]="$helpText"
    ["help-title"]="Help:  $dialogTitle" ["helpURL"]="true")
  local urls=()
  local urlRefChoices=("Arch Wiki" "Mirror Status" "Reflector")
  local -A urlDialogOpts=(["dialogHeight"]=20 )

  setELinksURLs "$helpText"

  for ctryCode in "${ctryCodes[@]}"; do
    dialogParms["$ctryCode"]="${ISO_CODE_NAMES["$ctryCode"]}"
  done

  while true; do
    exec 3>&1
      showListOfChoicesDialogBox "$dialogBackTitle" "$dialogTitle" "$dialogText" ctryCodes "checklist" dialogParms 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$mbSelVal"
    case ${fields[0]} in
      $DIALOG_OK)
        mbSelVal=$(echo "${fields[1]//\"/}")
        IFS=' ' read -d '' -a ctryCodes <<< "$mbSelVal"
        break
      ;;
      $DIALOG_CANCEL)
        ctryCodes=()
        break
      ;;
      $DIALOG_HELP)
        if [ "${fields[1]}" == "View-URL" ]; then
          displayURLinElinks "$dialogBackTitle" urlRefChoices urlDialogOpts
        fi
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                      selectLinuxKernel
# DESCRIPTION: Show a linux "radiolist" dialog to select the type of officially supported
#              linux kernel to install.  Set the value within the global variable "$mbSelVal"
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)     dialogTitle - String to be displayed at the top of the dialog box.
#      3)      dialogText - Text to display inside the dialog before the
#                           list of menu options
#      4)        helpText - Text to display on the help/more dialog
#---------------------------------------------------------------------------------------
selectLinuxKernel() {
  local dialogBackTitle="$1"
  local dialogTitle="$2"
  local dialogText="$3"
  local helpText="$4"
  local -A dialogParms=(["dialogHeight"]=20 ["help-button"]="true" ["helpWidth"]=85
      ["help-text"]="$helpText" ["help-title"]="Help:  $dialogTitle" ["helpURL"]="true")
  local urls=()
  local urlRefChoices=("Arch Wiki" "LTS & Stable" "Hardened" "ZEN Kernel")
  local -A urlDialogOpts=(["dialogHeight"]=20)

  setELinksURLs "$helpText"

  for kernel in "${kernelChoices[@]}"; do
    dialogParms["$kernel"]="${kernelDescs["$kernel"]}"
  done

  while true; do
    exec 3>&1
      showListOfChoicesDialogBox "$dialogBackTitle" "$dialogTitle" "$dialogText" kernelChoices "radiolist" dialogParms 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$mbSelVal"
    case ${fields[0]} in
      $DIALOG_OK)
        mbSelVal=$(echo "${fields[1]//\"/}")
        break
      ;;
      $DIALOG_CANCEL)
        mbSelVal=""
        break
      ;;
      $DIALOG_HELP)
        if [ "${fields[1]}" == "View-URL" ]; then
          displayURLinElinks "$dialogBackTitle" urlRefChoices urlDialogOpts
        fi
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                        showInstConf
# DESCRIPTION: Show a linux "dialog" box of type "yes no" to get confirmation to
#              install the base, linux kernel, python, etc.
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)     dialogTitle - String to be displayed at the top of the dialog box.
#      3)      dialogText - Text to display inside the dialog before the
#                           list of menu options
#      4)        helpText - Text to display on the help/more dialog
#---------------------------------------------------------------------------------------
showInstConf() {
  local dialogBackTitle="$1"
  local dialogTitle="$2"
  local dialogText="$3"
  local helpText="$4"
  local -A cmdOpts=(["yes-label"]="Confirm" ["no-label"]="Cancel" ["dialogHeight"]=20 ["helpWidth"]=85
    ["help-button"]=true ["help-text"]="$helpText" ["help-title"]="Help:  $dialogTitle")
  local urls=()

  setELinksURLs "$helpText"

  while true; do
    yesNoFlag=$(echo 'false' && return 1)
    exec 3>&1
      showYesNoBox "$DIALOG_BACK_TITLE" "$dialogText" "$dialogTitle" cmdOpts 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$inputRetVal"

    case ${fields[0]} in
      $DIALOG_OK)
        yesNoFlag=$(echo 'true' && return 0)
        break
      ;;
      $DIALOG_CANCEL)
        break
      ;;
      $DIALOG_HELP)
        if [ "${fields[1]}" == "View-URL" ]; then
          elinks "${urls[0]}"
        fi
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                    showDlgForSystemdSwap
# DESCRIPTION: Show a linux "dialog" box of type "yes no" to display information about
#              configuration of the 'systemd-swap' script.
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)     dialogTitle - String to be displayed at the top of the dialog box.
#      3)      dialogText - Text to display inside the dialog before the
#                           list of menu options
#---------------------------------------------------------------------------------------
showDlgForSystemdSwap() {
  local -A cmdOpts=(["yes-label"]="Ok" ["extra-label"]="View URL" ["no-label"]="Skip" ["dialogHeight"]=25)
  local fields=()
  local urls=()

  setELinksURLs "$3"
  yesNoFlag=$(echo 'false' && return 1)

  while true; do
    exec 3>&1
      showYesNoBox "$1" "$3" "$2" cmdOpts 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$inputRetVal"

    case ${fields[0]} in
      ${DIALOG_OK})
        yesNoFlag=$(echo 'true' && return 0)
        break
      ;;
      ${DIALOG_CANCEL})
        break
      ;;
      ${DIALOG_EXTRA})
        elinks "${urls[0]}"
      ;;
    esac
  done
}