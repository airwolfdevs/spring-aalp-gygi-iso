#!/bin/bash
#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# functions/methods
#===============================================================================

#---------------------------------------------------------------------------------------
#      METHOD:                 selectPartitionTableType
# DESCRIPTION: Select the type of partition table to create and set it in the
#              global variable "mbSelVal"
#  Required Params:
#      1) assocParamAry - associative array of key/value pairs of parameters for the method
#---------------------------------------------------------------------------------------
selectPartitionTableType() {
  local -n assocParamAry="$1"
  local fields=()
  local urlRefChoices=("Partition Table" "How-to Geek")
  local -A urlDialogOpts=(["dialogHeight"]=20 ["dialogWidth"]=90 ["Partition Table"]="- https://wiki.archlinux.org/index.php/partitioning#Partition_table"
  ["How-to Geek"]="- https://www.howtogeek.com/193669/whats-the-difference-between-gpt-and-mbr-when-partitioning-a-drive")

  while true; do
    exec 3>&1
      showDlgToSelectPartitionTableType assocParamAry 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$mbSelVal"
    case ${fields[0]} in
      $DIALOG_OK)
        mbSelVal=$(echo "${fields[1]}")
        break
      ;;
      $DIALOG_CANCEL)
        mbSelVal=""
        break
      ;;
      $DIALOG_HELP)
        if [ "${fields[1]}" == "View-URL" ]; then
          displayURLinElinks "${assocParamAry[dialogBackTitle]}" urlRefChoices urlDialogOpts
        fi
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:             showDlgToSelectPartitionTableType
# DESCRIPTION: Show the linux "dialog" of type "--radiolist" to select the operation to
#              select the type of partition table to create
#  Required Params:
#      1) selFuncParams - associative array of key/value pairs of parameters for the method:
#                 "choices" - names of the types of partition tables that can be created
#         "dialogBackTitle" - Specifies a backtitle string to be displayed on the
#                             backdrop, at the top of the screen.
#             "dialogTitle" - String to be displayed at the top of the dialog box.
#              "partLayout" - the partition layout/scheme
#                 "pttDesc" - partition table type
#---------------------------------------------------------------------------------------
showDlgToSelectPartitionTableType() {
  local -n selFuncParams=$1
  local dialogTitle="${selFuncParams[dialogTitle]}"
  local textArray=(
  "A Partition Table is a data structure that provides basic information of"
  "the device/disk.  There are two main types of partition tables available:"
  "       A) Master Boot Record (MBR)"
  "       B) GUID Partition Table (GPT)"
  "(More...)"
  "$DASH_LINE_WIDTH_80" " " "Select the type to create:"
  )
  local dialogText=$(getTextForDialog "${textArray[@]}")
  local helpText=$(getHelpTextForPTT)
  local pttChoices=()
  local -A dialogParms=(["dialogHeight"]=20 ["help-label"]="More..." ["help-text"]="$helpText"
  ["help-title"]="Help:  $dialogTitle" ["helpURL"]=true)

  IFS=$'\t' read -a pttChoices <<< "${selFuncParams["choices"]}"
  for opt in "${pttChoices[@]}"; do
    dialogParms["$opt"]="${selFuncParams[$opt]}"
  done

  showListOfChoicesDialogBox "${selFuncParams[dialogBackTitle]}" "$dialogTitle" "$dialogText" pttChoices "radiolist" dialogParms
}

#---------------------------------------------------------------------------------------
#      METHOD:                     getSizeForEspDevice
# DESCRIPTION: Get the size to be allocated for the EFI system partition.
#              Set the size in human-readable format in the global variable "mbSelVal"
#  Required Params:
#      1) assocParamAry - associative array of key/value pairs of parameters for the method
#              "deviceName" - Name of the partition
#              "deviceType" - Partition
#         "dialogBackTitle" - Specifies a backtitle string to be displayed on the
#                             backdrop, at the top of the screen.
#         "dialogPartTable" - string containing the rows of data for the
#                             partition table in a textual table format
#             "deviceSizes" - the min, max, & other sizes in human-readable format
#          "partTableStats" - summary table of the progress in the partitioning process
#                             in a textual table format
#---------------------------------------------------------------------------------------
getSizeForEspDevice() {
  local -n assocParamAry="$1"
  local deviceSizes=()
  local dialogTitle="Size of 'ESP' Allocation Choices"
  local helpText=$(getHelpTextForEspDevice)
  local -A dialogAddOpts=(["dialogHeight"]=30 ["dialogTitle"]="$dialogTitle" ["help-text"]="$helpText"
        ["urlRef"]="https://wiki.archlinux.org/index.php/EFI_system_partition")
  local textArray=("The '/boot/efi' directory is the storage place used by"
                    "the UEFI firmware and is mandatory for UEFI boot." "$DASH_LINE_WIDTH_80" " "
  )

  IFS=$'\t' read -d '' -a deviceSizes <<< "${assocParamAry[deviceSizes]}"
  deviceSizes[-1]=$(trimString "${deviceSizes[-1]}")

  if [[ "${assocParamAry[deviceType]}" =~ "Logical" ]]; then
    dialogAddOpts["dialogHeight"]=35
  fi

  setHRSForDevice dialogAddOpts deviceSizes assocParamAry
}

#---------------------------------------------------------------------------------------
#      METHOD:                   getSizeForBootDevice
# DESCRIPTION: Get the size to be allocated for the /boot partition or logical volume.
#              Set the size in human-readable format in the global variable "mbSelVal".
#  Required Params:
#      1) assocParamAry - associative array of key/value pairs of parameters for the method
#              "deviceName" - Name of the logical volume or partition
#              "deviceType" - Logical Volume or Partition
#         "dialogBackTitle" - Specifies a backtitle string to be displayed on the
#                             backdrop, at the top of the screen.
#         "dialogPartTable" - string containing the rows of data for the
#                             partition table in a textual table format
#             "deviceSizes" - the min, max, & other sizes in human-readable format
#          "partTableStats" - summary table of the progress in the partitioning process
#                             in a textual table format
#---------------------------------------------------------------------------------------
getSizeForBootDevice() {
  local -n assocParamAry="$1"
  local deviceSizes=()
  local dialogTitle="Size of '/boot' Allocation Choices"
  local helpText=$(getHelpTextForBootDevice "${assocParamAry[dialogPartTable]}")
  local -A dialogAddOpts=(["dialogHeight"]=30 ["dialogTitle"]="$dialogTitle" ["help-text"]="$helpText"
        ["urlRef"]="https://wiki.archlinux.org/index.php/partitioning#.2Fboot")
  local textArray=("The /boot directory contains the kernel and ramdisk images as well as the"
    "bootloader configuration file and bootloader stages."
    "$DASH_LINE_WIDTH_80" " "
  )
  local reqFlag=$(isBootPartReq)

  IFS=$'\t' read -d '' -a deviceSizes <<< "${assocParamAry[deviceSizes]}"
  deviceSizes[-1]=$(trimString "${deviceSizes[-1]}")

  if [[ "${assocParamAry[deviceType]}" =~ "Logical" ]]; then
    dialogAddOpts["dialogHeight"]=35
  fi

  if ${reqFlag}; then
    dialogAddOpts["nocancel"]=true
  else
    dialogAddOpts["cancel-label"]="Skip"
  fi

  setHRSForDevice dialogAddOpts deviceSizes assocParamAry
}

#---------------------------------------------------------------------------------------
#      METHOD:                   getSizeForRootDevice
# DESCRIPTION: Get the size to be allocated for the '/' (i.e. root) partition
#              or logical volume.  Set the size in human-readable format in the
#              global variable "mbSelVal".
#  Required Params:
#      1) assocParamAry - associative array of key/value pairs of parameters for the method
#              "deviceName" - Name of the logical volume or partition
#              "deviceType" - Logical Volume or Partition
#         "dialogBackTitle" - Specifies a backtitle string to be displayed on the
#                             backdrop, at the top of the screen.
#         "dialogPartTable" - string containing the rows of data for the
#                             partition table in a textual table format
#             "deviceSizes" - the min, max, & other sizes in human-readable format
#          "partTableStats" - summary table of the progress in the partitioning process
#                             in a textual table format
#---------------------------------------------------------------------------------------
getSizeForRootDevice() {
  local -n assocParamAry="$1"
  local deviceSizes=()
  local dialogTitle="Size of 'ROOT' Allocation Choices"
  local lcDevType=$(echo "${assocParamAry[deviceType]}" | tr '[:upper:]' '[:lower:]')
  local helpText=$(getHelpTextForRootWithWarning "$lcDevType" "${assocParamAry[dialogPartTable]}")
  local -A dialogAddOpts=(["dialogHeight"]=30 ["skipSizeFlag"]=true ["nocancel"]=true
    ["dialogTitle"]="$dialogTitle" ["help-text"]="$helpText"
    ["urlRef"]="https://wiki.archlinux.org/index.php/partitioning#.2F")
  local textArray=("The '/' or root $lcDevType is necessary and it is the most important."
    "$DASH_LINE_WIDTH_80" " " "${assocParamAry[partTableStats]}")

  assocParamAry["dialogTextHdr"]=$(getTextForDialog "${textArray[@]}")

  if [[ "${assocParamAry[deviceType]}" =~ "Logical" ]]; then
    dialogAddOpts["dialogHeight"]=35
  fi

  IFS=$'\t' read -d '' -a deviceSizes <<< "${assocParamAry[deviceSizes]}"
  deviceSizes[-1]=$(trimString "${deviceSizes[-1]}")

  setHRSForDevice dialogAddOpts deviceSizes assocParamAry
}

#---------------------------------------------------------------------------------------
#      METHOD:                    getSizeForVarDevice
# DESCRIPTION: Get the size to be allocated for the /var partition or logical volume.
#              Set the size in human-readable format in the global variable "mbSelVal".
#  Required Params:
#      1) assocParamAry - associative array of key/value pairs of parameters for the method
#              "deviceName" - Name of the logical volume or partition
#              "deviceType" - Logical Volume or Partition
#         "dialogBackTitle" - Specifies a backtitle string to be displayed on the
#                             backdrop, at the top of the screen.
#         "dialogPartTable" - string containing the rows of data for the
#                             partition table in a textual table format
#             "deviceSizes" - the min, max, & other sizes in human-readable format
#          "partTableStats" - summary table of the progress in the partitioning process
#                             in a textual table format
#---------------------------------------------------------------------------------------
getSizeForVarDevice() {
  local -n assocParamAry="$1"
  local deviceSizes=()
  local dialogTitle="Size of '/var' Allocation Choices"
  local delimiter=" data"
  local textArray=("${varDeviceTextArray[@]:0:2}")
  splitStr "${varDeviceTextArray[1]}" "$delimiter"
  textArray[1]=$(echo "${splitStrArray[0]}$delimiter (More...)")
  textArray+=("$DASH_LINE_WIDTH_80" " " "${assocParamAry[partTableStats]}")
  local lcDevType=$(echo "${assocParamAry[deviceType]}" | tr '[:upper:]' '[:lower:]')

  local lines=$(printf "\n%s" "${varDeviceTextArray[@]}")
  lines=${lines:1}

  local helpText=$(getHelpTextForVarDevice "$lcDevType" "$lines" "${assocParamAry[dialogPartTable]}")
  assocParamAry["dialogTextHdr"]=$(getTextForDialog "${textArray[@]}")

  local -A dialogAddOpts=(["dialogHeight"]=30 ["dialogTitle"]="$dialogTitle" ["help-label"]="More..."
      ["help-text"]="$helpText" ["urlRef"]="https://wiki.archlinux.org/index.php/partitioning#.2Fvar")

  if [[ "${assocParamAry[deviceType]}" =~ "Logical" ]]; then
    dialogAddOpts["dialogHeight"]=35
  fi

  IFS=$'\t' read -d '' -a deviceSizes <<< "${assocParamAry[deviceSizes]}"
  deviceSizes[-1]=$(trimString "${deviceSizes[-1]}")

  setHRSForDevice dialogAddOpts deviceSizes assocParamAry
}

#---------------------------------------------------------------------------------------
#      METHOD:                   selectDeviceToRemove
# DESCRIPTION: Get the key to the row that will be removed and set it within
#              the global variable "mbSelVal"
#      1) assocParamAry - associative array of key/value pairs of parameters for the method
#                 "choices" - string of the keys containing the eligible rows that can
#                             be removed and separated by '\t'
#              "deviceType" - Logical Volume or Partition
#         "dialogBackTitle" - Specifies a backtitle string to be displayed on the
#                             backdrop, at the top of the screen.
#         "dialogPartTable" - string containing the rows of data for the
#                             partition table in a textual table format
#          "partTableStats" - summary table of the progress in the partitioning process
#                             in a textual table format
#---------------------------------------------------------------------------------------
selectDeviceToRemove() {
  local -n assocParamAry="$1"
  local keys=()
  local fields=()
  local deviceType="${assocParamAry[deviceType]}"
  local dialogTitle=$(echo "Remove '$deviceType' Dialog")
  local -A dialogParms=(["no-collapse"]=true ["dialogHeight"]=30 ["help-label"]="View PT"
  ["help-text"]="${assocParamAry[dialogPartTable]}" ["help-title"]="Help:  $dialogTitle" ["helpWidth"]=98)
  local dialogText=$(getTextForRemDevDialog "$deviceType" "${assocParamAry[partTableStats]}")

  IFS=$'\t' read -a keys <<< "${assocParamAry[choices]}"

  for rowKey in "${keys[@]}"; do
    dialogParms["$rowKey"]="${assocParamAry[$rowKey]}"
  done

  while true; do
    exec 3>&1
      showListOfChoicesDialogBox "${assocParamAry[dialogBackTitle]}" "$dialogTitle" "$dialogText" keys \
        "radiolist" dialogParms 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$mbSelVal"
    case ${fields[0]} in
      $DIALOG_OK)
        mbSelVal=$(echo "${fields[1]}")
        break
      ;;
      $DIALOG_CANCEL)
        mbSelVal=""
        break
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getTextForRemDevDialog
# DESCRIPTION: Get the text to display in a linux "dialog" command when selecting
#              a partition to be removed
#      RETURN: concatenated string
#  Required Params:
#      1)      deviceType - Logical Volume or Partition
#      2)  partTableStats - displays the stats about the device
#---------------------------------------------------------------------------------------
function getTextForRemDevDialog() {
  local textArray=( "NOTE:" "-----"
  "   # Only the eligible '$1s' (i.e. NOT required by the system)"
  "     are available to be selected for removal!"
  "   # Select <View PT> to view all the rows configured in the"
  "     Partition Table"
  "$DASH_LINE_WIDTH_80" " "
  "$2" " "
  "Select the row to be removed:"
  )
  local dialogText=$(getTextForDialog "${textArray[@]}")

  echo "$dialogText"
}

#---------------------------------------------------------------------------------------
#      METHOD:                   getSelectedOperation
# DESCRIPTION: Get the selected operation to perform in order to manage
#              the logical volumes or partitions.  Set the selected choice in the
#              global variable "mbSelVal".
#  Required Params:
#      1)        urlRef - The URL in the helpText that is to be viewed within "elinks"
#      2) assocParamAry - associative array of key/value pairs of parameters for the method
#---------------------------------------------------------------------------------------
getSelectedOperation() {
  local urlRef="$1"
  local -n dlgFuncParams=$2
  local ary=()
  dlgFuncParams["helpText"]=$(getHelpTextForDeviceManagement "$urlRef" dlgFuncParams)

  while true; do
    exec 3>&1
      showDlgToSelOpToManDevs dlgFuncParams 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a ary <<< "$mbSelVal"
    case ${ary[0]} in
      $DIALOG_OK)
        mbSelVal=$(echo "${ary[1]}")
        break
      ;;
      $DIALOG_CANCEL)
        mbSelVal=$(echo "${ary[1]}")
        break
      ;;
      $DIALOG_EXTRA)
        mbSelVal=$(echo "${ary[1]}")
        break
      ;;
      $DIALOG_HELP)
        if [ "${fields[1]}" == "View-URL" ]; then
          elinks "$urlRef"
        fi
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                  showDlgToSelOpToManDevs
# DESCRIPTION: Show the linux "dialog" of type "--radiolist" to select the operation to
#              perform in order to manage the logical volumes or partitions
#  Required Params:
#      1) selFuncParams - associative array of key/value pairs of parameters for the method
#                 "choices" - concatenated string of choices to be displayed in the
#                             dialog separated by '\t'
#         "dialogBackTitle" - Specifies a backtitle string to be displayed on the
#                             backdrop, at the top of the screen.
#         "dialogPartTable" - string containing the rows of data for the partition table
#             "dialogTitle" - String to be displayed at the top of the dialog box.
#                "helpText" - Text to display on the help/more dialog
#          "partTableStats" - displays the stats about the partition table
#---------------------------------------------------------------------------------------
showDlgToSelOpToManDevs() {
  local -n selFuncParams=$1
  local rldChoices=()
  local textArray=( "   # < Help > will also display the partition table"
  "$DASH_LINE_WIDTH_80" " " "${selFuncParams[partTableStats]}" " "
  "Choose from the following options:")

  local dialogText=$(getTextForDialog "${textArray[@]}")
  local -A dialogParms=(["dialogHeight"]=30 ["extra-label"]="Done" ["no-collapse"]=true ["helpURL"]=true
    ["helpHeight"]=30 ["helpWidth"]=98 ["help-label"]="Help" ["help-text"]="${selFuncParams[helpText]}"
    ["help-title"]="Help: ${selFuncParams[dialogTitle]}" )

  if [[ "${assocParamAry[deviceType]}" =~ "Logical" ]]; then
    dialogAddOpts["dialogHeight"]=35
  fi

  IFS=$'\t' read -a rldChoices <<< "${selFuncParams[choices]}"
  for opt in "${rldChoices[@]}"; do
    dialogParms["$opt"]="${selFuncParams[$opt]}"
  done

  showListOfChoicesDialogBox "${selFuncParams[dialogBackTitle]}" "${selFuncParams[dialogTitle]}" "$dialogText" rldChoices "radiolist" dialogParms
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getNamesOfExistingDevices
# DESCRIPTION: Get the names of the logical volumes or partitions that exist in
#              the partition table.
#      RETURN: concatenated string delimited by '\t'
#  Required Params:
#      1) dialogPartTable - string containing the rows of data for the
#                           partition table in a textual table format
#      2)      deviceType - Logical Volume or Partition
#---------------------------------------------------------------------------------------
function getNamesOfExistingDevices() {
  local dialogPartTable="$1"
  local deviceType="$2"
  local devicePrefix="lvm-"
  local cols=()
  local deviceName=""
  local deviceNames=()
  local rows=()
  local startRow=3

  if [ "$deviceType" == "Partition" ]; then
    devicePrefix="dev"
  fi

  if [[ "$dialogPartTable" =~ "Volume Group" ]]; then
    startRow=$(expr $startRow + 1)
  fi

  IFS=$'\n' read -d '' -a rows <<< "$dialogPartTable"

  for row in "${rows[@]:${startRow}}"; do
    if [[ "$row" =~ "$devicePrefix" ]]; then
      IFS=$'|' read -a cols <<< "$row"
      deviceName=$(trimString "${cols[2]}")
      deviceNames+=("$deviceName")
    fi
  done

  deviceName=$(printf "\n%s" "${deviceNames[@]}")
  deviceName=${deviceName:1}
  echo "$deviceName"
}

#---------------------------------------------------------------------------------------
#      METHOD:                      getCustomDevice
# DESCRIPTION: Get a custom Logical Volume or Partition
#              Set the:
#                A) device size in human-readable format in the global "mbSelVal" variable
#                B) name of the device that was entered in the global "ibEnteredText" variable
#  Required Params:
#      1) assocParamAry - associative array of key/value pairs of parameters for the method:
#             "deviceNames" - string concatenated names of existing partitions separated by '\t'
#              "deviceType" - Logical Volume or Partition
#         "dialogBackTitle" - Specifies a backtitle string to be displayed on the
#                             backdrop, at the top of the screen.
#         "dialogPartTable" - string containing the rows of data for the partition table
#                             in a textual table format
#            "maxAllocSize" - maximum size in human-readable format
#          "partTableStats" - summarized stats about the partition table
#                             in a textual table format
#---------------------------------------------------------------------------------------
getCustomDevice() {
  local -n assocParamAry=$1

  while true; do
    getNameOfDevice "${assocParamAry[dialogBackTitle]}" "${assocParamAry[deviceType]}" "${assocParamAry[deviceNames]}"
    if [ ${#ibEnteredText} -gt 0 ]; then
      assocParamAry["deviceName"]=$(printf "%s" "$ibEnteredText")
      local deviceSizes=("$MIN_PART_SIZE" "${assocParamAry[maxAllocSize]}")
      local dialogTitle=$(echo "Choices for the Allocation Size of '${assocParamAry[deviceName]}'")
      local -A dialogAddOpts=(["dialogTitle"]="$dialogTitle" ["helpWidth"]=98 ["help-text"]="${assocParamAry[dialogPartTable]}")
      setHRSForDevice dialogAddOpts deviceSizes assocParamAry
      if [ ${#mbSelVal} -gt 0 ]; then
        ibEnteredText=$(printf "%s" "${assocParamAry[deviceName]}")
        break
      fi
    else
      break
    fi
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                      getNameOfDevice
# DESCRIPTION: Get the name of a partition or logical volume.  Set the value that was
#              entered in the global "ibEnteredText" variable.
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)      deviceType - Logical Volume or Partition
#      3)     deviceNames - names of the current logical volumes or partitions
#                           that exist in the partition table seperated by '\'
#---------------------------------------------------------------------------------------
getNameOfDevice() {
  local dialogBackTitle="$1"
  local deviceType="$2"
  local deviceNames="$3"
  local fields=()

  ibEnteredText=""
  while true; do
    getValidatedDeviceName "$dialogBackTitle" "$deviceType" "$deviceNames"
    IFS=',' read -a fields <<< "$ibEnteredText"
    case ${fields[0]} in
      $DIALOG_OK)
        ibEnteredText=$(echo "${fields[1]}")
        break
      ;;
      $DIALOG_CANCEL)
        ibEnteredText=""
        break
      ;;
      *)
        ibEnteredText=""
        dispDeviceNameErrorMsg "${fields[0]}" "${fields[1]}"
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                  getValidatedDeviceName
# DESCRIPTION: Uses a "dialog" inputbox to get the name of either a
#              partition or logic volume and validates that the name:
#              A) does NOT match any of the regular expressions in the
#                 global array "$REG_EXP_PATTERNS" (i.e. boot, home, etc.)
#              B) is NOT a duplicate
#              Set the value that was entered in the global variable "ibEnteredText".
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)      deviceType - Logical Volume or Partition
#      3)     deviceNames - names of the current logical volumes or partitions
#                           that exist in the partition table seperated by '\'
#---------------------------------------------------------------------------------------
getValidatedDeviceName() {
  local dialogBackTitle="$1"
  local deviceType="$2"
  local deviceNames="$3"
  local dialogText=$(getDialogTextForInputBox "$deviceType")
  local dialogTitle=$(echo "$deviceType Name Entry Dialog")
  local -A dialogOpts=(["dialogHeight"]=20)
  local fields=()

  exec 3>&1
    showDialogInputBox "$dialogBackTitle" "$dialogText" "$dialogTitle" dialogOpts 2>&1 1>&3
  exec 3>&-
  IFS=',' read -a fields <<< "$ibEnteredText"
  case ${fields[0]} in
    $DIALOG_OK)
      local enteredName=$(trimString "${fields[1]}")
      local isValid=$(isValidDeviceName "$enteredName")
      ibEnteredText=$(appendDeviceNameToPrefix "$enteredName" "$deviceType")
      local isDuplicate=$(isDuplicateDeviceName "$deviceNames")

      if ! ${isValid}; then
        ibEnteredText=$(echo "$DIALOG_INPUT_ERR0R,$enteredName")
      elif ${isDuplicate}; then
        ibEnteredText=$(echo "$DIALOG_VALUE_ERR0R,$ibEnteredText")
      else
        ibEnteredText=$(echo "$DIALOG_OK,$ibEnteredText")
      fi
    ;;
    $DIALOG_CANCEL)
      ibEnteredText=$(echo "$DIALOG_CANCEL,${fields[1]}")
    ;;
  esac
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getDialogTextForInputBox
# DESCRIPTION: Get the text to display within a linux "dialog" input box
#      RETURN: Concatenated string
#  Required Params:
#      1) deviceType - Logical Volume or Partition
#---------------------------------------------------------------------------------------
function getDialogTextForInputBox() {
  local deviceType="$1"
  local prefix="$LOGICAL_VOL_PREFIX"
  if [ "$deviceType" == "Partition" ]; then
    prefix="/"
  fi

  local textArray=(
  "NOTE:  If the name does NOT begin with '$prefix', then the installer will"
  "       append the name to it (i.e. nexus-oss would become ${prefix}nexus-oss)"
  "$DASH_LINE_WIDTH_80" " "
  "NOTE:  The following '$deviceType' Names are reserved for use"
  "       by the installer and may NOT be used:"
  )
  for invName in "${INVALID_DEVICE_NAMES[@]}"; do
    line=$(printf "    --- %s or %s%s" "$invName" "$prefix" "$invName")
    textArray+=("$line")
  done

  textArray+=("$DASH_LINE_WIDTH_80" " ")
  line=$(echo "Enter the name for the '$deviceType' [ex: ${prefix}dbhome, ${prefix}nexus-oss]:")
  textArray+=("$line")

  local dialogText=$(getTextForDialog "${textArray[@]}")

  echo "$dialogText"
}

#---------------------------------------------------------------------------------------
#      METHOD:                  dispDeviceNameErrorMsg
# DESCRIPTION: Displays within a "dialog" message box that the entered device name
#              is either a duplicate or a reserved name.
#  Required Params:
#      1) errorCode - $DIALOG_INPUT_ERR0R:  reserved name
#                     $DIALOG_VALUE_ERR0R:  duplicate
#      2)  inputVal - the name that was entered
#---------------------------------------------------------------------------------------
dispDeviceNameErrorMsg() {
  local errorCode=$(echo "$1")
  local inputVal=$(echo "$2")
  local errorMsg=""
  case $errorCode in
    $DIALOG_INPUT_ERR0R)
      errorMsg=$(printf "ERROR!!!  The device name '%s' contains part of a name\n          that is reserved for the installer." "$inputVal")
    ;;
    $DIALOG_VALUE_ERR0R)
      errorMsg=$(printf "ERROR!!!  There is already a device with the name '%s'\n          that exists within the partition table.." "$inputVal")
    ;;
  esac
  local msgTextArray=(
  " "
  "$DASH_LINE_WIDTH_80" " "
  "$errorMsg" " "
  "$DASH_LINE_WIDTH_80" " "
  "You must hit OK to continue & re-enter a valid value"
  )

  local msgText=$(getTextForDialog "${msgTextArray[@]}")

  $DIALOG --clear  --no-collapse --title "Invalid Device Name" --msgbox "$msgText" 25 80 3>&1 1>&2 2>&3
}

#---------------------------------------------------------------------------------------
#      METHOD:                      setHRSForDevice
# DESCRIPTION: Set the human-readable size that the installer will allocate based on the
#              recommended options passed in.  The value will be set in the
#              global variable "mbSelVal".
#  Required Params:
#      1)         addOpts - options for the linux "dialog" command
#      2)     sizeChoices - the minimum & maximum sizes for the device
#      3) dlgMethodParams - associative array of key/value pairs of parameters for the method:
#                   "deviceName" - Name of a partition, logical volume, or swap file
#                   "deviceType" - Logical Volume, Partition, or Swap
#              "dialogBackTitle" - Specifies a backtitle string to be displayed on the
#                                  backdrop, at the top of the screen.
#                "dialogTextHdr" - text to be added to the linux "dialog"
#---------------------------------------------------------------------------------------
setHRSForDevice() {
  local -n addOpts=$1
  local -n sizeChoices=$2
  local -n dlgMethodParams=$3
  local deviceType="${dlgMethodParams[deviceType]}"
  local dialogTextHdr=""
  local hrMinSize=$(echo "${sizeChoices[0]}")
  local hrMaxSize=$(echo "${sizeChoices[-1]}")
  local dialogSizes=()
  local fields=()
  local -A dlgAddOpts=(["dialogHeight"]=30 ["extra-label"]="Other" ["help-button"]=true
    ["helpHeight"]=30 ["helpWidth"]=98 ["help-title"]="Help:  ${addOpts[dialogTitle]}" ["helpURL"]=true )

  if [[ "${assocParamAry[deviceType]}" =~ "Logical" ]]; then
    dialogAddOpts["dialogHeight"]=35
  fi

  if [ "$deviceType" == "Swap" ]; then
    hrMinSize=$(echo "${sizeChoices[1]}")
  fi

  if [ ${dlgMethodParams["dialogTextHdr"]+_} ]; then
    dialogTextHdr="${dlgMethodParams[dialogTextHdr]}"
  else
    dialogTextHdr="${dlgMethodParams[partTableStats]}"
  fi

  if [ ${#sizeChoices[@]} -gt 1 ] && [ ! ${addOpts["skipSizeFlag"]+_} ]; then
    local hrSizes=$(getSelectableSizes sizeChoices)
    IFS=$'\t' read -a dialogSizes <<< "$hrSizes"
  else
    dialogSizes=( "${sizeChoices[@]:0}" )
    unset addOpts["skipSizeFlag"]
  fi

  if [ ${addOpts["help-label"]+_} ]; then
    unset dlgAddOpts["help-button"]
  fi

  for key in "${!addOpts[@]}"; do
    dlgAddOpts[$key]=$(echo "${addOpts[$key]}")
  done

  mbSelVal=""
  ibEnteredText=""

  local minMaxTable=$(getTextForSizeInputBox "$hrMinSize" "$hrMaxSize" "$deviceType" "${dlgMethodParams[deviceName]}")
  dlgAddOpts["minMaxTable"]="$minMaxTable"

  while true; do
    if [ "$mbSelVal" != "Other" ]; then
      exec 3>&1
        selectSizeOfDevice "${dlgMethodParams[dialogBackTitle]}" "$dialogTextHdr" dlgAddOpts dialogSizes 2>&1 1>&3
      exec 3>&-

      IFS=',' read -a fields <<< "$mbSelVal"
      case ${fields[0]} in
        $DIALOG_OK)
          local aryIdx=$(expr ${fields[1]} - 1)
          mbSelVal=$(trimString "${dialogSizes[$aryIdx]}")
          break
        ;;
        $DIALOG_CANCEL)
          mbSelVal=""
          break
        ;;
        $DIALOG_EXTRA)
          mbSelVal="Other"
        ;;
        $DIALOG_HELP)
          if [ "${fields[1]}" == "View-URL" ]; then
            elinks "${addOpts[urlRef]}"
          fi
        ;;
      esac
    else
      getSizeOfDevice "$hrMinSize" "$hrMaxSize" "${dlgMethodParams[dialogBackTitle]}" "$minMaxTable"
      IFS=',' read -a fields <<< "$ibEnteredText"
      case ${fields[0]} in
        $DIALOG_OK)
          mbSelVal=$(trimString "${fields[1]}")
          break
        ;;
        $DIALOG_CANCEL)
          mbSelVal=""
          ibEnteredText=""
        ;;
        *)  # Help or Error
          mbSelVal="Other"
          ibEnteredText=""
        ;;
      esac
    fi
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                    selectSizeOfDevice
# DESCRIPTION: Select the size to be allocated for the device within a "dialog"
#              of type radio list
#  Required Params:
#      1)   dialogBackTitle - Specifies a backtitle string to be displayed on the
#                             backdrop, at the top of the screen.
#      2)     dialogTextHdr - text to be added to the linux "dialog"
#      3)     addDialogOpts - options for the linux "dialog" command
#      4) dialogSizeChoices - the choice of sizes that can be selected
#---------------------------------------------------------------------------------------
selectSizeOfDevice() {
  local dialogBackTitle="$1"
  local dialogTextHdr="$2"
  local -n addDialogOpts=$3
  local -n dialogSizeChoices=$4
  local dialogTitle="Size of Device Allocation Choices"
  local -A dialogCmdOptions=(["no-collapse"]=true ["dialogHeight"]=27 ["help-label"]="Help" ["help-title"]="Help:  $dialogTitle")
  local textArray=(
  "NOTE:" "   # < Help > will also display the partition table"
  "$DASH_LINE_WIDTH_80" " " "$dialogTextHdr" " "
  "Select the size you would like the installer to allocate:"
  )

  if [ ${addDialogOpts["help-label"]+_} ]; then
    textArray=("${textArray[@]:3}")
  fi

  if [ ${addDialogOpts["partTableStats"]+_} ]; then
    textArray=("${addDialogOpts[partTableStats]}" "${textArray[@]:2}")
    unset addDialogOpts["partTableStats"]
  fi

  local dialogText=$(getTextForDialog "${textArray[@]}")
  local helpText=""

  if [ ! ${addDialogOpts["extra-label"]+_} ]; then
    dialogCmdOptions["extra-label"]="Other"
  fi

  if [ ${addDialogOpts["dialogTitle"]+_} ]; then
    dialogTitle=$(echo "${addDialogOpts[dialogTitle]}")
  fi

  for key in "${!addDialogOpts[@]}"; do
    dialogCmdOptions[$key]=$(echo "${addDialogOpts[$key]}")
  done

  if [ ${addDialogOpts["help-text"]+_} ]; then
    helpText=$(getHelpTextForSelDeviceSize "${addDialogOpts[help-text]}" "${addDialogOpts[minMaxTable]}")
  else
    helpText=$(getHelpTextForSelDeviceSize "" "${addDialogOpts[minMaxTable]}")
  fi

  dialogCmdOptions["help-text"]="$helpText"
  showListOfChoicesDialogBox "$dialogBackTitle" "$dialogTitle" "$dialogText" dialogSizeChoices "radiolist" dialogCmdOptions
}

#---------------------------------------------------------------------------------------
#      METHOD:                      getSizeOfDevice
# DESCRIPTION: Uses a "dialog" input box to get the human readable size of
#              either a partition, logic volume, or swap file.
#              Set the size in human-readable format in the global variable "inputRetVal"
#  Required Params:
#      1)    minSize - the minimum size in human-readable format
#      2)    maxSize - the maximum size in human-readable format
#      3)  backTitle - Specifies a backtitle string to be displayed on the
#                      backdrop, at the top of the screen.
#      4) dialogText - text to display in dialog before the menu options
#---------------------------------------------------------------------------------------
getSizeOfDevice() {
  local minSize="$1"
  local maxSize="$2"
  local backTitle="$3"
  local dialogText="$4"
  local dialogTitle="Input Box Dialog for Entering Device Size"
  local helpText=$(getHelpTextForHRF)
  local -A dialogOpts=(["dialogHeight"]=30 ["no-collapse"]=true ["help-button"]=true ["help-text"]="$helpText" ["help-title"]="Help:  Human-Readable Format")
  local fields=()
  local validFlag=0

  exec 3>&1
    showDialogInputBox "$backTitle" "$dialogText" "$dialogTitle" dialogOpts 2>&1 1>&3
  exec 3>&-
  IFS=',' read -a fields <<< "$ibEnteredText"
  case ${fields[0]} in
    $DIALOG_OK)
      validFlag=$(isValidHumanReadableFmt "${fields[1]}")
      if ! ${validFlag}; then
        dispHumanReadableFmtErrorMsg "${fields[1]}"
        ibEnteredText=$(echo "$DIALOG_INPUT_ERR0R, human-readable format error")
      else
        validFlag=$(isValidSize "${fields[1]}" "$1" "$2")
        if ! ${validFlag}; then
          dispSizeRangeErrorMsg "${fields[1]}" "$1" "$2"
          ibEnteredText=$(echo "$DIALOG_VALUE_ERR0R, size range error")
        else
          ibEnteredText=$(echo "$DIALOG_OK, ${fields[1]}")
        fi
      fi
    ;;
    $DIALOG_CANCEL)
      ibEnteredText=$(echo "$DIALOG_CANCEL, ${fields[1]}")
    ;;
    *)
      ibEnteredText=$(echo "${fields[0]}, ${fields[1]}")
    ;;
  esac
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getTextForSizeInputBox
# DESCRIPTION: Get the text to display in a linux "dialog" command for an input box
#              to enter a size in human-readable format.
#      RETURN: concatenated string
#  Required Params:
#      1)    minSize - the minimum size in human-readable format
#      2)    maxSize - the maximum size in human-readable format
#      3) deviceType - Logical Volume, Partition, or Swap
#      4) deviceName - Name of a partition or logical volume
#---------------------------------------------------------------------------------------
function getTextForSizeInputBox() {
  local minSize="$1"
  local maxSize="$2"
  local deviceType="$3"
  local deviceName="$4"
  local hdrDtl=$(printf "%s,%s\n%s,%s" "Minimum" "Maximum" "$minSize" "$maxSize")
  local tbl=$(printTable ',' "$hdrDtl")
  local line="Enter the size to be allocated for the"

  if [ "$deviceType" == "Swap" ]; then
    line=$(echo "$line $deviceType $deviceName that is between the Minimum & Maximum values.")
  else
    line=$(echo -e "$line $deviceType w/ Name of\n'$deviceName' that is between the Minimum & Maximum values.")
  fi

  local textArray=( "$tbl" " " "$DASH_LINE_WIDTH_80"
  "NOTE:  The value to be entered must be in a valid human-readable format"
  "(Hit the \"Help\" button for valid examples)"
  "$DASH_LINE_WIDTH_80" " " "$line")
  local dialogText=$(getTextForDialog "${textArray[@]}")

  echo "$dialogText"
}

#---------------------------------------------------------------------------------------
#      METHOD:               dispHumanReadableFmtErrorMsg
# DESCRIPTION: Displays within a "dialog" message box that the entered size is
#              NOT a valid human-readable format.
#  Required Params:
#      1) inputSize - the size that was entered
#---------------------------------------------------------------------------------------
dispHumanReadableFmtErrorMsg() {
  local inputSize=$(echo "$1")
  local msgTextArray=(
  " "
  "ERROR!!!  The entered value of '$inputSize' is NOT a valid human-readable format!"
  " " " "
  "You must hit OK to continue & re-enter a valid value"
  )
  local msgText=$(getTextForDialog "${msgTextArray[@]}")

  $DIALOG --clear  --no-collapse --title "Invalid Size Entered" --msgbox "$msgText" 25 80 3>&1 1>&2 2>&3
}

#---------------------------------------------------------------------------------------
#      METHOD:                   dispSizeRangeErrorMsg
# DESCRIPTION: Displays within a "dialog" message box that the entered size does
#              not fall between the minimum & maximum values.
#  Required Params:
#      1) inputSize - the size that was entered
#---------------------------------------------------------------------------------------
dispSizeRangeErrorMsg() {
  local inputSize=$(echo "$1")
  local hdrDtl=$(printf "%s,%s\n%s,%s" "Minimum" "Maximum" "$2" "$3")
  local tbl=$(printTable ',' "$hdrDtl" | sed 's/^\-\-\-/   /')
  local msgTextArray=(
  "$tbl"
  " "
  "ERROR!!!  The entered value of '$inputSize' is NOT between the MIN & MAX sizes!"
  " " " "
  "You must hit OK to continue & re-enter a valid value"
  )
  local msgText=$(getTextForDialog "${msgTextArray[@]}")

  $DIALOG --clear  --no-collapse --title "Invalid Size Entered" --msgbox "$msgText" 25 80 3>&1 1>&2 2>&3
}

#---------------------------------------------------------------------------------------
#      METHOD:                showSaveConfirmationDialog
# DESCRIPTION: Show a linux "dialog" of type "yesno" to get confirmation to save the
#              the partitions
#  Required Params:
#      1)  partCmdSteps - array of the linux commands that will be executed for each step
#      2) assocParamAry - associative array of key/value pairs of parameters for the method:
#              "deviceType" - Logical Volume or Partition
#         "dialogBackTitle" - Specifies a backtitle string to be displayed on the
#                             backdrop, at the top of the screen.
#         "dialogPartTable" - string containing the rows of data for the
#                             partition table in a textual table format
#          "partTableStats" - summary table of the progress in the partitioning process
#                             in a textual table format
#---------------------------------------------------------------------------------------
showSaveConfirmationDialog() {
  local -n partCmdSteps=$1
  local -n assocParamAry=$2
  local helpText=$(getHelpTextToSaveDevices "${assocParamAry[deviceType]}" partCmdSteps "${assocParamAry[dialogPartTable]}")
  local partLayoutType="Discrete Partitions"
  local urlRefChoices=("Arch Wiki")
  local -A urlDialogOpts=(["dialogHeight"]=20)

  if [[ "${varMap[PART-LAYOUT]}" =~ "LVM" ]]; then
    partLayoutType="LVM Partition"
  fi
  local dialogTitle=""
  local dialogText=$(getTextForConf "${assocParamAry[partTableStats]}")
  local -A addOpts=(["dialogHeight"]=22 ["helpHeight"]=30 ["helpWidth"]=98
    ["help-button"]=true ["extra-label"]="Exit" ["help-text"]="$helpText" ["helpURL"]=true)

  if [ ${assocParamAry["dialogTitle"]+_} ]; then
    dialogTitle="${assocParamAry[dialogTitle]}"
    addOpts["dialogHeight"]=27
  else
    dialogTitle=$(echo "Save Confirmation for '$partLayoutType' Layout")
  fi

  setURLs "$helpText" urlRefChoices urlDialogOpts

  showYesNoSaveConfDialog "${assocParamAry[dialogBackTitle]}" "$dialogText" "$dialogTitle" addOpts urlRefChoices urlDialogOpts
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                      getTextForConf
# DESCRIPTION: Get the text that will be displayed in the linux "dialog" for
#              for confirming to create the partition table
#      RETURN: concatenated string
#  Required Params:
#      1)  statsTbl - summary table of the progress in the partitioning process
#                     in a textual table format
#---------------------------------------------------------------------------------------
function getTextForConf() {
  local statsTbl="$1"
  local textArray=("$statsTbl")

  if [[ ! "$statsTbl" =~ "Logical Volume" ]]; then
    local numParts=$(cat "$PARTITION_TABLE_FILE" | wc -l)
    local str=$(printf "%d Partitions" ${numParts})
    textArray+=(" " "$str")
  fi

  textArray+=("$DASH_LINE_WIDTH_80" " "
  "Select < Help > to see all rows of the partition table and"
  "description of options." "$DASH_LINE_WIDTH_80" " "
  "Select from the options below:")
  local dialogText=$(getTextForDialog "${textArray[@]}")

  echo "$dialogText"
}

#---------------------------------------------------------------------------------------
#      METHOD:                          setURLs
# DESCRIPTION: Set the URLs that will be able to be viewed in the ELinks web browser
#  Required Params:
#      1)   helpText - string containing the URLs within the text to be displayed
#                      in the help dialog
#      2) urlChoices - array of option names to be displayed in the linux "dialog"
#                      of type "radiolist" to select the URL.
#      3)    urlOpts - associative array of key/value pairs for the URLs
#---------------------------------------------------------------------------------------
setURLs() {
  local helpText="$1"
  local -n urlChoices=$2
  local -n urlOpts=$3
  local textArray=()
  local urls=()
  local key="${urlChoices[@]}"
  local pos=0

  IFS=$'\n' read -d '' -a textArray <<< "$helpText"
  for line in "${textArray[@]}"; do
    if [[ "$line" =~  "http" ]]; then
      if [[ "$line" =~  "LVM_on_LUKS" ]] || [[ "$line" =~  "systemd" ]]; then
        urls+=($(echo "${line}"))
      else
        pos=$(expr index "$line" -)
        if [ ${pos} -gt 0 ]; then
          pos=$(expr $pos + 1)
        fi
        urls+=($(echo "${line:${pos}}"))
      fi
    fi
  done

  if [ ${#urls[@]} -gt 1 ]; then
    urlOpts["$key"]="${urls[0]}"
    if [[ "${urls[-1]}" =~ "gentoo" ]]; then
      key="Gentoo wiki"
    elif [[ "${urls[-1]}" =~ "digitalocean" ]]; then
      key="DigitalOcean"
    elif [[ "${urls[-1]}" =~ "systemd" ]]; then
      key="systemd-swap"
    else
      key="Tutorial"
    fi
    urlChoices+=("$key")
    urlOpts["$key"]="${urls[-1]}"
  elif [ ${#urls[@]} -gt 0 ]; then
    urlChoices=("${urls[0]}")
    urlOpts=()
  else
    urlChoices=()
    urlOpts=()
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                   getNameOfVolumeGroup
# DESCRIPTION: Get the name of the Volume Group.  Set the value that was entered in
#              the global variable "ibEnteredText"
#  Required Params:
#      1) assocParamAry - associative array of key/value pairs of parameters for the method:
#         "dialogBackTitle" - Specifies a backtitle string to be displayed on the
#                             backdrop, at the top of the screen.
#             "dialogTitle" - String to be displayed at the top of the dialog box.
#---------------------------------------------------------------------------------------
getNameOfVolumeGroup() {
  local -n assocParamAry="$1"
  local dialogBackTitle="${assocParamAry[dialogBackTitle]}"
  local fields=()
  local textArray=("${assocParamAry[partTableStats]}" "$DASH_LINE_WIDTH_80" " "
  "The Volume Group is the highest level abstraction used within the LVM.  It"
  "gathers together a collection of Logical Volumes and Physical Volumes into"
  "one administrative unit."
  "$DASH_LINE_WIDTH_80" " " "Enter the name for the Volume Group:"
  )
  local helpText=$(getHelpTextForVolumeGroup "${assocParamAry[dialogPartTable]}")
  local dialogText=$(getTextForDialog "${textArray[@]}")
  local -A dialogOpts=(["dialogHeight"]=30 ["max-input"]=20 ["initValue"]="$INIT_VOL_GROUP" ["helpWidth"]=98
    ["help-button"]=true ["help-text"]="$helpText" ["helpURL"]=true)
  local urlRefChoices=("Arch Wiki")
  local -A urlDialogOpts=(["dialogHeight"]=20)

  setURLs "$helpText" urlRefChoices urlDialogOpts

  while true; do
    exec 3>&1
      showDialogInputBox "$dialogBackTitle" "$dialogText" "${assocParamAry[dialogTitle]}" dialogOpts 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$ibEnteredText"
    case ${fields[0]} in
      ${DIALOG_OK})
        ibEnteredText=$(echo "${fields[1]}")
        break
      ;;
      ${DIALOG_CANCEL})
        ibEnteredText=""
        break
      ;;
      $DIALOG_HELP)
        if [ "${fields[1]}" == "View-URL" ]; then
          if [ ${#urlRefChoices[@]} -gt 1 ]; then
            displayURLinElinks "$dialogBackTitle" urlRefChoices urlDialogOpts
          else
            elinks "${urlRefChoices[0]}"
          fi
        fi
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                 showSelectSwapTypeDialog
# DESCRIPTION: Show the linux "dialog" of type "--radiolist" to select the type of swap
#              to create and set it in the global variable "mbSelVal"
#  Required Params:
#      1) assocParamAry - associative array of key/value pairs of parameters for the method:
#                 "choices" - choices for the dialog separated by '\t'
#              "deviceType" - Logical Volume or Partition
#         "dialogBackTitle" - Specifies a backtitle string to be displayed on the
#                             backdrop, at the top of the screen.
#             "dialogTitle" - String to be displayed at the top of the dialog box.
#         "dialogPartTable" - string containing the rows of data for the
#                             partition table in a textual table format
#          "partTableStats" - summary table of the progress in the partitioning process
#                             in a textual table format
#---------------------------------------------------------------------------------------
showSelectSwapTypeDialog() {
  local -n assocParamAry=$1
  local dialogBackTitle="${assocParamAry[dialogBackTitle]}"
  local dialogTitle="${assocParamAry[dialogTitle]}"
  local textArray=("${assocParamAry[partTableStats]}" "$DASH_LINE_WIDTH_80" " "
  "NOTE:  Hitting ESC is the same as < Skip >" " "
  "$DASH_LINE_WIDTH_80" " " "Select the type of swap:"
  )
  local dialogText=$(getTextForDialog "${textArray[@]}")
  local urlRefChoices=("Swap")
  local -A urlDialogOpts=(["dialogHeight"]=20)
  local swapOptions=()
  IFS=$'\t' read -a swapOptions <<< "${assocParamAry["choices"]}"
  local helpText=$(getHelpTextForSwapType swapOptions "${assocParamAry[dialogPartTable]}")
  local -A dialogParms=(["no-collapse"]=true ["cancel-label"]="Skip" ["dialogHeight"]=30 ["help-button"]=true ["helpHeight"]=30
    ["helpWidth"]=98 ["help-text"]="$helpText" ["help-title"]="Help:  $dialogTitle" ["helpURL"]=true)
  local fields=()

  setURLs "$helpText" urlRefChoices urlDialogOpts

  while true; do
    exec 3>&1
      showListOfChoicesDialogBox "$dialogBackTitle" "$dialogTitle" "$dialogText" \
        swapOptions "radiolist" dialogParms 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$mbSelVal"
    case ${fields[0]} in
      $DIALOG_OK)
        local aryIdx=$(expr ${fields[1]} - 1)
        mbSelVal=$(echo "${swapOptions[$aryIdx]}")
        break
      ;;
      $DIALOG_CANCEL)
        mbSelVal="Skip"
        break
      ;;
      $DIALOG_HELP)
        if [ "${fields[1]}" == "View-URL" ]; then
          displayURLinElinks "$dialogBackTitle" urlRefChoices urlDialogOpts
        fi
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                   showDlgToKeepSwapSize
# DESCRIPTION: Show a linux "dialog" of type "yesno" to see if the current Swap size
#              should be kept.  Set the value in the global variable "inputRetVal".
#  Required Params:
#      1) assocParamAry - associative array of key/value pairs of parameters for the method:
#              "deviceName" - ame of the swap device/type
#         "dialogBackTitle" - Specifies a backtitle string to be displayed on the
#                             backdrop, at the top of the screen.
#             "dialogTitle" - String to be displayed at the top of the dialog box.
#              "partLayout" - the partition layout/scheme
#                 "pttDesc" - partition table type
#---------------------------------------------------------------------------------------
showDlgToKeepSwapSize() {
  local -n assocParamAry="$1"
  local deviceName="${assocParamAry[deviceName]}"
  local textArray=(
  "NOTE:  Hitting ESC is the same as < No >" " "
  $DASH_LINE_WIDTH_80 " "
  "The current size of the $deviceName is set to '${varMap["SWAP-SIZE"]}'." " "
  "$DASH_LINE_WIDTH_80" " "
  "Do you want to keep this value?"
  )
  local dialogText=$(getTextForDialog "${textArray[@]}")
  local cmdOpts=()
  local fields=()

  showYesNoBox "${assocParamAry[dialogBackTitle]}" "$dialogText" "${assocParamAry[dialogTitle]}" cmdOpts

  IFS=',' read -a fields <<< "$inputRetVal"
  case ${fields[0]} in
    $DIALOG_OK)
      inputRetVal="YES"
    ;;
    $DIALOG_CANCEL)
      inputRetVal="NO"
    ;;
  esac
}

#---------------------------------------------------------------------------------------
#      METHOD:                   getSizeForSwapDevice
# DESCRIPTION: Get the size to be allocated for the swap device which could be a file,
#              logical volume, or partition.  Set the size in human-readable format in
#              the global variable "mbSelVal"
#  Required Params:
#      1) assocParamAry - associative array of key/value pairs of parameters for the method
#              "deviceName" - Name of the logical volume or partition
#              "deviceType" - Logical Volume or Partition
#         "dialogBackTitle" - Specifies a backtitle string to be displayed on the
#                             backdrop, at the top of the screen.
#         "dialogPartTable" - string containing the rows of data for the
#                             partition table in a textual table format
#             "deviceSizes" - the min, max, & other sizes in human-readable format
#          "partTableStats" - summary table of the progress in the partitioning process
#                             in a textual table format
#---------------------------------------------------------------------------------------
getSizeForSwapDevice() {
  local -n assocParamAry="$1"
  local deviceSizes=()
  local dialogTitle="${assocParamAry[dialogTitle]}"
  local helpText=$(getHelpTextForSwapSize "${assocParamAry[dialogPartTable]}")
  local -A dialogAddOpts=(["dialogTitle"]="$dialogTitle" ["skipSizeFlag"]=true ["help-button"]=true
    ["help-text"]="$helpText" ["help-title"]="Help:  $dialogTitle" ["helpWidth"]=98)

  IFS=$'\t' read -d '' -a deviceSizes <<< "${assocParamAry[deviceSizes]}"
  deviceSizes[-1]=$(trimString "${deviceSizes[-1]}")

  assocParamAry["deviceType"]="Swap"
  assocParamAry["dialogTextHdr"]=$(getHeaderText deviceSizes)

  setHRSForDevice dialogAddOpts deviceSizes assocParamAry
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getHeaderText
# DESCRIPTION: Get the header portion of the text to be displayed in the "linux" dialog
#      RETURN: Concatenated string
#  Required Params:
#      1) hrSizes - array containing the calculated sizes (see function calcSwapSizes)
#---------------------------------------------------------------------------------------
function getHeaderText() {
  local -n hrSizes=$1
  local hdr=$(printf "%s,%s,%s,%s" "RAM installed" "W/O Hibernation" "W/ Hibernation" "Maximum")
  local dtl=$(printf "%10s,%9s,%11s,%6s" "${hrSizes[0]}" "${hrSizes[1]}" "${hrSizes[2]}" "${hrSizes[3]}")
  local hdrDtl=$(printf "%s\n%s" "$hdr" "$dtl")
  local tbl=$(printTable ',' "$hdrDtl" | sed 's/^\-\-\-/   /')
  local textArray=(
  "This machine has ${hrSizes[0]} of RAM configured/installed."
  "Recommended sizes:"
  "$tbl"
  )
  local dialogTextHdr=$(getTextForDialog "${textArray[@]}")
  echo "$dialogTextHdr"
}

#---------------------------------------------------------------------------------------
#      METHOD:                   selectRootDevice
# DESCRIPTION: Select the '/' or root logical volume or partition and set the selected
#              value in the global variable "mbSelVal"
#  Required Params:
#      1) paramArray - associative array of key/value pairs of parameters for the method
#                 "choices" - options that can be selected within the dialog
#              "deviceName" - Name of the logical volume or partition
#              "deviceType" - Logical Volume or Partition
#         "dialogBackTitle" - Specifies a backtitle string to be displayed on the
#                             backdrop, at the top of the screen.
#         "dialogPartTable" - string containing the rows of data for the
#                             partition table in a textual table format
#          "partTableStats" - summary table of the progress in the partitioning process
#                             in a textual table format
#---------------------------------------------------------------------------------------
selectRootDevice() {
  local -n paramArray="$1"
  local deviceType="${paramArray[deviceType]}"
  local dialogTitle="${paramArray[dialogTitle]}"
  local deviceOptions=()
  local helpText=$(getHelpTextForRoot "$deviceType" "${paramArray[dialogPartTable]}")
  local -A dialogParms=(["no-collapse"]=true ["dialogHeight"]=32 ["helpHeight"]=30 ["helpWidth"]=98
    ["help-label"]="More..." ["help-text"]="$helpText" ["help-title"]="Help:  $dialogTitle")

  IFS=$'\t' read -d '' -a deviceOptions <<< "${paramArray[choices]}"
  deviceOptions[-1]=$(trimString "${deviceOptions[-1]}")

  for key in "${deviceOptions[@]}"; do
    dialogParms["$key"]=$(echo "${paramArray[$key]}")
  done

  local textArray=("${paramArray[partTableStats]}" "$DASH_LINE_WIDTH_80" " "
  "The '/' or root $deviceType is necessary and it is the most important."
  "This is the point where the primary filesystem will be  mounted and from"
  "which all other filesystems stem. (More...)" "$DASH_LINE_WIDTH_80" " "
  "Select the $deviceType that is the ROOT:")
  local dialogText=$(getTextForDialog "${textArray[@]}")

  while true; do
    exec 3>&1
      showListOfChoicesDialogBox "${paramArray[dialogBackTitle]}" "$dialogTitle" "$dialogText" \
        deviceOptions "radiolist" dialogParms 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$mbSelVal"
    case ${fields[0]} in
      $DIALOG_OK)
        mbSelVal=$(echo "${fields[1]}")
        break
      ;;
      $DIALOG_CANCEL)
        mbSelVal=""
        break
      ;;
    esac
  done

}

#---------------------------------------------------------------------------------------
#      METHOD:               selectSwapFileSizeExceedOpts
# DESCRIPTION: Select from the options available when the Swap File Size exceeds
#              the amount of buffer space in the root partition or logical volume.
#              Set the value selected within the global
#      1) assocParamAry - associative array of key/value pairs of parameters for the method
#                 "choices" - string of the keys containing the eligible rows that can
#                             be removed and separated by '\t'
#              "deviceType" - Logical Volume or Partition
#         "dialogBackTitle" - Specifies a backtitle string to be displayed on the
#                             backdrop, at the top of the screen.
#         "dialogPartTable" - string containing the rows of data for the
#                             partition table in a textual table format
#---------------------------------------------------------------------------------------
selectSwapFileSizeExceedOpt() {
  local -n assocParamAry="$1"
  local menuChoices=()
  local -A dialogOpts=(["dialogHeight"]=27 ["dialogWidth"]=85 ["cancel-label"]="Skip" ["no-collapse"]=true ["help-label"]="View Part. Tbl"
      ["help-text"]="${assocParamAry[dialogPartTable]}" ["help-title"]="Partition Table" ["helpWidth"]=98)
  local dialogText=$(getTextForSFSEO)

  IFS=$'\t' read -d '' -a menuChoices <<< "${assocParamAry[choices]}"
  menuChoices[-1]=$(trimString "${menuChoices[-1]}")

  for key in "${menuChoices[@]}"; do
    dialogOpts["$key"]=$(echo "${assocParamAry[$key]}")
  done
  while true; do
    exec 3>&1
      showListOfChoicesDialogBox "${assocParamAry[dialogBackTitle]}" "${assocParamAry[dialogTitle]}" \
        "$dialogText" menuChoices "radiolist" dialogOpts 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$mbSelVal"
    case ${fields[0]} in
      $DIALOG_OK)
        mbSelVal=$(echo "${fields[1]}")
        break
      ;;
      $DIALOG_CANCEL)
        mbSelVal="Skip"
        break
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     getTextForSFSEO
# DESCRIPTION: Get the text to display on a linux "dialog" when the Swap File Size
#              exceeds the amount of buffer space in the root partition or
#              logical volume.
#---------------------------------------------------------------------------------------
function getTextForSFSEO() {
  local minRootSize="${ROOT_HR_SIZES[0]}"
  local minSize=$(humanReadableToBytes "$minRootSize")
  local rootDeviceSize=$(bytesToHumanReadable ${varMap["ROOT-DEVICE-SIZE"]})
  local spaceRemain=$(expr ${varMap["ROOT-DEVICE-SIZE"]} - $minSize)
  local hrBuffer=$(bytesToHumanReadable $spaceRemain)
  local hdrDtl=$(printf "%s,%s,%s,%s\n%s,%s,%s,%s" "ROOT Device Size" "Min. Size" "Buffer Space" "File Size" \
      "$rootDeviceSize" "$minRootSize" "$hrBuffer" "${varMap[SWAP-SIZE]}")
  local tbl=$(printTable ',' "$hdrDtl" | sed 's/^\-\-\-/   /' | sed -e 's/^[[:space:]]*//')
  local textArray=("$tbl" " "
  "The size of the file exceeds the buffer space." "$DASH_LINE_WIDTH_80" " "
  "NOTE:" "-----"
  "   * <     Skip     > - will prevent the Swap from being created"
  "   * <View Part. Tbl> - view all the devices configured in the partition table"
  "$DASH_LINE_WIDTH_80" " "
  "Select from one of the available options below:")
  local dialogText=$(getTextForDialog "${textArray[@]}")
  echo "$dialogText"
}

#---------------------------------------------------------------------------------------
#      METHOD:                 showWarningDialogForBootLV
# DESCRIPTION: Show the warning message within a yad "text-info" dialog and get
#              confirmation to continue or not.  Set the value in the global
#              variable "yesNoFlag"
#  Required Params:
#      1) dialogTitle - String to be displayed at the top of the dialog window.
#      2)     warnMsg - the warning message/text to display
#---------------------------------------------------------------------------------------
showWarningDialogForBootLV() {
  local dialogBackTitle="$1"
  local dialogTitle="$2"
  local warnText=$(getTextForWarning)
  local textArray=("$warnText" " " "$2" "$DASH_LINE_WIDTH_80" " "
                  "Do you want to continue?")
  local dialogText=$(getTextForDialog "${textArray[@]}")
  local -A cmdOpts=()

  exec 3>&1
    showYesNoBox "Logical Volume Manager (LVM)" "$dialogText" "$1" cmdOpts 2>&1 1>&3
  exec 3>&-
  IFS=',' read -a fields <<< "$inputRetVal"
  case ${fields[0]} in
    $DIALOG_OK)
      yesNoFlag=$(echo 'true' && return 0)
    ;;
    *)
      yesNoFlag=$(echo 'false' && return 1)
    ;;
  esac
}

#---------------------------------------------------------------------------------------
#      METHOD:                  dispSkipSwapMsg
# DESCRIPTION: Display message that the installer cannot continue creating the
#              swap
#  Required Params:
#      1)     dialogTitle - String to be displayed at the top of the dialog window.
#      2) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      3)        errorMsg - Error message
#---------------------------------------------------------------------------------------
dispSkipSwapMsg() {
  showErrorDialog "$2" "$1" "$3"
}
