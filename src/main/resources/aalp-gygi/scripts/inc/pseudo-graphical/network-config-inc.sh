#!/bin/bash
#set -e

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# functions/methods
#===============================================================================

#---------------------------------------------------------------------------------------
#      METHOD:                       dispInvFormVals
# DESCRIPTION: Displays the appropriate error message
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)        errorMsg - the error message to display
#---------------------------------------------------------------------------------------
dispInvFormVals() {
  local dialogBackTitle="$1"
  local errorMsg="$2"
  local dialogTitle=$(echo "Error:  Invalid or Missing Form Fields")
  local errTxt=$(getTextForError)
  local concatStr=$(printf ", '%s'" "${fieldNames[@]}")
  local msgTextArray=("$errTxt" " " "$errorMsg" "$DASH_LINE_WIDTH_80" " "
  "Please enter valid values for the form fields:  ${concatStr:2}"
  "                                   OR" " "
  "Select the <Cancel> button to choose a different connection configuration")

  local msgText=$(getTextForDialog "${msgTextArray[@]}")

  $DIALOG --clear --no-collapse --backtitle "$dialogBackTitle" --title "$dialogTitle" --msgbox "$msgText" 25 80 3>&1 1>&2 2>&3
}

#---------------------------------------------------------------------------------------
#      METHOD:                     showManualConfigForm
# DESCRIPTION: Displays a linux "form" dialog to enter an "IP Address",
#              "Submask", & "Gateway".  Sets the values entered in the
#              global variable "ibEnteredText".
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#---------------------------------------------------------------------------------------
showManualConfigForm() {
  local dialogBackTitle="$1"

  # open fd
  exec 3>&1
  ibEnteredText=$($DIALOG --clear --ok-label "Submit" --backtitle "$1" \
    --title "Manual Configuration of Ethernet/Wired Connection" \
    --form "***** All fields are required *" 15 75 0 \
    "* IP Address:" 1 1	"" 1 15 20 0 \
    "*    Submask:" 2 1	"" 2 15 20 0 \
    "*    Gateway:" 3 1	"" 3 15 20 0 \
  2>&1 1>&3)

  btnClick=$?
  # close fd
  exec 3>&-


  case ${btnClick} in
    ${DIALOG_OK})
      ibEnteredText=$(tr '\n' "$FORM_FLD_SEP" <<< "$ibEnteredText")
      ibEnteredText=$(echo "${DIALOG_OK},$ibEnteredText")
    ;;
    ${DIALOG_CANCEL})
      ibEnteredText=$(echo "${DIALOG_CANCEL},Cancel")
    ;;
    ${DIALOG_ESC})
      ibEnteredText=$(echo "${DIALOG_CANCEL},ESC")
    ;;
  esac
}

#---------------------------------------------------------------------------------------
#      METHOD:                   showDlgInputBoxForProxy
# DESCRIPTION: Displays a linux "input box" dialog to enter a proxy.
#              Sets the values entered in the global variable "ibEnteredText".
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#---------------------------------------------------------------------------------------
showDlgInputBoxForProxy() {
  local dialogBackTitle="$1"
  local textArray=(
  "Please enter the proxy in the form of \"protocol://address:port\"" " "
  "                                   OR" " "
  "Select the <Cancel> button to choose a different connection configuration"
  )

  local endText=$(getTextForDialog "${textArray[@]}")
  local errorMsg=""
  local dialogText="Enter the proxy in the form of \\\"protocol://address:port\\\":"
  local dialogTitle="Proxy Configuration"

  while true; do
    getValueFromDialogInputBox "$dialogBackTitle" "$dialogTitle" "$dialogText"
    IFS=',' read -a fields <<< "$ibEnteredText"
    case ${fields[0]} in
      ${DIALOG_OK})
        ibEnteredText=$(echo "${fields[1]}")
        break
      ;;
      ${DIALOG_CANCEL})
        ibEnteredText=""
        break
      ;;
      *)
        case ${fields[0]} in
          ${DIALOG_VALUE_ERR0R})
            errorMsg=$(printf "The proxy value \"%s\" is invalid!" "${fields[1]}")
          ;;
          ${DIALOG_INPUT_ERR0R})
            errorMsg="No value was entered for the proxy!"
          ;;
        esac
        dispInvValueEntered "$dialogBackTitle" "$errorMsg" "Proxy Entered" "$endText"
      ;;
    esac
  done
}
