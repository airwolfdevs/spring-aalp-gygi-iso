#!/bin/bash
#set -e

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# globals
#===============================================================================
declare SEL_CHKLIST_OPT=()
declare -A TAB_ASSOC_ARRAY=()

#===============================================================================
# functions/methods
#===============================================================================

#---------------------------------------------------------------------------------------
#      METHOD:                     showPostInstOption
# DESCRIPTION: Show a linux "dialog" box of type "yes no" to determine if to continue or
#              skip this step.
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)     dialogTitle - String to be displayed at the top of the dialog box.
#      3)      dialogText - Text to display inside the dialog's text area
#      4)        helpText - Text to display on the help/more dialog
#---------------------------------------------------------------------------------------
showPostInstOption() {
  local dialogBackTitle="$1"
  local dialogTitle="$2"
  local dialogText=$(getAppendedTextToLogo "$3")
  local helpText="$4"
  local -A cmdOpts=(["yes-label"]="Continue" ["no-label"]="Skip" ["dialogHeight"]=35 ["dialogWidth"]=98 ["extra-label"]="View URL"
    ["help-button"]=true ["help-text"]="$helpText" ["help-title"]="Help:  $dialogTitle")
  local urls=()

  setELinksURLs "$helpText"

  while true; do
    yesNoFlag=$(echo 'false' && return 1)
    exec 3>&1
      showYesNoBox "$dialogBackTitle" "$dialogText" "$dialogTitle" cmdOpts 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$inputRetVal"

    case ${fields[0]} in
      $DIALOG_OK)
        yesNoFlag=$(echo 'true' && return 0)
        break
      ;;
      $DIALOG_CANCEL)
        break
      ;;
      ${DIALOG_EXTRA})
        elinks "${urls[0]}"
      ;;
      $DIALOG_HELP)
        if [ "${fields[1]}" == "View-URL" ]; then
          elinks "${urls[1]}"
        fi
        break
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getAppendedTextToLogo
# DESCRIPTION: Append the text to the ASCII text logo
#      RETURN: concatenated string
#  Required Params:
#      1) fmtText - Formatted text to append
#---------------------------------------------------------------------------------------
function getAppendedTextToLogo() {
  local logoText=$(getLogo)
  local lclTextArray=("$logoText" " " "$1")
  local concatStr=$(printf "\n%s" "${lclTextArray[@]}")
  concatStr=${concatStr:1}

  echo "$concatStr"
}

#---------------------------------------------------------------------------------------
#      METHOD:                   getDefaultTextEditor
# DESCRIPTION: Get the command line text editor used in modifying configuration
#              options and files.
#      RETURN: AUR package name in the global "mbSelVal" variable
#  Required Params:
#      1) opts - array of text editor AUR package/application names
#---------------------------------------------------------------------------------------
getDefaultTextEditor() {
  local -n opts=$1
  while true; do
    exec 3>&1
      selectDefaultEditor opts 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$mbSelVal"
    case ${fields[0]} in
      $DIALOG_OK)
        mbSelVal=$(echo "${fields[1]}")
        break
      ;;
      $DIALOG_CANCEL)
        mbSelVal="vi"
        break
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:              selectPartitionProcessType
# DESCRIPTION: Select the text editor from a linux "dialog" box
#              of type radio list
#  Required Params:
#      1) menuOptions - array of text editor AUR package/application names
#---------------------------------------------------------------------------------------
selectDefaultEditor() {
  local -n menuOptions=$1
  local textArray=( " "
  "This will be the command line text editor used in modifying configuration"
  "options and files." " "
  "The current Default Text Editor is:  [vi]"
  "(More...)"
  "$DASH_LINE_WIDTH_80" " "
  "Choose the text editor that is to be the default:"
  )

  local dialogText=$(getTextForDialog "${textArray[@]}")
  local helpText=$(getTextForDefaultEditor)
  local dialogTitle="Default Text Editor"

  local -A dialogParms=(["defaultChoice"]="vi" ["dialogHeight"]=30 ["help-label"]="More..." ["help-text"]="$helpText"
  ["help-title"]="Help: $dialogTitle")

  for opt in "${menuOptions[@]}"; do
    dialogParms["$opt"]=""
  done

  showListOfChoicesDialogBox "$DIALOG_BACK_TITLE" "$dialogTitle" "$dialogText" menuOptions "radiolist" dialogParms
}

#---------------------------------------------------------------------------------------
#      METHOD:                  showPostInstallChecklist
# DESCRIPTION: Displays the checklist for the post-installation step within a
#              linux "radiolist" dialog.  Set the selected value within the
#              global array variable "SEL_CHKLIST_OPT".
#        NOTE:  The calling method must declare an associative array variable
#               with the name of "piAssocArray" with the following key/value pairs:
#          "defaultVal" - value of the menu option to set the radio button to checked
#            "helpText" - text to display within the help dialog
#           "optsArray" - array of menu options that can be selected
#      "descAssocArray" - associative array of descriptions for the menu options
#---------------------------------------------------------------------------------------
showPostInstallChecklist() {
  local dialogText="Select the 'Software Task' you want to execute/run:"
  local dialogTitle="Checklist of $POST_INST_DIALOG_BACK_TITLE"
  local helpText="${piAssocArray["helpText"]}"
  local -A dialogParms=(["defaultChoice"]="${piAssocArray["defaultVal"]}" ["dialogHeight"]=20 ["dialogWidth"]=80
    ["help-label"]="Help" ["help-text"]="$helpText" ["help-title"]="Help: $POST_INST_DIALOG_BACK_TITLE"
    ["column-separator"]="$FORM_FLD_SEP" ["cancel-label"]="Exit"
    )
  local -n menuOptions="${piAssocArray["optsArray"]}"
  local -n descArray="${piAssocArray["descAssocArray"]}"
  local urls=()

  if [ ${piAssocArray["addSkipBtn"]+_} ]; then
    dialogParms["extra-label"]="Skip"
  fi

  setELinksURLs "$helpText"

  setDataForMenuChecklist "dialogParms"

  while true; do
    exec 3>&1
      showListOfChoicesDialogBox "$POST_INST_DIALOG_BACK_TITLE" "$dialogTitle" "$dialogText" menuOptions "radiolist" dialogParms 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$mbSelVal"
    case ${fields[0]} in
      $DIALOG_OK)
        mbSelVal=$(echo "${fields[1]//\"/}")
        SEL_CHKLIST_OPT=("$mbSelVal" "${descArray["$mbSelVal"]}")
        break
      ;;
      $DIALOG_HELP)
        if [ "${fields[1]}" == "View-URL" ]; then
          elinks "${urls[0]}"
        fi
      ;;
      ${DIALOG_EXTRA})
        unset piAssocArray["addSkipBtn"]
        piAssocArray["skipBtn"]="true"
        mbSelVal=$(printf "%s%sSkip" "${SEL_CHKLIST_OPT[0]}" "$FORM_FLD_SEP")
        break
      ;;
      *)
        SEL_CHKLIST_OPT=()
        break
      ;;
   esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                   setDataForMenuChecklist
# DESCRIPTION: Set the descriptions for the checklist and append either"
#                  "[ ]" - step/task is NOT done
#                  "[S]" - step/task has been Skipped
#                  "[X]" - step/task is done
#  Required Params:
#      1) dlgParamArray - name of the array containing the options for the linux dialog
#  associative array "piAssocArray" with key/value pairs:
#      "chklArray"      - name of the array to reference that is the checklist for the
#                         category/software task.
#      "descAssocArray" - name of the associative array to reference containing the
#                         descriptions of the values that can be selected
#      "optsArray"      - name of the array to reference containing the values that
#                         can be selected
#---------------------------------------------------------------------------------------
setDataForMenuChecklist() {
  local -n dlgParamArray="$1"
  local -n chklArray="${piAssocArray["chklArray"]}"
  local -n descAssocArray="${piAssocArray["descAssocArray"]}"
  local -n optsArray="${piAssocArray["optsArray"]}"
  local stepTask=()

  for menuOpt in "${optsArray[@]}"; do
    readarray -t stepTask <<< "${menuOpt//\./$'\n'}"
    local chklIdx=${stepTask[-1]}
    local idxVal=${chklArray[${chklIdx}]}
    local chkBox=""

    if [ ${idxVal} -gt 1 ]; then
      chkBox="[ ]"
    elif [ ${idxVal} -gt 0 ]; then
      chkBox="[X]"
    elif [ ${idxVal} -lt 0 ]; then
      chkBox="[S]"
    else
      chkBox="[ ]"
    fi

    dlgParamArray["$menuOpt"]=$(echo "${descAssocArray[$menuOpt]}${FORM_FLD_SEP}$chkBox")
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                 showWarningDialog
# DESCRIPTION:  Show the warning message within a linux "dialog" of type message box
#  Required Params:
#      1) dialogTitle - String to be displayed at the top of the dialog window.
#      2)     warnMsg - the warning message/text to display
#---------------------------------------------------------------------------------------
showWarningDialog() {
  $DIALOG --clear  --no-collapse --title "WARNING - $1!!!" --msgbox "$2" 25 80 3>&1 1>&2 2>&3
}

#---------------------------------------------------------------------------------------
#      METHOD:                      setNextSetupTask
# DESCRIPTION: Sets the next task to be done and its description for the current step
#              within the global variable "mbSelVal".
#---------------------------------------------------------------------------------------
setNextTaskForStep() {
  local dialogText="Select \"Ok\" to execute the selected task!"
  local dialogTitle="${SEL_CHKLIST_OPT[0]} - ${SEL_CHKLIST_OPT[1]}"
  local helpText="${piAssocArray["helpText"]}"
  local -A dialogParms=(["defaultChoice"]="${piAssocArray["defaultVal"]}" ["dialogHeight"]=25 ["dialogWidth"]=80
    ["help-label"]="Help" ["help-text"]="$helpText" ["help-title"]="${piAssocArray["helpTitle"]}"
    ["column-separator"]="$FORM_FLD_SEP")
  local -n menuOptions="${piAssocArray["optsArray"]}"
  local -n descArray="${piAssocArray["descAssocArray"]}"
  local urls=()

  setELinksURLs "$helpText"

  local key="extra-label"
  if [ ${piAssocArray["$key"]+_} ]; then
    dialogParms["$key"]="${piAssocArray["$key"]}"
  fi
  key="dialogText"
  if [ ${piAssocArray["$key"]+_} ]; then
    dialogText="${piAssocArray["$key"]}"
  fi

  setDataForMenuChecklist "dialogParms"

  while true; do
    exec 3>&1
      showListOfChoicesDialogBox "$POST_INST_DIALOG_BACK_TITLE" "$dialogTitle" "$dialogText" menuOptions "radiolist" dialogParms 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$mbSelVal"
    case ${fields[0]} in
      $DIALOG_OK)
        mbSelVal=$(echo "${fields[1]//\"/}")
        mbSelVal=$(printf "%s%s%s" "$mbSelVal" "$FORM_FLD_SEP" "${descArray["$mbSelVal"]}")
        break
      ;;
       ${DIALOG_EXTRA})
        mbSelVal=$(printf "%s%sSkip" "${SEL_CHKLIST_OPT[0]}" "$FORM_FLD_SEP")
        break
      ;;
      $DIALOG_HELP)
        if [ "${fields[1]}" == "View-URL" ]; then
          elinks "${urls[0]}"
        fi
      ;;
      *)
        mbSelVal=""
        break
      ;;
   esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                   showConfirmationDialog
# DESCRIPTION: Show a linux "menu" dialog to get confirmation to install the AUR packages
#              that are listed.
#  Required Params:
#      1) paramAssocArray - the name of the associative array with the required
#                           key/value pairs:
#      "dialogTitle"   - String to be displayed at the top of the dialog window.
#      "dialogText"    - Text to display inside the dialog before the list of menu options
#      "dialogTextHdr" - String to be displayed at the top of the text area.
#      "helpText"      - Text to display on the help/more dialog
#      "menuOptsRef"   - name of the array containing the menu options
#      "optDescsRef"   - name of the associative array containing the description
#                        of the menu options
#---------------------------------------------------------------------------------------
showConfirmationDialog() {
  local -n paramAssocArray="$1"
  local dialogText="${paramAssocArray["dialogText"]}"
  local dialogTitle="${paramAssocArray["dialogTitle"]}"
  local windowTitle="${paramAssocArray["dialogBackTitle"]}"
  local helpText="${paramAssocArray["helpText"]}"
  local -A dialogParms=(["dialogHeight"]=25 ["dialogWidth"]=98 ["ok-label"]="Confirm" ["cancel-label"]="Skip"
    ["help-label"]="Help" ["help-text"]="$helpText" ["help-title"]="Help:  $dialogTitle")
  local -n menuOpts="${paramAssocArray["menuOptsRef"]}"
  local urls=()

  if [ ${paramAssocArray["cancel-label"]+_} ]; then
    dialogParms["cancel-label"]="${paramAssocArray["cancel-label"]}"
  fi

  setELinksURLs "$helpText"

  addDescsToDlgParams "${paramAssocArray["menuOptsRef"]}" "${paramAssocArray["optDescsRef"]}" "dialogParms"

  yesNoFlag=$(echo 'false' && return 1)
  while true; do
    exec 3>&1
      showListOfChoicesDialogBox "$windowTitle" "$dialogTitle" "$dialogText" menuOpts "menu" dialogParms 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$mbSelVal"
    case ${fields[0]} in
      $DIALOG_OK)
        yesNoFlag=$(echo 'true' && return 0)
        break
      ;;
      $DIALOG_HELP)
        if [ "${fields[1]}" == "View-URL" ]; then
          elinks "${urls[0]}"
        fi
      ;;
      *)
        yesNoFlag=$(echo 'false' && return 1)
        break
      ;;
   esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                     addDescsToDlgParams
# DESCRIPTION: Add the key/value pairs to dialog parameters associative array where:
#                      key = name of the menu option
#                    value = description of the menu option
#  Required Params:
#      1)    menuOptions - name of the array containing the menu options
#      2) descAssocArray - name of the associative array that contains the description
#                          of the menu options
#      3)  dlgParamArray - name of the associative array that the key/value pairs
#                          will be added to.
#---------------------------------------------------------------------------------------
addDescsToDlgParams() {
  local -n menuOptions="$1"
  local -n descAssocArray="$2"
  local -n dlgParamArray="$3"

  for key in "${menuOptions[@]}"; do
    dlgParamArray["$key"]=$(echo "${descAssocArray[$key]}")
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                     showLinuxListDialog
# DESCRIPTION: Show the options that can be selected using a linux "dialog" of either
#              type check list, menu, or radio list. Set the value within the
#              global variable "mbSelVal".
#  Required Params:
#      1) dlgMethodParams - the name of the associative array with the required
#                           key/value pairs:
#      "dialogTitle"   - String to be displayed at the top of the dialog window.
#      "dialogText"    - Text to display inside the dialog before the list of menu options
#      "dialogTextHdr" - String to be displayed at the top of the text area.
#      "helpText"      - Text to display on the help/more dialog
#      "menuOptsRef"   - name of the array containing the menu options
#      "optDescsRef"   - name of the associative array containing the description
#                        of the menu options
#---------------------------------------------------------------------------------------
showLinuxListDialog() {
  local -n dlgMethodParams="$1"
  local dialogText="${dlgMethodParams["dialogText"]}"
  local dialogTitle="${dlgMethodParams["dialogTitle"]}"
  local windowTitle="${dlgMethodParams["dialogBackTitle"]}"
  local helpText="${dlgMethodParams["helpText"]}"
  local listType="radiolist"
  local -A dialogParms=(["dialogHeight"]=25 ["dialogWidth"]=98
    ["help-label"]="Help" ["help-text"]="$helpText" ["help-title"]="Help:  $dialogTitle")
  local -n menuOpts="${dlgMethodParams["menuOptsRef"]}"
  local urls=()

  setELinksURLs "$helpText"
  overrideOpts

  addDescsToDlgParams "${dlgMethodParams["menuOptsRef"]}" "${dlgMethodParams["optDescsRef"]}" "dialogParms"

  while true; do
    exec 3>&1
      showListOfChoicesDialogBox "$windowTitle" "$dialogTitle" "$dialogText" menuOpts "$listType" dialogParms 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$mbSelVal"
    case ${fields[0]} in
      $DIALOG_OK)
        mbSelVal=$(echo "${fields[1]//\"/}")
        break
      ;;
      $DIALOG_HELP)
        if [ "${fields[1]}" == "View-URL" ]; then
          elinks "${urls[0]}"
        fi
      ;;
      *)
        if [ ! ${dialogParms["nocancel"]+_} ]; then
          mbSelVal=""
          break
        fi
      ;;
   esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                        overrideOpts
# DESCRIPTION: Override the default options for a Linux "dialog" that are in the
#              associative array variable named "dialogParms" that was declared within
#              the calling method.
#---------------------------------------------------------------------------------------
overrideOpts() {
  local optKeys=("cancel-label" "defaultChoice" "dialogHeight" "dialogWidth" "nocancel" "listType" "output-separator")

  for optKey in "${optKey[@]}"; do
    if [ ${dlgMethodParams["$optKey"]+_} ]; then
     case "$optKey" in
      "cancel-label"|"defaultChoice"|"dialogHeight"|"dialogWidth"|"output-separator")
        dialogParms["$optKey"]="${dlgMethodParams["$optKey"]}"
      ;;
      "nocancel")
        dialogParms["nocancel"]="true"
      ;;
      "listType")
        listType="${dlgMethodParams["listType"]}"
      ;;
    esac
   fi
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                   showSkipBtnConfWarning
# DESCRIPTION: Get confirmation to of whether or not to continue with skipping the.
#              remaining tasks that are either NOT done or skipped.
#  Required Params:
#      1) paramAssocArray - the name of the associative array with the required
#                           key/value pairs:
#      "dialogBackTitle" - The string to be displayed on the backdrop, at the top
#                          of the screen.
#      "dialogTitle"     - String to be displayed at the top of the dialog box.
#      "dialogText"      - Text to display in the text area of the dialog
#---------------------------------------------------------------------------------------
showSkipBtnConfWarning() {
  local -n paramAssocArray="$1"
  local dialogBackTitle="${paramAssocArray["dialogBackTitle"]}"
  local dialogTitle="${paramAssocArray["dialogTitle"]}"
  local warnText=$(getTextForWarning)
  local dialogText=$(echo "$warnText ${paramAssocArray["dialogText"]}")
  local -A cmdOpts=(["yes-label"]="Continue" ["no-label"]="Cancel" ["dialogHeight"]=25)

  exec 3>&1
    showYesNoBox "$dialogBackTitle" "$dialogText" "$dialogTitle" cmdOpts 2>&1 1>&3
  exec 3>&-
  IFS=',' read -a fields <<< "$inputRetVal"

  case ${fields[0]} in
    $DIALOG_OK)
      yesNoFlag=$(echo 'true' && return 0)
    ;;
    $DIALOG_CANCEL)
      yesNoFlag=$(echo 'false' && return 1)
    ;;
  esac
}

#---------------------------------------------------------------------------------------
#      METHOD:                   showSoftwareOptsForCat
# DESCRIPTION: Displays the list of sub-categories and/or AUR software packages that can
#              be selected using a linux "dialog" of type "radiolist".
#  Required Params:
#    1) assocArray - the name of the associative array with the required key/value pairs
#    2)     optSel - the option that was selected
#    3)    optDesc - the description/name of the option that was selected
#---------------------------------------------------------------------------------------
showSoftwareOptsForCat() {
  local -n assocArray="$1"
  local optSel="$2"
  local optDesc="$3"
  local dialogText="${assocArray["dialogText"]}"
  local windowTitle="${SEL_CHKLIST_OPT[0]} - ${SEL_CHKLIST_OPT[1]}"
  local dialogTitle="$2 - $3"
  local helpText="${assocArray["helpText"]}"
  local sep="|"
  local -A dialogParms=(["defaultChoice"]="${assocArray["defaultVal"]}" ["dialogHeight"]=25 ["dialogWidth"]=90
    ["help-label"]="Help" ["help-text"]="$helpText" ["help-title"]="${assocArray["helpTitle"]}"
    ["column-separator"]="$sep" ["output-separator"]="$sep" )
  local -n menuOptions="${assocArray["optsArray"]}"
  local -n descArray="${assocArray["descAssocArray"]}"
  local menuOptKeys=()
  local keyFmt="aur#%02d"
  local optNum=1
  local errorTextArray=("There were NO AUR packages selected within the dialog '$dialogTitle'. "
    "Select <Cancel> or < Skip > if you don't want to install any packages from the '$3' category.")
  local errorMsg=$(printf " %s" "${errorTextArray[@]}")
  local urls=()

  setELinksURLs "$helpText"

  local key="extra-label"
  if [ ${piAssocArray["$key"]+_} ]; then
    dialogParms["$key"]="${piAssocArray["$key"]}"
  fi

  for menuOpt in "${menuOptions[@]}"; do
    local desc=$(printf "%s%s%s" "$menuOpt" "$sep" "${descArray["$menuOpt"]}")
    if [ ${#desc} -gt 71 ]; then
      desc="${desc:0:67}..."
    fi
    local aurKey=$(printf "$keyFmt" ${optNum})
    menuOptKeys+=("$aurKey")
    dialogParms["$aurKey"]="$desc"
    optNum=$(expr ${optNum} + 1)
  done

  while true; do
    exec 3>&1
      showListOfChoicesDialogBox "$windowTitle" "$dialogTitle" "$dialogText" menuOptKeys "checklist" dialogParms 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$mbSelVal"
    echo "${DIALOG_EXTRA}, ${fields[0]}" > /tmp/debug.log
    case ${fields[0]} in
      $DIALOG_OK)
        mbSelVal=$(echo "${fields[1]//\"/}")
        mbSelVal=$(getSelectedPackages "dialogParms" "$sep")
        if [ ${#mbSelVal} -gt 1 ]; then
          break
        else
          dispErrMsgDlg "$dialogTitle" "${errorMsg:1}"
        fi
      ;;
       ${DIALOG_EXTRA})
        mbSelVal=$(printf "%s%sSkip" "$optSel" "$FORM_FLD_SEP")
        break
      ;;
      $DIALOG_HELP)
        if [ "${fields[1]}" == "View-URL" ]; then
          elinks "${urls[0]}"
        fi
      ;;
      *)
        mbSelVal=""
        break
      ;;
   esac
  done
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     getSelectedPackages
# DESCRIPTION: Get the names of the AUR packages that match the keys within the
#              global string variable "$mbSelVal".
#      RETURN: concatenated string of AUR package names separated by the "$sep" variable.
#  Required Params:
#    1) dialogParmsRef - the name of the associative array containing the key/value pairs
#                        displayed in the dialog.
#    2)            sep - the "column-separator" & "output-separator"
#---------------------------------------------------------------------------------------
function getSelectedPackages() {
  local -n dialogParmsRef="$1"
  local sep="$2"
  local aurKeys=()
  local aurPckgs=()
  local nameDesc=()

  readarray -t aurKeys <<< "${mbSelVal//$sep/$'\n'}"

  for aurKey in "${aurKeys[@]:1}"; do
    readarray -t nameDesc <<< "${dialogParmsRef["$aurKey"]//$sep/$'\n'}"
    aurPckgs+=("${nameDesc[0]}")
  done

  local concatStr=$(printf "|%s" "${aurPckgs[@]}")
  echo "$concatStr"
}

#---------------------------------------------------------------------------------------
#      METHOD:                      showSoftwareOptsForSubCat
# DESCRIPTION: Displays a list of sub-categories within a linux "dialog" of type "radiolist"
#              Set the selected packages within the global variable "mbSelVal".
#  Required Params:
#    1) assocArray - the name of the associative array with the required key/value pairs
#    2)     optSel - the option that was selected
#    3)    optDesc - the description/name of the option that was selected
#---------------------------------------------------------------------------------------
showSoftwareOptsForSubCat() {
  local -n assocArray="$1"
  local optSel="$2"
  local optDesc="$3"
  local dialogText="${assocArray["dialogText"]}"
  local dialogTitle="${SEL_CHKLIST_OPT[0]} - ${SEL_CHKLIST_OPT[1]}"
  local helpText="${assocArray["helpText"]}"
  local -A dialogParms=(["defaultChoice"]="${assocArray["defaultVal"]}" ["dialogHeight"]=25 ["dialogWidth"]=90
    ["cancel-label"]="Skip" ["extra-label"]="Commit" ["help-label"]="Help" ["help-text"]="$helpText"
    ["help-title"]="${assocArray["helpTitle"]}")
  local -n menuOptions="${assocArray["optsArray"]}"
  local -n descArray="${assocArray["descAssocArray"]}"
  local -A catAssocArray=()
  local -A savedTabVals=()
  local dialogBackTitle="$2 - $3"
  local errorTextArray=("There were NO AUR packages selected from any of the sub-categories.  Select"
    "< Skip > if you don't want to install any packages from the sub-categories.  Otherwise,"
    "select the packages from the sub-categories in order to commit them to be installed!")
  local errorMsg=$(printf " %s" "${errorTextArray[@]}")

  for key in "${!descArray[@]}"; do
    local desc="${descArray["$key"]}"
    catAssocArray["$key"]="${CATEGORY_MENU_OPTS["$desc"]}"
  done

  for menuOpt in "${menuOptions[@]}"; do
    local desc="${descArray["$menuOpt"]}"
    if [ ${#desc} -gt 53 ]; then
      desc="${desc:0:53}..."
    fi
    dialogParms["$menuOpt"]="$desc"
  done

  while true; do
    exec 3>&1
      showListOfChoicesDialogBox "$POST_INST_DIALOG_BACK_TITLE" "$dialogTitle" "$dialogText" menuOptions "radiolist" dialogParms 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$mbSelVal"
    case ${fields[0]} in
      $DIALOG_OK)
        mbSelVal=$(echo "${fields[1]//\"/}")
        cleanDialogFiles
        showSubCatOptsDialog "$dialogBackTitle" "$mbSelVal" "${descArray["$mbSelVal"]}" "${catAssocArray["$mbSelVal"]}"
        cleanDialogFiles
      ;;
       ${DIALOG_EXTRA})
        if [ ${#savedTabVals[@]} -gt 0 ]; then
          local aurPckgs=()
          for key in "${!savedTabVals[@]}"; do
            aurPckgs+=("${savedTabVals["$key"]}")
          done
          mbSelVal=$(printf "|%s" "${aurPckgs[@]}")
          break
        else
          dispErrMsgDlg "$dialogBackTitle" "${errorMsg:1}"
        fi
      ;;
      ${DIALOG_CANCEL}|${DIALOG_ESC})
        mbSelVal=$(printf "%s%sSkip" "$optSel" "$FORM_FLD_SEP")
        break
      ;;
      *)
        echo "${fields[0]}" >> /tmp/debug.log
        break
      ;;
   esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                   showSubCatOptsDialog
# DESCRIPTION: Displays a list of the AUR Packages within a linux "dialog" of either type
#              "checklist" or "radiolist".  Save the selected values within
#  Required Params:
#    1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                         backdrop, at the top of the screen.
#    2)          optSel - the option that was selected
#    3)         optDesc - the description/name of the option that was selected
#    4)     menuOptsStr - concatenated string of AUR package names display separated by "|"
#---------------------------------------------------------------------------------------
showSubCatOptsDialog() {
  local dlgText="Select the AUR packages to install:"
  local dlgTitle="$2 - $3"
  local sep="|"
  local -A subCatDialogParms=(["ok-label"]="Save" ["dialogHeight"]=25 ["dialogWidth"]=90
    ["help-label"]="Help" ["help-title"]="Help:  $dlgTitle"
    ["column-separator"]="$sep" ["output-separator"]="$sep")
  local urls=()
  local urlRefChoices=("Arch Wiki" "Spring Tools")
  local -A urlDialogOpts=(["dialogHeight"]=20)
  local menuOptKeys=()
  local errorTextArray=("There were NO AUR packages selected within the dialog '$dlgTitle'. "
    "Select <Cancel> if you don't want to install any packages from the '$3' sub-category.")
  local errorMsg=$(printf " %s" "${errorTextArray[@]}")

  setDataForSubCatDialog "$3" "$4" "menuOptKeys" "subCatDialogParms" "$sep"

  local urls=()
  setELinksURLs "${subCatDialogParms["help-text"]}"

  local listType="${subCatDialogParms["listType"]}"
  unset subCatDialogParms["listType"]

  while true; do
    exec 3>&1
      showListOfChoicesDialogBox "$1" "$dlgTitle" "$dlgText" menuOptKeys "$listType" subCatDialogParms 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$mbSelVal"
    case ${fields[0]} in
      $DIALOG_OK)
        mbSelVal=$(echo "${fields[1]//\"/}")
        mbSelVal=$(getSelectedPackages "subCatDialogParms" "$sep")
        if [ ${#mbSelVal} -gt 1 ]; then
          savedTabVals["$2"]="${mbSelVal:1}"
          break
        else
          dispErrMsgDlg "$dlgTitle" "${errorMsg:1}"
        fi
      ;;
      $DIALOG_HELP)
        if [ "${fields[1]}" == "View-URL" ]; then
          if [ ${#urls[@]} -gt 1 ]; then
            displayURLinElinks "$1" urlRefChoices urlDialogOpts
          elif [ ${#urls[@]} -gt 0 ]; then
            elinks "${urls[0]}"
          fi
        fi
      ;;
      *)
        unset savedTabVals["$2"]
        break
      ;;
   esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                   setDataForSubCatDialog
# DESCRIPTION: Set the necessary data needed to display a linux "dialog" of either type
#              "checklist" or "radiolist" for a sub-category
#  Required Params:
#    1)      optDesc - the description/name of the sub-category option that was selected
#    2)  menuOptsStr - concatenated string of AUR package names display separated by "|"
#    3)   optKeysRef - name of the array that will hold values for the menu options
#    4) subCatDlgRef - name of the associative array containing the data for the dialog
#    5)          sep - the "column-separator" & "output-separator"
#        NOTE:  The arrays "descArray" & "savedTabVals" are declared in the
#               method "showSoftwareOptsForSubCat"
#---------------------------------------------------------------------------------------
setDataForSubCatDialog() {
  local -n optKeysRef="$3"
  local -n subCatDlgRef="$4"
  local sep="$5"
  local subCatMenuOpts=()
  local keyFmt="aur#%02d"
  local optNum=1
  local -n aurPckgDescs="AUR_PCKG_DESCS"

  if [ ${savedTabVals["$1"]+_} ]; then
    subCatDlgRef["checklistVals"]="${savedTabVals["$1"]}"
  fi

  readarray -t subCatMenuOpts <<< "${2//$sep/$'\n'}"
  case "$1" in
    "Eclipse")
      subCatDlgRef["listType"]="radiolist"
      subCatDlgRef["help-text"]=$(getHelpTextForEclipse)
    ;;
    "IntelliJ")
      subCatDlgRef["listType"]="radiolist"
      subCatDlgRef["help-text"]=$(getHelpTextForIntelliJ)
    ;;
    *)
      subCatDlgRef["listType"]="checklist"
      subCatDlgRef["help-text"]=$(getHelpTextForSubCatBtns)
    ;;
  esac

  for menuOpt in "${subCatMenuOpts[@]}"; do
    local desc=$(printf "%s%s%s" "$menuOpt" "$sep" "${aurPckgDescs["$menuOpt"]}")
    if [ ${#desc} -gt 71 ]; then
      desc="${desc:0:67}..."
    fi
    local aurKey=$(printf "$keyFmt" ${optNum})
    optKeysRef+=("$aurKey")
    subCatDlgRef["$aurKey"]="$desc"
    optNum=$(expr ${optNum} + 1)
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                        dispErrMsgDlg
# DESCRIPTION: Displays the appropriate error message
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)          errMsg - the error message to be displayed in the text area
#---------------------------------------------------------------------------------------
dispErrMsgDlg() {
  local dialogBackTitle="$1"
  local dialogTitle=$(echo "Error:  No AUR packages selected")
  local errTxt=$(getTextForError)
  local msgTextArray=("$errTxt" " " "$2")

  msgText=$(getTextForDialog "${msgTextArray[@]}")

  $DIALOG --clear --no-collapse --backtitle "$1" --title "$dialogTitle" --msgbox "$msgText" 25 80 3>&1 1>&2 2>&3
}

#---------------------------------------------------------------------------------------
#      METHOD:                     selectConkyPackage
# DESCRIPTION: Select the option to install the Conky package using a linux "radiolist"
#              dialog.  Set the value within the global variable "mbSelVal".
#  Required Params:
#      1) paramAssocArray - the name of the associative array with the required
#                           key/value pairs:
#      "dialogBackTitle" - String to be displayed at the top of the window.
#      "dialogTitle"     - String to be displayed at the top of the dialog window.
#      "dialogText"      - Text to display inside the dialog before the list of menu options
#      "helpText"        - Text to display on the help/more dialog
#      "menuOptsRef"     - name of the array containing the menu options
#      "optDescsRef"     - name of the associative array containing the description
#                          of the menu options
#---------------------------------------------------------------------------------------
selectConkyPackage() {
  local -n paramAssocArray="$1"
  paramAssocArray["nocancel"]="true"

  showLinuxListDialog "paramAssocArray"
}
