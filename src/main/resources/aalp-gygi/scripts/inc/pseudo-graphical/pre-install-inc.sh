#!/bin/bash
#set -e

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# functions/methods
#===============================================================================

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getKeyboardLayout
# DESCRIPTION: Get the keyboard configuration/layout.  Set the select keyboard layout in
#              the global variable "mbSelVal".
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)     dialogTitle - String to be displayed at the top of the dialog box.
#      3)   defaultLayout - the default keyboard layout
#      4)     defCtryCode - the default country code based on the geographical location
#---------------------------------------------------------------------------------------
getKeyboardLayout() {
  local concatStr=""
  local numDialogs=4
  local dialogNum=0
  local selData=()
  local -A dialogOpts1=()
  local -A dialogOpts2=()
  local -A dialogOpts3=()
  local -A dialogOpts4=()
  local kblOptions=()
  local kblOtherOpts=()
  local ctryCode="$4"
  local aryIdx=0
  local -A dialogParms=(["defaultChoice"]="$4" ["dialogHeight"]=30 ["help-button"]="true"
    ["help-text"]="" ["help-title"]="Help: $2" ["helpURL"]=true)
  local urls=()
  local showConfFlag=$(echo 'false' && return 1)

  for dialogNum in $(eval echo "{1..${numDialogs}}"); do
    setDataForDialog ${dialogNum} "$ctryCode" "$3"
  done

  setRefLinks "${dialogOpts1["help-text"]}" urls

  mbSelVal="$3"
  dialogNum=4
  while true; do
    exec 3>&1
      showDialogForKBL ${dialogNum} "$1" "$2" "$3" "$ctryCode" 2>&1 1>&3
    exec 3>&-
    if [ ${dialogNum} -gt 3 ]; then
      IFS=',' read -a selData <<< "$inputRetVal"
      selData[1]="$mbSelVal"
    else
      IFS=',' read -a selData <<< "$mbSelVal"
    fi
    case ${selData[0]} in
      ${DIALOG_OK})
        case ${dialogNum} in
          1)
            ctryCode="${selData[1]}"
            showConfFlag=$(showConfDialog "$ctryCode")
            if ${showConfFlag}; then
              mbSelVal="${kblMap["$ctryCode"]}"
              mbSelVal="${mbSelVal:1}"
              selData[1]="$mbSelVal"
              dialogNum=4
            else
              dialogNum=2
            fi
          ;;
          2|3)
            if [ ${dialogNum} -gt 2 ]; then
              mbSelVal="${dialogOpts3[${selData[1]}]}"
            else
              mbSelVal="${dialogOpts2[${selData[1]}]}"
            fi
            dialogNum=4
          ;;
          4)
            mbSelVal="${selData[1]}"
            break
          ;;
        esac
      ;;
      ${DIALOG_CANCEL})
        if [ ${dialogNum} -gt 1 ]; then
          dialogNum=1
        else
          mbSelVal="$3"
          break
        fi
      ;;
      $DIALOG_EXTRA)
        dialogNum=3
      ;;
      $DIALOG_HELP)
        if [ "${selData[1]}" == "View-URL" ]; then
          elinks "${urls[0]}"
        fi
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                      showDialogForKBL
# DESCRIPTION: Show the linux dialog for the keyboard layout:
#                 #1)  Countries that have keyboard layouts related to them
#                 #2)  Keyboard layouts related to the country chosen
#                 #3)  Keyboard layouts NOT related to a particular country
#                 #4)  Confirmation of Keyboard Layout Selection
#  Required Params:
#      1)       dialogNum - the number for the dialog to display
#      2) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      3)     dialogTitle - String to be displayed at the top of the dialog box.
#      4)   defaultLayout - the default keyboard layout
#      5)        ctryCode - the default or selected country code
#---------------------------------------------------------------------------------------
showDialogForKBL() {
  local dialogNum=$1
  local ctryCode=$(trimString "$5")
  local dialogText=""
  local textArray=("keymaps - keyboard table descriptions for loadkeys and dumpkeys" " "
    "The Default Keyboard Layout is:  [$4]" "$DASH_LINE_WIDTH_80" " ")

  case ${dialogNum} in
    1)
      textArray+=("Select the country that is related to a keyboard layout:")
      dialogText=$(getTextForDialog "${textArray[@]}")
      showListOfChoicesDialogBox "$2" "$3" "$dialogText" KBL_ISO_CODES "radiolist" dialogOpts1
    ;;
    2)
      setOptionsForCountry "$5" "$4"
      dialogText=$(printf "Select the keyboard layout that is related to the country '%s':" "${ISO_CODE_NAMES["$ctryCode"]}")
      textArray+=("$dialogText")
      dialogText=$(getTextForDialog "${textArray[@]}")
      showListOfChoicesDialogBox "$2" "$3" "$dialogText" kblOptions "radiolist" dialogOpts2
    ;;
    3) ### Other dialog
      textArray+=("Select the keyboard layout:")
      dialogText=$(getTextForDialog "${textArray[@]}")
      showListOfChoicesDialogBox "$2" "$3" "$dialogText" kblOtherOpts "radiolist" dialogOpts3
    ;;
    4)
      dialogText=$(printf "Confirm to set the Keyboard Layout to:  [%s]" "$mbSelVal")
      showYesNoBox "$2" "$dialogText" "Confirmation of Keyboard Layout Selection" dialogOpts4
    ;;
  esac
}

#---------------------------------------------------------------------------------------
#      METHOD:                      setDataForDialog
# DESCRIPTION: Set the help and the menu options for the dialog to be displayed:
#                 #1)  Countries that have keyboard layouts related to them
#                 #2)  Keyboard layouts related to the country chosen
#                 #3)  Keyboard layouts NOT related to a particular country
#                 #4)  Confirmation of Keyboard Layout Selection
#  Required Params:
#      1)     dialogNum - the number for the dialog to display
#      2)   defCtryCode - the default country code based on the geographical location
#      3) defaultLayout - the default keyboard layout
#---------------------------------------------------------------------------------------
setDataForDialog() {
  local dialogNum=$1
  local helpText=$(getHelpTextForKeyboard "$3" ${dialogNum})

  dialogParms["help-text"]="$helpText"
  case ${dialogNum} in
    1)
      for opt in "${!dialogParms[@]}"; do
        dialogOpts1["$opt"]="${dialogParms[$opt]}"
      done
      for ctryCode in "${KBL_ISO_CODES[@]}"; do
        dialogOpts1["$ctryCode"]="${ISO_CODE_NAMES["$ctryCode"]}"
      done
      dialogOpts1["cancel-label"]="Exit"
      dialogOpts1["extra-label"]="Other"
    ;;
    2)
      dialogOpts2["help-text"]="$helpText"
      setOptionsForCountry "$2" "$3"
    ;;
    3) ### Other dialog
      setOptionsForOtherDlg
    ;;
    4)
      dialogOpts4=(["dialogHeight"]=20 ["help-button"]=true ["help-text"]="$helpText"
        ["help-title"]="${dialogParms["help-title"]}")
    ;;
  esac
}

#---------------------------------------------------------------------------------------
#      METHOD:                    setOptionsForCountry
# DESCRIPTION: Set the menu options for the dialog to choose the keyboard layout related
#              to the country.
#  Required Params:
#      1)      ctryCode - country code
#      2) defaultLayout - the default keyboard layout
#---------------------------------------------------------------------------------------
setOptionsForCountry() {
  local ctryCode=$(trimString "$1")
  local concatStr="${kblMap["$ctryCode"]}"
  local kblData=()
  local optNum=0
  local key=""
  local defChoice=""
  local helpText="${dialogOpts2["help-text"]}"

  concatStr="${concatStr:1}"

  dialogOpts2=()
  for opt in "${!dialogParms[@]}"; do
    dialogOpts2["$opt"]="${dialogParms[$opt]}"
  done
  dialogOpts2["help-text"]="$helpText"
  dialogOpts2["dialogHeight"]=20

  kblOptions=()
  readarray -t kblData <<< "${concatStr//;/$'\n'}"
  for kbl in "${kblData[@]}"; do
    optNum=$(expr ${optNum} + 1)
    key=$(printf "#%d" ${optNum})
    kblOptions+=("$key")
    dialogOpts2["$key"]="$kbl"
    if [ "$kbl" == "$2" ]; then
      defChoice="$key"
    fi
  done

  if [ ${#defChoice} -gt 0 ]; then
    dialogOpts2["defaultChoice"]="$defChoice"
  else
    unset dialogOpts2["defaultChoice"]
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                    setOptionsForOtherDlg
# DESCRIPTION: Set the menu options for the dialog to choose the keyboard layout that is
#              NOT related to any particular country or language.
#---------------------------------------------------------------------------------------
setOptionsForOtherDlg() {
  local optNum=0
  local key=""

  for opt in "${!dialogParms[@]}"; do
    dialogOpts3["$opt"]="${dialogParms[$opt]}"
  done
  unset dialogOpts3["defaultChoice"]

  for kbl in "${kblOther[@]}"; do
    optNum=$(expr ${optNum} + 1)
    key=$(printf "#%d" ${optNum})
    kblOtherOpts+=("$key")
    dialogOpts3["$key"]="$kbl"
  done
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                       showConfDialog
# DESCRIPTION: Check if the selected country code has only one choice for a
#              keyboard layout
#      RETURN: 0 - true, 1 - false
#  Required Params:
#      1) ctryCode - the selected country code
#---------------------------------------------------------------------------------------
function showConfDialog() {
  local kblData=()
  local concatStr="${kblMap["$1"]}"
  concatStr="${concatStr:1}"

  readarray -t kblData <<< "${concatStr//;/$'\n'}"

  if [ ${#kblData[@]} -lt 2 ]; then
    echo 'true' && return 0
  fi

  echo 'false' && return 1
}
