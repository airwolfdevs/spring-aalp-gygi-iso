#!/bin/bash
#set -e

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# functions/methods
#===============================================================================

#---------------------------------------------------------------------------------------
#      METHOD:                getPasswordsFromFormDialog
# DESCRIPTION: Uses a linux "dialog" form box of type "--passwordform" to get the passwords
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)     dialogTitle - String to be displayed at the top of the dialog box.
#      3)          urlRef - the URL that can be referenced for further documentation
#                           from the help dialog
#   Optional Param:
#      4)      dialogText - text to display in the text area of the dialog above the menu
#---------------------------------------------------------------------------------------
getPasswordsFromFormDialog() {
  local dialogBackTitle="$1"
  local dialogTitle="$2"
  local urlRef="$3"
  local helpText=$(getHelpTextForPswdKB "$urlRef")
  local -A dialogOpts=(["dialogHeight"]=20 ["insecure"]=true ["help-button"]=true
      ["help-text"]="$helpText" ["help-title"]="Help:  $dialogTitle" ["helpURL"]=true
      ["initValue"]=0 ["ok-label"]="Submit")
  local fields=()
  local dialogText=""

  if [ "$#" -gt 3 ]; then
    dialogText="$4"
  else
    dialogText=$(getDialogTextForPswdForm)
  fi

  while true; do
    exec 3>&1
      showPasswordFormDialog "$dialogBackTitle" "$dialogText" "$dialogTitle" dialogOpts
    exec 3>&-

    IFS=$'\n' read -d '' -a fields <<< "${formVals//$FORM_FLD_SEP/$'\n'}"

    case ${fields[0]} in
      $DIALOG_OK)
        pswdVals=("${fields[@]:1}")
        local validFormVals=$(validateFormFields)
        if ${validFormVals}; then
          break
        fi
      ;;
      $DIALOG_CANCEL)
        pswdVals=()
        break
      ;;
      $DIALOG_HELP)
        exec 3>&1
          showHelpDialog "$dialogBackTitle" dialogOpts
        exec 3>&-
        IFS=',' read -d '' -a fields <<< "$mbSelVal"
        local retVal=$(trimString "${fields[1]}")
        if [ "$retVal" == "View-URL" ]; then
          elinks "$urlRef"
        fi
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getDialogTextForPswdForm
# DESCRIPTION: Get the text to display in a linux "dialog" box of type "--passwordform"
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getDialogTextForPswdForm() {
  local textArray=(" ")
  local actionTxt=""
  if [ "$#" -gt 0 ]; then
    textArray+=(
      "The passwords were generated with the AUR package '$1'" " "
      "Select < Help > for the options that were passed to the generator!")
    actionTxt="Choose the password"
  else
    textArray+=(
      "The password must be between 8 - 20 characters in length!" " "
      "Select < Help > for password suggestions!")
    actionTxt="Enter a password"
  fi
  textArray+=(" "
    "$DASH_LINE_WIDTH_80" " "
    "$actionTxt to encrypt & open the LUKS encrypted container:"
  )
  local dialogText=$(getTextForDialog "${textArray[@]}")
  echo "$dialogText"
}

#---------------------------------------------------------------------------------------
#      METHOD:                  showPasswordFormDialog
# DESCRIPTION: Displays a linux "dialog" box of type "--passwordform"
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)      dialogText - Text to display in dialog before the menu options
#      3)     dialogTitle - String to be displayed at the top of the dialog box.
#      4)         cmdOpts - options for the linux "dialog" command
#---------------------------------------------------------------------------------------
showPasswordFormDialog() {
  local dialogBackTitle="$1"
  local dialogText="$2"
  local dialogTitle="$3"
  local -n cmdOpts=$4
  local ary=()
  local sep=" - "

  if [[ "$dialogTitle" =~ "Samba" ]]; then
    ary=("Create" "Samba User")
  else
    readarray -t ary <<< "${dialogTitle//$sep/$'\n'}"
  fi

  if [ ${#ary[@]} -gt 1 ]; then
    formDlgCmd=$(getCmdForDialogBoxType "mixedform" "$dialogBackTitle" "$dialogText" "$dialogTitle" cmdOpts)
    case "${ary[1]}" in
      "Create User")
        formDlgCmd+=' "       Username :" 1 15 "" 1 35 20 20 0'
        formDlgCmd+=' "       Password :" 2 15 "" 2 35 20 20 1'
        formDlgCmd+=' "Retype Password :" 3 15 "" 3 35 20 20 1 2> $instInput'
        cmdOpts["nocancel"]="true"
      ;;
      *)
        formDlgCmd+=' "     Group Name :" 1 15 "" 1 35 20 20 0'
        formDlgCmd+=' "       Username :" 2 15 "" 2 35 20 20 0'
        formDlgCmd+=' "       Password :" 3 15 "" 3 35 20 20 1'
        formDlgCmd+=' "Retype Password :" 4 15 "" 4 35 20 20 1 2> $instInput'
      ;;
    esac
  else
    formDlgCmd=$(getCmdForDialogBoxType "passwordform" "$dialogBackTitle" "$dialogText" "$dialogTitle" cmdOpts)
    formDlgCmd+=' "       Password :" 1 15 "" 1 35 20 20'
    formDlgCmd+=' "Retype Password :" 2 15 "" 2 35 20 20 2> $instInput'
  fi

  eval "$formDlgCmd"
  cmdRetVal=$?

  case $cmdRetVal in
    ${DIALOG_OK})
      formVals=$(cat "$instInput")
      formVals=$(echo "${DIALOG_OK}${FORM_FLD_SEP}$formVals")
    ;;
    ${DIALOG_CANCEL})
      formVals=$(echo "${DIALOG_CANCEL}${FORM_FLD_SEP}Cancel")
    ;;
    ${DIALOG_HELP})
      formVals=$(echo "${DIALOG_HELP}${FORM_FLD_SEP}Help")
    ;;
    ${DIALOG_EXTRA})
      formVals=$(echo "${DIALOG_EXTRA}${FORM_FLD_SEP}Extra")
    ;;
    ${DIALOG_ESC})
      formVals=$(echo "${DIALOG_CANCEL}${FORM_FLD_SEP}ESC")
    ;;
  esac
}

#---------------------------------------------------------------------------------------
#      METHOD:                dispInvPswdErrorMsg
# DESCRIPTION: Displays the appropriate error message when either:
#              A) the password entered is NOT between 8-20 characters
#              B) does NOT match the verified the value.
#  Required Params:
#      1) enteredPswd - the password that was entered the first time
#
#        NOTE:  The password dialog will have a maximum character limit of 20
#---------------------------------------------------------------------------------------
dispInvPswdErrorMsg() {
  local enteredPswd="$1"
  local errTxt=$(getTextForError)
  local errorMsg="The passwords that were entered DO NOT match!"
  if [ ${#enteredPswd} -lt 8 ]; then
    errorMsg="The password must be between 8 - 20 characters in length!"
  fi
  local msgTextArray=("$errTxt" " " "$errorMsg" "$DASH_LINE_WIDTH_80" " " "$DASH_LINE_WIDTH_80" " "
    "You must hit OK to continue & re-enter the password"
  )

  local msgText=$(getTextForDialog "${msgTextArray[@]}")

  $DIALOG --clear --no-collapse --title "Error:  Invalid Password" --msgbox "$msgText" 25 80 3>&1 1>&2 2>&3
}

#---------------------------------------------------------------------------------------
#      METHOD:                     getSelAutoGenPswd
# DESCRIPTION: Get the password for the LUKS encrypted container from one of the
#              automated password generation tools and set it in the global variable "mbSelVal"
#  Required Params:
#      1) dialogBackTitle - Specifies a backtitle string to be displayed on the
#                           backdrop, at the top of the screen.
#      2)          urlRef - the URL that can be referenced for further documentation
#                           from the help dialog
#      3)         aurPckg - name of the AUR password generator package
#---------------------------------------------------------------------------------------
getSelAutoGenPswd() {
  local dialogBackTitle="$1"
  local urlRef="$2"
  local aurPckg="$3"
  local autoGenPswds=()
  local dialogTitle="Automated Generated Passwords w/ '$aurPckg'"
  local dialogText=$(getDialogTextForPswdForm "$aurPckg")
  local helpText=$(getHelpTextForAutoPswd "$aurPckg" "$urlRef")
  local -A dialogParms=(["dialogHeight"]=25 ["help-button"]=true ["help-text"]="$helpText"
      ["help-title"]="Help:  $dialogTitle" ["helpURL"]=true)
  local fields=()

  case "$aurPckg" in
    "apg")
      cmdOutput=$(execAPG)
    ;;
    "pwgen")
      cmdOutput=$(execPWGEN)
    ;;
  esac
  IFS=$'\t' read -a autoGenPswds <<< "$cmdOutput"

  while true; do
    exec 3>&1
      showListOfChoicesDialogBox "$dialogBackTitle" "$dialogTitle" "$dialogText" autoGenPswds \
        "radiolist" dialogParms 2>&1 1>&3
    exec 3>&-
    IFS=',' read -a fields <<< "$mbSelVal"
    case ${fields[0]} in
      $DIALOG_OK)
        local aryIdx=$(expr ${fields[1]} - 1)
        mbSelVal=$(echo "${autoGenPswds[$aryIdx]}")
        break
      ;;
      $DIALOG_CANCEL)
        mbSelVal=""
        break
      ;;
      $DIALOG_HELP)
        if [ "${fields[1]}" == "View-URL" ]; then
          elinks "$urlRef"
        fi
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     validateFormFields
# DESCRIPTION: Validate that the values entered in the form are valid
#      RETURN: 0 - true, 1 -false
#---------------------------------------------------------------------------------------
function validateFormFields() {
  local groupName=""
  local pswd1=""
  local pswd2=""
  local userName=""
  local validFormVals=$(echo 'true' && return 0)

  if [ ${#pswdVals[@]} -gt 3 ]; then
    groupName="${pswdVals[0]}"
    validFormVals=$(isValidGroupName "$groupName")
    if ${validFormVals}; then
      userName="${pswdVals[1]}"
      validFormVals=$(isValidUserName "$userName" "${POST_ASSOC_ARRAY["UserName"]}")
      pswd1="${pswdVals[2]}"
      pswd2="${pswdVals[3]}"
    fi
  elif [ ${#pswdVals[@]} -gt 2 ]; then
    userName="${pswdVals[0]}"
    validFormVals=$(isValidUserName "$userName")
    pswd1="${pswdVals[1]}"
    pswd2="${pswdVals[2]}"
  else
    pswd1="${pswdVals[0]}"
    pswd2="${pswdVals[1]}"
  fi

  if ${validFormVals}; then
    validFormVals=$(isValidPswd "$pswd1" "$pswd2")
    if ! ${validFormVals}; then
      dispInvPswdErrorMsg "$pswd1"
    fi
  else
    if [ ${#userName} -gt 0 ]; then
      dispInvUserNameMsg "$userName"
    else
      dispInvGroupNameMsg "$groupName"
    fi
  fi

  echo ${validFormVals}
}

#---------------------------------------------------------------------------------------
#      METHOD:                     dispInvUserNameMsg
# DESCRIPTION: Displays the appropriate error message when either:
#              A) The username already exists
#              B) Fails the criteria specified in the function "isValidUserName"
#  Required Params:
#      1) userName - the name of the user to be created
#---------------------------------------------------------------------------------------
dispInvUserNameMsg() {
  local userName="$1"
  local outFile="/tmp/findUser.out"
  local dialogTitle="Error:  Invalid Username"
  local errorMsg="The username \"$userName\""
  local errTxt=$(getTextForError)

  if [ -f "$outFile" ]; then
    errorMsg+=" already exists!"
    rm -rf "$outFile"
  else
    local textArray=()
    errorMsg+=" does NOT meet the installer's criteria!"
    textArray=("$errorMsg" " ")
    errorMsg=$(getTextForUserName)
    textArray+=("$errorMsg")
    errorMsg=$(getTextForDialog "${textArray[@]}")
  fi

  local msgTextArray=( " " "$DASH_LINE_WIDTH_80" " "
  "$errorMsg" " " "$DASH_LINE_WIDTH_80" " "
  "You must hit OK to continue & re-enter all the values"
  )

  local msgText=$(getTextForDialog "${msgTextArray[@]}")

  $DIALOG --clear --no-collapse --title "$dialogTitle" --msgbox "$msgText" 25 80 3>&1 1>&2 2>&3
}

#---------------------------------------------------------------------------------------
#      METHOD:                     dispInvGroupNameMsg
# DESCRIPTION: Displays the appropriate error message when either:
#              A) The group name already exists
#              B) Fails the criteria specified in the function "isValidGroupName"
#  Required Params:
#      1) groupName - the name of the group to be created
#---------------------------------------------------------------------------------------
dispInvGroupNameMsg() {
  local groupName="$1"
  local dialogTitle="Error:  Invalid Group Name"
  local errorMsg="The group name \"$groupName\""
  local errTxt=$(getTextForError)

  if [ $(getent group $1) ]; then
    errorMsg+=" already exists!"
  else
    local textArray=()
    errorMsg+=" does NOT meet the installer's criteria!"
    textArray=("$errorMsg" " ")
    errorMsg=$(getTextForGroupName)
    textArray+=("$errorMsg")
    errorMsg=$(getTextForDialog "${textArray[@]}")
  fi

  local msgTextArray=( " " "$DASH_LINE_WIDTH_80" " "
  "$errorMsg" " " "$DASH_LINE_WIDTH_80" " "
  "You must hit OK to continue & re-enter all the values"
  )

  local msgText=$(getTextForDialog "${msgTextArray[@]}")

  $DIALOG --clear --no-collapse --title "Invalid Password" --msgbox "$msgText" 25 80 3>&1 1>&2 2>&3
}
