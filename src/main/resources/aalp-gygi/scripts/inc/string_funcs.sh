#!/bin/bash
#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# functions
#===============================================================================


#---------------------------------------------------------------------------------------
#    FUNCTION:                       printTable
# DESCRIPTION: Outputs delimited strings into a table structure
#      RETURN: concatenated string
#  Required Params:
#      1) delimiter which MUST BE a single character
#      2) data containing the header & detail rows
#---------------------------------------------------------------------------------------
function printTable() {
  local -r delimiter="${1}"
  local -r data="$(removeEmptyLines "${2}")"
  local tableRows=""
  local lines=()

  IFS=$'\n' read -d '' -r -a lines <<< "$data"

  local hdrRow="${lines[0]}"

  IFS=$"$delimiter" read -a fields <<< "${lines[0]}"
  local numberOfColumns=${#fields[@]}
  local lineDelimiter="$(printf '%s#+' "$(repeatString '#+' "${numberOfColumns}")")"
  tableRows=$(echo "${lineDelimiter}")
  tableRows+="\n"

  for row in "${lines[@]}"; do
    IFS=$"$delimiter" read -a fields <<< "${row}"
    for col in "${fields[@]}"; do
      tableRows+=$(printf "#| %s" "$col")
    done
    tableRows+="#|\n"
    tableRows+=$(echo "${lineDelimiter}")
    tableRows+="\n"
  done

  tableRows=$(echo -e "${tableRows}" | column -s '#' -t | awk '/^\s+\+/{gsub(" ", "-", $0)}1' | sed 's/^\-\-\-/   /' | sed -e 's/^[[:space:]]*//')
  echo -e "$tableRows"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    removeEmptyLines
# DESCRIPTION: Remove empty or blank lines from the content
#      RETURN: content minus the empty or blank lines
#  Required Params:
#      1) lines of strings
#---------------------------------------------------------------------------------------
function removeEmptyLines() {
  local -r content="${1}"

  echo -e "${content}" | sed '/^\s*$/d'
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                      repeatString
# DESCRIPTION: Output the string that is repeated by a certain amount
#      RETURN: concatenated repeated string
#  Required Params:
#      1) string of characters
#      2) integer value for the number of times to repeat
#---------------------------------------------------------------------------------------
function repeatString() {
  local -r string="${1}"
  local -r numberToRepeat="${2}"

  if [[ "${string}" != '' && "${numberToRepeat}" =~ ^[1-9][0-9]*$ ]]; then
    local -r result="$(printf "%${numberToRepeat}s")"
    echo -e "${result// /${string}}"
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                      isEmptyString
# DESCRIPTION: Check if the string is either empty or blank
#      RETURN: 0 - true, 1 - false
#  Required Params:
#      1) string of characters
#---------------------------------------------------------------------------------------
function isEmptyString() {
  local -r string="${1}"
  local str=$(trimString "${string}")

  if [ ${#str} -lt 1 ]; then
    echo 'true' && return 0
  fi

  echo 'false' && return 1
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                         trimString
# DESCRIPTION: Removes spaces at the beginning and end, and the
#              new line/carriage return.
#      RETURN: string without all the padded spaces
#  Required Params:
#      1) string of characters
#---------------------------------------------------------------------------------------
function trimString() {
  local str="$1"
  str=$(echo "$str" | tr '\n' ' ')
  str=$(echo "$str" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')

  echo "$str"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     trimPartitionTable
# DESCRIPTION: Removes spaces at the beginning and end, and the
#              new line/carriage return of each line/row of data in the partition
#              table.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function trimPartitionTable() {
  local dialogPartTable=$(printPartitionTable)
  local lines=()

  IFS=$'\n' read -d '' -r -a lines <<< "$dialogPartTable"
  for row in "${lines[@]}"; do
    row=$(trimString "$row")
    echo "$row"
  done
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     printPartitionTable
# DESCRIPTION: Outputs the lines/rows of data for the partition table into
#              a text based table structure
#              NOTE:  None of the fields/columns may contain a ';'.  Othewise,
#              the output will become fubared.
#      RETURN: concatenated string
#   Optional Param:
#      1) line/row of data representing a partition or logical volume delimited by '\t'
#      2) last line/row number to print
#---------------------------------------------------------------------------------------
function printPartitionTable() {
  local row=""
  local tableRows=()
  local hdrDtl=$(printf "%s;%s;%s;%s;%s;%s;%s" "Device" "Name" "Size" "GUID" "FS Type" "Mount Pt" "Flags")
  local customRow=""

  tableRows+=("$hdrDtl")

  if [ "$#" -gt 0 ]; then
    customRow="$1"
  fi

  setRowsForPartitionTable tableRows "$customRow"

  if [ "$#" -gt 1 ]; then
    local lastIdx=$(expr $2 + 1)
    tableRows=("${tableRows[@]:0:${lastIdx}}}")
  fi

  hdrDtl=$(printf "\n%s" "${tableRows[@]}")
  hdrDtl=${hdrDtl:1}
  local tbl=$(printTable ';' "$hdrDtl" | sed 's/^\-\-\-/   /')

  #if [ "$#" -gt 1 ]; then
    #tbl=$(printf "%s\n(More...)" "$tbl")
  #fi

  echo "$tbl"
}

setRowsForPartitionTable() {
  local -n rows=$1
  local customRow="$2"
  local row=""
  local partTblRows=()

  IFS=$'\n' read -d '' -a partTblRows < "$PARTITION_TABLE_FILE"
  if [ ${#customRow} -gt 0 ]; then
    addCustomRowToArray partTblRows "$customRow"
  fi

  for row in "${partTblRows[@]}"; do
    row=$(getTableRow "$row")
    rows+=("$row")
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                  addCustomRowToArray
# DESCRIPTION: Add a customized row to the array of partition table rows
#---------------------------------------------------------------------------------------
addCustomRowToArray() {
  local -n tblRows=$1
  local customRow="$2"
  local rows=()
  local cols=()
  local len=${#tblRows[@]}
  local rowNum=0

  IFS=$'\t' read -a cols <<< "$customRow"
  if [ "${cols[3]}" == "lvm" ]; then
    rowNum=$(findRowNumForLVM tblRows)
    row="${tblRows[$rowNum]}"
    IFS=$'\t' read -a cols <<< "$row"
    cols[0]="${LVM_DEVICE_PREFIX}${varMap[VG-NAME]}"
    row=$(printf "\t%s" "${cols[@]}")
    row=${row:1}
    rows=("${tblRows[@]:0:${rowNum}}")
    rows+=("$row")
    rows+=("$customRow")
    rowNum=$(expr $rowNum + 1)
    if [ ${rowNum} -lt ${len} ]; then
      rows+=("${tblRows[@]:${rowNum}}")
    fi
    tblRows=("${rows[@]:0}")
  else
    tblRows+=("$customRow")
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                         getTableRow
# DESCRIPTION: Get the details of row in the partition table
#      RETURN: concatenated string delimited by ';'
#  Required Params:
#      1) row - row from the partition table
#---------------------------------------------------------------------------------------
function getTableRow() {
  local row="$1"
  local cols=()
  local colNum=0
  local partNum=""
  local partName=""
  local partSize=""
  local guid=""
  local fileSystem=""
  local dirName=""
  local flagsElem=""
  local reqFmt="%3s%s"

  if [ "$DIALOG" == "yad" ]; then
    reqFmt="%25s%s"
  fi

  IFS=$'\t' read -a cols <<< "$row"
  colNum=1
  for col in "${cols[@]}"; do
    case "$colNum" in
      1)
        partNum=$(echo "$col")
        if [ "$DIALOG" != "yad" ] && [ "${#partNum}" -gt 10 ]; then
          partNum=$(echo "$partNum" | cut -c1-13)
        fi
      ;;
      2)
        partName=$(echo "$col")
        if [ "$DIALOG" != "yad" ] &&[ "${#partName}" -gt 10 ]; then
          partName=$(echo "$partName" | cut -c1-10)
        fi
      ;;
      3)
        partSize=$(echo "$col")
      ;;
      4)
        guid=$(echo "$col")
      ;;
      5)
        if [ "$col" == "*" ]; then
          fileSystem=$(printf "$reqFmt" " " "$col")
        else
          fileSystem=$(echo "$col")
        fi
      ;;
      6)
        dirName=$(echo "$col")
      ;;
      7)
        flagsElem=$(echo "$col")
      ;;
    esac
    colNum=$(expr $colNum + 1)
  done

  row=$(printf ";%s" "$partNum" "$partName" "$partSize" "$guid" "$fileSystem" "$dirName" "$flagsElem")
  row=${row:1}
  echo "$row"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                      splitStr
# DESCRIPTION: Split a string with idiomatic expressions
#      RETURN: array of strings in the global "splitStrArray" variable
#  Required Params:
#      1) string to be split on
#      2) multi-character delimiter
#---------------------------------------------------------------------------------------
splitStr() {
  local str="$1"
  local delimiter="$2"
  local concatStr=$str$delimiter
  splitStrArray=();
  while [[ $concatStr ]]; do
    splitStrArray+=( "${concatStr%%"$delimiter"*}" );
    concatStr=${concatStr#*"$delimiter"};
  done;
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                getPartitionTableText
# DESCRIPTION: Displays the partitions and logical volumes to be created for the
#              "$BLOCK_DEVICE"
#      RETURN: concatenated string
#   Optional Param:
#      1) line/row of data representing a partition or logical volume delimited by '\t'
#      2) last line/row number to print
#---------------------------------------------------------------------------------------
function getPartitionTableText() {
  local dialogPartTable=""
  local pttDesc="${PART_TBL_TYPES[${varMap[PART-TBL-TYPE]}]}"

  if [ "$#" -gt 1 ]; then
    dialogPartTable=$(getPartitionTableForDialog "$BLOCK_DEVICE" "$BLOCK_DEVICE_SIZE" "$pttDesc" "$1" $2)
  elif [ "$#" -gt 0 ]; then
    dialogPartTable=$(getPartitionTableForDialog "$BLOCK_DEVICE" "$BLOCK_DEVICE_SIZE" "$pttDesc" "$1")
  else
    dialogPartTable=$(getPartitionTableForDialog "$BLOCK_DEVICE" "$BLOCK_DEVICE_SIZE" "$pttDesc")
  fi

  echo "$dialogPartTable"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:              getPartitionTableForDialog
# DESCRIPTION: Displays the partitions to be created for the device
#      RETURN: concatenated string
#  Required Params:
#      1)  blockDevice - string value of the device (i.e. "/dev/sda")
#      2) blockDevSize - size in human-readable format of "$blockDevice"
#      3)      pttDesc - value from global associative array variable "$PART_TBL_TYPES"
#   Optional Param:
#      4) line/row of data representing a partition or logical volume delimited by '\t'
#      5) last line/row number to print
#---------------------------------------------------------------------------------------
function getPartitionTableForDialog() {
  local blockDevice="$1"
  local blockDevSize="$2"
  local pttDesc="$3"
  local hdr=$(printf "Partition Table:%5s\\\"%s\\\"%17sDisk:%5s\\\"%s\\\"" " " \
    "${PART_TBL_TYPES[${varMap[PART-TBL-TYPE]}]}" " " " " "$BLOCK_DEVICE")
  local textArray=("$hdr")
  local tbl=""

  if [ ${varMap["VG-NAME"]+_} ]; then
    if [[ -z "${PHYSICAL_VOLUME}" ]]; then
      setPhysicalVolume
    fi

    lvm=$(printf "Physical Volume:%5s\"%s\"%26sVolume Group:%5s\"%s\"" " " "$PHYSICAL_VOLUME" \
        " " " " "${varMap[VG-NAME]}")
    textArray+=("$lvm")
  fi

  if [ "$#" -gt 4 ]; then
    tbl=$(printPartitionTable "$4" $5)
  elif [ "$#" -gt 3 ]; then
    tbl=$(printPartitionTable "$4")
  else
    tbl=$(printPartitionTable)
  fi

  textArray+=("$tbl")
  local dialogPartTable=$(getTextForDialog "${textArray[@]}")

  echo "$dialogPartTable"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                      isValidPswd
# DESCRIPTION: Validates that the password entered:
#              A) is between 8-20 characters
#              B) matches the verified the value.
#      RETURN: 0 - true, 1 - false
#  Required Params:
#      1)  enteredPswd - the password that was entered the first time
#      2) verifiedPswd - the password that was entered the second time
#
#        NOTE:  The password dialog will have a maximum character limit of 20
#---------------------------------------------------------------------------------------
function isValidPswd() {
  local enteredPswd="$1"
  local verifiedPswd="$2"

  if [ ${#enteredPswd} -gt 7 ] && [ "$enteredPswd" == "$verifiedPswd" ] ; then
    echo 'true' && return 0
  fi

  echo 'false' && return 1
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                       isValidUserName
# DESCRIPTION: Validates that the username entered:
#              A) Does not already exist.
#              B) Is less than 33 characters
#              C) The first character is either a lowercase letter or an underscore
#              D) The next 31 characters can be either:
#                 1) letters, numbers, underscores, and/or hyphens
#                                      OR
#                 2) 30 characters of 1) above plus a '$' at the end
#              E) If the username contains the '$' character, then it can ONLY be
#                 the last character
#      RETURN: 0 - true, 1 - false
#  Required Params:
#      1) userName - the name of the user to be created
#   Optional Param:
#      2) samba username
#---------------------------------------------------------------------------------------
function isValidUserName() {
  local userName="$1"
  local sambaUserName=""
  local pattern="^[a-z_]([a-z0-9_-]{0,31}|[a-z0-9_-]{0,30}\\$)$"
  local validationFlag=$(echo 'false' && return 1)

  if [ "$#" -gt 1 ]; then
    sambaUserName="$2"
  fi

  if [[ "$userName" =~ ${pattern} ]]; then
    if [ "$userName" == "$sambaUserName" ]; then
      validationFlag=$(echo 'true' && return 0)
    else
      local outFile="/tmp/findUser.out"
      local cmd="id $userName"
      changeRoot "$cmd" > "$outFile" 2>&1
      local cmdOut=$(cat "$outFile")

      if [[ "$cmdOut" =~ "no such user" ]]; then
        validationFlag=$(echo 'true' && return 0)
        rm -rf "$outFile"
      fi
    fi
  fi


  echo ${validationFlag}
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getDeviceStatsTable
# DESCRIPTION: Displays in tabular format the size of the device, total used,
#              and total remaining
#      RETURN: concatenated string
#  Required Params:
#      1)   lastDevice - the last logical volume or partition (i.e. /home)
#      2) hrDeviceSize - size of the device in human-readable format
#---------------------------------------------------------------------------------------
function getDeviceStatsTable() {
  local lastDevice="$1"
  local hrDeviceSize="$2"
  local cols=()
  local statsTable=""
  local pttDesc="${PART_TBL_TYPES[${varMap[PART-TBL-TYPE]}]}"
  local hdr=$(printf "Partition Table:%2s'%s'%10sDisk:%2s'%s'" " " "$pttDesc" " " " " "$BLOCK_DEVICE")
  local textArray=("$hdr")

  if [ ${varMap["VG-NAME"]+_} ]; then
    if [[ -z "${PHYSICAL_VOLUME}" ]]; then
      setPhysicalVolume
    fi
    lvm=$(printf "Physical Volume:%2s'%s'%19sVolume Group:%2s'%s'" " " "$PHYSICAL_VOLUME" \
        " " " " "${varMap[VG-NAME]}")
    textArray+=("$lvm")
  fi

  local partTableText=$(getTextForDialog "${textArray[@]}")
  local sizeInBytes=$(humanReadableToBytes "$hrDeviceSize")
  local hrsRemaining=$(echo "$hrDeviceSize")

  if [ ${#lastDevice} -gt 0 ]; then
    IFS=$'\t' read -a cols <<< "$lastDevice"
    hrsRemaining=$(echo "${cols[2]}")
  fi

  local totRem=$(humanReadableToBytes "$hrsRemaining")
  local totUsed=$(expr $sizeInBytes - $totRem)
  local hrsUsed=$(bytesToHumanReadable "$totUsed")

  hdr=$(printf "%s;%s;%s" "Size" "Used" "Remaining")
  lvm=$(printf "%s;%s;%s" "$hrDeviceSize" "$hrsUsed" "$hrsRemaining")
  local tableRows=( "$hdr" "$lvm" )
  statsTable=$(printf "\n%s" "${tableRows[@]}")
  statsTable=${statsTable:1}
  statsTable=$(printTable ';' "$statsTable" | sed 's/^\-\-\-/   /')

  textArray+=("$statsTable")
  statsTable=$(getTextForDialog "${textArray[@]}")

  echo "$statsTable"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getPartitionTableHdrDtls
# DESCRIPTION: Get the header and details of the partition table
#              NOTE:  None of the fields/columns may contain a ';'.  Othewise,
#                     the output will become fubared.
#      RETURN: concatenated string
#   Optional Param:
#      1) line/row of data representing a partition or logical volume delimited by '\t'
#      2) last line/row number to print
#---------------------------------------------------------------------------------------
function getPartitionTableHdrDtls() {
  local partTblRows=()
  local row=""
  local tableRows=()
  local hdrDtl=""

  IFS=$'\n' read -d '' -a partTblRows < "$PARTITION_TABLE_FILE"
  if [ "$#" -gt 0 ]; then
    addCustomRowToArray partTblRows "$1"
  fi

  if [ "$#" -gt 1 ]; then
    partTblRows=("${partTblRows[@]:0:$2}")
  fi

  for row in "${partTblRows[@]}"; do
    hdrDtl=$(getTableRow "$row")
    tableRows+=("$hdrDtl")
  done

  hdrDtl=$(printf "\n%s" "${tableRows[@]}")
  hdrDtl=${hdrDtl:1}

  echo "$hdrDtl"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getPartitionLayoutForYAD
# DESCRIPTION: Get the header of the partition table (i.e. type, device name, etc.) as
#              the rows of data for the logical volumes and/or partitions
#      RETURN: concatenated string separated by '\n'
#---------------------------------------------------------------------------------------
function getPartitionLayoutForYAD() {
  local hdr=$(printf "%3sPartition Table:%5s\"%s\"%37sDisk:%5s\"%s\"" " " \
    " " "${PART_TBL_TYPES[${varMap[PART-TBL-TYPE]}]}" " " " " "$BLOCK_DEVICE")
  local lvm=""
  local rows=()
  local customRow=""

  local textArray=("$hdr")

  if [ ${varMap["VG-NAME"]+_} ]; then
    setPhysicalVolume
    lvm=$(printf "Physical Volume:%5s\"%s\"%54sVolume Group:%5s\"%s\"" " " "$PHYSICAL_VOLUME" \
        " " " " "${varMap[VG-NAME]}")
    textArray+=("$lvm")
  fi

  if [ "$#" -gt 0 ]; then
    customRow="$1"
  fi

  setRowsForPartitionTable textArray "$customRow"

  local partitionLayout=$(getTextForDialog "${textArray[@]}")
  echo "$partitionLayout"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     getHeaderForTable
# DESCRIPTION: Get the header from the current partition layout/scheme.
#      RETURN: concatenated string
#  Required Params:
#      1) partitionLayout - string containing header, logical volumes and partitions
#---------------------------------------------------------------------------------------
function getHeaderForTable() {
  local partitionLayout="$1"
  local partTblRows=()

  IFS=$'\n' read -d '' -a partTblRows <<< "$partitionLayout"
  local hdr=$(printf "%s" "${partTblRows[0]}")
  if [[ "$partitionLayout" =~ "Volume" ]]; then
    hdr+=$(printf "\n%s" "${partTblRows[1]}")
  fi

  echo "$hdr"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  escapeSpecialCharacters
# DESCRIPTION: Escape the necessary the characters so that the string will display
#              correctly in the yad "dialog"
#      RETURN: concatenated string
#  Required Params:
#      1) dialogStr - string to be encoded
#---------------------------------------------------------------------------------------
function escapeSpecialCharacters() {
  local dialogStr="$1"
  local encodedStr=''
  local lastChar=$(expr ${#dialogStr} - 1)
  local char=''

  if [ ${lastChar} -gt -1 ]; then
    for pos in $(eval echo "{0..${lastChar}}"); do
      char=${dialogStr:${pos}:1}
      if [ ${ESCAPE_CHARS["$char"]+_} ]; then
        encodedStr+="${ESCAPE_CHARS[$char]}"
      else
        encodedStr+="$char"
      fi
    done
  fi

  echo "$encodedStr"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getStatsForLVM
# DESCRIPTION: Get the LVM statistics in tabular format
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getStatsForLVM() {
  local partTblRows=()
  local pttDesc="${PART_TBL_TYPES[${varMap[PART-TBL-TYPE]}]}"
  local hdr=$(printf "Partition Table:%2s'%s'%10sDisk:%2s'%s'" " " "$pttDesc" " " " " "$BLOCK_DEVICE")
  local lvm=$(printf "Physical Volume:%2s'%s'%19sVolume Group:%2s'%s'" " " "$PHYSICAL_VOLUME" " " " " "${varMap[VG-NAME]}")
  local cols=()
  local tableRows=()
  local row=$(printf "%s;%s;%s;%s" "Device" "Size" "GUID" "Name")
  local len=${#rows[@]}
  IFS=$'\n' read -d '' -a partTblRows < "$PARTITION_TABLE_FILE"

  tableRows+=("$row")
  for row in "${partTblRows[@]}"; do
    IFS=$'\t' read -a cols <<< "$row"
    case "${cols[3]}" in
      "8E00")
        row=$(printf "%s;%s;%s;%s" "${cols[0]}" "${cols[2]}" "${cols[3]}" "LVM physical volume")
        tableRows+=("$row")
        aryIdx=$(expr $aryIdx + 1)
      ;;
      "crypt")
        row=$(printf "%s;%s;%s;%s" "${cols[0]}" " " "${cols[3]}" "LUKS encrypted container")
        tableRows+=("$row")
        aryIdx=$(expr $aryIdx + 1)
      ;;
      "lvm")
        break
      ;;
    esac
  done

  local tbl=$(printf "\n%s" "${tableRows[@]}")
  tbl=${tbl:1}
  tbl=$(printTable ';' "$tbl" | sed 's/^\-\-\-/   /')

  if [ ${#tableRows[@]} -gt 2 ]; then
    tableRows=(" " "1   Partition/LVM physical volume" "1   LUKS encrypted container")
  else
    tableRows=(" " "1   Partition/LVM physical volume")
  fi

  row=$(printf "%d   Logical Volumes" $(cat "$PARTITION_TABLE_FILE" | grep "lvm-" | wc -l))
  tableRows+=( "$row" )
  row=$(printf "\n%s" "${tableRows[@]}")
  row=${row:1}

  local textArray=( "$hdr" "$lvm" "$tbl" "$row")
  local dialogText=$(getTextForDialog "${textArray[@]}")

  echo "$dialogText"
}

#---------------------------------------------------------------------------------------
#      METHOD:                 setDataForPartitionScheme
# DESCRIPTION: Sets the following key/value pairs to the associative array:
#         A) "partitionLayout" - string containing header, logical volumes and partitions
#                                        OR
#         B) "dialogPartTable" - string containing the rows of data for the
#                                partition table in a textual table format
#         C)  "partTableStats" - summary table of the progress in the partitioning process
#---------------------------------------------------------------------------------------
setDataForPartitionScheme() {
  local -n dataMethodParams=$1
  local lastDevice=""

  if [ ${dataMethodParams["lastDevice"]+_} ]; then
    lastDevice="${dataMethodParams[lastDevice]}"
    if [ "$DIALOG" == "yad" ]; then
      dataMethodParams["partitionLayout"]=$(getPartitionLayoutForYAD "$lastDevice")
    else
      dataMethodParams["dialogPartTable"]=$(getPartitionTableText "$lastDevice")
      dataMethodParams["partTableStats"]=$(getDiskStatsTable "$lastDevice" "${dataMethodParams[statsBlockSize]}" \
        "${dataMethodParams[deviceType]}")
    fi
  else
    if [ "$DIALOG" == "yad" ]; then
      dataMethodParams["partitionLayout"]=$(getPartitionLayoutForYAD)
    else
      dataMethodParams["dialogPartTable"]=$(getPartitionTableText)
      dataMethodParams["partTableStats"]=$(getDiskStatsTable "$lastDevice" "${dataMethodParams[statsBlockSize]}" \
        "${dataMethodParams[deviceType]}")
    fi
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getDiskStatsTable
# DESCRIPTION: Get the string containing the tabular format of the summary table that
#              will show the stats of the progress in the partitioning process.
#      RETURN: concatenated string
#  Required Params:
#      1)     lastDevice - the last logical volume or partition (i.e. /home) to be created
#      2) statsBlockSize - either BLOCK_DEVICE_SIZE or PHYSICAL_VOL_SIZE
#      2)     deviceType - Logical Volume or Partition
#---------------------------------------------------------------------------------------
function getDiskStatsTable() {
  local lastDevice="$1"
  local statsBlockSize="$2"
  local deviceType="$3"
  local statsTable=""

  if [ ${#lastDevice} -lt 1 ] && [ "$deviceType" == "Partition" ]; then
    local hrSize=$(getRemainingHRFSize)
    local cols=("/dev/sda" "dummy" "$hrSize" "8300" " " " " " ")
    lastDevice=$(printf "\t%s" "${cols[@]}")
    lastDevice=${lastDevice:1}
  fi

  if [ "$deviceType" == "Partition" ]; then
    statsTable=$(getDeviceStatsTable "$lastDevice" "$statsBlockSize")
  else
    statsTable=$(getStatsForLVM)
  fi

  echo "$statsTable"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getRemainingHRFSize
# DESCRIPTION: Get the size remaining for the device.
#      RETURN: size in human-readable format
#---------------------------------------------------------------------------------------
function getRemainingHRFSize() {
  local fields=()
  local lines=()
  local hrToBytes=0
  local totUsed=0
  local devSize=$(humanReadableToBytes "$BLOCK_DEVICE_SIZE")

  IFS=$'\n' read -d '' -r -a lines < "$PARTITION_TABLE_FILE"
  for line in "${lines[@]}"; do
    IFS=$'\t' read -a fields <<< "$line"
    hrToBytes=$(humanReadableToBytes "${fields[2]}")
    totUsed=$(expr $totUsed + $hrToBytes)
  done
  local totRemainingHR="0 B"

  if [ ${devSize} -gt ${totUsed} ]; then
    local totRemaining=$(expr $devSize - $totUsed)
    totRemainingHR=$(bytesToHumanReadable "$totRemaining")
  fi

  echo "$totRemainingHR"
}


#---------------------------------------------------------------------------------------
#    FUNCTION:                 getDeviceNameForListDialog
# DESCRIPTION: Get name of a logical volume or partition to be displayed in a linux or
#              yad dialog of type radio list
#      RETURN: formatted name of device
#  Required Params:
#      1) colVals - array of values from a row in the partition table
#---------------------------------------------------------------------------------------
function getDeviceNameForListDialog() {
  local -n colVals=$1
  local deviceName=""

  if [ "$DIALOG" == "yad" ]; then
    if [[ "${colVals[0]}" =~ "/dev/sda" ]]; then
      colVals[1]=$(printf "'%s':  '%s'" "${colVals[0]}" "${colVals[1]}")
    fi
    colVals=("${colVals[1]}" "${colVals[2]}" "${colVals[3]}" "${colVals[-1]}")
    deviceName=$(printf "\t%s" "${colVals[@]}")
    deviceName=${deviceName:1}
  else
    if [[ "${colVals[0]}" =~ "/dev/sda" ]]; then
      deviceName=$(printf "'%s'--'%s': %s" "${colVals[0]}" "${colVals[1]}" "${colVals[2]}")
    else
      deviceName=$(printf "'%s': %s" "${colVals[1]}" "${colVals[2]}")
    fi
  fi

  echo "$deviceName"
}


function getLogo() {
  local textArray=(
"                                               *                           "
"                                              (//                          "
"                                             /////                         "
"                                            ///////                        "
"                                           (////////                       "
"                                          (//////////                      "
"                                          ////////////                     "
"                                        (/##///////////                    "
"                                       ///(/((((((//////                   "
"                                      (((((/((((((///////%                 "
"                                     ((((((((((((((/((////%                "
"                                    (((((((((((((((/////(//(               "
"                                   ((((((((##########(#(#(((/              "
"                                 %(((#########%&&%####(###((((             "
"                                #%%%####%##%%&@@@@&########((              "
"                               %%%%%%%%#%#%%%     &&&%########(##          "
"                              %%%%%%%#%%%%#%        &%##########(#         "
"                             %%%%%%%%%%%%%#          %##########((#        "
"                           &%%%%%%%%%%%%%%           @##############       "
"                          %%%%%%%%%%%%%%%%            %##########(###      "
"                         %%%%%%%%%%%%%&&&&            &&&&&%###########    "
"                        %%%%%%%%&&&&&&&&@              &&&&&&&%%&%######   "
"                       %%%%%&&&&&&%                          %@&&%##&&%##  "
"                      %&%&&&&&                                    &&&&%%%% "
"                    %&%%&&                                            &&&%%"
"$DIALOG_BORDER_MAX")
  local dialogText=$(printf "\n%s" "${textArray[@]}")
  dialogText="${dialogText:1}"

  printf "%s" "$dialogText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                      isValidGroupName
# DESCRIPTION: Validates that the username entered:
#              A) Does not already exist.
#              B) Is less than 17 characters
#              C) The first character is either a lowercase letter or an underscore
#              D) The next 15 characters can be either letters, numbers,
#                 underscores, and/or hyphens
#      RETURN: 0 - true, 1 - false
#  Required Params:
#      1) userName - the name of the user to be created
#---------------------------------------------------------------------------------------
function isValidGroupName() {
  local groupName="$1"
  local pattern="^[a-z_]([a-z0-9_-]{0,15})$"
  local validationFlag=$(echo 'false' && return 1)

  if [[ "$groupName" =~ ${pattern} ]]; then
    if [ ! $(getent group $1) ]; then
      validationFlag=$(echo 'true' && return 0)
    fi
  fi

  echo ${validationFlag}
}
