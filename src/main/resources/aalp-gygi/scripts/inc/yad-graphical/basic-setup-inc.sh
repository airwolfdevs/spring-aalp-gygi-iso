#!/bin/bash

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# globals
#===============================================================================
source "$GRAPHICAL_PATH/yad-nb-inc.sh"

#===============================================================================
# functions/methods
#===============================================================================

#---------------------------------------------------------------------------------------
#      METHOD:                    selectUnoffUserRepos
# DESCRIPTION: Show a yad "notebook" dialog to select the unofficial signed & unsigned
#              user repositories.  The selections will be set within the global
#              associative array variable "TAB_SEL_VALS".  The keys will be:
#                      - "tab#01" for the selected signed repository names
#                      - "tab#02" for the selected unsigned repository names
#  Required Params:
#      1) dialogTitle - the title to be displayed at the top of the window
#      2)    helpText - Text to display on the help/more dialog
#---------------------------------------------------------------------------------------
selectUnoffUserRepos() {
  local nbKeyPlug=$(getSixDigitRandomNum)
  local notebookCmds=()
  local -A helpOpts=(["buttons-layout"]="center" ["window-icon"]="gtk-help.png" ["title"]="Help:  $1"
     ["center"]=true ["height"]=400 ["width"]=650 ["wrap"]=true ["fontname"]="Sans 12")

  setCmdToShowNotebookForAUR "$1" ${nbKeyPlug}

  local nbCommand=$(printf " %s" "${notebookCmds[@]}")
  nbCommand=${nbCommand:1}

  while true; do
    clickedVals=$(showNotebookDialog "$nbCommand" " ")
    IFS=$',' read -d '' -a btnVals <<< "$clickedVals"
    case ${btnVals[0]} in
      ${DIALOG_OK})
        saveSubmitValsFromTabs 2
        break
      ;;
      ${DIALOG_HELP})
        showTextInfoDialog "$2" helpOpts
      ;;
      *)
        TAB_SEL_VALS=()
        break
      ;;
    esac
  done

}

#---------------------------------------------------------------------------------------
#      METHOD:                 setCmdToShowNotebookForAUR
# DESCRIPTION: Set the command to display a yad "notebook" dialog with the following
#              tabs:
#              #1)   "Signed" - list of repositories that have signed keys
#              #2) "Unsigned" - list of repositories that will require a SigLevel prop.
#  Required Params:
#      1) dialogTitle - the title to be displayed at the top of the window
#      2)   nbKeyPlug - integer value for the notebook key & plug of its tabs
#---------------------------------------------------------------------------------------
setCmdToShowNotebookForAUR() {
  local nbKeyPlug=$2
  local numTabs=2
  local nbCmd=$(echo "$DIALOG --notebook --key=${nbKeyPlug}")
  local tabCmd=""
  local tabName=""
  local -A yadTabOpts=(["grid-lines"]="both" ["no-selection"]=true ["notebook"]=true )
  local nbButtons=("Ok" "Skip" "Help")
  local -A nbOpts=(["height"]=570 ["width"]=610 ["center"]=true ["buttons-layout"]="center"
                   ["image"]="${IMG_ROOT}/aur-repos.png" ["image-on-top"]=true ["listType"]="checklist"
                   ["title"]="$1" ["window-icon"]="app-icon.png")
  local listColHdrs=(Checkbox Repository Description)
  local tabTextFmt="Select from the list of \"%s\" Unofficial User Repos."
  local -A nbTabArray=(["listType"]="checklist" ["tabListColHdrs"]="listColHdrs")

  for tabNum in $(eval echo "{1..${numTabs}}"); do
    nbTabArray["tabNum"]=${tabNum}

    case ${tabNum} in
      1)
        tabName="Signed"
        nbTabArray["defaultChoice"]="archlinuxcn"
        nbTabArray["menuOptRef"]="SIGNED_MENU_OPTS"
        nbTabArray["optDescsRef"]="SIGNED_REPO_DESCS"
      ;;
      2)
        tabName="Unsigned"
        nbTabArray["defaultChoice"]="archlinuxfr"
        nbTabArray["menuOptRef"]="UNSIGNED_MENU_OPTS"
        nbTabArray["optDescsRef"]="UNSIGNED_REPO_DESCS"
      ;;
    esac
    nbTabArray["tabName"]="$tabName"
    nbTabArray["tabText"]=$(printf "$tabTextFmt" "$tabName")

    tabCmd=$(getTabCmdForList "nbTabArray")
    tabCmd=$(printf "$tabCmd" ${nbKeyPlug})

    notebookCmds+=("$tabCmd")
    nbCmd+=$(printf " --tab=\"%s\"" "$tabName")
  done

  nbCmd+=$(getOptionsForNotebook nbOpts)
  nbCmd+=$(getButtonsForDialog nbButtons)
  notebookCmds+=("$nbCmd")
}

#---------------------------------------------------------------------------------------
#      METHOD:                     selectOptionForSUDO
# DESCRIPTION: Select the option to use when issuing the "sudo" command using a yad
#              "radiolist" dialog.  Set the value within the global variable "mbSelVal".
#  Required Params:
#      1) paramAssocArray - the name of the associative array with the required
#                           key/value pairs:
#      "dialogTitle"   - String to be displayed at the top of the dialog window.
#      "dialogText"    - Text to display inside the dialog before the list of menu options
#      "dialogTextHdr" - String to be displayed at the top of the text area.
#      "helpText"      - Text to display on the help/more dialog
#      "menuOptsRef"   - name of the array containing the menu options
#      "optDescsRef"   - name of the associative array containing the description
#                        of the menu options
#---------------------------------------------------------------------------------------
selectOptionForSUDO() {
  local -n paramAssocArray="$1"
  local buttons=("Ok" "Help")
  local cols=("Radio Btn" "Password Option" "Description")
  local title=$(printf "%32s%s" " " "${paramAssocArray["dialogTextHdr"]}")
  local textArray=("$title" " " " " " " " " "${paramAssocArray["dialogText"]}")
  local dialogText=$(printf "\n%s" "${textArray[@]}")

  paramAssocArray["helpHeight"]=400
  paramAssocArray["helpWidth"]=650
  selectOptionFromRadioList "$1" "buttons" "cols" "$dialogText"
}

#---------------------------------------------------------------------------------------
#      METHOD:                       selectAURHelper
# DESCRIPTION: Select the name of the package to install for the AUR Helper using a yad
#              "radiolist" dialog.  Set the value within the global variable "mbSelVal".
#  Required Params:
#      1) paramAssocArray - the name of the associative array with the required
#                           key/value pairs:
#      "dialogTitle"   - String to be displayed at the top of the dialog window.
#      "helpText"      - Text to display on the help/more dialog
#      "menuOptsRef"   - name of the array containing the menu options
#      "optDescsRef"   - name of the associative array containing the description
#                        of the menu options
#---------------------------------------------------------------------------------------
selectAURHelper() {
  local -n paramAssocArray="$1"
  local -A notebookCmds=()
  local -A helpOpts=(["buttons-layout"]="center" ["window-icon"]="gtk-help.png" ["title"]="Help:  $1"
     ["center"]=true ["height"]=400 ["width"]=610 ["wrap"]=true ["fontname"]="Sans 12")
  local helpText="${paramAssocArray["helpText"]}"

  setCmdsForAURHelperNB "$1"

  local nbKey="${CAT_MENU_OPTS[0]}"
  local totNumTabs=${#CAT_MENU_OPTS[@]}
  setDataFilesForTabs ${totNumTabs} "$nbKey"

  local tabCols=()
  local sep="|"
  while true; do
    local nbCommand="${notebookCmds["$nbKey"]}"
    clickedVals=$(showNotebookDialog "$nbCommand" " ")
    IFS=$',' read -d '' -a btnVals <<< "$clickedVals"
    case ${btnVals[0]} in
      ${DIALOG_OK})
        saveSubmitValsFromTabs ${totNumTabs}
        readarray -t tabCols <<< "${TAB_SEL_VALS["tab#01"]//$sep/$'\n'}"
        if [ "${tabCols[1]}" == "$nbKey" ]; then
          break
        else
          nbKey="${tabCols[1]}"
          setDataFilesForTabs ${totNumTabs} "$nbKey"
        fi
      ;;
      ${DIALOG_HELP})
        showTextInfoDialog "$helpText" helpOpts
      ;;
      *)
        TAB_SEL_VALS=()
        break
      ;;
    esac
  done

  cleanDataFilesForTabs "CAT_MENU_OPTS"
}

#---------------------------------------------------------------------------------------
#      METHOD:                    setCmdsForAURHelperNB
# DESCRIPTION: Set the commands to display a yad "notebook" dialog with the following
#              tabs:
#       Notebook A:
#              Tab #1)   "Categories" - default radio button selected is "Command Line"
#                  #2) "Command Line" - pacman wrappers
#       Notebook B:
#              Tab #1) "Categories" - default radio button selected is "Graphical"
#                  #2)  "Graphical" - graphical AUR helpers
#  Required Params:
#      1) nbAssocArray - the name of the associative array with the required
#                           key/value pairs:
#      "windowTitle"      - String to be displayed at the top of the dialog window.
#      "CAT_MENU_OPTS[@]" - the name of array containing menu options to display
#---------------------------------------------------------------------------------------
setCmdsForAURHelperNB() {
  local -n nbAssocArray="$1"
  local totNumTabs=${#CAT_MENU_OPTS[@]}
  local cols=()
  local -A assocArray=()

  assocArray["nbKeyPlug"]=$(getSixDigitRandomNum)
  assocArray["numTabs"]=2
  assocArray["windowTitle"]="${nbAssocArray["windowTitle"]}"
  assocArray["nbImage"]="$YAD_GRAPHICAL_PATH/images/post-inst/aur-helper.png"

  if [ ${nbAssocArray["height"]+_} ]; then
    assocArray["height"]=${nbAssocArray["height"]}
  fi

  for cat in "${CAT_MENU_OPTS[@]}"; do
    local tabName=""
    local defChoice=""
    local tabText=""
    case "$cat" in
      "${CAT_MENU_OPTS[0]}")
        tabName="Command Line"
        defChoice="Trizen"
        tabText="Select the pacman wrapper to run from the command line:"
      ;;
      *)
        tabName="Graphical"
        defChoice="Octopi"
        tabText="Select the graphical AUR helper:"
      ;;
    esac

    for tabNum in $(eval echo "{1..${totNumTabs}}"); do
      local tabNameKey=$(printf "tabName%02d" ${tabNum})
      local defChoiceKey=$(printf "defChoiceTab%02d" ${tabNum})
      local menuOptRefKey=$(printf "menuOptsTab%02d" ${tabNum})
      local optDescsRefKey=$(printf "optDescsTab%02d" ${tabNum})
      local tabTextKey=$(printf "textTab%02d" ${tabNum})
      local listTypeKey=$(printf "listType%02d" ${tabNum})
      local colsTabKey=$(printf "colsTab%02d" ${tabNum})
      local concatStr=""

      case ${tabNum} in
        1)
          assocArray["$tabNameKey"]="Categories"
          assocArray["$defChoiceKey"]="$cat"
          assocArray["$menuOptRefKey"]="CAT_MENU_OPTS"
          assocArray["$optDescsRefKey"]="DESC_ASSOC_ARRAY"
          assocArray["$tabTextKey"]="Choose type of AUR Helper:"
          assocArray["$listTypeKey"]="radiolist"
          cols=("Radio Btn" "Category" "Name")
          concatStr=$(printf "$FORM_FLD_SEP%s" "${cols[@]}")
          concatStr=${concatStr:${#FORM_FLD_SEP}}
        ;;
        2)
          assocArray["$tabNameKey"]="$tabName"
          assocArray["$defChoiceKey"]="$defChoice"
          assocArray["$menuOptRefKey"]="${nbAssocArray["$cat"]}"
          assocArray["$optDescsRefKey"]="AUR_PCKG_DESCS"
          assocArray["$tabTextKey"]="$tabText"
          assocArray["$listTypeKey"]="radiolist"
          cols=("Radio Btn" "AUR Helper" "Description")
          concatStr=$(printf "$FORM_FLD_SEP%s" "${cols[@]}")
          concatStr=${concatStr:${#FORM_FLD_SEP}}
        ;;
      esac

      assocArray["$colsTabKey"]="$concatStr"
    done

    notebookCmds["$cat"]=$(getCmdToShowPostInstNB "assocArray")
    for tabNum in $(eval echo "{1..${totNumTabs}}"); do
      local srcFile=$(printf "${NB_TAB_DATA_FNS["tabData"]}" "$tabNum")
      local destFile=$(printf "/tmp/%s-%s" "$cat" "${srcFile:5}")
      mv "$srcFile" "$destFile"
    done
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                     selectDefTextEditor
# DESCRIPTION: Select the option using a yad "radiolist" dialog for the default text
#              editor.  Set the value within the global variable "mbSelVal".
#  Required Params:
#      1) paramAssocArray - the name of the associative array with the required
#                           key/value pairs:
#      "dialogTitle"   - String to be displayed at the top of the dialog window.
#      "dialogText"    - Text to display inside the dialog before the list of menu options
#      "dialogTextHdr" - String to be displayed at the top of the text area.
#      "helpText"      - Text to display on the help/more dialog
#      "menuOptsRef"   - name of the array containing the menu options
#      "optDescsRef"   - name of the associative array containing the description
#                        of the menu options
#---------------------------------------------------------------------------------------
selectDefTextEditor() {
  local -n paramAssocArray="$1"
  local buttons=("Ok" "Skip" "Help")
  local cols=("Radio Btn" "Text Editor" "Description")
  local title=$(printf "%16s%s" " " "${paramAssocArray["dialogTextHdr"]}")
  local textArray=("$title" " " " " " " " " "${paramAssocArray["dialogText"]}")
  local dialogText=$(printf "\n%s" "${textArray[@]}")

  paramAssocArray["height"]=${YAD_MAX_HEIGHT}
  paramAssocArray["width"]=650
  paramAssocArray["helpHeight"]=400
  paramAssocArray["helpWidth"]=650
  paramAssocArray["image"]="$YAD_GRAPHICAL_PATH/images/post-inst/def-txt-editor.png"
  paramAssocArray["wrap-cols"]=3
  paramAssocArray["wrap-width"]=400

  selectOptionFromRadioList "$1" "buttons" "cols" "$dialogText"
}

#---------------------------------------------------------------------------------------
#      METHOD:                     selectDefaultShell
# DESCRIPTION: Select the name of the shell to install using a yad "radiolist" dialog.
#              Set the value within the global array variable "TAB_SEL_VALS".
#  Required Params:
#      1) paramAssocArray - the name of the associative array with the required
#                           key/value pairs:
#      "dialogTitle"   - String to be displayed at the top of the dialog window.
#      "helpText"      - Text to display on the help/more dialog
#      "menuOptsRef"   - name of the array containing the menu options
#      "optDescsRef"   - name of the associative array containing the description
#                        of the menu options
#---------------------------------------------------------------------------------------
selectDefaultShell() {
  local -n paramAssocArray="$1"
  local -A notebookCmds=()
  local -A helpOpts=(["buttons-layout"]="center" ["window-icon"]="gtk-help.png" ["title"]="Help:  ${paramAssocArray["dialogTitle"]}"
     ["center"]=true ["height"]=400 ["width"]=650 ["wrap"]=true ["fontname"]="Sans 12")

  setCmdsForDefShellNB "$1"

  local nbKey="${SHELL_TYPES[0]}"
  local totNumTabs=${#SHELL_TYPES[@]}
  setDataFilesForTabs ${totNumTabs} "$nbKey"

  local tabCols=()
  local sep="|"
  while true; do
    local nbCommand="${notebookCmds["$nbKey"]}"
    clickedVals=$(showNotebookDialog "$nbCommand" " ")
    IFS=$',' read -d '' -a btnVals <<< "$clickedVals"
    case ${btnVals[0]} in
      ${DIALOG_OK})
        saveSubmitValsFromTabs ${totNumTabs}
        readarray -t tabCols <<< "${TAB_SEL_VALS["tab#01"]//$sep/$'\n'}"
        if [ "${tabCols[1]}" == "$nbKey" ]; then
          break
        else
          nbKey="${tabCols[1]}"
          setDataFilesForTabs ${totNumTabs} "$nbKey"
        fi
      ;;
      ${DIALOG_HELP})
        showTextInfoDialog "${paramAssocArray["helpText"]}" helpOpts
      ;;
      *)
        TAB_SEL_VALS=()
        break
      ;;
    esac
  done

  cleanDataFilesForTabs "SHELL_TYPES"
}

#---------------------------------------------------------------------------------------
#      METHOD:                    setCmdsForDefShellNB
# DESCRIPTION: Set the commands to display a yad "notebook" dialog with the following
#              tabs:
#       Notebook A:
#              Tab #1)  "Shell Types" - default radio button selected is
#                                       "POSIX compliant shells"
#                  #2) "POSIX compliant" - POSIX compliant shells
#       Notebook B:
#              Tab #1)  "Shell Types" - default radio button selected is
#                                       "Alternative shells"
#                  #2)  "Alternative" - Alternative shells
#  Required Params:
#      1) nbAssocArray - the name of the associative array with the required
#                        key/value pairs:
#      "windowTitle"    - String to be displayed at the top of the dialog window.
#      "SHELL_TYPES[@]" - the name of array containing menu options to display
#---------------------------------------------------------------------------------------
setCmdsForDefShellNB() {
  local -n nbAssocArray="$1"
  local totNumTabs=${#SHELL_TYPES[@]}
  local cols=()
  local -A assocArray=()

  assocArray["nbKeyPlug"]=$(getSixDigitRandomNum)
  assocArray["numTabs"]=2
  assocArray["windowTitle"]="${nbAssocArray["windowTitle"]}"
  assocArray["nbImage"]="$YAD_GRAPHICAL_PATH/images/post-inst/set-default-shell.png"

  for cat in "${SHELL_TYPES[@]}"; do
    local tabName=""
    local defChoice=""
    local tabText=""

    case "$cat" in
      "${SHELL_TYPES[0]}")
        tabName="POSIX compliant"
        defChoice="Bash"
        tabText="Select the POSIX compliant shell:"
      ;;
      *)
        tabName="Alternative"
        defChoice="PowerShell"
        tabText="Select the Alternative shell:"
      ;;
    esac

    for tabNum in $(eval echo "{1..${totNumTabs}}"); do
      local tabNameKey=$(printf "tabName%02d" ${tabNum})
      local defChoiceKey=$(printf "defChoiceTab%02d" ${tabNum})
      local menuOptRefKey=$(printf "menuOptsTab%02d" ${tabNum})
      local optDescsRefKey=$(printf "optDescsTab%02d" ${tabNum})
      local tabTextKey=$(printf "textTab%02d" ${tabNum})
      local listTypeKey=$(printf "listType%02d" ${tabNum})
      local colsTabKey=$(printf "colsTab%02d" ${tabNum})
      local concatStr=""

      case ${tabNum} in
        1)
          assocArray["$tabNameKey"]="Shell Types"
          assocArray["$defChoiceKey"]="$cat"
          assocArray["$menuOptRefKey"]="SHELL_TYPES"
          assocArray["$optDescsRefKey"]="DESC_ASSOC_ARRAY"
          assocArray["$tabTextKey"]="Choose the type of Command-line shell to install:"
          assocArray["$listTypeKey"]="radiolist"
          cols=("Radio Btn" "Shell Type" "Name")
          concatStr=$(printf "$FORM_FLD_SEP%s" "${cols[@]}")
          concatStr=${concatStr:${#FORM_FLD_SEP}}
        ;;
        2)
          assocArray["$tabNameKey"]="$tabName"
          assocArray["$defChoiceKey"]="$defChoice"
          assocArray["$menuOptRefKey"]="${nbAssocArray["$cat"]}"
          assocArray["$optDescsRefKey"]="AUR_PCKG_DESCS"
          assocArray["$tabTextKey"]="$tabText"
          assocArray["$listTypeKey"]="radiolist"
          cols=("Radio Btn" "Shell" "Description")
          concatStr=$(printf "$FORM_FLD_SEP%s" "${cols[@]}")
          concatStr=${concatStr:${#FORM_FLD_SEP}}
        ;;
      esac

      assocArray["$colsTabKey"]="$concatStr"
    done

    notebookCmds["$cat"]=$(getCmdToShowPostInstNB "assocArray")
    for tabNum in $(eval echo "{1..${totNumTabs}}"); do
      local srcFile=$(printf "${NB_TAB_DATA_FNS["tabData"]}" "$tabNum")
      local destFile=$(printf "/tmp/%s-%s" "$cat" "${srcFile:5}")
      mv "$srcFile" "$destFile"
    done
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                   showConfirmationDialog
# DESCRIPTION: Show a yad "list" dialog to get confirmation to install the AUR packages
#              that are listed.
#  Required Params:
#      1) paramAssocArray - the name of the associative array with the required
#                           key/value pairs:
#      "dialogTitle"   - String to be displayed at the top of the dialog window.
#      "dialogText"    - Text to display inside the dialog before the list of menu options
#      "dialogTextHdr" - String to be displayed at the top of the text area.
#      "helpText"      - Text to display on the help/more dialog
#      "menuOptsRef"   - name of the array containing the menu options
#      "optDescsRef"   - name of the associative array containing the description
#                        of the menu options
#---------------------------------------------------------------------------------------
showConfirmationDialog() {
  local -n paramAssocArray="$1"
  local buttons=("Ok" "Skip" "Help")
  local cols=("AUR Package" "Description")

  paramAssocArray["wrap-cols"]=2
  paramAssocArray["wrap-width"]=400

  showMenuForConfirmation "$1" "buttons" "cols"
}

#---------------------------------------------------------------------------------------
#      METHOD:                     selectPackageForSSH
# DESCRIPTION: Select the option using a yad "radiolist" dialog for the secure shell.
#              Set the value within the global variable "mbSelVal".
#  Required Params:
#      1) paramAssocArray - the name of the associative array with the required
#                           key/value pairs:
#      "dialogTitle"   - String to be displayed at the top of the dialog window.
#      "dialogText"    - Text to display inside the dialog before the list of menu options
#      "dialogTextHdr" - String to be displayed at the top of the text area.
#      "helpText"      - Text to display on the help/more dialog
#      "menuOptsRef"   - name of the array containing the menu options
#      "optDescsRef"   - name of the associative array containing the description
#                        of the menu options
#---------------------------------------------------------------------------------------
selectPackageForSSH() {
  local -n paramAssocArray="$1"
  local buttons=("Ok" "Skip" "Help")
  local cols=("Radio Btn" "Secure Shell" "Description")

  paramAssocArray["height"]=510
  paramAssocArray["width"]=650
  paramAssocArray["helpHeight"]=400
  paramAssocArray["helpWidth"]=650
  paramAssocArray["image"]="$YAD_GRAPHICAL_PATH/images/post-inst/ssh.png"

  selectOptionFromRadioList "$1" "buttons" "cols" ""
}

#---------------------------------------------------------------------------------------
#      METHOD:                  showInstallFontConfigStep
# DESCRIPTION: Add a tab to the current yad "notebook" dialog to display the packages
#              that will be installed for the font configuration.
#  Required Params:
#    1)  optSel - the value of the option that was selected for this task
#    2) optDesc - the description/name of this task
#---------------------------------------------------------------------------------------
showInstallFontConfigStep() {
  local -A localAssocArray=()

  yesNoFlag=$(echo 'false' && return 1)

  piAssocArray["contFlag"]="true"
  saveCurrentDataSet

  if [ "$DIALOG" == "yad" ]; then
    piAssocArray["tabName"]="$2"
    piAssocArray["tabNum"]=3
    piAssocArray["tabColumns"]="AUR Package${FORM_FLD_SEP}Description"
    piAssocArray["nbButtons"]="Ok${FORM_FLD_SEP}Skip${FORM_FLD_SEP}Help"
    piAssocArray["tabText"]="Click the \"Ok\" button to install library!"
  fi

  unset piAssocArray["listType"]
  piAssocArray["optsArray"]="aurPckgs"
  piAssocArray["descAssocArray"]="aurPckgDescs"
  piAssocArray["helpText"]=$(getTextForFontConfig)

  setNextTaskForStep
  resetToPrevDataSet

  if [ ${TAB_ASSOC_ARRAY["$2"]+_} ]; then
    yesNoFlag=$(echo 'true' && return 0)
  fi
}