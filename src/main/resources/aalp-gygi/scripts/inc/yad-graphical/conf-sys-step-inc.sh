#!/bin/bash
#set -e

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# globals
#===============================================================================

#===============================================================================
# functions/methods
#===============================================================================

#---------------------------------------------------------------------------------------
#      METHOD:                       selectGenFstab
# DESCRIPTION: Show a yad "radiolist" dialog to select the type of identifier to use
#              in the fstab file.  Set the value within the global variable "$mbSelVal"
#  Required Params:
#      1)   dialogTitle - String to be displayed at the top of the dialog window.
#      2) dialogTextHdr - String to be displayed at the top of the text area.
#      3)    dialogText - Text to display inside the dialog before the
#                           list of menu options
#      4)      helpText - Text to display on the help/more dialog
#---------------------------------------------------------------------------------------
selectGenFstab() {
  local dialogTitle="$1"
  local dlgButtons=("Ok" "Cancel" "Help")
  local dlgColumns=("Radio Btn" "Option" "Description")
  local title=$(printf "%32s%s" " " "$2")
  local textArray=("$title" " " " " " " " " "$3")
  local dialogText=$(printf "\n%s" "${textArray[@]}")
  dialogText=$(escapeSpecialCharacters "${dialogText:1}")
  local -A dlgOpts=(["height"]=500 ["width"]=655 ["image"]="$YAD_GRAPHICAL_PATH/images/config-system.png"
    ["image-on-top"]=true ["window-icon"]="config-system.png")
  local -A helpDlgOpts=(["width"]=610 ["title"]="Help:  $dialogTitle")
  local dlgData=()

  appendOptionsForRadiolist "$dialogText" "$dialogTitle" dlgOpts
  setDataForMultiColList fstabChoices fstabDescs dlgData
  local retVal=$(getValueFromRadiolist "$4" dlgOpts dlgButtons dlgColumns dlgData helpDlgOpts)

  if [ ${#retVal} -gt 0 ]; then
    mbSelVal="${fstabDescs["$retVal"]}"
  else
    mbSelVal=""
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                          getLocale
# DESCRIPTION: Get the locale.  Set the select keyboard layout in the global
#              variable "mbSelVal".
#  Required Params:
#      1)   dialogTitle - String to be displayed at the top of the dialog window.
#      2) dialogTextHdr - String to be displayed at the top of the text area.
#      3) defaultLocale - the default locale
#      4)   defCtryCode - the default country code based on the geographical location
#---------------------------------------------------------------------------------------
getLocale() {
  local defCtryCode="$4"
  local helpText=""
  local helpArray=()
  local lclDlgData=(" " " " " ")
  local concatStr=""
  local numDialogs=3
  local dialogNum=0
  local idx=1
  local yadCmd=""
  local yadCmdDlg2=""
  local yadCmds=(" " " " " ")
  local selData=()
  local -A helpOpts=(["buttons-layout"]="center" ["window-icon"]="gtk-help.png" ["title"]="$2"
  ["center"]=true ["height"]=400 ["width"]=610 ["wrap"]=true ["fontname"]="Sans 12")

  for dialogNum in $(eval echo "{1..${numDialogs}}"); do
    setCmdForDialogs ${dialogNum} "$1" "$2" "$3"
    setDataForDialog ${dialogNum} "$defCtryCode" "$3"
  done

  yadCmdDlg2="${yadCmds[$idx]}"
  dialogNum=3
  setDataForConfDialog ${dialogNum} " " "$3"
  while true; do
    idx=$(expr ${dialogNum} - 1)
    concatStr="${lclDlgData[$idx]}"
    echo "$concatStr" > "$DATA_FILE_NAME"
    helpText="${helpArray[$idx]}"
    yadCmd="${yadCmds[$idx]}"
    yadChoices=$(eval "$yadCmd")
    btnClick=$?

    case ${btnClick} in
      ${DIALOG_OK})
        case ${dialogNum} in
          1)
            readarray -t selData <<< "${yadChoices//|/$'\n'}"
            dialogNum=3
            setDataForConfDialog ${dialogNum} "${selData[1]}"
            concatStr=$(trimString "${lclDlgData[3]}")
            if [ ${#concatStr} -gt 0 ]; then
              selData=(TRUE "#1" "$mbSelVal")
            else
              dialogNum=2
              setOptionsForSelCode ${dialogNum} "${selData[1]}" "$3"
              setImageAndText ${dialogNum} "${selData[1]}" "$yadCmdDlg2"
            fi
          ;;
          2)
            readarray -t selData <<< "${yadChoices//|/$'\n'}"
            dialogNum=3
            setDataForConfDialog ${dialogNum} " " "${selData[1]}"
          ;;
          3)
            if [ ${#selData[@]} -gt 0 ]; then
              mbSelVal=$(trimString "${selData[1]}")
            else
              mbSelVal="$3"
            fi
            break
          ;;
        esac
      ;;
      ${DIALOG_CANCEL})
        dialogNum=1
        selData=()
      ;;
      ${DIALOG_EXIT})
        mbSelVal=""
        break
      ;;
      ${DIALOG_HELP})
        showTextInfoDialog "$helpText" helpOpts
      ;;
      ${DIALOG_ESC})
        if [ ${dialogNum} -gt 1 ]; then
          dialogNum=1
          selData=()
        else
          mbSelVal="$3"
          break
        fi
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                      setCmdForDialogs
# DESCRIPTION: Set the YAD commands for the dialog to be displayed:
#                 #1)  Languages and/or Countries that have locales associated to them
#                 #2)  Locales associated with the language and/or country chosen
#                 #3)  Confirmation of Locale Selection
#  Required Params:
#      1)     dialogNum - the number for the dialog to display
#      2) dialogTextHdr - String to be displayed at the top of the text area.
#      3)   dialogTitle - String to be displayed at the top of the dialog window.
#      4) defaultLocale - the default locale
#      5)   defCtryCode - the default country code based on the geographical location
#---------------------------------------------------------------------------------------
setCmdForDialogs() {
  local dialogNum=$1
  local dialogText=$(getDialogTextForKeyboardLayout ${dialogNum} "$3" "$4")
  local idx=$(expr ${dialogNum} - 1)
  local yadCmd=""

  case ${dialogNum} in
    1|2)
      yadCmd=$(getCmdKeyboardLayoutDlg ${dialogNum} "$2" "$dialogText")
    ;;
    3)
      yadCmd=$(getCmdKeyboardLayoutDlg ${dialogNum} "$2" "Confirmation of Locale Selection")
    ;;
  esac

  yadCmds[$idx]="$yadCmd"
}


#---------------------------------------------------------------------------------------
#    FUNCTION:                  getDialogTextForKeyboard
# DESCRIPTION: Get the text to display within a yad "dialog" for choosing a keyboard layout
#      RETURN: Concatenated string
#  Required Params:
#      1)     dialogNum - the number for the dialog to display
#      2) dialogTextHdr - String to be displayed at the top of the text area.
#      3) defaultLocale - the default locale
#---------------------------------------------------------------------------------------
function getDialogTextForKeyboardLayout() {
  local dialogNum=$1
  local title=$(printf "%12s%s" " " "$2")
  local textArray=("$title" " " " "
    "The Default Locale is:  ["$3"]"
    " " " ")
  case ${dialogNum} in
    1)
      textArray+=("Select the language or country code that is associated with a locale:")
    ;;
    2)
      textArray+=("Select the locale that is associated with the %s \"%s\":")
    ;;
  esac

  local dialogText=$(printf "\n%s" "${textArray[@]}")
  dialogText=$(escapeSpecialCharacters "${dialogText:1}")

  echo "$dialogText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getCmdKeyboardLayoutDlg
# DESCRIPTION: Get the command to show a yad "dialog" box of either type "radiolist"
#              or "text-info".
#      RETURN: concatenated string
#  Required Params:
#      1)   dialogNum - the number for the dialog to display
#      2) dialogTitle - String to be displayed at the top of the dialog window.
#      3)  dialogText - Text to be displayed in the text area of the dialog
#---------------------------------------------------------------------------------------
function getCmdKeyboardLayoutDlg() {
  local dialogNum=$1
  local nameOfImage=""
  local imagePath=""
  local dataFileName=$(printf " < \"%s\" 2> /dev/null" "$DATA_FILE_NAME")
  local dlgButtons=("Ok" "Exit" "Help")
  local dlgColumns=("Radio Btn" "Country or Language Code" "Name of Language" "Name of Country")
  local -A dlgOpts=(["buttons-layout"]="center" ["grid-lines"]="both" ["image-on-top"]=true
    ["title"]="$2" ["window-icon"]="config-system.png" ["list-type"]="radiolist"
    ["text"]="<span font_weight='bold' font_size='medium'>$3</span>" ["text-align"]="left"
    ["height"]=550 ["width"]=675 ["fontname"]="Sans 12" ["center"]=true ["wrap"]=true ["size"]="fit")
  local yadCmd=$(echo "$DIALOG ")

  case ${dialogNum} in
    1)
      imagePath=$(printf "%s/images/%s.png" "$YAD_GRAPHICAL_PATH" "world-globe")
      dlgOpts["image"]="$imagePath"
    ;;
    2)
      imagePath=$(printf "%s/images" "$YAD_GRAPHICAL_PATH")
      imagePath+="/%s.png"
      dlgOpts["image"]="$imagePath"
      dlgButtons[1]="Cancel"
      dlgColumns=("Radio Btn" "Locale" "%s")
    ;;
    3)
      imagePath=$(printf "%s/images/%s.png" "$YAD_GRAPHICAL_PATH" "question")
      dlgButtons=("Yes" "No" "Help")
      unset dlgOpts["list-type"]
      unset dlgOpts["grid-lines"]
      dlgOpts["height"]=350
      dlgOpts["image"]="$imagePath"
      dlgOpts["width"]=475
      dlgOpts["window-icon"]="question-icon.png"
      local textInfoOpts=$(getOptionsForTextInfo dlgOpts " ")
      textInfoOpts+=$(getButtonsForDialog dlgButtons)
      yadCmd=$(echo "$DIALOG $textInfoOpts --text-info < $DATA_FILE_NAME 2> /dev/null")
    ;;
  esac

  if [ ${dlgOpts["list-type"]+_} ]; then
    yadCmd+=$(getOptionsForListDialog dlgOpts)
    yadCmd+=$(getButtonsForDialog dlgButtons)
    yadCmd+=$(getColumnsForListDialog dlgColumns)
    yadCmd+="$dataFileName"
  fi

  echo "$yadCmd"
}

#---------------------------------------------------------------------------------------
#      METHOD:                      setDataForDialog
# DESCRIPTION: Set the help and the menu options for the dialog to be displayed:
#                 #1)  Languages and/or Countries that have locales associated to them
#                 #2)  Locales associated with the language and/or country chosen
#                 #3)  Confirmation of Locale Selection
#  Required Params:
#      1)     dialogNum - the number for the dialog to display
#      2)   defCtryCode - the default country code based on the geographical location
#      3) defaultLayout - the default keyboard layout
#---------------------------------------------------------------------------------------
setDataForDialog() {
  local dialogNum=$1
  local idx=$(expr ${dialogNum} - 1)
  local concatStr=""
  local dlgData=()
  local ctryLang=()
  local langNames=()
  local defFlag=$(echo 'false' && return 1)

  case ${dialogNum} in
    1)
      for key in "${KAAGI_LOCALE_KEYS[@]}"; do
        readarray -t ctryLang <<< "${KAAGI_DIALOG_DESCS["$key"]//|/$'\n'}"
        if [ "${ctryLang[1]}" == "none" ]; then
          ctryLang[1]=" "
        else
          if [ "${ctryLang[0]}" == "none" ]; then
            ctryLang[0]=" "
          fi
          readarray -t langNames <<< "${ctryLang[1]//;/$'\n'}"
          if [ ${#langNames[@]} -gt 2 ]; then
            ctryLang[1]=$(printf "; %s" "${langNames[@]:0:2}")
            ctryLang[1]="${ctryLang[1]:2}"
          fi
        fi

        if ! ${defFlag}; then
          defFlag=$(isDefaultChoice "$key" "$3")
          if ${defFlag}; then
            dlgData+=(TRUE)
          else
            dlgData+=(FALSE)
          fi
        else
          dlgData+=(FALSE)
        fi

        dlgData+=("$key")
        dlgData+=("${ctryLang[1]}")
        dlgData+=("${ctryLang[0]}")
      done

      if ! ${defFlag}; then
        dlgData[0]=TRUE
      fi
      concatStr=$(printf "\n%s" "${dlgData[@]}")
      lclDlgData[${idx}]="${concatStr:1}"
    ;;
    3)
      #setDataForConfDialog ${dialogNum} " " "$3"
    ;;
  esac

  local helpText=$(getHelpTextForLocalization "$3" ${dialogNum})
  helpArray+=("$helpText")
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                       isDefaultChoice
# DESCRIPTION: Check if the language or country code contains the default locale
#      RETURN: 0 - true, 1 - false
#  Required Params:
#      1)     localeKey - the language or country code
#      2) defaultLocale - the default locale
#---------------------------------------------------------------------------------------
function isDefaultChoice() {
  local localeData=()
  local concatStr="${KAAGI_LOCALES["$1"]}"
  concatStr="${concatStr:1}"

  readarray -t localeData <<< "${concatStr//;/$'\n'}"

  local pos=$(findPositionForString localeData "$2")
  if [ ${pos} -gt -1 ]; then
    echo 'true' && return 0
  fi

  echo 'false' && return 1
}

#---------------------------------------------------------------------------------------
#      METHOD:                    setOptionsForSelCode
# DESCRIPTION: Set the menu options for the dialog to choose the locale associated with
#              either a language or country code that was selected.
#  Required Params:
#      1)     dialogNum - the number for the dialog to display
#      2)       selCode - the selected language or country code
#      3) defaultLocale - the default locale
#---------------------------------------------------------------------------------------
setOptionsForSelCode() {
  local dialogNum=$1
  local selCode=$(trimString "$2")
  local concatStr="${KAAGI_LOCALES["$selCode"]}"
  local lclData=()
  local localeDlgData=()
  local localeOptions=()
  local -A localeData=()
  local defChoice=""
  local idx=0

  if [ ${ISO_LANG_NAMES["$selCode"]+_} ]; then
    idx=1
  fi

  readarray -t localeOptions <<< "${concatStr//;/$'\n'}"
  for lcl in "${localeOptions[@]}"; do
    readarray -t lclData <<< "${lcl//./$'\n'}"
    if [ ${#lclData[@]} -lt 2 ]; then
      readarray -t lclData <<< "${lcl//@/$'\n'}"
    fi
    concatStr="${lclData[0]}"

    readarray -t lclData <<< "${concatStr//_/$'\n'}"
    concatStr="${lclData[$idx]}"
    if [ ${idx} -gt 0 ]; then
      concatStr="${ISO_CODE_NAMES["$concatStr"]}"
    else
      concatStr="${ISO_LANG_NAMES["$concatStr"]}"
    fi
    localeData["$lcl"]="$concatStr"
    if [ "$lcl" == "$3" ]; then
      defChoice="$lcl"
    fi
  done

  if [ ${#defChoice} -gt 0 ]; then
    setDataForMultiColList localeOptions localeData localeDlgData "$defChoice"
  else
    setDataForMultiColList localeOptions localeData localeDlgData
  fi

  idx=$(expr ${dialogNum} - 1)
  concatStr=$(printf "\n%s" "${localeDlgData[@]}")
  lclDlgData[${idx}]="${concatStr:1}"
}

#---------------------------------------------------------------------------------------
#      METHOD:                       setImageAndText
# DESCRIPTION: Set the prefix of the image to be displayed and the name of the country
#              in the text area of the dialog.
#  Required Params:
#      1) dialogNum - the number for the dialog to display
#      2)   selCode - the selected language or country code
#      3)    yadCmd - command to display the dialog
#---------------------------------------------------------------------------------------
setImageAndText() {
  local dialogNum=$1
  local selCode=$(trimString "$2")
  local yadCmd="$3"
  local idx=$(expr ${dialogNum} - 1)
  local imagePath=""
  local colName=""
  local codeType=""
  local name=""

  if [ ${ISO_CODE_NAMES["$selCode"]+_} ]; then
    imagePath=$(printf "flags/%s" "$selCode")
    codeType="country"
    name="${ISO_CODE_NAMES["$selCode"]}"
    colName="Name of Language"
  else
    imagePath="world-globe"
    codeType="language"
    name="${ISO_LANG_NAMES["$selCode"]}"
    colName="Name of Country"
  fi

  yadCmds[${idx}]=$(printf "$yadCmd" "$imagePath" "$codeType" "$name" "$colName")
}

#---------------------------------------------------------------------------------------
#      METHOD:                    setDataForConfDialog
# DESCRIPTION: Set the text to be displayed in the confirmation dialog.
#  Required Params:
#      1) dialogNum - the number for the dialog to display
#      2)  langCtry - language or country code
#  Optional Params:
#      3) defLocale - default locale
#---------------------------------------------------------------------------------------
setDataForConfDialog() {
  local dialogNum=$1
  local selLocale=""
  local idx=$(expr ${dialogNum} - 1)
  local localeData=()

  if [ "$#" -gt 2 ]; then
    selLocale=$(trimString "$3")
  else
    local concatStr="${KAAGI_LOCALES["$2"]}"
    readarray -t localeData <<< "${concatStr//;/$'\n'}"
    if [ ${#localeData[@]} -lt 2 ]; then
      selLocale="${localeData[0]}"
    fi
  fi

  if [ ${#selLocale} -gt 0 ]; then
    lclDlgData[${idx}]=$(getTextForConfDlg "$selLocale")
  else
    lclDlgData[${idx}]=""
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                      getTextForConfDlg
# DESCRIPTION: Get the text to be displayed in the confirmation dialog.
#      RETURN: Concatenated string containing the locale selected, language of the locale,
#              and country of locale.
#  Required Params:
#      1) selLocale - the locale that was selected or the default
#---------------------------------------------------------------------------------------
function getTextForConfDlg() {
  local selData=()
  local dialogText=$(printf "Confirm to set the Locale to:  [%s]" "$1")
  local textArray=("$dialogText")

  mbSelVal="$1"

  readarray -t selData <<< "${mbSelVal//_/$'\n'}"

  dialogText="${selData[0]}"
  dialogText=$(printf "%15sLanguage [%s]:  \"%s\"" " " "$dialogText" "${ISO_LANG_NAMES["$dialogText"]}")
  textArray+=("$dialogText")

  if [ ${#selData[@]} -gt 1 ]; then
    dialogText="${selData[1]:0:2}"
    dialogText=$(printf "%16sCountry [%s]:  \"%s\"" " " "$dialogText" "${ISO_CODE_NAMES["$dialogText"]}")
    textArray+=("$dialogText")
  fi

  dialogText=$(getTextForDialog "${textArray[@]}")
  echo "$dialogText"
}

#---------------------------------------------------------------------------------------
#      METHOD:                     showDlgForHostName
# DESCRIPTION: Uses a yad "form" dialog to get the name of the host that the computer
#              will be configured with.  Sets the value that was entered in the
#              global variable "ibEnteredText"
#  Required Params:
#      1)   dialogTitle - String to be displayed at the top of the text area.
#      2) dialogTextHdr - String to be displayed at the top of the dialog window.
#      3)    dialogText - Text to display inside the dialog before the
#                         list of menu options
#      4)     initValue - the initial value to be displayed
#      5)      helpText - Text to display on the help/more dialog
#---------------------------------------------------------------------------------------
showDlgForHostName() {
  local -A helpOpts=(["buttons-layout"]="center" ["window-icon"]="gtk-help.png" ["title"]="Help:  $1"
    ["center"]=true ["height"]=350 ["width"]=675 ["wrap"]=true ["fontname"]="Sans 12")
  local dialogButtons=("Ok" "Exit" "Help")
  local dialogText=$(getDlgTextForHostNameForm "$2" "$3")
  local -A yadOpts=(["align"]="right" ["buttons-layout"]="center" ["center"]=true ["title"]="$1"
    ["text"]="<span font_weight='bold' font_size='medium'>$dialogText</span>" ["text-align"]="left"
    ["height"]=350 ["width"]=625 ["image"]="$YAD_GRAPHICAL_PATH/images/network-config.png"
    ["image-on-top"]=true ["window-icon"]="config-system.png" ["separator"]="\"$FORM_FLD_SEP\"")
  local formVals=""
  local formFields=("\"Host Name:\":STE \"$4\"")
  local cmd=$(getCommandForFormDialog yadOpts formFields dialogButtons)
  local isValid=$(echo 'false' && return 1)

  while true; do
    formVals=$(eval "$cmd")
    btnClick=$?

    case ${btnClick} in
      ${DIALOG_OK})
        IFS=$'\n' read -d '' -a formFields <<< "${formVals//$FORM_FLD_SEP/$'\n'}"
        ibEnteredText=$(trimString "${formFields[0]}")
        isValid=$(isValidHostName)
        if ${isValid}; then
          break
        else
          dispErrMsgDlg
        fi
      ;;
      ${DIALOG_EXIT}|${DIALOG_ESC})
        ibEnteredText=""
        break
      ;;
      ${DIALOG_HELP})
        showTextInfoDialog "$5" helpOpts
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getDialogTextForPswdForm
# DESCRIPTION: Get the text to display in a yad "form" dialog
#      RETURN: concatenated string
#  Required Params:
#      1) headerText - Text inside the dialog displayed at the very top.
#      2) footerText - Text inside the dialog displayed at the very bottom.
#---------------------------------------------------------------------------------------
function getDlgTextForHostNameForm() {
  local hdr=$(printf "%12s%s" " " "$1")
  local textArray=("$hdr" " " " " " " " $2")
  local dialogText=$(printf "\n%s" "${textArray[@]}")

  echo "$dialogText"
}

#---------------------------------------------------------------------------------------
#      METHOD:                        dispErrMsgDlg
# DESCRIPTION: Displays the appropriate error message
#---------------------------------------------------------------------------------------
dispErrMsgDlg() {
  local dialogTitle=$(echo "Error:  Invalid Name for Host")
  local dialogText=$(printf "$INV_HOST_NAME_MSG" "$ibEnteredText")
  local -A yadOpts=(["buttons-layout"]="center" ["image"]="$YAD_GRAPHICAL_PATH/images/error-icon.png"
    ["image-on-top"]=true ["title"]="$dialogTitle" ["window-icon"]="error-window-icon.png"
    ["text"]="<span font_weight='bold' font_size='large'>ERROR!!!!!!!</span>" ["text-align"]="left"
    ["height"]=425 ["width"]=600 ["wrap"]=true ["fontname"]="Sans 12" ["center"]=true ["wrap"]=true ["size"]="fit")

  local dlgBtns=("Exit")

  showTextInfoDialog "$dialogText" yadOpts dlgBtns
}

#---------------------------------------------------------------------------------------
#      METHOD:                      showDlgForNetMans
# DESCRIPTION: Show a linux "radiolist" dialog to select the type of network manager to
#              manage network connection settings.  Set the value within the global
#              variable "ibEnteredText"
#  Required Params:
#      1)   dialogTitle - String to be displayed at the top of the dialog window.
#      2) dialogTextHdr - String to be displayed at the top of the text area.
#      3)    dialogText - Text to display inside the dialog before the
#                         list of menu options
#      4)      helpText - Text to display on the help/more dialog
#---------------------------------------------------------------------------------------
showDlgForNetMans() {
  local dlgButtons=("Ok" "Cancel" "Help")
  local dlgColumns=("Radio Btn" "AUR Package" "Description")
  local title=$(printf "%12s%s" " " "$2")
  local textArray=("$title" " " " " " " " " "$3")
  local dialogText=$(printf "\n%s" "${textArray[@]}")
  dialogText=$(escapeSpecialCharacters "${dialogText:1}")
  local -A dlgOpts=(["height"]=500 ["width"]=655 ["image"]="$YAD_GRAPHICAL_PATH/images/network-manager.png"
    ["image-on-top"]=true ["window-icon"]="config-system.png")
  local -A helpDlgOpts=(["height"]=500  ["width"]=700 ["title"]="Help:  $2")
  local dlgData=()
  local defChoice="${networkManagers[1]}"

  appendOptionsForRadiolist "$dialogText" "$1" dlgOpts
  setDataForMultiColList networkManagers AUR_PCKG_DESCS dlgData "$defChoice"
  local retVal=$(getValueFromRadiolist "$4" dlgOpts dlgButtons dlgColumns dlgData helpDlgOpts)

  if [ ${#retVal} -gt 0 ]; then
    mbSelVal="$retVal"
  else
    mbSelVal="$defChoice"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                    showDialogForISPsDNS
# DESCRIPTION: Uses a yad "form" dialog to get the ISP's domain name systems
#  Required Params:
#      1)   dialogTitle - String to be displayed at the top of the dialog window.
#      2) dialogTextHdr - String to be displayed at the top of the text area.
#      3)    dialogText - Text to display inside the dialog before the form
#      4)      helpText - Text to display on the help/more dialog
#---------------------------------------------------------------------------------------
showDialogForISPsDNS() {
  local dialogButtons=("Submit" "Skip" "Help")
  local -A helpOpts=(["buttons-layout"]="center" ["window-icon"]="gtk-help.png" ["title"]="Help:  $2"
    ["center"]=true ["height"]=350 ["width"]=675 ["wrap"]=true ["fontname"]="Sans 12")
  local title=$(printf "%12s%s" " " "$2")
  local textArray=("$title" " " " " " " " " "$3")
  local dialogText=$(printf "\n%s" "${textArray[@]}")
  local -A yadOpts=(["align"]="right" ["buttons-layout"]="center" ["center"]=true ["title"]="$1"
    ["text"]="<span font_weight='bold' font_size='medium'>$dialogText</span>" ["text-align"]="left"
    ["height"]=350 ["width"]=560 ["image"]="$YAD_GRAPHICAL_PATH/images/dns.png"
    ["image-on-top"]=true ["window-icon"]="password-icon.png" ["separator"]="\"$FORM_FLD_SEP\"")
  local formVals=""
  local formFields=("\"Primary:\":STE" "\"Secondary:\":STE")
  local cmd=$(getCommandForFormDialog yadOpts formFields dialogButtons)

  while true; do
    formVals=$(eval "$cmd")
    btnClick=$?

    case ${btnClick} in
      ${DIALOG_OK})
        IFS=$'\n' read -d '' -a formFields <<< "${formVals//$FORM_FLD_SEP/$'\n'}"
        dnsServers=("${formFields[@]}")
        break
      ;;
      ${DIALOG_CANCEL}|${DIALOG_ESC})
        dnsServers=()
        break
      ;;
      ${DIALOG_HELP})
        showTextInfoDialog "$4" helpOpts
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                    showDialogForDnsmasq
# DESCRIPTION: Show a yad "radiolist" dialog to select the free DNS provider.
#              Set the value within the global variable "$mbSelVal".
#  Required Params:
#      1)   dialogTitle - String to be displayed at the top of the dialog window.
#      2) dialogTextHdr - String to be displayed at the top of the text area.
#      3)    dialogText - Text to display inside the dialog before the
#                           list of menu options
#      4)      helpText - Text to display on the help/more dialog
#---------------------------------------------------------------------------------------
showDialogForDnsmasq() {
  local dlgButtons=("Ok" "Cancel" "Help")
  local dlgColumns=("Radio Btn" "DNS Provider" "$5 Primary" "$5 Secondary")
  local title=$(printf "%32s%s" " " "$2")
  local textArray=("$title" " " " " " " " " "$3")
  local -A dlgOpts=(["height"]=500 ["width"]=655 ["image"]="$YAD_GRAPHICAL_PATH/images/dnsmasq.png"
    ["image-on-top"]=true ["window-icon"]="config-system.png")
  local -A helpDlgOpts=(["width"]=610 ["title"]="Help:  $2")
  local dlgData=()
  local ipAddrs=()
  local dialogText=$(printf "\n%s" "${textArray[@]}")
  dialogText=$(escapeSpecialCharacters "${dialogText:1}")

  appendOptionsForRadiolist "$dialogText" "$1" dlgOpts
  for opt in "${FREE_DNS_PROVIDERS[@]}"; do
    dlgData+=(FALSE)
    dlgData+=("$opt")
    readarray -t ipAddrs <<< "${FREE_DNS_IPS["$opt"]//$FORM_FLD_SEP/$'\n'}"
    dlgData+=("${ipAddrs[@]}")
  done

  dlgData[0]=TRUE

  local retVal=$(getValueFromRadiolist "$4" dlgOpts dlgButtons dlgColumns dlgData helpDlgOpts)

  if [ ${#retVal} -gt 0 ]; then
    mbSelVal="$retVal"
  else
    mbSelVal="${FREE_DNS_PROVIDERS[0]}"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                  selectLinuxCapableBootLoader
# DESCRIPTION: Select the boot loader that will be used to boot up the system.   Set the
#              name of the boot loader in the global variable "mbSelVal"
#  Required Params:
#      1) dlgFuncParams - associative array of key/value pairs of parameters for the method
#                      "choices" - choices of names for the available boot loaders
#              "dialogBackTitle" - Text inside the dialog displayed at the very top.
#                   "partLayout" - the partition layout/scheme
#                      "pttDesc" - partition table type
#                 "noCancelFlag" - NO Cancel button will be shown
#---------------------------------------------------------------------------------------
selectLinuxCapableBootLoader() {
  local -n dlgFuncParams=$1
  local dialogTextHdr="${dlgFuncParams[dialogBackTitle]}"
  local dialogTitle="${dlgFuncParams[dialogTitle]}"
  local partLayout="${dlgFuncParams[partLayout]}"
  local dlgButtons=("Ok" "Help")
  local dlgColumns=("Radio Btn" "Boot Loader" "Description")
  local -A dlgOpts=(["height"]=450 ["width"]=500 ["image-on-top"]=true )
  local blChoices=()
  local -A blDescs=()
  local dlgData=()
  local pttDesc="${dlgFuncParams[pttDesc]}"
  local dialogText=$(getDialogTextForBL "$dialogTextHdr" "$pttDesc")
  local helpText=$(getHelpTextForBL "$pttDesc")
  local -A helpDlgOpts=(["width"]=675 ["title"]="Help:  $dialogTitle")
  local imageName=$(cat "$DIALOG_IMG_ICON_FILE" | cut -d',' -f1)
  local retVal=""
  local confirmFlag=$(echo 'false' && return 1)

  dlgOpts["image"]="$YAD_GRAPHICAL_PATH/images/$imageName"
  dlgOpts["window-icon"]=$(cat "$DIALOG_IMG_ICON_FILE" | cut -d',' -f2)

  IFS=$'\t' read -a blChoices <<< "${dlgFuncParams["choices"]}"
  for choice in "${blChoices[@]}"; do
    blDescs["$choice"]="${dlgFuncParams[$choice]}"
  done

  dialogText=$(escapeSpecialCharacters "${dialogText:1}")
  appendOptionsForRadiolist "$dialogText" "$dialogTitle" dlgOpts
  setDataForMultiColList blChoices blDescs dlgData

  while true; do
    retVal=$(getValueFromRadiolist "$helpText" dlgOpts dlgButtons dlgColumns dlgData helpDlgOpts)
    if [ ${#retVal} -gt 0 ]; then
      mbSelVal=$(trimString "$retVal")
      break
    fi
  done
}
