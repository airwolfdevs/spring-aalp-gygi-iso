#!/bin/bash
#set -e

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================
source "$GRAPHICAL_PATH/yad-nb-inc.sh"

#===============================================================================
# globals
#===============================================================================

#===============================================================================
# functions/methods
#===============================================================================

#---------------------------------------------------------------------------------------
#      METHOD:                      showTimeZoneConf
# DESCRIPTION: Show a yad "dialog" box of type "list" to get confirmation to
#              set the time zone to the value being displayed.
#  Required Params:
#      1)   dialogTitle - String to be displayed at the top of the dialog window.
#      2) dialogTextHdr - String to be displayed at the top of the text area.
#      3)      timeZone - The Time Zone that was either automatically determined or
#                         manually selected
#      4)      helpText - Text to display on the help/more dialog
#      5)      autoFlag - flag to determine if time zone was manually entered or
#                         automatically determined
#---------------------------------------------------------------------------------------
showTimeZoneConf() {
  local helpText="$4"
  local autoFlag=$5
  local dataFileName=$(printf " < \"%s\" 2> /dev/null" "$DATA_FILE_NAME")
  local yadCmd=$(getCmdForConfDlg "$1" "$2" "$3" "$dataFileName")
  local -A helpDlgOpts=(["buttons-layout"]="center" ["window-icon"]="gtk-help.png"
    ["title"]="$1" ["center"]=true ["height"]=350 ["width"]=610 ["wrap"]=true ["fontname"]="Sans 12")

  while true; do
    setDataForConfDlg ${autoFlag} "$timeZone"
    eval "$yadCmd"
    btnClick=$?

    case ${btnClick} in
      ${DIALOG_OK})
        yesNoFlag=$(echo 'true' && return 0)
        break
      ;;
      ${DIALOG_CANCEL})
        break
      ;;
      ${DIALOG_HELP})
        cleanDialogFiles
        showTextInfoDialog "$helpText" helpDlgOpts
      ;;
      ${DIALOG_ESC})
        break
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                      getCmdForConfDlg
# DESCRIPTION: Get the command to show a yad "dialog" box of type "list" to get
#              confirmation to set the time zone.
#      RETURN: concatenated string
#  Required Params:
#      1)   dialogTitle - String to be displayed at the top of the dialog window.
#      2) dialogTextHdr - String to be displayed at the top of the text area.
#      3)      timeZone - The Time Zone that was either automatically determined or
#                         manually selected
#      4)  dataFileName - full path to the file that contains the data to display
#---------------------------------------------------------------------------------------
function getCmdForConfDlg() {
  local dialogTitle="$1"
  local dialogText=$(printf "%12s%s" " " "$2")
  local timeZone="$3"
  local dataFileName="$4"
  local dlgBtns=("Yes" "No" "Help")
  local dlgColumns=("Field Name" "Value")
  local imagePath=$(getImagePath "$timeZone")
  local -A dlgOpts=(["buttons-layout"]="center" ["image"]="$imagePath" ["grid-lines"]="both" ["no-selection"]=true
    ["image-on-top"]=true ["title"]="$dialogTitle" ["window-icon"]="config-system.png"
    ["text"]="<span font_weight='bold' font_size='medium'>$dialogText</span>" ["text-align"]="left"
    ["height"]=550 ["width"]=675 ["fontname"]="Sans 12" ["center"]=true ["wrap"]=true ["size"]="fit")

  local yadCmd=$(echo "$DIALOG ")
  yadCmd+=$(getOptionsForListDialog dlgOpts)
  yadCmd+=$(getButtonsForDialog dlgBtns)
  yadCmd+=$(getColumnsForListDialog dlgColumns)
  yadCmd+="$dataFileName"

  echo "$yadCmd"
}

#---------------------------------------------------------------------------------------
#      METHOD:                      setDataForConfDlg
# DESCRIPTION: Set the data to be displayed .
#  Required Params:
#      1) autoFlag - flag to determine if time zone was manually entered or
#                    automatically determined
#      2) timeZone - The Time Zone that was either automatically determined or
#                    manually selected
#---------------------------------------------------------------------------------------
setDataForConfDlg() {
  local autoFlag=$1
  local timeZone="$2"
  local tzSZ=()
  local numSpaces=0
  local len=0

  addStatusDataToFile "$DATA_FILE_NAME"

  echo " " >> "$DATA_FILE_NAME"
  echo " " >> "$DATA_FILE_NAME"
  echo " " >> "$DATA_FILE_NAME"
  if ${autoFlag}; then
    echo "Confirm Automatically Generated Time Zone?" >> "$DATA_FILE_NAME"
  else
    echo "Confirm Selected Time Zone?" >> "$DATA_FILE_NAME"
  fi
  IFS='/' read -a tzSZ <<< "$timeZone"
  local lbl=$(printf "%45s Zone" " ")
  echo "$lbl" >> "$DATA_FILE_NAME"
  echo "${tzSZ[0]}" >> "$DATA_FILE_NAME"
  if [ ${#tzSZ[@]} -gt 1 ]; then
    lbl=$(printf "%35s Sub-Zone" " ")
    echo "$lbl" >> "$DATA_FILE_NAME"
    echo "${tzSZ[1]}" >> "$DATA_FILE_NAME"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                     addStatusDataToFile
# DESCRIPTION: Add the data to the file in order to display the current settings of the
#              Hardware & System Clocks
#  Required Params:
#      1) dataFileName - full path to the file that contains the data.
#---------------------------------------------------------------------------------------
addStatusDataToFile() {
  local dataFileName="$1"
  local statusText=$(timedatectl status)
  local statusArray=()
  local cols=()
  local numSpaces=0
  local len=0
  local pattern="System"

  echo " " > "$dataFileName"
  echo "Current settings of the Hardware &amp; System Clocks:" >> "$dataFileName"

  readarray -t statusArray <<< "$statusText"
  if [ ${#statusArray[@]} -gt 6 ]; then
    pattern="systemd"
  fi
  for statusText in "${statusArray[@]}"; do
    readarray -t cols <<< "${statusText//: /$'\n'}"
    cols[0]=$(trimString "${cols[0]}")
    if [[ ! "${cols[0]}" =~ "$pattern" ]]; then
      if [[ "${cols[0]}" =~ "System" ]]; then
        numSpaces=14
      else
        len=${#cols[0]}
        case ${len} in
          8)
            numSpaces=36
          ;;
          9)
            numSpaces=34
          ;;
          10)
            numSpaces=34
          ;;
          11)
            numSpaces=31
          ;;
          14)
            numSpaces=25
          ;;
          15)
            numSpaces=24
          ;;
        esac
        if [ "$pattern" == "systemd" ]; then
          numSpaces=$(expr ${numSpaces} + 15)
        fi
      fi

      local fmt=$(echo "%${numSpaces}s %s")
      cols[0]=$(printf "$fmt" " " "${cols[0]}")
    fi
    echo "${cols[0]}" >> "$dataFileName"
    echo "${cols[1]}" >> "$dataFileName"
  done
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                        getImagePath
# DESCRIPTION: Get the full path to image file to display in the dialog.
#      RETURN: full path
#   Optional Param:
#      1) tzParam - Either:
#                   A) The Time Zone that was automatically determined
#                   B) The Zone of a Time Zone that was selected
#---------------------------------------------------------------------------------------
function getImagePath() {
  local tzParam=$(trimString "$1")
  local imageFN="$YAD_GRAPHICAL_PATH/images/HardwareSystemClock.png"

  if [ "${#tzParam}" -gt 0 ]; then
    local tzSZ=()
    IFS='/' read -a tzSZ <<< "$tzParam"
    local imageNameFmt="$YAD_GRAPHICAL_PATH/images/zones/%s.png"
    imageFN=$(printf "$imageNameFmt" "${tzSZ[0]}")
    if [ ! -f "$imageFN" ]; then
      if [ "$zone" == "Atlantic" ] || [ "$zone" == "Pacific" ]; then
        imageFN=$(printf "$imageNameFmt" "Atlantic-Pacific")
      else
        imageFN=$(printf "$imageNameFmt" "Other")
      fi
    fi
  fi

  echo "$imageFN"
}

#---------------------------------------------------------------------------------------
#      METHOD:                       showZoneDialog
# DESCRIPTION: Show a yad "notebook" dialog to select the zone section of a time zone.
#              Set the value within the global variable "$mbSelVal"
#  Required Params:
#      1)   dialogTitle - String to be displayed at the top of the dialog window.
#      2) dialogTextHdr - String to be displayed at the top of the text area.
#      3)      helpText - Text to display on the help/more dialog
#---------------------------------------------------------------------------------------
showZoneDialog() {
  local -A nbFuncParams=()
  local dialogTextHdr="Configuration of Time Zone (Zone/Sub-Zone)"
  local title=$(printf "%12s%s" " " "$dialogTextHdr")
  local textArray=("$title" " " " " " " " " " " "      Select the Zone:")
  local dialogText=$(printf "\n%s" "${textArray[@]}")
  local optNum=0
  local zoneOpts=()
  local key=""

  for zone in "${ZONE_ARRAY[@]}"; do
    optNum=$(expr ${optNum} + 1)
    key=$(printf "#%d" ${optNum})
    zoneOpts+=("$key")
    nbFuncParams["$key"]="$zone"
  done

  nbFuncParams["choices"]=$(printf "\t%s" "${zoneOpts[@]}")
  nbFuncParams["choices"]=${nbFuncParams["choices"]:1}
  nbFuncParams["dialogTitle"]="$1"
  nbFuncParams["dialogText"]=$(escapeSpecialCharacters "${dialogText:1}")
  nbFuncParams["helpText"]="$3"
  nbFuncParams["numTabs"]=2

  local zone=$(getLastTabValFromNB)
  if [ ${#zone} -gt 0 ]; then
    mbSelVal="$zone"
  else
    mbSelVal=""
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                      showSubZoneDialog
# DESCRIPTION: Show a yad "radiolist" dialog to select the sub-zone of a time zone.
#              Set the value within the global variable "$mbSelVal".
#  Required Params:
#      1)   dialogTitle - String to be displayed at the top of the dialog window.
#      2) dialogTextHdr - String to be displayed at the top of the text area.
#      3)          zone - The Zone of a time zone that was selected
#      4)      helpText - Text to display on the help/more dialog
#---------------------------------------------------------------------------------------
showSubZoneDialog() {
  local -A nbFuncParams=()
  local dialogTextHdr="Configuration of Time Zone (Zone/Sub-Zone)"
  local title=$(printf "%12s%s" " " "$dialogTextHdr")
  local textArray=("$title" " " " " " " " " " " "      Select the Sub-Zone:")
  local dialogText=$(printf "\n%s" "${textArray[@]}")
  local subZoneOpts=()

  for locSubZone in "${subZones[@]}"; do
    optNum=$(expr ${optNum} + 1)
    key=$(printf "#%d" ${optNum})
    subZoneOpts+=("$key")
    nbFuncParams["$key"]="$locSubZone"
  done

  nbFuncParams["choices"]=$(printf "\t%s" "${subZoneOpts[@]}")
  nbFuncParams["choices"]=${nbFuncParams["choices"]:1}
  nbFuncParams["dialogTitle"]="$1"
  nbFuncParams["dialogText"]=$(escapeSpecialCharacters "${dialogText:1}")
  nbFuncParams["zone"]="$3"
  nbFuncParams["helpText"]="$4"
  nbFuncParams["numTabs"]=3

  local subZone=$(getLastTabValFromNB)
  if [ ${#subZone} -gt 0 ]; then
    mbSelVal="$subZone"
  else
    mbSelVal=""
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                   setCmdToShowNotebook
# DESCRIPTION: Set the command to display the a yad "notebook" dialog with
#              the following tabs:
#              #1)      "Status" - a summary of the logical volumes & partitions
#              #2) Either:
#                   a)   "Select Zone" - available list of zones
#                   a) "Zone Selected" - the zone that was selected
#              #3)   "Select Sub-Zone" - available list of sub-zones for the zone
#  Required Params:
#      1)      nbKeyPlug - integer value for the notebook key & plug of its tabs
#---------------------------------------------------------------------------------------
setCmdToShowNotebookForTZ() {
  local nbKeyPlug="$1"
  local tabOutput=$(echo " < \"%s\" &> ${NB_TAB_DATA_FNS["nbCmd#01"]} 2> /dev/null &")
  local numTabs=${nbFuncParams["numTabs"]}
  local fileName=""
  local tabRedirect=""
  local nbCmd=$(echo "$DIALOG --notebook --key=${nbKeyPlug}")
  local tabCmd=""
  local tabName=""
  local -A yadTabOpts=(["grid-lines"]="both" ["no-selection"]=true ["notebook"]=true )
  local nbButtons=("Ok" "Cancel" "Help")
  local nbFormBtns=()
  local -A nbOpts=(["buttons-layout"]="center" ["center"]=true ["height"]=550 ["width"]=725
    ["title"]="${nbFuncParams["dialogTitle"]}" ["window-icon"]="config-system.png" ["image-on-top"]="true"
    ["text-align"]="left" ["text"]="<span font_weight='bold' font_size='medium'>${nbFuncParams["dialogText"]}</span>")
  local listColHdrs=()
  local assocKeys=("text-align" "text" "image" "image-on-top")

  nbOpts["image"]=$(getImagePath "${nbFuncParams["zone"]}")
  nbFuncParams["removeKeys"]=$(printf "\n%s" "${assocKeys[@]}")
  nbFuncParams["removeKeys"]=${nbFuncParams["removeKeys"]:1}

  if [ ${numTabs} -lt 3 ]; then
    nbButtons[1]="Exit"
  fi

  for tabNum in $(eval echo "{1..${numTabs}}"); do
    fileName=$(printf "${NB_TAB_DATA_FNS["tabData"]}" ${tabNum})
    tabRedirect=$(printf "$tabOutput" "$fileName" ${tabNum})
    case ${tabNum} in
      1)
        tabCmd=$(getCmdForStatusTab ${nbKeyPlug} ${tabNum} "$tabRedirect")
        tabName="Status"
      ;;
      2)
        tabCmd=$(getCmdForZoneTab ${nbKeyPlug} ${tabNum} "$tabRedirect")
        if [ ${nbFuncParams["zone"]+_} ]; then
          tabName="Zone"
        else
          tabName="Select Zone"
        fi
      ;;
      3)
        tabCmd=$(getCmdForSubZoneTab ${nbKeyPlug} ${tabNum} "$tabRedirect")
        tabName="Select Sub-Zone"
      ;;
    esac
    notebookCmds+=("$tabCmd")
    nbCmd+=$(printf " --tab=\"%s\"" "$tabName")
  done

  nbCmd+=$(getOptionsForNotebook nbOpts)
  nbCmd+=$(getButtonsForDialog nbButtons)
  notebookCmds+=("$nbCmd")
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     getCmdForStatusTab
# DESCRIPTION: Display a summary of the progress made in the partitioning process within
#              a tab containing a yad "list" dialog/widget
#      RETURN: concatenated string for command
#  Required Params:
#      1) nbKeyPlug - integer value for the notebook key & plug of its tabs
#      2)    tabNum - tab number
#      3) tabOutput - contains the name of the data file to load the tab with
#                     as well as the file name of the output generated by the tab
#---------------------------------------------------------------------------------------
function getCmdForStatusTab() {
  local nbKeyPlug="$1"
  local tabNum=$2
  local tabOutput="$3"
  local tabColumns=("Field Name" "Value")

  local yadCmd=$(echo "$DIALOG --plug=${nbKeyPlug} --tabnum=${tabNum}")
  yadCmd+=$(getOptionsForListDialog yadTabOpts)
  yadCmd+=$(getColumnsForListDialog tabColumns)
  yadCmd+="$tabOutput"

  echo "$yadCmd"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                      getCmdForZoneTab
# DESCRIPTION: Get the command to select from a list of available zones for time zone
#              within a tab containing a yad "radio list" dialog/widget
#      RETURN: concatenated string for command
#  Required Params:
#      1) nbKeyPlug - integer value for the notebook key & plug of its tabs
#      2)    tabNum - tab number
#      3) tabOutput - contains the name of the data file to load the tab with
#                     as well as the file name of the output generated by the tab
#---------------------------------------------------------------------------------------
function getCmdForZoneTab() {
  local nbKeyPlug="$1"
  local tabNum=$2
  local tabOutput="$3"
  local listColHdrs=("Radio Btn" "Option" "Zone")

  nbFuncParams["list-type"]="radiolist"
  if [ ${nbFuncParams["zone"]+_} ]; then
    optNum=$(findPositionForString ZONE_ARRAY "${nbFuncParams["zone"]}")
    key=$(printf "#%d" ${optNum})
    nbFuncParams["choices"]="$key"
    nbFuncParams["$key"]="${nbFuncParams["zone"]}"
  fi

  local tabCmd=$(getCmdForRadioListTab ${nbKeyPlug} ${tabNum} "$tabOutput")

  echo "$tabCmd"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     getCmdForSubZoneTab
# DESCRIPTION: Get the command to select a sub-zone for the zone that was selected
#              within a tab containing a yad "radio list" dialog/widget
#      RETURN: concatenated string for command
#  Required Params:
#      1) nbKeyPlug - integer value for the notebook key & plug of its tabs
#      2)    tabNum - tab number
#      3) tabOutput - contains the name of the data file to load the tab with
#                     as well as the file name of the output generated by the tab
#---------------------------------------------------------------------------------------
function getCmdForSubZoneTab() {
  local nbKeyPlug="$1"
  local tabNum=$2
  local tabOutput="$3"
  local listColHdrs=("Radio Btn" "Option" "Sub-Zone")
  local optNum=0
  local key=""

  nbFuncParams["list-type"]="radiolist"

  local tabCmd=$(getCmdForRadioListTab ${nbKeyPlug} ${tabNum} "$tabOutput")

  echo "$tabCmd"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                      getTimeZoneFromNB
# DESCRIPTION: Get the submitted zone and sub-zone of a time zone.
#      RETURN: concatenated string of the values submitted separated by '/'
#---------------------------------------------------------------------------------------
function getLastTabValFromNB() {
  local nbKeyPlug=$(getSixDigitRandomNum)
  local notebookCmds=()
  local nbCmd=""
  local dataFileName=$(printf "${NB_TAB_DATA_FNS["tabData"]}" 1)
  local -A helpDlgOpts=(["buttons-layout"]="center" ["window-icon"]="gtk-help.png"
    ["title"]="Help:  ${nbFuncParams["dialogTitle"]}" ["center"]=true ["height"]=350
    ["width"]=650 ["wrap"]=true ["fontname"]="Sans 12")
  local submittedVals=""
  local tabVals=()
  local tabVal=""

  setCmdToShowNotebookForTZ ${nbKeyPlug}
  nbCmd=$(printf " %s" "${notebookCmds[@]}")
  nbCmd=${nbCmd:1}

  while true; do
    rm -rf "$dataFileName"
    addStatusDataToFile "$dataFileName"
    eval "$nbCmd"
    btnClick=$?

    case ${btnClick} in
      ${DIALOG_OK})
        dataFileName=$(printf "${NB_TAB_DATA_FNS["nbCmd#01"]}" ${nbFuncParams["numTabs"]})
        submittedVals=$(cat "$dataFileName")
        readarray -t tabVals <<< "${submittedVals//$FORM_FLD_SEP/$'\n'}"
        tabVal="${tabVals[-2]}"
        break
      ;;
      ${DIALOG_CANCEL}|${DIALOG_EXIT}|${DIALOG_ESC})
        tabVal=""
        break
      ;;
      ${DIALOG_HELP})
        showTextInfoDialog "${nbFuncParams["helpText"]}" helpDlgOpts
      ;;
    esac
  done

  echo "$tabVal"
}

#---------------------------------------------------------------------------------------
#      METHOD:               showTimeStandardDialog
# DESCRIPTION: Displays a yad "dialog" of type text-info to choose the time standard for
#              the hardware clock to be set to.  Set the value within the global
#              variable "$inputRetVal"
#  Required Params:
#      1)   dialogTitle - String to be displayed at the top of the dialog window.
#      2) dialogTextHdr - String to be displayed at the top of the text area.
#      3)      helpText - Text to display on the help/more dialog
#      4)    dialogText - Text to be displayed in the text area of the dialog
#---------------------------------------------------------------------------------------
showTimeStandardDialog() {
  local -A yadOpts=(["buttons-layout"]="center" ["image"]="$YAD_GRAPHICAL_PATH/images/HardwareSystemClock.png"
    ["image-on-top"]=true ["title"]="$1" ["window-icon"]="config-system.png"
    ["text"]="<span font_weight='bold' font_size='medium'>$2</span>" ["text-align"]="left"
    ["height"]=500 ["width"]=610 ["fontname"]="Sans 12" ["center"]=true ["wrap"]=true ["size"]="fit")
  local dlgBtns=("Local Time" "UTC" "Exit" "Help")
  local -A helpDlgOpts=(["buttons-layout"]="center" ["window-icon"]="gtk-help.png"
    ["title"]="$2" ["center"]=true ["height"]=350 ["width"]=610 ["wrap"]=true ["fontname"]="Sans 12")

  while true; do
    showTextInfoDialog "$4" yadOpts dlgBtns
    btnClick=$?
    case ${btnClick} in
      $DIALOG_EXIT|${DIALOG_ESC})
        inputRetVal=""
        break
      ;;
      ${DIALOG_LOCAL})
        inputRetVal="localtime"
        break
      ;;
      ${DIALOG_UTC})
        inputRetVal="UTC"
        break
      ;;
      ${DIALOG_HELP})
        showTextInfoDialog "$3" helpDlgOpts
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                    showLocalTimeWarning
# DESCRIPTION: Get confirmation to continue to use the local timescale as the time standard.
#  Required Params:
#      1)   dialogTitle - String to be displayed at the top of the dialog box.
#      2) dialogTextHdr - String to be displayed at the top of the text area.
#      3)    dialogText - Warning message to be displayed in the text area of the dialog
#---------------------------------------------------------------------------------------
showLocalTimeWarning() {
  local dialogText=$(getTextForDialog "${textArray[@]}")
  local -A yadOpts=(["buttons-layout"]="center" ["image"]="$YAD_GRAPHICAL_PATH/images/warning-icon.png"
    ["image-on-top"]=true ["title"]="$1" ["window-icon"]="warning-window-icon.png"
    ["text"]="<span font_weight='bold' font_size='large'>WARNING!!!!!!!\n\n\n$2</span>" ["text-align"]="left"
    ["height"]=500 ["width"]=600 ["wrap"]=true ["fontname"]="Sans 12" ["center"]=true ["wrap"]=true ["size"]="fit")

  local dlgBtns=("Use Local" "Use UTC")

  showTextInfoDialog "$3" yadOpts dlgBtns
  btnClick=$?

  case ${btnClick} in
    ${DIALOG_LOCAL})
      yesNoFlag=$(echo 'true' && return 0)
    ;;
    ${DIALOG_UTC}|${DIALOG_ESC})
      yesNoFlag=$(echo 'false' && return 1)
    ;;
  esac
}
