#!/bin/bash
#set -e

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# functions/methods
#===============================================================================

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getDialogOptionforPPT
# DESCRIPTION: Get the option for the type of process to use to partition the disk
#      RETURN: type of partition process in the global "mbSelVal" variable
#  Required Params:
#      1) dialogTitle - String to be displayed at the top of the dialog box.
#      2)    helpText - text to display in the yad "text-info" dialog for viewing the help
#      3)  pptChoices - array of options that can be selected in the dialog
#---------------------------------------------------------------------------------------
getDialogOptionforPPT() {
  local dialogTitle="$1"
  local helpText="$2"
  local -n pptChoices=$3
  local dlgButtons=("Ok" "Cancel" "Help")
  local dlgColumns=("Radio Btn" "Partition Process Type" "PPT Description")
  local -A dlgOpts=(["height"]=350 ["window-icon"]="text-editor.png")
  local lastIdx=$(expr ${#pptChoices[@]} - 1)
  local -A pptDescs=()
  local dlgData=()
  local dialogText=$(getDialogTextForPPT)

  for aryIdx in $(eval echo "{0..$lastIdx}"); do
    local key=$(echo "${PART_PROC_TYPE_CHOICES[$aryIdx]}")
    pptDescs["$key"]=$(echo "${pptChoices[$aryIdx]}")
  done

  appendOptionsForRadiolist "$dialogText" "$dialogTitle" dlgOpts
  setDataForMultiColList PART_PROC_TYPE_CHOICES pptDescs dlgData

  mbSelVal=$(getValueFromRadiolist "$helpText" dlgOpts dlgButtons dlgColumns dlgData)
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getDialogTextForPPT
# DESCRIPTION: Get the text to display within a yad "dialog" for choosing the process to
#              partition the disk
#      RETURN: Concatenated string
#---------------------------------------------------------------------------------------
function getDialogTextForPPT() {
  local textArray=(
  "Partitioning a hard drive allows one to logically divide the available space into sections that"
  "can be accessed independently of one another."
  "$BORDER_WIDTH_600" " "
  "Choose which process you want to use:"
  )
  local dialogText=$(getTextForDialog "${textArray[@]}")
  echo "$dialogText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                getKeepOrZapOptForCurLayout
# DESCRIPTION: Get the option to either keep or destroy (i.e. zap) the current
#              partition layout/scheme.
#      RETURN: option chosen in the global variable "inputRetVal"
#  Required Params:
#      1) partitionLayout - string containing header, logical volumes and partitions
#---------------------------------------------------------------------------------------
getKeepOrZapOptForCurLayout() {
  local partitionLayout="$1"
  local dialogTitle="Keep OR Zap Partition Layout/Scheme"
  local dlgButtons=("Keep" "Zap" "Help")
  local dlgColumns=("Count" "Description")
  local helpText=$(getHelpTextForCurLayout)
  local hdr=$(getHeaderForTable "$partitionLayout")
  local -A helpTabOpts=(["wrap"]=true ["fontname"]="Sans 12")
  local -A helpNbOpts=(["height"]=450 ["width"]=710 ["buttons-layout"]="center"
    ["title"]="$dialogTitle" ["window-icon"]="gtk-help.png" ["center"]=true)
  local rows=()
  local -A yadOpts=(["buttons-layout"]="center" ["grid-lines"]="both" ["height"]=300 ["width"]=600
    ["no-selection"]=true ["text"]="<span font_weight='bold' font_size='medium'>$hdr</span>"
    ["text-align"]="left" ["title"]="$dialogTitle" ["center"]=true ["window-icon"]="app-icon.png"
  )
  local dialogData=()

  local rows=()
  setSummaryOfCurLayout rows

  for row in "${rows[@]}"; do
    IFS=$";" read -a cols <<< "$row"
    dialogData+=("${cols[@]}")
  done
  while true; do
    yadDlgData=$(getChoicesFromListDialog yadOpts dlgButtons dlgColumns dialogData)
    IFS=',' read -a dialogVals <<< "$yadDlgData"

    case ${dialogVals[0]} in
      ${DIALOG_CANCEL})
        inputRetVal="EXIT"
        break
      ;;
      ${DIALOG_KEEP})
        inputRetVal=$(echo "${dialogVals[1]}")
        break
      ;;
      ${DIALOG_HELP})
        showHelpAndPartTbl "$partitionLayout" "$helpText" helpTabOpts helpNbOpts
      ;;
      ${DIALOG_ZAP})
        inputRetVal=$(echo "${dialogVals[1]}")
        break
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getChoiceWhenNoParts
# DESCRIPTION: Get choice when no partitions were found.
#      RETURN: In the global variable "inputRetVal":
#              YES) run the linux partition tool again
#               NO) exit the manual partition process
#  Required Params:
#      1)   dialogTitle - String to be displayed at the top of the dialog box.
#      2) partitionTool - name of the linux partition tool that was previously used
#---------------------------------------------------------------------------------------
getChoiceWhenNoParts() {
  local dialogTitle="$1"
  local partitionTool="$2"
  local textArray=(
  "The Partition Table does not contain any partitions."
  "------------------------------------------------------------------------------------------" " "
  "Click:"
  "        1) \"Ok\" to run the linux partition tool \"$partitionTool\" again"
  "                                         OR"
  "        2) \"Cancel\" to exit the Manual Partition Process"
  )
  local dialogText=$(getTextForDialog "${textArray[@]}")
  local -A yadOpts=(["buttons-layout"]="center" ["image"]="$YAD_GRAPHICAL_PATH/images/warning-icon.png"
    ["image-on-top"]=true ["title"]="$dialogTitle" ["window-icon"]="warning-window-icon.png"
    ["text"]="<span font_weight='bold' font_size='large'>WARNING!!!!!!!</span>" ["text-align"]="left"
    ["height"]=325 ["width"]=550 ["wrap"]=true ["fontname"]="Sans 12" ["center"]=true ["wrap"]=true ["size"]="fit")

  local dlgBtns=("Ok" "Cancel")

  showTextInfoDialog "$dialogText" yadOpts dlgBtns
  btnClick=$?

  case ${btnClick} in
    ${DIALOG_OK})
      inputRetVal="YES"
    ;;
    ${DIALOG_CANCEL})
      inputRetVal="NO"
    ;;
    ${DIALOG_ESC})
      inputRetVal="NO"
    ;;
  esac
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getChoiceWhenUnallocSpace
# DESCRIPTION: Get choice when unallocated space is found
#      RETURN: In the global variable "inputRetVal":
#              YES) run the linux partition tool again
#               NO) continue with the manual partition process
#  Required Params:
#      1)      dialogTitle - String to be displayed at the top of the dialog box.
#      2)    partitionTool - name of the linux partition tool that was previously used
#      3) unallocSpacePerc - percentage of unallocated space
#---------------------------------------------------------------------------------------
getChoiceWhenUnallocSpace() {
  local dialogTitle="$1"
  local partitionTool="$2"
  local unallocSpacePerc="$3"
  local textArray=(
  "There is \"$unallocSpacePerc\" of Unallocated Space."
  "------------------------------------------------------------------------------------------" " "
  "Click:"
  "        1) \"Ok\" to run the linux partition tool \"$partitionTool\" again"
  "                                         OR"
  "        2) \"Cancel\" to exit the Manual Partition Process"
  )
  local dialogText=$(getTextForDialog "${textArray[@]}")
  local -A yadOpts=(["buttons-layout"]="center" ["image"]="$YAD_GRAPHICAL_PATH/images/warning-icon.png"
    ["image-on-top"]=true ["title"]="$dialogTitle" ["window-icon"]="warning-window-icon.png"
    ["text"]="<span font_weight='bold' font_size='large'>WARNING!!!!!!!</span>" ["text-align"]="left"
    ["height"]=325 ["width"]=550 ["wrap"]=true ["fontname"]="Sans 12" ["center"]=true ["wrap"]=true ["size"]="fit")

  local dlgBtns=("Ok" "Cancel")

  showTextInfoDialog "$dialogText" yadOpts dlgBtns
  btnClick=$?

  case ${btnClick} in
    ${DIALOG_OK})
      inputRetVal="YES"
    ;;
    ${DIALOG_CANCEL})
      inputRetVal="NO"
    ;;
    ${DIALOG_ESC})
      inputRetVal="NO"
    ;;
  esac
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getPartitionLayoutType
# DESCRIPTION: Get the type of partition layout/scheme
#      RETURN: selected value in the global variable "mbSelVal"
#  Required Params:
#      1)   dialogTitle - title of the dialog/window
#      2) dialogTextHdr - Text inside the dialog displayed at the very top.
#      3)    pltChoices - array of strings for the choices of partition layout/scheme types
#  Optional Params:
#      4) flag to indicate if this is just for "LVM"
#---------------------------------------------------------------------------------------
getPartitionLayoutType() {
  local dialogTitle="$1"
  local -n pltChoices=$3
  local dialogText=""
  local dlgButtons=("Ok")
  local dlgColumns=("Radio Btn" "Option" "Type of Partition Layout/Scheme")
  local -A dlgOpts=(["height"]=325 ["width"]=600 ["window-icon"]="partition-tool.png")
  local pltDlgChoices=()
  local -A optDescs=()
  local dlgData=()
  local helpText=""
  local key=""
  local optNum=0
  local -A helpDlgOpts=(["width"]=675 ["title"]="Help:  $2")
  local defaultChoice="#1"

  if [ "$#" -gt 3 ]; then
    dialogText=$(getTextForSelPLT "$2" "$4")
    helpText=$(getHelpTextForSelPartLayout "$4")
    startPos=1
  else
    dialogText=$(getTextForSelPLT "$2")
    helpText=$(getHelpTextForSelPartLayout)
    dlgButtons+=("Cancel")
  fi

  for opt in "${pltChoices[@]}"; do
    optNum=$(expr ${optNum} + 1)
    key=$(printf "#%d" ${optNum})
    pltDlgChoices+=("$key")
    optDescs["$key"]="$opt"
    if [ "$opt" == "${varMap["PART-LAYOUT"]}" ]; then
      defaultChoice="$key"
    fi
  done

  appendOptionsForRadiolist "$dialogText" "$dialogTitle" dlgOpts
  setDataForMultiColList pltDlgChoices optDescs dlgData "$defaultChoice"

  if [ ${varMap["PART-LAYOUT"]+_} ]; then
    dlgButtons+=("Keep" "Help")
    helpDlgOpts["helpText"]="$helpText"
    key=$(getSelectedOptionFromNotebook dlgOpts dlgButtons dlgColumns dlgData helpDlgOpts)
  else
    dlgButtons+=("Help")
    key=$(getValueFromRadiolist "$helpText" dlgOpts dlgButtons dlgColumns dlgData helpDlgOpts)
  fi

  if [ ${#key} -gt 0 ]; then
    if [ ${optDescs["$key"]+_} ]; then
      mbSelVal="${optDescs[$key]}"
    else
      mbSelVal="$key"
    fi
  else
    mbSelVal=""
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     getTextForSelPLT
# DESCRIPTION: Text to display the radio list dialog box when selecting the
#              type of partition layout/scheme.
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getTextForSelPLT() {
  local textArray=("                                                              $1" " ")

  if [ ${varMap["PART-LAYOUT"]+_} ]; then
    textArray+=( "Current Partition Layout/Scheme:  [${varMap[PART-LAYOUT]}]" " ")
  fi

  textArray+=("Click the &quot;Help&quot; button for an explanation of the options."
  "-----------------------------------------------------------------------------------------------"
  " ")

  if [ "$#" -gt 1 ]; then
    textArray+=("Choose the &quot;Partition Layout/Scheme Type (PLT)&quot; for LVM:")
  else
    textArray+=("Choose the &quot;Partition Layout/Scheme Type (PLT)&quot;:")
  fi

  local dialogText=$(getTextForDialog "${textArray[@]}")
  echo "$dialogText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:               getSelectedOptionFromNotebook
# DESCRIPTION: Get the type of partition layout/scheme
#      RETURN: option selected
#  Required Params:
#      1) pltDlgOpts - array of dialog options
#      2) pltDlgBtns - array of buttons to display in the notebook
#      3) pltDlgCols - array of column headers
#      4) pltDlgData - array of data containing the menu options
#      5)   helpVals - array of values for the help dialog
#---------------------------------------------------------------------------------------
function getSelectedOptionFromNotebook() {
  local -n pltDlgOpts=$1
  local -n pltDlgBtns=$2
  local -n pltDlgCols=$3
  local -n pltDlgData=$4
  local -n helpVals=$5
  local -A defTabOpts=(["notebook"]=true)
  local -A noteBookOpts=()
  local dataFileNameFmt="${NB_TAB_DATA_FNS["nbCmd#01"]}"
  local numTabs=2
  local partitionLayout=$(getPartitionLayoutForYAD)
  local summaryTabData=()
  local rows=()
  local nbKeyPlug=$(getSixDigitRandomNum)
  local -A helpTabOpts=(["wrap"]=true ["fontname"]="Sans 12")
  local -A helpNbOpts=(["height"]=450 ["width"]=${helpVals["width"]} ["buttons-layout"]="center"
    ["title"]="${helpVals[title]}" ["window-icon"]="gtk-help.png" ["center"]=true)
  local helpText="${helpVals[helpText]}"
  local pos=$(findPositionForString pltDlgBtns "Cancel")
  local selectedValue=""

  setOptionsForTabAndNB pltDlgOpts defTabOpts noteBookOpts

  setSummaryOfCurLayout rows
  for row in "${rows[@]}"; do
    IFS=$";" read -a cols <<< "$row"
    summaryTabData+=("${cols[@]}")
  done

  while true; do
    showNotebookToSelectOption ${nbKeyPlug} "$partitionLayout" "$dataFileNameFmt" pltDlgData pltDlgCols summaryTabData defTabOpts noteBookOpts pltDlgBtns
    btnClick=$?

    case ${btnClick} in
      ${DIALOG_OK})
        selectedValue=$(getSelectedOptionsFromNotebook 2 "$dataFileNameFmt")
        break
      ;;
      ${DIALOG_CANCEL})
        selectedValue=""
        if [ ${pos} -gt -1 ]; then
          break
        fi
      ;;
      ${DIALOG_KEEP})
        selectedValue="<Keep>"
        break
      ;;
      ${DIALOG_HELP})
        showHelpAndPartTbl "$partitionLayout" "$helpText" helpTabOpts helpNbOpts
      ;;
    esac
  done

  echo "$selectedValue"
}

#---------------------------------------------------------------------------------------
#      METHOD:                   setOptionsForTabAndNB
# DESCRIPTION: Set the options for a "notebook" dialog and its tabs
#  Required Params:
#      1) srcOpts - array of "radio list" dialog options
#      2) tabOpts - array of options to append to for the tabs of a "notebook" dialog
#      3)  nbOpts - array of options to append to for a "notebook" dialog
#---------------------------------------------------------------------------------------
setOptionsForTabAndNB() {
  local -n srcOpts=$1
  local -n tabOpts=$2
  local -n nbOpts=$3

  for key in "${!srcOpts[@]}"; do
    case "$key" in
      "buttons-layout")
        nbOpts["$key"]="${srcOpts[$key]}"
      ;;
      "center")
        nbOpts["$key"]=${srcOpts[$key]}
      ;;
      "grid-lines")
        tabOpts["$key"]="${srcOpts[$key]}"
      ;;
      "height")
        nbOpts["$key"]=$(expr ${srcOpts[$key]} + 50)
      ;;
      "list-type")
        tabOpts["$key"]="${srcOpts[$key]}"
      ;;
      "text")
        tabOpts["$key"]="${srcOpts[$key]}"
      ;;
      "text-align")
        tabOpts["$key"]="${srcOpts[$key]}"
      ;;
      "title")
        nbOpts["$key"]="${srcOpts[$key]}"
      ;;
      "width")
        nbOpts["$key"]="${srcOpts[$key]}"
      ;;
      "window-icon")
        nbOpts["$key"]="${srcOpts[$key]}"
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                showNotebookToSelectOption
# DESCRIPTION: Displays the a yad "notebook" dialog with the following tabs:
#              #1) "Summary" - a summary of the logical volumes & partitions
#              #2)    "Menu" - "radio list" dialog/widget to select a
#                              Partition Layout/scheme Type (PLT)
#  Required Params:
#      1)       nbKeyPlug - integer value for the notebook key & plug of its tabs
#      2) partitionLayout - string containing header, logical volumes and partitions
#      3) dataFileNameFmt - format of the data file name that will hold the
#                           selected value
#      4)   pltTabChoices - array of values for selecting a Partition Layout/scheme Type (PLT)
#      5)      pltTabCols - array of column headers for the PLT menu
#      6)      sumTabData - array of data to display on the summary tab
#      7)         tabOpts - array of options for the tabs
#      8)          nbOpts - array of options for the notebook
#      0)       nbButtons - array of button names for the notebook
#---------------------------------------------------------------------------------------
showNotebookToSelectOption() {
  local nbKeyPlug="$1"
  local partitionLayout="$2"
  local tabOutput=$(echo " < \"%s\" &> $3 2> /dev/null &")
  local -n pltTabChoices=$4
  local -n pltTabCols=$5
  local -n sumTabData=$6
  local -n tabOpts=$7
  local -n nbOpts=$8
  local -n nbButtons=$9
  local pltTabText="${tabOpts[text]}"
  local fileName=""
  local tabColumns=()
  local nbCmd=$(echo "$DIALOG --notebook --key=${nbKeyPlug}")
  local tabCmd=""
  local tabName=""
  local numTabs=2

  for tabNum in $(eval echo "{1..${numTabs}}"); do
    fileName=$(printf "${NB_TAB_DATA_FNS[tabData]}" ${tabNum})
    case ${tabNum} in
      1)
        setDataForTab "$fileName" sumTabData
        local hdr=$(getHeaderForTable "$partitionLayout")
        unset tabOpts["list-type"]
        tabOpts["text"]="<span font_weight='bold' font_size='medium'>$hdr</span>"
        tabOpts["no-selection"]=true
        tabColumns=("Count" "Description")
        tabName="Summary"
      ;;
      2)
        setDataForTab "$fileName" pltTabChoices
        tabColumns=("${pltTabCols[@]}")
        unset tabOpts["no-selection"]
        tabOpts["text"]="$pltTabText"
        tabOpts["list-type"]="radiolist"
        tabName="PLT Menu"
      ;;
    esac
    tabCmd=$(echo "$DIALOG --plug=${nbKeyPlug} --tabnum=${tabNum}")
    tabCmd+=$(getOptionsForListDialog tabOpts)
    tabCmd+=$(getColumnsForListDialog tabColumns)
    tabCmd+=$(printf "$tabOutput" "$fileName" ${tabNum})
    eval "$tabCmd"
    nbCmd+=$(printf " --tab=\"%s\"" "$tabName")
  done

  nbCmd+=$(getOptionsForNotebook nbOpts)
  nbCmd+=$(getButtonsForDialog nbButtons)

  eval "$nbCmd"
}

#---------------------------------------------------------------------------------------
#      METHOD:                       setDataForTab
# DESCRIPTION: Set the data to be displayed for a tab of a "notebook" dialog
#  Required Params:
#      1) dataFileName - name of the file where the data for a tab will reside
#      2)      tabData - array of values to be displayed in the tab
#---------------------------------------------------------------------------------------
setDataForTab() {
  local dataFileName="$1"
  local -n tabData=$2
  local lines=$(printf "\n%s" "${tabData[@]}")
  lines=${lines:1}
  echo -e "$lines" > "$dataFileName"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:               getConfirmationToEncrypt
# DESCRIPTION: Get confirmation of whether or not to continue with LUKS encryption
#      RETURN: value selected in the global variable "inputRetVal"
#  Required Params:
#      1) dialogTextHdr - Text inside the dialog displayed at the very top.
#      2)   dialogTitle - String to be displayed at the top of the dialog box.
#      3)      helpText - Text to display on the help/more dialog
#---------------------------------------------------------------------------------------
getConfirmationToEncrypt() {
  local dialogTextHdr="$1"
  local dialogTitle="$2"
  local helpText="$3"
  local dialogText=$(getTextForConfirmation "$dialogTextHdr")
  local warnTitle=$(getTextForWarning)
  local warnTxtArray=("$warnTitle" "If you are NOT going to be able to remember the Password, then write it down and"
    "store it in a safe place!"
  )
  local warnText=$(getTextForDialog "${warnTxtArray[@]}")
  local -A yadOpts=(["buttons-layout"]="center" ["image"]="$YAD_GRAPHICAL_PATH/images/warning-icon-sm.png"
      ["image-on-top"]=true ["title"]="$dialogTitle" ["window-icon"]="luks-icon.png"
      ["text"]="<span font_weight='bold' font_size='large'>$warnText</span>" ["text-align"]="left"
      ["height"]=400 ["width"]=775 ["fontname"]="Sans 12" ["center"]=true ["wrap"]=true ["size"]="fit")
  local partitionLayout=$(getPartitionLayoutForYAD)
  local dlgBtns=("Confirm" "Cancel" "Help")
  local -A helpTabOpts=(["wrap"]=true ["fontname"]="Sans 12")
  local -A helpNbOpts=(["height"]=450 ["width"]=710 ["buttons-layout"]="center" ["title"]="Help:  $dialogTextHdr"
      ["window-icon"]="gtk-help.png" ["center"]=true)

  while true; do
    showTextInfoDialog "$dialogText" yadOpts dlgBtns
    btnClick=$?

    case ${btnClick} in
      ${DIALOG_OK})
        inputRetVal="YES"
        break
      ;;
      ${DIALOG_CANCEL})
        inputRetVal="NO"
        break
      ;;
      ${DIALOG_ESC})
        inputRetVal="NO"
        break
      ;;
      ${DIALOG_HELP})
        showHelpAndPartTbl "$partitionLayout" "$helpText" helpTabOpts helpNbOpts
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                getTextForConfirmation
# DESCRIPTION: Get the text to display in a linux "dialog" command for the
#              confirmation of the LUKS encrypted container
#      RETURN: concatenated string
#  Required Params:
#      1) dialogTextHdr - Text inside the dialog displayed at the very top.
#---------------------------------------------------------------------------------------
function getTextForConfirmation() {
  local pvDevice=$(getDeviceNameForPV)
  local textArray=(
  "                                   Partition Layout/Scheme Type:  \"$1\"" " "
  "\"LUKS\" (Linux Unified Key Setup) is the preferred way to setup disk encryption with \"dm-crypt\"."
  "$BORDER_WIDTH_MAX" " "
  "\"Device-Mapper Crypt\" (dm-crypt) - The installer will use the straight-forward approach by:"
  "    1) first encrypting the device \"$pvDevice\" with \"LUKS\""
  "    2) creating the logical volumes on top of the encrypted device"
  "$BORDER_WIDTH_MAX" " "
  "Name of the LUKS encrypted container:  $LUKS_CONTAINER_NAME"
  "Password to encrypt & open container:  ${varMap[LVM-on-LUKS]}"
  )
  local dialogText=$(getTextForDialog "${textArray[@]}")

  echo "$dialogText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getSelPswdOptForLUKS
# DESCRIPTION: Get the option to create a password for the LUKS encrypted container
#      RETURN: option of how to create a password in the global variable "mbSelVal"
#  Required Params:
#      1) dialogTextHdr - Text inside the dialog displayed at the very top.
#      2)   dialogTitle - String to be displayed at the top of the dialog box.
#      3)      helpText - Text to display on the help/more dialog
#      4)   dlgPswdOpts - array of options to choose from
#      5)   dlgOptDescs - associative array of key/value pairs for the
#                           description of each option
#---------------------------------------------------------------------------------------
getSelPswdOptForLUKS() {
  local dialogTextHdr="$1"
  local dialogTitle="$2"
  local helpText="$3"
  local -n dlgPswdOpts=$4
  local -n dlgOptDescs=$5
  local dialogText=$(getDialogTextForLUKS "$dialogTextHdr")
  local dlgButtons=("Ok" "Cancel" "Help")
  local dlgColumns=("Radio Btn" "Option" "Description")
  local -A dlgOpts=(["height"]=370 ["image"]="$YAD_GRAPHICAL_PATH/images/luks-logo.png"
  ["image-on-top"]=true ["window-icon"]="password-icon.png")
  local lastIdx=$(expr ${#pptChoices[@]} - 1)
  local -A pptDescs=()
  local dlgData=()
  local -A helpDlgOpts=(["width"]=675 ["title"]="Help:  Password Create Option")

  appendOptionsForRadiolist "$dialogText" "$dialogTitle" dlgOpts
  setDataForMultiColList dlgPswdOpts dlgOptDescs dlgData

  mbSelVal=$(getValueFromRadiolist "$helpText" dlgOpts dlgButtons dlgColumns dlgData helpDlgOpts)
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getDialogTextForLUKS
# DESCRIPTION: Get the text to display in a yad "dialog" command when selecting
#              the option of creating a password for the LUKS encrypted container
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getDialogTextForLUKS() {
  local textArray=(
  "                                         $1" " "
  "&quot;LUKS&quot; (Linux Unified Key Setup) is the preferred way to"
  "setup disk encryption with &quot;dm-crypt&quot;."
  "------------------------------------------------------------------"
  "Click the &quot;Help&quot; button for password suggestions &amp;"
  "description of options."
  "------------------------------------------------------------------"
  "Choose the option to create the password to encrypt &amp; open"
  "the LUKS encrypted container:"
  )
  local dialogText=$(getTextForDialog "${textArray[@]}")
  echo "$dialogText"
}

#---------------------------------------------------------------------------------------
#      METHOD:                 showProgressBarForDataFile
# DESCRIPTION: Display a yad "progress indication" dialog (i.e. progress bar) while
#              executing the methods to create the data file for the partition table.
#  Required Params:
#      1)   pbLogText - String to be displayed at the top of the logging area.
#      2) dialogTitle - String to be displayed at the top of the dialog.
#---------------------------------------------------------------------------------------
showProgressBarForDataFile() {
  local methodNum=1
  local pbPerc=0
  local methodName=""
  local pbTitle=""
  local totalMethods=${#pbKeys[@]}

  (
    sleep 0.5
    for pbKey in "${pbKeys[@]}"; do
      pbTitle="${pbTitles["$pbKey"]}"
      if [ ${pbPerc} -lt 100 ]; then
        echo ${pbPerc}
      fi

      echo "# $pbTitle"

      case "$pbKey" in
        "done")
          sleep 0.5
        ;;
        *)
          methodName="${methodNames[$pbKey]}"
          clog_info "Executing method [$methodName]" >> "$INSTALLER_LOG"
          ${methodName}
          clog_info "Method [$methodName] is DONE" >> "$INSTALLER_LOG"
          methodNum=$(expr $methodNum + 1)
          pbPerc=$(printf "%.0f" $(echo "scale=3; $methodNum / $totalMethods * 100" | bc))
        ;;
      esac

      sleep 0.5
    done
  ) | $DIALOG --progress --pulsate --center --percentage=0 --log-expanded --enable-log="$1 Log" \
              --log-height=175 --width=800 --height=570 --title="$2" --size=fit \
              --text-align=center --no-buttons --auto-close --window-icon="$ICONS_PATH/app-icon.png" \
              --image="$YAD_GRAPHICAL_PATH/images/kaalp-gygi.png" --image-on-top 2> /dev/null
}
