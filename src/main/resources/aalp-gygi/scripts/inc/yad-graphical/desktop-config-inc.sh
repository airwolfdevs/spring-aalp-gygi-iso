#!/bin/bash

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# globals
#===============================================================================
source "$GRAPHICAL_PATH/yad-nb-inc.sh"

#===============================================================================
# functions/methods
#===============================================================================

#---------------------------------------------------------------------------------------
#      METHOD:                   showConfirmationDialog
# DESCRIPTION: Show a yad "list" dialog to get confirmation to install the AUR packages
#              that are listed.
#  Required Params:
#      1) paramAssocArray - the name of the associative array with the required
#                           key/value pairs:
#      "dialogTitle"   - String to be displayed at the top of the dialog window.
#      "dialogText"    - Text to display inside the dialog before the list of menu options
#      "dialogTextHdr" - String to be displayed at the top of the text area.
#      "helpText"      - Text to display on the help/more dialog
#      "menuOptsRef"   - name of the array containing the menu options
#      "optDescsRef"   - name of the associative array containing the description
#                        of the menu options
#---------------------------------------------------------------------------------------
showConfirmationDialog() {
  local -n paramAssocArray="$1"
  local buttons=("Confirm" "Skip" "Help")
  local cols=("AUR Package" "Description")

  paramAssocArray["wrap-cols"]=2
  paramAssocArray["wrap-width"]=400

  if [ ${paramAssocArray["width"]+_} ]; then
    paramAssocArray["width"]=${paramAssocArray["width"]}
  else
    paramAssocArray["width"]=610
  fi

  if [ ${paramAssocArray["buttons"]+_} ]; then
    readarray -t buttons <<< "${paramAssocArray["buttons"]//$FORM_FLD_SEP/$'\n'}"
  fi

  if [ ${paramAssocArray["cols"]+_} ]; then
    readarray -t cols <<< "${paramAssocArray["cols"]//$FORM_FLD_SEP/$'\n'}"
  fi

  showMenuForConfirmation "$1" "buttons" "cols"
}

#---------------------------------------------------------------------------------------
#      METHOD:                    setCmdsForVideoCardsNB
# DESCRIPTION: Set the commands to display a yad "notebook" dialog with the following
#              tabs:
#       Notebook A:
#              Tab #1)  "Shell Types" - default radio button selected is
#                                       "POSIX compliant shells"
#                  #2) "POSIX compliant" - POSIX compliant shells
#       Notebook B:
#              Tab #1)  "Shell Types" - default radio button selected is
#                                       "Alternative shells"
#                  #2)  "Alternative" - Alternative shells
#  Required Params:
#      1) nbAssocArray - the name of the associative array with the required
#                        key/value pairs:
#      "windowTitle"    - String to be displayed at the top of the dialog window.
#      "SHELL_TYPES[@]" - the name of array containing menu options to display
#---------------------------------------------------------------------------------------
setCmdsForVideoCardsNB() {
  local -n nbAssocArray="$1"
  local totNumTabs=$2
  local cols=()
  local -A assocArray=()

  assocArray["nbKeyPlug"]=$(getSixDigitRandomNum)
  assocArray["numTabs"]=2
  assocArray["windowTitle"]="${nbAssocArray["windowTitle"]}"
  assocArray["nbImage"]="$YAD_GRAPHICAL_PATH/images/post-inst/set-default-shell.png"

  for wmType in "${SHELL_TYPES[@]}"; do
    local tabName=""
    local defChoice=""
    local tabText=""

    case "$wmType" in
      "${SHELL_TYPES[0]}")
        tabName="POSIX compliant"
        defChoice="Bash"
        tabText="Select the POSIX compliant shell:"
      ;;
      *)
        tabName="Alternative"
        defChoice="PowerShell"
        tabText="Select the Alternative shell:"
      ;;
    esac

    for tabNum in $(eval echo "{1..${totNumTabs}}"); do
      local tabNameKey=$(printf "tabName%02d" ${tabNum})
      local defChoiceKey=$(printf "defChoiceTab%02d" ${tabNum})
      local menuOptRefKey=$(printf "menuOptsTab%02d" ${tabNum})
      local optDescsRefKey=$(printf "optDescsTab%02d" ${tabNum})
      local tabTextKey=$(printf "textTab%02d" ${tabNum})
      local listTypeKey=$(printf "listType%02d" ${tabNum})
      local colsTabKey=$(printf "colsTab%02d" ${tabNum})
      local concatStr=""

      case ${tabNum} in
        1)
          assocArray["$tabNameKey"]="Shell Types"
          assocArray["$defChoiceKey"]="$wmType"
          assocArray["$menuOptRefKey"]="SHELL_TYPES"
          assocArray["$optDescsRefKey"]="DESC_ASSOC_ARRAY"
          assocArray["$tabTextKey"]="Choose the type of Command-line shell to install:"
          assocArray["$listTypeKey"]="radiolist"
          cols=("Radio Btn" "Shell Type" "Name")
          concatStr=$(printf "$FORM_FLD_SEP%s" "${cols[@]}")
          concatStr=${concatStr:${#FORM_FLD_SEP}}
        ;;
        2)
          assocArray["$tabNameKey"]="$tabName"
          assocArray["$defChoiceKey"]="$defChoice"
          assocArray["$menuOptRefKey"]="${nbAssocArray["$wmType"]}"
          assocArray["$optDescsRefKey"]="AUR_PCKG_DESCS"
          assocArray["$tabTextKey"]="$tabText"
          assocArray["$listTypeKey"]="radiolist"
          cols=("Radio Btn" "Shell" "Description")
          concatStr=$(printf "$FORM_FLD_SEP%s" "${cols[@]}")
          concatStr=${concatStr:${#FORM_FLD_SEP}}
        ;;
      esac

      assocArray["$colsTabKey"]="$concatStr"
    done

    notebookCmds["$wmType"]=$(getCmdToShowPostInstNB "assocArray")
    for tabNum in $(eval echo "{1..${totNumTabs}}"); do
      local srcFile=$(printf "${NB_TAB_DATA_FNS["tabData"]}" "$tabNum")
      local destFile=$(printf "/tmp/%s-%s" "$wmType" "${srcFile:5}")
      mv "$srcFile" "$destFile"
    done
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                       dispStepInitPB
# DESCRIPTION: Display a yad "progress indication" dialog (i.e. progress bar) while
#              executing the methods to initialize the data for a step/task.
#  Required Params:
#      1) pbLogText - String to be displayed at the top of the logging area.
#      2) pbTitle - String to be displayed at the top of the dialog.
#---------------------------------------------------------------------------------------
dispStepInitPB() {
  local methodNum=1
  local pbPerc=0
  local methodName=""
  local pbTitle=""
  local totalMethods=${#pbKeys[@]}
  local continueFlag=$(echo 'true' && return 0)

  (
    sleep 0.5
    for pbKey in "${pbKeys[@]}"; do
      pbTitle="${pbTitles["$pbKey"]}"
      if [ ${pbPerc} -lt 100 ]; then
        echo ${pbPerc}
      fi
      methodName="${methodNames[$pbKey]}"

      echo "# $pbTitle"

      callPostInstMethod
      if ! ${continueFlag}; then
        break
      fi

      sleep 0.5
    done
  ) | $DIALOG --progress --pulsate --center --percentage=0 --log-expanded --enable-log="$1 Log" \
              --log-height=175 --width=800 --height=570 --title="$2" --size=fit \
              --text-align=center --no-buttons --auto-close --window-icon="$ICONS_PATH/app-icon.png" \
              --image="$YAD_GRAPHICAL_PATH/images/kaalp-gygi.png" --image-on-top 2> /dev/null
}

#---------------------------------------------------------------------------------------
#      METHOD:                   showDesktopConfWarning
# DESCRIPTION: Get confirmation to of whether or not to continue with the user's choice.
#              Some examples are:
#                 > use the "AMD Catalyst" type Driver & OpenGL packages
#                 > skipping installation of Xorg
#      1) paramAssocArray - the name of the associative array with the required
#                           key/value pairs:
#      "dialogTitle"   - String to be displayed at the top of the dialog window.
#      "dialogText"    - Warning message to be displayed in the text area of the dialog
#      "dialogTextHdr" - String to be displayed at the top of the text area.
#---------------------------------------------------------------------------------------
showDesktopConfWarning() {
  local -n paramAssocArray="$1"
  local hdr="${paramAssocArray["dialogTextHdr"]}"
  local -A yadOpts=(["buttons-layout"]="center" ["image"]="$YAD_GRAPHICAL_PATH/images/warning-icon.png"
    ["image-on-top"]=true ["title"]="${paramAssocArray["dialogTitle"]}" ["window-icon"]="warning-window-icon.png"
    ["text"]="<span font_weight='bold' font_size='large'>WARNING!!!!!!!\n\n\n${hdr}</span>" ["text-align"]="left"
    ["height"]=600 ["width"]=700 ["wrap"]=true ["fontname"]="Sans 12" ["center"]=true ["wrap"]=true ["size"]="fit")

  local dlgBtns=("Continue" "Cancel")

  showTextInfoDialog "${paramAssocArray["dialogText"]}" yadOpts dlgBtns
  btnClick=$?

  case ${btnClick} in
    ${DIALOG_OK})
      yesNoFlag=$(echo 'true' && return 0)
    ;;
    ${DIALOG_CANCEL}|${DIALOG_ESC})
      yesNoFlag=$(echo 'false' && return 1)
    ;;
  esac
}

#---------------------------------------------------------------------------------------
#      METHOD:                  selectFromRadioListDialog 
# DESCRIPTION: Show a YAD radiolist dialog and get the value that was selected.  Set
#              the value selected within the global variable "mbSelVal"
#  Required Params:
#      1) paramAssocArray - the name of the associative array with the required
#                           key/value pairs (see "showRadioListDialog" method)
#---------------------------------------------------------------------------------------
selectFromRadioListDialog() {
  showRadioListDialog "$1"
  if [ ${#mbSelVal} -gt 0 ]; then
    local selVals=()
    local sep="|"
    readarray -t selVals <<< "${mbSelVal//$sep/$'\n'}"
    mbSelVal="${selVals[1]}"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                     showRadioListDialog
# DESCRIPTION: This is a wrapper for calling a yad "radiolist" dialog to display the
#              options available for a specific task using the default properties
#              (i.e. height, width, buttons, etc.)
#              Sets the value selected within the global variable "mbSelVal".
#  Required Params:
#      1) paramAssocArray - the name of the associative array with the required
#                           key/value pairs:
#      "columnNames"   - column header names.
#      "cancelLabel"   - Skip or Cancel.
#      "dialogTitle"   - String to be displayed at the top of the dialog window.
#      "dialogTextHdr" - String to be displayed at the top of the text area.
#      "helpText"      - Text to display on the help/more dialog
#      "menuOptsRef"   - name of the array containing the menu options
#      "optDescsRef"   - name of the associative array containing the description
#                        of the menu options
#   Optional Param:
#      "dialogText"    - Text to display inside the dialog before the list of menu options
#---------------------------------------------------------------------------------------
showRadioListDialog() {
  local -n paramAssocArray="$1"
  local buttons=("Ok" "${paramAssocArray["cancelLabel"]}" "Help")
  local cols=()
  local propKeys=("helpHeight" "helpWidth" "height" "width" "wrap-cols" "wrap-width")
  local dialogText=""
  local prop="dialogText"

  readarray -t cols <<< "${paramAssocArray["columnNames"]//$FORM_FLD_SEP/$'\n'}"

  if [ ${paramAssocArray["$prop"]+_} ]; then
    dialogText="${paramAssocArray["$prop"]}"
  fi

  for prop in "${propKeys[@]}"; do
    if [ ! ${paramAssocArray["$prop"]+_} ]; then
      case "$prop" in
        "height")
          paramAssocArray["$prop"]=525
        ;;
        "helpHeight")
          paramAssocArray["$prop"]=400
        ;;
        "helpWidth")
          paramAssocArray["$prop"]=650
        ;;
        "width")
          paramAssocArray["$prop"]=610
        ;;
        "wrap-cols")
          paramAssocArray["$prop"]=3
        ;;
        "wrap-width")
          paramAssocArray["$prop"]=400
        ;;
      esac
    fi
  done

  selectOptionFromRadioList "$1" "buttons" "cols" ""
}

#---------------------------------------------------------------------------------------
#      METHOD:                      showWinManDialog
# DESCRIPTION: Select the type of Window Manger and the name of the WM to install
#              using a yad "notebook" dialog. Set the values within the global
#              array variable "TAB_SEL_VALS".
#  Required Params:
#      1) paramAssocArray - the name of the associative array with the required
#                           key/value pairs:
#      "dialogTitle"   - String to be displayed at the top of the dialog window.
#      "helpText"      - Text to display on the help/more dialog
#      "menuOptsRef"   - name of the array containing the menu options
#      "optDescsRef"   - name of the associative array containing the description
#                        of the menu options
#---------------------------------------------------------------------------------------
showWinManDialog() {
  local -n paramAssocArray="$1"
  local -A notebookCmds=()
  local -A helpOpts=(["buttons-layout"]="center" ["window-icon"]="gtk-help.png"
    ["title"]="Help:  Choose a standalone Window Manager" ["center"]=true
    ["height"]=400 ["width"]=650 ["wrap"]=true ["fontname"]="Sans 12")

  setCmdsForDefWinManNB "$1"

  local nbKey="${WINDOW_MANAGERS[1]}"
  local totNumTabs=2

  setDataFilesForTabs ${totNumTabs} "$nbKey"

  local tabCols=()
  local sep="|"
  while true; do
    local nbCommand="${notebookCmds["$nbKey"]}"
    clickedVals=$(showNotebookDialog "$nbCommand" " ")
    IFS=$',' read -d '' -a btnVals <<< "$clickedVals"
    case ${btnVals[0]} in
      ${DIALOG_OK})
        for tabNum in $(eval echo "{1..${totNumTabs}}"); do
          setSelectedTabOptionsFromNotebook $tabNum "${NB_TAB_DATA_FNS["nbCmd#01"]}"
        done
        readarray -t tabCols <<< "${TAB_SEL_VALS["tab#01"]//$sep/$'\n'}"
        if [ "${tabCols[1]}" == "$nbKey" ]; then
          break
        else
          nbKey="${tabCols[1]}"
          setDataFilesForTabs ${totNumTabs} "$nbKey"
        fi
      ;;
      ${DIALOG_HELP})
        showTextInfoDialog "${paramAssocArray["helpText"]}" helpOpts
      ;;
      *)
        TAB_SEL_VALS=()
        break
      ;;
    esac
  done

  cleanDataFilesForTabs "WINDOW_MANAGERS"
}

#---------------------------------------------------------------------------------------
#      METHOD:                    setCmdsForDefWinManNB
# DESCRIPTION: Set the commands to display a yad "notebook" dialog with the following
#              tabs:
#       Notebook A:
#              Tab #1) "WM Types" - radio list of window manager types
#                  #2) "[window manager type]" - list of window managers based on the
#                                                type selected in Tab #)
#  Required Params:
#      1) nbAssocArray - the name of the associative array with the required
#                        key/value pairs:
#      "windowTitle"    - String to be displayed at the top of the dialog window.
#      "WINDOW_MANAGERS" - the name of array containing menu options to display
#---------------------------------------------------------------------------------------
setCmdsForDefWinManNB() {
  local -n nbAssocArray="$1"
  local totNumTabs=2
  local cols=()
  local -A assocArray=()

  assocArray["nbKeyPlug"]=$(getSixDigitRandomNum)
  assocArray["numTabs"]=${totNumTabs}
  assocArray["windowTitle"]="${nbAssocArray["dialogTitle"]}"
  assocArray["nbImage"]="${nbAssocArray["image"]}"
  assocArray["nbButtons"]="Ok${FORM_FLD_SEP}Cancel${FORM_FLD_SEP}Help"

  if [ ${nbAssocArray["height"]+_} ]; then
    assocArray["height"]=${nbAssocArray["height"]}
  fi

  for wmType in "${WINDOW_MANAGERS[@]}"; do
    local tabName=""
    local defChoice=""
    local tabText=""

    case "$wmType" in
      "${WINDOW_MANAGERS[0]}")
        tabName=$(echo "${WINDOW_MANAGERS[0]} WMs")
        defChoice="dwm"
        tabText=$(echo "Select a standalone ${WINDOW_MANAGERS[0]} window manager:")
      ;;
      "${WINDOW_MANAGERS[1]}")
        tabName=$(echo "${WINDOW_MANAGERS[1]} WMs")
        defChoice="Openbox"
        tabText=$(echo "Select a standalone ${WINDOW_MANAGERS[1]} window manager:")
      ;;
      *)
        tabName=$(echo "${WINDOW_MANAGERS[2]} WMs")
        defChoice="i3"
        tabText=$(echo "Select a standalone ${WINDOW_MANAGERS[2]} window manager:")
      ;;
    esac

    for tabNum in $(eval echo "{1..${totNumTabs}}"); do
      local tabNameKey=$(printf "tabName%02d" ${tabNum})
      local defChoiceKey=$(printf "defChoiceTab%02d" ${tabNum})
      local menuOptRefKey=$(printf "menuOptsTab%02d" ${tabNum})
      local optDescsRefKey=$(printf "optDescsTab%02d" ${tabNum})
      local tabTextKey=$(printf "textTab%02d" ${tabNum})
      local listTypeKey=$(printf "listType%02d" ${tabNum})
      local colsTabKey=$(printf "colsTab%02d" ${tabNum})
      local concatStr=""

      case ${tabNum} in
        1)
          assocArray["$tabNameKey"]="Window Manager Types"
          assocArray["$defChoiceKey"]="$wmType"
          assocArray["$menuOptRefKey"]="WINDOW_MANAGERS"
          assocArray["$optDescsRefKey"]="${nbAssocArray["optDescsRef"]}"
          assocArray["$tabTextKey"]="Choose the type of Window Manager to install:"
          assocArray["$listTypeKey"]="radiolist"
          cols=("Radio Btn" "WM Type" "Name")
          concatStr=$(printf "$FORM_FLD_SEP%s" "${cols[@]}")
          concatStr=${concatStr:${#FORM_FLD_SEP}}
        ;;
        2)
          assocArray["$tabNameKey"]="$tabName"
          assocArray["$defChoiceKey"]="$defChoice"
          assocArray["$menuOptRefKey"]="${nbAssocArray["$wmType"]}"
          assocArray["$optDescsRefKey"]="${nbAssocArray["optDescsRef"]}"
          assocArray["$tabTextKey"]="$tabText"
          assocArray["$listTypeKey"]="radiolist"
          cols=("Radio Btn" "Win. Man." "Description")
          concatStr=$(printf "$FORM_FLD_SEP%s" "${cols[@]}")
          concatStr=${concatStr:${#FORM_FLD_SEP}}
        ;;
      esac

      assocArray["$colsTabKey"]="$concatStr"
    done

    notebookCmds["$wmType"]=$(getCmdToShowPostInstNB "assocArray")
    for tabNum in $(eval echo "{1..${totNumTabs}}"); do
      local srcFile=$(printf "${NB_TAB_DATA_FNS["tabData"]}" "$tabNum")
      local destFile=$(printf "/tmp/%s-%s" "$wmType" "${srcFile:5}")
      mv "$srcFile" "$destFile"
    done
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                     showCheckListDialog
# DESCRIPTION: This is a wrapper for calling a yad "checklist" dialog to display the
#              options available for a specific task using the default properties
#              (i.e. height, width, buttons, etc.)
#              Sets the value selected within the global variable "mbSelVal".
#  Required Params:
#      1) paArray - the name of the associative array with the required
#                           key/value pairs:
#      "columnNames"   - column header names.
#      "cancelLabel"   - Skip or Cancel.
#      "dialogTitle"   - String to be displayed at the top of the dialog window.
#      "dialogTextHdr" - String to be displayed at the top of the text area.
#      "helpText"      - Text to display on the help/more dialog
#      "menuOptsRef"   - name of the array containing the menu options
#      "optDescsRef"   - name of the associative array containing the description
#                        of the menu options
#   Optional Param:
#      "dialogText"    - Text to display inside the dialog before the list of menu options
#---------------------------------------------------------------------------------------
showCheckListDialog() {
  local -n paArray="$1"

  paArray["list-type"]="checklist"
  if [ "${paArray["menuOptsRef"]}" == "KDE_THEMES" ]; then
    paArray["height"]=570
    paArray["width"]=800
    paArray["wrap-cols"]="2,3"
    paArray["wrap-width"]=225
  fi
  showRadioListDialog "paArray"
}
