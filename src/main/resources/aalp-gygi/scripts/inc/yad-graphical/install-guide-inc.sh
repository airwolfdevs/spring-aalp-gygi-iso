#!/bin/bash
#set -e

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================
source "$GRAPHICAL_PATH/yad-nb-inc.sh"

#===============================================================================
# globals
#===============================================================================
declare INST_GUIDE_FILE_NAME="$BASE_DIR/data/install-guide.txt"

#===============================================================================
# functions/methods
#===============================================================================

#---------------------------------------------------------------------------------------
#      METHOD:                     dispWelcome
# DESCRIPTION: Displays the "Welcome" linux "dialog" box of type message box
#---------------------------------------------------------------------------------------
dispWelcome() {
  local dialogTitle="Airwolf Arch Linux Pseudo-Graphical & YAD Graphical Installer"
  local dlgBtns=("Ok")
  local -A yadOpts=(["buttons-layout"]="center" ["image"]="$YAD_GRAPHICAL_PATH/images/kaalp-gygi.png"
    ["image-on-top"]=true ["title"]="$dialogTitle" ["window-icon"]="app-icon.png"
    ["center"]=true ["wrap"]=true ["width"]=800 ["size"]="fit" ["textInfoFN"]="$GRAPHICAL_PATH/welcome.txt"
  )

  showTextInfoDialog " " yadOpts dlgBtns
  dialogCmdRetVal=$?
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getInstallationSteps
# DESCRIPTION: Get the installation steps to execute/run
#      RETURN: step #s in the global "mbSelVal" variable
#---------------------------------------------------------------------------------------
getInstallationSteps() {
  local btnVals=()
  local clickedVals=""
  local dialogTitle="AALP-GYGI Installation Guide Menu"
  local numInstSteps=$(expr ${#stepTotals[@]} - 1)
  local dataFileNameFmt="${NB_TAB_DATA_FNS["nbCmd#01"]}"
  local helpText=$(getHelpTextForInstGuide)
  local -A helpOpts=(["buttons-layout"]="center" ["window-icon"]="gtk-help.png" ["title"]="$dialogTitle"
     ["center"]=true ["height"]=400 ["width"]=610 ["wrap"]=true ["fontname"]="Sans 12")
  local nbKeyPlug=$(getSixDigitRandomNum)
  local notebookCmds=()
  local partLayoutCmd=""

  if [ ${INST_GUIDE_CHECKLIST[0]} -gt 0 ]; then
    partLayoutCmd=$(getCmdForPartLayoutNotebook)
  fi

  setInstallationGuideChecklistCmd ${nbKeyPlug} "$dialogTitle" ${numInstSteps}
  local nbCommand=$(printf " %s" "${notebookCmds[@]}")
  nbCommand=${nbCommand:1}

  while true; do
    clickedVals=$(showNotebookDialog "$nbCommand" " ")
    IFS=$',' read -d '' -a btnVals <<< "$clickedVals"
    case ${btnVals[0]} in
      ${DIALOG_OK})
        mbSelVal=$(getSelectedOptionsFromNotebook ${numInstSteps} "$dataFileNameFmt")
        break
      ;;
      ${DIALOG_LIST})
        eval "$partLayoutCmd"
      ;;
      ${DIALOG_HELP})
        showTextInfoDialog "$helpText" helpOpts
      ;;
      *)
        mbSelVal=""
        break
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:              setInstallationGuideChecklistCmd
# DESCRIPTION: Displays the installation guide within a yad "notebook" dialog where each
#              tab represents an installation/main step.
#  Required Params:
#      1)       nbKeyPlug - integer value for the notebook key & plug of its tabs
#      2)     dialogTitle - String to be displayed at the top of the dialog box.
#      3)         numTabs - total number of tabs to create
#      4) dataFileNameFmt - format of the data file name that will hold the
#                           selected value
#---------------------------------------------------------------------------------------
setInstallationGuideChecklistCmd() {
  local nbKeyPlug="$1"
  local dialogTitle="$2"
  local numTabs=$3
  local tabOutput=$(echo " < \"%s\" &> \"%s\" 2> /dev/null &")
  local tabColumns=("Check Box" "Step" "Description" "Value Of Step")
  local nbButtons=("Ok" "Exit" "Help")
  local instStep=""
  local tabCmd=""
  local tabName=""
  local tabDataFN=""
  local outputFN=""
  local prevStepNum=0
  local doneFlag=$(echo 'true' && return 0)
  local nbCmd=$(echo "$DIALOG --notebook --key=${nbKeyPlug}")
  local activeTabNum=1
  local -A tabOpts=(["grid-lines"]="both" ["list-type"]="checklist" ["notebook"]=true)
  local -A nbOpts=(["height"]=570 ["width"]=800  ["center"]=true
  ["buttons-layout"]="center" ["image"]="$YAD_GRAPHICAL_PATH/images/install-guide.png" ["image-on-top"]=true
  ["text"]="<span font_weight='bold' font_size='large'>Select the steps you want to execute/run:</span>"
  ["text-align"]="left" ["title"]="$dialogTitle" ["window-icon"]="checklist.png")

  setDataForTabsOfEachStep
  for tabNum in $(eval echo "{1..${numTabs}}"); do
    prevStepNum=$(expr ${tabNum} - 1)

    if [ ${prevStepNum} -gt 0 ]; then
      doneFlag=$(isInstStepDone ${prevStepNum})
      if ${doneFlag}; then
        activeTabNum=${tabNum}
        if [ ${prevStepNum} -lt 2 ]; then
          nbButtons=("Ok" "View Partition Layout" "Exit" "Help")
        fi
      fi
    fi

    if ! ${doneFlag}; then
      if [ ! ${tabOpts["hide-column"]+_} ] || [ ${tabNum} -gt 3 ]; then
        tabColumns=("${tabColumns[@]:1}")
        tabOpts["hide-column"]=3
        tabOpts["no-selection"]=true
        unset tabOpts["list-type"]
      fi
    elif [ ${tabNum} -gt 3 ] && [ ! ${tabOpts["hide-column"]+_} ]; then
      tabOpts["hide-column"]=4
    fi

    instStep=$(printf "step#%d" "$tabNum")
    tabDataFN=$(printf "${NB_TAB_DATA_FNS["tabData"]}" "$tabNum")
    outputFN=$(printf "${NB_TAB_DATA_FNS["nbCmd#01"]}" "$tabNum")

    tabCmd=$(echo "$DIALOG --plug=${nbKeyPlug} --tabnum=${tabNum}")
    tabCmd+=$(getOptionsForListDialog tabOpts)
    tabCmd+=$(getColumnsForListDialog tabColumns)
    tabCmd+=$(printf "$tabOutput" "$tabDataFN" "$outputFN")
    notebookCmds+=("$tabCmd")
    nbCmd+=$(printf " --tab=\"%s\"" "${checkListSteps[$instStep]}")
  done

  nbCmd+=$(printf " --active-tab=%d" ${activeTabNum})
  nbCmd+=$(getOptionsForNotebook nbOpts)
  nbCmd+=$(getButtonsForDialog nbButtons)
  notebookCmds+=("$nbCmd")
}

#---------------------------------------------------------------------------------------
#      METHOD:                  setDataForTabsOfEachStep
# DESCRIPTION: Set the choices of the steps and tasks within a file for the tab that will
#              display the data within either a yad "list" or "checklist" dialog
#---------------------------------------------------------------------------------------
setDataForTabsOfEachStep() {
  local installSteps=()
  local instStepNum=0
  local numInstSteps=$(expr ${#stepTotals[@]} - 1)
  local stepDesc=""
  local skipFlag=0
  local instStep=""
  local fileName=""
  local lines=""

  for instStepNum in $(eval echo "{1..$numInstSteps}"); do
    setDoneFlagForStep ${instStepNum} installSteps

    if [ ${#installSteps} -lt 1 ]; then
      skipFlag=$(echo 'true' && return 0)
    else
      skipFlag=$(echo 'false' && return 1)
    fi

    instStep=$(printf "step#%d" "$instStepNum")
    installSteps+=("$instStep")

    stepDesc=$(printf "%s" "${checkListSteps[$instStep]}")
    if [ "$instStep" == "step#4" ] && [ "${varMap["POST-INST"]}" == "Skipped" ]; then
      stepDesc+="          [Skipped]"
    fi
    installSteps+=("$stepDesc")
    installSteps+=(" ")
    if [ ${instStepNum} -lt 4 ]; then
      setTasksForTabStep "$instStep" ${instStepNum} installSteps ${skipFlag}
    fi

    fileName=$(printf "${NB_TAB_DATA_FNS[tabData]}" "$instStepNum")
    lines=$(printf "\n%s" "${installSteps[@]}")
    lines=${lines:1}
    echo -e "$lines" > "$fileName"

    installSteps=()
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                     setDoneFlagForStep
# DESCRIPTION: Set the flag to indicate whether or not all the tasks for an
#              installation/main step have been completed
#  Required Params:
#      1) stepNum - number of the installation/main step
#      2)   steps - array to append the flag to
#---------------------------------------------------------------------------------------
setDoneFlagForStep() {
  local stepNum=$1
  local -n steps=$2
  local prevStepNum=$(expr $stepNum - 1)
  local doneFlag=0
  local addFlag=$(echo 'true' && return 0)

  if [ ${prevStepNum} -gt 0 ]; then
    addFlag=$(isInstStepDone ${prevStepNum})
  fi
  if ${addFlag}; then
    doneFlag=$(isInstStepDone ${stepNum})
    if ${doneFlag}; then
      steps+=(TRUE)
    else
      steps+=(FALSE)
    fi
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                      setTasksForTabStep
# DESCRIPTION: Set the tasks to execute/run under the installation/main step
#              (i.e. Pre-Installation, etc.)
#  Required Params:
#      1)    instStep - step key of the installation/main step
#      2) instStepNum - number of installation/main step
#      3)   stepArray - array of values that will be appended to that represent each
#                       task of the installation/main step
#      4)    skipFlag - flag that indicates whether or not to add the value that indicates
#                       if the task has been executed/ran.
#---------------------------------------------------------------------------------------
setTasksForTabStep() {
  local instStep="$1"
  local instStepNum=$2
  local -n stepArray=$3
  local skipFlag=$4
  local totSteps=${stepTotals[$instStepNum]}
  local stepTask=""
  local stepDesc=""
  local taskNum=0
  local doneFlag=0
  local stepVal=" "

  for taskNum in $(eval echo "{1..$totSteps}"); do
    stepTask=$(printf "$instStep.%d" "$taskNum")
    stepDesc=$(printf "%10s%s" " " "${checkListSteps[$stepTask]}")

    if ! ${skipFlag}; then
      doneFlag=$(isTaskForStepDone ${instStepNum} ${taskNum})
      if ${doneFlag}; then
        stepArray+=(TRUE)
        stepVal=$(getValueOfTask "$instStepNum" "$taskNum")
      else
        stepArray+=(FALSE)
        stepVal=" "
      fi
    fi

    stepArray+=("$stepTask")
    stepArray+=("$stepDesc")
    stepArray+=("$stepVal")
  done
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                       getValueOfTask
# DESCRIPTION: Get the value that was configured of the task for an installation/main step
#      RETURN: Value mapped in the global associative array var "varMap"
#  Required Params:
#      1) installation/main step number
#      2) step number of the sub task of the installation/main step
#---------------------------------------------------------------------------------------
function getValueOfTask() {
  local arrayIndex=$(getArrayIndex $1 $2)
  local keyName=${VAR_KEY_NAMES["${arrayIndex}"]}
  local stepVal=" "

  if [ ${varMap["$keyName"]+_} ]; then
    local elem=$(echo "${varMap["$keyName"]}")
    if [ "$elem" != "done" ]; then
      stepVal=$(echo "${varMap[$keyName]}")
    fi
  fi

  echo "$stepVal"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:              getDataFromPartitionTable
# DESCRIPTION: Get the rows of data from the partition table to be displayed within
#              a linux "dialog" box
#      RETURN: concatenated string
#  Required Params:
#      1)  blockDevice - string value of the device (i.e. "/dev/sda")
#      2) blockDevSize - size in human-readable format of "$blockDevice"
#      3)      pttDesc - value from global associative array variable "$PART_TBL_TYPES"
#      4)      lvmDesc - contains the name of the Physical Volume and the Volume Group
#   Optional Param:
#      5) line/row of data representing a partition or logical volume delimited by '\t'
#      6) last line/row number to print
#---------------------------------------------------------------------------------------
function getDataFromPartitionTable() {
  local blockDevice="$1"
  local blockDevSize="$2"
  local pttDesc="$3"
  local lvmDesc="$4"
  local dialogPartTable=""
  local hdr=$(printf "Partition Table: %s%3sDisk '%s': %s" "$pttDesc" " " "$blockDevice" "$blockDevSize")

  if [ "$#" -gt 5 ]; then
    dialogPartTable=$(getPartitionTableHdrDtls "$5" $6)
  elif [ "$#" -gt 4 ]; then
    dialogPartTable=$(getPartitionTableHdrDtls "$5")
  else
    dialogPartTable=$(getPartitionTableHdrDtls)
  fi

  local textArray=("$lvmDesc" "$hdr" "$dialogPartTable")

  dialogPartTable=$(printf "\n%s" "${textArray[@]}")
  dialogPartTable=${dialogPartTable:1}

  echo "$dialogPartTable"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:               getSubmittedDataFromNotebook
# DESCRIPTION: Get either:
#                 A) the name and human-readable size to create either a logical volume
#                    or partition from the radio list and/or form tabs of the
#                    yad "notebook" dialog
#                 B) within the radiolist tab of the yad "notebook" dialog, the key
#                    to the row containing the logical volume or partition from the
#                    partition
#                 C) the value from clicking either the Save, Exit, or Cancel buttons
#      RETURN: concatenated string of the values submitted separated by '\t'
#  Required Params:
#      1) dialogFuncParams - associative array of key/value pairs of parameters for the function
#                           (see calling function for a description of the keys & values)
#---------------------------------------------------------------------------------------
function getCmdForPartLayoutNotebook() {
  local ptTabData=()
  local summaryTabData=()
  local nbSubData=""
  local cmdArray=()
  local -A nbCmdArray=()
  local -A nbHelpCmdArray=()
  local cmdKey="${NB_CMD_KEYS[1]}"
  local -A assocParamAry=()

  setDataForPartitionScheme assocParamAry

  assocParamAry["numTabs"]=2
  assocParamAry["dialogTitle"]=$(echo "Configured Partition/Layout Scheme")
  assocParamAry["lastTab"]="partLayout"
  assocParamAry["nbImage"]="$YAD_GRAPHICAL_PATH/images/install-guide.png"
  assocParamAry["nbText"]="<span font_weight='bold' font_size='large'>Configured Partition Layout/Scheme</span>"
  assocParamAry["nbHeight"]=570
  assocParamAry["nbWidth"]=750
  setDataForSumTblTab

  setDataForPartTblTab "${assocParamAry["partitionLayout"]}"

  local prev="${NB_TAB_DATA_FNS["tabData"]}"
  NB_TAB_DATA_FNS["tabData"]="/tmp/YAD-TabPL-%02d.data"
  setCmdToShowMainNB "$cmdKey" nbCmdArray summaryTabData ptTabData assocParamAry
  NB_TAB_DATA_FNS["tabData"]="$prev"
  echo "${nbCmdArray["$cmdKey"]}"
}

#---------------------------------------------------------------------------------------
#      METHOD:                      dispLoadingDataPB
# DESCRIPTION: Display a yad "progress indication" dialog (i.e. progress bar) while
#              executing the methods to initialize the data for the installation guide.
#  Required Params:
#      1) pbLogText - String to be displayed at the top of the logging area.
#      2) pbTitle - String to be displayed at the top of the dialog.
#---------------------------------------------------------------------------------------
dispLoadingDataPB() {
  local methodNum=1
  local pbPerc=0
  local methodName=""
  local pbTitle=""
  local totalMethods=${#pbKeys[@]}
  local continueFlag=$(echo 'true' && return 0)

  (
    sleep 0.5
    for pbKey in "${pbKeys[@]}"; do
      pbTitle="${pbTitles["$pbKey"]}"
      if [ ${pbPerc} -lt 100 ]; then
        echo ${pbPerc}
      fi
      methodName="${methodNames[$pbKey]}"

      echo "# $pbTitle"

      callInstMethod
      if ! ${continueFlag}; then
        break
      fi

      sleep 0.5
    done
  ) | $DIALOG --progress --pulsate --center --percentage=0 --log-expanded --enable-log="$1 Log" \
              --log-height=175 --width=800 --height=570 --title="$2" --size=fit \
              --text-align=center --no-buttons --auto-close --window-icon="$ICONS_PATH/app-icon.png" \
              --image="$YAD_GRAPHICAL_PATH/images/kaalp-gygi.png" --image-on-top 2> /dev/null
}

#---------------------------------------------------------------------------------------
#      METHOD:                   dispMissingRequirements
# DESCRIPTION: Display a yad "dialog" box of type "list" to display the requirements
#              and which ones are missing.
#  Required Params:
#      1)   dialogTitle - String to be displayed at the top of the dialog window.
#      2) dialogTextHdr - String to be displayed at the top of the text area.
#      3)      errorMsg - Error Message
#---------------------------------------------------------------------------------------
dispMissingRequirements() {
  local title=$(printf "%12s%s" " " "$2")
  local textArray=("$title" " " " " " " " " " " "$3")
  local dialogText=$(printf "\n%s" "${textArray[@]}")
  local dataFileName=$(printf " < \"%s\" 2> /dev/null" "$DATA_FILE_NAME")
  local yadCmd=$(getCmdForReqsDlg "$1" "$dialogText" "$dataFileName")
  local concatStr=$(printf "\n%s" "${chkList[@]}")
  concatStr=${concatStr:1}

  echo "$concatStr" > "$DATA_FILE_NAME"

  eval "$yadCmd"
  btnClick=$?
}


#---------------------------------------------------------------------------------------
#    FUNCTION:                      getCmdForReqsDlg
# DESCRIPTION: Get the command to show a yad "dialog" box of type "list" to get
#              for the requirements.
#      RETURN: concatenated string
#  Required Params:
#      1)  dialogTitle - String to be displayed at the top of the dialog window.
#      2)   dialogText - String to be displayed at the top of the text area.
#      3) dataFileName - full path to the file that contains the data to display
#---------------------------------------------------------------------------------------
function getCmdForReqsDlg() {
  local dlgBtns=("Exit")
  local dlgColumns=("Check Box" "Requirement" "Description" "Actual Value")
  local -A dlgOpts=(["buttons-layout"]="center" ["image"]="$YAD_GRAPHICAL_PATH/images/error-icon.png"
    ["window-icon"]="error-window-icon.png" ["grid-lines"]="both" ["no-selection"]=true
    ["image-on-top"]=true ["title"]="$1" ["list-type"]="checklist"
    ["text"]="<span font_weight='bold' font_size='medium'>$2</span>" ["text-align"]="left"
    ["height"]=550 ["width"]=675 ["fontname"]="Sans 12" ["center"]=true ["wrap"]=true ["size"]="fit")

  local yadCmd=$(echo "$DIALOG ")
  yadCmd+=$(getOptionsForListDialog dlgOpts)
  yadCmd+=$(getButtonsForDialog dlgBtns)
  yadCmd+=$(getColumnsForListDialog dlgColumns)
  yadCmd+="$3"

  echo "$yadCmd"
}

#---------------------------------------------------------------------------------------
#      METHOD:                      showRebootConfDlg
# DESCRIPTION: Show a yad "dialog" box of type "yes no" to get confirmation to
#              save the data and reboot the system.
#  Required Params:
#      1)   dialogTitle - String to be displayed at the top of the dialog window.
#      2) dialogTextHdr - String to be displayed at the top of the text area.
#      3)    dialogText - Text to display inside the dialog
#---------------------------------------------------------------------------------------
showRebootConfDlg() {
  local dlgBtns=("Continue" "Cancel")
  local -A yadOpts=(["buttons-layout"]="center"
  ["image"]="$YAD_GRAPHICAL_PATH/images/reboot-step.png"
    ["image-on-top"]=true ["title"]="$2" ["window-icon"]="app-icon.png" ["center"]=true
    ["wrap"]=true ["size"]="fit" ["height"]=530 ["width"]=700)

  showTextInfoDialog "$3" yadOpts dlgBtns
  btnClick=$?

  case ${btnClick} in
    ${DIALOG_OK})
      yesNoFlag=$(echo 'true' && return 0)
    ;;
    ${DIALOG_CANCEL}|${DIALOG_ESC})
      yesNoFlag=$(echo 'false' && return 1)
    ;;
  esac
}
