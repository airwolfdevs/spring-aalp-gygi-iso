#!/bin/bash
#set -e

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# globals
#===============================================================================

#===============================================================================
# functions/methods
#===============================================================================

#---------------------------------------------------------------------------------------
#      METHOD:                    selectPrimaryCountry
# DESCRIPTION: Show a yad "radiolist" dialog to select a country to be the primary
#              for configuration of the mirrors.  Set the value within the global
#              variable "$mbSelVal"
#  Required Params:
#      1)   dialogTitle - String to be displayed at the top of the dialog window.
#      2) dialogTextHdr - String to be displayed at the top of the text area.
#      3)    dialogText - Text to display inside the dialog before the
#                           list of menu options
#      4)      helpText - Text to display on the help/more dialog
#---------------------------------------------------------------------------------------
selectPrimaryCountry() {
  local dialogTitle="$1"
  local dlgButtons=("Ok" "Skip" "Help")
  local dlgColumns=("Radio Btn" "Country Code" "Name of Country")
  local title=$(printf "%32s%s" " " "$2")
  local textArray=("$title" " " " " " " " " "$3")
  local dialogText=$(printf "\n%s" "${textArray[@]}")
  dialogText=$(escapeSpecialCharacters "${dialogText:1}")
  local -A dlgOpts=(["height"]=500 ["width"]=655 ["image"]="$YAD_GRAPHICAL_PATH/images/reflector-mirror.png"
    ["image-on-top"]=true ["window-icon"]="reflector-mirror.png")
  local -A helpDlgOpts=(["width"]=610 ["title"]="Help:  $dialogTitle")
  local defaultChoice="US"
  local dlgData=()

  appendOptionsForRadiolist "$dialogText" "$dialogTitle" dlgOpts
  setDataForMultiColList MIRROR_CTRY_CODES MIRROR_CTRY_NAMES dlgData "$defaultChoice"
  local retVal=$(getValueFromRadiolist "$4" dlgOpts dlgButtons dlgColumns dlgData helpDlgOpts)

  if [ ${#retVal} -gt 0 ]; then
    mbSelVal="$retVal"
  else
    mbSelVal=""
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                     showSecondaryOption
# DESCRIPTION: Show a yad "dialog" box of type "yes no" to determine if more repo sites
#              should be added to the list of mirrors.  Set the global variable "yesNoFlag"
#              based on the option chosen.
#  Required Params:
#      1)   dialogTitle - String to be displayed at the top of the dialog window.
#      2) dialogTextHdr - String to be displayed at the top of the text area.
#      3)    dialogText - Text to display inside the dialog before the
#                           list of menu options
#      4)      helpText - Text to display on the help/more dialog
#---------------------------------------------------------------------------------------
showSecondaryOption() {
  local dialogTitle="$1"
  local dialogTextHdr="$2"
  local dialogText="$3"
  local helpText="$4"
  local dlgBtns=("Yes" "No" "Help")
  local -A helpDlgOpts=(["buttons-layout"]="center" ["window-icon"]="gtk-help.png" ["title"]="Help:  $dialogTitle"
    ["center"]=true ["height"]=350 ["width"]=610 ["wrap"]=true ["fontname"]="Sans 12")
  local -A yadOpts=(["buttons-layout"]="center" ["image"]="$YAD_GRAPHICAL_PATH/images/question.png"
  ["image-on-top"]=true ["title"]="$dialogTitle" ["window-icon"]="question-icon.png"
  ["text"]="<span font_weight='bold' font_size='large'>$dialogTextHdr</span>" ["text-align"]="left"
  ["height"]=350 ["width"]=575 ["fontname"]="Sans 12" ["center"]=true ["wrap"]=true ["size"]="fit")

  while true; do
    yesNoFlag=$(echo 'false' && return 1)
    showTextInfoDialog "$dialogText" yadOpts dlgBtns
    btnClick=$?

    case ${btnClick} in
      ${DIALOG_OK})
        yesNoFlag=$(echo 'true' && return 0)
        break
      ;;
      ${DIALOG_CANCEL})
        break
      ;;
      ${DIALOG_HELP})
        showTextInfoDialog "$helpText" helpDlgOpts
      ;;
      ${DIALOG_ESC})
        break
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                  selectSecondaryCountries
# DESCRIPTION: Show a yad "checklist" dialog to select country codes to be the secondary
#              for configuration of the mirrors.  Add the values selected to the
#              variable "ctryCodes".
#  Required Params:
#      1)   dialogTitle - String to be displayed at the top of the dialog window.
#      2) dialogTextHdr - String to be displayed at the top of the text area.
#      3)    dialogText - Text to display inside the dialog before the
#                           list of menu options
#      4)      helpText - Text to display on the help/more dialog
#---------------------------------------------------------------------------------------
selectSecondaryCountries() {
  local dialogTitle="$1"
  local primaryCtryCode="$5"
  local dlgButtons=("Ok" "Skip" "Help")
  local helpText=$(getHelpTextForNetworkConfig)
  local dlgColumns=("Radio Btn" "Country Code" "Name of Country")
  local title=$(printf "%32s%s" " " "$2")
  local textArray=("$title" " " " " " " " " "$3")
  local dialogText=$(printf "\n%s" "${textArray[@]}")
  dialogText=$(escapeSpecialCharacters "${dialogText:1}")
  local -A dlgOpts=(["height"]=500 ["width"]=655 ["image"]="$YAD_GRAPHICAL_PATH/images/reflector-mirror.png"
    ["image-on-top"]=true ["window-icon"]="reflector-mirror.png" ["list-type"]="checklist")
  local -A helpDlgOpts=(["width"]=610 ["title"]="Help:  $dialogTitle")
  local defaultChoice="US"
  local dlgData=()

  appendOptionsForRadiolist "$dialogText" "$dialogTitle" dlgOpts
  setDataForMultiColList ctryCodes ISO_CODE_NAMES dlgData "$defaultChoice"

  local retVal=$(getValueFromRadiolist "$4" dlgOpts dlgButtons dlgColumns dlgData helpDlgOpts)
  if [ ${#retVal} -gt 0 ]; then
    IFS=' ' read -d '' -a ctryCodes <<< "$retVal"
  else
    ctryCodes=()
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                      selectLinuxKernel
# DESCRIPTION: Show a yad "radiolist" dialog to select the type of officially supported
#              linux kernel to install.  Set the value within the global variable "$mbSelVal"
#  Required Params:
#      1)   dialogTitle - String to be displayed at the top of the dialog window.
#      2) dialogTextHdr - String to be displayed at the top of the text area.
#      3)    dialogText - Text to display inside the dialog before the
#                           list of menu options
#      4)      helpText - Text to display on the help/more dialog
#---------------------------------------------------------------------------------------
selectLinuxKernel() {
  local dialogTitle="$1"
  local dlgButtons=("Ok" "Cancel" "Help")
  local dlgColumns=("Radio Btn" "Linux Kernel" "Description")
  local title=$(printf "%32s%s" " " "$2")
  local textArray=("$title" " " " " " " " " "$3")
  local dialogText=$(printf "\n%s" "${textArray[@]}")
  dialogText=$(escapeSpecialCharacters "${dialogText:1}")
  local -A dlgOpts=(["height"]=500 ["width"]=655 ["image"]="$YAD_GRAPHICAL_PATH/images/arch-matrix.png"
    ["image-on-top"]=true ["window-icon"]="arch-matrix-icon.png")
  local -A helpDlgOpts=(["width"]=610 ["title"]="Help:  $dialogTitle")
  local dlgData=()

  appendOptionsForRadiolist "$dialogText" "$dialogTitle" dlgOpts
  setDataForMultiColList kernelChoices kernelDescs dlgData
  local retVal=$(getValueFromRadiolist "$4" dlgOpts dlgButtons dlgColumns dlgData helpDlgOpts)

  if [ ${#retVal} -gt 0 ]; then
    mbSelVal="$retVal"
  else
    mbSelVal=""
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                        showInstConf
# DESCRIPTION: Show a yad "dialog" box of type "yes no" to get confirmation to
#              install the base, linux kernel, python, etc.
#  Required Params:
#      1)   dialogTitle - String to be displayed at the top of the dialog window.
#      2) dialogTextHdr - String to be displayed at the top of the text area.
#      3)    dialogText - Text to display inside the dialog before the
#                           list of menu options
#      4)      helpText - Text to display on the help/more dialog
#---------------------------------------------------------------------------------------
showInstConf() {
  local dialogTitle="$1"
  local dialogTextHdr="$2"
  local dialogText="$3"
  local helpText="$4"
  local dlgBtns=("Yes" "No" "Help")
  local -A helpDlgOpts=(["buttons-layout"]="center" ["window-icon"]="gtk-help.png" ["title"]="Help:  $dialogTitle"
    ["center"]=true ["height"]=350 ["width"]=610 ["wrap"]=true ["fontname"]="Sans 12")
  local -A yadOpts=(["buttons-layout"]="center" ["image"]="$YAD_GRAPHICAL_PATH/images/question.png"
  ["image-on-top"]=true ["title"]="$dialogTitle" ["window-icon"]="question-icon.png"
  ["text"]="<span font_weight='bold' font_size='large'>$dialogTextHdr</span>" ["text-align"]="left"
  ["height"]=450 ["width"]=575 ["fontname"]="Sans 12" ["center"]=true ["wrap"]=true ["size"]="fit")

  while true; do
    yesNoFlag=$(echo 'false' && return 1)
      showTextInfoDialog "$dialogText" yadOpts dlgBtns
    btnClick=$?

    case ${btnClick} in
      ${DIALOG_OK})
        yesNoFlag=$(echo 'true' && return 0)
        break
      ;;
      ${DIALOG_CANCEL})
        break
      ;;
      ${DIALOG_HELP})
        showTextInfoDialog "$helpText" helpDlgOpts
      ;;
      ${DIALOG_ESC})
        break
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                    showDlgForSystemdSwap
# DESCRIPTION: Show a yad "dialog" box of type "yes no" to display information about
#              configuration of the 'systemd-swap' script.
#  Required Params:
#      1)   dialogTitle - String to be displayed at the top of the dialog window.
#      2) dialogTextHdr - String to be displayed at the top of the text area.
#      3)    dialogText - Text to display inside the text area of the dialog
#---------------------------------------------------------------------------------------
showDlgForSystemdSwap() {
  local dlgBtns=("Ok" "Skip")
  local -A yadOpts=(["buttons-layout"]="center" ["image"]="$YAD_GRAPHICAL_PATH/images/question.png"
  ["image-on-top"]=true ["title"]="$1" ["window-icon"]="question-icon.png"
  ["text"]="<span font_weight='bold' font_size='large'>$2</span>" ["text-align"]="left"
  ["height"]=550 ["width"]=610 ["fontname"]="Sans 12" ["center"]=true ["wrap"]=true ["size"]="fit")

  showTextInfoDialog "$3" yadOpts dlgBtns
  btnClick=$?

  case ${btnClick} in
    ${DIALOG_OK})
      yesNoFlag=$(echo 'true' && return 0)
    ;;
    *)
      yesNoFlag=$(echo 'false' && return 1)
    ;;
  esac
}
