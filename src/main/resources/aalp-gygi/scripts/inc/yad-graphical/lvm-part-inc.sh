#!/bin/bash
#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================
source "$GRAPHICAL_PATH/yad-nb-inc.sh"

#===============================================================================
# functions/methods
#===============================================================================

#---------------------------------------------------------------------------------------
#      METHOD:                 selectPartitionTableType
# DESCRIPTION: Select the type of partition table to create and set it in the
#              global variable "mbSelVal"
#  Required Params:
#      1) assocParamAry - associative array of key/value pairs of parameters for the method
#---------------------------------------------------------------------------------------
selectPartitionTableType() {
  local -n assocParamAry="$1"
  local dialogTextHdr="${assocParamAry[dialogBackTitle]}"
  local dialogTitle="${assocParamAry[dialogTitle]}"
  local dlgButtons=("Ok" "Cancel" "Help")
  local dlgColumns=("Radio Btn" "Option" "Partition Table Types")
  local -A dlgOpts=(["height"]=450 ["width"]=500 ["image-on-top"]=true )
  local pttChoices=()
  local -A pttDescs=()
  local dlgData=()
  local dialogText=$(getDialogTextForPTT "$dialogTextHdr")
  local helpText=$(getHelpTextForBL "$pttDesc")
  local -A helpDlgOpts=(["width"]=600 ["title"]="Help:  $dialogTitle")

  dlgOpts["image"]="$YAD_GRAPHICAL_PATH/images/partTableType.png"
  dlgOpts["window-icon"]="app-icon.png"

  IFS=$'\t' read -a pttChoices <<< "${assocParamAry["choices"]}"
  for choice in "${pttChoices[@]}"; do
    pttDescs["$choice"]=$(escapeSpecialCharacters "${assocParamAry[$choice]}")
  done

  appendOptionsForRadiolist "$dialogText" "$dialogTitle" dlgOpts
  setDataForMultiColList pttChoices pttDescs dlgData

  mbSelVal=$(getValueFromRadiolist "$helpText" dlgOpts dlgButtons dlgColumns dlgData helpDlgOpts)
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getDialogTextForPTT
# DESCRIPTION: Select the type of partition table to create within the linux
#              "dialog" program using the radio list type of box
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getDialogTextForPTT() {
  local line="A Partition Table is a data structure that provides basic information of"
  line+=" the device/disk.  There are two main types of partition tables available:"
  local textArray=("                                    $1" " "
  "$line"
  "       A) Master Boot Record (MBR)"
  "       B) GUID Partition Table (GPT)"
  "------------------------------------------------------------------"
  " " "Select the type to create:"
  )

  local dialogText=$(getTextForDialog "${textArray[@]}")
  echo "$dialogText"
}

#---------------------------------------------------------------------------------------
#      METHOD:                     getSizeForEspDevice
# DESCRIPTION: Get the size to be allocated for the EFI system partition.
#              Set the size in human-readable format in the global variable "mbSelVal"
#  Required Params:
#      1) assocParamAry - associative array of key/value pairs of parameters for the method
#              "deviceName" - Name of a partition or logical volume
#             "deviceSizes" - Minimum, Maximum, & Other human-readable sizes separated by '\t'
#              "deviceType" - Partition
#         "dialogBackTitle" - Text inside the dialog displayed at the very top.
#         "partitionLayout" - string containing header and partitions
#---------------------------------------------------------------------------------------
getSizeForEspDevice() {
  local -n assocParamAry="$1"
  local formVals=()

  assocParamAry["dialogText"]="The \"/boot/efi\" directory is the storage place used by "
  assocParamAry["dialogText"]+="the UEFI firmware and is mandatory for UEFI boot."

  IFS=$'\t' read -d '' -a formVals <<< "${assocParamAry[deviceSizes]}"
  formVals[-1]=$(trimString "${formVals[-1]}")
  assocParamAry["deviceSizes"]=$(getSelectableSizes formVals)

  assocParamAry["dialogTitle"]="Size of 'ESP' Allocation Choices"
  assocParamAry["dialogText"]=$(getFormattedTextForDlg)
  assocParamAry["helpText"]=$(getHelpTextForEspDevice)
  assocParamAry["lastTab"]="selectSize"
  assocParamAry["lastTabActive"]="true"

  local retVals=$(getSubmittedDataFromNotebook assocParamAry)
  if [ ${#retVals} -gt 0 ]; then
    IFS=$'\t' read -d '' -a formVals <<< "$retVals"
    mbSelVal=$(trimString "${formVals[-1]}")
  else
    mbSelVal=""
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                   getSizeForBootDevice
# DESCRIPTION: Get the size to be allocated for the /boot partition or logical volume.
#              Set the size in human-readable format in the global variable "mbSelVal"
#  Required Params:
#      1) assocParamAry - associative array of key/value pairs of parameters for the method
#              "deviceName" - Name of a partition or logical volume
#             "deviceSizes" - Minimum, Maximum, & Other human-readable sizes separated by '\t'
#              "deviceType" - Logical Volume or Partition
#         "dialogBackTitle" - Text inside the dialog displayed at the very top.
#         "partitionLayout" - string containing header, logical volumes and partitions
#---------------------------------------------------------------------------------------
getSizeForBootDevice() {
  local -n assocParamAry="$1"
  local formVals=()
  local reqFlag=$(isBootPartReq)

  assocParamAry["dialogText"]="The \"/boot\" directory contains the kernel and ramdisk images "
  assocParamAry["dialogText"]+="as well as the bootloader configuration file and bootloader stages."

  IFS=$'\t' read -d '' -a formVals <<< "${assocParamAry[deviceSizes]}"
  formVals[-1]=$(trimString "${formVals[-1]}")
  assocParamAry["deviceSizes"]=$(getSelectableSizes formVals)

  if ${reqFlag}; then
    assocParamAry["nocancel"]=true
  else
    assocParamAry["cancel-label"]="Skip"
  fi

  assocParamAry["dialogTitle"]="Size of '/boot' Allocation Choices"
  assocParamAry["dialogText"]=$(getFormattedTextForDlg)
  assocParamAry["helpText"]=$(getHelpTextForBootDevice)
  assocParamAry["lastTab"]="selectSize"
  assocParamAry["lastTabActive"]="true"

  local retVals=$(getSubmittedDataFromNotebook assocParamAry)
  if [ ${#retVals} -gt 0 ]; then
    IFS=$'\t' read -d '' -a formVals <<< "$retVals"
    mbSelVal=$(trimString "${formVals[-1]}")
  else
    mbSelVal=""
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                   getSizeForRootDevice
# DESCRIPTION: Get the size to be allocated for the '/' (i.e. root) partition
#              or logical volume.  Set the  size in human-readable format in the
#              global variable "mbSelVal"
#  Required Params:
#      1) assocParamAry - associative array of key/value pairs of parameters for the method
#              "deviceName" - Name of a partition or logical volume
#             "deviceSizes" - Minimum, Maximum, & Other human-readable sizes separated by '\t'
#              "deviceType" - Logical Volume or Partition
#         "dialogBackTitle" - Text inside the dialog displayed at the very top.
#         "partitionLayout" - string containing header, logical volumes and partitions
#---------------------------------------------------------------------------------------
getSizeForRootDevice() {
  local -n assocParamAry="$1"
  local formVals=()
  local lcDevType=$(echo "${assocParamAry[deviceType]}" | tr '[:upper:]' '[:lower:]')

  assocParamAry["dialogText"]="The '/' or root $lcDevType is necessary and it is the most important."

  assocParamAry["nocancel"]=true

  assocParamAry["dialogTitle"]="Size of 'ROOT' Allocation Choices"
  assocParamAry["dialogText"]=$(getFormattedTextForDlg)
  assocParamAry["helpText"]=$(getHelpTextForRootWithWarning "$lcDevType")
  assocParamAry["lastTab"]="selectSize"
  assocParamAry["lastTabActive"]="true"

  local retVals=$(getSubmittedDataFromNotebook assocParamAry)
  if [ ${#retVals} -gt 0 ]; then
    IFS=$'\t' read -d '' -a formVals <<< "$retVals"
    mbSelVal=$(trimString "${formVals[-1]}")
  else
    mbSelVal=""
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                   getSizeForVarDevice
# DESCRIPTION: Get the size to be allocated for the /var partition or logical volume.
#              Set the size in human-readable format in the global variable "mbSelVal"
#  Required Params:
#      1) assocParamAry - associative array of key/value pairs of parameters for the method
#              "deviceName" - Name of a partition or logical volume
#             "deviceSizes" - Minimum, Maximum, & Other human-readable sizes separated by '\t'
#              "deviceType" - Logical Volume or Partition
#         "dialogBackTitle" - Text inside the dialog displayed at the very top.
#         "partitionLayout" - string containing header, logical volumes and partitions
#             "maxPartSize" - maximum size in human-readable format
#---------------------------------------------------------------------------------------
getSizeForVarDevice() {
  local -n assocParamAry="$1"
  local formVals=()
  local lcDevType=$(echo "${assocParamAry[deviceType]}" | tr '[:upper:]' '[:lower:]')
  local lines=$(printf "\n%s" "${varDeviceTextArray[@]}")
  lines=${lines:1}

  assocParamAry["dialogText"]="${varDeviceTextArray[@]:0:2}......"

  IFS=$'\t' read -d '' -a formVals <<< "${assocParamAry[deviceSizes]}"
  formVals[-1]=$(trimString "${formVals[-1]}")
  assocParamAry["deviceSizes"]=$(getSelectableSizes formVals)

  assocParamAry["dialogTitle"]="Size of \"/var\" Allocation Choices"
  assocParamAry["dialogText"]=$(getFormattedTextForDlg)
  assocParamAry["helpText"]=$(getHelpTextForVarDevice "$lcDevType" "$lines")
  assocParamAry["lastTab"]="selectSize"
  assocParamAry["lastTabActive"]="true"

  local retVals=$(getSubmittedDataFromNotebook assocParamAry)
  if [ ${#retVals} -gt 0 ]; then
    IFS=$'\t' read -d '' -a formVals <<< "$retVals"
    mbSelVal=$(trimString "${formVals[-1]}")
  else
    mbSelVal=""
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                      getCustomDevice
# DESCRIPTION: Get a custom Logical Volume or Partition.
#              Set the:
#                 A) device size in human-readable format in the global variable "mbSelVal"
#                 B) name of the device that was entered in the global variable "ibEnteredText"
#  Required Params:
#      1) assocParamAry - associative array of key/value pairs of parameters for the method:
#             "deviceNames" - string concatenated names of existing partitions separated by '\t'
#              "deviceType" - Logical Volume or Partition
#         "dialogBackTitle" - Text inside the dialog displayed at the very top.
#            "maxAllocSize" - maximum size in human-readable format
#         "partitionLayout" - string containing header, logical volumes and partitions
#---------------------------------------------------------------------------------------
getCustomDevice() {
  local -n assocParamAry="$1"
  local formVals=("$MIN_PART_SIZE" "${assocParamAry[maxAllocSize]}")
  local deviceType="${assocParamAry[deviceType]}"
  local title=$(printf "%32sCreate a Custom \"%s\"" " " "$deviceType")
  local textArray=("$title" " " " " " " " " "Select the operation to perform on the Partition Layout/Scheme:")
  local dialogText=$(printf "\n%s" "${textArray[@]}")

  assocParamAry["deviceSizes"]=$(getSelectableSizes formVals)

  if [ "$deviceType" == "Partition" ]; then
    assocParamAry["deviceName"]="/"
  else
    assocParamAry["deviceName"]="$LOGICAL_VOL_PREFIX"
  fi

  assocParamAry["dialogTitle"]=$(echo "Create Custom $deviceType")
  assocParamAry["dialogText"]=$(escapeSpecialCharacters "${dialogText:1}")
  assocParamAry["helpText"]=$(getHelpTextForCustomDevice "$deviceType")
  assocParamAry["lastTab"]="createCustom"
  assocParamAry["numTabs"]=4
  assocParamAry["lastTabActive"]="true"

  local retVals=$(getSubmittedDataFromNotebook assocParamAry)
  if [ ${#retVals} -gt 0 ]; then
    IFS=$'\t' read -d '' -a formVals <<< "$retVals"
    mbSelVal=$(trimString "${formVals[-1]}")
    ibEnteredText=$(trimString "${formVals[0]}")
  else
    mbSelVal=""
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                   selectDeviceToRemove
# DESCRIPTION: Get the key to the row that will be removed and set it within
#              the global variable "mbSelVal"
#  Required Params:
#      1) assocParamAry - associative array of key/value pairs of parameters for the method
#                 "choices" - string of the keys containing the eligible rows that can
#                             be removed and separated by '\t'
#              "deviceType" - Logical Volume or Partition
#         "dialogBackTitle" - Specifies a backtitle string to be displayed on the
#                             backdrop, at the top of the screen.
#         "partitionLayout" - string containing header, logical volumes and partitions
#---------------------------------------------------------------------------------------
selectDeviceToRemove() {
  local -n assocParamAry="$1"
  local deviceType="${assocParamAry[deviceType]}"
  local title=$(printf "%32sRemove an Eligible \"%s\"" " " "$deviceType")
  local textArray=("$title" " " " " " " "Select the \"$deviceType\" to be removed:")
  local dialogText=$(printf "\n%s" "${textArray[@]}")
  local nbDlgVals=()

  assocParamAry["list-type"]="radiolist"
  assocParamAry["lastTab"]="removeDevice"
  assocParamAry["dialogTitle"]="Remove \"$deviceType\""
  assocParamAry["dialogText"]=$(escapeSpecialCharacters "${dialogText:1}")
  assocParamAry["helpText"]=$(getHelpTextForRemDevice "$deviceType")

  local retVal=$(getSubmittedDataFromNotebook assocParamAry)
  if [ ${#retVal} -gt 0 ]; then
    mbSelVal="$retVal"
  else
    mbSelVal=""
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                showSaveConfirmationDialog
# DESCRIPTION: Show the yad "notebook" dialog for confirmation to save the data
#              with the following tabs:
#                  #1)         "Summary" - a summary of the logical volumes & partitions
#                  #2) "Partition Table" - the rows of logical volumes & partitions
#              Set the response in the global variable "inputRetVal".
#  Required Params:
#      1)  partCmdSteps - array of the linux commands that will be executed for each step
#      2) assocParamAry - associative array of key/value pairs of parameters for the method:
#              "deviceType" - Logical Volume or Partition
#         "partitionLayout" - string containing header, logical volumes and partitions
#---------------------------------------------------------------------------------------
showSaveConfirmationDialog() {
  local -n partCmdSteps=$1
  local -n assocParamAry=$2
  local deviceType="${assocParamAry[deviceType]}"

  assocParamAry["numTabs"]=2
  assocParamAry["dialogTitle"]=$(echo "Save '${deviceType}s' Confirmation")
  assocParamAry["helpText"]=$(getHelpTextToSaveDevices "$deviceType" partCmdSteps)
  assocParamAry["lastTab"]="partTbl"

  inputRetVal=$(getSubmittedDataFromNotebook assocParamAry)
}

#---------------------------------------------------------------------------------------
#      METHOD:                   getNameOfVolumeGroup
# DESCRIPTION: Get the name of the Volume Group.  Set the value that was entered in the
#              global variable "ibEnteredText"
#  Required Params:
#      1) assocParamAry - associative array of key/value pairs of parameters for the method:
#         "dialogBackTitle" - Specifies a backtitle string to be displayed on the
#                             backdrop, at the top of the screen.
#             "dialogTitle" - String to be displayed at the top of the dialog box.
#---------------------------------------------------------------------------------------
getNameOfVolumeGroup() {
  local -n assocParamAry="$1"
  local title=$(printf "%32s%s" " " "${assocParamAry[dialogBackTitle]}")
  local textArray=("$title" " " " " " " "Enter the name of the Volume Group to be created:")
  local dialogText=$(printf "\n%s" "${textArray[@]}")

  assocParamAry["lastTab"]="volumeGroup"
  assocParamAry["dialogText"]=$(printf " %s" "${textArray[@]}")
  assocParamAry["helpText"]=$(getHelpTextForVolumeGroup)
  assocParamAry["lastTabActive"]="true"

  ibEnteredText=$(getSubmittedDataFromNotebook assocParamAry)
}

#---------------------------------------------------------------------------------------
#      METHOD:                  showSelectSwapTypeDialog
# DESCRIPTION: Display the available choices for the type of [SWAP] to create
#      1) assocParamAry - associative array of key/value pairs of parameters for the method
#             "dialogTitle" - title of dialog/window
#              "deviceType" - Logical Volume or Partition
#         "dialogBackTitle" - Text to be displayed inside the dialog towards the top.
#         "partitionLayout" - string containing header, logical volumes and/or partitions
#---------------------------------------------------------------------------------------
showSelectSwapTypeDialog() {
  local -n assocParamAry="$1"
  local deviceType="${assocParamAry[deviceType]}"
  local title=$(printf "%32s%s" " " "${assocParamAry[dialogBackTitle]}")
  local textArray=("$title" " " " " " " "Select the type of Swap to be created:")
  local dialogText=$(printf "\n%s" "${textArray[@]}")
  local swapOptions=()
  IFS=$'\t' read -a swapOptions <<< "${assocParamAry[choices]}"
  local nbDlgVals=()

  assocParamAry["lastTab"]="swapType"
  assocParamAry["list-type"]="radiolist"

  local rowNum=0
  local tabChoices=()
  for opt in "${swapOptions[@]}"; do
    rowNum=$(expr ${rowNum} + 1)
    key=$(printf "#%d" ${rowNum})
    tabChoices+=("$key")
    assocParamAry["$key"]="$opt"
  done

  assocParamAry["choices"]=$(printf "\t%s" "${tabChoices[@]}")
  assocParamAry["dialogText"]=$(escapeSpecialCharacters "${dialogText:1}")
  assocParamAry["helpText"]=$(getHelpTextForSwapType swapOptions)
  assocParamAry["cancel-label"]="Skip"

  local retVal=$(getSubmittedDataFromNotebook assocParamAry)
  if [ ${#retVal} -gt 0 ]; then
    mbSelVal=$(trimString "${assocParamAry["$retVal"]}")
  else
    mbSelVal="Skip"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                   setCmdToShowFormInNB
# DESCRIPTION: Get the command to display a yad "notebook" dialog where the last tab
#              is a form to enter a device name and/or size.
#  Required Params:
#      1)        cmdKey - the key to associative array containing the notebook & help dialogs
#      2) assocCmdArray - associative array of key/value pairs for the notebook commands
#      3)    tabDataSum - array containing the rows of data for the summary tab
#      4)     tabDataPT - array containing the rows of data for the partition table tab
#      5)    mthdParams - associative array of key/value pairs of parameters for the
#                         method (see calling function for a description of the keys & values)
#---------------------------------------------------------------------------------------
setCmdToShowFormInNB() {
  local cmdKey="$1"
  local -n assocCmdArray=$2
  local -n tabDataSum=$3
  local -n tabDataPT=$4
  local -n mthdParams=$5
  local nbKeyPlug=$(getSixDigitRandomNum)

  unset mthdParams["lastTab"]
  unset mthdParams["nocancel"]
  unset mthdParams["cancel-label"]

  mthdParams["dataFileNameFmt"]="${NB_TAB_DATA_FNS[$cmdKey]}"

  local yadCmdArray=()
  setCmdToShowNotebook ${nbKeyPlug} mthdParams tabDataPT tabDataSum yadCmdArray

  assocCmdArray["$cmdKey"]=$(printf " %s" "${yadCmdArray[@]}")
  assocCmdArray["$cmdKey"]=${assocCmdArray["$cmdKey"]:1}

  local helpText=$(getHelpTextForCreateDeviceForm mthdParams)
  helpFN=$(printf "${NB_TAB_DATA_FNS[helpText]}" 2)

  if [ ${#mthdParams["helpText"]} -gt 0 ] || [ ${mthdParams["helpTextKeys"]+_} ]; then
    mthdParams["$cmdKey"]=$(getHelpNotebookDialogCmd tabDataPT)
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                   addThirdTabToNB
# DESCRIPTION: Set the command to display the a 3rd tab within a yad "notebook" dialog
#  Required Params:
#      1)       nbKeyPlug - integer value for the notebook key & plug of its tabs
#      2)     tabRedirect - contains the name of the data file to load the tab with
#                           as well as the file name of the output generated by the tab
#---------------------------------------------------------------------------------------
addThirdTabToNB() {
  local nbKeyPlug="$1"
  local tabRedirect="$2"
  local tabNum=3
  local listColHdrs=()
  local tabCmd=""
  local tabName=""

  if [ ${nbFuncParams["lastTab"]+_} ]; then
    case "${nbFuncParams[lastTab]}" in
      "availableOperations"|"sfseOpts")
        listColHdrs=("Radio Btn" "Operation" "Description")
        if [ "${nbFuncParams[lastTab]}" == "availableOperations" ]; then
          nbButtons=("${nbButtons[@]:0:1}" "Done" "${nbButtons[@]:1}")
          tabName="Select Operation"
        else
          nbButtons[1]="Skip"
          tabName="Select SFSE Operation"
        fi

        tabCmd=$(getCmdForRadioListTab ${nbKeyPlug} ${tabNum} "$tabRedirect")
      ;;
      "createCustom")
        tabCmd=$(getCmdForSelDeviceSize ${nbKeyPlug} ${tabNum} "$tabRedirect")
        tabName="Select Size to Allocate"
      ;;
      "keepSwapSize")
        nbButtons=("Yes" "No")
        tabCmd=$(getCmdForKeepSwapSizeTab ${nbKeyPlug} ${tabNum})
        tabName=$(echo "${nbFuncParams[dialogTitle]}")
      ;;
      "removeDevice"|"selectRoot")
        listColHdrs=("Radio Btn" "Row Key" "Device Name" "Size" "Guid" "Flags")
        tabCmd=$(getCmdForRadioListTab ${nbKeyPlug} ${tabNum} "$tabRedirect")
        if [ "${nbFuncParams[lastTab]}" == "removeDevice" ]; then
          tabName=$(echo "Select &quot;${nbFuncParams[deviceType]}&quot;")
        else
          tabName=$(echo "Select ROOT &quot;${nbFuncParams[deviceType]}&quot;")
        fi
      ;;
      "selectSize")
        nbButtons=("${nbButtons[@]:0:1}" "Form" "${nbButtons[@]:1}")
        if [[ "${nbFuncParams[dialogBackTitle]}" =~ "Swap" ]]; then
          tabName=$(echo "Select Size of &quot;Swap&quot;")
          tabCmd=$(getCmdForSelDeviceSize ${nbKeyPlug} ${tabNum} "$tabRedirect" "swap")
        else
          tabName=$(echo "Select Size of &quot;${nbFuncParams[deviceType]}&quot;")
          tabCmd=$(getCmdForSelDeviceSize ${nbKeyPlug} ${tabNum} "$tabRedirect")
        fi
      ;;
      "swapType")
        listColHdrs=("Radio Btn" "Row Key" "Swap Type")
        tabCmd=$(getCmdForRadioListTab ${nbKeyPlug} ${tabNum} "$tabRedirect")
        tabName=$(echo "Select &quot;Swap Type&quot;")
      ;;
      "volumeGroup")
        tabCmd=$(getCmdForVolumeGroupEntry ${nbKeyPlug} ${tabNum} "$tabRedirect")
        tabName=$(echo "${nbFuncParams[dialogTitle]}")
      ;;
    esac
  else
    setNotebookButtonsForForm
    tabCmd=$(getCmdForCreateDeviceForm ${nbKeyPlug} ${tabNum} "$tabRedirect")
    if [ ${nbFuncParams["tabName"]+_} ]; then
      tabName="${nbFuncParams[tabName]}"
    else
      tabName=$(echo "Enter Size of &quot;${nbFuncParams[deviceType]}&quot;")
    fi
  fi
  nbFuncParams["tabCmd"]="$tabCmd"
  nbFuncParams["tabName"]="$tabName"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getCmdForSelDeviceSize
# DESCRIPTION: Get the command to show a tab in a yad "notebook" dialog to display a list
#              of device sizes to choose from.
#  Required Params:
#      1) nbKeyPlug - integer value for the notebook key & plug of its tabs
#      2)    tabNum - tab number
#      3) tabOutput - contains the name of the data file to load the tab with
#                     as well as the file name of the output generated by the tab
#---------------------------------------------------------------------------------------
function getCmdForSelDeviceSize() {
  local nbKeyPlug="$1"
  local tabNum=$2
  local tabOutput="$3"
  local deviceSizes=()
  local tabChoices=()
  local rowNum=0
  local key=""
  local colDesc=""

  IFS=$'\t' read -d '' -a deviceSizes <<< "${nbFuncParams[deviceSizes]}"
  deviceSizes[-1]=$(trimString "${deviceSizes[-1]}")
  for hrSize in "${deviceSizes[@]}"; do
    rowNum=$(expr ${rowNum} + 1)
    key=$(printf "#%d" ${rowNum})
    tabChoices+=("$key")
    if [ "$#" -gt 3 ]; then
      case $rowNum in
        1)
          colDesc="RAM installed"
        ;;
        2)
          colDesc="W/O Hibernation"
        ;;
        3)
          colDesc="W/ Hibernation"
        ;;
        4)
          colDesc="Maximum"
        ;;
      esac
      nbFuncParams["$key"]=$(printf "%s\t%s" "$hrSize" "$colDesc")
    else
      nbFuncParams["$key"]="$hrSize"
    fi
  done
  nbFuncParams["list-type"]="radiolist"
  nbFuncParams["choices"]=$(printf "\t%s" "${tabChoices[@]}")
  nbFuncParams["choices"]=${nbFuncParams["choices"]:1}

  if [ "$#" -gt 3 ]; then
    listColHdrs=("Radio Btn" "Row Key" "Human-Readable Sizes" "Description")
  else
    listColHdrs=("Radio Btn" "Row Key" "Human-Readable Sizes")
  fi
  local tabCmd=$(getCmdForRadioListTab ${nbKeyPlug} ${tabNum} "$tabOutput")

  echo "$tabCmd"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getCmdForKeepSwapSizeTab
# DESCRIPTION:  Get the command to display a yad "textinfo" dialog to ask if the existing
#               swap size should be kept
#  Required Params:
#      1) nbKeyPlug - integer value for the notebook key & plug of its tabs
#      2)    tabNum - tab number
#---------------------------------------------------------------------------------------
function getCmdForKeepSwapSizeTab() {
  local nbKeyPlug="$1"
  local tabNum=$2
  local -A nbTabOpts=(["text-align"]="left" ["notebook"]=true ["wrap"]=true
    ["text"]="<span font_weight='bold' font_size='medium'>${nbFuncParams[dialogText]}</span>"
    ["image"]="$YAD_GRAPHICAL_PATH/images/${nbFuncParams[image]}" ["image-on-top"]=true)
  local textArray=("$NB_TAB_BORDER" " "
      "The current size of the Swap '${nbFuncParams[deviceType]}' is set to \"${varMap[SWAP-SIZE]}\"." " "
      "$NB_TAB_BORDER" " " "Do you want to keep this value?"
  )
  local dialogText=$(getTextForDialog "${textArray[@]}")
  local yadCmd=$(getCmdForTextInfoTab ${nbKeyPlug} "$dialogText" ${tabNum} nbTabOpts)
  yadCmd+=" 2> /dev/null &"

  echo "$yadCmd"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getCmdForVolumeGroupEntry
# DESCRIPTION:  Get the command to display a yad "entry" dialog to set the name
#               of the Volume Group
#  Required Params:
#      1)     nbKeyPlug - integer value for the notebook key & plug of its tabs
#      2)        tabNum - tab number
#      3)     tabOutput - contains the name of the data file to load the tab with
#                         as well as the file name of the output generated by the tab
#---------------------------------------------------------------------------------------
function getCmdForVolumeGroupEntry() {
  local nbKeyPlug="$1"
  local tabNum=$2
  local tabOutput="$3"
  local -A yadOpts=(["align"]="right" ["columns"]=2 ["text-align"]="left" ["notebook"]=true
    ["text"]="<span font_weight='bold' font_size='medium'>${nbFuncParams[dialogText]}</span>"
    ["image"]="$YAD_GRAPHICAL_PATH/images/${nbFuncParams[image]}" ["image-on-top"]=true
    ["separator"]="\"$FORM_FLD_SEP\"")
  local formFields=("\"<b>Volume Group:</b>\":STE \"$INIT_VOL_GROUP\"")

  local pos=$(echo `expr index "$tabOutput" '&'`)
  pos=$(expr ${pos} - 1)

  local yadCmd=$(echo "$DIALOG --plug=${nbKeyPlug} --tabnum=${tabNum}")
  local cmd=$(getCommandForFormDialog yadOpts formFields)
  yadCmd+=$(echo "$cmd" | sed -e s/yad//)
  #yadCmd+="$tabOutput"
  yadCmd+="${tabOutput:${pos}}"

  echo "$yadCmd"
}

#---------------------------------------------------------------------------------------
#      METHOD:                  setNotebookButtonsForForm
# DESCRIPTION: Get the buttons for a notebook when displaying the form
#---------------------------------------------------------------------------------------
setNotebookButtonsForForm() {
  nbButtons=("Min" "Max" "Other" "Cancel" "Help")
  if [ ${nbFuncParams["nocancel"]+_} ]; then
    nbButtons=("${nbButtons[@]:0:3}" "${nbButtons[-1]}")
  elif [ ${nbFuncParams["cancel-label"]+_} ]; then
    nbButtons[-2]="${nbFuncParams[cancel-label]}"
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getCmdForCreateDeviceForm
# DESCRIPTION:  Get the command to display a yad "form" dialog to set the name and size
#               when creating either a logical volume or partition
#  Required Params:
#      1)     nbKeyPlug - integer value for the notebook key & plug of its tabs
#      2)        tabNum - tab number
#      3)     tabOutput - contains the name of the data file to load the tab with
#                         as well as the file name of the output generated by the tab
#---------------------------------------------------------------------------------------
function getCmdForCreateDeviceForm() {
  local nbKeyPlug="$1"
  local tabNum=$2
  local tabOutput="$3"
  local deviceName="${nbFuncParams[deviceName]}"
  local deviceSizes=()
  local -A yadOpts=(["align"]="right" ["columns"]=2 ["text-align"]="left" ["notebook"]=true
    ["text"]="<span font_weight='bold' font_size='medium'>${nbFuncParams[dialogText]}</span>"
    ["image"]="$YAD_GRAPHICAL_PATH/images/${nbFuncParams[image]}" ["image-on-top"]=true
    ["separator"]="\"$FORM_FLD_SEP\"")
  local formFields=("\"<b>Name of</b>\":LBL \" \"" "\"<b>Minimum:</b>\":RO \"%s\""
    "\"<b>Other:</b>\":NUM %.1f\!%.1f%s%.1f\!10\!1"
    "\"<b>${nbFuncParams[deviceType]}:</b>\":STE \"${deviceName}\""
    "\"<b>Maximum:</b>\":RO \"%s\"" "\"<b>Unit:</b>\":CB %s"
  )

  IFS=$'\t' read -d '' -a deviceSizes <<< "${nbFuncParams[deviceSizes]}"
  deviceSizes[-1]=$(trimString "${deviceSizes[-1]}")

  setFormFields "$deviceName" deviceSizes formFields

  local yadCmd=$(echo "$DIALOG --plug=${nbKeyPlug} --tabnum=${tabNum}")
  local cmd=$(getCommandForFormDialog yadOpts formFields)
  yadCmd+=$(echo "$cmd" | sed -e s/yad//)
  yadCmd+="$tabOutput"

  echo "$yadCmd"
}

#---------------------------------------------------------------------------------------
#      METHOD:                       setFormFields
# DESCRIPTION: Set the values to be displayed within the fields of a yad "form" dialog
#  Required Params:
#      1)    deviceName - name of the logical volume or partition
#      2)     hrsMinMax - array of human-readable sizes for the min & max values
#      3) dlgFormFields - array of the fields for the form
#---------------------------------------------------------------------------------------
setFormFields() {
  local deviceName="$1"
  local -n hrsMinMax=$2
  local -n dlgFormFields=$3
  local hrsMin="${hrsMinMax[0]}"
  local hrsMax="${hrsMinMax[-1]}"
  local hrsUnits=("B" "KiB" "MiB" "GiB" "TiB")
  local minSize=$(echo "$hrsMin" | cut -d' ' -f1)
  local maxSize=$(echo "$hrsMax" | cut -d' ' -f1)
  local minUnit=$(echo "$hrsMin" | cut -d' ' -f2)
  local maxUnit=$(echo "$hrsMax" | cut -d' ' -f2)
  local minPos=$(findPositionForString hrsUnits "$minUnit")
  local maxPos=$(findPositionForString hrsUnits "$maxUnit")
  local otherSize=""
  local power=$(expr ${maxPos} - ${minPos})

  if [ ${power} -gt 0 ]; then
    maxSize=$(echo "scale=1; $maxSize * 1024^${power}" | bc)
  fi

  if [ ${#hrsMinMax[@]} -gt 2 ]; then
    otherSize=$(echo "${hrsMinMax[-2]}" | cut -d' ' -f1)
  else
    otherSize=$(echo "scale=1; ($maxSize + $minSize) / 2" | bc)
  fi

  dlgFormFields[1]=$(printf "${dlgFormFields[1]}" "$hrsMin")
  dlgFormFields[2]=$(printf "${dlgFormFields[2]}" "$otherSize" "$minSize" ".." "$maxSize")
  if [ ${#deviceName} -gt 1 ]; then
    if [[ "$deviceName" =~ "File" ]]; then
      dlgFormFields[3]="\"<b>Swap:</b>\":RO \"File\""
      dlgFormFields=("${dlgFormFields[@]:1}")
    elif [ ${deviceName:0:4} != "$LOGICAL_VOL_PREFIX" ]; then
      dlgFormFields[3]=$(echo "${dlgFormFields[3]}" | sed -e s/":STE "/":RO "/)
    elif [ ${#deviceName} -gt 4 ]; then
      dlgFormFields[3]=$(echo "${dlgFormFields[3]}" | sed -e s/":STE "/":RO "/)
    fi
  fi


  dlgFormFields[-2]=$(printf "${dlgFormFields[-2]}" "$hrsMax")
  dlgFormFields[-1]=$(printf "${dlgFormFields[-1]}" "$minUnit")
}

#---------------------------------------------------------------------------------------
#      METHOD:                   addFourthTabToNB
# DESCRIPTION: Set the command to display the a 4th tab within a yad "notebook" dialog
#  Required Params:
#      1)       nbKeyPlug - integer value for the notebook key & plug of its tabs
#      2)     tabRedirect - contains the name of the data file to load the tab with
#                           as well as the file name of the output generated by the tab
#---------------------------------------------------------------------------------------
addFourthTabToNB() {
  local nbKeyPlug="$1"
  local tabRedirect="$2"
  local tabNum=4

  nbButtons=("${nbButtons[@]:0:1}" "Form" "${nbButtons[@]:1}")
  nbFuncParams["tabCmd"]=$(getCmdForDeviceNameEntry ${nbKeyPlug} ${tabNum} "$tabRedirect")
  nbFuncParams["tabName"]="Enter a Name"
}

#---------------------------------------------------------------------------------------
#      METHOD:                        swapImages
# DESCRIPTION: Swap the image that appears in the window icon with the image that appears
#              in the "Create Form", and vice-versa.
#---------------------------------------------------------------------------------------
swapImages() {
  local image="$1"
  local windowIcon="$2"
  echo "${image},${windowIcon}" > "$DIALOG_IMG_ICON_FILE"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getNamesOfExistingDevices
# DESCRIPTION: Get the names of the logical volumes or partitions that exist in
#              the partition table.
#      RETURN: concatenated string delimited by '\t'
#  Required Params:
#      1) partitionLayout - string containing header, logical volumes and partitions
#      2)      deviceType - Logical Volume or Partition
#---------------------------------------------------------------------------------------
function getNamesOfExistingDevices() {
  local partitionLayout="$1"
  local deviceType="$2"
  local devicePrefix="$LOGICAL_VOL_PREFIX"
  local cols=()
  local deviceName=""
  local deviceNames=()
  local rows=()
  local startRow=1

  if [ "$deviceType" == "Partition" ]; then
    devicePrefix="dev"
  fi

  if [[ "$partitionLayout" =~ "Volume Group" ]]; then
    startRow=$(expr $startRow + 1)
  fi

  IFS=$'\n' read -d '' -a rows <<< "$partitionLayout"

  for row in "${rows[@]:${startRow}}"; do
    if [[ "$row" =~ "$devicePrefix" ]]; then
      IFS=$';' read -a cols <<< "$row"
      deviceName=$(trimString "${cols[1]}")
      deviceNames+=("$deviceName")
    fi
  done

  deviceName=$(printf "\t%s" "${deviceNames[@]}")
  deviceName=${deviceName:1}
  echo "$deviceName"
}

#---------------------------------------------------------------------------------------
#      METHOD:                 showInvalidDeviceNameMsg
# DESCRIPTION: Displays the appropriate error message when either:
#              A) the name is a reserved name used by the installer
#              B) the name already exists
#  Required Params:
#      1)  errorCode - $DIALOG_INPUT_ERR0R:  reserved name
#                      $DIALOG_VALUE_ERR0R:  duplicate
#      2)   inputVal - the name that was entered
#      3) deviceType - Logical Volume or Partition
#---------------------------------------------------------------------------------------
showInvalidDeviceNameMsg() {
  local errorCode=$1
  local inputVal="$2"
  local deviceType="$3"
  local dialogTitle=$(echo "Invalid $deviceType Name Error")
  local errorMsg=""
  case $errorCode in
    $DIALOG_INPUT_ERR0R)
      errorMsg=$(printf "The %s Name \"%s\" is a reserved name that is used by the installer." \
                 "$deviceType" "$inputVal")
    ;;
    $DIALOG_VALUE_ERR0R)
      errorMsg=$(printf "There is already a %s with the name \"%s\" that exists within the partition table." \
                  "$deviceType" "$inputVal")
    ;;
  esac
  local textArray=( "$BORDER_WIDTH_600" "$errorMsg" "$BORDER_WIDTH_600" " "
  "You must re-enter the name and size!!!"
  )
  local dialogText=$(getTextForDialog "${textArray[@]}")
  local -A yadOpts=(["buttons-layout"]="center" ["image"]="$YAD_GRAPHICAL_PATH/images/error-icon.png"
  ["image-on-top"]=true ["title"]="$dialogTitle" ["window-icon"]="error-window-icon.png"
  ["text"]="<span font_weight='bold' font_size='large'>ERROR!!!!!!!</span>" ["text-align"]="left"
  ["height"]=350 ["width"]=600 ["wrap"]=true ["fontname"]="Sans 12" ["center"]=true ["wrap"]=true ["size"]="fit")

  local dlgBtns=("Exit")

  showTextInfoDialog "$dialogText" yadOpts dlgBtns
}

#---------------------------------------------------------------------------------------
#      METHOD:                   showDlgToKeepSwapSize
# DESCRIPTION: Get the name of the Volume Group and set the valuethat was entered in the
#              global variable "ibEnteredText"
#  Required Params:
#      1) assocParamAry - associative array of key/value pairs of parameters for the method:
#         "dialogBackTitle" - Specifies a backtitle string to be displayed on the
#                             backdrop, at the top of the screen.
#             "dialogTitle" - String to be displayed at the top of the dialog box.
#---------------------------------------------------------------------------------------
showDlgToKeepSwapSize() {
  local -n assocParamAry="$1"
  local title=$(printf "%32s%s" " " "${assocParamAry[dialogBackTitle]}")
  local textArray=("$title" " " " " " " "${assocParamAry[deviceName]}:")
  local dialogText=$(printf "\n%s" "${textArray[@]}")

  assocParamAry["lastTab"]="keepSwapSize"
  assocParamAry["dialogText"]="$dialogText"
  assocParamAry["yesNoFlag"]=$(echo 'true' && return 0)

  ibEnteredText=$(getSubmittedDataFromNotebook assocParamAry)
  ibEnteredText=$(trimString "$ibEnteredText")
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getFormattedTextForDlg
# DESCRIPTION: Get the text to be displayed at the top of a yad "dialog"
#---------------------------------------------------------------------------------------
function getFormattedTextForDlg() {
  local textArray=()
  local deviceName="${assocParamAry[deviceName]}"
  local deviceType="${assocParamAry[deviceType]}"
  local title=""
  if [[ "$deviceName" =~ "File" ]]; then
    title=("Create \"Swap\" $deviceName")
  elif [[ "$deviceName" =~ "swap" ]]; then
    title=("Create Swap \"$deviceType\":  '$deviceName'")
  else
    title=("Create \"$deviceType\":  '$deviceName'")
  fi

  title=$(printf "%32s%s" " " "$title")
  local textArray=("$title" " " " " " " "${assocParamAry[dialogText]}:")
  local dialogText=$(printf "\n%s" "${textArray[@]}")

  dialogText=$(escapeSpecialCharacters "${dialogText:1}")

  echo "$dialogText"
}

#---------------------------------------------------------------------------------------
#      METHOD:                   getSelectedOperation
# DESCRIPTION: Get the selected operation to perform in order to manage
#              the logical volumes or partitions.  Set the selected choice in the
#              global variable "mbSelVal".
#  Required Params:
#      1)        urlRef - The URL to be displayed in the help dialog
#      2) assocParamAry - associative array of key/value pairs of parameters for the method
#                 "choices" - string of the keys containing the available operations that can
#                             be performed on the partition table
#              "deviceType" - Logical Volume or Partition
#         "dialogBackTitle" - Specifies a backtitle string to be displayed on the
#                             backdrop, at the top of the screen.
#             "dialogTitle" - Top line in text area of the dialog/widget
#         "partitionLayout" - string containing header, logical volumes and partitions
#---------------------------------------------------------------------------------------
getSelectedOperation() {
  local urlRef="$1"
  local -n assocParamAry=$2
  local opTitle="Partitioning"
  if [[ "${assocParamAry[dialogTitle]}" =~ "LVM" ]]; then
    opTitle="LVM"
  fi
  local title=$(printf "%32sAvailable Operations for \"%s\"" " " "$opTitle")
  local textArray=("$title" " " " " " " " " "Select the operation to perform on the Partition Layout/Scheme:")
  local dialogText=$(printf "\n%s" "${textArray[@]}")

  assocParamAry["list-type"]="radiolist"
  assocParamAry["lastTab"]="availableOperations"
  assocParamAry["dialogTitle"]=assocParamAry["dialogBackTitle"]
  assocParamAry["dialogText"]=$(escapeSpecialCharacters "${dialogText:1}")
  assocParamAry["helpText"]=$(getHelpTextForDeviceManagement "$urlRef" assocParamAry)

  local apos="&apos;"
  assocParamAry["helpText"]="${assocParamAry[helpText]//$apos/"'"}"
  local retVal=$(getSubmittedDataFromNotebook assocParamAry)

  if [ ${#retVal} -gt 0 ]; then
    mbSelVal=$(trimString "$retVal")
  else
    mbSelVal="Cancel"
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getCmdForDeviceNameEntry
# DESCRIPTION:  Get the command to display a yad "entry" dialog to set the name
#               of the custom logical volume or partition
#  Required Params:
#      1)     nbKeyPlug - integer value for the notebook key & plug of its tabs
#      2)        tabNum - tab number
#      3)     tabOutput - contains the name of the data file to load the tab with
#                         as well as the file name of the output generated by the tab
#---------------------------------------------------------------------------------------
function getCmdForDeviceNameEntry() {
  local nbKeyPlug="$1"
  local tabNum=$2
  local tabOutput="$3"
  local deviceType="${nbFuncParams[deviceType]}"
  local title=$(printf "%32sCreate a Custom \"%s\"" " " "$deviceType")
  local textArray=("$title" " " " " " " "Enter a valid name:")
  local dialogText=$(printf "\n%s" "${textArray[@]}")

  dialogText=$(escapeSpecialCharacters "${dialogText:1}")

  local -A yadOpts=(["align"]="right" ["columns"]=2 ["text-align"]="left" ["notebook"]=true
    ["text"]="<span font_weight='bold' font_size='medium'>$dialogText</span>"
    ["image"]="$YAD_GRAPHICAL_PATH/images/lvm-logo.png" ["image-on-top"]=true
    ["separator"]="\"$FORM_FLD_SEP\"")
  local formFields=("\"<b>${nbFuncParams[deviceType]} Name:</b>\":STE \"${nbFuncParams[deviceName]}\"")

  local dataFileName=$(echo "$tabOutput" | cut -d'"' -f2)

  if [ ! -f "$dataFileName" ]; then
    echo -e "device name" > "$dataFileName"
  fi

  local yadCmd=$(echo "$DIALOG --plug=${nbKeyPlug} --tabnum=${tabNum}")
  local cmd=$(getCommandForFormDialog yadOpts formFields)
  yadCmd+=$(echo "$cmd" | sed -e s/yad//)
  yadCmd+="$tabOutput"

  echo "$yadCmd"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     isDeviceNameValid
# DESCRIPTION: Check if the value is a valid human-readable format
#      RETURN: 0 - true, 1 - false
#  Required Params:
#      1) hrVal - human-readable format value to be checked
#---------------------------------------------------------------------------------------
function isDeviceNameValid() {
  local submittedVals="$1"
  local deviceNames="$2"
  local deviceType="$3"
  local nameAndSize=()
  local errorCode=0

  readarray -t nameAndSize <<< "${submittedVals//$'\t'/$'\n'}"
  local deviceName=$(trimString "${nameAndSize[0]}")
  local validFlag=$(isValidDeviceName "$deviceName")

  if ! ${validFlag}; then
    errorCode=${DIALOG_INPUT_ERR0R}
  else
    deviceName=$(appendDeviceNameToPrefix "$deviceName" "$deviceType")
    ibEnteredText="$deviceName"
    validFlag=$(isDuplicateDeviceName "$deviceNames")
    if ${validFlag}; then
      errorCode=${DIALOG_VALUE_ERR0R}
    fi
  fi

  if [ "${errorCode}" -gt 0 ]; then
    showInvalidDeviceNameMsg ${errorCode} "$deviceName" "$deviceType"
    echo 'false' && return 1
  fi

  echo 'true' && return 0
}

#---------------------------------------------------------------------------------------
#      METHOD:                   getSizeForSwapDevice
# DESCRIPTION: Get the size to be allocated for the swap device which could be a file,
#              logical volume, or partition.  Set the size in human-readable format in
#              the global variable "mbSelVal"
#  Required Params:
#      1) assocParamAry - associative array of key/value pairs of parameters for the method
#              "deviceName" - Name of a partition or logical volume
#             "deviceSizes" - Minimum, Maximum, & Other human-readable sizes separated by '\t'
#              "deviceType" - Logical Volume or Partition
#         "dialogBackTitle" - Text to display inside the dialog at the very top.
#         "partitionLayout" - string containing header, logical volumes and partitions
#---------------------------------------------------------------------------------------
getSizeForSwapDevice() {
  local -n assocParamAry="$1"
  local formVals=()

  assocParamAry["dialogText"]="Select from the Recommended Sizes"
  assocParamAry["dialogTitle"]="${assocParamAry[dialogTitle]}"
  assocParamAry["dialogText"]=$(getFormattedTextForDlg)
  assocParamAry["helpText"]=$(getHelpTextForSwapSize)
  assocParamAry["lastTab"]="selectSize"

  local retVals=$(getSubmittedDataFromNotebook assocParamAry)
  if [ ${#retVals} -gt 0 ]; then
    IFS=$'\t' read -d '' -a formVals <<< "$retVals"
    mbSelVal=$(trimString "${formVals[-1]}")
  else
    mbSelVal=""
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                   selectRootDevice
# DESCRIPTION: Select the '/' or root logical volume or partition.
#  Required Params:
#      1) assocParamAry - associative array of key/value pairs of parameters for the method
#                 "choices" - string of the keys containing the eligible rows that can
#                             be removed and separated by '\t'
#              "deviceType" - Logical Volume or Partition
#         "dialogBackTitle" - Specifies a backtitle string to be displayed on the
#                             backdrop, at the top of the screen.
#         "partitionLayout" - string containing header, logical volumes and partitions
#---------------------------------------------------------------------------------------
selectRootDevice() {
  local -n assocParamAry="$1"
  local deviceType="${assocParamAry[deviceType]}"
  local title=$(printf "%32sDetermine ROOT \"%s\"" " " "$deviceType")
  local textArray=("$title" " " " " " " "Select the \"$deviceType\" that is the '/' or root:")
  local dialogText=$(printf "\n%s" "${textArray[@]}")

  local nbDlgVals=()

  assocParamAry["list-type"]="radiolist"
  assocParamAry["lastTab"]="selectRoot"
  assocParamAry["dialogTitle"]="${assocParamAry[dialogBackTitle]}"
  assocParamAry["dialogText"]=$(escapeSpecialCharacters "${dialogText:1}")
  assocParamAry["helpText"]=$(getHelpTextForRoot "$deviceType" )

  local retVal=$(getSubmittedDataFromNotebook assocParamAry)
  if [ ${#retVal} -gt 0 ]; then
    mbSelVal="$retVal"
  else
    mbSelVal=""
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:               selectSwapFileSizeExceedOpts
# DESCRIPTION: Select from the options available when the Swap File Size exceeds
#              the amount of buffer space in the root partition or logical volume.
#              Set the value selected within the global
#      1) assocParamAry - associative array of key/value pairs of parameters for the method
#                 "choices" - string of the keys containing the eligible rows that can
#                             be removed and separated by '\t'
#              "deviceType" - Logical Volume or Partition
#         "dialogBackTitle" - Specifies a backtitle string to be displayed on the
#                             backdrop, at the top of the screen.
#         "partitionLayout" - string containing header, logical volumes and partitions
#---------------------------------------------------------------------------------------
selectSwapFileSizeExceedOpt() {
  local -n assocParamAry="$1"
  local dialogText=$(getTextForSFSEO)
  local textArray=("              ${assocParamAry[dialogTitle]}"
  "\n\n$dialogText")
  dialogText=$(printf " %s" "${textArray[@]}")

  assocParamAry["list-type"]="radiolist"
  assocParamAry["lastTab"]="sfseOpts"
  assocParamAry["dialogTitle"]=assocParamAry["dialogBackTitle"]
  assocParamAry["dialogText"]=$(escapeSpecialCharacters "${dialogText:1}")
  assocParamAry["helpText"]=$(getHelpTextForDeviceManagement "$urlRef" assocParamAry)

  local retVal=$(getSubmittedDataFromNotebook assocParamAry)

  if [ ${#retVal} -gt 0 ]; then
    mbSelVal="$retVal"
  else
    mbSelVal="Skip"
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     getTextForSFSEO
# DESCRIPTION: Get the text to display on a linux "dialog" when the Swap File Size
#              exceeds the amount of buffer space in the root partition or
#              logical volume.
#---------------------------------------------------------------------------------------
function getTextForSFSEO() {
  local minRootSize="${ROOT_HR_SIZES[0]}"
  local minSize=$(humanReadableToBytes "$minRootSize")
  local rootDeviceSize=$(bytesToHumanReadable ${varMap["ROOT-DEVICE-SIZE"]})
  local spaceRemain=$(expr ${varMap["ROOT-DEVICE-SIZE"]} - $minSize)
  local hrBuffer=$(bytesToHumanReadable $spaceRemain)
  local textArray=("                    Root Size:  $rootDeviceSize                    Min. Size:  $minRootSize"
  "           Buffer Space:  $hrBuffer                       File Size:  ${varMap[SWAP-SIZE]}" "$DIALOG_BORDER")
  local lines=("The size of the file exceeds the buffer space.  You"
  "can choose to \"Skip\" the creation of the Swap entirely OR"
  "select from one of the available options below:")

  local dialogText=$(printf " %s" "${lines[@]}")
  dialogText=${dialogText:1}
  textArray+=("$dialogText")
  dialogText=$(getTextForDialog "${textArray[@]}")
  echo "$dialogText"
}

#---------------------------------------------------------------------------------------
#      METHOD:                 showWarningDialogForBootLV
# DESCRIPTION: Show the warning message within a yad "text-info" dialog and get
#              confirmation to continue or not.  Set the value in the global
#              variable "yesNoFlag"
#  Required Params:
#      1) dialogTitle - String to be displayed at the top of the dialog window.
#      2)     warnMsg - the warning message/text to display
#---------------------------------------------------------------------------------------
showWarningDialogForBootLV() {
  local dlgBtns=("Yes" "No")
  local textArray=("$2" "------------------------------------------------------------------"
                  " " "Do you want to continue?")
  local dialogText=$(getTextForDialog "${textArray[@]}")
  local -A yadOpts=(["buttons-layout"]="center" ["image"]="$YAD_GRAPHICAL_PATH/images/warning-icon.png"
      ["image-on-top"]=true ["title"]="Warning:  $1" ["window-icon"]="warning-window-icon.png"
      ["text"]="<span font_weight='bold' font_size='large'>WARNING!!!!!!!</span>" ["text-align"]="left"
      ["height"]=350 ["width"]=500 ["fontname"]="Sans 12" ["center"]=true ["wrap"]=true ["size"]="fit")

  showTextInfoDialog "$dialogText" yadOpts dlgBtns
  btnClick=$?
  case ${btnClick} in
    ${DIALOG_OK})
      yesNoFlag=$(echo 'true' && return 0)
    ;;
    *)
      yesNoFlag=$(echo 'false' && return 1)
    ;;
  esac
}

#---------------------------------------------------------------------------------------
#      METHOD:                  dispSkipSwapMsg
# DESCRIPTION: Display message that the installer cannot continue creating the
#              swap
#  Required Params:
#      1)   dialogTitle - String to be displayed at the top of the dialog window.
#      2) dialogTextHdr - String to be displayed at the top of the text area.
#      3)      errorMsg - Error message
#---------------------------------------------------------------------------------------
dispSkipSwapMsg() {
  showErrorDialog "Error:  $1" "$2" "$3"
}
