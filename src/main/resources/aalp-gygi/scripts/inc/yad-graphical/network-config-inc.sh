#!/bin/bash
#set -e

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# functions/methods
#===============================================================================

#---------------------------------------------------------------------------------------
#      METHOD:                     showManualConfigForm
# DESCRIPTION: Displays a linux "form" dialog to enter an "IP Address",
#              "Submask", & "Gateway".  Sets the values entered in the
#              global variable "ibEnteredText".
#  Required Params:
#      1) "dialogTitle" - String to be displayed at the top of the dialog box.
#---------------------------------------------------------------------------------------
showManualConfigForm() {
  local dialogTitle="$1"
  local dialogButtons=("Submit" "Cancel")
  local dialogText="Ethernet/Wired Manual Configuration\n\n\n\n\nAll fields are required:"
  local -A yadOpts=(["align"]="right" ["buttons-layout"]="center" ["center"]=true ["title"]="$dialogTitle"
    ["text"]="<span font_weight='bold' font_size='medium'>$dialogText</span>" ["text-align"]="left"
    ["height"]=350 ["width"]=610 ["image"]="$YAD_GRAPHICAL_PATH/images/network-config.png"
    ["image-on-top"]=true ["window-icon"]="network-config-icon.png" ["separator"]="\"$FORM_FLD_SEP\"")
  local formVals=""
  local formFields=("\"IP Address:\":STE" "\"Submask:\":STE" "\"Gateway:\":STE")
  local cmd=$(getCommandForFormDialog yadOpts formFields dialogButtons)

  formVals=$(eval "$cmd")
  btnClick=$?

  case ${btnClick} in
    ${DIALOG_OK})
      ibEnteredText="${formVals//$FORM_FLD_SEP/$'\n'}"
      ibEnteredText=$(tr '\n' "$FORM_FLD_SEP" <<< "$ibEnteredText")
      ibEnteredText=$(echo "${DIALOG_OK},$ibEnteredText")
    ;;
    ${DIALOG_CANCEL})
      ibEnteredText=$(echo "${DIALOG_CANCEL},Cancel")
    ;;
    ${DIALOG_ESC})
      ibEnteredText=$(echo "${DIALOG_CANCEL},ESC")
    ;;
  esac
}

#---------------------------------------------------------------------------------------
#      METHOD:                       dispInvFormVals
# DESCRIPTION: Displays the appropriate error message
#  Required Params:
#      1) dialogTitle - String to be displayed at the top of the dialog box.
#      2)        errorMsg - the error message to display
#---------------------------------------------------------------------------------------
dispInvFormVals() {
  local dialogTitle=$(echo "Error: Invalid $1")
  local errorMsg="$2"
  local concatStr=$(printf ", '%s'" "${fieldNames[@]}")
  local textArray=( "$errorMsg" "$BORDER_WIDTH_600" " "
    "Please enter valid values for the form fields:  ${concatStr:2}"
    "                                   OR" " "
    "Click the \"Cancel\" button to choose a different connection configuration")
  local dialogText=$(getTextForDialog "${textArray[@]}")
  local -A yadOpts=(["buttons-layout"]="center" ["image"]="$YAD_GRAPHICAL_PATH/images/error-icon.png"
  ["image-on-top"]=true ["title"]="$dialogTitle" ["window-icon"]="error-window-icon.png"
  ["text"]="<span font_weight='bold' font_size='large'>ERROR!!!!!!!</span>" ["text-align"]="left"
  ["height"]=450 ["width"]=600 ["wrap"]=true ["fontname"]="Sans 12" ["center"]=true ["wrap"]=true ["size"]="fit")

  local dlgBtns=("Exit")

  showTextInfoDialog "$dialogText" yadOpts dlgBtns
}

#---------------------------------------------------------------------------------------
#      METHOD:                   showDlgInputBoxForProxy
# DESCRIPTION: Displays a yad "form" dialog to enter a proxy.
#              Sets the values entered in the global variable "ibEnteredText".
#  Required Params:
#      1) "dialogTitle" - String to be displayed at the top of the dialog box.
#---------------------------------------------------------------------------------------
showDlgInputBoxForProxy() {
  local dialogTitle="$1"
  local formVals=()
  local retVal=""
  while true; do
    retVal=$(getProxyValue "$dialogTitle")
    IFS=',' read -a formVals <<< "$retVal"
    case ${formVals[0]} in
      ${DIALOG_OK})
        ibEnteredText=$(echo "${formVals[1]}")
        break
      ;;
      ${DIALOG_CANCEL})
        ibEnteredText=""
        break
      ;;
      *)
        showInvalidProxyMsg ${formVals[0]} "${formVals[1]}"
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                        getProxyValue
# DESCRIPTION: Get the value of the proxy entered in a yad "form" dialog.
#      RETURN: concatenated string of the Button ID and Proxy entered separated by ','
#  Required Params:
#      1) "dialogTitle" - String to be displayed at the top of the dialog box.
#---------------------------------------------------------------------------------------
function getProxyValue() {
  local dialogTitle="$1"
  local dialogButtons=("Submit" "Cancel")
  local dialogText="Proxy Configuration\n\n\n\n\nEnter the proxy in the form of \"protocol://address:port\":"
  local -A yadOpts=(["align"]="right" ["buttons-layout"]="center" ["center"]=true ["title"]="$dialogTitle"
    ["text"]="<span font_weight='bold' font_size='medium'>$dialogText</span>" ["text-align"]="left"
    ["height"]=350 ["width"]=610 ["image"]="$YAD_GRAPHICAL_PATH/images/network-config.png"
    ["image-on-top"]=true ["window-icon"]="network-config-icon.png" ["separator"]="\"$FORM_FLD_SEP\"")
  local formVals=""
  local formFields=("\"Proxy:\":STE")
  local cmd=$(getCommandForFormDialog yadOpts formFields dialogButtons)
  local validFlag=$(echo 'false' && return 1)
  local retVals=""

  formVals=$(eval "$cmd")
  btnClick=$?

  case ${btnClick} in
    ${DIALOG_OK})
      ibEnteredText="${formVals//$FORM_FLD_SEP/$'\n'}"
      ibEnteredText=$(trimString "$ibEnteredText")
      if [ ${#ibEnteredText} -gt 0 ]; then
        validFlag=$(validateProxy)
        if ${validFlag}; then
          retVals=$(echo "$DIALOG_OK,$ibEnteredText")
        else
          retVals=$(echo "$DIALOG_VALUE_ERR0R,$ibEnteredText")
        fi
      else
        retVals=$(echo "$DIALOG_INPUT_ERR0R,No Value")
      fi
    ;;
    ${DIALOG_CANCEL})
      retVals=$(echo "${DIALOG_CANCEL},Cancel")
    ;;
    ${DIALOG_ESC})
      retVals=$(echo "${DIALOG_CANCEL},ESC")
    ;;
  esac

  echo "$retVals"
}

#---------------------------------------------------------------------------------------
#      METHOD:                 showInvalidDeviceNameMsg
# DESCRIPTION: Displays the appropriate error message when either:
#              A) no proxy value was entered
#              B) the proxy value entered is invalid
#  Required Params:
#      1)  errorCode - $DIALOG_INPUT_ERR0R:  blank
#                      $DIALOG_VALUE_ERR0R:  invalid
#      2)   inputVal - the proxy value that was entered
#---------------------------------------------------------------------------------------
showInvalidProxyMsg() {
  local errorCode=$1
  local inputVal="$2"
  local dialogTitle=$(echo "Error:  Invalid Proxy Entered")
  local errorMsg=""
  case $errorCode in
    ${DIALOG_VALUE_ERR0R})
      errorMsg=$(printf "The proxy value \"%s\" is invalid!" "$inputVal")
    ;;
    ${DIALOG_INPUT_ERR0R})
      errorMsg="No value was entered for the proxy!"
    ;;
  esac
  local textArray=( "$errorMsg" "$BORDER_WIDTH_600" " "
    "Please enter the proxy in the form of \"protocol://address:port\"" " "
    "                                   OR" " "
    "Click the \"Cancel\" button to choose a different connection configuration")
  local dialogText=$(getTextForDialog "${textArray[@]}")
  local -A yadOpts=(["buttons-layout"]="center" ["image"]="$YAD_GRAPHICAL_PATH/images/error-icon.png"
  ["image-on-top"]=true ["title"]="$dialogTitle" ["window-icon"]="error-window-icon.png"
  ["text"]="<span font_weight='bold' font_size='large'>ERROR!!!!!!!</span>" ["text-align"]="left"
  ["height"]=350 ["width"]=600 ["wrap"]=true ["fontname"]="Sans 12" ["center"]=true ["wrap"]=true ["size"]="fit")

  local dlgBtns=("Exit")

  showTextInfoDialog "$dialogText" yadOpts dlgBtns
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                showTypeOfConnectionConfigDlg
# DESCRIPTION: Get the type of connection configuration to set up.
#              Set value within the global variable "mbSelVal".
#  Required Params:
#      1) dialogTextHdr - Text inside the dialog displayed at the very top.
#      2)      skipFlag - flag to indicate whether skip button is available
#---------------------------------------------------------------------------------------
showTypeOfConnectionConfigDlg() {
  local dialogTextHdr="$1"
  local skipFlag=$2
  local dlgButtons=("Ok" "Help")
  local helpText=$(getHelpTextForNetworkConfig)
  local dlgColumns=("Radio Btn" "Option" "Network Configuration Types")
  local title=$(printf "%32s%s" " " "WARNING!!!!!!!  Connection Not Found!")
  local textArray=("$title" "Ethernet/Wired Device Name:  \"$ETHERNET_DEVICE_NAME\""
  "      Wireless Device Name:  \"$WIRELESS_DEVICE_NAME\""
  " " " " " " "Choose how the connection should be configured:")
  local dialogText=$(printf "\n%s" "${textArray[@]}")
  dialogText=$(escapeSpecialCharacters "${dialogText:1}")
  local -A dlgOpts=(["height"]=400 ["width"]=655 ["image"]="$YAD_GRAPHICAL_PATH/images/warning-icon.png"
    ["image-on-top"]=true ["window-icon"]="warning-window-icon.png")
  local -A helpDlgOpts=(["width"]=610 ["title"]="Help:  $dialogTextHdr")
  local defaultChoice="#1"
  local configDlgChoices=()
  local -A configDescs=()
  local dlgData=()
  local optNum=0

  for opt in "${connConfigOpts[@]}"; do
    optNum=$(expr ${optNum} + 1)
    key=$(printf "#%d" ${optNum})
    configDlgChoices+=("$key")
    configDescs["$key"]="$opt"
  done

  if ${skipFlag}; then
    dlgButtons=("Ok" "Skip" "Help")
  fi

  appendOptionsForRadiolist "$dialogText" "$dialogTextHdr" dlgOpts
  setDataForMultiColList configDlgChoices configDescs dlgData "$defaultChoice"
  local retVal=$(getValueFromRadiolist "$helpText" dlgOpts dlgButtons dlgColumns dlgData helpDlgOpts)
  if [ ${#retVal} -gt 0 ]; then
    mbSelVal="${configDescs["$retVal"]}"
  else
    mbSelVal="Skip"
  fi
}
