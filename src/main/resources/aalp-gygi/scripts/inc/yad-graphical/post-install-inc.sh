#!/bin/bash

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# globals
#===============================================================================
declare NB_BTNS="Ok${FORM_FLD_SEP}Cancel${FORM_FLD_SEP}Run${FORM_FLD_SEP}Help"
declare NB_BTNS_WITH_SKIP="Ok${FORM_FLD_SEP}Cancel${FORM_FLD_SEP}Run${FORM_FLD_SEP}SSkip${FORM_FLD_SEP}Help"
declare NB_BTNS_FOR_CAT="Ok${FORM_FLD_SEP}Cancel${FORM_FLD_SEP}Select${FORM_FLD_SEP}SSkip${FORM_FLD_SEP}Help"
declare NB_BTNS_FOR_SUB_CAT="Ok${FORM_FLD_SEP}Cancel${FORM_FLD_SEP}Commit${FORM_FLD_SEP}Select${FORM_FLD_SEP}SSkip${FORM_FLD_SEP}Help"
declare CAT_NB_BTNS="Ok${FORM_FLD_SEP}Cancel${FORM_FLD_SEP}Install${FORM_FLD_SEP}SSkip${FORM_FLD_SEP}Help"
declare IMG_ROOT="$YAD_GRAPHICAL_PATH/images/post-inst"
declare -A POST_INST_NB_OPTS=(["height"]=${YAD_MAX_HEIGHT} ["width"]=610 ["center"]=true ["buttons-layout"]="center"
                              ["image"]="${IMG_ROOT}/post-inst-step.png" ["image-on-top"]=true
                              ["title"]="$POST_INST_DIALOG_BACK_TITLE" ["window-icon"]="app-icon.png")
declare -A SOFT_CAT_IMAGES=(
["Basic Setup"]="basic-setup.png"
["Desktop Configuration"]="desktop-conf.png"
["Accessory Software"]="accessory.png"
["Apache Tomcat"]="apache-tomcat.png"
["Application &amp; Web Servers"]="app-web-servers.png"
["Development Software"]="development.png"
["Documents &amp; Texts Software"]="documents-and-texts.png"
["Internet Software"]="internet.png"
["Java Development Kits"]="java.png"
["Multimedia Software"]="multimedia.png"
["System &amp; Utility Software"]="security.png"
["Security Software"]="sys-and-util.png"
["Software Extras"]="soft-extras.png"
)
declare DEF_TAB_COLUMNS=("Radio Button" "Is Done" "Software Task" "Description")
declare SEL_CHKLIST_OPT=()
declare -A TAB_ASSOC_ARRAY=()
declare -A SUB_CAT_ASSOC_ARRAY=()
source "$GRAPHICAL_PATH/yad-nb-inc.sh"

#===============================================================================
# functions/methods
#===============================================================================

#---------------------------------------------------------------------------------------
#      METHOD:                     showPostInstOption
# DESCRIPTION: Show a yad "dialog" box of type "yes no" to determine if to continue or
#              skip this step.
#  Required Params:
#      1)   dialogTitle - String to be displayed at the top of the dialog window.
#      2) dialogTextHdr - String to be displayed at the top of the text area.
#      3)    dialogText - Text to display inside the dialog before the
#                         list of menu options
#      4)      helpText - Text to display on the help/more dialog
#---------------------------------------------------------------------------------------
showPostInstOption() {
  local dialogTitle="$1"
  local dialogTextHdr="$2"
  local dialogText="$3"
  local helpText="$4"
  local dlgBtns=("Continue" "Skip" "Help")
  local -A helpDlgOpts=(["buttons-layout"]="center" ["window-icon"]="gtk-help.png" ["title"]="Help:  $dialogTitle"
    ["center"]=true ["height"]=350 ["width"]=610 ["wrap"]=true ["fontname"]="Sans 12")
  local -A yadOpts=(["buttons-layout"]="center" ["image"]="$YAD_GRAPHICAL_PATH/images/post-inst/post-inst-conf.png"
  ["image-on-top"]=true ["title"]="$dialogTitle" ["window-icon"]="app-icon.png" ["text-align"]="left"
  ["height"]=600 ["width"]=710 ["fontname"]="Sans 12" ["center"]=true ["wrap"]=true ["size"]="fit")

  while true; do
    yesNoFlag=$(echo 'false' && return 1)
    showTextInfoDialog "$dialogText" yadOpts dlgBtns
    btnClick=$?

    case ${btnClick} in
      ${DIALOG_OK})
        yesNoFlag=$(echo 'true' && return 0)
        break
      ;;
      ${DIALOG_CANCEL}|${DIALOG_ESC})
        break
      ;;
      ${DIALOG_HELP})
        showTextInfoDialog "$helpText" helpDlgOpts
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                  showPostInstallChecklist
# DESCRIPTION: Displays the checklist for the post-installation step within a yad
#              "notebook" dialog.
#        NOTE:  The calling method must declare an associative array variable
#               with the name of "piAssocArray" with the following key/value pairs:
#          "defaultVal" - value of the menu option to set the radio button to checked
#            "helpText" - text to display within the help dialog
#           "optsArray" - array of menu options that can be selected
#      "descAssocArray" - associative array of descriptions for the menu options
#---------------------------------------------------------------------------------------
showPostInstallChecklist() {
  local notebookCmds=()
  local -A helpOpts=(["buttons-layout"]="center" ["window-icon"]="gtk-help.png" ["title"]="Help:  ${POST_INST_NB_OPTS["title"]}"
     ["center"]=true ["wrap"]=true ["fontname"]="Sans 12")

  piAssocArray["nbKeyPlug"]=$(getSixDigitRandomNum)
  setPostInstallChecklistCmd

  local nbCommand=$(printf " %s" "${notebookCmds[@]}")
  nbCommand=${nbCommand:1}

  while true; do
    clickedVals=$(showNotebookDialog "$nbCommand" " ")
    IFS=$',' read -d '' -a btnVals <<< "$clickedVals"
    case ${btnVals[0]} in
      ${DIALOG_OK})
        setSelectedChklistOpt
        break
      ;;
      ${DIALOG_HELP})
        showTextInfoDialog "${piAssocArray["helpText"]}" helpOpts
      ;;
      ${DIALOG_SSKIP})
        unset piAssocArray["addSkipBtn"]
        piAssocArray["skipBtn"]="true"
        break
      ;;
      *)
        SEL_CHKLIST_OPT=()
        break
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                 setPostInstallChecklistCmd
# DESCRIPTION: Sets the command to display the "Checklist" tab of a "notebook" dialog.
#        NOTE:  The calling method must declare an associative array variable
#               with the name of "piAssocArray" with the following key/value pairs:
#          "defaultVal" - value of the menu option to set the radio button to checked
#            "helpText" - text to display within the help dialog
#           "optsArray" - array of menu options that can be selected
#      "descAssocArray" - associative array of descriptions for the menu options
#           "nbKeyPlug" - integer value for the notebook key & plug of its tabs
#---------------------------------------------------------------------------------------
setPostInstallChecklistCmd() {
  local nbKeyPlug=${piAssocArray["nbKeyPlug"]}
  local nbButtons=("Ok" "Exit" "Help")
  local nbCmd=$(echo "$DIALOG --notebook --key=${nbKeyPlug}")

  piAssocArray+=(["tabName"]="Checklist" ["tabNum"]=1 ["listType"]="radiolist"
                ["tabText"]='Select the "Software Task" you want to execute/run')

  if [ ${piAssocArray["addSkipBtn"]+_} ]; then
    nbButtons=("Ok" "Exit" "SSkip" "Help")
  fi

  setDataForPostInstTabNB

  local tabCmd=$(getCommandForTab "piAssocArray")
  tabCmd=$(printf "$tabCmd" ${nbKeyPlug})

  notebookCmds+=("$tabCmd")
  nbCmd+=$(printf " --tab=\"%s\"" "${piAssocArray["tabName"]}")
  nbCmd+=$(getOptionsForNotebook POST_INST_NB_OPTS)
  nbCmd+=$(getButtonsForDialog nbButtons)
  notebookCmds+=("$nbCmd")
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                      getCommandForTab
# DESCRIPTION: Get the command for the new tab to be displayed within a
#              yad "notebook" dialog.
#        NOTE: The tab command is appended/saved to the global
#              file name variable "POST_INST_YAD_TABS"
#      RETURN: concatenated string
#  Required Params:  associative array "piAssocArray" with key/value pairs:
#      "tabName"  - name of the tab to create
#      "tabNum"   - number representing the tab
#      "tabText"  - text to display below the name of the tab when active
#      "listType" - "checklist" or "radiolist"
#---------------------------------------------------------------------------------------
function getCommandForTab() {
  local -n tabAssocArray="$1"
  local tabName=$(escapeSpecialCharacters "${tabAssocArray["tabName"]}")
  local tabNum=${tabAssocArray["tabNum"]}
  local tabText=$(escapeSpecialCharacters "${tabAssocArray["tabText"]}")
  local -A tabOpts=(["grid-lines"]="both" ["notebook"]=true
                    ["text"]="<span font_weight='bold' font_size='medium'>%40s%s</span>"
                    ["text-align"]="left" ["wrap-cols"]="2,3" ["wrap-width"]=225)
  local tabOutput=$(echo " < \"%s\" &> \"%s\" 2> /dev/null &")
  local tabColumns=("${DEF_TAB_COLUMNS[@]}")
  local tabCmd=$(echo "$DIALOG --plug=%d --tabnum=${tabNum}")
  local cmdArray=("$tabCmd")
  local tabDataFN=$(printf "${NB_TAB_DATA_FNS["tabData"]}" "$tabNum")
  local outputFN=$(printf "${NB_TAB_DATA_FNS["nbCmd#01"]}" "$tabNum")

  if [ ${tabAssocArray["listType"]+_} ]; then
    tabOpts["list-type"]="${tabAssocArray["listType"]}"
  else
    tabOpts["no-selection"]=true
  fi

  if [ ${#tabText} -gt 58 ]; then
    tabOpts["text"]="<span font_weight='bold' font_size='medium'>%20s%s</span>"
  fi
  tabOpts["text"]=$(printf "${tabOpts["text"]}" " " "$tabText")

  tabCmd=$(getOptionsForListDialog tabOpts)
  cmdArray+=("$tabCmd")

  if [ ${tabAssocArray["tabColumns"]+_} ]; then
    readarray -t tabColumns <<< "${tabAssocArray["tabColumns"]//$FORM_FLD_SEP/$'\n'}"
  fi

  tabCmd=$(getColumnsForListDialog tabColumns)
  cmdArray+=("$tabCmd")
  tabCmd=$(printf "$tabOutput" "$tabDataFN" "$outputFN")
  cmdArray+=("$tabCmd")

  tabCmd=$(printf " %s" "${cmdArray[@]}")
  tabCmd=${tabCmd:1}

  if [ "$tabName" == "Checklist" ]; then
    echo "${tabName}${FORM_FLD_SEP}${tabCmd}" > "$POST_INST_YAD_TABS"
  else
    echo "${tabAssocArray["tabName"]}${FORM_FLD_SEP}${tabCmd}" >> "$POST_INST_YAD_TABS"
  fi

  echo "$tabCmd"
}

#---------------------------------------------------------------------------------------
#      METHOD:                   setDataForPostInstTabNB
# DESCRIPTION: Set the data for the new tab to be displayed within a
#              yad "notebook" dialog that lists the options for the selected option in
#              2nd to last tab.
#  Required Params: associative array "piAssocArray" with key/value pairs:
#      "chklArray"      - name of the array to reference that is the checklist for the
#                         category/software task.
#      "defaultVal"     - the value to mark as checked within the list
#      "descAssocArray" - name of the associative array to reference containing the
#                         descriptions of the values that can be selected
#      "optsArray"      - name of the array to reference containing the values that
#                         can be selected
#      "tabColumns"     - name of the column headers
#      "tabNum"         - number representing the tab
#---------------------------------------------------------------------------------------
setDataForPostInstTabNB() {
  local -n chklArray="${piAssocArray["chklArray"]}"
  local -n descAssocArray="${piAssocArray["descAssocArray"]}"
  local -n optsArray="${piAssocArray["optsArray"]}"
  local defaultVal="${piAssocArray["defaultVal"]}"
  local fileName=$(printf "${NB_TAB_DATA_FNS["tabData"]}" ${piAssocArray["tabNum"]})
  local stepTask=()
  local tabColumns=("${DEF_TAB_COLUMNS[@]}")
  local hasDoneCol=$(echo 'false' && return 1)

  if [ ${piAssocArray["tabColumns"]+_} ]; then
    readarray -t tabColumns <<< "${piAssocArray["tabColumns"]//$FORM_FLD_SEP/$'\n'}"
  fi

  if [ "${tabColumns[1]}" == "${DEF_TAB_COLUMNS[1]}" ]; then
    hasDoneCol=$(echo 'true' && return 0)
  fi

  for menuOpt in "${optsArray[@]}"; do
    local rows=()
    if ${hasDoneCol}; then
      if [ "$menuOpt" == "$defaultVal" ]; then
        rows=(TRUE)
      else
        rows=(FALSE)
      fi
    fi

    local desc=$(escapeSpecialCharacters "${descAssocArray["$menuOpt"]}")

    if ${hasDoneCol}; then
      readarray -t stepTask <<< "${menuOpt//\./$'\n'}"
      local chklIdx=${stepTask[-1]}
      local idxVal=${chklArray[${chklIdx}]}
      if [ ${idxVal} -gt 1 ]; then
        rows+=(" " "$menuOpt" "$desc")
      elif [ ${idxVal} -gt 0 ]; then
        rows+=("$DONE_FLAG" "$menuOpt" "$desc")
      elif [ ${idxVal} -lt 0 ]; then
        rows+=("   Skipped" "$menuOpt" "$desc")
      else
        rows+=(" " "$menuOpt" "$desc")
      fi
    else
      rows+=("$menuOpt" "$desc")
    fi

    local concatStr=$(printf "\n%s" "${rows[@]}")
    echo -e "${concatStr:1}" >> "$fileName"
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                   setDataForCategoryTabNB
# DESCRIPTION: Set the data for the new tab to be displayed within a yad "notebook"
#              dialog that lists the options for the selected option in 2nd to last tab.
#  Required Params: associative array "piAssocArray" with key/value pairs:
#      "descAssocArray" - name of the associative array to reference containing the
#                         descriptions of the values that can be selected
#      "optsArray"      - name of the array to reference containing the values that
#                         can be selected
#      "tabColumns"     - name of the column headers
#      "tabNum"         - number representing the tab
#---------------------------------------------------------------------------------------
setDataForCategoryTabNB() {
  local -n descAssocArray="${nbAssocArray["descAssocArray"]}"
  local -n optsArray="${nbAssocArray["optsArray"]}"
  local defaultVal="${nbAssocArray["defaultVal"]}"
  local fileName=$(printf "${NB_TAB_DATA_FNS["tabData"]}" ${nbAssocArray["tabNum"]})
  local stepTask=()
  local tabColumns=("${DEF_TAB_COLUMNS[@]}")

  if [ ${nbAssocArray["tabColumns"]+_} ]; then
    readarray -t tabColumns <<< "${nbAssocArray["tabColumns"]//$FORM_FLD_SEP/$'\n'}"
  fi

  for menuOpt in "${optsArray[@]}"; do
    local rows=()
    if [ "$menuOpt" == "$defaultVal" ]; then
      rows=(TRUE)
    else
      rows=(FALSE)
    fi

    local desc=$(escapeSpecialCharacters "${descAssocArray["$menuOpt"]}")

    rows+=("$menuOpt" "$desc")

    local concatStr=$(printf "\n%s" "${rows[@]}")
    echo -e "${concatStr:1}" >> "$fileName"
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                    setSelectedChklistOpt
# DESCRIPTION: Sets the option that was selected on the "Checklist" tab within the
#              global array variable "SEL_CHKLIST_OPT"
#  Required Params:
#      1)   nbKeyPlug - integer value for the notebook key & plug of its tabs
#      2) dialogTitle - String to be displayed at the top of the dialog box.
#      3) defaultVal - row to set radio button to checked
#---------------------------------------------------------------------------------------
setSelectedChklistOpt() {
  setSelectedTabOptionsFromNotebook 1 "${NB_TAB_DATA_FNS["nbCmd#01"]}"
  local tabKey=$(printf "$TAB_KEY_FMT" 1)
  local cols=()
  local sep="|"
  readarray -t cols <<< "${TAB_SEL_VALS["$tabKey"]//$sep/$'\n'}"
  local idx=-1
  unset cols[${idx}]
  SEL_CHKLIST_OPT=("${cols[-2]}" "${cols[-1]}")
}

#---------------------------------------------------------------------------------------
#      METHOD:                     showPostInstalOptsNB
# DESCRIPTION: Displays the list of options that was selected in the 2nd to last tab
#              a yad "notebook" dialog.
#  Required Params: associative array "piAssocArray" with key/value pairs:
#      "chklArray"      - name of the array to reference that is the checklist for the
#                         category/software task.
#      "defaultVal"     - the value to mark as checked within the list
#      "descAssocArray" - name of the associative array to reference containing the
#                         descriptions of the values that can be selected
#      "listType"       - "checklist" or "radiolist"
#      "nbButtons"      - name of the buttons to display in the notebook
#      "optsArray"      - name of the array to reference containing the values that
#                         can be selected
#      "tabColumns"     - name of the column headers
#      "tabName"        - name of the tab to create
#      "tabNum"         - number representing the tab
#      "tabText"        - text to display below the name of the tab when active
#---------------------------------------------------------------------------------------
showPostInstalOptsNB() {
  local tabName="${piAssocArray["tabName"]}"
  local tabNum=${piAssocArray["tabNum"]}
  local notebookCmds=()
  local -A helpOpts=(["buttons-layout"]="center" ["window-icon"]="gtk-help.png"
     ["center"]=true ["height"]=500 ["width"]=675 ["wrap"]=true ["fontname"]="Sans 12")

  if [ ${SOFT_CAT_IMAGES["$tabName"]+_} ]; then
    POST_INST_NB_OPTS["image"]="${IMG_ROOT}/${SOFT_CAT_IMAGES["$tabName"]}"
  fi

  helpOpts["title"]="${piAssocArray["helpTitle"]}"
  piAssocArray["nbKeyPlug"]=$(getSixDigitRandomNum)
  setCommandForSoftwareCategoryNB "piAssocArray"

  local nbCommand=$(printf " %s" "${notebookCmds[@]}")
  nbCommand=${nbCommand:1}

  while true; do
    clickedVals=$(showNotebookDialog "$nbCommand" " ")
    IFS=$',' read -d '' -a btnVals <<< "$clickedVals"
    case ${btnVals[0]} in
      ${DIALOG_OK})
        setSelValsFromTabs
        break
      ;;
      ${DIALOG_FORM})
        setSelValsFromTabs $tabNum
        break
      ;;
      ${DIALOG_SSKIP})
        mbSelVal=$(printf "%s%sSkip" "${SEL_CHKLIST_OPT[0]}" "$FORM_FLD_SEP")
        break
      ;;
      ${DIALOG_HELP})
        showTextInfoDialog "${piAssocArray["helpText"]}" helpOpts
      ;;
      *)
        TAB_ASSOC_ARRAY=()
        break
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:               setCommandForSoftwareCategoryNB
# DESCRIPTION: Sets the command to display the tabs a "notebook" dialog for the options
#              selected on the "Checklist" tab and the Category/Software task selected.
#  Required Params: associative array "piAssocArray" with key/value pairs:
#      "nbKeyPlug"   - integer value for the notebook key & plug of its tabs
#      "tabNum"         - number representing the tab
#---------------------------------------------------------------------------------------
setCommandForSoftwareCategoryNB() {
  local -n nbAssocArray="$1"
  local nbKeyPlug=${nbAssocArray["nbKeyPlug"]}
  local tabNum=${nbAssocArray["tabNum"]}
  local nbCmd=$(echo "$DIALOG --notebook --key=${nbKeyPlug} --active-tab=${tabNum}")
  local nbButtons=()
  local totNumTabs=$(cat "$POST_INST_YAD_TABS" | wc -l)
  local fileName=$(printf "${NB_TAB_DATA_FNS["tabData"]}" $tabNum)

  addExistingTabCmds ${nbKeyPlug}

  if [ ${tabNum} -gt ${totNumTabs} ]; then
    if [ ${nbAssocArray["chklArray"]+_} ]; then
      setDataForPostInstTabNB
    else
      setDataForCategoryTabNB
    fi
    local tabCmd=$(getCommandForTab "nbAssocArray")
    tabCmd=$(printf "$tabCmd" ${nbKeyPlug})
    notebookCmds+=("$tabCmd")
    nbCmd+=$(printf " --tab=\"%s\"" "${nbAssocArray["tabName"]}")
  elif [ ! -f "$fileName" ]; then
    if [ ${nbAssocArray["chklArray"]+_} ]; then
      setDataForPostInstTabNB
    else
      setDataForCategoryTabNB
    fi
  fi

  readarray -t nbButtons <<< "${nbAssocArray["nbButtons"]//$FORM_FLD_SEP/$'\n'}"

  nbCmd+=$(getOptionsForNotebook POST_INST_NB_OPTS)
  nbCmd+=$(getButtonsForDialog nbButtons)
  notebookCmds+=("$nbCmd")
}

#---------------------------------------------------------------------------------------
#      METHOD:                     addExistingTabCmds
# DESCRIPTION: Add the existing tabs (i.e. Checklist tab) to notebook array of commands
#              as well as copy the data files to the expected directory (i.e. /tmp).
#  Required Params:
#      1)   nbKeyPlug - integer value for the notebook key & plug of its tabs
#---------------------------------------------------------------------------------------
addExistingTabCmds() {
  local nbKeyPlug=$1
  local tabArray=()

  ls ${DATA_DIR}/software/*.data | while read file; do
    local fileName=`basename "$file"`
    cp -rf "$file" "/tmp/."
  done

  while IFS='' read -r line || [[ -n "$line" ]]; do
    readarray -t tabArray <<< "${line//$FORM_FLD_SEP/$'\n'}"
    local tabCmd=$(printf "${tabArray[1]}" ${nbKeyPlug})
    notebookCmds+=("$tabCmd")
    nbCmd+=$(printf " --tab=\"%s\"" "${tabArray[0]}")
  done < "$POST_INST_YAD_TABS"
}

#---------------------------------------------------------------------------------------
#      METHOD:                     setSelValsFromTabs
# DESCRIPTION: Sets the options that were selected in either all the tabs, or the one
#              that is the active/last tab.
#   Optional Param:
#    1) tabNum - integer value of the specific tab to get the options that were selected
#---------------------------------------------------------------------------------------
setSelValsFromTabs() {
  local lastTabNum=$(cat "$POST_INST_YAD_TABS" | wc -l)
  if [ "$#" -gt 0 ]; then
    setSelectedTabOptionsFromNotebook $1 "${NB_TAB_DATA_FNS["nbCmd#01"]}"
    fmtSelOptsForTab $1
  else
    for tabNum in $(eval echo "{1..${lastTabNum}}"); do
      setSelectedTabOptionsFromNotebook $tabNum "${NB_TAB_DATA_FNS["nbCmd#01"]}"
      fmtSelOptsForTab $tabNum
    done
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                      fmtSelOptsForTab
# DESCRIPTION: Formats each option selected into a concatenated string of:
#              "[Option Value]$FORM_FLD_SEP[Desc. of Option Value]\n"
#  Required Params:
#    1) tabNum - integer value of the specific tab to get the options that were selected
#---------------------------------------------------------------------------------------
fmtSelOptsForTab() {
  local tabNum=$1
  local tabKey=$(printf "$TAB_KEY_FMT" ${tabNum})
  local rows=()
  local cols=()
  local sep="|"
  local idx=-1
  local vals=()
  local concatStr=""
  local tab=$(sed -n ${tabNum}p "$POST_INST_YAD_TABS")
  readarray -t cols <<< "${tab//$FORM_FLD_SEP/$'\n'}"
  local tabName="${cols[0]}"

  readarray -t rows <<< "${TAB_SEL_VALS["$tabKey"]}"

  for row in "${rows[@]}"; do
    if [ ${#row} -gt 0 ]; then
      readarray -t cols <<< "${row//$sep/$'\n'}"
      unset cols[${idx}]
      concatStr=$(printf "%s%s%s" "${cols[-2]}" "$FORM_FLD_SEP" "${cols[-1]}")
      vals+=("$concatStr")
    fi
  done

  concatStr=$(printf "\n%s" "${vals[@]}")
  TAB_ASSOC_ARRAY["$tabName"]="${concatStr:1}"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                         getTabNames
# DESCRIPTION: Get names of all the tabs in the notebook
#      RETURN: concatenated string separated by '\n'
#---------------------------------------------------------------------------------------
function getTabNames() {
  local tabArray=()
  local tabNames=()

  while IFS='' read -r line || [[ -n "$line" ]]; do
    readarray -t tabArray <<< "${line//$FORM_FLD_SEP/$'\n'}"
    tabNames+=("${tabArray[0]}")
  done < "$POST_INST_YAD_TABS"

  local concatStr=$(printf "\n%s" "${tabNames[@]}")
  echo "${concatStr:1}"
}

#---------------------------------------------------------------------------------------
#      METHOD:                        dispErrMsgDlg
# DESCRIPTION: Displays the appropriate error message
#  Required Params:
#    1) errMsg - the error message to be displayed in the text area
#---------------------------------------------------------------------------------------
dispErrMsgDlg() {
  local -A yadOpts=(["buttons-layout"]="center" ["image"]="$YAD_GRAPHICAL_PATH/images/error-icon.png"
    ["image-on-top"]=true ["title"]="${POST_INST_NB_OPTS["title"]}" ["window-icon"]="error-window-icon.png"
    ["text"]="<span font_weight='bold' font_size='large'>ERROR!!!!!!!</span>" ["text-align"]="left"
    ["height"]=425 ["width"]=600 ["wrap"]=true ["fontname"]="Sans 12" ["center"]=true ["wrap"]=true ["size"]="fit")

  local dlgBtns=("Exit")

  showTextInfoDialog "$1" yadOpts dlgBtns
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                      getTabCmdForList
# DESCRIPTION: Get the command for a tab to be display either a "checklist" or
#              "radiolist" within a yad "notebook" dialog.
#      RETURN: concatenated string
#  Required Params:
#      1) tabCmdAssocArray - the name of the associative array with the required
#                            key/value pairs:
#      "defaultChoice"  - the value within the list to be auto checked
#      "listType"       - "checklist" or "radiolist"
#      "menuOptRef"     - name of the array containing the menu options
#      "optDescsRef"    - name of the associative array containing the description
#                         of the menu options
#      "tabName"        - name of the tab to create
#      "tabNum"         - number representing the tab
#      "tabText"        - text to display below the name of the tab when active
#      "tabListColHdrs" - name of the array containing the column header names
#---------------------------------------------------------------------------------------
function getTabCmdForList() {
  local -n tabCmdAssocArray="$1"
  local defaultChoice=""
  local tabName=$(escapeSpecialCharacters "${tabCmdAssocArray["tabName"]}")
  local tabNum=${tabCmdAssocArray["tabNum"]}
  local tabText=$(escapeSpecialCharacters "${tabCmdAssocArray["tabText"]}")
  local listType="${tabCmdAssocArray["listType"]}"
  local -A tabOpts=(["grid-lines"]="both" ["list-type"]="$listType" ["notebook"]=true
                    ["text"]="<span font_weight='bold' font_size='medium'>%60s%s</span>"
                    ["text-align"]="left")
  local tabOutput=$(echo " < \"%s\" &> \"%s\" 2> /dev/null &")
  local -n tabListColHdrs="${tabCmdAssocArray["tabListColHdrs"]}"
  local tabCmd=$(echo "$DIALOG --plug=%d --tabnum=${tabNum}")
  local cmdArray=("$tabCmd")
  local tabDataFN=$(printf "${NB_TAB_DATA_FNS["tabData"]}" "$tabNum")
  local outputFN=$(printf "${NB_TAB_DATA_FNS["nbCmd#01"]}" "$tabNum")

  tabOpts["text"]=$(printf "${tabOpts["text"]}" " " "$tabText")

  local keys=("wrap-cols" "wrap-width")
  for key in "${keys[@]}"; do
    if [ ${tabCmdAssocArray["$key"]+_} ]; then
      tabOpts["$key"]="${tabCmdAssocArray["$key"]}"
    fi
  done

  tabCmd=$(getOptionsForListDialog tabOpts)
  cmdArray+=("$tabCmd")

  tabCmd=$(getColumnsForListDialog tabListColHdrs)
  cmdArray+=("$tabCmd")
  tabCmd=$(printf "$tabOutput" "$tabDataFN" "$outputFN")
  cmdArray+=("$tabCmd")

  tabCmd=$(printf " %s" "${cmdArray[@]}")
  tabCmd=${tabCmd:1}

  if [ ${tabCmdAssocArray["defaultChoice"]+_} ]; then
    defaultChoice="${tabCmdAssocArray["defaultChoice"]}"
  fi

  setDataForListTab "${tabCmdAssocArray["menuOptRef"]}" "${tabCmdAssocArray["optDescsRef"]}" "$defaultChoice" "$tabDataFN"

  echo "$tabCmd"
}

#---------------------------------------------------------------------------------------
#      METHOD:                      setDataForListTab
# DESCRIPTION: Set the data to be displayed within a tab for yad "notebook" dialog that
#              uses a widget of type list
#  Required Params:
#      1)    menuOptRef - name of the array containing the menu options
#      2)   optDescsRef - name of the associative array containing the description
#      3) defaultChoice - the value within the list to be auto checked
#      4)     tabDataFN - name of the data file for the tab
#---------------------------------------------------------------------------------------
setDataForListTab() {
  local -n menuOptRef="$1"
  local -n optDescsRef="$2"
  local defaultChoice="$3"
  local tabDataFN="$4"
  local tabFileData=()

  for opt in "${menuOptRef[@]}"; do
    if [ "$opt" == "$defaultChoice" ]; then
      tabFileData+=(TRUE)
    else
      tabFileData+=(FALSE)
    fi
    tabFileData+=("$opt")
    local colDesc=$(escapeSpecialCharacters "${optDescsRef["$opt"]}")
    tabFileData+=("$colDesc")
  done

  local lines=$(printf "\n%s" "${tabFileData[@]}")
  lines=${lines:1}

  echo -e "$lines" > "$tabDataFN"
}

#---------------------------------------------------------------------------------------
#      METHOD:                   saveSubmitValsFromTabs
# DESCRIPTION: Save the values that were submitted within each tab of the yad "notebook"
#              dialog within the global associative array variable TAB_ASSOC_ARRAY
#  Required Params:
#      1) lastTabNum - integer value of last tab for the notebook
#   Optional Param:
#    2) tabNum - integer value of the specific tab to get the options that were submitted
#---------------------------------------------------------------------------------------
saveSubmitValsFromTabs() {
  local lastTabNum=$1
  if [ "$#" -gt 1 ]; then
    setSelectedTabOptionsFromNotebook $2 "${NB_TAB_DATA_FNS["nbCmd#01"]}"
    fmtSelOptsForTab $2
  else
    for tabNum in $(eval echo "{1..${lastTabNum}}"); do
      setSelectedTabOptionsFromNotebook $tabNum "${NB_TAB_DATA_FNS["nbCmd#01"]}"
      fmtSelOptsForTab $tabNum
    done
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getCmdToShowPostInstNB
# DESCRIPTION: Get the command to display a yad "notebook" dialog
#      RETURN: concatenated string
#  Required Params:
#      1) methodParams - the name of the associative array with the required
#                        key/value pairs:
#      "nbImage"     - full path to image to be displayed before the tabs
#      "nbKeyPlug"   - integer value for the notebook key & plug of its tabs
#      "numTabs"     - total number of tabs
#      "windowTitle" - String to be displayed at the top of the dialog window.
#
#      For each of the tabs:
#      "tabName%02d"      - name of the tab
#      "defChoiceTab%02d" - default choice
#      "menuOptsTab%02d"  - name of the array containing the menu options
#      "optDescsTab%02d"  - name of the associative array containing the description
#                           of the menu options
#      "textTab%02d"      - Text to be displayed before the list of menu options
#      "listType%02d"     - "checklist" or "radiolist"
#      "colsTab%02d"      - names of the columns separated by $FORM_FLD_SEP
#---------------------------------------------------------------------------------------
function getCmdToShowPostInstNB() {
  local -n methodParams="$1"
  local nbKeyPlug=${methodParams["nbKeyPlug"]}
  local numTabs=${methodParams["numTabs"]}
  local nbCmd=$(echo "$DIALOG --notebook --key=${nbKeyPlug}")
  local tabCmd=""
  local tabName=""
  local -A yadTabOpts=(["grid-lines"]="both" ["no-selection"]=true ["notebook"]=true )
  local nbButtons=("Ok" "Skip" "Help")
  local -A nbOpts=(["height"]=570 ["width"]=610 ["center"]=true ["buttons-layout"]="center"
                   ["image"]="${methodParams["nbImage"]}"
                   ["image-on-top"]=true
                   ["title"]="${methodParams["windowTitle"]}" ["window-icon"]="app-icon.png")
  local listColHdrs=()
  local -A nbTabArray=( ["tabListColHdrs"]="listColHdrs")
  local nbTabCmds=()

  if [ ${methodParams["nbButtons"]+_} ]; then
    readarray -t nbButtons <<< "${methodParams["nbButtons"]//$FORM_FLD_SEP/$'\n'}"
  fi
  if [ ${methodParams["height"]+_} ]; then
    nbOpts["height"]=${methodParams["height"]}
  fi

  for tabNum in $(eval echo "{1..${numTabs}}"); do
    local tabNameKey=$(printf "tabName%02d" ${tabNum})
    local tabName="${methodParams["$tabNameKey"]}"
    local defChoiceKey=$(printf "defChoiceTab%02d" ${tabNum})
    local menuOptRefKey=$(printf "menuOptsTab%02d" ${tabNum})
    local optDescsRefKey=$(printf "optDescsTab%02d" ${tabNum})
    local tabTextKey=$(printf "textTab%02d" ${tabNum})
    local listTypeKey=$(printf "listType%02d" ${tabNum})
    local colsTabKey=$(printf "colsTab%02d" ${tabNum})

    nbTabArray["tabNum"]=${tabNum}
    nbTabArray["defaultChoice"]="${methodParams["$defChoiceKey"]}"
    nbTabArray["menuOptRef"]="${methodParams["$menuOptRefKey"]}"
    nbTabArray["optDescsRef"]="${methodParams["$optDescsRefKey"]}"
    nbTabArray["tabText"]="${methodParams["$tabTextKey"]}"
    nbTabArray["listType"]="${methodParams["$listTypeKey"]}"
    readarray -t listColHdrs <<< "${methodParams["$colsTabKey"]//$FORM_FLD_SEP/$'\n'}"

    case "$tabName" in
      "Alternative"|"POSIX compliant")
        nbTabArray["wrap-cols"]=3
        nbTabArray["wrap-width"]=400
      ;;
    esac

    tabCmd=$(getTabCmdForList "nbTabArray")
    tabCmd=$(printf "$tabCmd" ${nbKeyPlug})

    nbTabCmds+=("$tabCmd")
    nbCmd+=$(printf " --tab=\"%s\"" "$tabName")
  done

  nbCmd+=$(getOptionsForNotebook nbOpts)
  nbCmd+=$(getButtonsForDialog nbButtons)
  nbTabCmds+=("$nbCmd")

  local nbCommand=$(printf " %s" "${nbTabCmds[@]}")
  echo "${nbCommand:1}"
}

#---------------------------------------------------------------------------------------
#      METHOD:                     setDataFilesForTabs
# DESCRIPTION: Set the data files for each of the tabs to be displayed in the
#              yad "notebook" dialog
#  Required Params:
#      1) numTabFiles - total numbed of data files
#      2)      prefix - value of the menu option that will be appended by the
#                       name of the data file
#---------------------------------------------------------------------------------------
setDataFilesForTabs() {
  local numTabFiles=$1

  for tabNum in $(eval echo "{1..${numTabFiles}}"); do
    local destFile=$(printf "${NB_TAB_DATA_FNS["tabData"]}" "$tabNum")
    local srcFile=$(printf "/tmp/%s-%s" "$2" "${destFile:5}")
    cp -rf "$srcFile" "$destFile"
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                    cleanDataFilesForTabs
# DESCRIPTION: Remove the data files
#  Required Params:
#      1) arrayRef - name of the array containing the menu options that prefixes the
#                    file names
#---------------------------------------------------------------------------------------
cleanDataFilesForTabs() {
  local -n arrayRef="$1"
  local aryLen=${#arrayRef[@]}

  for opt in "${arrayRef[@]}"; do
    for tabNum in $(eval echo "{1..${aryLen}}"); do
      local destFile=$(printf "${NB_TAB_DATA_FNS["tabData"]}" "$tabNum")
      local srcFile=$(printf "/tmp/%s-%s" "$opt" "${destFile:5}")
      rm -rf "$srcFile" "$destFile"
    done
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                 showWarningDialog
# DESCRIPTION:  Show the warning message within a yad "text-info" dialog
#  Required Params:
#      1) dialogTitle - String to be displayed at the top of the dialog window.
#      2)     warnMsg - the warning message/text to display
#---------------------------------------------------------------------------------------
showWarningDialog() {
  local -A yadOpts=(["buttons-layout"]="center" ["image"]="$YAD_GRAPHICAL_PATH/images/warning-icon.png"
    ["image-on-top"]=true ["title"]="Warning:  $1" ["window-icon"]="warning-window-icon.png"
    ["text"]="<span font_weight='bold' font_size='large'>WARNING!!!!!!!</span>" ["text-align"]="left"
    ["height"]=325 ["width"]=550 ["wrap"]=true ["fontname"]="Sans 12" ["center"]=true ["wrap"]=true ["size"]="fit")

  local dlgBtns=("Exit")

  showTextInfoDialog "$2" yadOpts dlgBtns
}

#---------------------------------------------------------------------------------------
#      METHOD:                     setNextTaskForStep
# DESCRIPTION: Sets the next task to be done and its description for the current step
#              within the global variable "mbSelVal".
#---------------------------------------------------------------------------------------
setNextTaskForStep() {
  mbSelVal=""
  TAB_ASSOC_ARRAY=()
  showPostInstalOptsNB
  cleanDialogFiles

  if [ ${#TAB_ASSOC_ARRAY[@]} -gt 0 ]; then
    local tabVals=()
    local tabNames=()
    local concatStr=$(getTabNames)

    readarray -t tabNames <<< "$concatStr"
    for tabName in "${tabNames[@]}"; do
      case "$tabName" in
        "Checklist")
          if [ ${TAB_ASSOC_ARRAY["$tabName"]+_} ]; then
            readarray -t tabVals <<< "${TAB_ASSOC_ARRAY["$tabName"]//$FORM_FLD_SEP/$'\n'}"

            if [ "${tabVals[0]}" != "${SEL_CHKLIST_OPT[0]}" ]; then
              local isDone=$(isReqStepsDone)
              if ${isDone}; then
                loopFlag=$(echo 'false' && return 1)
                mbSelVal="exit"
              else
                dispErrMsgDlg "${errMsg:1}"
                cleanDialogFiles
              fi
              break
            fi
          fi
        ;;
        "Basic Setup"|"Desktop Configuration")
          readarray -t tabVals <<< "${TAB_ASSOC_ARRAY["$tabName"]//$FORM_FLD_SEP/$'\n'}"
          mbSelVal=$(printf "%s%s%s" "${tabVals[0]}" "$FORM_FLD_SEP" "${tabVals[1]}")
          break
        ;;
        *)
          readarray -t tabVals <<< "${TAB_ASSOC_ARRAY["$tabName"]//$FORM_FLD_SEP/$'\n'}"
          mbSelVal=$(printf "%s%s%s" "${tabVals[0]}" "$FORM_FLD_SEP" "${tabVals[1]}")
          break
        ;;
      esac
    done
  else
    loopFlag=$(echo 'false' && return 1)
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                     overrideDialogOpts
# DESCRIPTION: Override the default options set for a yad "radiolist" dialog.
#  Required Params:
#      1) optAssocArray - the name of the associative array with the optional
#                         key/value pairs:
#           "dialogText"    - Text to display inside the dialog before
#                             the list of menu options
#           "height"        - the height to set the window to
#           "helpHeight"    - the height to set the help dialog to
#           "helpWidth"     - the width to set the help dialog to
#           "image"         - the image to display above the radio list
#           "width"         - the width to set the window to
#           "window-icon"   - the icon for the window/dialog
#---------------------------------------------------------------------------------------
overrideDialogOpts() {
  local -n optAssocArray="$1"
  local keyNames=("dialogText" "height" "helpHeight" "helpWidth" "image" "list-type"
                  "sel-line" "width" "window-icon" "wrap-cols" "wrap-width")
  for key in "${keyNames[@]}"; do
    if [ ${optAssocArray["$key"]+_} ]; then
      case "$key" in
        "dialogText")
          optAssocArray["$key"]=$(escapeSpecialCharacters "${optAssocArray["$key"]}")
          dlgOpts["text"]="<span font_weight='bold' font_size='medium'>${optAssocArray["$key"]}</span>"
          dlgOpts["text-align"]="left"
        ;;
        "helpHeight")
          helpDlgOpts["height"]="${optAssocArray["$key"]}"
        ;;
        "helpWidth")
          helpDlgOpts["width"]="${optAssocArray["$key"]}"
        ;;
        "sel-line")
          unset dlgOpts["no-selection"]
        ;;
        *)
          dlgOpts["$key"]="${optAssocArray["$key"]}"
        ;;
      esac
    fi
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                   showMenuForConfirmation
# DESCRIPTION: Show a yad "list" dialog with the AUR package names & descriptions that
#              will be installed.
#  Required Params:
#      1) assocParamArray - the name of the associative array with the required
#                           key/value pairs:
#      "dialogTitle"   - String to be displayed at the top of the dialog window.
#      "helpDlgTitle"  - String to be displayed at the top of the HELP dialog window.
#      "helpText"      - Text to display on the help/more dialog
#      "image"         - the image to display above the radio list
#      "menuOptsRef"   - name of the array containing the menu options
#      "optDescsRef"   - name of the associative array containing the description
#                        of the menu options
#           Optional:
#             "height"        - the height to set the window to
#             "helpHeight"    - the height to set the help dialog to
#             "helpWidth"     - the width to set the help dialog to
#             "width"         - the width to set the window to
#             "window-icon"   - the icon for the window/dialog
#      2) dlgButtons     - the name of the array for the buttons
#      3) dlgColumns     - the name of the array for the column headers
#---------------------------------------------------------------------------------------
showMenuForConfirmation() {
  local -n assocParamArray="$1"
  local -n dlgButtons="$2"
  local -n dlgColumns="$3"
  local -n menuOptsRef="${assocParamArray["menuOptsRef"]}"
  local -n optDescsRef="${assocParamArray["optDescsRef"]}"
  local helpText="${assocParamArray["helpText"]}"
  local -A dlgOpts=(["width"]=650 ["center"]=true ["buttons-layout"]="center" ["image"]="${assocParamArray["image"]}"
                    ["image-on-top"]=true ["window-icon"]="app-icon.png" ["title"]="${assocParamArray["dialogTitle"]}"
                    ["no-selection"]=true)
  local -A helpOpts=(["buttons-layout"]="center" ["window-icon"]="gtk-help.png"
                     ["title"]="Help:  ${assocParamArray["helpDlgTitle"]}" ["center"]=true
                     ["height"]=400 ["width"]=650 ["wrap"]=true ["fontname"]="Sans 12")
  local dlgData=()
  local dataFileName=$(printf " < \"%s\" 2> /dev/null" "$DATA_FILE_NAME")

  overrideDialogOpts "$1"

  for opt in "${menuOptsRef[@]}"; do
    dlgData+=("$opt")
    dlgData+=("${optDescsRef[$opt]}")
  done

  local concatStr=$(printf "\n%s" "${dlgData[@]}")
  echo -e "${concatStr:1}" > "$DATA_FILE_NAME"

  local yadCmd=$(echo "$DIALOG ")
  yadCmd+=$(getOptionsForListDialog dlgOpts)
  yadCmd+=$(getButtonsForDialog dlgButtons)
  yadCmd+=$(getColumnsForListDialog dlgColumns)
  yadCmd+="$dataFileName"

  while true; do
    eval "$yadCmd"
    btnClick=$?

    case ${btnClick} in
      ${DIALOG_OK})
        yesNoFlag=$(echo 'true' && return 0)
        break
      ;;
      ${DIALOG_CANCEL}|${DIALOG_ESC})
        yesNoFlag=$(echo 'false' && return 1)
        break
      ;;
      ${DIALOG_HELP})
        showTextInfoDialog "${assocParamArray["helpText"]}" helpOpts
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                  selectOptionFromRadioList
# DESCRIPTION: Select an option from a yad "radiolist" dialog.  Set the value selected
#              within the global variable "mbSelVal".
#  Required Params:
#      1) assocParamArray - the name of the associative array with the required
#                           key/value pairs:
#      "dialogTitle"   - String to be displayed at the top of the dialog window.
#      "helpText"      - Text to display on the help/more dialog
#      "menuOptsRef"   - name of the array containing the menu options
#      "optDescsRef"   - name of the associative array containing the description
#                        of the menu options
#           Optional:
#             "defaultChoice" - the default value to set as checked/selected
#             "height"        - the height to set the window to
#             "helpHeight"    - the height to set the help dialog to
#             "helpWidth"     - the width to set the help dialog to
#             "image"         - the image to display above the radio list
#             "width"         - the width to set the window to
#             "window-icon"   - the icon for the window/dialog
#      2) dlgButtons     - the name of the array for the buttons
#      3) dlgColumns     - the name of the array for the column headers
#      4) dialogText     - Text to display inside the dialog before the list of menu options
#---------------------------------------------------------------------------------------
selectOptionFromRadioList() {
  local -n assocParamArray="$1"
  local -n dlgButtons="$2"
  local -n dlgColumns="$3"
  local dialogText="$4"
  local -n menuOptsRef="${assocParamArray["menuOptsRef"]}"
  local -n optDescsRef="${assocParamArray["optDescsRef"]}"
  local dialogTitle="${assocParamArray["dialogTitle"]}"
  local helpText="${assocParamArray["helpText"]}"
  local -A dlgOpts=(["height"]=270 ["width"]=625 ["center"]=true ["buttons-layout"]="center"
                    ["image"]="$YAD_GRAPHICAL_PATH/images/root.png"
                    ["image-on-top"]=true ["window-icon"]="app-icon.png")
  local -A helpDlgOpts=(["width"]=610 ["title"]="Help:  $dialogTitle")
  local defaultChoice="${assocParamArray["defaultChoice"]}"
  local dlgData=()

  overrideDialogOpts "$1"
  dlgOpts["ret-separator"]="true"

  appendOptionsForRadiolist "$dialogText" "$dialogTitle" dlgOpts
  setDataForMultiColList menuOptsRef optDescsRef dlgData "$defaultChoice"

  mbSelVal=$(getValueFromRadiolist "$helpText" dlgOpts dlgButtons dlgColumns dlgData helpDlgOpts)
}

#---------------------------------------------------------------------------------------
#      METHOD:                   showSkipBtnConfWarning
# DESCRIPTION: Get confirmation to of whether or not to continue with skipping the.
#              remaining tasks that are either NOT done or skipped.
#      1) paramAssocArray - the name of the associative array with the required
#                           key/value pairs:
#      "dialogTitle"   - String to be displayed at the top of the dialog window.
#      "dialogText"    - Warning message to be displayed in the text area of the dialog
#      "dialogTextHdr" - String to be displayed at the top of the text area.
#---------------------------------------------------------------------------------------
showSkipBtnConfWarning() {
  local -n paramAssocArray="$1"
  local hdr="${paramAssocArray["dialogTextHdr"]}"
  local -A yadOpts=(["buttons-layout"]="center" ["image"]="$YAD_GRAPHICAL_PATH/images/warning-icon.png"
    ["image-on-top"]=true ["title"]="${paramAssocArray["dialogTitle"]}" ["window-icon"]="warning-window-icon.png"
    ["text"]="<span font_weight='bold' font_size='large'>WARNING!!!!!!!\n\n\n${hdr}</span>" ["text-align"]="left"
    ["height"]=450 ["width"]=650 ["wrap"]=true ["fontname"]="Sans 12" ["center"]=true ["wrap"]=true ["size"]="fit")

  local dlgBtns=("Continue" "Cancel")

  showTextInfoDialog "${paramAssocArray["dialogText"]}" yadOpts dlgBtns
  btnClick=$?

  case ${btnClick} in
    ${DIALOG_LOCAL})
      yesNoFlag=$(echo 'true' && return 0)
    ;;
    ${DIALOG_CANCEL}|${DIALOG_ESC})
      yesNoFlag=$(echo 'false' && return 1)
    ;;
  esac
}

#---------------------------------------------------------------------------------------
#      METHOD:                   showSoftwareOptsForCat
# DESCRIPTION: Wrapper to display the list of sub-categories and/or AUR software
#              packages within a yad "notebook" dialog.  Set the selected value(s) within
#              the global variable "mbSelVal".
#  Required Params:
#    1) assocArray - the name of the associative array with the required key/value pairs
#    2)     optSel - the option that was selected
#    3)    optDesc - the description/name of the option that was selected
#---------------------------------------------------------------------------------------
showSoftwareOptsForCat() {
  local -n assocArray="$1"
  local errorTextArray=("There were NO applications/AUR packages selected within the '$3' tab.  Click"
    "the \"Skip\" button if you don't want to install any packages within this.  Otherwise,"
    "select the from list of options in order to commit them to be installed!")
  local errorMsg=$(printf " %s" "${errorTextArray[@]}")

  while true; do
    showOptsforCatNB "assocArray" "$2"
    sed -i '$d' "$POST_INST_YAD_TABS"

    if [ ${#TAB_ASSOC_ARRAY[@]} -gt 0 ]; then
      mbSelVal=$(getSelectedTabValues "$2" "$3")
      if [ ${#mbSelVal} -gt 0 ]; then
        break
      else
        dispErrMsgDlg "${errorMsg:1}"
        cleanDialogFiles
      fi
    else  #### Cancel or Skip
      break
    fi
  done
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getSelectedTabValues
# DESCRIPTION: Get a concatenated string of the value(s) selected within a tab based on
#              their order of precedence.  For example:
#                  Tab #1:  Checklist
#                  Tab #2:  Accessory Software
#                  Tab #3:  Calendars
#                  If the selected value on the Checklist is not Accessory Software, then
#                     return the selected Software Category task
#                  Otherwise, if the selected value on Accessory Software is not
#                  Calendars, then
#                     return the selected Category
#                  Otherwise,
#                     return the selected AUR packages for Calendars to install
#      RETURN: concatenated string or empty string
#  Required Params:
#    1)  optSel - the option that was selected
#    2) optDesc - the description/name of the option that was selected
#---------------------------------------------------------------------------------------
function getSelectedTabValues() {
  local chkListOpt=$(escapeSpecialCharacters "${SEL_CHKLIST_OPT[1]}")
  local catOpt=$(escapeSpecialCharacters "$2")
  local tabNames=("Checklist" "$chkListOpt" "$catOpt")
  local concatStr=""
  local subCatRegEx=^[0-9]+\.[0-9]+\.

  if [ ${#TAB_ASSOC_ARRAY[@]} -gt 0 ]; then
    for tabName in "${tabNames[@]}"; do
      if [ ${TAB_ASSOC_ARRAY["$tabName"]+_} ]; then
        local tabVals=()
        readarray -t tabVals <<< "${TAB_ASSOC_ARRAY["$tabName"]//$FORM_FLD_SEP/$'\n'}"
        case "$tabName" in
          "Checklist")
            if [ "${tabVals[0]}" != "${SEL_CHKLIST_OPT[0]}" ]; then
              concatStr=$(printf "%s%s%s" "${tabVals[0]}" "$FORM_FLD_SEP" "${tabVals[1]}")
              break
            fi
          ;;
          "$chkListOpt")
            if [ "${tabVals[0]}" != "$1" ]; then
              concatStr=$(printf "%s%s%s" "${tabVals[0]}" "$FORM_FLD_SEP" "${tabVals[1]}")
              break
            fi
          ;;
          *)
            local rows=()
            local splitArray=()
            readarray -t rows <<< "${TAB_ASSOC_ARRAY["$tabName"]}"
            if [ ${#rows[@]} -gt 1 ]; then
              tabVals=()
              for row in "${rows[@]}"; do
                readarray -t splitArray <<< "${row//$FORM_FLD_SEP/$'\n'}"
                tabVals+=("${splitArray[0]}")
              done
              concatStr=$(printf "|%s" "${tabVals[@]}")
            else
              local row="${rows[0]}"
              readarray -t splitArray <<< "${row//$FORM_FLD_SEP/$'\n'}"
              if [[ ${splitArray[0]} =~ $subCatRegEx ]]; then
                #### Selected a sub-category
                concatStr=$(printf "%s%s%s" "${splitArray[0]}" "$FORM_FLD_SEP" "${splitArray[1]}")
              else
                concatStr="|${splitArray[0]}"
              fi
            fi
            break
          ;;
        esac
      fi
    done
  fi

  echo "$concatStr"
}

#---------------------------------------------------------------------------------------
#      METHOD:                      showOptsforCatNB
# DESCRIPTION: Displays the list of sub-categories and/or AUR software packages that can
#              selected based on the radio button clicked on the 2nd to last tab
#              a yad "notebook" dialog.  Set the selected value(s) within the
#              global variable "mbSelVal".
#  Required Params:
#    1) paramAssocArray - the name of the associative array with the required
#                           key/value pairs:
#      "chklArray"      - name of the array to reference that is the checklist for the
#                         category/software task.
#      "defaultVal"     - the value to mark as checked within the list
#      "descAssocArray" - name of the associative array to reference containing the
#                         descriptions of the values that can be selected
#      "listType"       - "checklist" or "radiolist"
#      "nbButtons"      - name of the buttons to display in the notebook
#      "optsArray"      - name of the array to reference containing the values that
#                         can be selected
#      "tabColumns"     - name of the column headers
#      "tabName"        - name of the tab to create
#      "tabNum"         - number representing the tab
#      "tabText"        - text to display below the name of the tab when active
#    2) optSel - the option that was selected
#---------------------------------------------------------------------------------------
showOptsforCatNB() {
  local -n paramAssocArray="$1"
  local tabName="${paramAssocArray["tabName"]}"
  local tabNum=${paramAssocArray["tabNum"]}
  local notebookCmds=()
  local -A helpOpts=(["buttons-layout"]="center" ["window-icon"]="gtk-help.png"
     ["center"]=true ["height"]=500 ["width"]=675 ["wrap"]=true ["fontname"]="Sans 12")

  if [ ${SOFT_CAT_IMAGES["$tabName"]+_} ]; then
    POST_INST_NB_OPTS["image"]="${IMG_ROOT}/${SOFT_CAT_IMAGES["$tabName"]}"
  fi

  helpOpts["title"]="${paramAssocArray["helpTitle"]}"
  paramAssocArray["nbKeyPlug"]=$(getSixDigitRandomNum)
  setCommandForSoftwareCategoryNB "paramAssocArray"

  local nbCommand=$(printf " %s" "${notebookCmds[@]}")
  nbCommand=${nbCommand:1}

  while true; do
    clickedVals=$(showNotebookDialog "$nbCommand" " ")
    IFS=$',' read -d '' -a btnVals <<< "$clickedVals"
    case ${btnVals[0]} in
      ${DIALOG_OK})
        setSelValsFromCatTabs
        break
      ;;
      ${DIALOG_FORM})
        setSelValsFromCatTabs $tabNum
        break
      ;;
      ${DIALOG_SSKIP})
        mbSelVal=$(printf "%s%sSkip" "$2" "$FORM_FLD_SEP")
        TAB_ASSOC_ARRAY=()
        break
      ;;
      ${DIALOG_HELP})
        showTextInfoDialog "${paramAssocArray["helpText"]}" helpOpts
      ;;
      *)
        mbSelVal=""
        TAB_ASSOC_ARRAY=()
        break
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                     setSelValsFromCatTabs
# DESCRIPTION: Sets the options that were selected in either all the tabs, or the one
#              that is the active/last tab.
#   Optional Param:
#    1) tabNum - integer value of the specific tab to get the options that were selected
#---------------------------------------------------------------------------------------
setSelValsFromCatTabs() {
  local lastTabNum=$(cat "$POST_INST_YAD_TABS" | wc -l)
  TAB_SEL_VALS=()
  TAB_ASSOC_ARRAY=()

  if [ "$#" -gt 0 ]; then
    setSelectedTabOptsFromNB $1 "${NB_TAB_DATA_FNS["nbCmd#01"]}"
    fmtSelOptsForTab $1
  else
    for tabNum in $(eval echo "{1..${lastTabNum}}"); do
      setSelectedTabOptsFromNB ${tabNum} "${NB_TAB_DATA_FNS["nbCmd#01"]}"
      fmtSelOptsForTab ${tabNum}
    done
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  setSelectedTabOptsFromNB
# DESCRIPTION: Set the options that were selected for a tab of a yad "notebook" dialog.
#  Required Params:
#      1)          tabNum - total number of tabs
#      2) dataFileNameFmt - format of the data file name that will hold the
#                           selected and/or entered values
#---------------------------------------------------------------------------------------
setSelectedTabOptsFromNB() {
  local tabNum=$1
  local dataFileNameFmt="$2"
  local dataFN=$(printf "$dataFileNameFmt" ${tabNum})
  local tabKey=$(printf "$TAB_KEY_FMT" ${tabNum})

  TAB_SEL_VALS["$tabKey"]=$(cat "$dataFN")
}

#---------------------------------------------------------------------------------------
#      METHOD:                      showSoftwareOptsForSubCat
# DESCRIPTION: Displays a list of sub-categories within the last tab of a yad
#              "notebook" dialog.  Set the selected value(s) within the
#              global variable "mbSelVal".
#  Required Params:
#    1) paramAssocArray - the name of the associative array with the required
#                           key/value pairs:
#      "chklArray"      - name of the array to reference that is the checklist for the
#                         category/software task.
#      "defaultVal"     - the value to mark as checked within the list
#      "descAssocArray" - name of the associative array to reference containing the
#                         descriptions of the values that can be selected
#      "listType"       - "checklist" or "radiolist"
#      "nbButtons"      - name of the buttons to display in the notebook
#      "optsArray"      - name of the array to reference containing the values that
#                         can be selected
#      "tabColumns"     - name of the column headers
#      "tabName"        - name of the tab to create
#      "tabNum"         - number representing the tab
#      "tabText"        - text to display below the name of the tab when active
#    2) optSel - the option that was selected
#    3) optDesc - the description/name of the option that was selected
#---------------------------------------------------------------------------------------
showSoftwareOptsForSubCat() {
  local -n paramAssocArray="$1"
  local catTabNum=${paramAssocArray["tabNum"]}
  local tabName="${paramAssocArray["tabName"]}"
  local notebookCmds=()
  local -A helpOpts=(["buttons-layout"]="center" ["window-icon"]="gtk-help.png"
     ["center"]=true ["height"]=500 ["width"]=675 ["wrap"]=true ["fontname"]="Sans 12")
  local -A catAssocArray=()
  local -A savedTabVals=()
  local -n subCatDescs="${paramAssocArray["descAssocArray"]}"
  local errorTextArray=("There were NO AUR packages selected from any of the sub-categories.  Click"
    "the \"Skip\" button if you don't want to install any packages from the sub-categories.  Otherwise,"
    "select the packages from the categories in order to commit them to be installed!")
  local errorMsg=$(printf " %s" "${errorTextArray[@]}")

  for key in "${!subCatDescs[@]}"; do
    local desc="${subCatDescs["$key"]}"
    catAssocArray["$key"]="${CATEGORY_MENU_OPTS["$desc"]}"
  done

  helpOpts["title"]="${paramAssocArray["helpTitle"]}"
  local nbKeyPlug=$(getSixDigitRandomNum)
  paramAssocArray["nbKeyPlug"]=${nbKeyPlug}
  setCommandForSoftwareCategoryNB "paramAssocArray"

  local nbCommand=$(printf " %s" "${notebookCmds[@]}")
  nbCommand=${nbCommand:1}

  while true; do
    clickedVals=$(showNotebookDialog "$nbCommand" " ")
    IFS=$',' read -d '' -a btnVals <<< "$clickedVals"
    case ${btnVals[0]} in
      ${DIALOG_OK})
        setSelValsFromCatTabs
      ;;
      ${DIALOG_FORM})
        setSelValsFromCatTabs ${catTabNum}
      ;;
      ${DIALOG_COMMIT})
        if [ ${#savedTabVals[@]} -gt 0 ]; then
          local aurPckgs=()
          for key in "${!savedTabVals[@]}"; do
            aurPckgs+=("${savedTabVals["$key"]}")
          done
          mbSelVal=$(printf "|%s" "${aurPckgs[@]}")
          break
        else
          dispErrMsgDlg "${errorMsg:1}"
        fi
      ;;
      ${DIALOG_SSKIP})
        mbSelVal=$(printf "%s%sSkip" "$2" "$FORM_FLD_SEP")
        TAB_ASSOC_ARRAY=()
        break
      ;;
      ${DIALOG_HELP})
        showTextInfoDialog "${paramAssocArray["helpText"]}" helpOpts
      ;;
      *)
        mbSelVal=""
        TAB_ASSOC_ARRAY=()
        break
      ;;
    esac
    local concatStr=$(getSelectedTabValues "$2" "$3")
    local strSplitArray=()
    readarray -t strSplitArray <<< "${concatStr//$FORM_FLD_SEP/$'\n'}"
    local firstElem="${strSplitArray[0]}"
    if [ ${#firstElem} -gt 0 ]; then
      if [ ${catAssocArray["$firstElem"]+_} ]; then
        processTabDataForSubCat "$firstElem"
        cleanDialogFiles
        showSubCatOptsDialog "${catAssocArray["$firstElem"]}" "$firstElem" "${strSplitArray[1]}" "$2 - $3"
        cleanDialogFiles

        cp -rf ${DATA_DIR}/software/YAD-* /tmp/.
      else
        mbSelVal="$concatStr"
        break
      fi
    fi
  done

  processTabDataForSubCat " " 1
}

#---------------------------------------------------------------------------------------
#      METHOD:                    saveTabDataForSubCat
# DESCRIPTION: Save or remove the data file for the YAD tab.
#  Required Params:
#    1)  optSel - the option that was selected
#   Optional Param:
#    2) flag to remove the data file that was saved
#---------------------------------------------------------------------------------------
processTabDataForSubCat() {
  local optSel="$1"
  local srcFile=$(printf "${NB_TAB_DATA_FNS["tabData"]}" 3)
  local path=()
  local sep="/"

  readarray -t path <<< "${srcFile//$sep/$'\n'}"
  path[1]="${DATA_DIR}/software"
  local destFile=$(printf "/%s" "${path[@]:1}")
  destFile=${destFile:1}

  if [ "$#" -gt 1 ]; then
    rm -rf "$destFile"
  else
    setDataForPostInstTabNB
    mv "$srcFile" "$destFile"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                    showSubCatOptsDialog
# DESCRIPTION: Displays a list of the AUR Packages for a sub-category within a yad
#              dialog of either type "checklist" or "radiolist".
#  Required Params:
#    1)    menuOpts - concatenated string of options to display separated by "|"
#    2)   subCatOpt - the sub-category option that was selected
#    3)  subCatDesc - the description of the sub-category option that was selected
#    4) dialogTitle - String to be displayed at the top of the dialog window.
#---------------------------------------------------------------------------------------
showSubCatOptsDialog() {
  local subCatOpt="$2"
  local subCatDesc="$3"
  local title=$(printf "%22s%s" " " "$2 - $3")
  local dlgButtons=("Save" "Cancel" "Help")
  local dlgColumns=("Checkbox" "AUR Package Name" "Description")
  local subCatMenuOpts=()
  local dlgData=()
  local sep="|"
  local -A optsForHelp=(["buttons-layout"]="center" ["window-icon"]="gtk-help.png"
     ["center"]=true ["wrap"]=true ["fontname"]="Sans 12" ["title"]="Help:  $2 - $3")
  local -A dlgOpts=(["height"]=500 ["width"]=655 ["center"]=true ["buttons-layout"]="center"
                    ["image-on-top"]=true ["window-icon"]="app-icon.png" ["ret-separator"]="true"
                    ["wrap-cols"]="2,3" ["wrap-width"]=225)

  readarray -t subCatMenuOpts <<< "${1//$sep/$'\n'}"
  setOptsForSubCatDialog "$2" "$3" "${subCatMenuOpts[0]}"

  local helpText="${optsForHelp["helpText"]}"
  unset optsForHelp["helpText"]

  local textArray=("$title" " " " " " " " " "${dlgOpts["dialogText"]}")
  local dialogText=$(printf "\n%s" "${textArray[@]}")
  unset dlgOpts["dialogText"]

  local defaultChoice="${dlgOpts["defaultVal"]}"
  unset dlgOpts["defaultVal"]

  appendOptionsForRadiolist "${dialogText:1}" "$4" dlgOpts
  setDataForMultiColList subCatMenuOpts AUR_PCKG_DESCS dlgData "$defaultChoice"

  local retVal=$(getValueFromRadiolist "$helpText" dlgOpts dlgButtons dlgColumns dlgData optsForHelp)
  if [ ${#retVal} -gt 0 ]; then
    saveSelectedPackages "$subCatOpt" "$subCatDesc" "$retVal"
  else
    unset savedTabVals["$2"]
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                   setOptsForSubCatDialog
# DESCRIPTION: Set the data for yad dialog of either type "checklist" or "radiolist"
#              to display the options for a sub-category.
#  Required Params:
#    1)  subCatOpt - the sub-category option that was selected
#    2) subCatDesc - the description of the sub-category option that was selected
#    2)     defVal - the default value
#---------------------------------------------------------------------------------------
setOptsForSubCatDialog() {
  local subCatOpt="$1"
  local subCatDesc="$2"
  local defVal="$3"

  case "$subCatDesc" in
    "Eclipse")
      dlgColumns[0]="Radio Btn"
      if [ ${savedTabVals["$subCatOpt"]+_} ]; then
        dlgOpts["defaultVal"]="${savedTabVals["$subCatOpt"]}"
      else
        dlgOpts["defaultVal"]="$defVal"
      fi
      dlgOpts["image"]="${YAD_GRAPHICAL_PATH}/images/post-inst/${subCatDesc,,}.png"
      dlgOpts["list-type"]="radiolist"
      dlgOpts["dialogText"]="Click the \"Save\" button to save the selected AUR package of \"Eclipse\"!"
      optsForHelp["helpText"]=$(getHelpTextForEclipse)
      optsForHelp["height"]=550
      optsForHelp["width"]=675
    ;;
    "IntelliJ")
      if [ ${savedTabVals["$subCatOpt"]+_} ]; then
        dlgOpts["defaultVal"]="${savedTabVals["$subCatOpt"]}"
      else
        dlgOpts["defaultVal"]="$defVal"
      fi
      dlgColumns[0]="Radio Btn"
      dlgOpts["defaultVal"]="$defVal"
      dlgOpts["image"]="${YAD_GRAPHICAL_PATH}/images/post-inst/${subCatDesc,,}.png"
      dlgOpts["list-type"]="radiolist"
      dlgOpts["dialogText"]="Click the \"Save\" button to save the selected AUR package of \"IntelliJ\"!"
      optsForHelp["helpText"]=$(getHelpTextForIntelliJ)
      optsForHelp["height"]=550
      optsForHelp["width"]=675
    ;;
    *)
      if [ ${savedTabVals["$subCatOpt"]+_} ]; then
        dlgOpts["defaultVal"]="${savedTabVals["$subCatOpt"]}"
      else
        dlgOpts["defaultVal"]="none"
      fi
      dlgOpts["image"]="${YAD_GRAPHICAL_PATH}/images/post-inst/sub-category.png"
      dlgOpts["list-type"]="checklist"
      dlgOpts["dialogText"]="Click the \"Save\" button to save the selected AUR packages of the sub-category!"
      optsForHelp["helpText"]=$(getHelpTextForSubCatBtns)
      optsForHelp["height"]=300
      optsForHelp["width"]=400
    ;;
  esac
}

#---------------------------------------------------------------------------------------
#      METHOD:                    saveSelectedPackages
# DESCRIPTION: Save the AUR package names that were selected within the tab for the
#              sub-categories.
#  Required Params:
#    1)  subCatOpt - the sub-category option that was selected
#    2) subCatDesc - the description of the sub-category option that was selected
#    3)    selVals - concatenated string of values selected separated by a '\n'
#---------------------------------------------------------------------------------------
saveSelectedPackages() {
  local subCatOpt="$1"
  local subCatDesc="$2"
  local selTabVals=()
  local rows=()
  local nameDesc=()
  local sep="|"

  readarray -t rows <<< "$3"
  if [ ${#rows[@]} -gt 0 ] && [ ${#rows[0]} -gt 0 ]; then
    for row in "${rows[@]}"; do
      readarray -t nameDesc <<< "${row//$sep/$'\n'}"
      selTabVals+=("${nameDesc[1]}")
    done

    local concatStr=$(printf "|%s" "${selTabVals[@]}")
    savedTabVals["$subCatOpt"]="${concatStr:1}"
  else
    unset savedTabVals["$subCatOpt"]
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                     selectConkyPackage
# DESCRIPTION: Select the option to install the Conky package using a yad "radiolist"
#              dialog.  Set the value within the global variable "mbSelVal".
#  Required Params:
#      1) paramAssocArray - the name of the associative array with the required
#                           key/value pairs:
#      "dialogTitle"   - String to be displayed at the top of the dialog window.
#      "dialogText"    - Text to display inside the dialog before the list of menu options
#      "dialogTextHdr" - String to be displayed at the top of the text area.
#      "helpText"      - Text to display on the help/more dialog
#      "menuOptsRef"   - name of the array containing the menu options
#      "optDescsRef"   - name of the associative array containing the description
#                        of the menu options
#---------------------------------------------------------------------------------------
selectConkyPackage() {
  local -n paramAssocArray="$1"
  local buttons=("Ok" "Help")
  local cols=("Radio Btn" "Package Name" "Description")
  local title=$(printf "%32s%s" " " "${paramAssocArray["dialogTextHdr"]}")
  local textArray=("$title" " " " " " " " " "${paramAssocArray["dialogText"]}")
  local sep="|"

  paramAssocArray["height"]=500
  paramAssocArray["width"]=650
  paramAssocArray["wrap-cols"]=3
  paramAssocArray["wrap-width"]=400
  paramAssocArray["image"]="$YAD_GRAPHICAL_PATH/images/post-inst/conky.png"
  paramAssocArray["dialogText"]=$(printf "\n%s" "${textArray[@]}")
  selectOptionFromRadioList "$1" "buttons" "cols" "$dialogText"

  readarray -t cols <<< "${mbSelVal//$sep/$'\n'}"
  mbSelVal="${cols[1]}"
}
