#!/bin/bash
#set -e

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# functions/methods
#===============================================================================

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getKeyboardLayout
# DESCRIPTION: Get the keyboard configuration/layout.  Set the select keyboard layout in
#              the global variable "mbSelVal".
#  Required Params:
#      1)   dialogTitle - String to be displayed at the top of the dialog window.
#      2) dialogTextHdr - String to be displayed at the top of the text area.
#      3) defaultLayout - the default keyboard layout
#      4)   defCtryCode - the default country code based on the geographical location
#---------------------------------------------------------------------------------------
getKeyboardLayout() {
  local defCtryCode="$4"
  local helpText=""
  local helpArray=()
  local kblDlgData=(" " " " " " " ")
  local concatStr=""
  local numDialogs=4
  local dialogNum=0
  local idx=1
  local yadCmd=""
  local yadCmdDlg2=""
  local yadCmds=(" " " " " ")
  local ctryData=()
  local selData=()
  local -A helpOpts=(["buttons-layout"]="center" ["window-icon"]="gtk-help.png" ["title"]="$2"
    ["center"]=true ["height"]=400 ["width"]=610 ["wrap"]=true ["fontname"]="Sans 12")

  for dialogNum in $(eval echo "{1..${numDialogs}}"); do
    setCmdForDialogs ${dialogNum} "$2" "$3" "$1"
    setDataForDialog ${dialogNum} "$defCtryCode" "$3"
  done

  yadCmdDlg2="${yadCmds[$idx]}"

  dialogNum=4

  while true; do
    idx=$(expr ${dialogNum} - 1)
    concatStr="${kblDlgData[$idx]}"
    echo "$concatStr" > "$DATA_FILE_NAME"
    helpText="${helpArray[$idx]}"
    yadCmd="${yadCmds[$idx]}"
    yadChoices=$(eval "$yadCmd")
    btnClick=$?

    case ${btnClick} in
      ${DIALOG_OK})
        case ${dialogNum} in
          1)
            readarray -t ctryData <<< "${yadChoices//|/$'\n'}"
            dialogNum=4
            setDataForConfDialog ${dialogNum} "${ctryData[1]}"
            concatStr=$(trimString "${kblDlgData[3]}")
            if [ ${#concatStr} -gt 0 ]; then
              selData=(TRUE "#1" "$mbSelVal")
            else
              dialogNum=2
              setOptionsForCountry ${dialogNum} "${ctryData[1]}" "$3"
              setImageAndText ${dialogNum} "${ctryData[1]}" "${ctryData[2]}" "$yadCmdDlg2"
            fi
          ;;
          2|3)
            readarray -t selData <<< "${yadChoices//|/$'\n'}"
            dialogNum=4
            setDataForConfDialog ${dialogNum} " " "${selData[2]}"
          ;;
          4)
            if [ ${#selData[@]} -gt 0 ]; then
              mbSelVal=$(trimString "${selData[2]}")
            else
              mbSelVal="$3"
            fi
            break
          ;;
        esac
      ;;
      ${DIALOG_CANCEL})
        dialogNum=1
        ctryData=()
        selData=()
      ;;
      ${DIALOG_EXIT})
        mbSelVal="$3"
        break
      ;;
      ${DIALOG_BROWSER}) ### Other button
        dialogNum=3
      ;;
      ${DIALOG_HELP})
        showTextInfoDialog "$helpText" helpOpts
      ;;
      ${DIALOG_ESC})
        if [ ${dialogNum} -gt 1 ]; then
          dialogNum=1
          ctryData=()
          selData=()
        else
          mbSelVal="$3"
          break
        fi
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                      setCmdForDialogs
# DESCRIPTION: Set the YAD commands for the dialog to be displayed:
#                 #1)  Countries that have keyboard layouts related to them
#                 #2)  Keyboard layouts related to the country chosen
#                 #3)  Keyboard layouts NOT related to a particular country
#                 #4)  Confirmation of Keyboard Layout Selection
#  Required Params:
#      1)     dialogNum - the number for the dialog to display
#      2) dialogTextHdr - String to be displayed at the top of the text area.
#      3) defaultLayout - the default keyboard layout
#      4)   dialogTitle - String to be displayed at the top of the dialog window.
#---------------------------------------------------------------------------------------
setCmdForDialogs() {
  local dialogNum=$1
  local dialogText=$(getDialogTextForKeyboardLayout ${dialogNum} "$2" "$3")
  local idx=$(expr ${dialogNum} - 1)
  local yadCmd=""

  case ${dialogNum} in
    1)
      yadCmd=$(getCmdKeyboardLayoutDlg "$4" "$dialogText" "world-map")
    ;;
    2)
      yadCmd=$(getCmdKeyboardLayoutDlg "$4" "$dialogText" "flags")
    ;;
    3) ### Other dialog
      yadCmd=$(getCmdKeyboardLayoutDlg "$4" "$dialogText" "keyboard-logo")
    ;;
    4)
      yadCmd=$(getCmdKeyboardLayoutDlg "$4" "Confirmation of Keyboard Layout Selection" "question")
    ;;
  esac

  yadCmds[$idx]="$yadCmd"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getDialogTextForKeyboard
# DESCRIPTION: Get the text to display within a yad "dialog" for choosing a keyboard layout
#      RETURN: Concatenated string
#  Required Params:
#      1)     dialogNum - the number for the dialog to display
#      2) dialogTextHdr - String to be displayed at the top of the text area.
#      3) defaultLayout - the default keyboard layout
#---------------------------------------------------------------------------------------
function getDialogTextForKeyboardLayout() {
  local dialogNum=$1
  local title=$(printf "%12s%s" " " "$2")
  local textArray=("$title" " " " "
    "The Default Keyboard Layout is:  [$3]"
    " " " ")
  case ${dialogNum} in
    1)
      textArray+=("Select the country that is related to a keyboard layout:")
    ;;
    2)
      textArray+=("Select the keyboard layout that is related to the country \"%s\":")
    ;;
    3) ### Other dialog
      textArray+=("Select the keyboard layout:")
    ;;
  esac

  local dialogText=$(printf "\n%s" "${textArray[@]}")
  dialogText=$(escapeSpecialCharacters "${dialogText:1}")

  echo "$dialogText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getCmdKeyboardLayoutDlg
# DESCRIPTION: Get the command to show a yad "dialog" box of either type "radiolist"
#              or "text-info".
#      RETURN: concatenated string
#  Required Params:
#      1) dialogTitle - String to be displayed at the top of the dialog window.
#      2)  dialogText - Text to be displayed in the text area of the dialog
#      3) nameOfImage - prefix to the name of the image to display
#---------------------------------------------------------------------------------------
function getCmdKeyboardLayoutDlg() {
  local nameOfImage="$3"
  local imagePath=$(printf "%s/images/%s.png" "$YAD_GRAPHICAL_PATH" "$nameOfImage")
  local dataFileName=$(printf " < \"%s\" 2> /dev/null" "$DATA_FILE_NAME")
  local dlgButtons=("Ok" "Other" "Exit" "Help")
  local dlgColumns=("Radio Btn" "Country Code" "Name of Country")
  local -A dlgOpts=(["buttons-layout"]="center" ["image"]="$imagePath" ["grid-lines"]="both"
    ["image-on-top"]=true ["title"]="$1" ["window-icon"]="config-system.png" ["list-type"]="radiolist"
    ["text"]="<span font_weight='bold' font_size='medium'>$2</span>" ["text-align"]="left"
    ["height"]=550 ["width"]=675 ["fontname"]="Sans 12" ["center"]=true ["wrap"]=true ["size"]="fit")
  local yadCmd=$(echo "$DIALOG ")

  case "$nameOfImage" in
    "flags")
      imagePath=$(printf "%s/images/%s/" "$YAD_GRAPHICAL_PATH" "$nameOfImage")
      imagePath+="%s.png"
      dlgOpts["image"]="$imagePath"
      dlgButtons[2]="Cancel"
      dlgColumns=("Radio Btn" "Option" "Keyboard Layout")
    ;;
    "keyboard-logo")
      dlgButtons=("Ok" "Cancel" "Help")
      dlgColumns=("Radio Btn" "Option" "Keyboard Layout")
    ;;
    "question") ### Other dialog
      dlgButtons=("Yes" "No" "Help")
      unset dlgOpts["list-type"]
      unset dlgOpts["grid-lines"]
      dlgOpts["height"]=350
      dlgOpts["width"]=475
      dlgOpts["window-icon"]="question-icon.png"
      local textInfoOpts=$(getOptionsForTextInfo dlgOpts " ")
      textInfoOpts+=$(getButtonsForDialog dlgButtons)
      yadCmd=$(echo "$DIALOG $textInfoOpts --text-info < $DATA_FILE_NAME 2> /dev/null")
    ;;
  esac

  if [ ${dlgOpts["list-type"]+_} ]; then
    yadCmd+=$(getOptionsForListDialog dlgOpts)
    yadCmd+=$(getButtonsForDialog dlgButtons)
    yadCmd+=$(getColumnsForListDialog dlgColumns)
    yadCmd+="$dataFileName"
  fi

  echo "$yadCmd"
}

#---------------------------------------------------------------------------------------
#      METHOD:                      setDataForDialog
# DESCRIPTION: Set the help and the menu options for the dialog to be displayed:
#                 #1)  Countries that have keyboard layouts related to them
#                 #2)  Keyboard layouts related to the country chosen
#                 #3)  Keyboard layouts NOT related to a particular country
#                 #4)  Confirmation of Keyboard Layout Selection
#  Required Params:
#      1)     dialogNum - the number for the dialog to display
#      2)   defCtryCode - the default country code based on the geographical location
#      3) defaultLayout - the default keyboard layout
#---------------------------------------------------------------------------------------
setDataForDialog() {
  local dialogNum=$1
  local concatStr=""
  local helpText=""
  local dlgData=()
  local -A kblCtryNames=()
  local idx=$(expr ${dialogNum} - 1)

  case ${dialogNum} in
    1)
      for ctryCode in "${KBL_ISO_CODES[@]}"; do
        kblCtryNames["$ctryCode"]="${ISO_CODE_NAMES["$ctryCode"]}"
      done
      setDataForMultiColList KBL_ISO_CODES kblCtryNames dlgData "$defCtryCode"
      concatStr=$(printf "\n%s" "${dlgData[@]}")
      kblDlgData[${idx}]="${concatStr:1}"
    ;;
    2)
      setOptionsForCountry ${dialogNum} "$2" "$3"
    ;;
    3) ### Other dialog
      setOptionsForOtherDlg ${dialogNum}
    ;;
    4)
      setDataForConfDialog ${dialogNum} " " "$3"
    ;;
  esac

  helpText=$(getHelpTextForKeyboard "$3" ${dialogNum})
  helpArray+=("$helpText")

}

#---------------------------------------------------------------------------------------
#      METHOD:                    setOptionsForCountry
# DESCRIPTION: Set the menu options for the dialog to choose the keyboard layout related
#              to the country.
#  Required Params:
#      1)     dialogNum - the number for the dialog to display
#      2)      ctryCode - country code
#      3) defaultLayout - the default keyboard layout
#---------------------------------------------------------------------------------------
setOptionsForCountry() {
  local dialogNum=$1
  local ctryCode=$(trimString "$2")
  local concatStr="${kblMap["$ctryCode"]}"
  local idx=$(expr ${dialogNum} - 1)
  local kblOptions=()
  local -A kbls=()
  local kblData=()
  local optNum=0
  local key=""
  local defChoice=""

  concatStr="${concatStr:1}"

  readarray -t kblData <<< "${concatStr//;/$'\n'}"
  for kbl in "${kblData[@]}"; do
    optNum=$(expr ${optNum} + 1)
    key=$(printf "#%d" ${optNum})
    kblOptions+=("$key")
    kbls["$key"]="$kbl"
    if [ "$kbl" == "$3" ]; then
      defChoice="$key"
    fi
  done

  kblData=()

  if [ ${#defChoice} -gt 0 ]; then
    setDataForMultiColList kblOptions kbls kblData "$defChoice"
  else
    setDataForMultiColList kblOptions kbls kblData
  fi

  concatStr=$(printf "\n%s" "${kblData[@]}")
  kblDlgData[${idx}]="${concatStr:1}"
}

#---------------------------------------------------------------------------------------
#      METHOD:                    setOptionsForOtherDlg
# DESCRIPTION: Set the menu options for the dialog to choose the keyboard layout that is
#              NOT related to any particular country or language.
#  Required Params:
#      1) dialogNum - the number for the dialog to display
#---------------------------------------------------------------------------------------
setOptionsForOtherDlg() {
  local dialogNum=$1
  local idx=$(expr ${dialogNum} - 1)
  local kblOptions=()
  local -A kbls=()
  local kblData=()
  local optNum=0
  local key=""

  for kbl in "${kblOther[@]}"; do
    optNum=$(expr ${optNum} + 1)
    key=$(printf "#%d" ${optNum})
    kblOptions+=("$key")
    kbls["$key"]="$kbl"
  done

  setDataForMultiColList kblOptions kbls kblData

  concatStr=$(printf "\n%s" "${kblData[@]}")
  kblDlgData[${idx}]="${concatStr:1}"
}

#---------------------------------------------------------------------------------------
#      METHOD:                    setDataForConfDialog
# DESCRIPTION: Set the text to be displayed in the confirmation dialog.
#  Required Params:
#      1) dialogNum - the number for the dialog to display
#      2)  ctryCode - country code
#  Optional Params:
#      3)    selKBL - the keyboard layout that was selected
#---------------------------------------------------------------------------------------
setDataForConfDialog() {
  local dialogNum=$1
  local selKBL=""
  local idx=$(expr ${dialogNum} - 1)

  if [ "$#" -gt 2 ]; then
    selKBL=$(trimString "$3")
  else
    local kblData=()
    local concatStr="${kblMap["$2"]}"
    concatStr="${concatStr:1}"
    readarray -t kblData <<< "${concatStr//;/$'\n'}"
    if [ ${#kblData[@]} -lt 2 ]; then
      selKBL="${kblData[0]}"
      mbSelVal="$selKBL"
    fi
  fi

  if [ ${#selKBL} -gt 0 ]; then
    kblDlgData[${idx}]=$(printf "Confirm to set the Keyboard Layout to:  [%s]" "$selKBL")
  else
    kblDlgData[${idx}]=""
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                       setImageAndText
# DESCRIPTION: Set the prefix of the image to be displayed and the name of the country
#              in the text area of the dialog.
#  Required Params:
#      1) dialogNum - the number for the dialog to display
#      2)  ctryCode - country code
#      3)  ctryName - full name of the country
#      4)    yadCmd - command to display the dialog
#---------------------------------------------------------------------------------------
setImageAndText() {
  local dialogNum=$1
  local ctryCode=$(trimString "$2")
  local ctryName=$(trimString "$3")
  local yadCmd="$4"
  local idx=$(expr ${dialogNum} - 1)

  yadCmds[${idx}]=$(printf "$yadCmd" "$ctryCode" "$ctryName")
}
