#!/bin/bash
#set -e

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# functions/methods
#===============================================================================

#---------------------------------------------------------------------------------------
#      METHOD:                getPasswordsFromFormDialog
# DESCRIPTION: Uses a yad "form" dialog to get the password and verification.
#  Required Params:
#      1) dialogTextHdr - Text inside the dialog displayed at the very top.
#      2)   dialogTitle - String to be displayed at the top of the dialog box.
#      3)        urlRef - the URL that can be referenced for further documentation
#                         from the help dialog
#   Optional Param:
#      4)    dialogText - text to display in the text area of the dialog above the menu
#---------------------------------------------------------------------------------------
getPasswordsFromFormDialog() {
  local helpText=$(getHelpTextForPswdKB "$3")
  local -A helpOpts=(["buttons-layout"]="center" ["window-icon"]="gtk-help.png" ["title"]="Help:  $2"
  ["center"]=true ["height"]=350 ["width"]=675 ["wrap"]=true ["fontname"]="Sans 12")
  local formVals=""
  local cmd=$(getCommandForDialog "$@")

  while true; do
    formVals=$(eval "$cmd")
    btnClick=$?

    case ${btnClick} in
      ${DIALOG_OK})
        local validFormVals=$(validateFormFields "$formVals")
        if ${validFormVals}; then
          local formFields=()
          readarray -t formFields <<< "${formVals//$FORM_FLD_SEP/$'\n'}"
          pswdVals=("${formFields[@]}")
          break
        fi
      ;;
      ${DIALOG_CANCEL}|${DIALOG_ESC})
        pswdVals=()
        break
      ;;
      ${DIALOG_HELP})
        showTextInfoDialog "$helpText" helpOpts
      ;;
    esac
  done

}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getDialogTextForPswdForm
# DESCRIPTION: Get the text to display in a yad "form" dialog
#      RETURN: concatenated string
#  Required Params:
#      1) dialogTextHdr - Text inside the dialog displayed at the very top.
#---------------------------------------------------------------------------------------
function getDialogTextForPswdForm() {
  local textArray=()
  local actionTxt=""
  if [ "$#" -gt 1 ]; then
    textArray=("                                                      $1")
    textArray+=(" "
      "The passwords were generated with the AUR package &quot;$2&quot;" " "
      "Click the &quot;Help&quot; button for the options that were passed to the generator!")
    actionTxt="Choose the password"
  else
    textArray=("                         $1")
    textArray+=(" "
      "The password must be between 8 - 20 characters in length!" " "
      "Click the &quot;Help&quot; button for password suggestions!")
    actionTxt="Enter a password"
  fi
  textArray+=(" "
    "$actionTxt to encrypt &amp; open the LUKS encrypted container:"
  )
  local dialogText=$(getTextForDialog "${textArray[@]}")
  echo "$dialogText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     getCommandForDialog
# DESCRIPTION: Get the command to display a yad "form" dialog
#      RETURN: concatenated string
#  Required Params:
#      1) dialogTextHdr - Text inside the dialog displayed at the very top.
#      2)   dialogTitle - String to be displayed at the top of the dialog box.
#   Optional Param:
#      4)    dialogText - text to display in the text area of the dialog above the menu
#---------------------------------------------------------------------------------------
function getCommandForDialog() {
  local dialogTextHdr="$1"
  local dialogButtons=("Ok" "Cancel" "Help")
  local -A yadOpts=(["align"]="right" ["buttons-layout"]="center" ["center"]=true ["title"]="$dialogTitle"
    ["text-align"]="left" ["height"]=350 ["width"]=560
    ["image-on-top"]=true ["window-icon"]="password-icon.png" ["separator"]="\"$FORM_FLD_SEP\"")
  local formFields=("\"Password:\":H" "\"Retype Password:\":H")
  local dialogText=""
  local imageName="keyboard-logo.png"

  if [ "$#" -gt 3 ]; then
    if [[ "$2" =~ "ROOT" ]]; then
      local title=$(printf "%22s%s" " " "$2")
      local textArray=("$title" " " " " " " " " "$4")
      dialogText=$(printf "\n%s" "${textArray[@]}")
      dialogText=$(escapeSpecialCharacters "$dialogText")
      imageName="root.png"
    elif [[ "$2" =~ "Samba" ]]; then
      imageName="post-inst/samba-user.png"
      yadOpts["height"]=450
      yadOpts["width"]=500
      yadOpts["align"]="center"
      formFields=("\"<b>Enter Group Name, User Name &amp; Password:</b>\":LBL \"blank\""
        "\"Group Name:\":STE \"sambashare\"" "\"User Name:\":STE \"${POST_ASSOC_ARRAY["UserName"]}\""
        "${formFields[@]}")
    else
      imageName="post-inst/user-pswd.png"
      yadOpts["height"]=400
      yadOpts["width"]=500
      yadOpts["align"]="center"
      formFields=("\"<b>Enter User Name &amp; Password:</b>\":LBL" "\"User Name:\":STE" "${formFields[@]}")
    fi
    yadOpts["title"]="$dialogTextHdr"
  else
    dialogText=$(getDialogTextForPswdForm "$dialogTextHdr")
  fi

  yadOpts["image"]="$YAD_GRAPHICAL_PATH/images/$imageName"
  if [ ${#dialogText} -gt 0 ]; then
    yadOpts["text"]="<span font_weight='bold' font_size='medium'>$dialogText</span>"
  fi

  local cmd=$(getCommandForFormDialog yadOpts formFields dialogButtons)
  echo "$cmd"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     validateFormFields
# DESCRIPTION: Validate that the values entered in the form are valid
#      RETURN: 0 - true, 1 -false
#---------------------------------------------------------------------------------------
function validateFormFields() {
  local formVals="$1"
  local formFields=()
  local groupName=""
  local idxVals=(0 -1)
  local pswd1=""
  local pswd2=""
  local userName=""
  local validFormVals=$(echo 'true' && return 0)

  readarray -t formFields <<< "${formVals//$FORM_FLD_SEP/$'\n'}"
  for idx in "${idxVals[@]}"; do
    if [ ${#formFields[$idx]} -lt 1  ]; then
      unset formFields[$idx]
    fi
  done

  if [ ${#formFields[@]} -gt 3 ]; then
    groupName="${formFields[1]}"
    validFormVals=$(isValidGroupName "$groupName")
    if ${validFormVals}; then
      userName="${formFields[2]}"
      validFormVals=$(isValidUserName "$userName" "${POST_ASSOC_ARRAY["UserName"]}")
      pswd1="${formFields[3]}"
      pswd2="${formFields[4]}"
    fi
  elif [ ${#formFields[@]} -gt 2 ]; then
    userName="${formFields[1]}"
    validFormVals=$(isValidUserName "$userName")
    pswd1="${formFields[2]}"
    pswd2="${formFields[3]}"
  else
    pswd1="${formFields[0]}"
    pswd2="${formFields[1]}"
  fi

  if ${validFormVals}; then
    validFormVals=$(isValidPswd "$pswd1" "$pswd2")
    if ! ${validFormVals}; then
      dispInvPswdErrorMsg "$pswd1"
    fi
  else
    if [ ${#userName} -gt 0 ]; then
      dispInvUserNameMsg "$userName"
    else
      dispInvGroupNameMsg "$groupName"
    fi
  fi

  echo ${validFormVals}
}

#---------------------------------------------------------------------------------------
#      METHOD:                     dispInvPswdErrorMsg
# DESCRIPTION: Displays the appropriate error message when either:
#              A) the password entered is NOT between 8-20 characters
#              B) does NOT match the verified the value.
#  Required Params:
#      1) enteredPswd - the password that was entered the first time
#
#        NOTE:  The password dialog will have a maximum character limit of 20
#---------------------------------------------------------------------------------------
dispInvPswdErrorMsg() {
  local enteredPswd="$1"
  local dialogTitle="Error:  Invalid Password"
  local errorMsg="The passwords that were entered DO NOT match!"
  if [ ${#enteredPswd} -lt 8 ]; then
    errorMsg="The password must be between 8 - 20 characters in length!"
  fi
  local textArray=( "$BORDER_WIDTH_600" "$errorMsg" "$BORDER_WIDTH_600" " "
  "You must re-enter the password"
  )
  local dialogText=$(getTextForDialog "${textArray[@]}")
  local -A yadOpts=(["buttons-layout"]="center" ["image"]="$YAD_GRAPHICAL_PATH/images/error-icon.png"
    ["image-on-top"]=true ["title"]="$dialogTitle" ["window-icon"]="error-window-icon.png"
    ["text"]="<span font_weight='bold' font_size='large'>ERROR!!!!!!!</span>" ["text-align"]="left"
    ["height"]=300 ["width"]=600 ["wrap"]=true ["fontname"]="Sans 12" ["center"]=true ["wrap"]=true ["size"]="fit")

  local dlgBtns=("Exit")

  showTextInfoDialog "$dialogText" yadOpts dlgBtns
}

#---------------------------------------------------------------------------------------
#      METHOD:                     dispInvUserNameMsg
# DESCRIPTION: Displays the appropriate error message when either:
#              A) The username already exists
#              B) Fails the criteria specified in the function "isValidUserName"
#  Required Params:
#      1) userName - the name of the user to be created
#---------------------------------------------------------------------------------------
dispInvUserNameMsg() {
  local userName="$1"
  local outFile="/tmp/findUser.out"
  local errorMsg="The username \"$userName\""
  local dialogTitle="Error:  Invalid Username"
  local textArray=()
  local -A yadOpts=(["buttons-layout"]="center" ["image"]="$YAD_GRAPHICAL_PATH/images/error-icon.png"
    ["image-on-top"]=true ["title"]="$dialogTitle" ["window-icon"]="error-window-icon.png"
    ["text"]="<span font_weight='bold' font_size='large'>ERROR!!!!!!!</span>" ["text-align"]="left"
    ["height"]=300 ["width"]=600 ["wrap"]=true ["fontname"]="Sans 12" ["center"]=true ["wrap"]=true ["size"]="fit")
  local dlgBtns=("Exit")
  local dashBorder="$BORDER_WIDTH_600"

  if [ -f "$outFile" ]; then
    errorMsg+=" already exists!"
    rm -rf "$outFile"
  else
    errorMsg+=" does NOT meet the installer's criteria!"
    textArray=("$errorMsg" " ")
    errorMsg=$(getTextForUserName)
    textArray+=("$errorMsg")
    errorMsg=$(getTextForDialog "${textArray[@]}")
    yadOpts["height"]=465
    yadOpts["width"]=675
    dashBorder="${BORDER_WIDTH_600}-------------"
  fi

  textArray=("$dashBorder" "$errorMsg" "$dashBorder" " "
    "You must re-enter the username!")
  local dialogText=$(getTextForDialog "${textArray[@]}")

  showTextInfoDialog "$dialogText" yadOpts dlgBtns
}

#---------------------------------------------------------------------------------------
#      METHOD:                     getSelAutoGenPswd
# DESCRIPTION: Get the password for the LUKS encrypted container from one of the
#              automated password generation tools and set it in the global variable "mbSelVal"
#  Required Params:
#      1) dialogTextHdr - Text inside the dialog displayed at the very top.
#      2)        urlRef - the URL that can be referenced for further documentation
#                         from the help dialog
#      3)       aurPckg - name of the AUR password generator package
#---------------------------------------------------------------------------------------
getSelAutoGenPswd() {
  local dialogTextHdr="$1"
  local urlRef="$2"
  local aurPckg="$3"
  local autoGenPswds=()
  local dlgButtons=("Ok" "Cancel" "Help")
  local dialogTitle="Automated Generated Passwords w/ '$aurPckg'"
  local dialogText=$(getDialogTextForPswdForm "$dialogTextHdr" "$aurPckg")
  local helpText=$(getHelpTextForAutoPswd "$aurPckg" "$urlRef")
  local dlgColumns=("Radio Btn" "Option" "Automated Generated Passwords")
  local -A dlgOpts=(["height"]=500 ["width"]=655 ["image"]="$YAD_GRAPHICAL_PATH/images/${aurPckg}-logo.png"
    ["image-on-top"]=true ["window-icon"]="password-icon.png")
  local -A helpDlgOpts=(["width"]=500 ["title"]="Help:  $dialogTitle")
  local dlgData=()
  local agpChoices=()
  local -A agpChoiceDescs=()

  case "$aurPckg" in
    "apg")
      cmdOutput=$(execAPG)
    ;;
    "pwgen")
      cmdOutput=$(execPWGEN)
    ;;
  esac

  IFS=$'\t' read -a autoGenPswds <<< "$cmdOutput"

  setAutoGenPswdChoices autoGenPswds agpChoices agpChoiceDescs
  appendOptionsForRadiolist "$dialogText" "$dialogTitle" dlgOpts
  setDataForMultiColList agpChoices agpChoiceDescs dlgData

  key=$(getValueFromRadiolist "$helpText" dlgOpts dlgButtons dlgColumns dlgData helpDlgOpts)

  if [ ${#key} -gt 0 ]; then
    if [ ${agpChoiceDescs["$key"]+_} ]; then
      local aryIdx=$(expr ${key:1} - 1)
      mbSelVal="${autoGenPswds[$aryIdx]}"
    else
      mbSelVal="$key"
    fi
  else
    mbSelVal=""
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                   setAutoGenPswdChoices
# DESCRIPTION: Set the option values and passwords that can be chose for a
#              yad "radiolist" dialog
#  Required Params:
#      1)        apps - array of passwords generated
#      2)     agpList - array of values for the "Option" column that will be appended to
#      3) agpDescList - associative array of key/value pairs that will be appended to
#                       containing the option & its generated password
#---------------------------------------------------------------------------------------
setAutoGenPswdChoices() {
  local -n agps=$1
  local -n agpList=$2
  local -n agpDescList=$3
  local key=""
  local optNum=0

  for pswd in "${agps[@]}"; do
    optNum=$(expr ${optNum} + 1)
    key=$(printf "#%d" ${optNum})
    agpChoices+=("$key")
    agpChoiceDescs["$key"]=$(escapeSpecialCharacters "$pswd")
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                     dispInvGroupNameMsg
# DESCRIPTION: Displays the appropriate error message when either:
#              A) The group name already exists
#              B) Fails the criteria specified in the function "isValidGroupName"
#  Required Params:
#      1) groupName - the name of the group to be created
#---------------------------------------------------------------------------------------
dispInvGroupNameMsg() {
  local groupName="$1"
  local errorMsg="The group name \"$groupName\""
  local dialogTitle="Error:  Invalid Group Name"
  local textArray=()
  local -A yadOpts=(["buttons-layout"]="center" ["image"]="$YAD_GRAPHICAL_PATH/images/error-icon.png"
    ["image-on-top"]=true ["title"]="$dialogTitle" ["window-icon"]="error-window-icon.png"
    ["text"]="<span font_weight='bold' font_size='large'>ERROR!!!!!!!</span>" ["text-align"]="left"
    ["height"]=300 ["width"]=600 ["wrap"]=true ["fontname"]="Sans 12" ["center"]=true ["wrap"]=true ["size"]="fit")
  local dlgBtns=("Exit")
  local dashBorder="$BORDER_WIDTH_600"

  if [ $(getent group $1) ]; then
    errorMsg+=" already exists!"
  else
    errorMsg+=" does NOT meet the installer's criteria!"
    textArray=("$errorMsg" " ")
    errorMsg=$(getTextForGroupName)
    textArray+=("$errorMsg")
    errorMsg=$(getTextForDialog "${textArray[@]}")
    yadOpts["height"]=435
    yadOpts["width"]=675
    dashBorder="${BORDER_WIDTH_600}-------------"
  fi

  textArray=("$dashBorder" "$errorMsg" "$dashBorder" " "
    "You must re-enter the group name!")
  local dialogText=$(getTextForDialog "${textArray[@]}")

  showTextInfoDialog "$dialogText" yadOpts dlgBtns
}
