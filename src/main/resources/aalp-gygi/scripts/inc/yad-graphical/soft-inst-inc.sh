#!/bin/bash
#set -e

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================
source "$GRAPHICAL_PATH/yad-nb-inc.sh"

#===============================================================================
# globals
#===============================================================================

#===============================================================================
# functions/methods
#===============================================================================

#---------------------------------------------------------------------------------------
#      METHOD:                       showMenuDialog
# DESCRIPTION: Displays the "Welcome" linux "dialog" box of type message box
# DESCRIPTION: Show a yad "radiolist" dialog to select the software to be installed.
#              Set the value within the global variable "$mbSelVal"
#  Required Params:
#      1) dialogTitle - String to be displayed at the top of the dialog box.
#      2)    helpText - text to display in the yad "text-info" dialog for viewing the help
#----------------------------------------------------------------------------~-----------
showMenuDialog() {
  local dialogTitle="$1"
  local dlgButtons=("Ok" "Exit" "Help")
  local dlgColumns=("Radio Btn" "Software Name" "Description")
  local -A dlgOpts=(["buttons-layout"]="center" ["image"]="$YAD_GRAPHICAL_PATH/images/kaalp-gygi.png"
    ["grid-lines"]="both" ["list-type"]="radiolist" ["image-on-top"]=true ["title"]="$1"
    ["window-icon"]="app-icon.png" ["center"]=true ["wrap"]=true ["size"]="fit"
  )

  setDataForMultiColList MENU_OPTS AUR_PCKG_DESCS dlgData

  mbSelVal=$(getValueFromRadiolist "$2" dlgOpts dlgButtons dlgColumns dlgData)
}

#---------------------------------------------------------------------------------------
#      METHOD:                     selectTomcatVersion
# DESCRIPTION: Show a yad "radiolist" dialog to select the version of Tomcat to install.
#              Set the value within the global variable "$mbSelVal".
#  Required Params:
#      1)   dialogTitle - String to be displayed at the top of the dialog window.
#      2) dialogTextHdr - String to be displayed at the top of the text area.
#      3)    dialogText - Text to display inside the dialog before the
#                         list of menu options
#---------------------------------------------------------------------------------------
selectTomcatVersion() {
  local dlgButtons=("Ok" "Cancel")
  local dlgColumns=("Radio Btn" "Version" "Description")
  local title=$(printf "%12s%s" " " "$2")
  local textArray=("$title" " " " " " " " " "$3")
  local dialogText=$(printf "\n%s" "${textArray[@]}")
  dialogText=$(escapeSpecialCharacters "${dialogText:1}")
  local -A dlgOpts=(["buttons-layout"]="center" ["image"]="$YAD_GRAPHICAL_PATH/images/tomcat.png"
    ["height"]=350 ["grid-lines"]="both" ["list-type"]="radiolist" ["image-on-top"]=true ["title"]="$1"
    ["window-icon"]="app-icon.png" ["center"]=true ["wrap"]=true ["size"]="fit" ["text-align"]="left"
    ["text"]="<span font_weight='bold' font_size='medium'>$dialogText</span>"
  )
  local dlgData=()

  setDataForMultiColList versOpts versDescs dlgData "${versOpts[1]}"
  local retVal=$(getValueFromRadiolist "$4" dlgOpts dlgButtons dlgColumns dlgData helpDlgOpts)

  if [ ${#retVal} -gt 0 ]; then
    mbSelVal="$retVal"
  else
    mbSelVal=""
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                      dispLoadingDataPB
# DESCRIPTION: Display a yad "progress indication" dialog (i.e. progress bar) while
#              executing the methods to initialize the data for the installation guide.
#  Required Params:
#      1) pbLogText - String to be displayed at the top of the logging area.
#      2) pbTitle - String to be displayed at the top of the dialog.
#---------------------------------------------------------------------------------------
dispLoadingDataPB() {
  local methodNum=1
  local pbPerc=0
  local pbTitle=""
  local totalMethods=${#pbKeys[@]}

  (
    sleep 0.5
    for pbKey in "${pbKeys[@]}"; do
      pbTitle="${pbTitles["$pbKey"]}"
      if [ ${pbPerc} -lt 100 ]; then
        echo ${pbPerc}
      fi

      echo "# $pbTitle"

      callBackMethod

      sleep 0.5
    done
  ) | $DIALOG --progress --pulsate --center --percentage=0 --log-expanded --enable-log="$2 Log" \
              --log-height=175 --width=800 --height=570 --title="$1" --size=fit \
              --text-align=center --no-buttons --auto-close --window-icon="$ICONS_PATH/app-icon.png" \
              --image="$YAD_GRAPHICAL_PATH/images/kaalp-gygi.png" --image-on-top 2> /dev/null
}
