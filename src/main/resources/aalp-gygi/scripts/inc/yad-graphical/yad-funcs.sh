#!/bin/bash

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# globals
#===============================================================================
declare ICONS_PATH="$GRAPHICAL_PATH/icons"
declare -A BUTTON_ICONS=(["Ok"]="gtk-ok.png" ["Cancel"]="gtk-cancel.png" ["Confirm"]="gtk-confirm.png"
  ["Continue"]="gtk-continue.png" ["Help"]="gtk-help.png" ["View Partition Layout"]="gtk-view.png"
  ["View URL"]="gtk-browser.png" ["Firefox"]="firefox.png" ["Exit"]="gtk-exit.png" ["Keep"]="gtk-keep.png"
  ["Zap"]="gtk-zap.png" ["Min"]="gtk-down.png"  ["Max"]="gtk-up.png" ["Other"]="gtk-other.png"
  ["Skip"]="gtk-skip.png" ["Yes"]="gtk-ok.png" ["No"]="gtk-cancel.png" ["Save"]="gtk-save.png"
  ["Form"]="gtk-form.png" ["Done"]="gtk-commit.png" ["Format"]="format-icon.png" ["Mount"]="mount-icon.png"
  ["Submit"]="gtk-form.png" ["All"]="gtk-all.png" ["Reset"]="gtk-reset.png" ["ROOT"]="gtk-root.png"
  ["Edit"]="gtk-edit.png" ["Local Time"]="gtk-local.png" ["Use Local"]="gtk-local.png" ["UTC"]="gtk-utc.png"
  ["Use UTC"]="gtk-utc.png" ["Run"]="gtk-run.png" ["SSkip"]="gtk-skip.png" ["Select"]="gtk-select.png"
  ["Install"]="gtk-install.png" ["Commit"]="gtk-commit.png"
)
declare PART_LAYOUT_COLS=("Device" "Name" "Size" "GUID" "FS Type" "Mount Pt" "Flags")
declare -A ESCAPE_CHARS=(['&']="&amp;" ['<']="&lt;" ['>']="&gt;" ['"']="&quot;" ["'"]="&apos;")
declare BORDER_WIDTH_600="--------------------------------------------------------------------------------------------------"
declare BORDER_WIDTH_MAX="-------------------------------------------------------------------------------------------------------------------------------"
declare NB_TAB_BORDER="----------------------------------------------------------------------------"

declare NB_CMD_KEYS=("nbCmd#01" "nbCmd#02")
declare -A NB_TAB_DATA_FNS=(["nbCmd#01"]="/tmp/tabRes#%02d" ["nbCmd#02"]="/tmp/tabFormRes#%02d"
  ["helpText"]="/tmp/YAD-Help-Text-Info#%02d.txt" ["tabData"]="/tmp/YAD-Tab-%02d.data"
  ["plTabData"]="/tmp/YAD-TabPL-%02d.data")
declare DATA_FILE_NAME="/tmp/YAD-List-Dialog.data"
declare TEXT_INFO_FILE_NAME="/tmp/YAD-Text-Info.txt"
declare -A TAB_SEL_VALS=()
declare TAB_KEY_FMT="tab#%02d"
declare YAD_MAX_WIDTH=1000
declare YAD_MAX_HEIGHT=700

: ${DIALOG=yad}
: ${DIALOG_OK=0}
: ${DIALOG_CANCEL=1}
: ${DIALOG_EXTRA=2}
: ${DIALOG_HELP=3}
: ${DIALOG_BROWSER=4}
: ${DIALOG_LIST=5}
: ${DIALOG_EXIT=6}
: ${DIALOG_KEEP=7}
: ${DIALOG_ZAP=8}
: ${DIALOG_COMMIT=9}
: ${DIALOG_FORM=10}
: ${DIALOG_EDIT=11}
: ${DIALOG_ALL=12}
: ${DIALOG_RESET=13}
: ${DIALOG_ROOT=15}
: ${DIALOG_LOCAL=17}
: ${DIALOG_UTC=19}
: ${DIALOG_SSKIP=20}
: ${DIALOG_ESC=252}
: ${DIALOG_INPUT_ERR0R=409}
: ${DIALOG_VALUE_ERR0R=406}

#===============================================================================
# functions / methods
#===============================================================================

#---------------------------------------------------------------------------------------
#    FUNCTION:              getChoicesFromListDialog
# DESCRIPTION: Displays a list of choices using the linux "dialog" command's
#              check list, menu, or radio list dialog box
#
#  Required Params:
#      1)    listDlgOpts - associative array that contains key/value pair of options
#                          for the yad "dialog" of type list
#                          (see function getOptionsForListDialog)
#      2) listDlgButtons - array of button titles that map to the icon in the global
#                          associative array var "BUTTON_ICONS"
#      3) listDlgColumns - array of column header names
#      4)    listDlgData - array of column values to be displayed within the dialog
#   Optional Param:
#      5) string to override the default data file name
#---------------------------------------------------------------------------------------
function getChoicesFromListDialog() {
  local -n listDlgOpts=$1
  local -n listDlgButtons=$2
  local -n listDlgColumns=$3
  local -n listDlgData=$4
  local dataFileName=$(echo "$DATA_FILE_NAME")
  local dialogSelVal=""
  local choices=()
  local yadCmd=$(getCommandForListDialog listDlgOpts listDlgButtons listDlgColumns)
  local lines=$(printf "\n%s" "${listDlgData[@]}")
  lines=${lines:1}

  if [ "$#" -gt 4 ]; then
    dataFileName="$5"
  fi

  echo -e "$lines" > "$dataFileName"
  yadCmd+=$(printf " < \"%s\" 2> /dev/null" "$dataFileName")
  yadChoices=$(eval "$yadCmd")
  btnClick=$?

  case ${btnClick} in
    ${DIALOG_OK})
      if [ ${listDlgOpts["ret-separator"]+_} ]; then
        dialogSelVal=$(printf "\n%s" "${yadChoices[@]}")
        dialogSelVal="$DIALOG_OK,${dialogSelVal:1}"
      else
        for row in "${yadChoices[@]}"; do
          choices+=($(echo "$row" | cut -d"|" -f2))
        done
        dialogSelVal=$(echo "$DIALOG_OK,${choices[@]}")
      fi
    ;;
    ${DIALOG_CANCEL})
      dialogSelVal=$(echo "$DIALOG_CANCEL,Cancel")
    ;;
    ${DIALOG_HELP})
      dialogSelVal=$(echo "$DIALOG_HELP,Help")
    ;;
    ${DIALOG_ESC})
      dialogSelVal=$(echo "$DIALOG_CANCEL,ESC")
    ;;
    ${DIALOG_EXIT})
      dialogSelVal=$(echo "$DIALOG_EXIT,Exit")
    ;;
    ${DIALOG_EXTRA})
      dialogSelVal=$(echo "$DIALOG_EXTRA,Extra")
    ;;
    ${DIALOG_LIST})
      dialogSelVal=$(echo "$DIALOG_LIST,showPartTable")
    ;;
    ${DIALOG_KEEP})
      dialogSelVal=$(echo "$DIALOG_KEEP,KEEP")
    ;;
    ${DIALOG_ZAP})
      dialogSelVal=$(echo "$DIALOG_ZAP,ZAP")
    ;;
    ${DIALOG_SSKIP})
      dialogSelVal=$(echo "$DIALOG_SSKIP,Skip")
    ;;
  esac

  echo "$dialogSelVal"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getCommandForListDialog
# DESCRIPTION: Get the command that will display a yad "dialog" of type list
#      RETURN: concatenated string
#  Required Params:
#      1)    yadOpts - associative array that contain the available options
#      2) yadButtons - array of dialog buttons
#      3)    yadCols - array of column headers
#---------------------------------------------------------------------------------------
function getCommandForListDialog() {
  local -n listOpts=$1
  local -n listButtons=$2
  local -n listCols=$3
  local cmd=$(echo "$DIALOG")

  cmd+=$(getOptionsForListDialog listOpts)
  cmd+=$(getButtonsForDialog listButtons)
  cmd+=$(getColumnsForListDialog listCols)

  echo "$cmd"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getOptionsForListDialog
# DESCRIPTION: Get available options for a yad "dialog" of type list
#      RETURN: concatenated string
#  Required Params:
#      1) dialogOpts - associative array that contain the available options
#  Options for the yad "list" dialog:
#              "list-type" - checklist or radiolist
#           "no-selection" - Disable selection in list.
#             "grid-lines" - Draw grid lines of type:
#                            A) hor[izontal]
#                            B) vert[ical]
#                            C) both
#              "separator" - Set output separator characters.
#             "no-headers" - Do not show column headers.
#               "no-click" - Disable sorting of column content by clicking on its header.
#            "hide-column" - Hide a specific column
#---------------------------------------------------------------------------------------
function getOptionsForListDialog() {
  local -n dialogOpts=$1
  local key="list-type"
  local keyArray=("grid-lines" "hide-column" "list-type" "no-click" "no-headers"
                  "no-selection" "separator" "wrap-cols" "wrap-width")
  local yadCmdOpts=$(getGeneralOptionsForDialogs dialogOpts)

  if [ ! ${dialogOpts["$key"]+_} ]; then
    yadCmdOpts+=$(echo " --list")
  fi

  for key in "${keyArray[@]}"; do
    if [ ${dialogOpts["$key"]+_} ]; then
      case "$key" in
        "grid-lines")
          yadCmdOpts+=$(echo " --$key=${dialogOpts[$key]}")
        ;;
        "hide-column")
          yadCmdOpts+=$(echo " --$key=${dialogOpts[$key]}")
        ;;
        "list-type")
          yadCmdOpts+=$(echo " --list --${dialogOpts[$key]}")
        ;;
        "no-click")
          yadCmdOpts+=$(echo " --$key")
        ;;
        "no-headers")
          yadCmdOpts+=$(echo " --$key")
        ;;
        "no-selection")
          yadCmdOpts+=$(echo " --$key")
        ;;
        "separator")
          yadCmdOpts+=$(echo " --$key=${dialogOpts[$key]}")
        ;;
        "wrap-cols")
          yadCmdOpts+=$(echo " --$key=${dialogOpts[$key]}")
        ;;
        "wrap-width")
          yadCmdOpts+=$(echo " --$key=${dialogOpts[$key]}")
        ;;
      esac
    fi
  done

  echo "$yadCmdOpts"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                getGeneralOptionsForDialogs
# DESCRIPTION: Get the general options that are available for every yad "dialog"
#      RETURN: concatenated string
#  Required Params:
#      1) dialogOpts - associative array that contain the available options
#  General Options for a yad "dialog":
#                  "title" - Set the dialog title.
#            "window-icon" - Set the window icon.
#                  "width" - Set the dialog window width.
#                 "height" - Set the dialog window height.
#                "timeout" - Set the dialog timeout in seconds.
#      "timeout-indicator" - Show timeout indicator in given position.
#                            Positions are top, bottom, left or right.
#                   "text" - Set the dialog text.
#             "text-align" - Set type of dialog text justification.
#                            Values may be left, right, center or fill.
#                  "image" - Set the dialog image which appears on the left side of dialog.
#                            IMAGE might be file name or icon name from current icon theme.
#           "image-on-top" - Show image above main widget instead of left.
#         "buttons-layout" - Set buttons layout type. Possible types are: spread, edge,
#                            start, end or center. Default is end.
#              "no-markup" - Don't use pango markup in dialog's text.
#              "maximized" - Run dialog window maximized.
#                 "on-top" - Place window over other windows.
#                 "center" - Place window on center of screen.
#                    "size"- Set initial size of picture. Available values are:
#                            A) "fit" for fitting image in window
#                            B) "orig" to show in its original size.
#---------------------------------------------------------------------------------------
function getGeneralOptionsForDialogs() {
  local -n generalOpts=$1
  local key="notebook"
  local keyArray=("buttons-layout" "center" "entry-label" "entry-text"
    "image" "image-on-top" "no-markup" "on-top" "size" "text" "text-align"
    "timeout" "timeout-indicator" "title" "window-icon")
  local yadCmdOpts=""

  if [ ! ${generalOpts["$key"]+_} ]; then
    yadCmdOpts=$(getSizeOptionsForDialog generalOpts)
  fi

  for key in "${keyArray[@]}"; do
    if [ ${generalOpts["$key"]+_} ]; then
      case "$key" in
        "buttons-layout")
          yadCmdOpts+=$(echo " --$key=${generalOpts[$key]}")
        ;;
        "center")
          yadCmdOpts+=$(echo " --$key")
        ;;
        "entry-label")
          yadCmdOpts+=$(echo " --entry --$key=\"${generalOpts[$key]}\"")
        ;;
        "entry-text")
          yadCmdOpts+=$(echo " --$key=\"${generalOpts[$key]}\"")
        ;;
        "grid-lines")
          yadCmdOpts+=$(echo " --$key=${generalOpts[$key]}")
        ;;
        "image")
          yadCmdOpts+=$(echo " --$key=\"${generalOpts[$key]}\"")
        ;;
        "image-on-top")
          yadCmdOpts+=$(echo " --$key")
        ;;
        "no-markup")
          yadCmdOpts+=$(echo " --$key")
        ;;
        "on-top")
          yadCmdOpts+=$(echo " --$key")
        ;;
        "size")
          yadCmdOpts+=$(echo " --$key=${generalOpts[$key]}")
        ;;
        "text")
          yadCmdOpts+=$(echo " --$key=\"${generalOpts[$key]}\"")
        ;;
        "text-align")
          yadCmdOpts+=$(echo " --$key=${generalOpts[$key]}")
        ;;
        "timeout")
          yadCmdOpts+=$(echo " --$key=${generalOpts[$key]}")
        ;;
        "timeout-indicator")
          yadCmdOpts+=$(echo " --$key=${generalOpts[$key]}")
        ;;
        "title")
          yadCmdOpts+=$(echo " --$key=\"${generalOpts[$key]}\"")
        ;;
        "window-icon")
          yadCmdOpts+=$(printf " --%s=\"%s/%s\"" "$key" "$ICONS_PATH" "${generalOpts[$key]}")
        ;;
      esac
    fi
  done

  echo "$yadCmdOpts"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getSizeOptionsForDialog
# DESCRIPTION: Get the size options for a yad "dialog"
#      RETURN: concatenated string
#  Required Params:
#      1) sizeOpts - associative array that contain the options
#                  "width" - Set the dialog window width.
#                 "height" - Set the dialog window height.
#              "maximized" - Run dialog window maximized.
#---------------------------------------------------------------------------------------
function getSizeOptionsForDialog() {
  local -n sizeOpts=$1
  local width=$YAD_MAX_WIDTH
  local height=$YAD_MAX_HEIGHT
  local key="maximized"
  local dialogSize=""

  if [ ! ${sizeOpts["$key"]+_} ]; then
    key="height"
    if [ ${sizeOpts["$key"]+_} ]; then
      height=${sizeOpts["$key"]}
    fi
    dialogSize+=$(echo " --$key=${height}")
    key="width"
    if [ ${sizeOpts["$key"]+_} ]; then
      width=${sizeOpts["$key"]}
    fi
    dialogSize+=$(echo " --$key=${width}")
  else
    dialogSize+=$(echo " --$key")
  fi

  echo "$dialogSize"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getButtonsForDialog
# DESCRIPTION: Get the buttons that will display in a yad "dialog"
#      RETURN: concatenated string
#  Required Params:
#      1) cmdButtons - array of dialog buttons
#---------------------------------------------------------------------------------------
function getButtonsForDialog() {
  local -n cmdButtons=$1
  local btnID=0
  local btnIcon=""
  local btns=""

  for btn in "${cmdButtons[@]}"; do
    btnIcon="${BUTTON_ICONS[$btn]}"
    case "$btn" in
      "Cancel"|"No"|"Skip")
        btnID=${DIALOG_CANCEL}
      ;;
      "Continue"|"Confirm"|"Min"|"Ok"|"Save"|"Submit"|"Yes")
        btnID=${DIALOG_OK}
      ;;
      "Commit"|"Done"|"Format"|"Mount")
        btnID=${DIALOG_COMMIT}
      ;;
      "Edit")
        btnID=${DIALOG_EDIT}
      ;;
      "Exit")
        btnID=${DIALOG_EXIT}
      ;;
      "Form"|"Install"|"Run"|"Select")
        btnID=${DIALOG_FORM}
      ;;
      "Help")
        btnID=${DIALOG_HELP}
      ;;
      "Keep")
        btnID=${DIALOG_KEEP}
      ;;
      "Local Time"|"Use Local")
        btnID=${DIALOG_LOCAL}
      ;;
      "Max")
        btnID=${DIALOG_EXTRA}
      ;;
      "Other")
        btnID=${DIALOG_BROWSER}
      ;;
      "View Partition Layout")
        btnID=${DIALOG_LIST}
      ;;
      "Zap")
        btnID=${DIALOG_ZAP}
      ;;
      "All")
        btnID=${DIALOG_ALL}
      ;;
      "Reset")
        btnID=${DIALOG_RESET}
      ;;
      "ROOT")
        btnID=${DIALOG_ROOT}
      ;;
      "UTC"|"Use UTC")
        btnID=${DIALOG_UTC}
      ;;
      "SSkip")
        btnID=${DIALOG_SSKIP}
        btn="Skip"
      ;;
      *)
        btnID=${DIALOG_EXTRA}
      ;;
    esac

    btns+=$(printf " --button=\" %s!%s/%s\":%d" "$btn" "$ICONS_PATH" "$btnIcon" ${btnID})
  done

  echo "$btns"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getColumnsForListDialog
# DESCRIPTION: Get the column headers that will display in a yad "dialog" of type list
#      RETURN: concatenated string
#  Required Params:
#      1) colNames - array of column headers
#---------------------------------------------------------------------------------------
function getColumnsForListDialog() {
  local -n colNames=$1
  local colHdrs=""

  for col in "${colNames[@]}"; do
    colHdrs+=$(printf " --column=\"%s\"" "$col")
  done

  echo "$colHdrs"
}

#---------------------------------------------------------------------------------------
#      METHOD:                 showHelpAndPartTbl
# DESCRIPTION: Displays a yad "dialog" of type notebook that will contain the following tabs:
#              Tab #1) the partition table and text in a yad "dialog" of type "list"
#              Tab #2) the text for the help section in a yad "dialog" of type "text-info"
#  Required Params:
#      1) partitionLayout - string containing the header, logical volumes and partitions
#                           of the partition table
#      2)        helpText - text to display in the help "dialog"
#      3)        helpOpts - options for the both the notebook and help dialog
#---------------------------------------------------------------------------------------
showHelpAndPartTbl() {
  local partitionLayout="$1"
  local helpText="$2"
  local -n helpOpts=$3
  local -n nbOpts=$4
  local yadBtns=("Exit")
  local nbKeyPlug=$(getSixDigitRandomNum)
  local yadCmd=$(echo "$DIALOG --notebook --key=${nbKeyPlug} --tab=\"Partition Table\" --tab=\"Help\"")

  showPartTable "$partitionLayout" ${nbKeyPlug}
  setHelpTab ${nbKeyPlug} "$helpText" helpOpts

  yadCmd+=$(getOptionsForNotebook nbOpts)
  yadCmd+=$(getButtonsForDialog yadBtns)

  eval "$yadCmd"
}

#---------------------------------------------------------------------------------------
#      METHOD:                     setHelpTab
# DESCRIPTION: Sets up the second tab of a yad "notebook" dialog that will display the
#              text for the help section
#  Required Params:
#      1) nbKeyPlug - integer value for the notebook key & plug of its tabs
#      2)  helpText - text to display in the help "dialog" of type text-info
#      3) nbTabOpts - options for the "dialog"
#---------------------------------------------------------------------------------------
setHelpTab() {
  local nbKeyPlug="$1"
  local helpText="$2"
  local -n nbTabOpts=$3
  local tabNum=2
  local tabCmd=$(getCmdForTextInfoTab ${nbKeyPlug} "$helpText" ${tabNum} nbTabOpts)

  eval "$tabCmd"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getOptionsForNotebook
# DESCRIPTION: Get available options for a yad "notebook" dialog
#      RETURN: concatenated string
#  Required Params:
#      1) opts - associative array that contain the available options
#  Options for the yad "notebook":
#              "tab-pos" - Set the position of the tabs  Value may be top, bottom,
#                          left, or right. Default is top.
#          "tab-borders" - Set the width of the borders around the widget in the tabs.
#---------------------------------------------------------------------------------------
function getOptionsForNotebook() {
  local -n opts=$1
  local key=""
  local keyArray=("tab-borders" "tab-pos")
  local yadOptions=$(getGeneralOptionsForDialogs opts)

  for key in "${keyArray[@]}"; do
    if [ ${opts["$key"]+_} ]; then
      case "$key" in
        "tab-borders")
          yadOptions+=$(echo " --$key=${opts[$key]}")
        ;;
        "tab-pos")
          yadOptions+=$(echo " --$key=${opts[$key]}")
      esac
    fi
  done

  echo "$yadOptions"
}

#---------------------------------------------------------------------------------------
#      METHOD:                 showTextInfoDialog
# DESCRIPTION: Displays a yad "dialog" of type text-info
#  Required Params:
#      1) dialogText - text to display in the yad "text-info" dialog
#      2) dialogOpts - associative array that contain the available options
#   Optional Param:
#      3) array of button titles to override the default
#---------------------------------------------------------------------------------------
showTextInfoDialog() {
  local dialogText="$1"
  local -n dialogOpts=$2
  local textInfoOpts=$(getOptionsForTextInfo dialogOpts "$dialogText")
  local yadBtns=("Exit")
  local fileName=$(echo "$TEXT_INFO_FILE_NAME")

  if [ "$#" -gt 2 ]; then
    local -n optParam=$3
    yadBtns=("${optParam[@]}")
  fi

  if [ ${dialogOpts["textInfoFN"]+_} ]; then
    fileName=$(echo "${dialogOpts["textInfoFN"]}")
  else
    echo -e "$dialogText" > "$fileName"
  fi

  textInfoOpts+=$(getButtonsForDialog yadBtns)

  local yadCmd=$(echo "$DIALOG $textInfoOpts --text-info < $fileName 2> /dev/null")
  eval "$yadCmd"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getOptionsForTextInfo
# DESCRIPTION: Get available options for a yad "dialog" of type text-info
#      RETURN: concatenated string
#  Required Params:
#      1) opts - associative array that contain's the available options
#  Options for the yad "text-info" dialog:
#                   "wrap" - Enable text wrapping.
#                "justify" - Set the justification. Values may be left, right,
#                            center or fill. Default is left.
#               "fontname" - Sets the font for the text. The value must be in a Pango
#                            font description format. By default Monospace is used.
#               "show-uri" - If the text contains links/URLs, then this option will make
#                            them clickable. They will open with the xdg-open command.
#---------------------------------------------------------------------------------------
function getOptionsForTextInfo() {
  local -n opts=$1
  local dialogText="$2"
  local width=$YAD_MAX_WIDTH
  local height=$YAD_MAX_HEIGHT
  local key="show-uri"
  local keyArray=("fontname" "justify" "wrap")
  local yadOptions=$(getGeneralOptionsForDialogs opts)

  if [[ "$dialogText" =~ "http" ]]; then
    yadOptions+=$(echo " --$key")
  fi
  for key in "${keyArray[@]}"; do
    if [ ${opts["$key"]+_} ]; then
      case "$key" in
        "fontname")
          yadOptions+=$(echo " --$key=\"${opts[$key]}\"")
        ;;
        "justify")
          yadOptions+=$(echo " --$key=${opts[$key]}")
        ;;
        "on-top")
          yadOptions+=$(echo " --$key")
        ;;
        "timeout")
          yadOptions+=$(echo " --$key=${opts[$key]}")
        ;;
        "timeout-indicator")
          yadOptions+=$(echo " --$key=${opts[$key]}")
        ;;
        "title")
          yadOptions+=$(echo " --$key=\"Help:  ${opts[$key]}\"")
        ;;
        "wrap")
          yadOptions+=$(echo " --$key")
        ;;
        "window-icon")
          yadOptions+=$(printf " --%s=\"%s/%s\"" "$key" "$ICONS_PATH" "${opts[$key]}")
        ;;
      esac
    fi
  done

  echo "$yadOptions"
}

#---------------------------------------------------------------------------------------
#      METHOD:                   showPartTable
# DESCRIPTION: Displays the partition table and text in a yad "dialog" of type list
#  Required Params:
#      1) partitionLayout - string containing header, logical volumes and partitions
#   Optional Param:
#      2) integer value for the notebook key & plug of its tabs
#      3) tab number
#---------------------------------------------------------------------------------------
showPartTable() {
  local partitionLayout="$1"
  local partTblRows=()
  local yadButtons=("Exit")
  local cols=()
  local yadColumns=("${PART_LAYOUT_COLS[@]}")
  local dialogData=()
  local startRow=1
  local tabNum=1

  IFS=$'\n' read -d '' -a partTblRows <<< "$partitionLayout"

  local hdr=$(printf "%s" "${partTblRows[0]}")
  if [[ "$partitionLayout" =~ "Volume" ]]; then
    hdr+=$(printf "\n%s" "${partTblRows[1]}")
    startRow=2
  fi

  hdr=$(escapeSpecialCharacters "$hdr")

  local -A yadOpts=(["buttons-layout"]="center" ["grid-lines"]="both" ["height"]=250 ["width"]=680
    ["no-selection"]=true ["text"]="<span font_weight='bold' font_size='medium'>$hdr</span>"
    ["text-align"]="left" ["title"]="Partition Layout/Scheme" ["center"]=true
  )

  for row in "${partTblRows[@]:${startRow}}"; do
    IFS=$";" read -a cols <<< "$row"
    dialogData+=("${cols[@]}")
  done

  if [ "$#" -gt 1 ]; then
    unset yadOpts["height"]
    unset yadOpts["width"]
    unset yadOpts["title"]
    unset yadOpts["center"]
    unset yadOpts["buttons-layout"]
    yadOpts["notebook"]=true

    if [ "$#" -gt 2 ]; then
      tabNum=$3
    fi

    local tabOutput=$(printf " < \"%s\" &> /tmp/res1 &" "$DATA_FILE_NAME")
    local yadCmd=$(getTabCmdForPartTable $2 ${tabNum} "$tabOutput" yadColumns dialogData yadOpts)
    eval "$yadCmd"
  else
    getChoicesFromListDialog yadOpts yadButtons yadColumns dialogData
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getTabCmdForPartTable
# DESCRIPTION: Gets the command for a tab of a yad "notebook" dialog that will display
#              the partition table within a dialog/widget of type list
#      RETURN: concatenated string for command
#  Required Params:
#      1)    nbKeyPlug - integer value for the notebook key & plug of its tabs
#      2)       tabNum - tab number
#      3)    tabOutput - contains the name of the data file to load the tab with
#                        as well as the file name containing the data generated from the tab
#      4)  tabListCols - array of column header names
#      5)  tabListData - array of rows containing the data of the partition table
#      6)  tabListOpts - associative array that contain's the available options
#---------------------------------------------------------------------------------------
function getTabCmdForPartTable() {
  local nbKeyPlug="$1"
  local tabNum=$2
  local tabOutput="$3"
  local -n tabListCols=$4
  local -n tabListData=$5
  local -n tabListOpts=$6
  local dataFileName=$(echo "$tabOutput" | cut -d'"' -f2)

  if [ ! -f "$dataFileName" ]; then
    local lines=$(printf "\n%s" "${tabListData[@]}")
    lines=${lines:1}
    echo -e "$lines" > "$dataFileName"
  fi

  local yadCmd=$(echo "$DIALOG --plug=${nbKeyPlug} --tabnum=${tabNum}")
  yadCmd+=$(getOptionsForListDialog tabListOpts)
  yadCmd+=$(getColumnsForListDialog tabListCols)
  yadCmd+="$tabOutput"

  echo "$yadCmd"
}

#---------------------------------------------------------------------------------------
#      METHOD:               getSelectedDeviceToPartition
# DESCRIPTION: Get the device to be partitioned and where Arch Linux will be installed.
#      RETURN: name of device in the global "mbSelVal" variable
#---------------------------------------------------------------------------------------
getSelectedDeviceToPartition() {
  local dataFileNameFmt="${NB_TAB_DATA_FNS["nbCmd#01"]}"
  local numTabs=2
  local nbKeyPlug=$(getSixDigitRandomNum)

  showNotebookForSelectingDevice ${nbKeyPlug} "$dataFileNameFmt" ${numTabs} devices
  btnClick=$?
  case ${btnClick} in
    ${DIALOG_OK})
      mbSelVal=$(getSelectedOptionsFromNotebook ${numTabs} "$dataFileNameFmt")
    ;;
    *)
      mbSelVal=""
    ;;
  esac
}

#---------------------------------------------------------------------------------------
#      METHOD:               showNotebookForSelectingDevice
# DESCRIPTION: Displays the a yad "notebook" dialog with the following tabs:
#              #1) list of attached devices
#              #2) "radio list" dialog/widget to select a device
#  Required Params:
#      1)       nbKeyPlug - integer value for the notebook key & plug of its tabs
#      2) dataFileNameFmt - format of the data file name that will hold the
#                           selected value
#      3)         numTabs - total number of tabs to create
#      4)   deviceChoices - array of device names that are available to partition and
#                           install Arch Linux
#---------------------------------------------------------------------------------------
showNotebookForSelectingDevice() {
  local nbKeyPlug="$1"
  local tabOutput=$(echo " < \"%s\" &> $2 2> /dev/null &")
  local numTabs=$3
  local -n deviceChoices=$4
  local dialogTitle="Primary Device Selection"
  local fileName=""
  local tabColumns=()
  local -A tabOpts=(["grid-lines"]="both" ["notebook"]=true)
  local nbButtons=("Ok" "Cancel")
  local -A nbOpts=(["height"]=300 ["width"]=600 ["buttons-layout"]="center"
    ["title"]="$dialogTitle" ["window-icon"]="device.png" ["center"]=true)
  local nbCmd=$(echo "$DIALOG --notebook --key=${nbKeyPlug}")
  local tabCmd=""
  local tabName=""
  local textArray=(
  "Select the device to be partitioned and where"
  "Arch Linux will be installed:"
  )
  local dialogText=$(getTextForDialog "${textArray[@]}")

  for tabNum in $(eval echo "{1..${numTabs}}"); do
    fileName=$(printf "${NB_TAB_DATA_FNS[tabData]}" ${tabNum})
    case ${tabNum} in
      1)
        setDataForAttachedDevicesTab "$fileName"
        tabOpts["no-selection"]=true
        tabColumns=("Name" "Size" "Type" "Mount PT")
        tabName="Attached Devices"
      ;;
      2)
        setDataForSelectDeviceTab "$fileName" deviceChoices
        tabColumns=("Radio Btn" "Device Name")
        unset tabOpts["no-selection"]
        tabOpts["list-type"]="radiolist"
        tabOpts["text"]="<span font_weight='bold' font_size='medium'>$dialogText</span>"
        tabOpts["text-align"]="left"
        tabName="Select Device"
      ;;
    esac
    tabCmd=$(echo "$DIALOG --plug=${nbKeyPlug} --tabnum=${tabNum}")
    tabCmd+=$(getOptionsForListDialog tabOpts)
    tabCmd+=$(getColumnsForListDialog tabColumns)
    tabCmd+=$(printf "$tabOutput" "$fileName" ${tabNum})
    eval "$tabCmd"
    nbCmd+=$(printf " --tab=\"%s\"" "$tabName")
  done

  nbCmd+=$(getOptionsForNotebook nbOpts)
  nbCmd+=$(getButtonsForDialog nbButtons)

  eval "$nbCmd"
}

#---------------------------------------------------------------------------------------
#      METHOD:                 setDataForAttachedDevicesTab
# DESCRIPTION: Set the data to be displayed on the Attached Devices tab
#  Required Params:
#      1) fileName - name of the file containing the data
#---------------------------------------------------------------------------------------
setDataForAttachedDevicesTab() {
  local fileName="$1"
  local yadColumns=("Name" "Size" "Type" "Mount PT")
  local -A tabOpts=(["grid-lines"]="both" ["list-type"]="checklist" ["notebook"]=true)
  local cmdOutput=$(lsblk -lnp -I 2,3,8,9,22,34,56,57,58,65,66,67,68,69,70,71,72,91,128,129,130,131,132,133,134,135,259 | awk '{print $1,$4,$6,$7}'| column -t | sed -e 's/^ *//g' | sed -e 's/  /:/g' | sed -e 's/:::*/:/g')
  local attachedDevices=()
  local cols=()
  local tabData=()

  IFS=$'\n' read -d '' -r -a attachedDevices <<< "$cmdOutput"
  for row in "${attachedDevices[@]}"; do
    IFS=':' read -a cols <<< "$row"

    for col in "${cols[@]}"; do
      tabData+=($(trimString "$col"))
    done

    if [ ${#cols[@]} -lt 4 ]; then
      tabData+=(" ")
    fi
  done
  local lines=$(printf "\n%s" "${tabData[@]}")
  lines=${lines:1}
  echo -e "$lines" > "$fileName"
}

#---------------------------------------------------------------------------------------
#      METHOD:                  setDataForSelectDeviceTab
# DESCRIPTION: Set the data that can be selected on the Select Device tab
#  Required Params:
#      1)   fileName - name of the file containing the data
#      2) dlgChoices - array of device names that can be partitioned
#---------------------------------------------------------------------------------------
setDataForSelectDeviceTab() {
  local fileName="$1"
  local -n dlgChoices=$2
  local tabData=()
  for devName in "${dlgChoices[@]}"; do
    tabData+=(FALSE)
    tabData+=("$devName")
  done

  tabData[0]=TRUE
  local lines=$(printf "\n%s" "${tabData[@]}")
  lines=${lines:1}
  echo -e "$lines" > "$fileName"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:               getSelectedOptionsFromNotebook
# DESCRIPTION: Get the options that were selected on each tab.  These are values within
#              the first column of a "list" dialog/widget, or the text entered on an
#              "entry" dialog/widget.
#      RETURN: concatenated string
#  Required Params:
#      1)         numTabs - total number of tabs
#      2) dataFileNameFmt - format of the data file name that will hold the
#                           selected and/or entered values
#---------------------------------------------------------------------------------------
function getSelectedOptionsFromNotebook() {
  local numTabs=$1
  local dataFileNameFmt="$2"
  local dataFN=""
  local aryElems=()

  for tabNum in $(eval echo "{1..${numTabs}}"); do
    dataFN=$(printf "$dataFileNameFmt" ${tabNum})
    aryElems+=("$dataFN")
  done

  local elems=$(printf " %s" "${aryElems[@]}")
  elems=${elems:1}

  aryElems=()
  local yadChoices=$(eval "cat $elems")
  for row in "${yadChoices[@]}"; do
    if [[ "$row" =~ "|" ]]; then
      aryElems+=($(echo "$row" | cut -d"|" -f2))
    else
      aryElems+=($(echo "$row"))
    fi
  done

  yadChoices=$(printf " %s" "${aryElems[@]}")
  yadChoices=${yadChoices:1}
  echo "$yadChoices"
}

#---------------------------------------------------------------------------------------
#      METHOD:                  appendOptionsForRadiolist
# DESCRIPTION: Append the common options when displaying a list dialog
#  Required Params:
#      1)  dialogText - text to display above the list of choices
#      2) dialogTitle - title of the dialog/window
#      3)  dialogOpts - array to append the options to
#---------------------------------------------------------------------------------------
appendOptionsForRadiolist() {
  local dialogText=$1
  local dialogTitle=$2
  local -n dialogOpts=$3
  local -A opts=(["buttons-layout"]="center" ["center"]=true
  ["grid-lines"]="both" ["list-type"]="radiolist" ["width"]=610
  ["text"]="<span font_weight='bold' font_size='medium'>$dialogText</span>"
  ["text-align"]="left" ["title"]="$dialogTitle"
  )

  for key in "${!opts[@]}"; do
    if [ ! ${dialogOpts["$key"]+_} ]; then
      dialogOpts[$key]=$(echo "${opts[$key]}")
    fi
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                     setDataForRadiolist
# DESCRIPTION: Set the data to be displayed in a yad "radiolist" dialog
#  Required Params:
#      1) dialogChoices - array of the options to choose from
#      2)      listData - array of the
#---------------------------------------------------------------------------------------
setDataForRadiolist() {
  local -n dialogChoices=$1
  local -n listData=$2
  local defaultChoice=""

  if [ "$#" -gt 2 ]; then
    defaultChoice="$3"
  fi

  for opt in "${dialogChoices[@]}"; do
    if [ "$opt" == "$defaultChoice" ]; then
      listData+=(TRUE)
    else
      listData+=(FALSE)
    fi
    listData+=("$opt")
  done

  if [ ${#defaultChoice} -lt 1 ]; then
    listData[0]=TRUE
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getValueFromRadiolist
# DESCRIPTION: Get the value selected from yad "radiolist" dialog
#      RETURN: value selected or blank if "Cancel" button was clicked
#  Required Params:
#      1)    helpText - text to display in the yad "text-info" dialog for viewing the help
#      2)     yadOpts - array of options for the dialog
#      3)  yadButtons - array of button titles to display at the bottom of the dialog
#      4)  yadColumns - array of column header names
#      5)  dialogData - array of choices to display in the dialog
#  Optional Params:
#      6) associative array of key/value pairs for the options of the help dialog
#---------------------------------------------------------------------------------------
function getValueFromRadiolist() {
  local helpText="$1"
  local -n yadOpts=$2
  local -n yadButtons=$3
  local -n yadColumns=$4
  local -n dialogData=$5
  local -A helpOpts=(["buttons-layout"]="center" ["window-icon"]="gtk-help.png" ["title"]="Help:  ${yadOpts[title]}"
    ["center"]=true ["height"]=350 ["width"]=610 ["wrap"]=true ["fontname"]="Sans 12")
  local selectedValue=""
  local btns=("Cancel" "Exit" "Skip")
  local pos=-1
  local sep=","

  if [ "$#" -gt 5 ]; then
    local -n opts=$6
    local key=""
    for key in "${!opts[@]}"; do
      helpOpts[$key]=$(echo "${opts[$key]}")
    done
  fi

  for btnName in "${btns[@]}"; do
    pos=$(findPositionForString yadButtons "$btnName")
    if [ ${pos} -gt -1 ]; then
      break
    fi
  done

  while true; do
    yadDlgData=$(getChoicesFromListDialog yadOpts yadButtons yadColumns dialogData)

    readarray -t dialogVals <<< "${yadDlgData//$sep/$'\n'}"

    case ${dialogVals[0]} in
      ${DIALOG_OK})
        if [ ${yadOpts["ret-separator"]+_} ]; then
          selectedValue="${yadDlgData:2}"
        else
          selectedValue=$(echo "${dialogVals[1]}")
        fi
        break
      ;;
      ${DIALOG_CANCEL}|${DIALOG_EXIT})
        selectedValue=""
        if [ ${pos} -gt -1 ]; then
          break
        fi
      ;;
      ${DIALOG_KEEP})
        selectedValue="<Keep>"
        break
      ;;
      ${DIALOG_HELP})
        showTextInfoDialog "$helpText" helpOpts
      ;;
    esac
  done

  echo "$selectedValue"
}

#---------------------------------------------------------------------------------------
#      METHOD:                   setDataForMultiColList
# DESCRIPTION: Set the data to be displayed in a yad "list" dialog for more than
#              2 columns
#  Required Params:
#      1)  dialogChoices - array of the options to choose from
#      2) dlgChoiceDescs - associative array of the key/value pairs of descriptions
#      3)       listData - array that the data for the dialog will be appended to
#---------------------------------------------------------------------------------------
setDataForMultiColList() {
  local -n dialogChoices=$1
  local -n dlgChoiceDescs=$2
  local -n listData=$3
  local checklistVals=()
  local chkSep="|"

  if [ "$#" -gt 3 ]; then
    readarray -t checklistVals <<< "${4//$chkSep/$'\n'}"
  fi

  for opt in "${dialogChoices[@]}"; do
    if [ ${#checklistVals[@]} -gt 0 ]; then
      local pos=$(findPositionForString checklistVals "$opt")
      if [ ${pos} -gt -1 ]; then
        listData+=(TRUE)
      else
        listData+=(FALSE)
      fi
    else
      listData+=(FALSE)
    fi

    listData+=("$opt")
    listData+=("${dlgChoiceDescs[$opt]}")
  done

  if [ ${#checklistVals[@]} -lt 1 ]; then
    listData[0]=TRUE
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                  executeLinuxCommands
# DESCRIPTION: Display a yad "progress indication" dialog (i.e. progress bar) while
#              executing a set of linux commands and logging the results.
#  Required Params:
#      1)       pbTitle - String to be displayed at the top of the dialog box.
#      2)        pbText - Text to be displayed inside of the dialog box.
#      3)   cmdKeyArray - Array of keys to the associative arrays
#      4) cmdTitleArray - Associative array containing the text to be displayed
#                         for the commands in the progress bar
#      5)      cmdArray - Associative array containing the linux commands to be executed
#  Optional Params:
#      6) concatenated string of additional options for YAD separated by '|'
#---------------------------------------------------------------------------------------
executeLinuxCommands() {
  local pbTitle="$1"
  local pbText=$(escapeSpecialCharacters "$2")
  local -n cmdKeyArray=$3
  local -n cmdTitleArray=$4
  local -n cmdArray=$5
  local cmdNum=1
  local pbPerc=0
  local cmd=""
  local evalCmds=()
  local pbOpts=""
  local debugFlag=$(echo 'false' && return 1)

  pbText="<span font_weight='bold' font_size='medium'>$pbText</span>"

  cmdKeyArray+=("done")
  cmdTitleArray["done"]="All commands have been executed!!!!"

  if [ "$AALP_GYGI_YAD" == "debug" ]; then
    debugFlag=$(echo 'true' && return 0)
  fi

  if [ "$#" -gt 5 ]; then
    local yadOpts=()
    local splitArray=()
    local sep="|"

    pbOpts="$6"

    readarray -t splitArray <<< "${pbOpts//$sep/$'\n'}"

    for val in "${splitArray[@]}"; do
      yadOpts+=("$val")
    done

    if [ ${#yadOpts[@]} -gt 0 ]; then
      pbOpts==$(printf " %s" "${yadOpts[@]}")
      pbOpts="${pbOpts:1}"
    else
      pbOpts=""
    fi
  fi

  local totCmds=${#cmdKeyArray[@]}
  (
    sleep 0.5
    for pbKey in "${cmdKeyArray[@]}"; do
      if [ ${pbPerc} -lt 100 ]; then
        echo $pbPerc
      fi
      echo "# ${cmdTitleArray[$pbKey]}..."
      cmd="${cmdArray[$pbKey]}"

      case $pbKey in
        "done")
          if [ ${#cmd} -gt 0 ]; then
            clog_info "${cmdTitleArray[$pbKey]}\nResult of command [$cmd]" >> "$INSTALLER_LOG"
            ${cmd} &>> "$INSTALLER_LOG"
          else
            clog_warn "${cmdTitleArray[$pbKey]}" >> "$INSTALLER_LOG"
          fi
          sleep 1
        ;;
        *)
          IFS=$'\t' read -a evalCmds <<< "$cmd"
          for cmd in "${evalCmds[@]}"; do
            clog_info "Executing command [$cmd]" >> "$INSTALLER_LOG"

            if ${debugFlag}; then
              clog_warn "debugFlag=[$debugFlag] - sleep 5" >> "$INSTALLER_LOG"
              sleep 5
            else
              eval ${cmd} &>> "$INSTALLER_LOG"
            fi

            clog_info "Command [$cmd] is DONE" >> "$INSTALLER_LOG"
          done
          cmdNum=$(expr $cmdNum + 1)
          pbPerc=$(printf "%.0f" $(echo "scale=3; $cmdNum / $totCmds * 100" | bc))
        ;;
      esac

      sleep 0.5
    done
  ) | $DIALOG --progress ${pbOpts} --center --percentage=0 --log-expanded --enable-log="Progress Bar Log" \
              --log-height=175 --width=500 --height=300 --title="$pbTitle" --size=fit --text="$pbText" \
              --text-align=center --no-buttons --auto-close --window-icon="$ICONS_PATH/app-icon.png" 2> /dev/null
}

#---------------------------------------------------------------------------------------
#      METHOD:               getSelectedLinuxPartitionTool
# DESCRIPTION: Get the linux tool that will be used to create and/or manipulate
#              the partition table
#      RETURN: selected linux tool in the global variable "mbSelVal"
#  Required Params:
#      1)     dialogTitle - title of the dialog/window
#      2)   dialogTextHdr - Text inside the dialog at the very top.
#      3)      linuxTools - array of linux partition tool names
#      4)  linuxToolDescs - associative array of key/value pairs that describe the type
#                           of interaction for each tool
#---------------------------------------------------------------------------------------
getSelectedLinuxPartitionTool() {
  local dialogTitle="$1"
  local textArray=(
    "                                               $2" " "
    "Partitioning tools that are used to create and/or manipulate the"
    "device partition tables and partitions. " " "
    "Click the &quot;Help&quot; button for descriptions of each tool."
    "------------------------------------------------------------------------------" " "
    "Select a partition tool:"
  )
  local -n linuxTools=$3
  local -n linuxToolDescs=$4
  local dialogText=$(getTextForDialog "${textArray[@]}")
  local dlgButtons=("Ok" "Cancel" "Help")
  local dlgColumns=("Radio Btn" "Tool Name" "Type of Interaction")
  local -A dlgOts=(["height"]=475 ["width"]=475 ["window-icon"]="partition-tool.png")
  local dlgData=()
  local helpText=$(getHelpTextForTool)
  local -A helpDlgOpts=(["width"]=675 ["title"]="Help:  $2")

  appendOptionsForRadiolist "$dialogText" "$dialogTitle" dlgOts
  setDataForMultiColList linuxTools linuxToolDescs dlgData

  mbSelVal=$(getValueFromRadiolist "$helpText" dlgOts dlgButtons dlgColumns dlgData helpDlgOpts)
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getOptionsForListDialog
# DESCRIPTION: Get the command to display a yad "form" dialog
#      RETURN: concatenated string
#  Required Params:
#      1) formOpts - associative array that contain the available options
#      2) formFlds - array of fields for the form defined by LABEL[:TYPE] where
#                     * LABEL - string
#                     * TYPE - optional type of form field
#                              (visit https://www.systutorials.com/docs/linux/man/1-yad/#lbAO)
#   Optional Param:
#      3) array of button titles that map to the icon in the global
#         associative array var "BUTTON_ICONS"
#  Options for the yad "form" dialog:
#               "align" - Set alignment of field labels. Possible types are left,
#                         center or right.  Default is left.
#             "columns" - Set number of columns in form. Fields will be placed from
#                         top to bottom.
#      "item-separator" - Set separator character for combo-box or scale values.
#                         Default is '!'.
#           "separator" - Set output separator character/string.
#                         Default is '|'.
#               "field" - form field defined by LABEL and optional TYPE.
#                         Default TYPE is visible input.
#---------------------------------------------------------------------------------------
function getCommandForFormDialog() {
  local -n formOpts=$1
  local -n formFlds=$2
  local keyArray=("align" "columns" "item-separator" "separator")
  local yadCmdOpts="$DIALOG --form"
  local yadCmdOpts+=$(getGeneralOptionsForDialogs formOpts)

  for key in "${keyArray[@]}"; do
    if [ ${formOpts["$key"]+_} ]; then
      case "$key" in
        "align")
          yadCmdOpts+=$(echo " --$key=${formOpts[$key]}")
        ;;
        "columns")
          yadCmdOpts+=$(echo " --$key=${formOpts[$key]}")
        ;;
        "item-separator")
          yadCmdOpts+=$(echo " --$key=${formOpts[$key]}")
        ;;
        "separator")
          yadCmdOpts+=$(echo " --$key=${formOpts[$key]}")
        ;;
      esac
    fi
  done

  if [ "$#" -gt 2 ]; then
    local -n dlgBtns=$3
    yadCmdOpts+=$(getButtonsForDialog dlgBtns)
  fi

  for fld in "${formFlds[@]}"; do
    yadCmdOpts+=$(echo " --field=$fld")
  done

  echo "$yadCmdOpts"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getCmdToShowHelpNotebook
# DESCRIPTION: Get the command to display a yad "dialog" of type notebook that will
#              contain the following tabs:
#              Tab #1) the partition table and text in a yad "dialog" of type "list"
#              Tab #2) the text for the help section in a yad "dialog" of type "text-info"
#      RETURN: concatenated string
#  Required Params:
#      1) helpText - text to display in the help "dialog"
#      2)    ptHdr - text to display at the top of the partition table tab
#      3) helpOpts - associative array that contain the available options for the help tab
#      4)  tabData - array of data to display on the partition table tab
#      5)   nbOpts - associative array that contain the available options for a notebook
#---------------------------------------------------------------------------------------
function getCmdToShowHelpNotebook() {
  local helpText="$1"
  local -A ptTabOpts=(["grid-lines"]="both" ["no-selection"]=true
    ["text"]="<span font_weight='bold' font_size='medium'>$2</span>" ["text-align"]="left")
  local -n helpOpts=$3
  local -n yadTabData=$4
  local -n nbOpts=$5
  local tabNum=2
  local yadBtns=("Exit")
  local nbKeyPlug=$(getSixDigitRandomNum)
  local tabOutput=$(printf " < \"%s\" &> /tmp/res1 &" "$DATA_FILE_NAME")
  local yadColumns=("${PART_LAYOUT_COLS[@]}")
  local yadCmd=$(getTabCmdForPartTable ${nbKeyPlug} 1 "$tabOutput" yadColumns yadTabData ptTabOpts)
  local keys=()
  yadCmd+=" "

  if [ ${helpOpts["helpTextKeys"]+_} ]; then
    IFS=$'\t' read -a keys <<< "${helpOpts["helpTextKeys"]}"
    for htk in "${keys[@]}"; do
      helpOpts["helpTextFN"]=$(printf "${NB_TAB_DATA_FNS[helpText]}" ${tabNum})
      yadCmd+=$(getCmdForTextInfoTab ${nbKeyPlug} "${helpOpts["$htk"]}" ${tabNum} helpOpts)
      tabNum=$(expr $tabNum + 1)
    done
  else
    helpOpts["helpTextFN"]=$(printf "${NB_TAB_DATA_FNS[helpText]}" 1)
    yadCmd+=$(getCmdForTextInfoTab ${nbKeyPlug} "$helpText" ${tabNum} helpOpts)
  fi

  if [ ${helpOpts["helpTabKeys"]+_} ]; then
    yadCmd+=$(echo " $DIALOG --notebook --key=${nbKeyPlug} --tab=\"Partition Table\"")
    IFS=$'\t' read -a keys <<< "${helpOpts["helpTabKeys"]}"
    for htk in "${keys[@]}"; do
      yadCmd+=$(printf " --tab=\"%s\"" "${helpOpts["$htk"]}")
    done
  else
    yadCmd+=$(echo " $DIALOG --notebook --key=${nbKeyPlug} --tab=\"Partition Table\" --tab=\"Help\"")
  fi

  yadCmd+=$(getOptionsForNotebook nbOpts)
  yadCmd+=$(getButtonsForDialog yadBtns)

  echo "$yadCmd"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getCmdForRadioListTab
# DESCRIPTION:  Get the command to display a yad "list" dialog within a tab of notebook
#  Required Params:
#      1)      nbKeyPlug - integer value for the notebook key & plug of its tabs
#      2)         tabNum - tab number
#      3)      tabOutput - contains the name of the data file to load the tab with
#                          as well as the file name of the output generated by the tab
#---------------------------------------------------------------------------------------
function getCmdForRadioListTab() {
  local nbKeyPlug="$1"
  local tabNum=$2
  local tabOutput="$3"
  local dataFileName=$(echo "$tabOutput" | cut -d'"' -f2)
  local -A tabListOpts=(["grid-lines"]="both" ["text-align"]="left" ["notebook"]=true
    ["text"]="<span font_weight='bold' font_size='medium'>${nbFuncParams[dialogText]}</span>"
    ["image"]="$YAD_GRAPHICAL_PATH/images/${nbFuncParams[image]}" ["image-on-top"]=true
    ["separator"]="\"$FORM_FLD_SEP\"")
  local rowKeys=()
  local tabListData=()
  local cols=()

  if [ ${nbFuncParams["removeKeys"]+_} ]; then
    readarray -t rowKeys <<< "${nbFuncParams["removeKeys"]}"
    for rowKey in "${rowKeys[@]}"; do
      unset tabListOpts["$rowKey"]
    done
  fi

  if [ ${nbFuncParams["list-type"]+_} ]; then
    tabListOpts["list-type"]="${nbFuncParams[list-type]}"
  fi

  IFS=$'\t' read -a rowKeys <<< "${nbFuncParams[choices]}"
  for rowKey in "${rowKeys[@]}"; do
    rowkey=$(trimString "$rowKey")
    if [ "$rowKey" == "${nbFuncParams["defaultChoice"]}" ]; then
      tabListData+=(TRUE)
    else
      tabListData+=(FALSE)
    fi
    tabListData+=("$rowKey")
    IFS=$'\t' read -a cols <<< "${nbFuncParams[$rowKey]}"
    tabListData+=("${cols[@]}")
  done

  if [ ! ${nbFuncParams["defaultChoice"]+_} ]; then
    tabListData[0]=TRUE
  fi
  local lines=$(printf "\n%s" "${tabListData[@]}")
  lines=${lines:1}
  echo -e "$lines" > "$dataFileName"

  local yadCmd=$(echo "$DIALOG --plug=${nbKeyPlug} --tabnum=${tabNum}")
  yadCmd+=$(getOptionsForListDialog tabListOpts)
  yadCmd+=$(getColumnsForListDialog listColHdrs)
  yadCmd+="$tabOutput"

  echo "$yadCmd"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     getCmdForTextInfoTab
# DESCRIPTION: Get the command for the tab of a yad "notebook" dialog that will display
#              the text in the tab that has a "text-info" widget
#      RETURN: concatenated string
#  Required Params:
#      1) nbKeyPlug - integer value for the notebook key & plug of its tabs
#      2)   tabText - text to display in the widget
#      3)   tabOpts - options for the "dialog"
#---------------------------------------------------------------------------------------
function getCmdForTextInfoTab() {
  local nbKeyPlug="$1"
  local tabText="$2"
  local tabNum=$3
  local -n tabOpts=$4
  local textFileName="$TEXT_INFO_FILE_NAME"
  local tabCmd=$(echo "$DIALOG --plug=${nbKeyPlug} --tabnum=${tabNum}")

  if [ ${tabOpts["helpTextFN"]+_} ]; then
    textFileName="${tabOpts[helpTextFN]}"
  fi

  echo -e "$tabText" > "$textFileName"

  tabCmd+=$(getOptionsForTextInfo tabOpts "$tabText")
  tabCmd+=$(echo " --text-info < $textFileName &> /tmp/res${tabNum} &")

  echo "$tabCmd"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    showNotebookDialog
# DESCRIPTION: Show the tabs of a yad "notebook" dialog
#      RETURN: button code and name that was clicked ','
#  Required Params:
#      1)    yadCmd - command to display the tabs of a notebook
#      2) nbHelpCmd - command to display the help dialog
#---------------------------------------------------------------------------------------
function showNotebookDialog() {
  local yadCmd="$1"
  local nbHelpCmd=$(trimString "$2")
  local btnClick=0
  local nbClick=""

  while true; do
    eval "$yadCmd"
    btnClick=$?
    case ${btnClick} in
      ${DIALOG_OK})
        nbClick=$(printf "%d,Ok" ${btnClick})
        break
      ;;
      ${DIALOG_CANCEL})
        nbClick=$(printf "%d,Cancel" ${btnClick})
        break
      ;;
      ${DIALOG_HELP})
        if [ ${#nbHelpCmd} -gt 0 ]; then
          eval "$nbHelpCmd"
        else
          nbClick=$(printf "%d,Help" ${btnClick})
          break
        fi
      ;;
      ${DIALOG_EXTRA})
        nbClick=$(printf "%d,Extra" ${btnClick})
        break
      ;;
      ${DIALOG_BROWSER})
        nbClick=$(printf "%d,Browser" ${btnClick})
        break
      ;;
      ${DIALOG_LIST})
        nbClick=$(printf "%d,List" ${btnClick})
        break
      ;;
      ${DIALOG_EXIT})
        nbClick=$(printf "%d,Exit" ${btnClick})
        break
      ;;
      ${DIALOG_COMMIT})
        nbClick=$(printf "%d,Done" ${btnClick})
        break
      ;;
      ${DIALOG_FORM})
        nbClick=$(printf "%d,Form" ${btnClick})
        break
      ;;
      ${DIALOG_EDIT})
        nbClick=$(printf "%d,Edit" ${btnClick})
        break
      ;;
      ${DIALOG_ALL})
        nbClick=$(printf "%d,All" ${btnClick})
        break
      ;;
      ${DIALOG_RESET})
        nbClick=$(printf "%d,Reset" ${btnClick})
        break
      ;;
      ${DIALOG_ROOT})
        nbClick=$(printf "%d,ROOT" ${btnClick})
        break
      ;;
      ${DIALOG_ESC})
        nbClick=$(printf "%d,ESC" ${DIALOG_CANCEL})
        break
      ;;
      ${DIALOG_SSKIP})
        nbClick=$(printf "%d,Skip" ${DIALOG_SSKIP})
        break
      ;;
    esac
  done

  echo "$nbClick"
}

#---------------------------------------------------------------------------------------
#      METHOD:                      removeTabFiles
# DESCRIPTION: Remove all the files that were generated for the tabs of the notebook
#---------------------------------------------------------------------------------------
removeFilesForYAD() {
  local maxNumTabs=5
  local fileName=""
  local cmdNum=0
  local fileKeys=("${!NB_TAB_DATA_FNS[@]}")

  rm -rf "$DATA_FILE_NAME"
  rm -rf "$TEXT_INFO_FILE_NAME"

  for cmdKey in "${NB_CMD_KEYS[@]}"; do
    cmdNum=$(expr $cmdNum + 1)
    fileName=$(printf "${NB_TAB_DATA_FNS["helpText"]}" ${cmdNum})
    rm -rf "$fileName"
    for tabNum in $(eval echo "{1..${maxNumTabs}}"); do
      for key in "${fileKeys[@]}"; do
        fileName=$(printf "${NB_TAB_DATA_FNS["$key"]}" ${tabNum})
        rm -rf "$fileName"
      done
    done
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                       showErrorDialog
# DESCRIPTION: Displays the appropriate error message
#  Required Params:
#      1)   dialogTitle - String to be displayed at the top of the dialog window.
#      2) dialogTextHdr - String to be displayed at the top of the text area.
#      3)      errorMsg - Error message
#---------------------------------------------------------------------------------------
showErrorDialog() {
  local textArray=( "$3" "$BORDER_WIDTH_600" " ")
  local dialogText=$(getTextForDialog "${textArray[@]}")
  local -A yadOpts=(["buttons-layout"]="center" ["image"]="$YAD_GRAPHICAL_PATH/images/error-icon.png"
  ["image-on-top"]=true ["title"]="$1" ["window-icon"]="error-window-icon.png"
  ["text"]="<span font_weight='bold' font_size='large'>Error:  $2</span>" ["text-align"]="left"
  ["height"]=450 ["width"]=600 ["wrap"]=true ["fontname"]="Sans 12" ["center"]=true ["wrap"]=true ["size"]="fit")

  local dlgBtns=("Exit")

  showTextInfoDialog "$dialogText" yadOpts dlgBtns
}

#---------------------------------------------------------------------------------------
#      METHOD:              setSelectedTabOptionsFromNotebook
# DESCRIPTION: Get the options that were selected within each tab of a
#              yad "notebook" dialog.
#  Required Params:
#      1)         numTabs - total number of tabs
#      2) dataFileNameFmt - format of the data file name that will hold the
#                           selected and/or entered values
#---------------------------------------------------------------------------------------
setSelectedTabOptionsFromNotebook() {
  local numTabs=$1
  local dataFileNameFmt="$2"

  TAB_SEL_VALS=()

  for tabNum in $(eval echo "{1..${numTabs}}"); do
    local dataFN=$(printf "$dataFileNameFmt" ${tabNum})
    local tabKey=$(printf "$TAB_KEY_FMT" ${tabNum})
    TAB_SEL_VALS["$tabKey"]=$(cat "$dataFN")
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                     showProgBarAndTail
# DESCRIPTION: Show a yad "progress" dialog and a terminal window that tails the log
#              file to display the progress of the AUR packages getting installed and/or
#              the commands getting executed.
#---------------------------------------------------------------------------------------
showProgBarAndTail() {
  ./install-aur-package.sh
}
