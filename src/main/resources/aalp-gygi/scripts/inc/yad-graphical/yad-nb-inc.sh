#!/bin/bash
#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# globals
#===============================================================================
declare VALIDATION_KEYS=("valDevName" "blContinue")

#===============================================================================
# functions/methods
#===============================================================================

#---------------------------------------------------------------------------------------
#    FUNCTION:               getSubmittedDataFromNotebook
# DESCRIPTION: Get either:
#                 A) the name and human-readable size to create either a logical volume
#                    or partition from the radio list and/or form tabs of the
#                    yad "notebook" dialog
#                 B) within the radiolist tab of the yad "notebook" dialog, the key
#                    to the row containing the logical volume or partition from the
#                    partition
#                 C) the value from clicking either the Save, Exit, or Cancel buttons
#      RETURN: concatenated string of the values submitted separated by '\t'
#  Required Params:
#      1) dialogFuncParams - associative array of key/value pairs of parameters for the function
#                           (see calling function for a description of the keys & values)
#---------------------------------------------------------------------------------------
function getSubmittedDataFromNotebook() {
  local -n dialogFuncParams=$1
  local partitionLayout="${dialogFuncParams[partitionLayout]}"
  local ptTabData=()
  local summaryTabData=()
  local nbSubData=""
  local cmdArray=()
  local -A nbCmdArray=()
  local -A nbHelpCmdArray=()
  local cmdKey="${NB_CMD_KEYS[0]}"
  local lastTabName="${dialogFuncParams[lastTab]}"

  setDataForSumTblTab

  if [ ${dialogFuncParams["statsTable"]+_} ]; then
    appendStatsToSumTblTab "${dialogFuncParams[statsTable]}" summaryTabData
  fi

  setDataForPartTblTab "$partitionLayout"

  setCmdToShowMainNB "$cmdKey" nbCmdArray summaryTabData ptTabData dialogFuncParams

  if [ ${dialogFuncParams["$cmdKey"]+_} ]; then
    nbHelpCmdArray["$cmdKey"]="${dialogFuncParams[$cmdKey]}"
    unset dialogFuncParams["$cmdKey"]
  fi
  unset dialogFuncParams["partitionLayout"]
  unset dialogFuncParams["helpText"]

  if [ "$lastTabName" == "createCustom" ] || [ "$lastTabName" == "selectSize" ]; then
    cmdKey="${NB_CMD_KEYS[1]}"
    if [ "$lastTabName" == "createCustom" ]; then
      local title=$(printf "%32sCreate a Custom \"%s\"" " " "$deviceType")
      local textArray=("$title" " " " " " " "Enter a valid name and the size to be allocated:")
      local dialogText=$(printf "\n%s" "${textArray[@]}")

      dialogFuncParams["dialogText"]=$(escapeSpecialCharacters "${dialogText:1}")
      dialogFuncParams["${VALIDATION_KEYS[0]}"]=$(echo 'true' && return 0)
      dialogFuncParams["tabName"]=$(echo "Create &quot;${dialogFuncParams[deviceType]}&quot; Form")
    fi
    setCmdToShowFormInNB "$cmdKey" nbCmdArray summaryTabData ptTabData dialogFuncParams
    dialogFuncParams["lastTab"]="$lastTabName"
    nbHelpCmdArray["$cmdKey"]="${dialogFuncParams[$cmdKey]}"
    unset dialogFuncParams["$cmdKey"]
  fi

  if [ ! ${dialogFuncParams["yesNoFlag"]+_} ]; then
    dialogFuncParams["yesNoFlag"]=$(echo 'false' && return 1)
  fi

  nbSubData=$(getSubmittedValuesFromNotebook dialogFuncParams nbCmdArray nbHelpCmdArray)

  if typeset -f swapImages > /dev/null; then
    swapImages "${dialogFuncParams[window-icon]}" "${dialogFuncParams[image]}"
  fi

  echo "$nbSubData"
}

#---------------------------------------------------------------------------------------
#      METHOD:                    setDataForSumTblTab
# DESCRIPTION: Set the values that will be displayed on the "Summary" tab
#---------------------------------------------------------------------------------------
setDataForSumTblTab() {
  local rows=()

  setSummaryOfCurLayout rows
  for row in "${rows[@]}"; do
    IFS=$";" read -a cols <<< "$row"
    summaryTabData+=("${cols[@]}")
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                   appendStatsToSumTblTab
# DESCRIPTION: Set the values that will be displayed on the "Summary" tab
#  Required Params:
#      1)      stats - string of statistics where each row is separated by '\n'
#      2) sumTblData - array that will have the statistics appended
#---------------------------------------------------------------------------------------
appendStatsToSumTblTab() {
  local stats="$1"
  local -n sumTblData=$2
  local rows=()

  IFS=$'\n' read -d '' -a rows <<< "$stats"
  for row in "${rows[@]}"; do
    IFS=$";" read -a cols <<< "$row"
    sumTblData+=("${cols[@]}")
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                   setDataForPartTblTab
# DESCRIPTION: Set the values that will be displayed on the "Partition Table" tab
#  Required Params:
#      1) partitionLayout - string containing header, logical volumes and partitions
#---------------------------------------------------------------------------------------
setDataForPartTblTab() {
  local partitionLayout="$1"
  local partTblRows=()
  local startRow=1

  IFS=$'\n' read -d '' -a partTblRows <<< "$partitionLayout"

  if [[ "$partitionLayout" =~ "Volume" ]]; then
    startRow=2
  fi

  for row in "${partTblRows[@]:${startRow}}"; do
    IFS=$";" read -a cols <<< "$row"
    ptTabData+=("${cols[@]}")
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                    setCmdToShowMainNB
# DESCRIPTION: Get the command to display the main (i.e. first one displayed)
#              yad "notebook" dialog.  For example, the last tab might contain a radio list
#              containing choices of sizes to allocate the logical volume or partition.
#  Required Params:
#      1)        cmdKey - the key to associative array containing the notebook & help dialogs
#      2) assocCmdArray - associative array of key/value pairs for the notebook commands
#      3)    tabDataSum - array containing the rows of data for the summary tab
#      4)     tabDataPT - array containing the rows of data for the partition table tab
#      5)    mthdParams - associative array of key/value pairs of parameters for the
#                         method (see calling function for a description of the keys & values)
#---------------------------------------------------------------------------------------
setCmdToShowMainNB() {
  local cmdKey="$1"
  local -n assocCmdArray=$2
  local -n tabDataSum=$3
  local -n tabDataPT=$4
  local -n mthdParams=$5
  local yadCmdArray=()
  local hasCancelBtn=$(echo 'false' && return 1)
  local nbKeyPlug=$(getSixDigitRandomNum)
  local tblHdr=$(getHeaderForTable "${mthdParams[partitionLayout]}")

  if [ ! ${mthdParams["nocancel"]+_} ]; then
    hasCancelBtn=$(echo 'true' && return 0)
  fi

  mthdParams["dataFileNameFmt"]="${NB_TAB_DATA_FNS[$cmdKey]}"

  if [ ! ${mthdParams["numTabs"]+_} ]; then
    mthdParams["numTabs"]=3
  elif [ ${mthdParams["numTabs"]} -lt 3 ]; then
    mthdParams["yesNoFlag"]=$(echo 'true' && return 0)
  fi

  mthdParams["hasCancelBtn"]=${hasCancelBtn}
  mthdParams["tblHdr"]=$(escapeSpecialCharacters "$tblHdr")
  mthdParams["image"]=$(cat "$DIALOG_IMG_ICON_FILE" | cut -d',' -f1)
  mthdParams["window-icon"]=$(cat "$DIALOG_IMG_ICON_FILE" | cut -d',' -f2)

  setCmdToShowNotebook ${nbKeyPlug} mthdParams tabDataPT tabDataSum yadCmdArray
  assocCmdArray["$cmdKey"]=$(printf " %s" "${yadCmdArray[@]}")
  assocCmdArray["$cmdKey"]=${assocCmdArray["$cmdKey"]:1}

  if [ ${#mthdParams["helpText"]} -gt 0 ] || [ ${mthdParams["helpTextKeys"]+_} ]; then
    mthdParams["$cmdKey"]=$(getHelpNotebookDialogCmd tabDataPT)
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                   setCmdToShowNotebook
# DESCRIPTION: Set the command to display the a yad "notebook" dialog with
#              the following tabs:
#              #1)         "Summary" - a summary of the logical volumes & partitions
#              #2) "Partition Table" - the rows of logical volumes & partitions
#              #3) If displaying 3 tabs, then
#                     If the key "lastTab" is set, then
#                        create the tab command to display what is needed
#                      Otherwise,
#                         "Create Form" - form to enter the name and human-readable size
#                                         of a logical volume or partition
#  Required Params:
#      1)      nbKeyPlug - integer value for the notebook key & plug of its tabs
#      2)   nbFuncParams - associative array of key/value pairs of parameters for the method
#                          (see calling function for a description of the keys & values)
#      3) partTblTabData - array of data to display on the partition table tab
#      4)     sumTabData - array of data to display on the summary tab
#      5)   notebookCmds - array of strings containing the "yad" commands to display
#                          the notebook
#---------------------------------------------------------------------------------------
setCmdToShowNotebook() {
  local nbKeyPlug="$1"
  local -n nbFuncParams=$2
  local -n partTblTabData=$3
  local -n sumTabData=$4
  local -n notebookCmds=$5
  local tabOutput=$(echo " < \"%s\" &> ${nbFuncParams[dataFileNameFmt]} 2> /dev/null &")
  local numTabs=${nbFuncParams["numTabs"]}
  local yadTabColumns=("${PART_LAYOUT_COLS[@]}")
  local fileName=""
  local tabRedirect=""
  local nbCmd=$(echo "$DIALOG --notebook --key=${nbKeyPlug}")
  local tabCmd=""
  local tabName=""
  local -A yadTabOpts=(["grid-lines"]="both" ["no-selection"]=true ["notebook"]=true
      ["text-align"]="left" ["text"]="<span font_weight='bold' font_size='medium'>${nbFuncParams["tblHdr"]}</span>")
  local nbButtons=("Ok" "Cancel" "Help")
  local nbFormBtns=()
  local -A nbOpts=(["buttons-layout"]="center" ["center"]=true ["height"]=450 ["width"]=725
      ["title"]="${nbFuncParams["dialogBackTitle"]}" ["window-icon"]="${nbFuncParams["window-icon"]}")
  local listColHdrs=()

  if [ ${nbFuncParams["nbImage"]+_} ]; then
    nbOpts["image"]="${nbFuncParams["nbImage"]}"
    nbOpts["image-on-top"]=true
  fi

  if [ ${nbFuncParams["nbText"]+_} ]; then
    nbOpts["text"]=${nbFuncParams["nbText"]}
    nbOpts["text-align"]="left"
  fi

  if [ ${nbFuncParams["nbHeight"]+_} ]; then
    nbOpts["height"]=${nbFuncParams["nbHeight"]}
  fi

  if [ ${nbFuncParams["nbWidth"]+_} ]; then
    nbOpts["width"]=${nbFuncParams["nbWidth"]}
  fi

  if [ ${numTabs} -lt 3 ]; then
    if [ ${nbFuncParams["lastTab"]} == "partLayout" ]; then
      nbButtons=("Exit")
    else
      nbButtons=("Save" "Exit" "Cancel" "Help")
    fi
    nbOpts["title"]="${nbFuncParams[dialogTitle]}"
  else
    if [ ${nbFuncParams["nocancel"]+_} ]; then
      nbButtons=("${nbButtons[@]:0:1}" "${nbButtons[-1]}")
    elif [ ${nbFuncParams["cancel-label"]+_} ]; then
      nbButtons[-2]="${nbFuncParams[cancel-label]}"
    fi
  fi

  if [ ! ${nbFuncParams["lastTab"]+_} ]; then
    numTabs=3
  fi

  for tabNum in $(eval echo "{1..${numTabs}}"); do
    fileName=$(printf "${NB_TAB_DATA_FNS[tabData]}" ${tabNum})
    tabRedirect=$(printf "$tabOutput" "$fileName" ${tabNum})
    case ${tabNum} in
      1)
        tabCmd=$(getCmdForSummaryTableTab ${nbKeyPlug} ${tabNum} "$tabRedirect" sumTabData yadTabOpts)
        tabName="Summary"
      ;;
      2)
        tabCmd=$(getTabCmdForPartTable ${nbKeyPlug} ${tabNum} "$tabRedirect" yadTabColumns partTblTabData yadTabOpts)
        tabName="Partition Table"
      ;;
      3)
        addThirdTabToNB ${nbKeyPlug} "$tabRedirect"
        tabCmd="${nbFuncParams["tabCmd"]}"
        tabName="${nbFuncParams["tabName"]}"
        unset nbFuncParams["tabCmd"]
        unset nbFuncParams["tabName"]
      ;;
      4)
        if typeset -f addFourthTabToNB > /dev/null; then
          addFourthTabToNB ${nbKeyPlug} "$tabRedirect"
          tabCmd="${nbFuncParams["tabCmd"]}"
          tabName="${nbFuncParams["tabName"]}"
          unset nbFuncParams["tabCmd"]
          unset nbFuncParams["tabName"]
        fi
      ;;
    esac
    notebookCmds+=("$tabCmd")
    nbCmd+=$(printf " --tab=\"%s\"" "$tabName")
  done

  if [ ${nbFuncParams["lastTabActive"]+_} ]; then
    nbCmd+=$(printf " --active-tab=%d" $numTabs)
  fi

  nbCmd+=$(getOptionsForNotebook nbOpts)
  nbCmd+=$(getButtonsForDialog nbButtons)
  notebookCmds+=("$nbCmd")
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getCmdForSummaryTableTab
# DESCRIPTION: Display a summary of the progress made in the partitioning process within
#              a tab containing a yad "list" dialog/widget
#      RETURN: concatenated string for command
#  Required Params:
#      1) nbKeyPlug - integer value for the notebook key & plug of its tabs
#      2)    tabNum - tab number
#      3) tabOutput - contains the name of the data file to load the tab with
#                     as well as the file name of the output generated by the tab
#      4)   tabData - array of rows containing the data of the summary table
#      5)   tabOpts - associative array that contain's the available options
#---------------------------------------------------------------------------------------
function getCmdForSummaryTableTab() {
  local nbKeyPlug="$1"
  local tabNum=$2
  local tabOutput="$3"
  local -n tabData=$4
  local -n tabOpts=$5
  local tabColumns=("Count" "Description")
  local dataFileName=$(echo "$tabOutput" | cut -d'"' -f2)

  if [ ! -f "$dataFileName" ]; then
    local lines=$(printf "\n%s" "${tabData[@]}")
    lines=${lines:1}
    echo -e "$lines" > "$dataFileName"
  fi

  local yadCmd=$(echo "$DIALOG --plug=${nbKeyPlug} --tabnum=${tabNum}")
  yadCmd+=$(getOptionsForListDialog tabOpts)
  yadCmd+=$(getColumnsForListDialog tabColumns)
  yadCmd+="$tabOutput"

  echo "$yadCmd"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:              getSubmittedValuesFromNotebook
# DESCRIPTION: Get the data that was submitted from the notebook being displayed.
#      RETURN: concatenated or empty string
#  Required Params:
#      1)   mthdParams - associative array of key/value pairs of parameters for the
#                        method (see calling function for a description of the keys & values)
#      2)  yadCmdArray - associative array of key/value pairs for the yad commands to
#                        display the appropriate notebook
#      3) helpCmdArray - associative array of key/value pairs for the yad commands to
#                        display the appropriate "help" notebook dialog
#---------------------------------------------------------------------------------------
function getSubmittedValuesFromNotebook() {
  local -n mthdParams=$1
  local -n yadCmdArray=$2
  local -n helpCmdArray=$3
  local cmdKey="${NB_CMD_KEYS[0]}"
  local clickedVals=()
  local submittedVals=""
  local hasCancelBtn=${mthdParams["hasCancelBtn"]}
  local -A subMethodParams=(["yesNoFlag"]=${mthdParams["yesNoFlag"]} ["numCmds"]=${#yadCmdArray[@]}
    ["deviceName"]="${mthdParams[deviceName]}" ["numTabs"]=${mthdParams["numTabs"]}
    ["lastTab"]="${mthdParams["lastTab"]}")
  local validateFlag=$(echo 'false' && return 1)
  local isValid=$(echo 'false' && return 1)

  for key in "${VALIDATION_KEYS[@]}"; do
    if [ ${mthdParams["$key"]+_} ]; then
      validateFlag=$(echo 'true' && return 0)
      break
    fi
  done

  while true; do
    subMethodParams["dfnFmt"]="${NB_TAB_DATA_FNS[$cmdKey]}"
    submittedVals=$(showNotebookDialog "${yadCmdArray[$cmdKey]}" "${helpCmdArray[$cmdKey]}")

    IFS=$',' read -d '' -a clickedVals <<< "$submittedVals"
    subMethodParams["btnID"]=${clickedVals[0]}

    case ${clickedVals[0]} in
      ${DIALOG_OK}|${DIALOG_EXTRA}|${DIALOG_BROWSER})
        submittedVals=$(getSubmittedValuesFromTabs)
        if ${validateFlag}; then
          isValid=$(validateValues "$submittedVals")
          if ${isValid}; then
            break
          fi
        else
          break
        fi
      ;;
      ${DIALOG_CANCEL})
        submittedVals=""
        #### NOTE:  if the main notebook (i.e. first one displayed) does not have a Cancel button,
        ####        redisplay the main notebook if the ESC key was pressed.  A good example of this is
        ####        when selecting a size to be allocated for the '/' or root logical volume or partition
        if [ "$cmdKey" == "${NB_CMD_KEYS[0]}" ] && ${hasCancelBtn}; then
          submittedVals=$(getSubmittedValuesFromTabs)
          break
        else
          cmdKey="${NB_CMD_KEYS[0]}"
          subMethodParams["numTabs"]=${mthdParams["numTabs"]}
        fi
      ;;
      ${DIALOG_ALL})
        submittedVals=$(getSubmittedValuesFromTabs)
        submittedVals=$(printf "All\r%s" "$submittedVals")
        break
      ;;
      ${DIALOG_EDIT}|${DIALOG_EXIT}|${DIALOG_COMMIT}|${DIALOG_RESET}|${DIALOG_ROOT})
        submittedVals="${clickedVals[1]}"
        break
      ;;
      ${DIALOG_FORM})
        cmdKey="${NB_CMD_KEYS[1]}"
        subMethodParams["numTabs"]=3
      ;;
    esac
  done

  echo "$submittedVals"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                getSubmittedValuesFromTabs
# DESCRIPTION: Get the values that were submitted from either the "radiolist" and/or
#              "form" widgets within the tabs of a yad "notebook" dialog
#      RETURN: string of the values submitted
#  Required Params:
#      1) subMthdParams - associative array of key/value pairs of parameters for the
#                         function containing:
#               "btnID" - ID of the button that was clicked to submit
#          "deviceName" - name of the logical volume or partition to create
#              "dfnFmt" - format of the data file name that will hold the selected
#                         and/or entered values
#             "numCmds" - the number of yad commands created to display a "notebook" dialog
#             "numTabs" - the total number of tabs being displayed in the
#                         "notebook" dialog
#           "yesNoFlag" - boolean flag to indicate whether or not the values should be
#                         either "YES" or "NO"
#---------------------------------------------------------------------------------------
function getSubmittedValuesFromTabs() {
  local yesNoFlag=${subMethodParams["yesNoFlag"]}
  local dfnFmt=${subMethodParams["dfnFmt"]}
  local numTabs=${subMethodParams["numTabs"]}
  local dataFN=""
  local formVals=""
  local tabKey=""
  local tabKeys=()
  local numLines=0
  local tabLines=()

  if ${yesNoFlag}; then
    formVals=$(getYesNoValue)
  else
    for tabNum in $(eval echo "{1..${numTabs}}"); do
      dataFN=$(printf "$dfnFmt" ${tabNum})
      if [ -f "$dataFN" ]; then
        numLines=$(cat "$dataFN" | wc -l)
        if [ ${numLines} -gt 1 ]; then
          tabKey=$(echo "$dataFN" | cut -d'/' -f3)
          while IFS=$'\n' read -r tabLine; do
            tabLines+=("$tabLine")
          done < "$dataFN"
          subMethodParams["$tabKey"]=$(printf "\r%s" "${tabLines[@]}")
          subMethodParams["$tabKey"]=${subMethodParams["$tabKey"]:1}
          tabKeys+=("$tabKey")
        elif [ ${numLines} -gt 0 ]; then
          tabKey=$(echo "$dataFN" | cut -d'/' -f3)
          subMethodParams["$tabKey"]=$(cat "$dataFN")
          tabKeys+=("$tabKey")
        fi
      fi
    done
    if [ ${#tabKeys[@]} -gt 0 ]; then
      subMethodParams["tabKeys"]=$(printf "\t%s" "${tabKeys[@]}")
      subMethodParams["tabKeys"]=${subMethodParams["tabKeys"]:1}
      formVals=$(getConcatenatedTabValues)
    fi
  fi

  echo "$formVals"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                        getYesNoValue
# DESCRIPTION: Get either "YES" or "NO" depending on which button was clicked
#      RETURN: "YES" or "NO"
#---------------------------------------------------------------------------------------
function getYesNoValue() {
  local btnID=${subMethodParams["btnID"]}
  local retVal=""
  local yesNo=("YES" "NO")

  case ${btnID} in
    ${DIALOG_OK})
      retVal="${yesNo[0]}"
    ;;
    ${DIALOG_CANCEL})
      retVal="${yesNo[1]}"
    ;;
  esac
  echo "$retVal"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getConcatenatedTabValues
# DESCRIPTION: Get the values of the specific fields contained within the string of
#              the submitted values that are separated by the global variable 'FORM_FLD_SEP'
#      RETURN: string of the specific fields which could be either:
#                    A) The value of the option selected in the "radio list" widget
#                    B) "YES" or "NO"
#                    C) The name of a logical volume or partition, and the human-readable
#                       size to allocate it
#  Required Params:
#      1)         btnID - ID of the button that was clicked to submit
#      2)       numCmds - the number of yad commands created to display a "notebook" dialog
#      3) submittedVals - String of the values submitted separated by the
#                         global variable 'FORM_FLD_SEP'
#      4)    deviceName - name of the logical volume or partition to create
#---------------------------------------------------------------------------------------
function getConcatenatedTabValues() {
  local tabVals=()
  local tabKeys=()
  local retVal=""
  local tabKey=""
  local lastTabName="${subMethodParams[lastTab]}"
  local tabLine=""

  IFS=$'\t' read -a tabKeys <<< "${subMethodParams["tabKeys"]}"

  if [ "$lastTabName" == "createCustom" ] || [ "$lastTabName" == "selectSize" ]; then
    retVal=$(getNameAndSize)
  else
    retVal=""
    for tabKey in "${tabKeys[@]}"; do
      tabLine=$(getConcatenatedValues)
      tabVals+=("$tabLine")
    done
    if [ ${#tabVals[@]} -gt 1 ]; then
      retVal=$(printf "\r%s" "${tabVals[@]}")
      retVal=${retVal:1}
    else
      retVal="${tabVals[0]}"
    fi
  fi

  echo "$retVal"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                       getNameAndSize
# DESCRIPTION: Get the name of the device and its human-readable size
#      RETURN: concatenated string of the values separated by '\t'
#---------------------------------------------------------------------------------------
function getNameAndSize() {
  local btnID=${subMethodParams["btnID"]}
  local numTabs=${subMethodParams["numTabs"]}
  local deviceName="${subMethodParams[deviceName]}"
  local submittedVals=""
  local lastTabVals=()
  local retVal=""
  local pattern=" |'"

  for tabKey in "${tabKeys[@]}"; do
    submittedVals+="${subMethodParams["$tabKey"]}"
  done

  IFS=$'\n' read -d '' -a lastTabVals <<< "${submittedVals//$FORM_FLD_SEP/$'\n'}"
  if [ "${lastTabVals[0]}" == "TRUE" ]; then
    retVal="${lastTabVals[2]}"
    if [ ${#lastTabVals[@]} -gt 3 ]; then
      deviceName="${lastTabVals[3]}"
    fi
  else
    deviceName="${lastTabVals[2]}"
    case ${btnID} in
      ${DIALOG_OK})
        retVal="${lastTabVals[0]}"
      ;;
      ${DIALOG_EXTRA}) #### maximum button
        retVal="${lastTabVals[3]}"
      ;;
      ${DIALOG_BROWSER}) #### other button
        retVal=$(printf "%s %s" "${lastTabVals[1]}" "${lastTabVals[-1]}")
      ;;
    esac
  fi

  if [[ ! "$retVal" =~ $pattern ]]; then
    local bytesToHR=$(humanReadableToBytes "$retVal")
    retVal=$(bytesToHumanReadable "$bytesToHR")
  fi

  retVal=$(printf "%s\t%s" "$deviceName" "$retVal")
  echo "$retVal"
}

getConcatenatedValues() {
  local tabFileLines=()
  local tabLine=""
  local vals=()
  local checkListVals=()

  IFS=$'\r' read -a tabFileLines <<< "${subMethodParams["$tabKey"]}"
  if [ ${#tabFileLines[@]} -gt 1 ]; then
    checkListVals=()
    for line in "${tabFileLines[@]}"; do
      IFS=$'\n' read -d '' -a vals <<< "${line//$FORM_FLD_SEP/$'\n'}"
      checkListVals+=("${vals[1]}")
    done
    tabLine=$(printf "\t%s" "${checkListVals[@]}")
    tabLine=${tabLine:1}
  else
    IFS=$'\n' read -d '' -a vals <<< "${tabFileLines[0]//$FORM_FLD_SEP/$'\n'}"
    if [ "${vals[0]}" == "TRUE" ]; then
      tabLine="${vals[1]}"
    else
      tabLine=$(printf "\t%s" "${vals[@]}")
      tabLine=${tabLine:1}
    fi
  fi

  echo "$tabLine"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                       validateValues
# DESCRIPTION: Check if the values submitted are valid
#      RETURN: 0 - true, 1 - false
#  Required Params:
#      1) submittedVals - String of the values submitted separated by the
#                         global variable 'FORM_FLD_SEP'
#---------------------------------------------------------------------------------------
function validateValues() {
  local submittedVals="$1"
  local validFlag=$(echo 'true' && return 0)

  for key in "${VALIDATION_KEYS[@]}"; do
    if [ ${mthdParams["$key"]+_} ]; then
      case "$key" in
        "${VALIDATION_KEYS[0]}")
          validFlag=$(isDeviceNameValid "$submittedVals" "${mthdParams[deviceNames]}" "${mthdParams[deviceType]}")
          break
        ;;
        "${VALIDATION_KEYS[1]}")
          if [[ "${mthdParams[partLayout]}" =~ "LVM" ]] && [ "$submittedVals" != "Grub2" ]; then
            validFlag=$(confirmChoiceOfBL "$submittedVals" "${mthdParams[dialogBackTitle]}")
          fi
          break
        ;;
        *)
          clog_error "No case defined for '$key'!"
          exit 1
        ;;
      esac
    fi
  done

  if ${validFlag}; then
    echo 'true' && return 0
  fi

  echo 'false' && return 1
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 getHelpNotebookDialogCmd
# DESCRIPTION: Displays a yad "dialog" containing the help section for the type of
#              device being created
#      RETURN: command to display the help "notebook" dialog
#  Required Params:
#      1)  dialogData - array of data to display on the partition table tab
#---------------------------------------------------------------------------------------
function getHelpNotebookDialogCmd() {
  local -n dialogData=$1
  local -A helpTabOpts=(["wrap"]=true ["fontname"]="Sans 12")
  local -A helpNbOpts=(["height"]=425 ["width"]=710 ["buttons-layout"]="center"
    ["title"]="Help:  ${mthdParams["dialogTitle"]}" ["window-icon"]="gtk-help.png" ["center"]=true)
  local helpKeyNames=("helpTextKeys" "helpTabKeys")
  local helpKeys=()

  if [ ${mthdParams["helpTextKeys"]+_} ]; then
    for helpKeyName in "${helpKeyNames[@]}"; do
      helpTabOpts["$helpKeyName"]="${mthdParams["$helpKeyName"]}"
      IFS=$'\t' read -a helpKeys <<< "${mthdParams["$helpKeyName"]}"
      for htk in "${helpKeys[@]}"; do
        helpTabOpts["$htk"]="${mthdParams["$htk"]}"
      done
    done
  fi

  if [ ${dialogFuncParams["statsTable"]+_} ]; then
    appendStatsToSumTblTab "${dialogFuncParams[statsTable]}" summaryTabData
  fi

  local nbHelpCmd=$(getCmdToShowHelpNotebook "${mthdParams[helpText]}" "${mthdParams[tblHdr]}" \
                    helpTabOpts dialogData helpNbOpts)
  echo "$nbHelpCmd"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getDialogTextForBL
# DESCRIPTION: Get the text to display in a yad "dialog" command when selecting
#              the option of the boot loader to install
#      RETURN: concatenated string
#  Required Params:
#      1) Text inside the dialog displayed at the very top.
#      2) partition table type
#---------------------------------------------------------------------------------------
function getDialogTextForBL() {
  local textArray=("                                         $1" " "
  "In order to boot Arch Linux, a Linux-capable boot loader"
  "must be installed."
  "------------------------------------------------------------------"
  "Choose from the available boot loaders for the"
  "\"$2\":"
  )
  local dialogText=$(getTextForDialog "${textArray[@]}")
  echo "$dialogText"
}

#---------------------------------------------------------------------------------------
#      METHOD:                     confirmChoiceOfBL
# DESCRIPTION: Confirm choice of boot loader when it is not Grub2 and the partition layout
#              is "LVM".  Set the value in the global variable "inputRetVal".
#  Required Params:
#      1) name of Boot Loader
#      2) Text inside the dialog displayed at the very top.
#---------------------------------------------------------------------------------------
confirmChoiceOfBL() {
  local dialogTitle="LVM DOES NOT support '$1'"
  local dlgBtns=("Yes" "No")
  local warnText=$(getTextForWarning)

  local textArray=("\"/boot\" cannot reside in LVM when using a boot loader which"
  "does not support LVM; you must create a separate \"/boot partition\""
  "and format it directly. Only \"GRUB\" (i.e Grub2) is known to support LVM.")
  local imageName=$(cat "$DIALOG_IMG_ICON_FILE" | cut -d',' -f1)
  local -A yadOpts=(["buttons-layout"]="center" ["image-on-top"]=true ["title"]="$dialogTitle"
    ["text"]="<span font_weight='bold' font_size='large'>$2</span>" ["text-align"]="left"
    ["height"]=425 ["width"]=550 ["fontname"]="Sans 12" ["center"]=true ["wrap"]=true ["size"]="fit")

  local dialogText=$(printf " %s" "${textArray[@]}")
  dialogText=${dialogText:1}
  textArray=("$warnText" "$dialogText"
  "------------------------------------------------------------------------------------------" " "
  "Do you want to continue?")
  dialogText=$(getTextForDialog "${textArray[@]}")

  yadOpts["image"]="$YAD_GRAPHICAL_PATH/images/$imageName"
  yadOpts["window-icon"]=$(cat "$DIALOG_IMG_ICON_FILE" | cut -d',' -f2)

  showTextInfoDialog "$dialogText" yadOpts dlgBtns
  btnClick=$?
  case ${btnClick} in
    ${DIALOG_OK})
      echo 'true' && return 0
    ;;
    *)
      echo 'false' && return 1
    ;;
  esac
}
