#!/bin/bash

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# globals
#===============================================================================
CUR_DIR="${PWD}"
INC_FILE=$(echo "$CUR_DIR/inc/post-inst-inc.sh")
source "$INC_FILE"
declare DIALOG_BACK_TITLE="Step #4:  Post-installation"

#===============================================================================
# functions/methods
#===============================================================================

#---------------------------------------------------------------------------------------
#    FUNCTION:                       checkTrizenUser
# DESCRIPTION: Checks if the user $TRIZEN_USER exists
#      RETURN: 0 - true, 1 - false
#---------------------------------------------------------------------------------------
function checkTrizenUser() {
  local cmd="id -u $TRIZEN_USER"

  if [ $(changeRoot "$cmd" 2> /dev/null || echo -1) -lt 0 ]; then
    echo 'false' && return 1
  fi
  echo 'true' && return 0
}

#---------------------------------------------------------------------------------------
#      METHOD:                        addTrizenUser
# DESCRIPTION: Add the user $TRIZEN_USER if it doesn't exist.  This will be the user
#              that will be used to execute the AUR helper 'trizen'
#---------------------------------------------------------------------------------------
addTrizenUser() {
  local existFlag=$(checkTrizenUser)
  if ! ${existFlag}; then
    local cmd="useradd -mg users -G wheel -s /usr/bin/nologin $TRIZEN_USER"
    clog_info "Add user '$TRIZEN_USER'"
    changeRoot "$cmd"
  fi

  setSudoForTrizenUser
}

#---------------------------------------------------------------------------------------
#      METHOD:                    setSudoForTrizenUser
# DESCRIPTION: Configure the SUDOER file to temporarily enable root privileges so that
#              the 'trizenuser' can compile the "trizen" application as well as be able
#              to install additional dependencies through pacman
#---------------------------------------------------------------------------------------
setSudoForTrizenUser() {
  local sudoerFile="${ROOT_MOUNTPOINT}/etc/sudoers"

  if [ ! -f "${sudoerFile}.orig" ]; then
    cp "$sudoerFile" "${sudoerFile}.orig"
    chmod 640 "$sudoerFile"
    sed -i '/%wheel ALL=(ALL) NOPASSWD: ALL/s/^#[[:space:]]*//' ${sudoerFile}
    chmod 440 "$sudoerFile"
  fi

}

#---------------------------------------------------------------------------------------
#      METHOD:                        installTrizen
# DESCRIPTION: Install the AUR helper 'trizen' if it isn't already.
#---------------------------------------------------------------------------------------
installTrizen() {
  local cmdKeys=("trizen" "done")
  local -A cmdTitles=(["trizen"]="Installing 'trizen'......." ["done"]="Process of Installing 'trizen' is done!!!!!!!")
  local -A cmdArray=(["trizen"]="software-install/trizen.sh" ["done"]="")

  local existFlag=$(isPackageInstalled "trizen")
  if ! ${existFlag}; then
    if [ "$DIALOG" == "yad" ]; then
      execInstWithYAD
    else
      execInstWithDialog "Installing AUR package......."
      clog_info "Hit Ctrl+c to Close!!!!!!!" >> "$INSTALLER_LOG"
    fi
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                        loadTerminal
# DESCRIPTION: Load a terminal to tail the log of while a YAD progress bar displays
#              the software that is being installed
#      RETURN: pid of the terminal process
#---------------------------------------------------------------------------------------
function loadTerminal() {
  nohup xfce4-terminal --geometry 100x30+500+0 --font="Courier Std Bold 10" --hide-menubar \
        --working-directory="$DATA_DIR" -e "tail -f aalp-gygi.log" > /dev/null 2>&1 &
  echo $!
}

#---------------------------------------------------------------------------------------
#      METHOD:                   installSelectedPackages
# DESCRIPTION: Install all the AUR packages that were selected
#  Required Params:
#      1) output - lines of AUR package names listed in the file $SEL_AUR_PCKGS
#---------------------------------------------------------------------------------------
installSelectedPackages() {
  if [ -f "$SEL_AUR_PCKGS" ]; then
    local output=$(cat "$SEL_AUR_PCKGS")
    local cmdKeys=()
    local -A cmdTitles=(["done"]="Process of installing AUR package is complete!"
                        ["pbTitle"]="Installing AUR package.......")
    local -A cmdArray=()
    local aurPckgLines=()

    setPackagesToInst "$output"

    if [ ${#aurPckgLines[@]} -gt 0 ]; then
      setCommandsToInstSoft

      cmdKeys+=("done")

      local pbTitle="${cmdTitles["pbTitle"]}"
      unset cmdTitles["pbTitle"]

      if [ "$DIALOG" == "yad" ]; then
        execInstWithYAD
      else
        execInstWithDialog "$pbTitle"
        clog_info "Hit Ctrl+c to Close!!!!!!!" >> "$INSTALLER_LOG"
      fi
    fi
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                      setPackagesToInst
# DESCRIPTION: Set the array "aurPckgLines" that was declared in the calling method
#              with the names of AUR packages that have NOT been installed.
#  Required Params:
#      1) output - lines of AUR package names listed in the file $SEL_AUR_PCKGS
#---------------------------------------------------------------------------------------
setPackagesToInst() {
  local output="$1"
  local aurPckgs=()
  local lines=()
  local pckgNames=()
  local sep=" "
  local totNum=0

  readarray -t lines <<< "$output"

  for line in "${lines[@]}"; do
    readarray -t pckgNames <<< "${line//$sep/$'\n'}"
    for aurPckg in "${pckgNames[@]}"; do
      local existFlag=$(isPckgInst "$aurPckg")
      if ! ${existFlag}; then
        aurPckgs+=("$aurPckg")
      fi
    done
    if [ ${#aurPckgs[@]} -gt 0 ]; then
      local concatStr=$(printf " %s" "${aurPckgs[@]}")
      aurPckgLines+=("${concatStr:1}")
      totNum=$(expr ${#aurPckgs[@]} + ${totNum})
      aurPckgs=()
    fi
  done

  if [ ${totNum} -gt 1 ]; then
    cmdTitles["pbTitle"]=$(printf "Installing (%d) AUR packages.............." ${totNum} )
    cmdTitles["done"]=$(printf "Process of installing (%d) AUR packages is complete!" ${totNum} )
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                    setCommandsToInstSoft
# DESCRIPTION: Set the arrays "cmdKeys", "cmdTitles", "cmdArray" with the necessary
#              elements to install the names of the AUR packages listed in the array
#              "aurPckgLines".
#        NOTE: The arrays in the description were declared within the calling method
#---------------------------------------------------------------------------------------
setCommandsToInstSoft() {
  local cmdNum=1
  local pckgNames=()
  local adobeTTF="ttf-adobe-fonts"
  local pos=0
  local idx=0
  local sep=" "

  for line in "${aurPckgLines[@]}"; do
    readarray -t pckgNames <<< "${line//$sep/$'\n'}"
    pos=$(findPositionForString pckgNames "$adobeTTF")
    if [ ${pos} -gt 0 ]; then
      idx=$(expr ${pos} - 1)
      unset pckgNames[${idx}]

      cmdKeys+=("$adobeTTF")
      cmdTitles["$adobeTTF"]=$(echo "Installing 'Adobe TTF' font package")
      cmdArray["$adobeTTF"]="adobeTTF.sh"
      cmdNum=$(expr ${cmdNum} + 1)

      if [ ${#pckgNames[@]} -gt 0 ]; then
        line=$(printf " %s" "${pckgNames[@]}")
        addTrizenCmd "${line:1}"
      fi
    else
      addTrizenCmd "$line"
    fi
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                        addTrizenCmd
# DESCRIPTION: Add the command to call 'trizen' to install the package names.  The
#              arrays "cmdKeys", "cmdTitles", "cmdArray" will be updated with the
#              necessary elements to install the names of the AUR packages.
#        NOTE: The arrays in the description were declared within the calling method
#---------------------------------------------------------------------------------------
addTrizenCmd() {
  local pckgNames="$1"
  local keyFmt="cmd#%02d"
  local concatStr=$(printf ",'%s'" "${pckgNames[@]}")
  local cmd=$(echo "sudo -u $TRIZEN_USER trizen -S --skipinteg --noconfirm $pckgNames")
  local cmdKey=$(printf "$keyFmt" ${cmdNum})

  cmdKeys+=("$cmdKey")
  cmdTitles["$cmdKey"]=$(echo "Installing ${concatStr:1}")
  cmdArray["$cmdKey"]="$cmd"
  cmdNum=$(expr ${cmdNum} + 1)
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                         isPckgInst
# DESCRIPTION: Check if an AUR package is installed.
#      RETURN: 0 - true, 1 - false
#  Required Params:
#      1) aurPckgName - name of the AUR package
#---------------------------------------------------------------------------------------
function isPckgInst() {
  local cmd=$(echo "sudo -u $TRIZEN_USER trizen -Qi $1")

  if changeRoot "$cmd" &> /dev/null; then
    echo 'true' && return 0
  fi
  echo 'false' && return 1
}

#---------------------------------------------------------------------------------------
#      METHOD:                       executeCommands
# DESCRIPTION: Execute the commands within the file name contained in the
#              global variable "$POST_INST_CMD_FILE"
#---------------------------------------------------------------------------------------
executeCommands() {
  if [ -f "$POST_INST_CMD_FILE" ]; then
    local -A cmdArray=()
    local cmdKeys=()
    local cmdNum=1
    local -A cmdTitles=()
    local fields=()
    local keyFmt="cmd#%02d"
    local lines=()
    local output=$(cat "$POST_INST_CMD_FILE")
    local cmd=""
    local cmdKey=""

    readarray -t lines <<< "$output"

    for line in "${lines[@]}"; do
      readarray -t fields <<< "${line//$FORM_FLD_SEP/$'\n'}"
      cmd="${fields[1]}"
      if [[ "$cmd" =~ "configureOpenSSH" ]]; then
        cmdKey="configureOpenSSH"
      elif [[ "$cmd" =~ "configureXinitrc" ]]; then
        cmdKey="configureXinitrc"
      else
        cmdKey=$(printf "$keyFmt" ${cmdNum})
        cmdNum=$(expr ${cmdNum} + 1)
      fi
      cmdKeys+=("$cmdKey")
      cmdTitles["$cmdKey"]="${fields[0]}"
      cmdArray["$cmdKey"]="$cmd"
    done
    cmdKeys+=("done")
    cmdTitles["done"]="All commands have been executed!!!!"
    if [ "$DIALOG" == "yad" ]; then
      execInstWithYAD
    else
      execInstWithDialog
      clog_info "Hit Ctrl+c to Close!!!!!!!" >> "$INSTALLER_LOG"
    fi
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                     execInstWithDialog
# DESCRIPTION: Display a linux "dialog" box of type gauge (i.e. progress bar) to show
#              the progress of either the commands being executed or the AUR packages
#              getting Installed.
#   Optional Param:
#      1) pbTitle - override the title for the dialog
#---------------------------------------------------------------------------------------
execInstWithDialog() {
  local pbTitle="Executing command.............."
  local cmdNum=1
  local cmdTitle=""
  local pbPerc=0
  local evalCmds=()

  local totCmds=${#cmdKeys[@]}
  if [ "$#" -gt 0 ]; then
     pbTitle="$1"
  elif [ ${totCmds} -gt 2 ]; then
    pbTitle=$(printf "Executing (%d) commands.............." $(expr ${totCmds} - 1) )
  fi
  (
    sleep 0.5
    for pbKey in "${cmdKeys[@]}"; do
      cmdTitle="${cmdTitles[$pbKey]}"
      echo $pbPerc
      echo "XXX"
      echo "${cmdTitle}..."
      echo "XXX"

      case "$pbKey" in
        "done")
          clog_warn "$cmdTitle" >> "$INSTALLER_LOG"
        ;;
        *)
          execCmdForPB "$pbKey" "${cmdArray[$pbKey]}"
        ;;
      esac

      cmdNum=$(expr $cmdNum + 1)
      pbPerc=$(printf "%.0f" $(echo "scale=3; $cmdNum / $totCmds * 100" | bc))

      sleep 0.5
    done
  ) | $DIALOG --clear --begin 3 3 --backtitle "$DIALOG_BACK_TITLE" --title "$pbTitle" --gauge "Please Wait..." 10 80 0
}

#---------------------------------------------------------------------------------------
#      METHOD:                       execInstWithYAD
# DESCRIPTION: Display a yad "progress indication" dialog (i.e. progress bar) to show
#              the progress of either the commands being executed or the AUR packages
#              getting Installed.
#---------------------------------------------------------------------------------------
execInstWithYAD() {
  local termPID=$(loadTerminal)
  local cmdTitle=""

  {
    sleep 0.5
    for pbKey in "${cmdKeys[@]}"; do
      cmdTitle="${cmdTitles[$pbKey]}"
      echo "# ${cmdTitle}..."

      case "$pbKey" in
        "done")
          clog_warn "$cmdTitle" >> "$INSTALLER_LOG"
          break
        ;;
        *)
          execCmdForPB "$pbKey" "${cmdArray[$pbKey]}"
        ;;
      esac

      sleep 0.5
    done
    sleep 2
  } | yad --progress --title "$DIALOG_BACK_TITLE" --posx=20 --posy=20 --width=300 --height=200 --pulsate --auto-close \
          --enable-log="Progress Bar Log" --log-expanded --no-buttons 2> /dev/null

  kill -9 ${termPID}
}

#---------------------------------------------------------------------------------------
#      METHOD:                        execCmdForPB
# DESCRIPTION: Execute the command or install AUR packages for the progress bar.
#---------------------------------------------------------------------------------------
execCmdForPB() {
  local cmdKey="$1"
  local cmd="$2"

  case "$cmdKey" in
    "configureOpenSSH")
      configureOpenSSH &>> "$INSTALLER_LOG"
    ;;
    "configureXinitrc")
      ${cmd} &>> "$INSTALLER_LOG"
    ;;
    "trizen")
      software-install/trizen.sh &>> "$INSTALLER_LOG"
    ;;
    *)
      changeRoot "${cmd}" &>> "$INSTALLER_LOG"
    ;;
  esac
}

#---------------------------------------------------------------------------------------
#      METHOD:                      configureOpenSSH
# DESCRIPTION: Execute the necessary commands to configure the SSH daemon
#---------------------------------------------------------------------------------------
configureOpenSSH() {
  local sshdConfFile="${ROOT_MOUNTPOINT}/etc/ssh/sshd_config"

  local sedExprs=("/Port 22/s/^#//"
    "/Protocol 2/s/^#//"
    "/HostKey \/etc\/ssh\/ssh_host_rsa_key/s/^#//"
    "/HostKey \/etc\/ssh\/ssh_host_dsa_key/s/^#//"
    "/HostKey \/etc\/ssh\/ssh_host_ecdsa_key/s/^#//"
    "/KeyRegenerationInterval/s/^#//"
    "/ServerKeyBits/s/^#//"
    "/SyslogFacility/s/^#//"
    "/LogLevel/s/^#//"
    "/LoginGraceTime/s/^#//"
    "/PermitRootLogin/s/^#//"
    "/HostbasedAuthentication no/s/^#//"
    "/StrictModes/s/^#//"
    "/RSAAuthentication/s/^#//"
    "/PubkeyAuthentication/s/^#//"
    "/IgnoreRhosts/s/^#//"
    "/PermitEmptyPasswords/s/^#//"
    "/AllowTcpForwarding/s/^#//"
    "/AllowTcpForwarding no/d"
    "/X11Forwarding/s/^#//"
    "/X11Forwarding/s/no/yes/"
    "/X11DisplayOffset/s/^#//"
    "/X11UseLocalhost/s/^#//"
    "/PrintMotd/s/^#//"
    "/PrintMotd/s/yes/no/"
    "/PrintLastLog/s/^#//"
    "/TCPKeepAlive/s/^#//"
    "/the setting of/s/^/#/"
    "/RhostsRSAAuthentication and HostbasedAuthentication/s/^/#/")

    for expr in "${sedExprs[@]}"; do
      sed -i "$expr" "$sshdConfFile"
    done
}

#---------------------------------------------------------------------------------------
#      METHOD:                      configureXinitrc
# DESCRIPTION: Create and configure an ".xinitrc" in the user's home directory so that
#              'xinit' will use it to start the "Desktop Environment" or the standalone
#              "Window Manager" that was installed.
#---------------------------------------------------------------------------------------
configureXinitrc() {
  local userName="$1"
  local destFile="/home/${userName}/.xinitrc"
  local srcFile="/etc/X11/xinit/xinitrc"

  cp -rfv ${srcFile} "${ROOT_MOUNTPOINT}$destFile"
  echo -e "$2" >> "${ROOT_MOUNTPOINT}$destFile"
  local cmd=$(printf "chown -R %s:users %s" "$userName" "$destFile")
  changeRoot "$cmd"
}

#---------------------------------------------------------------------------------------
#  Main
#---------------------------------------------------------------------------------------
main() {
  if [ -f "$SEL_AUR_PCKGS" ] || [ -f "$POST_INST_CMD_FILE" ]; then
    installSelectedPackages
    executeCommands
  else
    addTrizenUser
    installTrizen
  fi
}

main "$@"
exit 0
