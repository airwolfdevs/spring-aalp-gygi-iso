#!/bin/bash

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# globals
#===============================================================================
CUR_DIR="${PWD}"
INC_FILE=$(echo "$CUR_DIR/inc/inst_inc.sh")

source "$INC_FILE"
source "$GRAPHICAL_PATH/install-guide-inc.sh"

declare -A varMap=()
declare INST_GUIDE_CHECKLIST=( 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 )
declare -A checkListSteps=()
declare stepTotals=()
declare BLOCK_DEVICE=""
declare BLOCK_DEVICE_SIZE=""
declare PARTITION_LAYOUT_TEXT=""
declare MAX_LINE_CHARS=0
declare DIALOG_BACK_TITLE="Arch Linux Installation Guide"
declare -A INST_STEP_TASKS=()
declare UEFI_FLAG=0
declare HXU_NORMALIZE="hxnormalize"
declare HXU_SELECT="hxselect"
declare PHANTOM_JS="phantomjs"
declare METHOD_KEY_FMT="method#%02d"
declare REQ_BINARIES=("apg" "$HXU_NORMALIZE" "$HXU_SELECT" "$JSON_PROCESSOR" "$PHANTOM_JS"
    "pwgen" "rankmirrors" "reflector" "screen" "xdg-open" "xfce4-terminal")
declare -A BINARY_DESCS=(["apg"]="Automated Password Generator."
    ["$HXU_NORMALIZE"]="Normalize HTML before extraction."
    ["$HXU_SELECT"]="Extact HTML elements with CSS."
    ["$JSON_PROCESSOR"]="Command-line JSON processor"
    ["$PHANTOM_JS"]="Headless WebKit with JavaScript API"
    ["pwgen"]="Generator to create memorable passwords"
    ["rankmirrors"]="Ranks pacman mirrors by their connection and opening speed"
    ["reflector"]="Retrieve and filter the latest Pacman mirror list"
    ["screen"]="Splits the screen to display progress bar & tail log file"
    ["xdg-open"]="Open a file or URI in the user's preferred application"
    ["xfce4-terminal"]="Terminal Emulator used to tail log file for YAD dialogs"
    )

#===============================================================================
# functions/methods
#===============================================================================

#---------------------------------------------------------------------------------------
#      METHOD:                   setStepAndTasks
# DESCRIPTION: Sets the installation/main steps and their tasks as choices to be
#              displayed within the installation guide menu
#---------------------------------------------------------------------------------------
setStepAndTasks() {
  local preInst=(
  "  Set the keyboard layout"
  "  Verify boot mode"
  "  Internet Connection"
  "  Set the system clock"
  "  Partition the disks"
  "  Format the partitions"
  "  Mount the file systems"
  )
  local inst=(
  "  Install Base System"
  "  Configure Mirror list"
  )
  local configSys=(
  "  Fstab"
  "  Hardware & System Clocks"
  "  Locale"
  "  Network configuration"
  "  Initialize RAM Filesystem"
  "  Root Password"
  "  Boot loader"
  )
  local postInst=(
  "  Select the Editor"
  )

  if [ "$DIALOG" == "yad" ]; then
    configSys[1]="  Time zone &amp; Hardware Clock"
  fi

  stepTotals+=(0)
  stepTotals+=(${#preInst[@]})
  stepTotals+=(${#inst[@]})
  stepTotals+=(${#configSys[@]})
  stepTotals+=(1)
  stepTotals+=(1)

  checkListSteps["step#1"]="Pre-installation"
  checkListSteps["step#2"]="Installation"
  checkListSteps["step#3"]="Configure the system"
  checkListSteps["step#4"]="Post-installation"
  checkListSteps["step#5"]="Reboot"

  local numInstSteps=$(expr ${#stepTotals[@]} - 1)
  for methodNum in $(eval echo "{1..$numInstSteps}"); do
    case ${methodNum} in
      1)
        setTasksForStep "$methodNum" preInst
      ;;
      2)
        setTasksForStep "$methodNum" inst
      ;;
      3)
        setTasksForStep "$methodNum" configSys
      ;;
      *)
        local stepKey=$(printf "step#%d" ${methodNum})
        INST_STEP_TASKS["$stepKey"]="$stepTasks"
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                   setTasksForStep
# DESCRIPTION: Sets the tasks of their installation/main step as choices to be displayed
#              within the installation guide menu
#---------------------------------------------------------------------------------------
setTasksForStep() {
  local stepNum=$1
  local -n steps=$2
  local idx=1
  local len=0
  local maxLen=0
  local taskKey=""
  local stepKey=$(printf "step#%d" "$stepNum")
  local tasks=()

  for elem in "${steps[@]}"; do
    taskKey=$(printf "step#%d.%d" "$stepNum" "$idx")
    tasks+=("$taskKey")
    checkListSteps["$taskKey"]=$(echo "$elem")
    idx=$(expr $idx + 1)

    len=${#elem}
    if [ "$len" -gt "$maxLen" ]; then
      maxLen=$len
    fi
    MAX_LINE_CHARS=$(expr $maxLen + 2)
  done

  local stepTasks=$(printf "\t%s" "${tasks[@]}")
  stepTasks=${stepTasks:1}
  INST_STEP_TASKS["$stepKey"]="$stepTasks"
}

#---------------------------------------------------------------------------------------
#      METHOD:                  initInstallationGuide
# DESCRIPTION: Initializes the data needed by the installation guide
#---------------------------------------------------------------------------------------
initInstallationGuide() {
  setStepAndTasks

  while true; do
    initVars
    if [ -f "$UPD_ASSOC_ARRAY_FILE" ]; then
      source -- "${UPD_ASSOC_ARRAY_FILE}"
      rm -rf "${UPD_ASSOC_ARRAY_FILE}"

      if [ ${updVarMap["errorMethod"]+_} ]; then
        case "${updVarMap["errorMethod"]}" in
          "setTypeOfInternetConnection")
            setInternetConnection
          ;;
          *)
            ${updVarMap["errorMethod"]} 1
          ;;
        esac
      else
        for key in "${!updVarMap[@]}"; do
          varMap["$key"]=$(echo "${updVarMap[$key]}")
        done

        if [ ${varMap["PassReqs"]} -lt 1 ]; then
          declare -p varMap > "$ASSOC_ARRAY_FILE"
        else
          break
        fi
      fi
    else
      break
    fi
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                          initVars
# DESCRIPTION: Initialize the global variables.
#---------------------------------------------------------------------------------------
initVars() {
  local pbTitle="Airwolf Arch Linux Pseudo-Graphical & YAD Graphical Installer"
  local pbKeys=()
  local -A pbTitles=()
  local -A methodNames=()
  local initMethodNames=("setDeviceAndSize" "verifyInternetConnection" "verifyMissingBinaries"
                        "verifySupportOfTRIM" "verifyRequirements" "verifyInternetProtocolVer"
                        "verifyOpenNIC" "verifyIPAddress" "verifyGeoLocation" "verifyBootMode"
                        "checkSystemClock" "verifyPartitionLayout" "verifyPartTableDataFile"
                        "initializeChecklist")

  for methodName in "${initMethodNames[@]}"; do
    ${methodName}
  done

  verifyInstSteps

  if [ ${#pbKeys[@]} -gt 0 ]; then
    pbKeys+=("done")
    pbTitles["done"]="Executed ALL the methods to initialize the data for the Installation Guide!!!!"
    dispLoadingDataPB "$DIALOG_BACK_TITLE" "$pbTitle"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                      setDeviceAndSize
# DESCRIPTION: Set size and the name of the device that will be partitioned and where
#              Arch Linux will be installed.
#---------------------------------------------------------------------------------------
setDeviceAndSize() {
  if [ ${varMap["BLOCK-DEVICE"]+_} ]; then
    BLOCK_DEVICE=$(echo "${varMap["BLOCK-DEVICE"]}")
    BLOCK_DEVICE_SIZE=$(echo "${varMap["BLOCK_DEVICE_SIZE"]}")
  else
    local methodNum=$(expr ${#pbKeys[@]} + 1)
    local methodKey=$(printf "$METHOD_KEY_FMT" ${methodNum})
    pbKeys+=("$methodKey")
    pbTitles["$methodKey"]="Setting the device to be partitioned and where Arch Linux will be installed...."
    methodNames["$methodKey"]="setDeviceToPartition"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                    setDeviceToPartition
# DESCRIPTION: Set the device to be partitioned and where Arch Linux will be installed.
#---------------------------------------------------------------------------------------
setDeviceToPartition() {
  local devices=()
  local blockDev=""
  local errMsg="Installation cannot proceed without a primary device to partition "
  errMsg+="and where Arch Linux will be installed."

  findEligibleDevices
  if [ ${#devices[@]} -gt 1 ] && [ "$#" -gt 0 ]; then
    if [ "$DIALOG" == "yad" ]; then
      getSelectedDeviceToPartition
    else
      getSelectedDeviceToPartition "$DIALOG_BACK_TITLE"
    fi
    if [[ -n "$mbSelVal" ]]; then
      blockDev="$mbSelVal"
    else
      showErrorDialog "$DIALOG_BACK_TITLE" "Primary Device Selection" "$errMsg"
      exit 1
    fi
  elif [ ${#devices[@]} == 1 ]; then
    blockDev="${devices[0]}"
  fi

  if [[ -n "$blockDev" ]]; then
    varMap["BLOCK-DEVICE"]=$(echo "$blockDev")
    local devSize=$(blockdev --getsize64 "$blockDev")
    local blockDevSize=$(bytesToHumanReadable "$devSize")
    varMap["BLOCK_DEVICE_SIZE"]=$(echo "$blockDevSize")
  fi

}

#---------------------------------------------------------------------------------------
#      METHOD:                   verifySupportOfTRIM
# DESCRIPTION: Verify if the hard drive can support running the TRIM command
#---------------------------------------------------------------------------------------
verifySupportOfTRIM() {
  if [ ${varMap["TRIM"]+_} ]; then
    TRIM=${varMap["TRIM"]}
  else
    local methodNum=$(expr ${#pbKeys[@]} + 1)
    local methodKey=$(printf "$METHOD_KEY_FMT" ${methodNum})
    pbKeys+=("$methodKey")
    pbTitles["$methodKey"]="Verifying if the hard drive can support running the TRIM command...."
    methodNames["$methodKey"]="setFlagForTRIM"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                       setFlagForTRIM
# DESCRIPTION: Set flag to indicate if the hard drive can support running the TRIM command
#---------------------------------------------------------------------------------------
setFlagForTRIM() {
  local cmdOutput=$(lsblk --discard "${varMap["BLOCK-DEVICE"]}" | grep sda | awk '{print $1":"$3":"$4}')
  local rows=()
  IFS=$'\n' read -d '' -r -a rows <<< $cmdOutput
  local row="${rows[0]}"
  local cols=()
  IFS=':' read -a cols <<< $row
  local discGran=$(humanReadableToBytes "${cols[1]}")
  local discMax=$(humanReadableToBytes "${cols[2]}")
  if [ ${discGran} -gt 0 ] || [ ${discMax} -gt 0 ]; then
    varMap["TRIM"]=1
  else
    varMap["TRIM"]=0
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                   verifyMissingBinaries
# DESCRIPTION: Verify that the required binaries are installed.
#---------------------------------------------------------------------------------------
verifyMissingBinaries() {
  if [ ! ${varMap["PassReqs"]+_} ] || [ ${varMap["PassReqs"]} -gt 0 ]; then
    local methodNum=$(expr ${#pbKeys[@]} + 1)
    local methodKey=$(printf "$METHOD_KEY_FMT" ${methodNum})
    pbKeys+=("$methodKey")
    pbTitles["$methodKey"]="Verifying required AUR packages/binaries...."
    methodNames["$methodKey"]="setMissingBinaries"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                     setMissingBinaries
# DESCRIPTION: Set the names of the AUR packages/binaries that are NOT installed
#              in the global assoc. array variable "varMap"
#---------------------------------------------------------------------------------------
setMissingBinaries() {
  local missingBins=()
  for binary in "${REQ_BINARIES[@]}"; do
    if ! type "$binary" &> /dev/null; then
      missingBins+=("$binary")
    fi
  done

  if [ ${#missingBins[@]} -gt 0 ]; then
    varMap["MissingBinaries"]=$(printf "$FORM_FLD_SEP%s" "${missingBins[@]}")
    varMap["MissingBinaries"]=${varMap["MissingBinaries"]:${#FORM_FLD_SEP}}
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                  verifyInternetConnection
# DESCRIPTION: Verify that there is a connection to the internet.
#---------------------------------------------------------------------------------------
verifyInternetConnection() {
  if [ ! ${varMap["CONNECTION-TYPE"]+_} ]; then
    local methodNum=$(expr ${#pbKeys[@]} + 1)
    local methodKey=$(printf "$METHOD_KEY_FMT" ${methodNum})
    pbKeys+=("$methodKey")
    pbTitles["$methodKey"]="Setting type of connection to internet...."
    methodNames["$methodKey"]="setTypeOfInternetConnection"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                 setTypeOfInternetConnection
# DESCRIPTION: Verify that there is a connection to the internet.  Set the type of
#              connection in the global assoc. array variable "varMap"
#---------------------------------------------------------------------------------------
setTypeOfInternetConnection() {
  local connFlag=$(isConnToInternet)

  if ${connFlag}; then
    local wiredConnFlag=`ip link | grep "ens\|eno\|enp" | awk '{print $2}'| sed 's/://' | sed '1!d'`
    local wirelessConnFlag=`ip link | grep wlp | awk '{print $2}'| sed 's/://' | sed '1!d'`
    if [[ -n $wiredConnFlag ]]; then
      varMap["CONNECTION-TYPE"]=$(echo "$wiredConnFlag")
    else
      varMap["CONNECTION-TYPE"]=$(echo "$wirelessConnFlag")
    fi
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    isConnToInternet
# DESCRIPTION: Checks to see if Connection to the Internet is available
#      RETURN: 0 - true, 1 - false
#---------------------------------------------------------------------------------------
function isConnToInternet() {
  local lineCnt=$(ping -q -w 1 -c 1 `ip r | grep default | awk 'NR==1 {print $3}'` | wc -l 2>&1)

  if [ "$lineCnt" -lt 1 ]; then
    echo 'false' && return 1
  fi

  echo 'true' && return 0
}

#---------------------------------------------------------------------------------------
#      METHOD:                   setInternetConnection
# DESCRIPTION: Set the connection to the internet.  This will give the user the ability
#              to configure the connection manually.
#---------------------------------------------------------------------------------------
setInternetConnection(){
  XPINGS=$(( $XPINGS + 1 ))

  local connFlag=$(isConnToInternet)
  local errorMsg=""

  if ${connFlag} && [ ! ${varMap["CONNECTION-TYPE"]+_} ]; then
    if [[ $XPINGS -gt 2 ]]; then
      errorMsg="Can't establish connection. exiting..."
      showErrorDialog "$DIALOG_BACK_TITLE" "Connecting to the Internet" "$errMsg"
      clog_error "$errMsg" >> "$INSTALLER_LOG"
      exit 1
    fi

    executeScript "step#1" "step#1.3"

    [[ "${varMap["CONNECTION-TYPE"]}" != "Skip" ]] && setInternetConnection
  elif [ "${varMap["CONNECTION-TYPE"]}" == "Skip" ]; then
    errorMsg="Can't continue without an internet connection. exiting..."
    showErrorDialog "$DIALOG_BACK_TITLE" "Connecting to the Internet" "$errMsg"
    clog_error "$errMsg" >> "$INSTALLER_LOG"
    exit 1
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                     verifyRequirements
# DESCRIPTION: Verify the requirements to install Arch Linux are met.
#---------------------------------------------------------------------------------------
verifyRequirements() {
  if [ ! ${varMap["PassReqs"]+_} ] || [ ${varMap["PassReqs"]} -gt 0 ]; then
    local methodNum=$(expr ${#pbKeys[@]} + 1)
    local methodKey=$(printf "$METHOD_KEY_FMT" ${methodNum})
    pbKeys+=("$methodKey")
    pbTitles["$methodKey"]="Verifying that the requirments are met...."
    methodNames["$methodKey"]="setPassReqsFlag"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                       setPassReqsFlag
# DESCRIPTION: Set the flag to indicate if requirements are met in the global
#              associative array variable "varMap"
#---------------------------------------------------------------------------------------
setPassReqsFlag() {
  local passFlag=$(echo 'true' && return 0)
  local numReqs=4
  #### Used in the calling functions
  local reqDesc=""
  local actualVal=""
  for reqNum in $(eval echo "{1..${numReqs}}"); do
    case ${reqNum} in
      1)  #### RAM
        setReqForRAM
      ;;
      2)  #### Disk Space
        setReqForDiskSpace
      ;;
      3)  #### Internet Connection
        if [ ! ${varMap["CONNECTION-TYPE"]+_} ]; then
          passFlag=$(echo 'false' && return 1)
        fi
      ;;
      4)  #### AUR packages/binaries
        if [ ${varMap["MissingBinaries"]+_} ]; then
          passFlag=$(echo 'false' && return 1)
        fi
      ;;
    esac
  done

  if ${passFlag}; then
    varMap["PassReqs"]=0
  else
    varMap["PassReqs"]=1
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                  verifyInternetProtocolVer
# DESCRIPTION: Verify if the internet connection is able to browse IPv4 and/or IPv6 sites.
#---------------------------------------------------------------------------------------
verifyInternetProtocolVer() {
  if [ ! ${varMap["IPv4"]+_} ] && [ ! ${varMap["IPv6"]+_} ]; then
    local methodNum=$(expr ${#pbKeys[@]} + 1)
    local methodKey=$(printf "$METHOD_KEY_FMT" ${methodNum})
    pbKeys+=("$methodKey")
    pbTitles["$methodKey"]="Verifying ability to browse IPv4 and/or IPv6 sites...."
    methodNames["$methodKey"]="setInternetProtocolVer"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                   setInternetProtocolVer
# DESCRIPTION: Determine whether the internet connection is able to browse IPv4 and IPv6
#              sites.  Set the flags in the global assoc. array variable "varMap"
#---------------------------------------------------------------------------------------
setInternetProtocolVer() {
  local fileName="${DATA_DIR}/test-ipv6.html"
  local -A ipVers=()
  ipVers["IPv4"]=$(echo 'false' && return 1)
  ipVers["IPv6"]=$(echo 'false' && return 1)

  if [ ${varMap["PassReqs"]} -lt 1 ]; then
    if [ ! -f "$fileName" ]; then
      ${PHANTOM_JS} "${DATA_DIR}/save-html.js" | $HXU_NORMALIZE -x > "$fileName"
      sed -i '/^$/d' $fileName
    fi

    local htmlTbl=$($HXU_SELECT "tr" < $fileName)
    local htmlLines=()
    local tableRows=()
    readarray -t htmlLines <<< "$htmlTbl"
    for htmlLine in "${htmlLines[@]}"; do
      htmlLine=$(trimString "$htmlLine")
      tableRows+=("$htmlLine")
    done
    local concatStr=$(printf " %s" "${tableRows[@]}")
    concatStr=${concatStr:1}
    local sep="</tr>"

    readarray -t tableRows <<< "${concatStr//$sep/$'\n'}"
    for row in "${tableRows[@]:1}"; do
      concatStr=$($HXU_SELECT "[alt='green']" <<< "$row")
      if [ ${#concatStr} -gt 0 ]; then
        concatStr=$($HXU_SELECT -c "b" <<< "$row")
        if [ ${ipVers["$concatStr"]+_} ]; then
          ipVers["$concatStr"]=$(echo 'true' && return 0)
        fi
      fi
    done

    for key in "${!ipVers[@]}"; do
      if ${ipVers["$key"]}; then
        varMap["$key"]="true"
      fi
    done
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                        verifyOpenNIC
# DESCRIPTION: Verify IP Addresses for the OpenNIC provider
#---------------------------------------------------------------------------------------
verifyOpenNIC() {
  if [ ! ${varMap["OpenNIC"]+_} ] ; then
    local methodNum=$(expr ${#pbKeys[@]} + 1)
    local methodKey=$(printf "$METHOD_KEY_FMT" ${methodNum})
    pbKeys+=("$methodKey")
    pbTitles["$methodKey"]="Setting IP Addresses that are the closest servers "
    pbTitles["$methodKey"]+="provided by the OpenNIC provider...."
    methodNames["$methodKey"]="setIPsForOpenNIC"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                      setIPsForOpenNIC
# DESCRIPTION: Set the IPv4 and IPv6 addresses that are the closest servers provided
#              by the OpenNIC provider & store the addresses in the
#              global associative array variable "varMap"
#---------------------------------------------------------------------------------------
setIPsForOpenNIC() {
  local fileName="${DATA_DIR}/OpenNIC.html"
  if [ ${varMap["PassReqs"]} -lt 1 ]; then
    if [ ! -f "$fileName" ]; then
      ${PHANTOM_JS} "${DATA_DIR}/OpenNIC-html.js" | $HXU_NORMALIZE -x > "$fileName"
      sed -i '/^$/d' $fileName
    fi

    local htmlDiv=$($HXU_SELECT -c "[id='geoip4']" < $fileName)
    local htmlLines=()
    local row=()
    local ipAddrs=()

    readarray -t htmlLines <<< "$htmlDiv"
    for htmlLine in "${htmlLines[@]}"; do
      htmlLine=$(trimString "$htmlLine")
      readarray -t row <<< "${htmlLine// (/$'\n'}"
      ipAddrs+=("${row[0]}")
    done

    ipAddrs=("${ipAddrs[@]:0:2}")

    htmlDiv=$($HXU_SELECT -c "[id='geoip6']" < $fileName)
    readarray -t htmlLines <<< "$htmlDiv"
    for htmlLine in "${htmlLines[@]}"; do
      htmlLine=$(trimString "$htmlLine")
      readarray -t row <<< "${htmlLine// (/$'\n'}"
      ipAddrs+=("${row[0]}")
    done

    ipAddrs=("${ipAddrs[@]:0:4}")

    local concatStr=$(printf "$FORM_FLD_SEP%s" "${ipAddrs[@]}")
    concatStr=${concatStr:${#FORM_FLD_SEP}}
    varMap["OpenNIC"]="$concatStr"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                       verifyIPAddress
# DESCRIPTION: Verify that IP Address of the ISP has been set
#---------------------------------------------------------------------------------------
verifyIPAddress() {
  if [ ! ${varMap["$IP_KEY_NAME"]+_} ]; then
    local methodNum=$(expr ${#pbKeys[@]} + 1)
    local methodKey=$(printf "$METHOD_KEY_FMT" ${methodNum})
    pbKeys+=("$methodKey")
    pbTitles["$methodKey"]="Getting IP Address of ISP...."
    methodNames["$methodKey"]=$(echo "setIPAddressOfISP")
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                      setIPAddressOfISP
# DESCRIPTION: Set the IP Address of the ISP.
#---------------------------------------------------------------------------------------
setIPAddressOfISP() {
  local ipArray=()
  if [ ${varMap["PassReqs"]} -lt 1 ]; then
    local fileName="${DATA_DIR}/ipstack.html"
    curl -s https://ipstack.com/ | $HXU_NORMALIZE -x  > "$fileName"
    if [ -f "$fileName" ]; then
      varMap["$IP_KEY_NAME"]=$($HXU_SELECT "[name='client_ip']" < "$fileName")
      IFS=$'"' read -a ipArray <<< "${varMap["$IP_KEY_NAME"]}"
      varMap["$IP_KEY_NAME"]=${ipArray[-2]}
    fi
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                       verifyGeoLocation
# DESCRIPTION: Verify the geographical location information has been set
#---------------------------------------------------------------------------------------
verifyGeoLocation() {
  if [ ! ${varMap["$CTRY_KEY_NAME"]+_} ] || [ ! -f "$TZ_FILE_NAME" ]; then
    local methodNum=$(expr ${#pbKeys[@]} + 1)
    local methodKey=$(printf "$METHOD_KEY_FMT" ${methodNum})
    pbKeys+=("$methodKey")
    pbTitles["$methodKey"]="Getting Geographical Location Information...."
    methodNames["$methodKey"]=$(echo "setGeoLocation")
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                       setGeoLocation
# DESCRIPTION: Set the geographical location information based on the IP Address.
#---------------------------------------------------------------------------------------
setGeoLocation() {
  local fileName="${DATA_DIR}/geoLoc.json"
  if [ ${varMap["PassReqs"]} -lt 1 ]; then
    local cmd=$(printf "curl -s curl -s http://ip-api.com/json/%s" "${varMap["$IP_KEY_NAME"]}")
    ${cmd} > "$fileName"

    if [ -f "$fileName" ]; then
      varMap["$CTRY_KEY_NAME"]=$($JSON_PROCESSOR -r '.countryCode' "$fileName")
      $JSON_PROCESSOR -r '.timezone' "$fileName" > "$TZ_FILE_NAME"
    fi
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                        verifyBootMode
# DESCRIPTION: Verify if the flag for determining the boot mode has been set
#---------------------------------------------------------------------------------------
verifyBootMode() {
  if [ ${varMap["BOOT-MODE"]+_} ] ; then
    if [ "${varMap["BOOT-MODE"]}" == "UEFI" ]; then
      UEFI_FLAG=$(echo 'true' && return 0)
    else
      UEFI_FLAG=$(echo 'false' && return 1)
    fi
  else
    local methodNum=$(expr ${#pbKeys[@]} + 1)
    local methodKey=$(printf "$METHOD_KEY_FMT" ${methodNum})
    pbKeys+=("$methodKey")
    pbTitles["$methodKey"]="Setting the boot mode to either 'BIOS' or 'UEFI'...."
    methodNames["$methodKey"]="setBootMode"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                       setBootMode
# DESCRIPTION: Set the boot mode to either 'BIOS' or 'UEFI'
#---------------------------------------------------------------------------------------
setBootMode() {
  if [ "$(cat /sys/class/dmi/id/sys_vendor)" == 'Apple Inc.' ] || [ "$(cat /sys/class/dmi/id/sys_vendor)" == 'Apple Computer, Inc.' ]; then
    modprobe -r -q efivars || true  # if MAC
  else
    modprobe -q efivarfs            # all others
  fi
  if [[ -d "/sys/firmware/efi/" ]]; then
    ## Mount efivarfs if it is not already mounted
    if [[ -z $(mount | grep /sys/firmware/efi/efivars) ]]; then
      mount -t efivarfs efivarfs /sys/firmware/efi/efivars
    fi
    varMap["BOOT-MODE"]=$(echo "UEFI")
  else
    varMap["BOOT-MODE"]=$(echo "BIOS")
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                    verifyPartitionLayout
# DESCRIPTION: Verify all the necessary values for the partition layout/scheme
#              have been set.
#---------------------------------------------------------------------------------------
verifyPartitionLayout() {
  if [ ${varMap["BLOCK-DEVICE"]+_} ]; then
    local existFlag=$(isPartitioned "${varMap["BLOCK-DEVICE"]}")
    if ${existFlag} && [ ! ${varMap["PART-LAYOUT"]+_} ]; then
      local methodNum=$(expr ${#pbKeys[@]} + 1)
      local methodKey=$(printf "$METHOD_KEY_FMT" ${methodNum})
      pbKeys+=("$methodKey")
      pbTitles["$methodKey"]="Setting the partition layout/scheme values...."
      methodNames["$methodKey"]=$(echo "setPartitionLayout")
    fi
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                   setPartitionLayout
# DESCRIPTION: Set the name of the partition layout/scheme that was used to create the
#              partition table
#  Required Params:
#      1) blockDevice - string value of the device (i.e. "/dev/sda")
#---------------------------------------------------------------------------------------
setPartitionLayout() {
  local blockDevice=""
  local keyName=${VAR_KEY_NAMES[5]}

  if [ ${varMap["PassReqs"]} -lt 1 ]; then
    if [ "$#" -gt 0 ]; then
      blockDevice="$1"
    else
      blockDevice="${varMap["BLOCK-DEVICE"]}"
    fi

    local existFlag=$(isLVM "$blockDevice")

    varMap["PART-TBL-TYPE"]=$(parted -s "$blockDevice" print | grep 'Partition Table' | awk '{print $3}')

    if ${existFlag}; then
      varMap["VG-NAME"]=$(trimString $(vgdisplay | grep 'VG Name' | sed 's/VG Name//g'))
      existFlag=$(isLUKS "$blockDevice")
      if ${existFlag}; then
        varMap["$keyName"]=$(echo "${PART_LAYOUTS[2]}")
      else
        varMap["$keyName"]=$(echo "${PART_LAYOUTS[1]}")
      fi
    else
      varMap["$keyName"]=$(echo "${PART_LAYOUTS[0]}")
    fi

    setSwapTypeAndSize
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                     setSwapTypeAndSize
# DESCRIPTION: Set the type of swap and its size
#---------------------------------------------------------------------------------------
setSwapTypeAndSize() {
  if [ ! ${varMap["SWAP-TYPE"]+_} ]; then
    #"systemd-swap"
    local cmdOut=$(swapon --show | awk '{print $2,$3}'| column -t)
    if [[ -n "$cmdOut" ]]; then
      local rows=()
      readarray -t rows <<< "$cmdOut"
      local cols=($(echo "${rows[1]}" | sed 's/  \+/ /g'))
      varMap["SWAP-TYPE"]=$(echo "${cols[0]}")
      varMap["SWAP-SIZE"]=$(echo "${cols[1]}")
    else
      varMap["SWAP-TYPE"]="Skip"
    fi
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                   verifyPartTableDataFile
# DESCRIPTION: Verify the data file for the partition table exists if the device has
#              been partitioned
#---------------------------------------------------------------------------------------
verifyPartTableDataFile() {
  local methodNum=0
  local methodKey=""

  if [ ${varMap["BLOCK-DEVICE"]+_} ]; then
    local existFlag=$(isPartitioned "${varMap["BLOCK-DEVICE"]}")
    if ${existFlag}; then
      if [ ! -f "$PARTITION_TABLE_FILE" ]; then
        methodNum=$(expr ${#pbKeys[@]} + 1)
        methodKey=$(printf "$METHOD_KEY_FMT" ${methodNum})
        pbKeys+=("$methodKey")
        pbTitles["$methodKey"]="Generating data file for the configured partition table...."
        methodNames["$methodKey"]="writePartitionTableToFile"

        rm -rf "$PART_LAYOUT_TEXT_FILE"
      fi

      if [ ! -f "$PART_LAYOUT_TEXT_FILE" ]; then
        methodNum=$(expr ${#pbKeys[@]} + 1)
        methodKey=$(printf "$METHOD_KEY_FMT" ${methodNum})
        pbKeys+=("$methodKey")
        pbTitles["$methodKey"]="Creating text file of the partition scheme...."
        methodNames["$methodKey"]="setPartitionScheme"
      fi
    fi
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                     setPartitionScheme
# DESCRIPTION: Sets the data of the partition table into the global string variable
#              "PARTITION_LAYOUT_TEXT".  The data will contain all rows of
#              the logical volumes and partitions
#---------------------------------------------------------------------------------------
setPartitionScheme() {
  if [ ${varMap["PassReqs"]} -lt 1 ]; then
    if [ -f "$PART_LAYOUT_TEXT_FILE" ]; then
      PARTITION_LAYOUT_TEXT=$(cat "$PART_LAYOUT_TEXT_FILE")
    else
      local pttDesc="${PART_TBL_TYPES[${varMap[PART-TBL-TYPE]}]}"
      local lvmDesc=$(getDescForLVM)
      PARTITION_LAYOUT_TEXT=$(getDataFromPartitionTable "$BLOCK_DEVICE" "$BLOCK_DEVICE_SIZE" "$pttDesc" "$lvmDesc")
      PARTITION_LAYOUT_TEXT=$(echo "$PARTITION_LAYOUT_TEXT" | sed 's/^--+/+/g')
      echo "$PARTITION_LAYOUT_TEXT" > "$PART_LAYOUT_TEXT_FILE"
    fi
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                     initializeChecklist
# DESCRIPTION: Initialize the global array variable "INST_GUIDE_CHECKLIST"
#---------------------------------------------------------------------------------------
initializeChecklist() {
  if [ ${varMap["INST_GUIDE_CHECKLIST"]+_} ] ; then
    readarray -t INST_GUIDE_CHECKLIST <<< "${varMap["INST_GUIDE_CHECKLIST"]//$FORM_FLD_SEP/$'\n'}"
  else
    local numInstSteps=$(expr ${#stepTotals[@]} - 1)
    local methodNum=$(expr ${#pbKeys[@]} + 1)
    local methodKey=$(printf "$METHOD_KEY_FMT" ${methodNum})
    pbKeys+=("$methodKey")
    pbTitles["$methodKey"]="Initializing the Installation Guide's Checklist...."
    methodNames["$methodKey"]=$(echo "setInstStepInChecklist ${numInstSteps}")
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                 setInstStepInChecklist
# DESCRIPTION: Set/Mark the steps & their tasks that have been executed within the
#              global array variable "INST_GUIDE_CHECKLIST"
#---------------------------------------------------------------------------------------
setInstStepInChecklist() {
  local numInstSteps=$1
  local instStepNum=0
  local startIdx=0
  local endIdx=1
  local totSteps=0
  local totOn=0
  local -A startEnd=(["start"]=0 ["end"]=0)

  if [ ${varMap["PassReqs"]} -lt 1 ]; then
    for instStepNum in $(eval echo "{1..${numInstSteps}}"); do
      totSteps=${stepTotals[$instStepNum]}
      setStartEndIndexes ${instStepNum}
      startIdx=${startEnd["start"]}
      endIdx=${startEnd["end"]}

      setSubTaskStatus ${startIdx} ${endIdx}

      if [ ${startIdx} -lt ${endIdx} ]; then
        totOn=$(getTotalOn ${startIdx} ${endIdx})
        startIdx=$(expr ${startIdx} - 1)
        if [ ${totOn} -eq ${totSteps} ]; then
          INST_GUIDE_CHECKLIST[$startIdx]=1
        else
          INST_GUIDE_CHECKLIST[$startIdx]=0
          break
        fi
      fi
    done
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                       verifyInstSteps
# DESCRIPTION: Verify if all the tasks for the first 3 installation steps are done
#---------------------------------------------------------------------------------------
verifyInstSteps() {
  local numInstSteps=3
  local aryIdx=0

  if [[ -z "${VERIFY_INST_STEPS}" ]]; then
    if [ ${varMap["INST_GUIDE_CHECKLIST"]+_} ] ; then
      for instStepNum in $(eval echo "{1..${numInstSteps}}"); do
        totSteps=${stepTotals[$instStepNum]}
        if [ ${INST_GUIDE_CHECKLIST[$aryIdx]} -gt 0 ]; then
          aryIdx=$(expr ${aryIdx} + ${totSteps} + 1)
        else
          local methodNum=$(expr ${#pbKeys[@]} + 1)
          local methodKey=$(printf "$METHOD_KEY_FMT" ${methodNum})
          pbKeys+=("$methodKey")
          pbTitles["$methodKey"]="Initializing the Installation Guide's Checklist...."
          methodNames["$methodKey"]=$(echo "setInstStepInChecklist ${numInstSteps}")

          VERIFY_INST_STEPS="true"
          break
        fi
      done
    fi
  else
    unset VERIFY_INST_STEPS
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getDescForLVM
# DESCRIPTION: Get the name of the Physical Volume and the Volume Group to be
#              displayed when printing the partition table
#      RETURN: string value defined in the DESCRIPTION
#---------------------------------------------------------------------------------------
function getDescForLVM() {
  local cols=()
  local lvmDesc=""

  local deviceNum=$(gdisk -l /"$BLOCK_DEVICE" | grep 8E00 | awk '{print $1}')
  if [ ${#deviceNum} -gt 0 ]; then
    lvmDesc=$(printf "Physical Volume:  [/dev/sda%s]%3sVolume Group:  [%s]" "$deviceNum" " " "${varMap[VG-NAME]}")
  fi

  echo "$lvmDesc"
}

#---------------------------------------------------------------------------------------
#      METHOD:                      executeScript
# DESCRIPTION: Executes the script specified by the parameter
#  Required Params:
#      1) installStep - string that is mapped to the name of the script to execute
#      2)        args - string of arguments for the executable script separated by '\t'
#---------------------------------------------------------------------------------------
executeScript() {
  local installStep="$1"
  local args="$2"
  local scriptName=""
  local scriptArgs=()
  local scriptCmd=""

  if [ "$#" -gt 1 ]; then
    args="$2"
    readarray -t scriptArgs <<< "${args//$'\t'/$'\n'}"
    args=$(printf ' "%s"' "${scriptArgs[@]}")
    args=${args:1}
  fi

  case "$installStep" in
    "step#1")
      scriptName="pre-installation.sh"
    ;;
    "step#2")
      scriptName="installation-step.sh"
    ;;
    "step#3")
      scriptName="config-system-step.sh"
    ;;
    "step#4")
      scriptName="post-install-step/post-installation.sh"
    ;;
  esac

  declare -p varMap > "$ASSOC_ARRAY_FILE"

  if [[ -n "$args" ]]; then
    scriptCmd=$(printf "./%s %s" "$scriptName" "$args")
  else
    scriptCmd=$(printf "./%s" "$scriptName")
  fi

  clog_info "scriptCmd=[$scriptCmd]" >> "$INSTALLER_LOG"
  eval "$scriptCmd"

  if [ -f "$UPD_ASSOC_ARRAY_FILE" ]; then
    source -- "${UPD_ASSOC_ARRAY_FILE}"

    for key in "${!updVarMap[@]}"; do
      varMap["$key"]=$(echo "${updVarMap[$key]}")
    done

    rm -rf "${UPD_ASSOC_ARRAY_FILE}"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                     checkSystemClock
# DESCRIPTION: Ensure the system clock is accurate
#---------------------------------------------------------------------------------------
checkSystemClock() {
  IFS=$'\n' read -d '' -r -a lines <<< $(timedatectl status)
  IFS=':' read -a fields <<< "${lines[4]}"
  setNTPD="$(echo -e "${fields[1]}" | tr -d '[:space:]')"
  if [ "$setNTPD" != "yes" ]; then
    timedatectl set-ntp true
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                 dispStepErrorMsg
# DESCRIPTION: Displays error message when trying to execute the installation/main step
#              or its tasks when the previous installation/main step is not done
#---------------------------------------------------------------------------------------
dispStepErrorMsg() {
  local stepNum=$1
  local prevStepNum=$(expr $1 - 1)
  local stepKey=$(printf "step#%d" "$stepNum")
  local prevStepKey=$(printf "step#%d" "$prevStepNum")
  local step=$(printf "\"%s:  %s\"" "$stepKey" "${checkListSteps[$stepKey]}")
  local prevStep=$(printf "\"%s:  %s\"" "$prevStepKey" "${checkListSteps[$prevStepKey]}")
  local textArray=(" "
  "The tasks for the previous installation step $prevStep"
  "are not done!  Please complete all the tasks for the previous step before"
  "selecting the installation step $step" "or its tasks!"
  )
  local errorMsg=$(getTextForDialog "${textArray[@]}")

  dispInvValueEntered "$DIALOG_BACK_TITLE" "$errorMsg" "Invalid Step & Tasks Selected"
}

#---------------------------------------------------------------------------------------
#      METHOD:                     setStartEndIndexes
# DESCRIPTION: Set the starting and ending indexes within the global array variable
#              "INST_GUIDE_CHECKLIST" for the tasks of an installation/main step
#  Required Params:
#      1) instStepNum - the step number of an installation/main step
#---------------------------------------------------------------------------------------
setStartEndIndexes() {
  local instStepNum=$1
  local startIdx=0
  local endIdx=0
  local totSteps=0

  #declare INST_GUIDE_CHECKLIST=( 1 0 0 0 0 0 0 0 0 2  0  0  3  0  0  0  0  0  0  0  4  5 )
  #                               0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21

  for stepNum in $(eval echo "{1..${instStepNum}}"); do
    totSteps=${stepTotals[$stepNum]}
    case ${stepNum} in
      1)
        startIdx=1
        endIdx=${totSteps}
      ;;
      2)
        startIdx=$(expr ${endIdx} + 2)
        endIdx=$(expr ${endIdx} + ${totSteps} + 1)
      ;;
      3)
        startIdx=$(expr ${endIdx} + 2)
        endIdx=$(expr ${endIdx} + ${totSteps} + 1)
      ;;
      4)
        startIdx=$(expr ${endIdx} + 1)
        endIdx=$(expr ${endIdx} + ${totSteps})
      ;;
      5)
        startIdx=$(expr ${endIdx} + 1)
        endIdx=$(expr ${endIdx} + ${totSteps})
      ;;
    esac
  done
  startEnd["start"]=${startIdx}
  startEnd["end"]=${endIdx}
}

#---------------------------------------------------------------------------------------
#      METHOD:                    setSubTaskStatus
# DESCRIPTION: Within the global array variable "INST_GUIDE_CHECKLIST", set/mark the
#              tasks of their associated installation/main step that have been executed
#  Required Params:
#      1) startIdx - array index within the global array variable "INST_GUIDE_CHECKLIST"
#                    that is the first task of the installation/main step
#      2)   endIdx - array index within the global array variable "INST_GUIDE_CHECKLIST"
#                    that is the last task of the installation/main step
#---------------------------------------------------------------------------------------
setSubTaskStatus() {
  local startIdx=$1
  local endIdx=$2
  local onOffFlag=0
  local aryIdx=0
  local setFlag=$(echo 'false' && return 1)
  local totOn=0

  for aryIdx in $(eval echo "{$startIdx..${endIdx}}"); do
    onOffFlag=${INST_GUIDE_CHECKLIST[$aryIdx]}
    if [ ${onOffFlag} -lt 1 ]; then
      onOffFlag=0
      setFlag=$(isValueSet ${aryIdx})
      if ${setFlag}; then
        onOffFlag=1
      else
        setValueOfTask ${aryIdx}
        onOffFlag=${INST_GUIDE_CHECKLIST[$aryIdx]}
      fi
      totOn=$(expr $totOn + $onOffFlag)
      INST_GUIDE_CHECKLIST[$aryIdx]=${onOffFlag}
    else
      totOn=$(expr $totOn + 1)
    fi
  done

  if [ ${startIdx} -lt ${endIdx} ]; then
    aryIdx=$(expr ${startIdx} - 1)
  else
    aryIdx=${endIdx}
  fi
  INST_GUIDE_CHECKLIST[$aryIdx]=${totOn}
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                       isValueSet
# DESCRIPTION: Checks to see if the value of the task for an installation/main step exists
#      RETURN: 0 - true, 1 - false
#  Required Params:
#      1) arrayIndex - array index within the global array variable "INST_GUIDE_CHECKLIST"
#                      that maps to the task of the installation/main step
#---------------------------------------------------------------------------------------
function isValueSet() {
  local arrayIndex=$1
  local keyName=${VAR_KEY_NAMES[${arrayIndex}]}

  if [ ${varMap["$keyName"]+_} ]; then
    echo 'true' && return 0
  fi

  echo 'false' && return 1
}

#---------------------------------------------------------------------------------------
#      METHOD:                       setValueOfTask
# DESCRIPTION: Set the value of the task for an installation/main step if it exists
#  Required Params:
#      1) arrayIndex - array index within the global array variable "INST_GUIDE_CHECKLIST"
#                      that maps to the task of the installation/main step
#---------------------------------------------------------------------------------------
setValueOfTask() {
  local arrayIndex=$1
  local prevIndex=$(expr ${arrayIndex} - 1)
  local result=$(echo 'false' && return 1)
  local keyName=${VAR_KEY_NAMES[${arrayIndex}]}
  local existFlag=$(echo 'false' && return 1)
  local blockDevice=$(echo "${varMap["BLOCK-DEVICE"]}")
  local fileName=""
  local cmdOut=""
  local outArray=()
  local sep=""

  case ${arrayIndex} in
    5)
      existFlag=$(isPartitioned "$blockDevice")
      if ${existFlag}; then
        setPartitionLayout "$blockDevice"
        result=$(echo 'true' && return 0)
      fi
    ;;
    6)
      if [ ${INST_GUIDE_CHECKLIST[${prevIndex}]} -gt 0 ]; then
	existFlag=$(isFmtOrMnt 1 "$blockDevice")
	if ${existFlag}; then
	  varMap["$keyName"]=$(echo "done")
	  result=$(echo 'true' && return 0)
	fi
      fi
    ;;
    7)
      if [ ${INST_GUIDE_CHECKLIST[${prevIndex}]} -gt 0 ]; then
	existFlag=$(isFmtOrMnt 2 "$blockDevice")
	if ${existFlag}; then
	  varMap["$keyName"]=$(echo "done")
	  result=$(echo 'true' && return 0)
	fi
      fi
    ;;
    9)
      if [ -d "/mnt/etc" ]; then
	local aurPckgs=($(printf "%s\n" "${!LINUX_KERNEL_NAMES[@]}" | sort -r))
	for pckg in "${aurPckgs[@]}"; do
	  existFlag=$(isPackageInstalled "$pckg")
	  if ${existFlag}; then
	    varMap["$keyName"]=$(printf "%s:  %s" "${LINUX_KERNEL_NAMES["$pckg"]}" "$pckg")
	    result=$(echo 'true' && return 0)
	    break
	  fi
	done
      fi
    ;;
    10)
      fileName="${ROOT_MOUNTPOINT}/etc/pacman.d/mirrorlist"
      if [ -f "$fileName" ]; then
        local ctryArray=()
        while IFS='' read -r line || [[ -n "$line" ]]; do
          if [[ $line =~ "reflector" ]]; then
            outArray=()
            sep="--"
            readarray -t outArray <<< "${line//$sep/$'\n'}"
            sep="-c "
            readarray -t outArray <<< "${outArray[0]//$sep/$'\n'}"
            for ctry in "${outArray[@]:1}"; do
              ctry=$(trimString "$ctry")
              ctryArray+=("$ctry")
            done
          fi
        done < "$fileName"
        if [ ${#ctryArray[@]} -gt 0 ]; then
          local concatStr=$(printf ",%s" "${ctryArray[@]}")
          varMap["$keyName"]="${concatStr:1}"
          result=$(echo 'true' && return 0)
        fi
      fi
    ;;
    12)
      fileName="${ROOT_MOUNTPOINT}/etc/fstab"
      local numLines=0
      if [ -f "$fileName" ]; then
	numLines=$(wc -l "$fileName" | awk '{print $1}')
      fi
      if [ ${numLines} -gt 4 ]; then
        local fstabID=""
        local idCnt=$(grep '^UUID=' "$fileName" | wc -l)
        local lblCnt=$(grep '^LABEL=' "$fileName" | wc -l)
        if [ ${idCnt} -gt 0 ]; then
          fstabID="FS UUIDs"
        elif [ ${lblCnt} -gt 0 ]; then
          fstabID="FS LABELs"
        else
          fstabID="Dev. Names"
        fi
        varMap["$keyName"]="$fstabID"
        result=$(echo 'true' && return 0)
      fi
    ;;
    13)
      existFlag=$(isPackageInstalled "ntp")
      if ${existFlag}; then
        cmdOut=$(changeRoot "timedatectl")
        readarray -t outArray <<< "$cmdOut"
        varMap["$keyName"]=$(trimString "${outArray[3]}")
        cmdOut=$(trimString "${outArray[-1]}")
        readarray -t outArray <<< "${cmdOut//$':'/$'\n'}"
        cmdOut=$(trimString "${outArray[-1]}")
        if [ "$cmdOut" == "no" ]; then
          varMap["Time Standard"]="UTC"
        else
          varMap["Time Standard"]="localtime"
        fi
        readarray -t outArray <<< "${varMap["$keyName"]//$':'/$'\n'}"
        varMap["$keyName"]=$(trimString "${outArray[-1]}")
        result=$(echo 'true' && return 0)
      fi
    ;;
    14)
      fileName="${ROOT_MOUNTPOINT}/etc/locale.gen"
      if [ -f "$fileName" ]; then
        local numStart=$(grep -c '^#.*UTF-8[[:space:]]*$' "$fileName")
        local numEnd=$(grep -c 'UTF-8[[:space:]]*$' "$fileName")
        if [ ${numStart} -ne ${numEnd} ]; then
          cmdOut=$(changeRoot "localectl status")
          readarray -t outArray <<< "$cmdOut"
          cmdOut=$(trimString "${outArray[0]}")
          readarray -t outArray <<< "${cmdOut//$':'/$'\n'}"
          readarray -t outArray <<< "${outArray[-1]//$'='/$'\n'}"
          varMap["$keyName"]=$(trimString "${outArray[-1]}")
          result=$(echo 'true' && return 0)
        fi
      fi
    ;;
    15)
      fileName="${ROOT_MOUNTPOINT}/etc/hostname"
      if [ -f "$fileName" ]; then
        existFlag=$(isPackageInstalled "iproute2")
        if ${existFlag}; then
          varMap["$keyName"]=$(changeRoot "cat ${ROOT_MOUNTPOINT}/etc/hostname")
          result=$(echo 'true' && return 0)
        fi
      fi
    ;;
    16)
      fileName="${ROOT_MOUNTPOINT}/etc/mkinitcpio.conf"
      if [ -f "$fileName" ]; then
        local lineNum=$(awk '/AALP-GYGI/{print NR; exit}' "$fileName")

        if [ ${#lineNum} -gt 0 ]; then
          varMap["$keyName"]="true"
          result=$(echo 'true' && return 0)
        fi
      fi
    ;;
    17)
      local cmd="passwd -S"
      cmdOut=$(changeRoot "$cmd" | awk '{print $2}')
      if [ "$cmdOut" == "P" ]; then
        varMap["$keyName"]="true"
        result=$(echo 'true' && return 0)
      fi
    ;;
    18)
      local aurPckgs=("grub" "refind-efi" "syslinux" "systemd-boot")
      for pckg in "${aurPckgs[@]}"; do
        case "$pckg" in
          "systemd-boot")
            fileName="${ROOT_MOUNTPOINT}/boot/loader/loader.conf"
            if [ -f "$fileName" ]; then
              varMap["BOOT-LOADER"]="$pckg"
              varMap["$keyName"]="true"
              result=$(echo 'true' && return 0)
              break
            fi
          ;;
          *)
            existFlag=$(isPackageInstalled "$pckg")
            if ${existFlag}; then
              varMap["BOOT-LOADER"]="$pckg"
              varMap["$keyName"]="true"
              result=$(echo 'true' && return 0)
              break
            fi
          ;;
        esac
      done
    ;;
  esac

  if ${result}; then
    INST_GUIDE_CHECKLIST[${arrayIndex}]=1
  else
    INST_GUIDE_CHECKLIST[${arrayIndex}]=0
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                      isInstStepDone
# DESCRIPTION: Check if an installation/main step has been marked as complete within
#              the global array var "INST_GUIDE_CHECKLIST"
#      RETURN: 0 - true, 1 - false
#  Required Params:
#      1) stepNum - integer value of the installation/main step
#                   (i.e. for the installation guide menu choice "step#2", the step
#                   number would be 2)
#---------------------------------------------------------------------------------------
function isInstStepDone() {
  local arrayIndex=$(getArrayIndex $1)

  if [ ${INST_GUIDE_CHECKLIST[${arrayIndex}]} -gt 0 ]; then
    echo 'true' && return 0
  fi

  echo 'false' && return 1
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                       getArrayIndex
# DESCRIPTION: Get the value of the index to the global array var "INST_GUIDE_CHECKLIST" for either
#              an installation/main step, or the task of an installation/main step
#      RETURN: index to the global array var "INST_GUIDE_CHECKLIST"
#  Required Params:
#      1) stepNum - integer value of the installation/main step
#                   (i.e. for the installation guide menu choice "step#2", the step
#                   number would be 2)
#   Optional Param:
#      2) taskNum - integer value for the task of an installation/main step
#                   (i.e. for the installation guide menu choice "step#2.1", the task
#                   number would be 1)
#---------------------------------------------------------------------------------------
function getArrayIndex() {
  local stepNum=$1
  local arrayIndex=0
  local len=${#INST_GUIDE_CHECKLIST[@]}

  if [ ${stepNum} -gt 4 ]; then
    arrayIndex=$(expr ${len} - 1)
  elif [ ${stepNum} -gt 1 ]; then
    for instStepNum in $(eval echo "{2..${stepNum}}"); do
      prevStepNum=$(expr ${instStepNum} - 1)
      totSteps=${stepTotals[${prevStepNum}]}
      arrayIndex=$(expr ${arrayIndex} + ${totSteps} + 1)
    done
  fi

  if [ "$#" -gt 1 ]; then
    arrayIndex=$(expr ${arrayIndex} + $2)
  fi

  echo ${arrayIndex}
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     isTaskForStepDone
# DESCRIPTION: Check if a task of an installation/main step has been marked as complete
#              within the global array var "INST_GUIDE_CHECKLIST"
#      RETURN: 0 - true, 1 - false
#  Required Params:
#      1) stepNum - integer value of the installation/main step
#                   (i.e. for the installation guide menu choice "step#2", the step
#                   number would be 2)
#      2) taskNum - integer value for the task of an installation/main step
#                   (i.e. for the installation guide menu choice "step#2.1", the task
#                   number would be 1)
#---------------------------------------------------------------------------------------
function isTaskForStepDone() {
  local arrayIndex=$(getArrayIndex $1 $2)
  local elem=${INST_GUIDE_CHECKLIST[$arrayIndex]}

  if [ ${elem} -gt 0 ]; then
    echo 'true' && return 0
  fi

  echo 'false' && return 1
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 appendValueToTaskDesc
# DESCRIPTION: Append the value that was configured to the end of task's description
#  Required Params:
#      1) installation/main step number
#      2) step number of the sub task of the installation/main step
#---------------------------------------------------------------------------------------
function appendValueToTaskDesc() {
  local arrayIndex=$(getArrayIndex $1 $2)
  local step=$(printf "step#%d.%d" $1 $2)
  local stepDesc=$(echo "${checkListSteps[$step]}")
  local keyName=${VAR_KEY_NAMES[${arrayIndex}]}

  if [ ${varMap["$keyName"]+_} ]; then
    local elem=$(echo "${varMap["$keyName"]}")
    if [ "$elem" != "done" ]; then
      local len=${#stepDesc}
      local pad=$(expr $MAX_LINE_CHARS - $len)
      local fmt="%"
      fmt+=$(echo "$pad")
      fmt+="s"
      stepDesc=$(printf "%s$fmt[%s]" "$stepDesc" " " "${varMap["$keyName"]}")
    fi
  fi

  echo "$stepDesc"
}

#---------------------------------------------------------------------------------------
#      METHOD:                 processSelStepsAndTasks
# DESCRIPTION: Process all the steps & their associated tasks that were selected on
#              the installation guide menu
#---------------------------------------------------------------------------------------
processSelStepsAndTasks() {
  local instSteps=()
  local -A instStepTasks=()
  local instStep=""
  local stepTasks=""

  setStepAndTasksToExec

  if [ ${#instSteps[@]} -gt 0 ]; then
    instStep="${instSteps[0]}"
    stepTasks="${instStepTasks[$instStep]}"
    if [[ -n "$stepTasks" ]]; then
      executeScript "$instStep" "$stepTasks"
      updateChecklist "$instStep" "$stepTasks"
    else
      if [ "$instStep" == "step#4" ]; then
        executeScript "$instStep"
        if [ ${varMap["POST-INST"]+_} ]; then
          INST_GUIDE_CHECKLIST[-2]=1
          local concatStr=$(printf "${FORM_FLD_SEP}%d" "${INST_GUIDE_CHECKLIST[@]}")
          varMap["INST_GUIDE_CHECKLIST"]=$(echo "${concatStr:${#FORM_FLD_SEP}}")
        fi
      else
        showFinalStep
      fi
    fi
  #else
    #### TODO:  Change to display all steps have completed
    #dispStepErrorMsg ${stepNum}
  #  clog_error "dispStepErrorMsg ${stepNum}"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                    backup_setStepAndTasksToExec
# DESCRIPTION: Sets the installation/main step as well as its tasks to execute
#---------------------------------------------------------------------------------------
backup_setStepAndTasksToExec() {
  local selectedVals=()
  local stepKey=""
  local stepTasks=()
  local doneFlag=$(echo 'false' && return 1)
  local stepNum=()

  readarray -t selectedVals <<< "${mbSelVal// /$'\n'}"

  for stepTask in "${selectedVals[@]}"; do
    stepNum=$(echo "$stepTask" | cut -d"#" -f2)
    case "$stepTask" in
      "step#1"|"step#2"|"step#3")
        doneFlag=$(isInstStepDone ${stepNum})
        if ! ${doneFlag}; then
          stepKey="$stepTask"
          readarray -t stepTasks <<< "${INST_STEP_TASKS[$stepTask]//$'\t'/$'\n'}"
          break
        fi
      ;;
      "step#4"|"step#5")
        doneFlag=$(isInstStepDone ${stepNum})
        if ! ${doneFlag}; then
          stepKey="$stepTask"
          stepTasks=()
          break
        fi
      ;;
      *)
        stepTasks+=("$stepTask")
      ;;
    esac
  done
    echo "stepTasks=[${stepTasks[@]}]" >> "$INSTALLER_LOG"

  if [ ${#stepTasks[@]} -gt 0 ]; then
    stepKey=$(getInstStep "${stepTasks[0]}")
    stepNum=$(echo "$stepKey" | cut -d"#" -f2)
    doneFlag=$(isInstStepDone ${stepNum})
    if ! ${doneFlag}; then
      instSteps=("$stepKey")
      instStepTasks["$stepKey"]=$(getTasksNotDone)
    fi
  elif [[ -n "$stepKey" ]]; then
    instSteps=("$stepKey")
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                    setStepAndTasksToExec
# DESCRIPTION: Sets the installation/main step as well as its tasks to execute
#---------------------------------------------------------------------------------------
setStepAndTasksToExec() {
  local concatStr=""
  local -A selStepTasks=()

  setSelStepTasks

  local stepKeys=($(printf "%s\n" "${!selStepTasks[@]}" | sort -n))
  for stepKey in "${stepKeys[@]}"; do
    local stepNum=$(echo "$stepKey" | cut -d"#" -f2)
    local doneFlag=$(isInstStepDone ${stepNum})
    if ! ${doneFlag}; then
      instSteps=("$stepKey")
      if [ ${stepNum} -lt 4 ]; then
        concatStr="${selStepTasks["$stepKey"]}"
        if [ ${#concatStr} -gt 0 ]; then
          readarray -t stepTasks <<< "$concatStr"
          concatStr=$(getTasksNotDone)
        else
          concatStr="${INST_STEP_TASKS["$stepKey"]}"
        fi
        instStepTasks["$stepKey"]="$concatStr"
      fi
      break
    fi
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                    setSelStepTasks
# DESCRIPTION: Sets the steps & tasks that were selected within the associative array 
#              "selStepTasks" that was declared in the calling method.
#---------------------------------------------------------------------------------------
setSelStepTasks() {
  local stepTasks=()
  local selectedVals=()
  local stepKey=""
  local concatStr=""

  readarray -t selectedVals <<< "${mbSelVal// /$'\n'}"

  for selVal in "${selectedVals[@]}"; do
    if [ ${#selVal} -gt 6 ]; then
      stepKey="${selVal:0:6}"
      if [ ${selStepTasks["$stepKey"]+_} ]; then
        stepTasks+=("$selVal")
      else
        concatStr=$(printf "\n%s" "${stepTasks[@]}")
        selStepTasks["$stepKey"]="${concatStr:1}"
        stepTasks=("$selVal")
      fi
    else
      selStepTasks["$selVal"]=""
      if [ ${#stepTasks[@]} -gt 0 ]; then
        stepKey="${stepTasks[-1]:0:6}"
        concatStr=$(printf "\n%s" "${stepTasks[@]}")
        selStepTasks["$stepKey"]="${concatStr:1}"
        stepTasks=()
      fi
    fi
  done

  if [ ${#stepTasks[@]} -gt 0 ]; then
    stepKey="${stepTasks[-1]:0:6}"
    concatStr=$(printf "\n%s" "${stepTasks[@]}")
    selStepTasks["$stepKey"]="${concatStr:1}"
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     getInstStep
# DESCRIPTION: Get the installation/main step that a task is associated with
#      RETURN: installation/main step in the form of "step#d"
#  Required Params:
#      1) task of an installation/main step in the form of "step#d.d"
#---------------------------------------------------------------------------------------
function getInstStep() {
  local instStep=$(echo "$1" | cut -d"#" -f2)
  local stepNum=$(echo "$instStep" | cut -d"." -f1)
  instStep=$(printf "step#%d" ${stepNum})
  echo "$instStep"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getTasksNotDone
# DESCRIPTION: Get the tasks that are NOT done/finished for an installation/main step
#      RETURN: string containing tasks separated by '\t'
#---------------------------------------------------------------------------------------
function getTasksNotDone() {
  local splitArray=()
  local taskArray=()
  local doneFlag=0
  local tasksForStep=""

  for task in "${stepTasks[@]}"; do
    splitArray=(${task//#/ })
    splitArray=(${splitArray[1]//\./ })
    doneFlag=$(isTaskForStepDone ${splitArray[0]} ${splitArray[1]})
    if ! ${doneFlag}; then
      taskArray+=("$task")
    fi
  done

  if [ ${#taskArray[@]} -gt 0 ]; then
    tasksForStep=$(printf "\t%s" "${taskArray[@]}")
    tasksForStep=${tasksForStep:1}
  fi

  echo "$tasksForStep"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     removeDupElems
# DESCRIPTION: Remove any duplicate elements from the array
#      RETURN: concatenated string separated by '\t'
#  Required Params:
#      1) aryElem - array of values
#---------------------------------------------------------------------------------------
function removeDupElems() {
  local -n aryElem=$1
  local str=$(printf "\n%s" "${aryElem[@]}")
  str=${str:1}
  echo -e "$str" > "/tmp/array.txt"
  local output=$(cat "/tmp/array.txt" | awk '!x[$0]++')

  echo "$output"
}

#---------------------------------------------------------------------------------------
#      METHOD:                     updateChecklist
# DESCRIPTION: Updates the global array variable "INST_GUIDE_CHECKLIST" by marking the
#              tasks of an installation/main step as done if the values are set.
#  Required Params:
#      1) installStep - string that is mapped to the name of the script to execute
#      2)        args - string of arguments for the executable script separated by '\t'
#---------------------------------------------------------------------------------------
updateChecklist() {
clog_info "Entering updateChecklist....." >> "$INSTALLER_LOG"
  local installStep="$1"
  local stepTasks="$2"
  local taskArray=()
  local setFlag=0
  local arrayIndex=0
  local tasks=()

  if [ ${#stepTasks} -gt 0 ]; then
    IFS=$'\t' read -a tasks <<< "$stepTasks"
    for task in "${tasks[@]}"; do
      splitArray=(${task//#/ })
      splitArray=(${splitArray[1]//\./ })
      arrayIndex=$(getArrayIndex ${splitArray[0]} ${splitArray[1]})
      setFlag=$(isValueSet ${arrayIndex})
      if ${setFlag}; then
        INST_GUIDE_CHECKLIST[$arrayIndex]=1
      fi
    done

    local doneFlag=$(isAllTasksForInstStepDone ${installStep})
    if ${doneFlag}; then
      arrayIndex=$(getArrayIndex ${splitArray[0]})
      INST_GUIDE_CHECKLIST[$arrayIndex]=1
    fi
  fi

  if [ "$installStep" == "step#1" ]; then
    updatePartSchemeData
  fi

  local concatStr=$(printf "${FORM_FLD_SEP}%d" "${INST_GUIDE_CHECKLIST[@]}")
  varMap["INST_GUIDE_CHECKLIST"]=$(echo "${concatStr:${#FORM_FLD_SEP}}")
  clog_info "Exiting updateChecklist....." >> "$INSTALLER_LOG"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                 isAllTasksForInstStepDone
# DESCRIPTION: Check if all the tasks for an installation/main step have been marked as
#              done within the global array variable "INST_GUIDE_CHECKLIST"
#      RETURN: 0 - true, 1 - false
#  Required Params:
#      1) installStep - string that is mapped to the name of the script to execute
#---------------------------------------------------------------------------------------
function isAllTasksForInstStepDone() {
  local installStep="$1"
  local instStepNum=$(echo "$installStep" | cut -d"#" -f2)
  local totSteps=${stepTotals[$instStepNum]}
  local -A startEnd=(["start"]=0 ["end"]=0)

  setStartEndIndexes ${instStepNum}

  local totOn=$(getTotalOn ${startEnd["start"]} ${startEnd["end"]})

  if [ ${totOn} -eq ${totSteps} ]; then
    echo 'true' && return 0
  fi

  echo 'false' && return 1
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                         getTotalOn
# DESCRIPTION: Get the total number of tasks for an installation/main step that have
#              been marked as done within the global array variable "INST_GUIDE_CHECKLIST"
#      RETURN: 0 - true, 1 - false
#  Required Params:
#      1) startIdx - starting index
#      2)   endIdx - ending index
#---------------------------------------------------------------------------------------
function getTotalOn() {
  local startIdx=$1
  local endIdx=$2
  local totOn=0

  for aryIdx in $(eval echo "{${startIdx}..${endIdx}}"); do
    onOffFlag=${INST_GUIDE_CHECKLIST[${aryIdx}]}
    if [ ${INST_GUIDE_CHECKLIST[${aryIdx}]} -gt 0 ]; then
      totOn=$(expr ${totOn} + 1)
    fi
  done

  echo ${totOn}
}

#---------------------------------------------------------------------------------------
#      METHOD:                    updatePartSchemeData
# DESCRIPTION: Create the files, if necessary, for the partition layout/scheme.
#---------------------------------------------------------------------------------------
updatePartSchemeData() {
  local pbTitle="Updating data for the Partition Layout/Scheme"
  local pbKeys=()
  local -A pbTitles=()
  local -A methodNames=()

  verifyPartTableDataFile

  if [ ${#pbKeys[@]} -gt 0 ]; then
    local filePath=(${PARTITION_TABLE_FILE//\// })
    pbKeys+=("done")
    pbTitles["done"]=$(echo "Finished generating data file '${filePath[-2]}/${filePath[-1]}'")
    dispLoadingDataPB "$DIALOG_BACK_TITLE" "$pbTitle"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                       callInstMethod
# DESCRIPTION: Calls the method to initialize the data for the installation guide.  This
#              is called by the "dispLoadingDataPF" method.
#---------------------------------------------------------------------------------------
callInstMethod() {
  case "$pbKey" in
    "done")
      clog_warn "$pbTitle" >> "$INSTALLER_LOG"

      INST_GUIDE_CHECKLIST[2]=1
      INST_GUIDE_CHECKLIST[3]=1
      INST_GUIDE_CHECKLIST[4]=1
      local concatStr=$(printf "${FORM_FLD_SEP}%d" "${INST_GUIDE_CHECKLIST[@]}")
      varMap["INST_GUIDE_CHECKLIST"]=$(echo "${concatStr:${#FORM_FLD_SEP}}")

      local -A updVarMap=()
      for key in "${!varMap[@]}"; do
        updVarMap["$key"]=$(echo "${varMap[$key]}")
      done

      declare -p updVarMap > "${UPD_ASSOC_ARRAY_FILE}"
      sleep 0.5
    ;;
    *)
      clog_info "Executing method [$methodName]" >> "$INSTALLER_LOG"
      ${methodName}
      clog_info "Method [$methodName] is DONE" >> "$INSTALLER_LOG"
      methodNum=$(expr $methodNum + 1)
      pbPerc=$(printf "%.0f" $(echo "scale=3; $methodNum / $totalMethods * 100" | bc))
      case "$methodName" in
        "setDeviceToPartition")
          if [ ! ${varMap["BLOCK-DEVICE"]+_} ]; then
            continueFlag=$(echo 'false' && return 1)
          fi
        ;;
        "setTypeOfInternetConnection")
          if [ ! ${varMap["CONNECTION-TYPE"]+_} ]; then
            continueFlag=$(echo 'false' && return 1)
          fi
        ;;
      esac
      if ! ${continueFlag}; then
        local -A updVarMap=()
        updVarMap["errorMethod"]="$methodName"
        declare -p updVarMap > "${UPD_ASSOC_ARRAY_FILE}"
      fi
    ;;
  esac
}

#---------------------------------------------------------------------------------------
#      METHOD:                   showMissingRequirements
# DESCRIPTION: Show a dialog to display the requirements and which ones are missing.
#---------------------------------------------------------------------------------------
showMissingRequirements() {
  local reqsArray=(" " "RAM" "Disk Space" "Active I.C." "${REQ_BINARIES[@]}")
  local len=$(expr ${#reqsArray[@]} - 1)
  local chkList=()
  local menuItem=()
  local concatStr=""
  local missingBins=()
  local txtArray=("The installation cannot proceed because of the missing requirements below that"
    "are NOT checked:")
  local errMsg=$(printf " %s" "${txtArray[@]}")
  errMsg=${errMsg:1}

  readarray -t missingBins <<< "${varMap["MissingBinaries"]//$FORM_FLD_SEP/$'\n'}"

  for reqNum in $(eval echo "{1..${len}}"); do
    concatStr=$(getReqMenuItem ${reqNum})
    readarray -t menuItem <<< "${concatStr//$FORM_FLD_SEP/$'\n'}"
    if [ "$DIALOG" == "yad" ]; then
      chkList+=("${menuItem[@]}")
    else
      concatStr=$(printf "$FORM_FLD_SEP%s" "${menuItem[@]:1:2}")
      concatStr=${concatStr:${#FORM_FLD_SEP}}
      concatStr=$(printf '"%s"' "$concatStr")
      menuItem[0]=$(printf '"%s"' "${menuItem[0]}")
      menuItem=("${menuItem[0]}" "$concatStr" "${menuItem[-1]}")
      concatStr=$(printf " %s" "${menuItem[@]}")
      concatStr=${concatStr:1}
      chkList+=("$concatStr")
    fi
  done

  dispMissingRequirements "$DIALOG_BACK_TITLE" "Requirements NOT met!" "$errMsg"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                       getReqMenuItem
# DESCRIPTION: Gets the line/menu item to display for a requirement of the installer
#      RETURN: Concatenated string separated by the global string variable "$FORM_FLD_SEP"
#  Required Params:
#      1) reqNum - index to the array "reqsArray" declared in the calling method
#---------------------------------------------------------------------------------------
function getReqMenuItem() {
  local reqNum=$1
  local reqDesc=""
  local actualVal=""
  local passFlag=$(echo 'true' && return 0)
  local pos=0
  local req=$(echo "${reqsArray[$reqNum]}")

  case ${reqNum} in
    1)  #### RAM
      setReqForRAM
    ;;
    2)  #### Disk Space
      setReqForDiskSpace
    ;;
    3)  #### Internet Connection
      setReqForInternetConnection
    ;;
    *)  #### AUR packages/binaries
      pos=$(findPositionForString missingBins "$req")
      actualVal="   N/A"
      reqDesc="${BINARY_DESCS["$req"]}"
      if [ ${pos} -gt -1 ]; then
        passFlag=$(echo 'false' && return 1)
      fi
    ;;
  esac

  setReqForMenuItem

  local concatStr=$(printf "$FORM_FLD_SEP%s" "${menuItem[@]}")
  concatStr=${concatStr:${#FORM_FLD_SEP}}
  echo "$concatStr"
}

#---------------------------------------------------------------------------------------
#      METHOD:                        setReqForRAM
# DESCRIPTION: Set value for the RAM requirement.
#---------------------------------------------------------------------------------------
setReqForRAM() {
  local mem=`grep MemTotal /proc/meminfo | awk '{print $2}' | sed 's/\..*//'`
  local ram=$(echo "$mem * 1024" | bc)
  local reqRam=$(humanReadableToBytes "511 MiB")
  reqDesc="Minimum 512 MB of RAM (recommended 2 GB)"
  actualVal=$(bytesToHumanReadable ${ram})

  if [ ${reqRam} -gt ${ram} ]; then
    passFlag=$(echo 'false' && return 1)
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                     setReqForDiskSpace
# DESCRIPTION: Set value for the Disk Space requirement.
#---------------------------------------------------------------------------------------
setReqForDiskSpace() {
  local devSize=0
  local reqDevSize=$(humanReadableToBytes "999 MiB")
  reqDesc="1 GB of free disk space (recommended 20 GB)"

  if [ ${varMap["BLOCK_DEVICE_SIZE"]+_} ]; then
    devSize=$(humanReadableToBytes "${varMap["BLOCK_DEVICE_SIZE"]}")
    actualVal=$(echo "${varMap["BLOCK_DEVICE_SIZE"]}")
  else
    actualVal="0 B"
  fi

  if [ ${reqDevSize} -gt ${devSize} ]; then
    passFlag=$(echo 'false' && return 1)
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                 setReqForInternetConnection
# DESCRIPTION: Set value for the Internet Connection requirement.
#---------------------------------------------------------------------------------------
setReqForInternetConnection() {
  reqDesc="An active internet connection"

  if [ ${varMap["CONNECTION-TYPE"]+_} ]; then
    actualVal=$(echo "${varMap["CONNECTION-TYPE"]}")
  else
    passFlag=$(echo 'false' && return 1)
    actualVal="None"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                    setReqForMenuItem
# DESCRIPTION: Set the menu item for a requirement.
#---------------------------------------------------------------------------------------
setReqForMenuItem() {
  if [ "$DIALOG" == "yad" ]; then
    if ${passFlag}; then
      menuItem=(TRUE)
    else
      menuItem=(FALSE)
    fi
    menuItem+=("$req" "$reqDesc" "$actualVal")
  else
    reqDesc=$(printf " - %s" "$reqDesc")
    menuItem=("$req" "$reqDesc" "$actualVal")
    if ${passFlag}; then
      menuItem+=(ON)
    else
      menuItem+=(OFF)
    fi
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                        showFinalStep
# DESCRIPTION: Get confirmation save the data generated by the application and
#              reboot the system.
#---------------------------------------------------------------------------------------
showFinalStep() {
  local textArray=("Congratulations! You have successfully installed a minimal command line Arch"
  "Linux.  The installer will now make a backup of the partition table and save the file in the"
  "data directory.  In addition, a copy of the installer application and the data that was"
  "generated by the installer will be placed in the ROOT user's home directory (i.e. /root).  To"
  "test the installation, the system will need to be rebooted.  Once the reboot is complete, you"
  "should get the Arch Linux login prompt.  Log in as the root user and use the password"
  "set in step#3.6.")
  local dialogText=$(printf " %s" "${textArray[@]}")
  local dialogBorder="$DIALOG_BORDER"
  dialogText="${dialogText:1}"

  if [ "$DIALOG" == "yad" ]; then
    dialogBorder="-----------------------------------------------------------------------------"
  fi

  textArray=("$dialogText" "$dialogBorder" " " "Do you want to continue?")
  dialogText=$(getTextForDialog "${textArray[@]}")

  showRebootConfDlg "$DIALOG_BACK_TITLE" "Reboot Step" "$dialogText"
  if ${yesNoFlag}; then
    runRebootStep
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                        runRebootStep
# DESCRIPTION: Saves a copy of the scripts and data files, makes a backup of the
#              partition table, and then reboots.
#---------------------------------------------------------------------------------------
runRebootStep(){
  INST_GUIDE_CHECKLIST[-2]=1
  local concatStr=$(printf "${FORM_FLD_SEP}%d" "${INST_GUIDE_CHECKLIST[@]}")
  local dt=$(date +%Y-%m-%d)
  local destDir="${ROOT_MOUNTPOINT}/root"
  local fileName="${DATA_DIR}/root.bashrc"

  varMap["INST_GUIDE_CHECKLIST"]=$(echo "${concatStr:${#FORM_FLD_SEP}}")
  declare -p varMap > "$ASSOC_ARRAY_FILE"

  cp -rf "$fileName" "${destDir}/.bashrc"
  destDir="${ROOT_MOUNTPOINT}/root/AALP-GYGInstaller"
  mkdir -p "$destDir"
  cp -rf "$CUR_DIR" "${destDir}"
  fileName="${destDir}/scripts/data/sgdisk-backup_$dt"
  local cmd=$(echo "sgdisk --backup $fileName ${varMap["BLOCK-DEVICE"]}")
  changeRoot "$cmd" &> /dev/null

  local sudoerFile="${ROOT_MOUNTPOINT}/etc/sudoers"
  fileName="${sudoerFile}.orig"

  chmod 777 "$sudoerFile"
  chmod 777 "$fileName"
  mv "$fileName" "$sudoerFile"
  chmod 440 "$sudoerFile"

  swapoff -a
  umount -R "$ROOT_MOUNTPOINT"
  reboot
}

#---------------------------------------------------------------------------------------
#  Main
#---------------------------------------------------------------------------------------
main() {
  cleanDialogFiles
  if [ "$DIALOG" == "yad" ] && [[ ! -f "$DIALOG_IMG_ICON_FILE" ]]; then
    echo "${PART_DIALOG_IMAGE},${PART_WINDOW_ICON}" > "$DIALOG_IMG_ICON_FILE"
  fi

  if [ -f "$ASSOC_ARRAY_FILE" ]; then
    source -- "$ASSOC_ARRAY_FILE"
  fi

  dispWelcome
  if [ $dialogCmdRetVal -lt 1 ]; then
    initInstallationGuide

    while true; do
      if [ ${varMap["PassReqs"]} -lt 1 ]; then
        getInstallationSteps
        cleanDialogFiles
        if [[ -n "$mbSelVal" ]]; then
          mbSelVal=$(echo "$mbSelVal" | sed 's/--//g')
          processSelStepsAndTasks
        else
          break
        fi
      else
        showMissingRequirements
        cleanDialogFiles
        exit 1
      fi
    done

    declare -p varMap > "$ASSOC_ARRAY_FILE"
  fi
}

main "$@"
exit 0
