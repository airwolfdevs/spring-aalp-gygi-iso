#!/bin/bash
#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# globals
#===============================================================================
CUR_DIR="${PWD}"
INC_FILE=$(echo "$CUR_DIR/inc/inst_inc.sh")

source "$INC_FILE"
source "$GRAPHICAL_PATH/install-step-inc.sh"

declare -A varMap
declare BLOCK_DEVICE=""
declare DIALOG_BACK_TITLE=""
declare UEFI_FLAG=0
declare DEBUG_FLAG=1
declare -A MIRROR_CTRY_NAMES=(["AU"]="Australia" ["AT"]="Austria" ["BD"]="Bangladesh" ["BY"]="Belarus"
                          ["BE"]="Belgium" ["BA"]="Bosnia and Herzegovina" ["BR"]="Brazil" ["BG"]="Bulgaria"
                          ["CA"]="Canada" ["CL"]="Chile" ["CN"]="China" ["CO"]="Colombia" ["HR"]="Croatia"
                          ["CZ"]="Czech Republic" ["DK"]="Denmark" ["EC"]="Ecuador" ["FI"]="Finland"
                          ["FR"]="France" ["DE"]="Germany" ["GR"]="Greece" ["HK"]="Hong Kong" ["HU"]="Hungary"
                          ["IS"]="Iceland" ["IN"]="India" ["ID"]="Indonesia" ["IR"]="Iran" ["IE"]="Ireland"
                          ["IL"]="Israel" ["IT"]="Italy" ["JP"]="Japan" ["KZ"]="Kazakhstan" ["LV"]="Latvia"
                          ["LT"]="Lithuania" ["LU"]="Luxembourg" ["MK"]="Macedonia" ["MX"]="Mexico"
                          ["NL"]="Netherlands" ["NC"]="New Caledonia" ["NZ"]="New Zealand" ["NO"]="Norway"
                          ["PH"]="Philippines" ["PL"]="Poland" ["PT"]="Portugal" ["QA"]="Qatar" ["RO"]="Romania"
                          ["RU"]="Russia" ["RS"]="Serbia" ["SG"]="Singapore" ["SK"]="Slovakia" ["SI"]="Slovenia"
                          ["ZA"]="South Africa" ["KR"]="South Korea" ["ES"]="Spain" ["SE"]="Sweden"
                          ["CH"]="Switzerland" ["TW"]="Taiwan" ["TH"]="Thailand" ["TR"]="Turkey" ["UA"]="Ukraine"
                          ["GB"]="United Kingdom" ["US"]="United States" ["VN"]="Vietnam")

declare MIRROR_CTRY_CODES=( "AU" "AT" "BD" "BY" "BE" "BA" "BR" "BG" "CA" "CL" "CN" "CO" "HR" "CZ" "DK" "EC" "FI"
                        "FR" "DE" "GR" "HK" "HU" "IS" "IN" "ID" "IR" "IE" "IL" "IT" "JP" "KZ" "LV" "LT" "LU"
                        "MK" "MX" "NL" "NC" "NZ" "NO" "PH" "PL" "PT" "QA" "RO" "RU" "RS" "SG" "SK" "SI" "ZA"
                        "KR" "ES" "SE" "CH" "TW" "TH" "TR" "UA" "GB" "US" "VN")
declare -A FST_TOOLS=(["btrfs"]="btrfs-progs"
    ["exfat"]="exfat-utils"
    ["f2fs"]="f2fs-tools"
    ["nilfs2"]="nilfs-utils"
    ["ntfs"]="ntfs-3g"
    ["reiser4"]="reiser4progs"
    ["reiserfs"]="reiserfsprogs"
    ["vfat"]="dosfstools")
declare -A HELP_TEXT_ARRAY=(
  ["python"]="Python v3 - Next generation of the python high-level scripting language.  It is a dependency for some of the file system tools and other applications."
  ["btrfs-progs"]="Utilities for the 'Btrfs' file system."
  ["exfat-utils"]="Utilities for the 'exFAT' file system."
  ["f2fs-tools"]="Tools for the 'Flash-Friendly File System (F2FS)' file system."
  ["nilfs-utils"]="Utilities for the 'NILFS2' file system."
  ["ntfs-3g"]="Utilities and drive for the 'NTFS' file system."
  ["reiser4progs"]="Programs for the 'Reiser4' file system."
  ["reiserfsprogs"]="Utilities for the 'ReiserFS' file system."
  ["dosfstools"]="Utilities for the 'VFAT' file system."
  ["base"]="An AUR package group containing 53 packages that are the \"default\" set needed for an Arch Linux install."
  ["base-devel"]="An AUR package group containing 26 packages to build and install AUR packages."
  ["bash-completion"]="Programmable completion for the bash shell."
  ["curl"]="An URL retrieval utility and library."
  ["rsync"]="A file transfer program to keep remote files in sync."
  ["sudo"]="Give certain users the ability to run some commands as root."
  ["tree"]="A directory listing program displaying a depth indented list of files."
  ["wget"]="Network utility to retrieve files from the Web."
  ["linux"]="The Stable/Vanilla Linux kernel and modules that are the most up-to-date, providing the best hardware support, and with a few patches applied."
  ["linux-docs"]="HTML documentation of the Linux kernel hackers manual."
  ["linux-headers"]="Header files and scripts for building modules for the Linux Kernel."
  ["linux-hardened"]="The Linux-hardened kernel and modules that are focused on security, which contains the \"Grsecurity Patchset\" and \"PaX\" for increased security."
  ["linux-hardened-docs"]="HTML documentation of the Linux-hardened kernel hackers manual"
  ["linux-hardened-headers"]="Header files and scripts for building modules for the Linux-hardened kernel."
  ["linux-lts"]="The Linux-lts kernel and modules that are focused on stability, which may lack some newer features because it is based on an older kernel."
  ["linux-lts-docs"]="HTML documentation of the Linux-lts kernel hackers manual"
  ["linux-lts-headers"]="Header files and scripts for building modules for the Linux-lts kernel."
  ["linux-zen"]="The Linux-zen kernel and modules that are a collaboration of kernel hackers to provide the best possible kernel for everyday systems."
  ["linux-zen-docs"]="HTML documentation of the Linux-zen kernel hackers manual"
  ["linux-zen-headers"]="Header files and scripts for building modules for the Linux-zen kernel."
)

source -- "$ASSOC_ARRAY_FILE"

#===============================================================================
# functions/methods
#===============================================================================

#---------------------------------------------------------------------------------------
#      METHOD:                       initVars
# DESCRIPTION: Initialize the global variables.
#  Required Params:
#      1) installStep - 'Format' or 'Mount'
#---------------------------------------------------------------------------------------
initVars() {

  DEBUG_FLAG=$(echo 'false' && return 1)
  if [[ ! -z "${AALP_GYGI_YAD}" ]] && [ "$AALP_GYGI_YAD" == "debug" ]; then
    DEBUG_FLAG=$(echo 'true' && return 0)
  elif [[ ! -z "${AALP_GYGI_DIALOG}" ]] && [ "$AALP_GYGI_DIALOG" == "debug" ]; then
    DEBUG_FLAG=$(echo 'true' && return 0)
  fi

  if ${DEBUG_FLAG}; then
    ROOT_MOUNTPOINT="/tmp/mnt"
    if [ ! -d "${ROOT_MOUNTPOINT}/etc" ] ; then
      mkdir -p "$ROOT_MOUNTPOINT"
      cp -rf "/etc/" "${ROOT_MOUNTPOINT}/."
    fi
  fi

  BLOCK_DEVICE=$(echo "${varMap["BLOCK-DEVICE"]}")
  DIALOG_BACK_TITLE="Step #2:  Installation"
  if [ "${varMap[BOOT-MODE]}" == "UEFI" ]; then
    UEFI_FLAG=$(echo 'true' && return 0)
  else
    UEFI_FLAG=$(echo 'false' && return 1)
  fi

  for key in "${!HELP_TEXT_ARRAY[@]}"; do
    HELP_TEXT_ARRAY["$key"]=$(printf "%10s* \"%s\":  %s" " " "$key" "${HELP_TEXT_ARRAY["$key"]}")
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                   runConfigMirrorListTask
# DESCRIPTION: Configure the list of mirror servers/sites that the packages to be
#              installed will be downloaded from.  These will be defined in
#              /etc/pacman.d/mirrorlist.
#---------------------------------------------------------------------------------------
runConfigMirrorListTask() {
  local helpText=$(getHelpTextForMirroList)
  local dialogText="Select the primary country:"
  local dialogTitle="Configuration of Mirrors"
  local ctryCodes=()
  local primaryCtryCode=""

  selectPrimaryCountry "$DIALOG_BACK_TITLE" "$dialogTitle" "$dialogText" "$helpText"
  cleanDialogFiles
  if [ ${#mbSelVal} -gt 0 ]; then
    primaryCtryCode="$mbSelVal"
    helpText=$(getHelpTextForSecondary "$primaryCtryCode")
    dialogText=$(getDialogTextForSecondary "$primaryCtryCode")
    showSecondaryOption "$DIALOG_BACK_TITLE" "$dialogTitle" "$dialogText" "$helpText"
    cleanDialogFiles
    if ${yesNoFlag}; then
      helpText=$(getHelpTextForSecondary "$primaryCtryCode")
      dialogText=$(getDialogTextForSecondary "$primaryCtryCode")
      setSecCtryCodes "$primaryCtryCode"
      selectSecondaryCountries "$DIALOG_BACK_TITLE" "$dialogTitle" "$dialogText" "$helpText"
      cleanDialogFiles
    fi

    if [ ${#ctryCodes[@]} -gt 0 ]; then
      ctryCodes[-1]=$(trimString "${ctryCodes[-1]}")
      ctryCodes=("$primaryCtryCode" "${ctryCodes[@]}")
    else
      ctryCodes=("$primaryCtryCode")
    fi
  fi

  if [ ${#ctryCodes[@]} -gt 0 ]; then
    configureMirrorList
    local concatStr=$(printf ",%s" "${ctryCodes[@]}")
    varMap["CONFIG-MLIST"]="${concatStr:1}"
  else
    varMap["CONFIG-MLIST"]="Skip"
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getDialogTextForSecondary
# DESCRIPTION: Get the text to display in the secondary option linux "dialog"
#      RETURN: concatenated string
#  Required Params:
#      1) primaryCtryCode - the primary country that the mirrors will be searched first
#---------------------------------------------------------------------------------------
function getDialogTextForSecondary() {
  local textArray=("Primary Country:  [${ISO_CODE_NAMES["$1"]}]" " " " " " "
    "Select a secondary set of countries to be the alternate mirror sites?"
  )

  local dialogText=$(getTextForDialog "${textArray[@]}")

  echo "$dialogText"
}

#---------------------------------------------------------------------------------------
#      METHOD:                       setSecCtryCodes
# DESCRIPTION: Set the list of available countries for the secondary list of mirrors
#              by removing the primary country.
#  Required Params:
#      1) primaryCtryCode - the primary country that the mirrors will be searched first
#---------------------------------------------------------------------------------------
setSecCtryCodes() {
  local idx=$(expr ${#MIRROR_CTRY_CODES[@]} - 1)
  let "pos=(`echo ${MIRROR_CTRY_CODES[@]} | tr -s " " "\n" | grep -n "$1" | cut -d":" -f 1`)"
  if [ ${pos} -lt 2 ]; then
    ctryCodes=("${MIRROR_CTRY_CODES[@]:${pos}}")
  elif [ ${pos} -gt ${idx} ]; then
    ctryCodes=("${MIRROR_CTRY_CODES[@]:0:${idx}}")
  else
    idx=$(expr ${pos} - 1)
    ctryCodes=("${MIRROR_CTRY_CODES[@]:0:${idx}}" "${MIRROR_CTRY_CODES[@]:${pos}}")
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                     configureMirrorList
# DESCRIPTION: Configure the list of mirror servers/sites to be defined in
#              /etc/pacman.d/mirrorlist.  These will be based on the countries selected.
#---------------------------------------------------------------------------------------
configureMirrorList() {
  local lclKeys=()
  local cmdKeys=("primary" "rank-primary" "secondary" "rank-secondary")
  local -A cmdTitles=()
  local -A cmds=()
  local ctryName="${ISO_CODE_NAMES["${ctryCodes[0]}"]}"
  local cmdLines=()
  local concatStr=""
  local fileName="${ROOT_MOUNTPOINT}/etc/pacman.d/mirrorlist"
  local rankMirrorCmd=("rankmirrors -v -n 0 /tmp/mirrorlist.new > /tmp/mirrorlist.%s"
          "&& rm -rf /tmp/mirrorlist.new")

  if [ ! -f "${fileName}.orig" ]; then
    cmdKeys=("backup" "${cmdKeys[@]}")
  fi

  lclKeys=("${cmdKeys[@]}")
  cmdKeys=()
  for cmdKey in "${lclKeys[@]}"; do
    case "$cmdKey" in
      "backup")
        cmdKeys+=("$cmdKey")
        cmds["$cmdKey"]=$(printf "changeRoot \"cp %s %s\"" "$fileName" "${fileName}.orig")
        cmdTitles["$cmdKey"]="Backing up the original mirrorlist...."
      ;;
      "primary"|"rank-primary")
        cmdKeys+=("$cmdKey")
        setCmdsForPrimary "$cmdKey"
      ;;
      "secondary"|"rank-secondary")
        if [ ${#ctryCodes[@]} -gt 1 ]; then
          cmdKeys+=("$cmdKey")
          setCmdsForSecondary "$cmdKey"
        fi
      ;;
    esac
  done

  if [ "$DIALOG" == "yad" ]; then
    executeLinuxCommands "$DIALOG_BACK_TITLE" "Configuration of Mirrors" cmdKeys cmdTitles cmds "--pulsate|"
  else
    executeLinuxCommands "$DIALOG_BACK_TITLE" "Configuration of Mirrors" cmdKeys cmdTitles cmds
  fi

  sed -i '2 a #################                   Primary                    #################' /tmp/mirrorlist.primary
  mv /tmp/mirrorlist.primary /tmp/mirrorlist
  if [ -f /tmp/mirrorlist.secondary ]; then
    echo -e "\n\n" >> /tmp/mirrorlist
    sed -i '2 a #################                  Secondary                   #################' /tmp/mirrorlist.secondary
    cat /tmp/mirrorlist.secondary >> /tmp/mirrorlist
    rm -rf /tmp/mirrorlist.secondary
  fi

  clog_info "Backing up the mirrorlist..." >> "$INSTALLER_LOG"
  mv "$fileName" "${fileName}.backup"
  clog_warn "Rotating the new list into place..." >> "$INSTALLER_LOG"
  mv /tmp/mirrorlist "$fileName"

  rm -rf /tmp/mirrorlist.*
}

#---------------------------------------------------------------------------------------
#      METHOD:                      setCmdsForPrimary
# DESCRIPTION: Set the commands to set the list of mirror sites based on the country
#              selected for the primary.  This will use:
#                   1) "reflector" to get the most up-to-mirrors and sort them by speed
#                   2) "rankmirrors" to rank the mirrors according to their connection
#                      and opening speeds
#---------------------------------------------------------------------------------------
setCmdsForPrimary() {
  local cmdKey="$1"
  case "$cmdKey" in
    "primary")
      cmdLines=("reflector -c '$ctryName'"
        "--connection-timeout 60 --cache-timeout 60 -l 200 -f 200 --age 12"
        "--sort rate --threads 7 --verbose --save /tmp/mirrorlist.new")
      concatStr=$(printf " %s" "${cmdLines[@]}")
      cmds["$cmdKey"]="${concatStr:1}"
      cmdTitles["$cmdKey"]=$(echo "Getting mirror list for Primary.....")
    ;;
    "rank-primary")
      cmdLines=($(printf "${rankMirrorCmd[0]}" "primary"))
      cmdLines+=("${rankMirrorCmd[-1]}")
      concatStr=$(printf " %s" "${cmdLines[@]}")
      cmds["$cmdKey"]="${concatStr:1}"
      cmds["$cmdKey"]=$(printf "${cmds["$cmdKey"]}" " " " ")
      cmdTitles["$cmdKey"]=$(echo "Ranking mirror list for Primary.....")
    ;;
  esac
}

#---------------------------------------------------------------------------------------
#      METHOD:                     setCmdsForSecondary
# DESCRIPTION: Set the commands to set the list of mirror sites based on the countries
#              selected for the alternate/secondary sites.  This will use:
#                   1) "reflector" to get the most up-to-mirrors and sort them by speed
#                      between all the countries selected
#                   2) "rankmirrors" to rank the mirrors according to their connection
#                      and opening speeds
#---------------------------------------------------------------------------------------
setCmdsForSecondary() {
  local cmdKey="$1"
  case "$cmdKey" in
    "secondary")
      cmdLines=("reflector")
      for ctryCode in "${ctryCodes[@]:1}"; do
        ctryName="${ISO_CODE_NAMES["$ctryCode"]}"
        cmdLines+=("-c '$ctryName'")
      done
      cmdLines+=("--connection-timeout 60 --cache-timeout 60 -l 200 -f 200 --age 12"
        "--sort rate --threads 7 --verbose --save /tmp/mirrorlist.new")
      concatStr=$(printf " %s" "${cmdLines[@]}")
      cmds["$cmdKey"]="${concatStr:1}"
      cmdTitles["$cmdKey"]=$(echo "Getting mirror list for Secondary.....")
    ;;
    "rank-secondary")
      cmdLines=($(printf "${rankMirrorCmd[0]}" "secondary"))
      cmdLines+=("${rankMirrorCmd[-1]}")
      concatStr=$(printf " %s" "${cmdLines[@]}")
      cmds["$cmdKey"]="${concatStr:1}"
      cmds["$cmdKey"]=$(printf "${cmds["$cmdKey"]}" " " " ")
      cmdTitles["$cmdKey"]=$(echo "Ranking mirror list for Secondary.....")
    ;;
  esac
}

#---------------------------------------------------------------------------------------
#      METHOD:                    runInstallBaseSystem
# DESCRIPTION: Installs the selected base system that was chosen, as well as the
#              base-devel package using "pacstrap"
#---------------------------------------------------------------------------------------
runInstallBaseSystem() {
  local kernelChoices=("linux" "linux-hardened" "linux-lts" "linux-zen")
  local -A kernelDescs=(["linux"]="Stable" ["linux-hardened"]="A security-focused Linux kernel"
    ["linux-lts"]="Long-term support (LTS) " ["linux-zen"]="A collaborative effort of kernel hackers")
  local dialogText="Select the Linux kernel to install:"
  local dialogTitle="Base Packages Installation"
  local helpText=$(getHelpTextForBaseInstall)
  local cmdKeys=()
  local -A cmdTitles=()
  local -A cmds=()

  while true; do
    yesNoFlag=$(echo 'false' && return 1)
    selectLinuxKernel "$DIALOG_BACK_TITLE" "$dialogTitle" "$dialogText" "$helpText"
    cleanDialogFiles
    if [ ${#mbSelVal} -gt 0 ]; then
      confirmToInstall
      if ${yesNoFlag}; then
        break
      fi
    else
      break
    fi
  done

  if ${yesNoFlag}; then
    if [ "$DIALOG" == "yad" ]; then
      executeLinuxCommands "$DIALOG_BACK_TITLE" "Configuration of Mirrors" cmdKeys cmdTitles cmds "--pulsate|"
    else
      executeLinuxCommands "$DIALOG_BACK_TITLE" "Configuration of Mirrors" cmdKeys cmdTitles cmds
    fi
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                      confirmToInstall
# DESCRIPTION: Get confirmation to install the base, linux kernel, etc.
#---------------------------------------------------------------------------------------
confirmToInstall() {
  local cmd=""
  local helpCmds=()
  local keys=("base" "upd" "kernel" "common" "tools")
  local lclTextArray=("Confirm to:")
  local dialogTitle="Confirmation of Installation"
  local title=""
  local cmdNum=1
  cmdKeys=()

  if [ "${varMap["SWAP-TYPE"]}" == "systemd-swap" ]; then
    keys+=("swap")
  fi

  for cmdKey in "${keys[@]}"; do
    case "$cmdKey" in
      "base")
        cmd="pacstrap ${ROOT_MOUNTPOINT} base base-devel"
        title="Installing Base group packages....."
      ;;
      "kernel")
        cmd=$(getCmdToInstKernel)
        title="Installing Linux Kernel....."
      ;;
      "common")
        cmd=$(getCmdToInstCommonCmds)
        title="Installing Python and Common Linux commands....."
      ;;
      "tools")
        cmd=$(getCmdToInstLinuxTools)
        title="Installing tools/utilities for the file system types....."
      ;;
      "swap")
        cmd=$(printf "changeRoot \"%s\"" "pacman -S --noconfirm systemd-swap")
        title="Installing 'systemd-swap' scripts....."
      ;;
      "upd")
        cmd=$(printf "changeRoot \"%s\"" "pacman -Syyu --noconfirm")
        title="Synchronizing, refreshing, and upgrading AUR packages ....."
      ;;
    esac

    if [ ${#cmd} -gt 0 ]; then
      helpCmds+=("$cmd")
      cmdKeys+=("$cmdKey")
      cmds["$cmdKey"]="$cmd"
      cmdTitles["$cmdKey"]="$title"
      if [ "$cmdKey" == "upd" ]; then
        title="Synchronize, refresh, and upgrade AUR packages ....."
        title=$(printf "   %d)  %s" ${cmdNum} "$title")
      else
        title=$(printf "   %d)  %s" ${cmdNum} "${title//ing}")
      fi
      cmd=$(printf "%7s* %s" " " "$cmd")
      lclTextArray+=("$title" "$cmd" " ")
    fi
    cmdNum=$(expr ${cmdNum} + 1)
  done

  local dialogText=$(getTextForDialog "${lclTextArray[@]}")
  local helpText=$(getHelpTextForInstConf helpCmds)

  showInstConf "$DIALOG_BACK_TITLE" "$dialogTitle" "$dialogText" "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     getCmdToInstKernel
# DESCRIPTION: Get the command that will install the Linux kernel
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getCmdToInstKernel() {
  local docs=$(echo "${mbSelVal}-docs")
  local hdr=$(echo "${mbSelVal}-headers")
  local pckgs=("pacman -S --noconfirm")
  pckgs+=("$mbSelVal")
  pckgs+=("$docs")
  pckgs+=("$hdr")

  local cmd=$(printf " %s" "${pckgs[@]}")
  cmd=$(printf "changeRoot \"%s\"" "${cmd:1}")
  echo "$cmd"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getCmdToInstCommonCmds
# DESCRIPTION: Get the command that will install python v3 & common commands
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getCmdToInstCommonCmds() {
  local pckgs=("pacman -S --noconfirm python")
  pckgs+=("curl" "rsync" "sudo" "tree" "wget")

  local cmd=$(printf " %s" "${pckgs[@]}")
  cmd=$(printf "changeRoot \"%s\"" "${cmd:1}")
  echo "$cmd"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getCmdToInstLinuxTools
# DESCRIPTION: Get the command that will install the tools/utils used by the file system
#              and not part of the AUR "base" group package.
#        NOTE: These are included with the "base" group package:
#                 "e2fsprogs" - "Utilities for the 'Ext3/4' file systems"
#                  "jfsutils" - "Utilities for the 'JFS' file system"
#                  "xfsprogs" - "Utilities for the 'XFS' file system"
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getCmdToInstLinuxTools() {
  local fstArray=()
  local fst=""
  local pckgs=("pacman -S --noconfirm")
  local cmd=""

  setFileSystemTypes

  for fst in "${fstArray[@]}"; do
    if [ ${FST_TOOLS["$fst"]+_} ]; then
      pckgs+=("${FST_TOOLS["$fst"]}")
    fi
  done

  if [ ${#pckgs[@]} -gt 1 ]; then
    cmd=$(printf " %s" "${pckgs[@]}")
    cmd=$(printf "changeRoot \"%s\"" "${cmd:1}")
  fi

  echo "$cmd"
}

#---------------------------------------------------------------------------------------
#      METHOD:                      configSystemdSwap
# DESCRIPTION: Show the message to the user about editing the configuration file
#              for the "systemd-swap" script to manage the swap.
#---------------------------------------------------------------------------------------
configSystemdSwap() {
  local cmd=$(printf "changeRoot \"%s\"" "systemctl enable systemd-swap")
  local editorCmd=${varMap["TEXT-EDITOR"]}
  local configFile="${ROOT_MOUNTPOINT}/etc/systemd/swap.conf"
  local msgTextArray=("URL Reference:" "$URL_REF_BORDER"
  "https://github.com/Nefelim4ag/systemd-swap" "$DIALOG_BORDER" " "
  "The 'systemd-swap' Script is installed to manage the SWAP on either:"
      "       * zswap — Enable/Configure"
      "       * zram — Auto configuration for swap"
      "       * files — (sparse files for saving space, support btrfs)"
      "       * block devices — auto find and do swapon" "$DIALOG_BORDER" " ")
  if [ "$DIALOG" == "yad" ]; then
    msgTextArray+=("   * Click the \"OK\" button " "   * Click the \"Skip\" button ")
  else
    msgTextArray+=("   * Select <   Ok   > " "   * Select <  Skip  > ")
  fi

  msgTextArray[-2]+="to edit the configuration file \"$configFile\" with the text editor '$editorCmd'"
  msgTextArray[-2]+=".  When finished, the 'systemd-swap' service will be enabled."
  msgTextArray[-1]+="to do the editing & enabling at a later time."

  if [ "$DIALOG" == "yad" ]; then
    msgTextArray+=("   * Click the URL above to read more about the configuration.")
  else
    msgTextArray+=(
    "$DIALOG_BORDER" " " "NOTE:" "$NOTE_BORDER"
    "  * Select <View URL> to read more about the configuration."
    )
  fi

  local msgText=$(getTextForDialog "${msgTextArray[@]}")
  showDlgForSystemdSwap "$DIALOG_BACK_TITLE" "Configuration of systemd-swap" "$msgText"
  if ${yesNoFlag}; then
    $editorCmd "$configFile"
    $cmd
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                     setFileSystemTypes
# DESCRIPTION: Set the list of file system types that exist in the partition table
#---------------------------------------------------------------------------------------
setFileSystemTypes() {
  local fst=""
  local partTableArray=()
  local cols=()

  IFS=$'\n' read -d '' -a partTableArray < "$PARTITION_TABLE_FILE"
  for row in "${partTableArray[@]}"; do
    IFS=$'\t' read -a cols <<< "$row"
    fst=$(trimString "${cols[4]}")
    if [ ${#fst} -gt 0 ]; then
      fstArray+=("$fst")
    fi
  done

  if ${UEFI_FLAG}; then
    fstArray+=("vfat")
  fi

  fstArray=($(echo "${fstArray[@]}" | tr ' ' '\n' | sort -u | tr '\n' ' '))
}

#---------------------------------------------------------------------------------------
#  MAIN
#---------------------------------------------------------------------------------------
main() {
  local stepTaskArray=("$@")
  initVars

  for stepTask in "${stepTaskArray[@]}"; do

    stepTask=$(trimString "$stepTask")
    case "$stepTask" in
      "step#2.1")
        runInstallBaseSystem

        #### Make the KEYMAP persistent
        localectl set-keymap --no-convert "${varMap["KEYMAP"]}"

        local existFlag=$(isPackageInstalled "systemd-swap")
        if ${existFlag}; then
          configSystemdSwap
        fi
      ;;
      "step#2.2")
        runConfigMirrorListTask
      ;;
      *)
        echo "invalid_option:[$stepTask]"
      ;;
    esac
  done

  cleanDialogFiles

  local -A updVarMap=()
  for key in "${!varMap[@]}"; do
    updVarMap["$key"]=$(echo "${varMap[$key]}")
  done

  declare -p updVarMap > "${UPD_ASSOC_ARRAY_FILE}"
}

main "$@"
exit 0
