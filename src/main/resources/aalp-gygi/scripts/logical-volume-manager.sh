#!/bin/bash
#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================
CUR_DIR="${PWD}"
INC_FILE=$(echo "$CUR_DIR/inc/inst_inc.sh")
MAN_DEV_INC_FILE=$(echo "$CUR_DIR/inc/manage-device-funcs.sh")

source "$INC_FILE"
source "$MAN_DEV_INC_FILE"
source "$GRAPHICAL_PATH/lvm-part-inc.sh"

declare -A varMap
declare UEFI_FLAG=0
declare BLOCK_DEVICE=""
declare BLOCK_DEVICE_SIZE=""
declare HR_BLOCK_DEVICE_SIZE=""
declare PHYSICAL_VOLUME=""
declare PHYSICAL_VOL_SIZE=""
declare DEVICE_TYPE=""
declare PART_TBL_TYPE=""
declare DIALOG_BACK_TITLE="Logical Volume Manager (LVM)"
declare HOME_LV_NAME="${LOGICAL_VOL_PREFIX}home"
declare VAR_LV_NAME="${LOGICAL_VOL_PREFIX}var"
declare -A HELP_TEXT_OPTS=()
source -- "$ASSOC_ARRAY_FILE"
declare ADD_CUST_TEXT_ARRAY=(
"      choose the name, other than \"var\" & \"home\", and allocation size"
"      to give more flexibility (i.e. backup operations) and not fill up"
"      other logical volumes like '/' or home."
)
declare ADD_SWAP_TEXT_ARRAY=(
"      can take the form of a logical volume or a file. Swap space can be"
"      used for two purposes:"
"      A) extend the virtual memory beyond the installed physical memory"
"         (RAM), a.k.a \"enable swap\""
"      B) for suspend-to-disk support."
)
declare ADD_VAR_TEXT_ARRAY=(
"      will store variable data such as spool directories, files, etc. and"
"      help avoid running out of disk space."
)
declare REMOVE_TEXT_ARRAY=(
"      allows certain logical volumes to be removed from the partition"
"      table that are not required by the system (i.e. root, etc.)"
)

#---------------------------------------------------------------------------------------
#      METHOD:                       initVars
# DESCRIPTION: Initialize the global variables.
#---------------------------------------------------------------------------------------
initVars() {
  if [ "${varMap[BOOT-MODE]}" == "UEFI" ]; then
    UEFI_FLAG=$(echo 'true' && return 0)
  else
    UEFI_FLAG=$(echo 'false' && return 1)
  fi

  initVarsForDevice

  setPhysicalVolume

  setHelpTextOpts

  if [[ ! -f "$DIALOG_IMG_ICON_FILE" ]]; then
    echo "${PART_DIALOG_IMAGE},${PART_WINDOW_ICON}" > "$DIALOG_IMG_ICON_FILE"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                      initVarsForDevice
# DESCRIPTION: Initialize the global variables dealing with the block device &
#              the partition table
#---------------------------------------------------------------------------------------
initVarsForDevice() {
  BLOCK_DEVICE=$(echo "${varMap["BLOCK-DEVICE"]}")
  if [ ${varMap["BLOCK_DEVICE_SIZE"]+_} ]; then
    HR_BLOCK_DEVICE_SIZE=$(echo "${varMap["BLOCK_DEVICE_SIZE"]}")
    BLOCK_DEVICE_SIZE=$(humanReadableToBytes "$HR_BLOCK_DEVICE_SIZE")
  else
    BLOCK_DEVICE_SIZE=$(blockdev --getsize64 "$BLOCK_DEVICE")
    HR_BLOCK_DEVICE_SIZE=$(bytesToHumanReadable "$BLOCK_DEVICE_SIZE")
  fi
  DEVICE_TYPE="Logical Volume"

  if [ ! ${varMap["PART-TBL-TYPE"]+_} ]; then
    PART_TBL_TYPE=$(parted -s "$BLOCK_DEVICE" print | grep 'Partition Table' | awk '{print $3}')
    varMap["PART-TBL-TYPE"]=$(echo "$PART_TBL_TYPE")
  else
    PART_TBL_TYPE=$(echo "${varMap[PART-TBL-TYPE]}")
  fi

  local tableRows=()
  local cols=()

  readarray -t tableRows < "$PARTITION_TABLE_FILE"
  for row in "${tableRows[@]}"; do
    readarray -t cols <<< "${row//$'\t'/$'\n'}"
    if [[ "${cols[1]}" =~ "boot" ]]; then
      varMap["BOOT-DEVICE"]="${cols[0]}"
      break
    fi
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                       setHelpTextOpts
# DESCRIPTION: Set the text for the help of the options to manage the logical volumes
#---------------------------------------------------------------------------------------
setHelpTextOpts() {
  local key=""
  for opt in "${DEVICE_MANAGEMENT_OPTS[@]}"; do
    key=$(echo "${opt}Text")
    case "$opt" in
      "addCustom")
        HELP_TEXT_OPTS["$key"]=$(getTextForDialog "${ADD_CUST_TEXT_ARRAY[@]}")
      ;;
      "addSwap")
        HELP_TEXT_OPTS["$key"]=$(getTextForDialog "${ADD_SWAP_TEXT_ARRAY[@]}")
      ;;
      "addVAR")
        HELP_TEXT_OPTS["$key"]=$(getTextForDialog "${ADD_VAR_TEXT_ARRAY[@]}")
      ;;
      "remove")
        HELP_TEXT_OPTS["$key"]=$(getTextForDialog "${REMOVE_TEXT_ARRAY[@]}")
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     getNextStep
# DESCRIPTION: Determine the next step in managing the logical volumes
#      RETURN: string containing the next step key
#---------------------------------------------------------------------------------------
function getNextStep() {
  local stepNum=5
  local nextStep=""
  local numLines=0
  local rows=()
  local cols=()

  if [ ! ${varMap["VG-NAME"]+_} ]; then
    stepNum=1
  else
    readarray -t rows < "$PARTITION_TABLE_FILE"
    local len=${#rows[@]}
    for (( aryIdx = len-1; aryIdx > -1; aryIdx-- )); do
      readarray -t cols <<< "${rows[$aryIdx]//$'\t'/$'\n'}"

      case "${cols[1]}" in
        "$BOOT_LV_NAME")
          stepNum=3
          break
        ;;
        "$HOME_LV_NAME")
          stepNum=5
          break
        ;;
        "$LVM_PART_NAME")
          if [ ! ${varMap["BOOT-DEVICE"]+_} ]; then
            stepNum=2
          else
            stepNum=3
          fi
          break
        ;;
        *)
          local existFlag=$(isRootDevice "${cols[-1]}")
          if ${existFlag}; then
            stepNum=4
            break
          fi
        ;;
      esac
    done
  fi

  nextStep=$(printf "step#%02d" "$stepNum")
  echo "$nextStep"
}

#---------------------------------------------------------------------------------------
#      METHOD:                 manageLogicalVolumes
# DESCRIPTION: Do all the processing necessary to create the volume group &
#              the logical volumes
#  Required Params:
#      1) nextStep - the next step in the partitioning process
#---------------------------------------------------------------------------------------
manageLogicalVolumes() {
  local nextStep="$1"
  local -A funcParams=(["dialogBackTitle"]="$DIALOG_BACK_TITLE" ["deviceType"]="$DEVICE_TYPE"
    ["statsBlockSize"]="$PHYSICAL_VOL_SIZE"
  )

  case "$nextStep" in
    "step#01")
      echo "lvm-logo.png,${PART_WINDOW_ICON}" > "$DIALOG_IMG_ICON_FILE"
      setNameOfVolumeGroup funcParams
      echo "${PART_DIALOG_IMAGE},${PART_WINDOW_ICON}" > "$DIALOG_IMG_ICON_FILE"
    ;;
    "step#02")
      local numDevs=$(cat "$PARTITION_TABLE_FILE" | wc -l)
      funcParams["deviceName"]="$BOOT_LV_NAME"
      addBootDevice funcParams
      local numLines=$(cat "$PARTITION_TABLE_FILE" | wc -l)
      if [ $numLines -gt $numDevs ]; then
        varMap["BOOT-DEVICE"]="$BOOT_LV_NAME"
      else
        varMap["BOOT-DEVICE"]="Skip"
      fi
    ;;
    "step#03")
      funcParams["deviceName"]="$ROOT_LV_NAME"
      addRootDevice funcParams
    ;;
    "step#04")
      funcParams["lastDevice"]=$(getLastLV)
      runDevManOp "$VAR_LV_NAME" funcParams
    ;;
    "step#05")
      saveDataToDisk funcParams
    ;;
  esac
  cleanDialogFiles
}

#---------------------------------------------------------------------------------------
#      METHOD:                 setNameOfVolumeGroup
# DESCRIPTION: Set the name of the volume group
#---------------------------------------------------------------------------------------
setNameOfVolumeGroup() {
  local -n methodParams=$1
  methodParams["dialogTitle"]="Volume Group Name Entry Dialog"

  setDataForPartitionScheme methodParams
  getNameOfVolumeGroup methodParams

  if [ ${#ibEnteredText} -gt 0 ]; then
    varMap["VG-NAME"]="$ibEnteredText"
  else
    cleanKeyAndVals
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                      getLastLV
# DESCRIPTION: Get the last logical volume
#      RETURN: concatenated string delimited by '\t'
#---------------------------------------------------------------------------------------
function getLastLV() {
  local rows=()
  local cols=()
  local hrToBytes=0
  local totUsed=0
  local lvmSize=0
  local rowNum=0

  IFS=$'\n' read -d '' -a rows < "$PARTITION_TABLE_FILE"
  local len=${#rows[@]}
  for (( aryIdx = len-1; aryIdx > -1; aryIdx-- ))
  do
    IFS=$'\t' read -a cols <<< "${rows[$aryIdx]}"
    case "${cols[3]}" in
      "8E00")
        lvmSize=$(humanReadableToBytes "${cols[2]}")
        break
      ;;
      "crypt")
        lvmSize=$(humanReadableToBytes "${cols[2]}")
        break
      ;;
      "lvm")
        hrToBytes=$(humanReadableToBytes "${cols[2]}")
        totUsed=$(expr $totUsed + $hrToBytes)
      ;;
    esac
  done

  lvmSize=$(expr $lvmSize - $totUsed)
  cols[0]="${LAST_LVM_PREFIX}${varMap[VG-NAME]}"
  cols[1]="$HOME_LV_NAME"
  cols[2]=$(bytesToHumanReadable "$lvmSize")
  cols[3]="lvm"
  cols[-1]=" "

  local lastRow=$(printf "\t%s" "${cols[@]}")
  lastRow=${lastRow:1}

  echo -e "$lastRow"
}

#---------------------------------------------------------------------------------------
#      METHOD:                      saveDataToDisk
# DESCRIPTION: Save/Write the rows of logical volumes in the partition table to disk
#  Required Params:
#      1) methodParams - associative array of key/value pairs of parameters for the method:
#                        (see calling method manageLogicalVolumes)
#---------------------------------------------------------------------------------------
saveDataToDisk() {
  local -n methodParams=$1
  local cmdSteps=()
  local linuxCmdKeys=()
  local -A linuxCmdTitles=()
  local -A linuxCmds=()
  local pbTitle="Creating Partitions, Physical Volume, Volume Group, & Logical Volumes"

  setDataForPartitionScheme methodParams

  setLinuxCmds
  setStepsForCmds

  if [ "$DIALOG" == "yad" ]; then
    methodParams["dialogTitle"]=$(echo "Save Confirmation for '${methodParams[dialogBackTitle]}'")
  else
    methodParams["dialogTitle"]=$(echo "Save Confirmation for 'LVM'")
  fi

  showSaveConfirmationDialog cmdSteps methodParams

  case "$inputRetVal" in
    "NO")
      cleanKeyAndVals
    ;;
    "YES")
      executeLinuxCommands "$DIALOG_BACK_TITLE" "$pbTitle" linuxCmdKeys linuxCmdTitles linuxCmds
    ;;
  esac
}

#---------------------------------------------------------------------------------------
#      METHOD:                    setLinuxCmds
# DESCRIPTION: Set the linux commands to execute in order to create the partitions &
#              logical volumes.  They will be added to the "linuxCmd*" array variables
#              declared within the calling method.
#---------------------------------------------------------------------------------------
setLinuxCmds() {
  local key="zap"
  local keyFmt="cmdNum%02d"
  local tableRows=()
  local pvDevice=""
  local lastIdx=-1
  local deviceCols=()

  readarray -t tableRows < "$PARTITION_TABLE_FILE"
  local lastRow="${tableRows[$lastIdx]}"
  unset tableRows[$lastIdx]

  appendPartCmds "$keyFmt"

  local aryIdx=-1
  for row in "${tableRows[@]}"; do
    readarray -t deviceCols <<< "${row//$'\t'/$'\n'}"
    aryIdx=$(expr $aryIdx + 1)
    if [ "${deviceCols[-1]}" == "lvm"  ]; then
      pvDevice=$(echo "${deviceCols[0]}")
      break
    fi
  done

  appendCmdForLVs "$keyFmt" $aryIdx "$pvDevice"

  readarray -t deviceCols <<< "${lastRow//$'\t'/$'\n'}"
  local lvName="${deviceCols[1]}"
  aryIdx=$(expr ${#linuxCmdKeys[@]} + 1)
  key=$(printf "$keyFmt" $aryIdx)

  linuxCmdKeys+=("$key")
  linuxCmds["$key"]=$(printf "lvcreate -l %s %s -n %s" "100%FREE" "${varMap["VG-NAME"]}" "$lvName")
  linuxCmdTitles["$key"]=$(printf "Creating Logical Volume '%s' with all the remaining free space" "$lvName")

  key="zap"
  linuxCmdKeys=("$key" "${linuxCmdKeys[@]}")
  linuxCmdTitles["$key"]="Zapping Current Partition Layout/Scheme"
  linuxCmds["$key"]=$(printf "sgdisk -Z %s" "$BLOCK_DEVICE")
}

#---------------------------------------------------------------------------------------
#      METHOD:                       appendPartCmds
# DESCRIPTION: Add the commands to the "linuxCmd*" array variables declared within
#              the calling method
#  Required Params:
#      1) keyFmt - string format of key
#---------------------------------------------------------------------------------------
appendPartCmds() {
  local cols=()
  local partName=""
  local partSize=""
  local cmdNum=0
  local cmd=""
  local cmdTitle=""

  for row in "${tableRows[@]}"; do
    cmdNum=$(expr $cmdNum + 1)
    key=$(printf "$1" "$cmdNum")

    readarray -t cols <<< "${row//$'\t'/$'\n'}"

    partName="${cols[1]}"
    if [ "$partName" == "$LVM_PART_NAME" ]; then
      cmd=$(printf "sgdisk -n 0:0:0 -t 0:%s -c 0:\"%s\" %s" "${cols[3]}" "$partName" "$BLOCK_DEVICE")
      cmdTitle=$(printf "Creating Partition '%s' with all the remaining free space" "$partName")
    else
      partSize="${cols[2]}"
      cmd=$(printf "sgdisk -n 0:0:\"+%s\" -t 0:%s -c 0:\"%s\" %s" "$partSize" "${cols[3]}" "$partName" "$BLOCK_DEVICE")
      cmdTitle=$(printf "Creating Partition '%s' with size of '%s'" "$partName" "${partSize:1}")
    fi

    linuxCmdKeys+=("$key")
    linuxCmds["$key"]="$cmd"
    linuxCmdTitles["$key"]="$cmdTitle"

    if [ "$partName" == "$LVM_PART_NAME" ]; then
      break
    fi
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                       appendCmdForLVs
# DESCRIPTION: Add the commands to the "linuxCmd*" array variables declared within
#              the calling method
#  Required Params:
#      1)   keyFmt - string format of key
#      2) startIdx - array index of the first LV device
#      3) pvDevice - the name of the device for the Logical Volume
#---------------------------------------------------------------------------------------
appendCmdForLVs() {
  local startIdx=$2
  local cols=()
  local cmdNum=${#linuxCmdKeys[@]}
  local cmd=""
  local cmdTitle=""
  local pvDevice="$3"
  local vgName="${varMap["VG-NAME"]}"

  for row in "${tableRows[@]:${startIdx}}"; do
    cmdNum=$(expr $cmdNum + 1)
    key=$(printf "$1" "$cmdNum")

    readarray -t cols <<< "${row//$'\t'/$'\n'}"
    case "${cols[3]}" in
      "8E00")
        linuxCmdKeys+=("$key")
        linuxCmds["$key"]=$(printf "pvcreate %s" "$pvDevice")
        linuxCmdTitles["$key"]=$(printf "Creating the physical volume on the device '%s'" "$pvDevice")

        cmdNum=$(expr $cmdNum + 1)
        key=$(printf "$1" "$cmdNum")
        cmd=$(printf "vgcreate %s %s" "$vgName" "$pvDevice")
        cmdTitle=$(printf "Creating the volume group '%s' on the device '%s'" "$vgName" "$pvDevice")
      ;;
      "crypt")
        appendCmdsForLUKS "$1" "$pvDevice"
        local luksContainer=$(echo "/dev/mapper/$LUKS_CONTAINER_NAME")

        cmdNum=${#linuxCmdKeys[@]}
        key=$(printf "$1" "$cmdNum")
        linuxCmdKeys+=("$key")
        linuxCmds["$key"]=$(printf "pvcreate %s" "$luksContainer")
        linuxCmdTitles["$key"]="Creating the physical volume on top of the opened 'LUKS' container"

        cmdNum=$(expr $cmdNum + 1)
        key=$(printf "$1" "$cmdNum")
        cmd=$(printf "vgcreate %s %s" "$vgName" "$luksContainer")
        cmdTitle=$(printf "Creating the volume group '%s' for the opened 'LUKS' container" "$vgName")
      ;;
      "lvm"|"8200")
        local lvName="${cols[1]}"
        local lvSize="${cols[2]}"
        local hrSize=(${lvSize// /})
        cmd=$(printf "lvcreate -L %s %s -n %s" "$hrSize" "$vgName" "$lvName")
        cmdTitle=$(printf "Creating Logical Volume '%s' with size of '%s'" "$lvName" "$lvSize")
      ;;
    esac

    linuxCmdKeys+=("$key")
    linuxCmds["$key"]="$cmd"
    linuxCmdTitles["$key"]="$cmdTitle"
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                      appendCmdsForLUKS
# DESCRIPTION: Add the commands to create & open the LUKS container
#  Required Params:
#      1)   keyFmt - string format of key
#      2) pvDevice - the name of the device for the Logical Volume
#---------------------------------------------------------------------------------------
appendCmdsForLUKS() {
  local luksPswd="${varMap["LVM-on-LUKS"]}"
  local luksType="--type luks2"
  local pvDevice="$2"
  local luksCmdNum=${#linuxCmdKeys[@]}
  local cmdKey=$(printf "$1" "$luksCmdNum")
  local luksCmd=$(printf "echo \"%s\" | cryptsetup %s %s %s" "$luksPswd" "$luksType" \
        "--cipher aes-xts-plain64 --key-size 512 --hash sha512 --iter-time 5000 --use-random luksFormat" "$pvDevice")
  linuxCmdKeys+=("$cmdKey")
  linuxCmds["$cmdKey"]="$luksCmd"
  linuxCmdTitles["$cmdKey"]="Creating the LUKS encrypted container..."

  luksCmd=$(printf "echo \"%s\" | cryptsetup open %s %s %s" "$luksPswd" "$luksType" "$pvDevice" "$LUKS_CONTAINER_NAME")
  luksCmdNum=$(expr $luksCmdNum + 1)
  cmdKey=$(printf "$1" "$luksCmdNum")
  linuxCmdKeys+=("$cmdKey")
  linuxCmds["$cmdKey"]="$luksCmd"
  linuxCmdTitles["$cmdKey"]="Opening the LUKS encrypted container..."
}

#---------------------------------------------------------------------------------------
#  MAIN
#---------------------------------------------------------------------------------------
main() {
  while true; do
    if [ ${#BLOCK_DEVICE} -lt 1 ]; then
      initVars
    else
      local nextStep=$(getNextStep)
      manageLogicalVolumes "$nextStep"
      if [ ! ${varMap["PART-LAYOUT"]+_} ]; then
        break
      elif [ "$nextStep" == "step#05" ]; then
        break
      fi
    fi
  done

  local -A updVarMap=()
  for key in "${!varMap[@]}"; do
    updVarMap["$key"]=$(echo "${varMap[$key]}")
  done

  declare -p updVarMap > "${UPD_ASSOC_ARRAY_FILE}"
}

main "$@"
exit 0
