#!/bin/bash
#set -e

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# globals
#===============================================================================
CUR_DIR="${PWD}"
INC_FILE=$(echo "$CUR_DIR/inc/inst_inc.sh")

source "$INC_FILE"
source "$GRAPHICAL_PATH/network-config-inc.sh"

declare -A varMap
declare BLOCK_DEVICE=""
declare DIALOG_BACK_TITLE=""
declare ETHERNET_DEVICE_NAME=""
declare WIRELESS_DEVICE_NAME=""
declare SKIP_FLAG=0
source -- "$ASSOC_ARRAY_FILE"

#===============================================================================
# functions
#===============================================================================

#---------------------------------------------------------------------------------------
#      METHOD:                       initVars
# DESCRIPTION: Initialize the global variables.
#---------------------------------------------------------------------------------------
initVars() {
  DIALOG_BACK_TITLE="$1"
  ETHERNET_DEVICE_NAME="$2"
  WIRELESS_DEVICE_NAME="$3"
  if [ "$#" -gt 3 ]; then
    SKIP_FLAG=$(echo 'false' && return 1)
  else
    SKIP_FLAG=$(echo 'true' && return 0)
  fi

  BLOCK_DEVICE=$(echo "${varMap["BLOCK-DEVICE"]}")
}

#---------------------------------------------------------------------------------------
#      METHOD:              configureInternetConnection
# DESCRIPTION: Configure a connection to the internet when there wasn't one found.
#---------------------------------------------------------------------------------------
configureInternetConnection() {
  local connConfigOpts=("Configure Proxy")

  if [ ${#ETHERNET_DEVICE_NAME} -gt 0 ]; then
    connConfigOpts+=("Wired Automatic" "Wired Manual")
  else
    connConfigOpts+=("Wired Manual")
  fi

  if [ ${#WIRELESS_DEVICE_NAME} -gt 0 ]; then
    connConfigOpts+=("Wireless")
  fi

  while true; do
    showTypeOfConnectionConfigDlg "$DIALOG_BACK_TITLE" ${SKIP_FLAG}
    case "$mbSelVal" in
      "Configure Proxy")
        configProxyConn
        if [ ${#ibEnteredText} -gt 0 ]; then
          break
        fi
      ;;
      "Skip")
        break
      ;;
      "Wired Automatic")
        systemctl start dhcpcd@${ETHERNET_DEVICE_NAME}.service
        break
      ;;
      "Wired Manual")
        configConnManually
        if [ ${#ibEnteredText} -gt 0 ]; then
          break
        fi
      ;;
      "Wireless")
        wifi-menu ${WIRELESS_DEVICE_NAME}
        break
      ;;
    esac
  done
  varMap["CONNECTION-TYPE"]=$(echo "$mbSelVal")

}

#---------------------------------------------------------------------------------------
#      METHOD:                    configProxyConn
# DESCRIPTION: Configure a connection to the internet through a proxy
#---------------------------------------------------------------------------------------
configProxyConn() {
  showDlgInputBoxForProxy "$DIALOG_BACK_TITLE"

  if [ ${#ibEnteredText} -gt 0 ]; then
    export http_proxy=$ibEnteredText
    export https_proxy=$ibEnteredText
    export ftp_proxy=$ibEnteredText
    echo "proxy = $ibEnteredText" > ~/.curlrc

    ibEnteredText="Exit"
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                        validateProxy
# DESCRIPTION: Validate that the proxy entered is a valid URL
#      RETURN: 0 - true, 1 - false
#---------------------------------------------------------------------------------------
function validateProxy() {
  local regex='(https?|ftp|file)://[-A-Za-z0-9\+&@#/%?=~_|!:,.;]*[-A-Za-z0-9\+&@#/%=~_|]'

  if [[ "$ibEnteredText" =~ $regex ]]; then
    echo 'true' && return 0
  fi

  echo 'false' && return 1
}

#---------------------------------------------------------------------------------------
#      METHOD:                     configConnManually
# DESCRIPTION: Configure a connection to the internet manually
#---------------------------------------------------------------------------------------
configConnManually() {
  local editorCmd=${varMap["TEXT-EDITOR"]}
  local fieldNames=("IP Address" "Submask" "Gateway")
  local -A formVals=()
  local submittedVals=()
  local formField=""
  local errMsg=""
  local aryIdx=0
  local fldName=""

  while true; do
    for fldName in "${fieldNames[@]}"; do
      formVals["$fldName"]=""
    done

    showManualConfigForm "$DIALOG_BACK_TITLE"
    IFS=',' read -a submittedVals <<< "$ibEnteredText"

    case ${submittedVals[0]} in
      ${DIALOG_OK})
        ibEnteredText=$(tr "$FORM_FLD_SEP" '\n' <<< "${submittedVals[1]}")
        if [ ${#ibEnteredText} -gt 0 ]; then
          aryIdx=0
          while IFS=$'\n' read -r formField; do
            fldName="${fieldNames[$aryIdx]}"
            formVals["$fldName"]="$formField"
            aryIdx=$(expr $aryIdx + 1)
          done <<< "$ibEnteredText"
        fi
        errMsg=$(validateFormFields)

        if [ ${#errMsg} -gt 0 ]; then
          dispInvFormVals "$DIALOG_BACK_TITLE" "$errMsg"
        else
          break
        fi
      ;;
      ${DIALOG_CANCEL})
        ibEnteredText=""
        for fld in "${fieldNames[@]}"; do
          unset formVals["$fld"]
        done
        break
      ;;
    esac
  done

  if [ ${formVals["${fieldNames[0]}"]+_} ]; then
    systemctl stop dhcpcd@${ETHERNET_DEVICE_NAME}.service
    ip link set ${ETHERNET_DEVICE_NAME} up
    ip addr add ${formVals["${fieldNames[0]}"]}/${formVals["${fieldNames[1]}"]} dev ${ETHERNET_DEVICE_NAME}
    ip route add default via ${formVals["${fieldNames[2]}"]}
    $editorCmd /etc/resolv.conf

    ibEnteredText="Exit"
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     validateFormFields
# DESCRIPTION: Validate that the "IP Address", "Submask", & "Gateway" are NOT blank
#              and have valid ip values
#      RETURN: concatenated string of the form fields that are invalid
#---------------------------------------------------------------------------------------
function validateFormFields() {
  local errTextArray=("There are either missing or invalid values for the following:"
    "$DASH_LINE_WIDTH_80" " ")
  local errMsg=""
  local validFlag=$(echo 'false' && return 1)

  if [ "$DIALOG" == "yad" ]; then
    errTextArray[1]="$BORDER_WIDTH_600"
  fi

  for fld in "${fieldNames[@]}"; do
    formVal="${formVals["$fld"]}"
    validFlag=$(echo 'true' && return 0)
    if [ ${#formVal} -gt 0 ]; then
      validFlag=$(validateIP "$formVal")
      if ! ${validFlag}; then
        errMsg=$(printf "%s is invalid" "$formVal")
      fi
    else
      errMsg="Missing"
      validFlag=$(echo 'false' && return 1)
    fi
    if ! ${validFlag}; then
      if [ "$fld" == "IP Address" ]; then
        errMsg=$(printf "'%s':%5s \"%s\"" "$fld" " " "$errMsg")
      else
        errMsg=$(printf "%3s'%s':%5s \"%s\"" " " "$fld" " " "$errMsg")
      fi
      errTextArray+=("$errMsg")
    fi
  done

  if [ ${#errTextArray[@]} -gt 3 ]; then
    errMsg=$(getTextForDialog "${errTextArray[@]}")
  else
    errMsg=""
  fi

  echo "$errMsg"
}

#---------------------------------------------------------------------------------------
#  MAIN
#---------------------------------------------------------------------------------------
main() {
  initVars "$@"
  configureInternetConnection

  local -A updVarMap=()
  for key in "${!varMap[@]}"; do
    updVarMap["$key"]=$(echo "${varMap[$key]}")
  done

  declare -p updVarMap > "${UPD_ASSOC_ARRAY_FILE}"
}

main "$@"
exit 0