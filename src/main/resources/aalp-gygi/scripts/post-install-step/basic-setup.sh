#!/bin/bash

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# globals
#===============================================================================
CUR_DIR="${PWD}"
INC_FILE=$(echo "$CUR_DIR/inc/post-inst-inc.sh")

source "$INC_FILE"
source "$GRAPHICAL_PATH/post-install-inc.sh"
source "$GRAPHICAL_PATH/pswd-funcs-inc.sh"
source "$GRAPHICAL_PATH/basic-setup-inc.sh"
source -- "$POST_ASSOC_ARRAY_FILE"
source -- "$ASSOC_ARRAY_FILE"

declare BASIC_SETUP_CHECKLIST=(0)
declare CHECKLIST_KEY="BASIC_SETUP_CHECKLIST"
declare CAT_MENU_OPTS=()

declare INCLUDE_PI_FILES=("cat-basic-setup.sh" "aur-signed-repos.sh" "aur-unsigned-repos.sh")
for fileName in "${INCLUDE_PI_FILES[@]}"; do
  source "$CUR_DIR/inc/post-inst/$fileName"
done

#===============================================================================
# functions/methods
#===============================================================================

#---------------------------------------------------------------------------------------
#      METHOD:                       initVars
# DESCRIPTION: Initialize the global variables.
#---------------------------------------------------------------------------------------
initVars() {
  readarray -t SEL_CHKLIST_OPT <<< "${1//$FORM_FLD_SEP/$'\n'}"

  if [ ${POST_ASSOC_ARRAY["$CHECKLIST_KEY"]+_} ]; then
    readarray -t BASIC_SETUP_CHECKLIST <<< "${POST_ASSOC_ARRAY["$CHECKLIST_KEY"]//$FORM_FLD_SEP/$'\n'}"
  else
    for menuOpt in "${MENU_OPTS_ARRAY[@]}"; do
      BASIC_SETUP_CHECKLIST+=(0)
    done
  fi

  local swapKey="SWAP-TYPE"
  if [ ${varMap["$swapKey"]+_} ]; then
    local idx=-1
    if [ "${varMap["$swapKey"]}" == "${ZRAM_PCKG[0]}" ]; then
      updateSoftwareTaskChecklist "BASIC_SETUP_CHECKLIST" 2 "${MENU_OPTS_ARRAY[${idx}]}"
    else
      updateSoftwareTaskChecklist "BASIC_SETUP_CHECKLIST" -1 "${MENU_OPTS_ARRAY[${idx}]}"
      unset MENU_OPTS_ARRAY[${idx}]
    fi
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                     execBasicSetupTask
# DESCRIPTION: Execute the task that was selected
#  Required Params:
#    1)  optSel - the option that was selected
#    2) optDesc - the description/name of the option that was selected
#---------------------------------------------------------------------------------------
execBasicSetupTask() {
  local optSel="$1"
  local optDesc="$2"
  local doneFlag=$(isSoftwareTaskDone "${piAssocArray["chklArray"]}" "$optSel")

  if ! ${doneFlag}; then
    case "$optDesc" in
      "Add Unofficial User Repositories")
        addUURepos "$optSel" "$optDesc"
      ;;
      "Check MultiLib")
        checkMultiLib "$optSel"
      ;;
      "Configure SUDO")
        configureSUDO "$optSel" "$optDesc"
      ;;
      "Choose AUR Helper")
        chooseAURHelper "$optSel" "$optDesc"
      ;;
      "Create User")
        createUser "$optSel" "$optDesc"
      ;;
      "Install Font Config")
        installFontConfig "$optSel" "$optDesc"
      ;;
      "Install CUPS")
        installCUPS "$optSel" "$optDesc"
      ;;
      "Install NFS")
        installNFS "$optSel" "$optDesc"
      ;;
      "Install Samba")
        installSamba "$optSel" "$optDesc"
      ;;
      "Install SSH")
        installSSH "$optSel" "$optDesc"
      ;;
      "Install TLP")
        installTLP "$optSel" "$optDesc"
      ;;
      "Install ZRam Swap")
        installZRamSwap "$optSel" "$optDesc"
      ;;
      "Set Default Shell")
        setDefaultShell "$optSel" "$optDesc"
      ;;
      "Set Default Text Editor")
        setDefaultTextEditor "$optSel" "$optDesc"
      ;;
      "Update System")
        updateSystem "$optSel"
      ;;
      *)
        clog_error "No case statement defined for tabOpt: [$optSel]"
        exit 1
      ;;
    esac
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                        checkMultiLib
# DESCRIPTION: Allows installing 32-bit software on a 64-bit system
#    1)  optSel - the value of the option that was selected for this task
#---------------------------------------------------------------------------------------
checkMultiLib() {
  local pacmanConfFile="${ROOT_MOUNTPOINT}/etc/pacman.conf"
  local lineNum=$(awk '/#\[multilib\]/{ print NR; exit }' "$pacmanConfFile")

  if [ ! -f "${pacmanConfFile}.orig" ]; then
    cp "$pacmanConfFile" "${pacmanConfFile}.orig"
  fi

  if [ ${#lineNum} -gt 0 ]; then
    sed -i "${lineNum}s/\#//g" "$pacmanConfFile"

    lineNum=$(expr ${lineNum} + 1)
    sed -i "${lineNum}s/\#//g" "$pacmanConfFile"
  fi

  updateSoftwareTaskChecklist "${piAssocArray["chklArray"]}" 1 "$1"
}

#---------------------------------------------------------------------------------------
#      METHOD:                         addUURepos
# DESCRIPTION: Add the selected unofficial user repositories to the
#              "/etc/pacman.conf" file.
#  Required Params:
#    1)  optSel - the value of the option that was selected for this task
#    2) optDesc - the description/name of this task
#---------------------------------------------------------------------------------------
addUURepos() {
  local dialogTitle=$(printf "%s - %s" "$1" "$2")
  local flagVal=0
  local helpText=$(getTextForAddUUR)
  local warnMsgLines=("The official Arch Linux Developers and the Trusted Users do not"
                      "perform tests of any sort to verify the contents of these"
                      "repositories. You must decide whether to trust their maintainers"
                      "and you take full responsibility for any consequences of using"
                      "any unofficial repository.")
  local warnMsg=$(printf " %s" "${warnMsgLines[@]}")

  showWarningDialog "Add Unofficial User Repositories!" "${warnMsg:1}"
  cleanDialogFiles

  selectUnoffUserRepos "$dialogTitle" "$helpText"
  cleanDialogFiles

  if [ ${#TAB_SEL_VALS[@]} -gt 0 ]; then
    local tabKeys=($(printf "%s\n" "${!TAB_SEL_VALS[@]}" | sort ))
    local urlAssocArrayRef=""
    local lines=()
    local tabRows=()
    local tabCols=()
    local sep="|"
    local cmds=()
    local aurRepoName=""
    local serverURL=""
    local cnRepoFlag=$(echo 'false' && return 1)

    for tabKey in "${tabKeys[@]}"; do
      case "$tabKey" in
        "tab#01"|"Signed")
          urlAssocArrayRef="SIGNED_REPO_URLS"
        ;;
        *)
          urlAssocArrayRef="UNSIGNED_REPO_URLS"
        ;;
      esac
      readarray -t tabRows <<< "${TAB_SEL_VALS["$tabKey"]}"
      for row in "${tabRows[@]}"; do
        case "$tabKey" in
          "Signed"|"Unsigned")
            aurRepoName="$row"
          ;;
          *)
            readarray -t tabCols <<< "${row//$sep/$'\n'}"
            aurRepoName="${tabCols[1]}"
          ;;
        esac
        case "$urlAssocArrayRef" in
          "SIGNED_REPO_URLS")
            serverURL="${SIGNED_REPO_URLS["$aurRepoName"]}"
          ;;
          *)
            serverURL="${UNSIGNED_REPO_URLS["$aurRepoName"]}"
          ;;
        esac

        lines+=("" "[$aurRepoName]" "SigLevel = TrustedOnly" "Server = $serverURL")
        if [ "$aurRepoName" == "archlinuxcn"  ]; then
          cnRepoFlag=$(echo 'true' && return 0)
        elif [ ${SIGNED_REPO_KEY_IDS["$aurRepoName"]+_} ]; then
          local keyID="${SIGNED_REPO_KEY_IDS["$aurRepoName"]}"
          local cmd=$(printf "Locally signing the associated key for '%s'%spacman-key -r %s && pacman-key --finger %s && pacman-key --lsign-key %s" "$aurRepoName" "$FORM_FLD_SEP" "$keyID" "$keyID" "$keyID")
          cmds+=("$cmd")
        fi
      done
    done

    local concatStr=$(printf "\n%s" "${lines[@]}")
    echo -e "${concatStr:1}" > "${DATA_DIR}/unofficialUserRepos.txt"

    local pacmanConfFile="${ROOT_MOUNTPOINT}/etc/pacman.conf"
    cat "${DATA_DIR}/unofficialUserRepos.txt" >> "$pacmanConfFile"

    if [ ${#cmds[@]} -gt 0 ]; then
      concatStr=$(printf "\n%s" "${cmds[@]}")
      echo -e "${concatStr:1}" > "$POST_INST_CMD_FILE"
    fi

    if ${cnRepoFlag}; then
      echo "archlinuxcn-keyring" > "$SEL_AUR_PCKGS"
      showProgBarAndTail
      rm -rf "$SEL_AUR_PCKGS"
    fi
    if [ -f "$POST_INST_CMD_FILE" ]; then
      showProgBarAndTail
      rm -rf "$POST_INST_CMD_FILE"
    fi

    flagVal=1
  else
    flagVal=-1
  fi

  updateSoftwareTaskChecklist "${piAssocArray["chklArray"]}" ${flagVal} "$1"
}

#---------------------------------------------------------------------------------------
#      METHOD:                        updateSystem
# DESCRIPTION: Update the system as well as initialize and manage the keyring.
#  Required Params:
#    1)  optSel - the value of the option that was selected for this task
#---------------------------------------------------------------------------------------
updateSystem() {
  local cmds=(
    "Initializing the keyring${FORM_FLD_SEP}pacman-key --init"
    "Verifying the master keys${FORM_FLD_SEP}pacman-key --populate archlinux"
    "System Upgrade${FORM_FLD_SEP}pacman -Syyu --noconfirm"
  )
  local concatStr=$(printf "\n%s" "${cmds[@]}")

  echo -e "${concatStr:1}" > "$POST_INST_CMD_FILE"

  showProgBarAndTail

  rm -rf "$POST_INST_CMD_FILE"

  updateSoftwareTaskChecklist "${piAssocArray["chklArray"]}" 1 "$1"
}

#---------------------------------------------------------------------------------------
#      METHOD:                        configureSUDO
# DESCRIPTION: Configure the SUDOER file to temporarily enable root privileges.
#  Required Params:
#    1)  optSel - the value of the option that was selected for this task
#    2) optDesc - the description/name of this task
#---------------------------------------------------------------------------------------
configureSUDO() {
  local sudoerFile="${ROOT_MOUNTPOINT}/etc/sudoers.orig"
  local sudoPswdOpts=("Require" "None")
  local -A pswdOptDescs=(["Require"]=" - Allow members of group wheel to execute any command"
    ["None"]=" - Same thing, but without a password (not secure)")
  local -A funcParams=(["helpText"]=$(getTextForSUDO) ["menuOptsRef"]="sudoPswdOpts" ["optDescsRef"]="pswdOptDescs")
  local windowTitle=$(printf "%s - %s" "${SEL_CHKLIST_OPT[0]}" "${SEL_CHKLIST_OPT[1]}")
  local dialogTitle=$(printf "%s - %s" "$1" "$2")

  funcParams["defaultChoice"]="${sudoPswdOpts[0]}"

  if [ "$DIALOG" == "yad" ]; then
    funcParams["dialogTitle"]="$windowTitle"
    funcParams["dialogTextHdr"]="$dialogTitle"
    funcParams["dialogText"]="Choose the password option when executing the \"sudo\" command:"
  else
    funcParams["dialogBackTitle"]="$windowTitle"
    funcParams["dialogText"]="Choose the option of whether or not the ROOT password is required when executing the \"sudo\" command:"
    funcParams["dialogTitle"]="$dialogTitle"
  fi

  selectOptionForSUDO "funcParams"
  cleanDialogFiles

  chmod 640 "$sudoerFile"
  if [ "$mbSelVal" == "${sudoPswdOpts[0]}" ]; then
    sed -i '/%wheel ALL=(ALL) ALL/s/^#[[:space:]]*//' ${sudoerFile}
  else
    sed -i '/%wheel ALL=(ALL) NOPASSWD: ALL/s/^#[[:space:]]*//' ${sudoerFile}
  fi
  chmod 440 "$sudoerFile"

  updateSoftwareTaskChecklist "${piAssocArray["chklArray"]}" 1 "$1"
}

#---------------------------------------------------------------------------------------
#      METHOD:                      installFontConfig
# DESCRIPTION: Install the AUR package "fontconfig"
#  Required Params:
#    1)  optSel - the value of the option that was selected for this task
#    2) optDesc - the description/name of this task
#---------------------------------------------------------------------------------------
installFontConfig() {
  local aurPckgs=("fontconfig" "noto-fonts")
  local -A aurPckgDescs=(["fontconfig"]=" - A library for configuring and customizing font access"
                         ["noto-fonts"]=" - Google Noto TTF fonts")
  local flagVal=0

  showInstallFontConfigStep "$1" "$2"
  cleanDialogFiles

  if ${yesNoFlag}; then
    local concatStr=$(printf " %s" "${aurPckgs[@]}")
    echo "${concatStr:1}" > "$SEL_AUR_PCKGS"

    showProgBarAndTail
    rm -rf "$SEL_AUR_PCKGS"
    makeFontsBeautiful
    flagVal=1
  else
    flagVal=-1
  fi

  updateSoftwareTaskChecklist "${piAssocArray["chklArray"]}" ${flagVal} "$1"
}

#---------------------------------------------------------------------------------------
#      METHOD:                     makeFontsBeautiful
# DESCRIPTION: Improve the look and feel of the Arch fonts.
#---------------------------------------------------------------------------------------
makeFontsBeautiful() {
  local srcDir="${ROOT_MOUNTPOINT}/etc/fonts/conf.avail"
  local destDir="${ROOT_MOUNTPOINT}/etc/fonts/conf.d"
  local freetypeScript="${ROOT_MOUNTPOINT}/etc/profile.d/freetype2.sh"
  local confFiles=("70-no-bitmaps.conf" "10-sub-pixel-rgb.conf" "11-lcdfilter-default.conf")
  local lastLineNum=$(wc -l < "$freetypeScript")

  for confFile in "${confFiles[@]}"; do
    local softLinkCmd=$(printf "ln -sf %s/%s %s" "$srcDir" "$confFile" "$destDir")
    ${softLinkCmd}
  done

  sed -i "${lineNum}s/\#//g" "$freetypeScript"

  srcDir="${DATA_DIR}/Fontconfig"
  destDir="${ROOT_MOUNTPOINT}/etc/fonts"
  cp -rf "${srcDir}/local.conf" "${destDir}/."

  destDir="${ROOT_MOUNTPOINT}/home/${POST_ASSOC_ARRAY["UserName"]}/.config"
  cp -rf "${srcDir}/fonts.conf" "${destDir}/."

  mkdir -p "$destDir"
}

#---------------------------------------------------------------------------------------
#      METHOD:                         createUser
# DESCRIPTION: Create the new user
#  Required Params:
#    1)  optSel - the value of the option that was selected for this task
#    2) optDesc - the description/name of this task
#---------------------------------------------------------------------------------------
createUser() {
  local urlRef="https://wiki.archlinux.org/index.php/Users_and_groups"
  local dlgWindowTitle=$(echo "$POST_INST_DIALOG_BACK_TITLE")
  local dialogTitle=$(printf "%s - %s" "$1" "$2")
  local dialogText="Enter the user name & password:"

  if [ "$DIALOG" == "yad" ]; then
    dlgWindowTitle=$(echo "$dialogTitle")
    dialogTitle="Create New User:"
  fi

  getPasswordsFromFormDialog "$dlgWindowTitle" "$dialogTitle" "$urlRef" "$dialogText"
  cleanDialogFiles

  if [ ${#pswdVals[@]} -gt 0 ]; then
    if [ ${#pswdVals[@]} -gt 3 ]; then
      pswdVals=(${pswdVals[@]:1})
    fi

    addUser "$1" "pswdVals"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                           addUser
# DESCRIPTION: Add a user and set the password
#  Required Params:
#    1)   optSel - the value of the option that was selected for this task
#    2) userVals - the reference name of the array containing the username & password
#---------------------------------------------------------------------------------------
addUser() {
  local -n userVals="$2"
  local pckgName="xdg-user-dirs"
  local cmds=(
    "Adding the user${FORM_FLD_SEP}useradd -mg users -G wheel -s /bin/bash ${userVals[0]}"
    "Setting the user's password${FORM_FLD_SEP}echo ${userVals[0]}:${userVals[1]} | chpasswd"
    "Adding default bash configuration file${FORM_FLD_SEP}cp /etc/skel/.bashrc /home/${userVals[0]}/."
    "Installing '$pckgName' to manage the user directories${FORM_FLD_SEP}sudo -u $TRIZEN_USER trizen -S --skipinteg --noconfirm $pckgName"
  )
  local concatStr=$(printf "\n%s" "${cmds[@]}")

  echo -e "${concatStr:1}" > "$POST_INST_CMD_FILE"

  showProgBarAndTail

  rm -rf "$POST_INST_CMD_FILE"

  POST_ASSOC_ARRAY["UserName"]="${userVals[0]}"

  updateSoftwareTaskChecklist "${piAssocArray["chklArray"]}" 1 "$1"
}

#---------------------------------------------------------------------------------------
#      METHOD:                       chooseAURHelper
# DESCRIPTION: Choose the AUR helper to install
#  Required Params:
#    1)  optSel - the option that was selected
#    2) optDesc - the description/name of the option that was selected
#---------------------------------------------------------------------------------------
chooseAURHelper() {
  local -A funcParams=(["helpText"]=$(getTextForAURHelper) ["menuOptsRef"]="AUR_HELPERS" ["optDescsRef"]="AUR_PCKG_DESCS")
  local warnMsgLines=("AUR helpers are not supported by Arch Linux. You should become"
                      "familiar with the manual build process in order to be prepared to troubleshoot problems.")
  local warnMsg=$(printf " %s" "${warnMsgLines[@]}")
  local windowTitle=$(printf "%s - %s" "${SEL_CHKLIST_OPT[0]}" "${SEL_CHKLIST_OPT[1]}")
  local dialogTitle=$(printf "%s - %s" "$1" "$2")

  CAT_MENU_OPTS=()
  readarray -t CAT_MENU_OPTS <<< "${SUB_CAT_ASSOC_ARRAY["$2"]//$FORM_FLD_SEP/$'\n'}"

  showWarningDialog "Using an 'AUR Helper' application!" "${warnMsg:1}"
  cleanDialogFiles

  for cat in "${CAT_MENU_OPTS[@]}"; do
    case "$cat" in
      "4.1.7.1")
        funcParams["$cat"]="CLI_AUR_HELPERS"
      ;;
      *)
        funcParams["$cat"]="GUI_AUR_HELPERS"
      ;;
    esac
  done

  if [ "$DIALOG" == "yad" ]; then
    funcParams["windowTitle"]="$windowTitle"
  else
    funcParams["dialogBackTitle"]="$windowTitle"
    funcParams["dialogTitle"]="$dialogTitle"
  fi

  selectAURHelper "funcParams"
  cleanDialogFiles
  installAURHelper "$1"
}

#---------------------------------------------------------------------------------------
#      METHOD:                      installAURHelper
# DESCRIPTION: Install the AUR helper that was selected.
#  Required Params:
#    1)  optSel - the option that was selected
#---------------------------------------------------------------------------------------
installAURHelper() {
  local flagVal=0
  local tabCols=()
  local aurPckgName=""

  if [ ${#TAB_SEL_VALS[@]} -gt 0 ]; then
    local tabKeys=($(printf "%s\n" "${!TAB_SEL_VALS[@]}" | sort ))
    local sep="|"
    for tabKey in "${tabKeys[@]}"; do
      case "$tabKey" in
        "AUR_HELPER_TYPE")
          POST_ASSOC_ARRAY["$tabKey"]="${TAB_SEL_VALS["$tabKey"]}"
        ;;
        "Command Line"|"Graphical")
          aurPckgName="${AUR_PCKGS["${TAB_SEL_VALS["$tabKey"]}"]}"
          POST_ASSOC_ARRAY["AUR_HELPER"]="$aurPckgName"
        ;;
        "tab#01")
          readarray -t tabCols <<< "${TAB_SEL_VALS["$tabKey"]//$sep/$'\n'}"
          POST_ASSOC_ARRAY["AUR_HELPER_TYPE"]="${tabCols[2]}"
        ;;
        *)
          readarray -t tabCols <<< "${TAB_SEL_VALS["$tabKey"]//$sep/$'\n'}"
          aurPckgName="${AUR_PCKGS["${tabCols[1]}"]}"
          POST_ASSOC_ARRAY["AUR_HELPER"]="$aurPckgName"
        ;;
      esac
    done

    if [ "${POST_ASSOC_ARRAY["AUR_HELPER"]}" != "trizen" ]; then
      echo "${POST_ASSOC_ARRAY["AUR_HELPER"]}" > "$SEL_AUR_PCKGS"
      showProgBarAndTail
      rm -rf "$SEL_AUR_PCKGS"
    fi
    flagVal=1
  else
    flagVal=-1
  fi

  updateSoftwareTaskChecklist "${piAssocArray["chklArray"]}" ${flagVal} "$1"
}

#---------------------------------------------------------------------------------------
#      METHOD:                       setDefaultShell
# DESCRIPTION:  Install and configure the default shell selected.
#  Required Params:
#    1)  optSel - the value of the option that was selected for this task
#    2) optDesc - the description/name of this task
#---------------------------------------------------------------------------------------
setDefaultShell() {
  local -A funcParams=(["helpText"]=$(getTextForCmdLineShells))
  local windowTitle=$(printf "%s - %s" "${SEL_CHKLIST_OPT[0]}" "${SEL_CHKLIST_OPT[1]}")
  local dialogTitle=$(printf "%s - %s" "$1" "$2")

  SHELL_TYPES=()
  readarray -t SHELL_TYPES <<< "${SUB_CAT_ASSOC_ARRAY["$2"]//$FORM_FLD_SEP/$'\n'}"

  for cat in "${SHELL_TYPES[@]}"; do
    case "$cat" in
      "${SHELL_TYPES[0]}")
        funcParams["$cat"]="POSIX_COMPLIANT_SHELLS"
      ;;
      *)
        funcParams["$cat"]="ALTERNATIVE_SHELLS"
      ;;
    esac
  done

  if [ "$DIALOG" == "yad" ]; then
    funcParams["windowTitle"]="$windowTitle"
    funcParams["dialogTitle"]="$dialogTitle"
    funcParams["height"]=${YAD_MAX_HEIGHT}
  else
    funcParams["dialogBackTitle"]="$windowTitle"
    funcParams["dialogTitle"]="$dialogTitle"
  fi

  selectDefaultShell "funcParams"
  cleanDialogFiles
  installDefShell "$1"
}

#---------------------------------------------------------------------------------------
#      METHOD:                       installDefShell
# DESCRIPTION: Install and configure the shell that was selected.
#  Required Params:
#    1)  optSel - the option that was selected
#---------------------------------------------------------------------------------------
installDefShell() {
  local flagVal=0
  local tabCols=()
  local -A shellBins=(["Dash"]="bin/dash" ["MirBSD™ Korn Shell"]="/usr/bin/mksh"
                      ["C shell(tcsh)"]="bin/tcsh" ["Fish"]="/usr/bin/fish"
                      ["Oh"]="/usr/bin/oh"["PowerShell"]="/usr/bin/pwsh")
  local setCmd=""
  if [ ${#TAB_SEL_VALS[@]} -gt 0 ]; then
    local tabKeys=($(printf "%s\n" "${!TAB_SEL_VALS[@]}" | sort ))
    local sep="|"
    for tabKey in "${tabKeys[@]}"; do
      case "$tabKey" in
        "SHELL_NAME"|"SHELL_TYPE")
          POST_ASSOC_ARRAY["$tabKey"]="${TAB_SEL_VALS["$tabKey"]}"
        ;;
        "tab#01")
          readarray -t tabCols <<< "${TAB_SEL_VALS["$tabKey"]//$sep/$'\n'}"
          POST_ASSOC_ARRAY["SHELL_TYPE"]="${tabCols[2]}"
        ;;
        *)
          readarray -t tabCols <<< "${TAB_SEL_VALS["$tabKey"]//$sep/$'\n'}"
          POST_ASSOC_ARRAY["SHELL_NAME"]="${tabCols[1]}"
        ;;
      esac
    done

    local shellName="${POST_ASSOC_ARRAY["SHELL_NAME"]}"
    local cmdShellBin="${shellBins["$shellName"]}"
    local userName="${POST_ASSOC_ARRAY["UserName"]}"

    echo "${AUR_PCKGS["$shellName"]}" > "$SEL_AUR_PCKGS"
    showProgBarAndTail
    rm -rf "$SEL_AUR_PCKGS"
    case "$shellName" in
      "Oh")
        echo "$cmdShellBin" >> "${ROOT_MOUNTPOINT}/etc/shells"
        setCmd=$(printf "sudo -u %s chsh -s %s" "$userName" "$cmdShellBin")
      ;;
      "Zsh")
        local curlCmd="sh -c \"$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)\""
        setCmd=$(printf "sudo -u %s %s" "$userName" "$curlCmd")
      ;;
      *)
        if [ ${shellBins["$shellName"]+_} ]; then
          setCmd=$(printf "sudo -u %s chsh -s %s" "$userName" "$cmdShellBin")
        fi
      ;;
    esac

    if [ ${#setCmd} -gt 0 ]; then
      changeRoot "${setCmd}" &>> "$INSTALLER_LOG"
    fi

    flagVal=1
  else
    flagVal=-1
  fi

  updateSoftwareTaskChecklist "${piAssocArray["chklArray"]}" ${flagVal} "$1"
}

#---------------------------------------------------------------------------------------
#      METHOD:                    setDefaultTextEditor
# DESCRIPTION: Set the command line text editor used in modifying configuration
#              options and files.
#  Required Params:
#    1)  optSel - the value of the option that was selected for this task
#    2) optDesc - the description/name of this task
#---------------------------------------------------------------------------------------
setDefaultTextEditor() {
  local flagVal=0
  local -A funcParams=(["helpText"]=$(getTextForDefaultEditor)
                      ["menuOptsRef"]="DEFAULT_EDITORS" ["optDescsRef"]="AUR_PCKG_DESCS")
  local windowTitle=$(printf "%s - %s" "${SEL_CHKLIST_OPT[0]}" "${SEL_CHKLIST_OPT[1]}")
  local dialogTitle=$(printf "%s - %s" "$1" "$2")

  if [ "$DIALOG" == "yad" ]; then
    funcParams["dialogTitle"]="$windowTitle"
    funcParams["dialogTextHdr"]="$dialogTitle"
  else
    funcParams["dialogBackTitle"]="$windowTitle"
    funcParams["dialogTitle"]="$dialogTitle"
  fi

  funcParams["defaultChoice"]="${DEFAULT_EDITORS[-2]}"
  funcParams["dialogText"]="Choose the default command-line text editor:"

  selectDefTextEditor "funcParams"
  cleanDialogFiles

  if [ ${#mbSelVal} -gt 0 ]; then
    POST_ASSOC_ARRAY["TEXT-EDITOR"]=$(echo "$mbSelVal")
    echo "${POST_ASSOC_ARRAY["TEXT-EDITOR"]}" > "$SEL_AUR_PCKGS"
    showProgBarAndTail
    rm -rf "$SEL_AUR_PCKGS"

    local confFile=""
    local shellName="${POST_ASSOC_ARRAY["SHELL_NAME"]}"
    case "$shellName" in
      "Bash")
        confFile="${ROOT_MOUNTPOINT}/home/${POST_ASSOC_ARRAY["UserName"]}/.bashrc"
      ;;
      "Zsh")
        confFile="${ROOT_MOUNTPOINT}/home/${POST_ASSOC_ARRAY["UserName"]}/.zshrc"
      ;;
    esac
    if [ ${#confFile} -gt 0 ]; then
      echo "export EDITOR='${POST_ASSOC_ARRAY["TEXT-EDITOR"]}'" >> "$confFile"
      echo "export VISUAL='${POST_ASSOC_ARRAY["TEXT-EDITOR"]}'" >> "$confFile"
    fi

    flagVal=1
  else
    flagVal=-1
  fi

  updateSoftwareTaskChecklist "${piAssocArray["chklArray"]}" ${flagVal} "$1"
}

#---------------------------------------------------------------------------------------
#      METHOD:                         installCUPS
# DESCRIPTION: Install the group of AUR packages for CUPS
#              (aka Common Unix Printing System)
#  Required Params:
#    1)  optSel - the value of the option that was selected for this task
#    2) optDesc - the description/name of this task
#---------------------------------------------------------------------------------------
installCUPS() {
  local flagVal=0
  local helpText=$(getTextForCUPS)

  showInstConfDialog "$1" "$2" "$helpText" "CUPS_PCKGS" "cups.png"

  if ${yesNoFlag}; then
    local concatStr=$(printf " %s" "${CUPS_PCKGS[@]}")
    echo -e "${concatStr:1}" > "$SEL_AUR_PCKGS"
    concatStr=$(printf "Enabling the CUPS service${FORM_FLD_SEP}systemctl enable org.cups.cupsd.service")
    echo -e "$concatStr" > "$POST_INST_CMD_FILE"
    showProgBarAndTail
    rm -rf "$SEL_AUR_PCKGS"
    rm -rf "$POST_INST_CMD_FILE"
    flagVal=1
  else
    flagVal=-1
  fi

  updateSoftwareTaskChecklist "${piAssocArray["chklArray"]}" ${flagVal} "$1"
}

#---------------------------------------------------------------------------------------
#      METHOD:                     showInstConfDialog
# DESCRIPTION: Show a confirmation dialog to get confirmation to install the AUR packages
#              being displayed.
#  Required Params:
#    1)      optSel - The value of the option that was selected for this task
#    2)     optDesc - The description/name of this task
#    3)    helpText - Text to display on the help/more dialog
#    4) menuOptsRef - Name of the array containing the names of the packages
#    5)       image - The image to display above the radio list
#---------------------------------------------------------------------------------------
showInstConfDialog() {
  local -A funcParams=(["helpText"]="$3"
                      ["menuOptsRef"]="$4" ["optDescsRef"]="AUR_PCKG_DESCS")
  local windowTitle=$(printf "%s - %s" "${SEL_CHKLIST_OPT[0]}" "${SEL_CHKLIST_OPT[1]}")
  local dialogTitle=$(printf "%s - %s" "$1" "$2")

  if [ "$DIALOG" == "yad" ]; then
    funcParams["image"]="$YAD_GRAPHICAL_PATH/images/post-inst/$5"
    funcParams["dialogTitle"]="$windowTitle"
    funcParams["helpDlgTitle"]="$dialogTitle"
    if [ "$4" == "ZRAM_PCKG" ]; then
      funcParams["height"]=400
    fi
  else
    funcParams["dialogBackTitle"]="$windowTitle"
    funcParams["dialogTitle"]="$dialogTitle"
    local txtArray=()
    local dialogText=""
    case "$2" in
      "Install CUPS")
        funcParams["dialogText"]=$(getHdrTxtForCUPS)
      ;;
      "Install NFS")
        txtArray=("The Network File System (NFS) is a way of mounting Linux discs/directories over"
                  "a network. An NFS server can export one or more directories that can then be"
                  "mounted on a remote Linux machine.")
      ;;
      "Install Samba")
        txtArray=("Samba is the standard Windows interoperability suite of programs for Linux and"
                  "Unix.  This is a software package that gives network administrators flexibility"
                  "and freedom in terms of setup, configuration, and choice of systems and equipment.")
      ;;
      "Install TLP")
        txtArray=("TLP is a free open source, feature-rich and command line tool for advanced power"
                  "management, which helps to optimize battery life in laptops powered by Linux.  It"
                  "runs on every laptop brand, and comes with a default configuration already tuned"
                  "to effectively and reliably maintain battery life.")
      ;;
      "Install ZRam Swap")
        txtArray=("When used for swap, ZRam (like ZSwap also) allows Linux to make more efficient use"
                  "of RAM, since the operating system can then hold more pages of memory in the compressed"
                  "swap than if the same amount of RAM had been used as application memory or disk"
                  "cache. This is particularly effective on machines that do not have much memory.")
      ;;
    esac

    if [ ${#txtArray[@]} ]; then
      dialogText=$(printf " %s" "${txtArray[@]}")
      funcParams["dialogText"]="${dialogText:1}"
    fi
  fi

  showConfirmationDialog "funcParams"
  cleanDialogFiles
}

#---------------------------------------------------------------------------------------
#      METHOD:                        installNFS
# DESCRIPTION: Install the group of AUR packages for Network File System (NFS)
#---------------------------------------------------------------------------------------
installNFS() {
  local flagVal=0
  local helpText=$(getTextForNFS)

  showInstConfDialog "$1" "$2" "$helpText" "NFS_PCKG" "nfs.png"

  if ${yesNoFlag}; then
    local concatStr=$(printf " %s" "${NFS_PCKG[@]}")
    echo -e "${concatStr:1}" > "$SEL_AUR_PCKGS"

    local cmds=(
      "Enabling the NFS server${FORM_FLD_SEP}systemctl enable nfs-server"
      "Enabling the rpcbind service${FORM_FLD_SEP}systemctl enable rpcbind.service"
      "Enabling the NFS client service${FORM_FLD_SEP}systemctl enable nfs-client.target"
      "Enabling the remote-fs service${FORM_FLD_SEP}systemctl enable remote-fs.target"
    )
    concatStr=$(printf "\n%s" "${cmds[@]}")
    echo -e "${concatStr:1}" > "$POST_INST_CMD_FILE"

    showProgBarAndTail
    rm -rf "$SEL_AUR_PCKGS"
    rm -rf "$POST_INST_CMD_FILE"
    flagVal=1
  else
    flagVal=-1
  fi

  updateSoftwareTaskChecklist "${piAssocArray["chklArray"]}" ${flagVal} "$1"
}

#---------------------------------------------------------------------------------------
#      METHOD:                        installSamba
# DESCRIPTION: Install the group of AUR packages to be able to do Network File Sharing
#  Required Params:
#    1)      optSel - The value of the option that was selected for this task
#    2)     optDesc - The description/name of this task
#---------------------------------------------------------------------------------------
installSamba() {
  local flagVal=2
  local helpText=$(getTextForSamba)
  local dlgFormVals=()

  while [ ${flagVal} -gt 1 ]; do
    showInstConfDialog "$1" "$2" "$helpText" "SAMBA_PCKGS" "samba.png"

    if ${yesNoFlag}; then
      createSambaUser "$1" "$2" "dlgFormVals"

      if [ ${#dlgFormVals[@]} -gt 0 ]; then
        local concatStr=$(printf " %s" "${SAMBA_PCKGS[@]}")
        echo -e "${concatStr:1}" > "$SEL_AUR_PCKGS"

        createCmdFileForSamba "dlgFormVals"

        showProgBarAndTail
        rm -rf "$SEL_AUR_PCKGS"
        rm -rf "$POST_INST_CMD_FILE"

        flagVal=1
      fi
    else
      flagVal=-1
    fi
  done

  updateSoftwareTaskChecklist "${piAssocArray["chklArray"]}" ${flagVal} "$1"
}

#---------------------------------------------------------------------------------------
#      METHOD:                       createSambaUser
# DESCRIPTION: Create the user & group to be used with samba
#  Required Params:
#    1)      optSel - The value of the option that was selected for this task
#    2)     optDesc - The description/name of this task
#    3)    formVals - name of the array that will contain the values submitted
#---------------------------------------------------------------------------------------
createSambaUser() {
  local -n formVals="$3"
  local urlRef="https://wiki.archlinux.org/index.php/samba#Adding_a_user"
  local dlgWindowTitle=$(printf "%s - %s" "$1" "$2")
  local dialogTitle="Create Samba User:"
  local dialogText="Enter the group name, user name & password:"

  getPasswordsFromFormDialog "$dlgWindowTitle" "$dialogTitle" "$urlRef" "$dialogText"
  cleanDialogFiles

  if [ ${#pswdVals[@]} -gt 0 ]; then
    if [ ${#pswdVals[@]} -gt 4 ]; then
      formVals=(${pswdVals[@]:1})
    else
      formVals=(${pswdVals[@]})
    fi
  else
    formVals=()
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                    createCmdFileForSamba
# DESCRIPTION: Create the command file for Samba that will contain the necessary
#              commands to configure and enable Samba
#    1) sambaUserArray - name of the array that will contain the following values
#                        at the specified index:
#               index 0) samba group name
#               index 1) name of the samba user
#               index 2) password of the samba user
#               index 3) password confirmation
#---------------------------------------------------------------------------------------
createCmdFileForSamba() {
  local -n sambaUserArray="$1"
  local sambaConfFile="/etc/samba/smb.conf"
  local sambaShareDir="/var/lib/samba/user-share"
  local sambaGroup="${sambaUserArray[0]}"
  local sambaUser="${sambaUserArray[1]}"
  local userName="${POST_ASSOC_ARRAY["UserName"]}"
  local cmds=("Add group for Samba sharing${FORM_FLD_SEP}groupadd -r $sambaGroup")

  if [ "$sambaUser" != "${POST_ASSOC_ARRAY["UserName"]}" ]; then
    cmds+=("Creating samba user${FORM_FLD_SEP}useradd -r -s /usr/bin/nologin $sambaUser")
    cmds+=("Adding samba user '$sambaUser' to group${FORM_FLD_SEP}gpasswd $sambaGroup -a $sambaUser")
    cmds+=("Adding user '$userName' to group${FORM_FLD_SEP}gpasswd $sambaGroup -a $userName")
  else
    cmds+=("Adding samba user '$sambaUser' to group${FORM_FLD_SEP}gpasswd $sambaGroup -a $sambaUser")
  fi

  local cmd=$(echo "echo -ne ${sambaUserArray[2]}\n${sambaUserArray[3]}\n | smbpasswd -a $sambaUser")
  cmds+=("Adding samba user '$sambaUser' to Samba${FORM_FLD_SEP}$cmd"
        "Creating Samba shared directory${FORM_FLD_SEP}mkdir -p ${sambaShareDir}"
        "Setting ownership on Samba shared directory${FORM_FLD_SEP}chown root:${sambaGroup} ${sambaShareDir}"
        "Setting permissions on Samba shared directory${FORM_FLD_SEP}chmod 1770 ${sambaShareDir}")

  cmd=$(echo "sed -i -e '/\[global\]/a\\n   usershare path = /var/lib/samba/usershare\n   usershare max shares = 100\n   usershare allow guests = yes\n   usershare owner only = False' $sambaConfFile")
  cmds+=("Setting the 'usershare' parameters in the Samba configuration file${FORM_FLD_SEP}$cmd")
  cmd=$(echo "sed -i -e '/\[global\]/a\\n   socket options = IPTOS_LOWDELAY TCP_NODELAY SO_KEEPALIVE\n   write cache size = 2097152\n   use sendfile = yes\n' $sambaConfFile")
  cmds+=("Setting the 'socket' parameters in the Samba configuration file${FORM_FLD_SEP}$cmd"
        "Enabling service for Samba${FORM_FLD_SEP}systemctl enable smb.service"
        "Enabling service for NetBIOS name server${FORM_FLD_SEP}systemctl enable nmb.service")

  local concatStr=$(printf "\n%s" "${cmds[@]}")

  echo -e "${concatStr:1}" > "$POST_INST_CMD_FILE"
}

#---------------------------------------------------------------------------------------
#      METHOD:                         installSSH
# DESCRIPTION: Install the group of AUR packages to be able to do remote login into
#              the Arch Linux system.
#  Required Params:
#    1)      optSel - The value of the option that was selected for this task
#    2)     optDesc - The description/name of this task
#---------------------------------------------------------------------------------------
installSSH() {
  local flagVal=0
  local -A funcParams=(["helpText"]=$(getTextForSSH)
                      ["menuOptsRef"]="SSH_IMPLS" ["optDescsRef"]="AUR_PCKG_DESCS")
  local windowTitle=$(printf "%s - %s" "${SEL_CHKLIST_OPT[0]}" "${SEL_CHKLIST_OPT[1]}")
  local dialogTitle=$(printf "%s - %s" "$1" "$2")

  if [ "$DIALOG" == "yad" ]; then
    funcParams["dialogTitle"]="$windowTitle"
  else
    funcParams["dialogBackTitle"]="$windowTitle"
    funcParams["dialogTitle"]="$dialogTitle"
    funcParams["dialogText"]="Choose the implementation of the Secure Shell (SSH) to install:"
  fi

  funcParams["defaultChoice"]="${SSH_IMPLS[1]}"

  selectPackageForSSH "funcParams"
  cleanDialogFiles

  if [ ${#mbSelVal} -gt 0 ]; then
    local aurPckgName="${AUR_PCKGS["$mbSelVal"]}"
    POST_ASSOC_ARRAY["SSH"]=$(echo "$aurPckgName")
    echo "$aurPckgName" > "$SEL_AUR_PCKGS"

    case "$mbSelVal" in
      "${SSH_IMPLS[1]}")
        local cmds=(
          "Enabling the service for ${mbSelVal}${FORM_FLD_SEP}systemctl enable sshd.service"
          "Generating the host keys for each of the key types (rsa, dsa, ecdsa and ed25519)${FORM_FLD_SEP}ssh-keygen -A"
          "Configuring the SSH daemon configuration file${FORM_FLD_SEP}configureOpenSSH"
        )

        concatStr=$(printf "\n%s" "${cmds[@]}")
        echo -e "${concatStr:1}" > "$POST_INST_CMD_FILE"
      ;;
      *)
        local cmd=$(echo "Enabling the service for ${mbSelVal}${FORM_FLD_SEP}systemctl enable ${aurPckgName}.service")
        echo -e "$cmd" > "$POST_INST_CMD_FILE"
      ;;
    esac

    showProgBarAndTail
    rm -rf "$SEL_AUR_PCKGS"
    rm -rf "$POST_INST_CMD_FILE"
    flagVal=1
  else
    flagVal=-1
  fi

  updateSoftwareTaskChecklist "${piAssocArray["chklArray"]}" ${flagVal} "$1"
}

#---------------------------------------------------------------------------------------
#      METHOD:                         installTLP
# DESCRIPTION: Install the group of AUR packages for the Linux Advanced Power Management
#  Required Params:
#    1)      optSel - The value of the option that was selected for this task
#    2)     optDesc - The description/name of this task
#---------------------------------------------------------------------------------------
installTLP() {
  local flagVal=0
  local helpText=$(getTextForTLP)

  showInstConfDialog "$1" "$2" "$helpText" "TLP_PCKGS" "tlp.png"

  if ${yesNoFlag}; then
    local pckgNames=()
    for menuOpt in "${TLP_PCKGS[@]}"; do
      pckgNames+=("${AUR_PCKGS["$menuOpt"]}")
    done

    local concatStr=$(printf " %s" "${pckgNames[@]}")
    echo -e "${concatStr:1}" > "$SEL_AUR_PCKGS"

    local cmds=(
      "Enabling service for TLP${FORM_FLD_SEP}systemctl enable tlp.service"
      "Enabling TLP sleep service${FORM_FLD_SEP}systemctl enable tlp-sleep.service"
      "Masking RF kill switch service${FORM_FLD_SEP}systemctl mask systemd-rfkill.service"
      "Masking RF kill switch socket${FORM_FLD_SEP}systemctl mask systemd-rfkill.socket"
    )

    local existFlag=$(isAURPckgInst "networkmanager")
    if ! ${existFlag}; then
      cmds+=("Masking service for NetworkManager${FORM_FLD_SEP}systemctl mask NetworkManager.service")
    fi

    concatStr=$(printf "\n%s" "${cmds[@]}")
    echo -e "${concatStr:1}" > "$POST_INST_CMD_FILE"

    showProgBarAndTail
    rm -rf "$SEL_AUR_PCKGS"
    rm -rf "$POST_INST_CMD_FILE"
    flagVal=1
  else
    flagVal=-1
  fi

  updateSoftwareTaskChecklist "${piAssocArray["chklArray"]}" ${flagVal} "$1"
}

#---------------------------------------------------------------------------------------
#      METHOD:                       installZRamSwap
# DESCRIPTION: Install the group of AUR packages to be able to use "swap" with RAM
#              instead of the disk.
#  Required Params:
#    1)      optSel - The value of the option that was selected for this task
#    2)     optDesc - The description/name of this task
#---------------------------------------------------------------------------------------
installZRamSwap() {
  local flagVal=0
  local helpText=$(getTextForZRamSwap)

  if [ ${BASIC_SETUP_CHECKLIST[-1]} -gt 1 ]; then
    yesNoFlag=$(echo 'true' && return 0)
  else
    showInstConfDialog "$1" "$2" "$helpText" "ZRAM_PCKG" "zram.png"
  fi

  if ${yesNoFlag}; then
    local aurPckgName="${ZRAM_PCKG[0]}"
    echo -e "$aurPckgName" > "$SEL_AUR_PCKGS"

    echo -e "Enabling service for ZRam Swap${FORM_FLD_SEP}systemctl enable ${aurPckgName}.service" > "$POST_INST_CMD_FILE"

    showProgBarAndTail
    rm -rf "$SEL_AUR_PCKGS"
    rm -rf "$POST_INST_CMD_FILE"
    flagVal=1
  else
    flagVal=-1
  fi

  updateSoftwareTaskChecklist "${piAssocArray["chklArray"]}" ${flagVal} "$1"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                         getWarnMsg
# DESCRIPTION: Get the warning message when a task is selected out of sequence.
#      RETURN: concatenated string
#  Required Params:
#      1) paramAssocArray - the name of the associative array with the required
#         key/value pairs:
#      "defaultVal"  - The next logical task.
#      "optDescsRef" - name of the associative array containing the description
#                      of the menu options
#---------------------------------------------------------------------------------------
function getWarnMsg() {
  local -n paramAssocArray="$1"
  local -n optDescsRef="${paramAssocArray["descAssocArray"]}"
  local nextTask="${paramAssocArray["defaultVal"]}"
  local desc="${optDescsRef["$nextTask"]}"
  local textArray=("Some of the tasks depend other tasks to be completed first.  Therefore, the tasks for the"
                    "\"Basic Setup\" step need to run in the order that is displayed.")
  local concatStr=$(printf " %s" "${textArray[@]}")
  textArray=("${concatStr:1}" " " "Please continue with the task \"${nextTask}:  ${desc}\"!")
  local warnMsg=$(getTextForDialog "${textArray[@]}")

  echo "$warnMsg"
}

#---------------------------------------------------------------------------------------
#      METHOD:                     setHelpTextAndTitle
# DESCRIPTION: Sets the title and text for the help section for the current task to exec.
#---------------------------------------------------------------------------------------
setHelpTextAndTitle() {
  local defVal="${piAssocArray["defaultVal"]}"
  local -n descAssocArray="${piAssocArray["descAssocArray"]}"
  local taskName="${descAssocArray["$defVal"]}"
  piAssocArray["helpTitle"]="Help:  ${defVal} - $taskName"

  piAssocArray["helpText"]=$(getHelpForBasicSetup "$taskName")
}

#---------------------------------------------------------------------------------------
#  MAIN
#---------------------------------------------------------------------------------------
main() {
  local errMsgArray=("The \"$2\" software task must be the first to run and complete"
  "before the other category/software tasks in the \"Checklist\" tab can be selected!!!!!!!")
  local errMsg=$(printf " %s" "${errMsgArray[@]}")
  local loopFlag=$(echo 'true' && return 0)
  initVars "$@"

  local -A piAssocArray=(["listType"]="radiolist" ["optsArray"]="MENU_OPTS_ARRAY"
                        ["descAssocArray"]="DESC_ASSOC_ARRAY" ["chklArray"]="BASIC_SETUP_CHECKLIST")
  if [ "$DIALOG" == "yad" ]; then
    piAssocArray+=(["tabName"]="${SEL_CHKLIST_OPT[1]}" ["tabNum"]=2
          ["tabColumns"]="Radio Button${FORM_FLD_SEP}Is Done${FORM_FLD_SEP}$2 Task${FORM_FLD_SEP}Description"
          ["nbButtons"]="$NB_BTNS"
          ["tabText"]="Click the \"Run\" button to execute the selected task!")
  fi

  while ${loopFlag}; do
    piAssocArray["defaultVal"]=$(getDefaultOption)
    setHelpTextAndTitle
    if [ "${piAssocArray["defaultVal"]}" == "done" ]; then
      clog_info "All tasks for the \"Basic Setup\" step are done!!!"
      loopFlag=$(echo 'false' && return 1)
    else
      setNextTaskForStep
      if [ ${#mbSelVal} -gt 0 ]; then
        local optDesc=()
        readarray -t optDesc <<< "${mbSelVal//$FORM_FLD_SEP/$'\n'}"
        if [ ${#optDesc[@]} -gt 1 ]; then
          if [ "${piAssocArray["defaultVal"]}" == "${optDesc[0]}" ]; then
            execBasicSetupTask "${optDesc[0]}" "${optDesc[1]}"
          else
            local warnMsg=$(getWarnMsg "piAssocArray")
            showWarningDialog "Selection of \"$2\" task!" "$warnMsg"
          fi
        fi
      else
        loopFlag=$(echo 'false' && return 1)
      fi
    fi
    if ! ${loopFlag} && [ ${piAssocArray["contFlag"]+_} ]; then
      loopFlag=$(echo 'true' && return 0)
      unset piAssocArray["contFlag"]
    fi

  done

  updatePostInstAssocArray "${piAssocArray["chklArray"]}"

  if [ ${BASIC_SETUP_CHECKLIST[0]} -gt 0 ]; then
    rm -rf ${DATA_DIR}/software/YAD-*
    rm -rf ${POST_INST_YAD_TABS}
  fi
}

main "$@"
exit 0
