#!/bin/bash

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# globals
#===============================================================================
CUR_DIR="${PWD}"
INC_FILE=$(echo "$CUR_DIR/inc/post-inst-inc.sh")
KDE_CONSTS_FILE=$(echo "$CUR_DIR/inc/post-inst/kde-constants.sh")
CONSTS_FILE=$(echo "$CUR_DIR/inc/post-inst/$2")

source "$INC_FILE"
source "$GRAPHICAL_PATH/post-install-inc.sh"
source "$KDE_CONSTS_FILE"
source "$CONSTS_FILE"
source -- "$POST_ASSOC_ARRAY_FILE"
source -- "$ASSOC_ARRAY_FILE"

declare CATEGORY_CHECKLIST=(0)
declare CHECKLIST_KEY=""
declare METHOD_KEY_FMT="method#%02d"
declare UEFI_FLAG=0
declare -A INST_AUR_PCKGS=()

if [ -f "$AUR_INSTALLED_PCKGS" ]; then
  source -- "$AUR_INSTALLED_PCKGS"
fi

#===============================================================================
# functions/methods
#===============================================================================

#---------------------------------------------------------------------------------------
#      METHOD:                       initVars
# DESCRIPTION: Initialize the global variables.
#---------------------------------------------------------------------------------------
initVars() {
  UEFI_FLAG=$(echo 'false' && return 1)
  if [ "${varMap["BOOT-MODE"]}" == "UEFI" ]; then
    UEFI_FLAG=$(echo 'true' && return 0)
  fi

  readarray -t SEL_CHKLIST_OPT <<< "${1//$FORM_FLD_SEP/$'\n'}"

  CHECKLIST_KEY="${SEL_CHKLIST_OPT[1]}"

  if [ ${POST_ASSOC_ARRAY["$CHECKLIST_KEY"]+_} ]; then
    readarray -t CATEGORY_CHECKLIST <<< "${POST_ASSOC_ARRAY["$CHECKLIST_KEY"]//$FORM_FLD_SEP/$'\n'}"
  else
    for menuOpt in "${MENU_OPTS_ARRAY[@]}"; do
      CATEGORY_CHECKLIST+=(0)
    done
  fi

  local txt="Basic Setup"
  TEXT_SKIP_TASK=$(echo "${TEXT_SKIP_TASK/$txt/${SEL_CHKLIST_OPT[1]}}")
}

#---------------------------------------------------------------------------------------
#      METHOD:                      displayOptsForCat
# DESCRIPTION: Show the options available for the selected category
#  Required Params:
#    1)  optSel - the option that was selected
#    2) optDesc - the description/name of the option that was selected
#---------------------------------------------------------------------------------------
displayOptsForCat() {
  piAssocArray["defaultVal"]="$1"
  if [ "$DIALOG" == "yad" ]; then
    saveTabData "$1"
  fi

  if [ ${SUB_CAT_ASSOC_ARRAY["$2"]+_} ]; then
    showSoftOptsDialogForSubCat "$1" "$2"
  else
    showSoftOptsDialogForCat "$1" "$2"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                  showSoftOptsDialogForCat
# DESCRIPTION: Show a dialog of the options available for the selected category
#  Required Params:
#    1)  optSel - the option that was selected
#    2) optDesc - the description/name of the option that was selected
#---------------------------------------------------------------------------------------
showSoftOptsDialogForCat() {
  local optSel="$1"
  local optDesc="$2"
  local catMenuOpts=()
  local sep="|"
  local -A funcAssocArray=(["listType"]="checklist" ["descAssocArray"]="AUR_PCKG_DESCS"
                          ["helpTitle"]="Help:  $optSel - $optDesc")
  local pckgs="${CATEGORY_MENU_OPTS["$optDesc"]}"
  if [[ "$pckgs" =~ \.sh$ ]]; then
    local constsFile=$(echo "$CUR_DIR/inc/post-inst/$pckgs")
    source "$constsFile"
    funcAssocArray["optsArray"]="SOFT_CAT_OPTS"
  else
    readarray -t catMenuOpts <<< "${pckgs//$sep/$'\n'}"
    funcAssocArray["optsArray"]="catMenuOpts"
  fi
  funcAssocArray["helpText"]=$(getHelpTextForCategoryDialog "${SEL_CHKLIST_OPT[1]}" "$2")

  if [ "$DIALOG" == "yad" ]; then
    local lastTabNum=$(cat "$POST_INST_YAD_TABS" | wc -l)

    funcAssocArray+=(["tabName"]=$(escapeSpecialCharacters "$optDesc") ["tabNum"]=$(expr ${lastTabNum} + 1)
          ["tabColumns"]="Checkbox${FORM_FLD_SEP}AUR Package Name${FORM_FLD_SEP}Description"
          ["nbButtons"]="$CAT_NB_BTNS"
          ["tabText"]="Click the \"Install\" button to install the selected AUR packages!")
  else
    funcAssocArray["extra-label"]="Skip"
    local catTextArray=("Select <  OK  > to install the AUR packages that were selected, or"
      "Select < Skip > to mark the category as skipped.")
    funcAssocArray["dialogText"]=$(getTextForDialog "${catTextArray[@]}")
  fi

  showSoftwareOptsForCat "funcAssocArray" "$optSel" "$optDesc"
  cleanDialogFiles
  processSelSoftOpts "$optSel" "$optDesc"
}

#---------------------------------------------------------------------------------------
#      METHOD:                     processSelSoftOpts
# DESCRIPTION: Either:
#                A) Install the AUR packages that match the names of those selected.
#                   After the installation has finished processing what was selected,
#                   the category will be marked as Done.
#                B) Mark this category as Skipped if the "Skip" button was clicked.
#  Required Params:
#    1)  optSel - the option that was selected
#    2) optDesc - the description/name of the option that was selected
#---------------------------------------------------------------------------------------
processSelSoftOpts() {
  local flagVal=0
  echo "$mbSelVal"

  if [ ${#mbSelVal} -gt 0 ]; then
    local dialogVals=()
    local sep="|"
    readarray -t dialogVals <<< "${mbSelVal//$FORM_FLD_SEP/$'\n'}"

    if [ ${#dialogVals[@]} -gt 1 ]; then
      if [ "${dialogVals[1]}" == "Skip" ]; then
        flagVal=-1
      else
        piAssocArray["yadNB"]="$mbSelVal"
      fi
    else
      createFileToInstPckgs "$2"

      flagVal=1

      showProgBarAndTail

      if [[ -f "$SEL_AUR_PCKGS" ]]; then
        rm -rf "$SEL_AUR_PCKGS"
      fi
      if [[ -f "$POST_INST_CMD_FILE" ]]; then
        rm -rf "$POST_INST_CMD_FILE"
      fi
    fi

    if [ ${flagVal} -ne 0 ]; then
      updateSoftwareTaskChecklist "${piAssocArray["chklArray"]}" ${flagVal} "$1"
      updateTabData "$1" ${flagVal}
    fi
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                    createFileToInstPckgs
# DESCRIPTION: Create the file for the global variable "$SEL_AUR_PCKGS" that will contain
#              the name of the AUR packages to install.
#  Required Params:
#    1) optDesc - the description/name of the option that was selected
#---------------------------------------------------------------------------------------
createFileToInstPckgs() {
  local selDialogVals=()
  local sep="|"
  local aurPckgName=""
  local scriptCmd="Setting 'arch-release'${FORM_FLD_SEP}cat /proc/version > /etc/arch-release"
  local scriptCmdArray=()

  readarray -t selDialogVals <<< "${mbSelVal//$sep/$'\n'}"
  rm -rf "$SEL_AUR_PCKGS"

  for aurName in "${selDialogVals[@]:1}"; do
    aurPckgName="${AUR_PCKGS["$aurName"]}"
    case "$aurName" in
      "Adobe TTF")
        scriptCmdArray+=("Installing the '$aurName' package ${FORM_FLD_SEP}software-install/adobeTTF.sh")
      ;;
      "Conky")
        showConkySelectionDlg
      ;;
      "IntelliJ IDEA CE"|"IntelliJ IDEA UE")
        echo "libdbusmenu-glib $aurPckgName" >> "$SEL_AUR_PCKGS"
      ;;
      "Sublime Text")
        scriptCmdArray+=("Installing the '$aurName' package ${FORM_FLD_SEP}software-install/install-sublime.sh")
      ;;
      *)
        if [[ "$aurPckgName" =~ ^software-install ]]; then
          scriptCmdArray+=("Installing the '$aurName' package ${FORM_FLD_SEP}$aurPckgName")
        else
          echo "$aurPckgName" >> "$SEL_AUR_PCKGS"
        fi
      ;;
    esac
  done

  readarray -t selDialogVals < "$SEL_AUR_PCKGS"
  local concatStr=$(printf " %s" "${selDialogVals[@]}")
  INST_AUR_PCKGS["$1"]=$(echo -e "${concatStr}")
  declare -p INST_AUR_PCKGS > "${AUR_INSTALLED_PCKGS}"
  if [ ${#scriptCmdArray[@]} -gt 0 ]; then
    local concatStr=$(printf "\n%s" "${scriptCmdArray[@]}")
    echo -e "${concatStr:1}" > "$POST_INST_CMD_FILE"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                    showConkySelectionDlg
# DESCRIPTION: Show a dialog to select the package of Conky to install.
#---------------------------------------------------------------------------------------
showConkySelectionDlg() {
  local flagVal=0
  local conkyPckgs=("${CONKY_PCKGS[@]}")
  local -A funcParams=(["helpText"]=$(getHelpTextForConky)
    ["menuOptsRef"]="conkyPckgs" ["optDescsRef"]="AUR_PCKG_DESCS")
  local windowTitle="Installation of the Conky system monitor software"
  local dialogTitle="Conky and alternative packages with extra compile options enabled."
  local sep="|"

  if [ "${POST_ASSOC_ARRAY["VC-BRAND"]}" != "NVIDIA" ]; then
    local idx=-1
    unset conkyPckgs[$idx]
    unset conkyPckgs[$idx]
  fi

  if [ "$DIALOG" == "yad" ]; then
    funcParams["dialogTitle"]="$windowTitle"
    funcParams["dialogTextHdr"]="$dialogTitle"
  else
    funcParams["dialogBackTitle"]="$windowTitle"
    funcParams["dialogTitle"]="$dialogTitle"
  fi

  funcParams["defaultChoice"]="${conkyPckgs[1]}"
  funcParams["dialogText"]="Choose the package to install:"

  selectConkyPackage "funcParams"
  cleanDialogFiles
  local aurPckgName="${AUR_PCKGS["$mbSelVal"]}"
  echo "$aurPckgName" >> "$SEL_AUR_PCKGS"
}

#---------------------------------------------------------------------------------------
#      METHOD:                 showSoftOptsDialogForSubCat
# DESCRIPTION: Show a dialog of the options available for the selected sub-category
#              (i.e. "Readers & Viewers")
#  Required Params:
#    1)  optSel - the option that was selected
#    2) optDesc - the description/name of the option that was selected
#---------------------------------------------------------------------------------------
showSoftOptsDialogForSubCat() {
  local optSel="$1"
  local optDesc="$2"
  local rvMenuOpts=()
  local -A rvMenuOptDecs=()
  local constsFile=$(echo "$CUR_DIR/inc/post-inst/${CATEGORY_MENU_OPTS["$optDesc"]}")
  local -A funcAssocArray=(["listType"]="radiolist" ["optsArray"]="rvMenuOpts"
                        ["descAssocArray"]="rvMenuOptDecs"
                        ["helpText"]=$(getHelpTextForSubCatDialog "${SEL_CHKLIST_OPT[1]}" "$2")
                        ["helpTitle"]="Help:  $optSel - $optDesc")
  readarray -t rvMenuOpts <<< "${SUB_CAT_ASSOC_ARRAY["$2"]//$FORM_FLD_SEP/$'\n'}"

  for rvOpt in "${rvMenuOpts[@]}"; do
    rvMenuOptDecs["$rvOpt"]="${DESC_ASSOC_ARRAY["$rvOpt"]}"
  done

  funcAssocArray["defaultVal"]="${rvMenuOpts[0]}"

  source "$constsFile"

  if [ "$DIALOG" == "yad" ]; then
    local lastTabNum=$(cat "$POST_INST_YAD_TABS" | wc -l)

    funcAssocArray+=(["tabName"]=$(escapeSpecialCharacters "$optDesc") ["tabNum"]=$(expr ${lastTabNum} + 1)
          ["tabColumns"]="Radio Btn${FORM_FLD_SEP}Sub-Category${FORM_FLD_SEP}Sub-Cat. Name"
          ["nbButtons"]="$NB_BTNS_FOR_SUB_CAT"
          ["tabText"]="Click the \"Select\" button to select the AUR packages of the sub-category!")
  else
    local catTextArray=("Select <  OK  > to select the AUR packages of the sub-category, or"
      "Select < Skip > to mark the category as skipped.")
    funcAssocArray["dialogText"]=$(getTextForDialog "${catTextArray[@]}")
  fi

  showSoftwareOptsForSubCat "funcAssocArray" "$optSel" "$optDesc"
  if [[ -e "$POST_INST_YAD_TABS" ]]; then
    sed -i '$d' "$POST_INST_YAD_TABS"
  fi
  cleanDialogFiles
  processSelSoftOpts "$optSel" "$optDesc"
}

#---------------------------------------------------------------------------------------
#      METHOD:                         saveTabData
# DESCRIPTION: Save the data file for the YAD tab.
#  Required Params:
#    1)  optSel - the option that was selected
#---------------------------------------------------------------------------------------
saveTabData() {
  local optSel="$1"
  local srcFile=$(printf "${NB_TAB_DATA_FNS["tabData"]}" 2)
  local path=()
  local sep="/"

  readarray -t path <<< "${srcFile//$sep/$'\n'}"
  path[1]="${DATA_DIR}/software"
  local destFile=$(printf "/%s" "${path[@]:1}")
  destFile=${destFile:1}

  setDataForPostInstTabNB
  mv "$srcFile" "$destFile"
}

#---------------------------------------------------------------------------------------
#      METHOD:                        removeTabData
# DESCRIPTION: Remove the data file that was saved for the 2nd YAD tab.
#---------------------------------------------------------------------------------------
removeTabData() {
  if [ "$DIALOG" == "yad" ]; then
    local srcFile=$(printf "${NB_TAB_DATA_FNS["tabData"]}" 2)
    local path=()
    local sep="/"

    readarray -t path <<< "${srcFile//$sep/$'\n'}"
    path[1]="${DATA_DIR}/software"
    local destFile=$(printf "/%s" "${path[@]:1}")
    destFile=${destFile:1}

    rm -rf "$destFile"
    sed -i '$d' "$POST_INST_YAD_TABS"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                        updateTabData
# DESCRIPTION: Update 'Is Done' column within data file for the 2nd tab
#  Required Params:
#      1)  optSel - the category option that was selected
#  Optional Param:
#      2) flagVal - value to set the 'Is Done' column to
#              a) -1 - Skipped
#              b)  1 - $DONE_FLAG
#---------------------------------------------------------------------------------------
updateTabData() {
  local optSel="$1"
  local flagVal=$2
  local lineNum=0
  local lastTabNum=0
  if [[ -e "$POST_INST_YAD_TABS" ]]; then
    lastTabNum=$(cat "$POST_INST_YAD_TABS" | wc -l)
  fi
  if [ ${lastTabNum} -gt 1 ]; then
    local srcFile=$(printf "${NB_TAB_DATA_FNS["tabData"]}" 2)
    local path=()
    local sep="/"

    readarray -t path <<< "${srcFile//$sep/$'\n'}"
    path[1]="${DATA_DIR}/software"
    local destFile=$(printf "/%s" "${path[@]:1}")
    destFile=${destFile:1}

    local lineNum=$(sed -n "/^${optSel}/=" "$destFile")

    if [ "$#" -gt 1 ]; then
      local status=""

      lineNum=$(expr ${lineNum} - 1)
      if [ ${flagVal} -gt 0 ]; then
        status="$DONE_FLAG"
      else
        status="   Skipped"
      fi
      sed -i "${lineNum}s/^/${status}/g" "$destFile"
    else
      sed -i "s/TRUE/FALSE/g" "$destFile"
      lineNum=$(expr ${lineNum} - 2)
      sed -i "${lineNum}s/FALSE/TRUE/g" "$destFile"
    fi
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                      setDefaultMenuOpt
# DESCRIPTION: Set the default menu option.
#---------------------------------------------------------------------------------------
setDefaultMenuOpt() {
  if [ ${piAssocArray["yadNB"]+_} ]; then
    local yadOpt=()
    local digits=()
    local sep="."

    readarray -t yadOpt <<< "${piAssocArray["yadNB"]//$FORM_FLD_SEP/$'\n'}"

    readarray -t digits <<< "${yadOpt[0]//$sep/$'\n'}"

    if [ ${#digits[@]} -gt 2 ]; then
      piAssocArray["defaultVal"]="${piAssocArray["yadNB"]}"
    else
      updateChecklist "${yadOpt[0]}"
      piAssocArray["defaultVal"]="done"
    fi

    unset piAssocArray["yadNB"]
  else
    piAssocArray["defaultVal"]=$(getDefaultOption)
    updateTabData "${piAssocArray["defaultVal"]}"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                       updateChecklist
# DESCRIPTION: Set the option in the Checklist tab as the one selected based the currently
#              selected value within the tab.
#  Required Params:
#      1)  optSel - the software task that was selected
#---------------------------------------------------------------------------------------
updateChecklist() {
  local optSel="$1"
  local srcFile=$(printf "${NB_TAB_DATA_FNS["tabData"]}" 1)
  local path=()
  local sep="/"

  readarray -t path <<< "${srcFile//$sep/$'\n'}"
  path[1]="${DATA_DIR}/software"
  local destFile=$(printf "/%s" "${path[@]:1}")
  destFile=${destFile:1}

  local lineNum=$(sed -n "/^${optSel}/=" "$destFile")
  lineNum=$(expr ${lineNum} - 2)

  sed -i "s/TRUE/FALSE/g" "$destFile"

  sed -i "${lineNum}s/FALSE/TRUE/g" "$destFile"
  POST_ASSOC_ARRAY["nextSoftCatTask"]="$1"
}

#---------------------------------------------------------------------------------------
#      METHOD:                       setSkippedCats
# DESCRIPTION: Set/mark the categories within the checklist that have a value of 0
#              (i.e. neither done nor been skipped) to -1 (Skipped).
#  Required Params:
#      1)  optSel - the software task that was selected
#---------------------------------------------------------------------------------------
setSkippedCats() {
  local startIdx=1
  local endIdx=$(expr ${#CATEGORY_CHECKLIST[@]} - 1)

  for aryIdx in $(eval echo "{${startIdx}..${endIdx}}"); do
    if isValZero ${CATEGORY_CHECKLIST[${aryIdx}]} &> /dev/null; then
      CATEGORY_CHECKLIST[${aryIdx}]=-1
    fi
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                     setHelpTextAndTitle
# DESCRIPTION: Sets the title and text for the help section for the current task to exec.
#---------------------------------------------------------------------------------------
setHelpTextAndTitle() {
  piAssocArray["helpTitle"]="Help:  ${SEL_CHKLIST_OPT[0]} - ${SEL_CHKLIST_OPT[1]}"

  piAssocArray["helpText"]=$(getHelpTextForCatSoftTask "${SEL_CHKLIST_OPT[1]}")
}

#---------------------------------------------------------------------------------------
#  MAIN
#---------------------------------------------------------------------------------------
main() {
  local loopFlag=$(echo 'true' && return 0)
  initVars "$@"

  local -A piAssocArray=(["listType"]="radiolist" ["optsArray"]="MENU_OPTS_ARRAY"
                        ["descAssocArray"]="DESC_ASSOC_ARRAY" ["chklArray"]="CATEGORY_CHECKLIST")
  if [ "$DIALOG" == "yad" ]; then
    piAssocArray+=(["tabName"]=$(escapeSpecialCharacters "${SEL_CHKLIST_OPT[1]}") ["tabNum"]=2
          ["tabColumns"]="Radio Button${FORM_FLD_SEP}Is Done${FORM_FLD_SEP}Desktop Env. Task${FORM_FLD_SEP}Description"
          ["nbButtons"]="$NB_BTNS_FOR_CAT"
          ["tabText"]="Click the \"Select\" button to choose from the category of the AUR packages to install!")
  else
    piAssocArray["extra-label"]="Skip"
    local catTextArray=("Select <  OK  > to display the AUR packages and sub-categories that can be installed, or"
      "Select < Skip > to mark all the categories that have '[ ]' in their last column as skipped.")
    piAssocArray["dialogText"]=$(getTextForDialog "${catTextArray[@]}")
  fi

  while ${loopFlag}; do
    setDefaultMenuOpt
    setHelpTextAndTitle

    if [ "${piAssocArray["defaultVal"]}" == "done" ]; then
      loopFlag=$(echo 'false' && return 1)
    else
      local optDesc=()
      readarray -t optDesc <<< "${piAssocArray["defaultVal"]//$FORM_FLD_SEP/$'\n'}"
      if [ "$DIALOG" == "yad" ] && [ ${#optDesc[@]} -gt 1 ]; then
        #### Need to do this for YAD dialogs because of possible special characters
        optDesc[1]="${DESC_ASSOC_ARRAY["${optDesc[0]}"]}"

        displayOptsForCat "${optDesc[0]}" "${optDesc[1]}"
      else
        setNextTaskForStep
        if [ ${#mbSelVal} -gt 0 ]; then
          readarray -t optDesc <<< "${mbSelVal//$FORM_FLD_SEP/$'\n'}"
          if [ ${#optDesc[@]} -gt 1 ]; then
            if [ "${optDesc[1]}" == "Skip" ]; then
              setSkippedCats
              loopFlag=$(echo 'false' && return 1)
            else
              #### Need to do this for YAD dialogs because of possible special characters
              optDesc[1]="${DESC_ASSOC_ARRAY["${optDesc[0]}"]}"
              displayOptsForCat "${optDesc[0]}" "${optDesc[1]}"
            fi
          fi
        else
          loopFlag=$(echo 'false' && return 1)
        fi
      fi
    fi
  done

  updatePostInstAssocArray "${piAssocArray["chklArray"]}" "${SEL_CHKLIST_OPT[1]}"

  if isValZero ${CATEGORY_CHECKLIST[0]} &> /dev/null; then
    removeTabData
  else
    rm -rf ${DATA_DIR}/software/YAD-*
    rm -rf ${POST_INST_YAD_TABS}
  fi
}

main "$@"
exit 0
