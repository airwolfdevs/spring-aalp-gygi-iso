#!/bin/bash

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# globals
#===============================================================================
CUR_DIR="${PWD}"
INC_FILE=$(echo "$CUR_DIR/inc/post-inst-inc.sh")
CONSTS_FILE=$(echo "$CUR_DIR/inc/post-inst/cat-desktop-config.sh")
DM_WM_CONSTS_FILE=$(echo "$CUR_DIR/inc/post-inst/de-dm-wm-consts.sh")
DIT_CONSTS_FILE=$(echo "$CUR_DIR/inc/post-inst/desktop-icon-themes.sh")
FONT_CONSTS_FILE=$(echo "$CUR_DIR/inc/post-inst/fonts.sh")
VIDEO_CONSTS_FILE=$(echo "$CUR_DIR/inc/post-inst/video-card-drivers.sh")
KDE_CONSTS_FILE=$(echo "$CUR_DIR/inc/post-inst/kde-constants.sh")
TASKBAR_CONSTS_FILE=$(echo "$CUR_DIR/inc/post-inst/taskbars.sh")

source "$INC_FILE"
source "$GRAPHICAL_PATH/desktop-config-inc.sh"
source "$GRAPHICAL_PATH/post-install-inc.sh"
source "$CONSTS_FILE"
source "$DM_WM_CONSTS_FILE"
source "$VIDEO_CONSTS_FILE"
source "$DIT_CONSTS_FILE"
source "$FONT_CONSTS_FILE"
source "$KDE_CONSTS_FILE"
source "$TASKBAR_CONSTS_FILE"
source -- "$POST_ASSOC_ARRAY_FILE"
source -- "$ASSOC_ARRAY_FILE"

declare DESKTOP_ENV_CHECKLIST=(0)
declare CHECKLIST_KEY="DESKTOP_ENV_CHECKLIST"
declare -A BRANDS_DESCS=()
declare -A VCD_HELP_ARRAY=()
declare METHOD_KEY_FMT="method#%02d"
declare BRANDS_HELP_KEY="unknown"
declare UEFI_FLAG=0
declare -A INST_AUR_PCKGS=()

if [ -f "$AUR_INSTALLED_PCKGS" ]; then
  source -- "$AUR_INSTALLED_PCKGS"
fi

#===============================================================================
# functions/methods
#===============================================================================

#---------------------------------------------------------------------------------------
#      METHOD:                       initVars
# DESCRIPTION: Initialize the global variables.
#---------------------------------------------------------------------------------------
initVars() {
  UEFI_FLAG=$(echo 'false' && return 1)
  if [ "${varMap["BOOT-MODE"]}" == "UEFI" ]; then
    UEFI_FLAG=$(echo 'true' && return 0)
  fi

  readarray -t SEL_CHKLIST_OPT <<< "${1//$FORM_FLD_SEP/$'\n'}"

  if [ ${POST_ASSOC_ARRAY["$CHECKLIST_KEY"]+_} ]; then
    readarray -t DESKTOP_ENV_CHECKLIST <<< "${POST_ASSOC_ARRAY["$CHECKLIST_KEY"]//$FORM_FLD_SEP/$'\n'}"
  else
    for menuOpt in "${MENU_OPTS_ARRAY[@]}"; do
      DESKTOP_ENV_CHECKLIST+=(0)
    done
  fi

  local txt="Basic Setup"
  TEXT_SKIP_TASK=$(echo "${TEXT_SKIP_TASK/$txt/${SEL_CHKLIST_OPT[1]}}")

  initDataForStep

  local existFlag=$(isAURPckgInst "cups")
  if ! ${existFlag}; then
    local idx=-1
    updateSoftwareTaskChecklist "DESKTOP_ENV_CHECKLIST" -1 "${MENU_OPTS_ARRAY[${idx}]}"
    unset MENU_OPTS_ARRAY[${idx}]
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                       initDataForStep
# DESCRIPTION: Initialize any data that is necessary (i.e. help text for Video Card Drivers).
#---------------------------------------------------------------------------------------
initDataForStep() {
  local pbTitle=$(printf "%s - %s" "${SEL_CHKLIST_OPT[0]}" "${SEL_CHKLIST_OPT[1]}")
  local pbKeys=()
  local -A pbTitles=()
  local -A methodNames=()
  local initMethodNames=("initDataForVCD" "formatDescsForYAD")

  for methodName in "${initMethodNames[@]}"; do
    ${methodName}
  done

  if [ ${#pbKeys[@]} -gt 0 ]; then
    pbKeys+=("done")
    pbTitles["done"]="Executed ALL the methods to initialize the data for ${pbTitle}!!!!"
    dispStepInitPB "$pbTitle" "Initializing Data"
  fi

  if [ -f "$UPD_ASSOC_ARRAY_FILE" ]; then
    source -- "${UPD_ASSOC_ARRAY_FILE}"
    rm -rf "${UPD_ASSOC_ARRAY_FILE}"

    local vcdStep="${MENU_OPTS_ARRAY[0]}"
    if ! isSoftwareTaskDone "DESKTOP_ENV_CHECKLIST" "$vcdStep" &> /dev/null; then
      setDataForVCD
    fi
  fi

  if [ "$DIALOG" == "yad" ]; then
    overrideDescsForYAD
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                        setDataForVCD
# DESCRIPTION: Set the necessary values for the "Video Card Drivers" task.
#---------------------------------------------------------------------------------------
setDataForVCD() {
  if [ ${updVarMap["${BRANDS[0]}"]+_} ]; then
    for brand in "${BRANDS[@]}"; do
      VCD_DESCS["$brand"]=$(echo "${updVarMap["$brand"]}")
      unset updVarMap["$brand"]
    done
  fi

  readarray -t VIRTUALBOX_PCKGS <<< "${updVarMap["VIRTUALBOX"]//$FORM_FLD_SEP/$'\n'}"
  unset updVarMap["VIRTUALBOX"]

  readarray -t NVIDIA_PROPRIETARY_PCKGS <<< "${updVarMap["NVIDIA_PROPRIETARY"]//$FORM_FLD_SEP/$'\n'}"
  unset updVarMap["NVIDIA_PROPRIETARY"]
  BUMBLEBEE_PCKGS[2]="${NVIDIA_PROPRIETARY_PCKGS[0]}"

  for key in "${!updVarMap[@]}"; do
    VCD_HELP_ARRAY["$key"]=$(echo "${updVarMap[$key]}")
    unset updVarMap["$key"]
  done

  if [ ${POST_ASSOC_ARRAY["VC-BRAND"]+_} ] && [ "${POST_ASSOC_ARRAY["VC-BRAND"]}" == "NVIDIA" ]; then
    local idx=0
    if [ ${#VCD_HELP_ARRAY[@]} -gt 1 ]; then
      unset BRAND_NVIDIA[$idx]
    else
      POST_ASSOC_ARRAY["VC-BRAND"]="NVIDIA Optimus"
    fi
  fi

  if [ "$DIALOG" != "yad" ]; then
    local brand="${BRANDS[0]}"
    local brandDesc="${VCD_DESCS["$brand"]}"
    unset VCD_DESCS["$brand"]
    brand="AMD/ATI"
    BRANDS[0]="$brand"
    VCD_DESCS["$brand"]=$(echo "$brandDesc")
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                       initDataForVCD
# DESCRIPTION: Initialize the necessary values for the "Video Card Drivers" task.
#---------------------------------------------------------------------------------------
initDataForVCD() {
  local firstStep="${MENU_OPTS_ARRAY[0]}"

  if ! isSoftwareTaskDone "DESKTOP_ENV_CHECKLIST" "$firstStep" &> /dev/null; then
    setReqValuesForVideoCard

    local methodNum=$(expr ${#pbKeys[@]} + 1)
    local methodKey=$(printf "$METHOD_KEY_FMT" ${methodNum})
    pbKeys+=("$methodKey")
    pbTitles["$methodKey"]="Setting the data for the Video Card Drivers task...."

    if [ ${POST_ASSOC_ARRAY["VC-BRAND"]+_} ]; then
      methodNames["$methodKey"]="setValsForVCD ${POST_ASSOC_ARRAY["VC-BRAND"]} ${POST_ASSOC_ARRAY["Linux-Kernel"]}"
    else
      methodNames["$methodKey"]="setValsForVCD $BRANDS_HELP_KEY ${POST_ASSOC_ARRAY["Linux-Kernel"]}"
    fi
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                  setReqValuesForVideoCard
# DESCRIPTION: Set the values necessary to determine which drivers need to be installed
#              for the video card.  These should be saved within the global associative
#              array variable "POST_ASSOC_ARRAY".
#---------------------------------------------------------------------------------------
setReqValuesForVideoCard() {
  if [ ! ${POST_ASSOC_ARRAY["VC-BRAND"]+_} ]; then
    local vcBrand=$(getBrandNameOfVC)
    if [ ${#vcBrand} -gt 0 ]; then
      if [[ "$vcBrand" =~ "AMD" ]] || [[ "$vcBrand" =~ "ATI" ]] || [[ "$vcBrand" =~ "Advanced Micro Devices" ]]; then
        POST_ASSOC_ARRAY["VC-BRAND"]="${BRANDS[0]}"
      elif [[ "$vcBrand" =~ "Intel" ]]; then
        POST_ASSOC_ARRAY["VC-BRAND"]="${BRANDS[1]}"
      elif [[ "$vcBrand" =~ "NVIDIA" ]]; then
        POST_ASSOC_ARRAY["VC-BRAND"]="${BRANDS[2]}"
      else
        POST_ASSOC_ARRAY["VC-BRAND"]=$(echo "$vcBrand")
      fi
    fi

    local fileName=$(ls /mnt/boot/initramfs* | sort -r | head -n 1)
    fileName=$(basename "$fileName")
    POST_ASSOC_ARRAY["Linux-Kernel"]=$(echo "$fileName" | sed -e 's/initramfs-\(.*\)\.img/\1/')
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                      getBrandNameOfVC
# DESCRIPTION: Get the brand name of the video card that is installed
#      RETURN: string containing the "Vendor" value of the 'lshw' command.
#---------------------------------------------------------------------------------------
function getBrandNameOfVC() {
  local fileName="/root/vc-brand.txt"

  echo "lshw" > "$SEL_AUR_PCKGS"

  local cmd=$(printf "%s &> %s" "lshw -C display" "$fileName")
  cmd=$(printf "Getting hardware info of video card%s%s" "${FORM_FLD_SEP}" "$cmd") 
  echo -e "$cmd" > "$POST_INST_CMD_FILE"
  showProgBarAndTail
  rm -rf "$SEL_AUR_PCKGS"
  rm -rf "$POST_INST_CMD_FILE"

  fileName="${ROOT_MOUNTPOINT}$fileName"
  local vcBrand=$(grep -R "vendor" "$fileName")
  local splitStr=()
  local sep=": "
  
  readarray -t splitStr <<< "${vcBrand//$sep/$'\n'}"
  if [ ${#splitStr[@]} -gt 1 ]; then
    vcBrand=$(trimString "${splitStr[1]}")
  else
    vcBrand=""
  fi
  rm -rf "$fileName"

  echo "$vcBrand"
}

#---------------------------------------------------------------------------------------
#      METHOD:                        setValsForVCD
# DESCRIPTION: Set the description for the options to be displayed in the dialog to choose
#              the brand name of the video card.
#  Required Params:
#      1)   brandName - the name of the brand for the video card installed
#      2) linuxKernel - linux, linux-hardened, linux-lts, or linux-zen
#---------------------------------------------------------------------------------------
setValsForVCD() {
  local -A updVarMap=()

  if [ "$DIALOG" == "yad" ]; then
    for brand in "${BRANDS[@]}"; do
      local brandDesc="${VCD_DESCS["$brand"]}"
      updVarMap["$brand"]=$(escapeSpecialCharacters "$brandDesc")
    done
  fi

  setHelpTextForVCD "$1"
  setVideoCardDriverOption "$2"

  declare -p updVarMap > "${UPD_ASSOC_ARRAY_FILE}"
}

#---------------------------------------------------------------------------------------
#      METHOD:                      setHelpTextForVCD
# DESCRIPTION: Set the help text to be displayed for each dialog that will be displayed
#              when selecting the Driver & OpenGL packages for the video card.
#  Required Params:
#      1) brandName - the name of the brand for the video card installed
#---------------------------------------------------------------------------------------
setHelpTextForVCD() {
  local helpKeys=()

  case "$1" in
    "${BRANDS[0]}")
      helpKeys=("$1" "${BRAND_AMD_ATI[@]}")
    ;;
    "${BRANDS[1]}"|"VirtualBox"|"VMware")
      helpKeys=("$1")
    ;;
    "${BRANDS[2]}")
      local idx=0
      local cmd="lspci | grep VGA | wc -l"
      local cmdOutput=$(changeRoot "$cmd")
      if [ ${cmdOutput} -gt 1 ]; then
        helpKeys=("${BRAND_NVIDIA[0]}")
      else
        unset BRAND_NVIDIA[$idx]
        helpKeys=("$1" "${BRAND_NVIDIA[@]}")
      fi
    ;;
    *)
      helpKeys=("${BRANDS[0]}" "${BRAND_AMD_ATI[@]}" "${BRANDS[1]}" "${BRANDS[2]}" "${BRAND_NVIDIA[@]}")
      updVarMap["$1"]=$(getHelpTextForBrandOfVC)
    ;;
  esac

  setHelpTextForKeys "helpKeys"
}

#---------------------------------------------------------------------------------------
#      METHOD:                     setHelpTextForKeys
# DESCRIPTION: Set the help text for each key name contained in the array of the calling
#              method.
#  Required Params:
#      1) assocArrayKeys - the name of the array containing the key names set in
#                          the calling method
#---------------------------------------------------------------------------------------
setHelpTextForKeys() {
  local -n assocArrayKeys="$1"
  for key in "${assocArrayKeys[@]}"; do
    case "$key" in
      "${BRANDS[0]}")
        updVarMap["h$key"]=$(getHelpTextForAMD "$1")
      ;;
      "${BRAND_AMD_ATI[0]}")
        updVarMap["h$key"]=$(getHelpTextForAMDGPU "${BRANDS[0]}")
        updVarMap["Vulkan_${BRAND_AMD_ATI[0]}"]=$(getHelpTextForVulkan "${BRAND_AMD_ATI[0]}")
      ;;
      "${BRAND_AMD_ATI[1]}")
        updVarMap["h$key"]=$(getHelpTextForATI "${BRANDS[0]}")
      ;;
      "${BRAND_AMD_ATI[2]}")
        updVarMap["h$key"]=$(getHelpTextForCatalyst "${BRANDS[0]}")
      ;;
      "${BRANDS[1]}")
        updVarMap["h$key"]=$(getHelpTextForIntel "$1")
        updVarMap["Vulkan_$key"]=$(getHelpTextForVulkan "$key")
      ;;
      "${BRANDS[2]}")
        updVarMap["h$key"]=$(getHelpTextForNvidiaOpts "$1")
      ;;
      "${BRAND_NVIDIA[0]}")
        updVarMap["h$key"]=$(getHelpTextForBumblebee "${BRANDS[2]}")
      ;;
      "${BRAND_NVIDIA[1]}")
        updVarMap["h$key"]=$(getHelpTextForNouveau "${BRANDS[2]}")
      ;;
      "${BRAND_NVIDIA[2]}")
        updVarMap["h$key"]=$(getHelpTextForNVIDIA "${BRANDS[2]}")
      ;;
      "VirtualBox")
        updVarMap["$key"]=$(getHelpTextForVirtualBox)
      ;;
      "VMware")
        updVarMap["$key"]=$(getHelpTextForVMware)
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                  setVideoCardDriverOption
# DESCRIPTION: Set the name of the package for the driver to be installed based on the
#              type of linux kernel that was installed.
#  Required Params:
#      1) linuxKernel - linux, linux-hardened, linux-lts, or linux-zen
#---------------------------------------------------------------------------------------
setVideoCardDriverOption() {
  local vbIdx=1
  local nvidiaIdx=0

  case "$1" in
    "linux-hardened")
      nvidiaIdx=1
      vbIdx=0
    ;;
    "linux-lts")
      nvidiaIdx=2
      vbIdx=0
    ;;
    "linux-zen")
      nvidiaIdx=3
      vbIdx=0
    ;;
  esac

  local choices=("${VIRTUALBOX_PCKGS[@]}")
  unset choices[$vbIdx]

  local concatStr=$(printf "$FORM_FLD_SEP%s" "${choices[@]}")
  updVarMap["VIRTUALBOX"]="${concatStr:${#FORM_FLD_SEP}}"

  choices=("${NVIDIA_PROPRIETARY_PCKGS[$nvidiaIdx]}" "${NVIDIA_PROPRIETARY_PCKGS[@]:4}")
  concatStr=$(printf "$FORM_FLD_SEP%s" "${choices[@]}")
  updVarMap["NVIDIA_PROPRIETARY"]="${concatStr:${#FORM_FLD_SEP}}"
}

#---------------------------------------------------------------------------------------
#      METHOD:                      formatDescsForYAD
# DESCRIPTION: Format the descriptions of the menu options by escaping the necessary
#              characters so that they display correctly within a YAD dialog by .
#---------------------------------------------------------------------------------------
formatDescsForYAD() {
  if [ "$DIALOG" == "yad" ]; then
    local methodNum=$(expr ${#pbKeys[@]} + 1)
    local methodKey=$(printf "$METHOD_KEY_FMT" ${methodNum})
    pbKeys+=("$methodKey")
    pbTitles["$methodKey"]="Formatting the descriptions for the YAD dialogs...."
    methodNames["$methodKey"]="escapeDescsForYAD"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                      escapeDescsForYAD
# DESCRIPTION: Escape the necessary characters within the descriptions of the menu
#              options to be displayed within a YAD dialog.
#---------------------------------------------------------------------------------------
escapeDescsForYAD(){
  local arrayNames=("DIT_AUR_DESCS" "KDE_AUR_DESCS" "FONT_AUR_DESCS")
  local descDir="${DATA_DIR}/YAD-Descs"

  mkdir "$descDir"

  for arrayName in "${arrayNames[@]}"; do
    escapeDescStrsForYAD "$arrayName" "${descDir}/${arrayName}.txt"
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                    escapeDescStrsForYAD
# DESCRIPTION: Escape the necessary characters within description elements of the array
#  Required Params:
#    1) srcArray - name of the associative array that contains the descriptions
#    2) destFile - The name of the file to save the values to
#---------------------------------------------------------------------------------------
escapeDescStrsForYAD() {
  local -n srcArray="$1"
  local destFile="$2"
  local -A descAssocArray=()

  for key in "${!srcArray[@]}"; do
    local desc=$(escapeSpecialCharacters "${srcArray["$key"]}")
    descAssocArray["$key"]="$desc"
  done

  declare -p descAssocArray > "${destFile}"
}

#---------------------------------------------------------------------------------------
#      METHOD:                     overrideDescsForYAD
# DESCRIPTION: Override the descriptions of the menu options with the characters that
#              were escaped so that they display correctly within a YAD dialog.
#  Required Params:
#    1) srcArray - name of the associative array that contains the descriptions
#    2) destFile - The description/name of this task
#---------------------------------------------------------------------------------------
overrideDescsForYAD(){
  local arrayNames=("DIT_AUR_DESCS" "KDE_AUR_DESCS" "FONT_AUR_DESCS")
  local descDir="${DATA_DIR}/YAD-Descs"

  for arrayName in "${arrayNames[@]}"; do
    updateDescsForYAD "${descDir}/${arrayName}.txt" "$arrayName"
  done

  rm -rf "$descDir"
}

#---------------------------------------------------------------------------------------
#      METHOD:                      updateDescsForYAD
# DESCRIPTION: Update the description elements within the array
#  Required Params:
#    1)   srcFile - The name of the file that contains the saved values
#    2) destArray - name of the associative array that contains the descs. to update
#---------------------------------------------------------------------------------------
updateDescsForYAD() {
  local srcFile="$1"
  local -n destArray="$2"

  source -- "${srcFile}"

  for key in "${!descAssocArray[@]}"; do
    destArray["$key"]="${descAssocArray["$key"]}"
  done

  unset descAssocArray
}

#---------------------------------------------------------------------------------------
#      METHOD:                       callPostInstMethod
# DESCRIPTION: Calls the method to initialize the data for a step/task within
#              "Step #4:  Post-installation".  This is called by the
#              "dispLoadingDataPF" method.
#---------------------------------------------------------------------------------------
callPostInstMethod() {
  case "$pbKey" in
    "done")
      clog_warn "$pbTitle" >> "$INSTALLER_LOG"
      sleep 0.5
    ;;
    *)
      clog_info "Executing method [$methodName]" >> "$INSTALLER_LOG"
      ${methodName}
      clog_info "Method [$methodName] is DONE" >> "$INSTALLER_LOG"
      methodNum=$(expr $methodNum + 1)
      pbPerc=$(printf "%.0f" $(echo "scale=3; $methodNum / $totalMethods * 100" | bc))
    ;;
  esac
}

#---------------------------------------------------------------------------------------
#      METHOD:                     showInstConfDialog
# DESCRIPTION: Show a confirmation dialog to get confirmation to install the AUR packages
#              being displayed.
#  Required Params:
#    1)        optSel - The value of the option that was selected for this task
#    2)       optDesc - The description/name of this task
#    3) dialogOptions - name of associative array declared in calling method
#---------------------------------------------------------------------------------------
showInstConfDialog() {
  local -n dialogOptions="$3"
  local -A funcParams=(["helpText"]="${dialogOptions["helpText"]}"
                      ["menuOptsRef"]="${dialogOptions["menuOptsRef"]}" ["optDescsRef"]="AUR_PCKG_DESCS")
  local windowTitle=$(printf "%s - %s" "${SEL_CHKLIST_OPT[0]}" "${SEL_CHKLIST_OPT[1]}")
  local dialogTitle=$(printf "%s - %s" "$1" "$2")

  if [ ${dialogOptions["optDescsRef"]+_} ]; then
    funcParams["optDescsRef"]="${dialogOptions["optDescsRef"]}"
  fi

  if [ "$DIALOG" == "yad" ]; then
    funcParams["image"]="$YAD_GRAPHICAL_PATH/images/post-inst/${dialogOptions["image"]}"
    funcParams["dialogTitle"]="$windowTitle"
    funcParams["helpDlgTitle"]="$dialogTitle"
    if [ ${dialogOptions["buttons"]+_} ]; then
      funcParams["buttons"]="${dialogOptions["buttons"]}"
    fi
  else
    funcParams["dialogBackTitle"]="$windowTitle"
    funcParams["dialogText"]="${dialogOptions["dialogText"]}"
    funcParams["dialogTitle"]="$dialogTitle"
  fi

  showConfirmationDialog "funcParams"
  cleanDialogFiles
}

#---------------------------------------------------------------------------------------
#      METHOD:                   installVideoCardDrivers
# DESCRIPTION: Install the group of AUR packages for the video driver
#  Required Params:
#    1)  optSel - the value of the option that was selected for this task
#    2) optDesc - the description/name of this task
#---------------------------------------------------------------------------------------
installVideoCardDrivers() {
  local flagVal=0
  local vcBrandName=""

  if [ ${POST_ASSOC_ARRAY["VC-BRAND"]+_} ]; then
    vcBrandName="${POST_ASSOC_ARRAY["VC-BRAND"]}"
  fi

  case "$vcBrandName" in
    "${BRANDS[0]}")    #  AMD / ATI
      selectTypeForBrand "$1" "$2" "Skip"
    ;;
    "${BRANDS[1]}"|"NVIDIA Optimus"|"VirtualBox"|"VMware")
      showInstConfDialogForVCD  "$1" "$2" "$vcBrandName"
    ;;
    "${BRANDS[2]}")    #  NVIDIA
      selectTypeForBrand "$1" "$2" "Skip"
    ;;
    *)
      selectBrandOfVideoCard "$1" "$2"
    ;;
  esac

  if [ ${POST_ASSOC_ARRAY["VC-TYPE"]+_} ]; then
    setDriverAndOpenGLPckgs
    appendPckgsForVulkan
    setCommandsForVCD

    showProgBarAndTail
    rm -rf "$SEL_AUR_PCKGS"
    rm -rf "$POST_INST_CMD_FILE"

    if [ "${BRAND_AMD_ATI[2]}" == "${POST_ASSOC_ARRAY["VC-TYPE"]}" ]; then
      flagVal=-1
      updateSoftwareTaskChecklist "${piAssocArray["chklArray"]}" ${flagVal} "${MENU_OPTS_ARRAY[1]}"
    fi
    flagVal=1
  else
    flagVal=-1
  fi

  updateSoftwareTaskChecklist "${piAssocArray["chklArray"]}" ${flagVal} "$1"
}

#---------------------------------------------------------------------------------------
#      METHOD:                  showInstConfDialogForVCD
# DESCRIPTION: Show a confirmation dialog for the Video Card Driver that the installer
#              found to be installed.
#  Required Params:
#    1)      optSel - The value of the option that was selected for this task
#    2)     optDesc - The description/name of this task
#    3) vcBrandName - name of the brand for the installed video card
#---------------------------------------------------------------------------------------
showInstConfDialogForVCD() {
  local vcBrandName="$3"
  local -A vcdDlgOpts=()

  case "$vcBrandName" in
    "${BRANDS[1]}")    #  Intel
      vcdDlgOpts=(["helpText"]="${VCD_HELP_ARRAY["h$vcBrandName"]}" ["image"]="intel.png"
                  ["menuOptsRef"]="BRAND_INTEL" ["optDescsRef"]="VCD_DESCS")
    ;;
    "NVIDIA Optimus")  #  Bumblebee
      vcdDlgOpts=(["helpText"]="${VCD_HELP_ARRAY["h$vcBrandName"]}" ["image"]="bumblebee.png"
                  ["menuOptsRef"]="BUMBLEBEE_PCKGS" ["optDescsRef"]="VCD_DESCS")
    ;;
    "VirtualBox")
      vcdDlgOpts=(["helpText"]="${VCD_HELP_ARRAY["$vcBrandName"]}" ["image"]="virtualbox.png"
                  ["menuOptsRef"]="VIRTUALBOX_PCKGS" ["optDescsRef"]="VCD_DESCS")
    ;;
    *)                 #  VMware
      vcdDlgOpts=(["helpText"]="${VCD_HELP_ARRAY["$vcBrandName"]}" ["image"]="vmware.png"
                  ["menuOptsRef"]="VMWARE_PCKGS" ["optDescsRef"]="VCD_DESCS")
    ;;
  esac

  showInstConfDialog "$1" "$2" "vcdDlgOpts"

  if ${yesNoFlag}; then
    case "$vcBrandName" in
      "NVIDIA Optimus")
        POST_ASSOC_ARRAY["VC-TYPE"]="${BRAND_NVIDIA[0]}"
      ;;
      "${BRANDS[1]}")
        POST_ASSOC_ARRAY["VC-TYPE"]="Intel Graphics"
      ;;
      "VirtualBox")
        POST_ASSOC_ARRAY["VC-TYPE"]="Oracle VB"
      ;;
      "VMware")
        POST_ASSOC_ARRAY["VC-TYPE"]="VMware App"
      ;;
    esac
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                   selectBrandOfVideoCard
# DESCRIPTION: Select the brand for the video card installed.
#  Required Params:
#    1)  optSel - the value of the option that was selected for this task
#    2) optDesc - the description/name of this task
#---------------------------------------------------------------------------------------
selectBrandOfVideoCard() {
  local -A funcParams=(["defaultChoice"]="${BRANDS[0]}" ["helpText"]="${VCD_HELP_ARRAY["$BRANDS_HELP_KEY"]}"
                      ["menuOptsRef"]="BRANDS" ["optDescsRef"]="VCD_DESCS")

  setOptsForRadioListDialog "$1" "$2" "funcParams"

  while true; do
    showRadioListDialog "funcParams"
    cleanDialogFiles
    if [ ${#mbSelVal} -gt 0 ]; then
      POST_ASSOC_ARRAY["VC-BRAND"]=$(echo "$mbSelVal")
      case "$mbSelVal" in
        "${BRANDS[0]}"|"${BRANDS[-1]}")  # "AMD / ATI" or "NVIDIA"
          selectTypeForBrand "$1" "$2" "Cancel"
          cleanDialogFiles
          if [ ${POST_ASSOC_ARRAY["VC-TYPE"]+_} ]; then
            break
          fi
        ;;
        "${BRANDS[1]}")  # Intel
          local -A vcdDlgOpts=(["helpText"]="${VCD_HELP_ARRAY["h$mbSelVal"]}" ["image"]="intel.png"
                              ["menuOptsRef"]="BRAND_INTEL" ["optDescsRef"]="VCD_DESCS"
                              ["buttons"]="Confirm${FORM_FLD_SEP}Cancel${FORM_FLD_SEP}Help")
          local vcType="Intel Graphics"
          vcdDlgOpts["dialogText"]=$(echo "Confirm to configure & install the Drivers & OpenGL packages listed below for the type '$vcType'")
          showInstConfDialog "$1" "$2" "vcdDlgOpts"
          if ${yesNoFlag}; then
            POST_ASSOC_ARRAY["VC-TYPE"]="$vcType"
            break
          fi
        ;;
      esac
    else
      unset POST_ASSOC_ARRAY["VC-BRAND"]
      break
    fi
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                     selectTypeForBrand
# DESCRIPTION: Select the type of Driver & OpenGL packages for a brand, which is just
#              currently for "AMD / ATI" & "NVIDIA".  Set the option in the global
#              variable name "mbSelVal"
#  Required Params:
#    1)      optSel - the value of the option that was selected for this task
#    2)     optDesc - the description/name of this task
#    3) cancelLabel - Skip or Cancel
#---------------------------------------------------------------------------------------
selectTypeForBrand() {
  local -A brandTypesOpts=(["optDescsRef"]="VCD_DESCS")
  local -A vcdDlgOpts=(["optDescsRef"]="VCD_DESCS" ["buttons"]="Confirm${FORM_FLD_SEP}Cancel${FORM_FLD_SEP}Help")

  setOptsForBrandTypeDialog "$1" "$2" "$3" "brandTypesOpts"

  while true; do
    mbSelVal=""
    showRadioListDialog "brandTypesOpts"

    if [ ${#mbSelVal} -gt 0 ]; then
      vcdDlgOpts["helpText"]="${VCD_HELP_ARRAY["h$mbSelVal"]}"
      yesNoFlag=$(echo 'true' && return 0)
      case "$mbSelVal" in
        "${BRAND_AMD_ATI[0]}")
          vcdDlgOpts["image"]="amdgpu.png"
          vcdDlgOpts["menuOptsRef"]="AMDGPU_PCKGS"
        ;;
        "${BRAND_AMD_ATI[1]}")
          vcdDlgOpts["image"]="ati.png"
          vcdDlgOpts["menuOptsRef"]="ATI_PCKGS"
        ;;
        "${BRAND_AMD_ATI[2]}")
          vcdDlgOpts["image"]="amd-catalyst.png"
          vcdDlgOpts["menuOptsRef"]="CATALYST_PCKGS"
          showCatalystDriverWarning "$1" "$2"
        ;;
        #### NOTE:  Have to use hardcoded values for NVIDIA because the size of the array could change
        "Bumblebee")
          vcdDlgOpts["image"]="bumblebee.png"
          vcdDlgOpts["menuOptsRef"]="BUMBLEBEE_PCKGS"
        ;;
        "Nouveau")
          vcdDlgOpts["image"]="nouveau.png"
          vcdDlgOpts["menuOptsRef"]="NOUVEAU_PCKGS"
        ;;
        "NVIDIA Proprietary")
          vcdDlgOpts["image"]="nvidia.png"
          vcdDlgOpts["menuOptsRef"]="NVIDIA_PROPRIETARY_PCKGS"
        ;;
        *)
          clog_error "Method [${FUNCNAME[0]}]:  Undefined option [$mbSelVal]"
          break
        ;;
      esac

      if ${yesNoFlag}; then
        local vcType="$mbSelVal"
        if [ "$DIALOG" != "yad" ]; then
          vcdDlgOpts["dialogText"]=$(echo "Confirm to configure & install the Drivers & OpenGL packages listed below for the type '$vcType'")
        fi

        showInstConfDialog "$1" "$2" "vcdDlgOpts"
        if ${yesNoFlag}; then
          POST_ASSOC_ARRAY["VC-TYPE"]="$vcType"
          break
        fi
      fi
    else
      break
    fi
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                  showCatalystDriverWarning
# DESCRIPTION: Show a warning message about what happens when installing the "AMD Catalyst"
#              type Driver & OpenGL packages and get confirmation of whether or not
#              to continue.
#  Required Params:
#    1)      optSel - the value of the option that was selected for this task
#    2)     optDesc - the description/name of this task
#---------------------------------------------------------------------------------------
showCatalystDriverWarning() {
  local -A funcParams=()
  local windowTitle=$(printf "%s - %s" "$1" "$2")
  local xorgTask="${MENU_OPTS_ARRAY[1]}"
  local dialogTitle=$(echo "Skipping of '${xorgTask} - ${DESC_ASSOC_ARRAY["$xorgTask"]}' task!")
  local msgArray=("Catalyst is no longer updated by AMD and does not support the latest"
    "Xorg.  As such, installing the lastest version of Xorg will break compatibility for the"
    "Driver & OpenGL packages.  This means that the '${xorgTask} - ${DESC_ASSOC_ARRAY["$xorgTask"]}'"
    "task will be 'Skipped' and the backported/older version packages of Xorg will be installed!")
  local concatStr=$(printf " %s" "${msgArray[@]}")

  local extraDashes=""

  if [ "$DIALOG" == "yad" ]; then
    extraDashes="---------"
  fi

  msgArray=(" " "${concatStr:1}" "${DIALOG_BORDER}${extraDashes}" " "
    "Are you sure you want to continue to install the '${BRAND_AMD_ATI[2]}' type Driver & OpenGL packages?")
  funcParams["dialogText"]=$(getTextForDialog "${msgArray[@]}")

  if [ "$DIALOG" == "yad" ]; then
    funcParams["dialogTitle"]="$windowTitle"
    funcParams["dialogTextHdr"]="$dialogTitle"
  else
    funcParams["dialogBackTitle"]="$windowTitle"
    funcParams["dialogTitle"]=$(echo "WARNING: $dialogTitle")
  fi

  showDesktopConfWarning "funcParams"
}

#---------------------------------------------------------------------------------------
#      METHOD:                  setOptsForBrandTypeDialog
# DESCRIPTION: Set the options for the Linux or YAD dialogs to display for the types
#              of Driver & OpenGL options available.
#  Required Params:
#    1)        optSel - the value of the option that was selected for this task
#    2)       optDesc - the description/name of this task
#    3)   cancelLabel - Skip or Cancel
#    4) dlgAssocArray - name of the associative array declared in the calling function
#---------------------------------------------------------------------------------------
setOptsForBrandTypeDialog() {
  local -n dlgAssocArray="$4"

  dlgAssocArray["dialogTitle"]=$(printf "%s - %s" "$1" "$2")
  case "${POST_ASSOC_ARRAY["VC-BRAND"]}" in
    "${BRANDS[0]}")
      dlgAssocArray["helpText"]="${VCD_HELP_ARRAY["h$mbSelVal"]}"
      dlgAssocArray["menuOptsRef"]="BRAND_AMD_ATI"
    ;;
    *)
      dlgAssocArray["helpText"]="${VCD_HELP_ARRAY["h$mbSelVal"]}"
      dlgAssocArray["menuOptsRef"]="BRAND_NVIDIA"
    ;;
  esac

  if [ "$DIALOG" == "yad" ]; then
    dlgAssocArray["cancelLabel"]="$3"
    case "${POST_ASSOC_ARRAY["VC-BRAND"]}" in
      "${BRANDS[0]}")
        dlgAssocArray["image"]="${YAD_GRAPHICAL_PATH}/images/post-inst/amd-ati.png"
      ;;
      *)
        dlgAssocArray["image"]="${YAD_GRAPHICAL_PATH}/images/post-inst/nvidia-choices.png"
      ;;
    esac
    local cols=("Radio Btn" "Type" "Description")
    local concatStr=$(printf "$FORM_FLD_SEP%s" "${cols[@]}")
    dlgAssocArray["columnNames"]=${concatStr:${#FORM_FLD_SEP}}
  else
    dlgAssocArray["cancel-label"]="$3"
    dlgAssocArray["dialogBackTitle"]=$(printf "%s - %s" "${SEL_CHKLIST_OPT[0]}" "${SEL_CHKLIST_OPT[1]}")
    dlgAssocArray["dialogText"]=$(echo "Choose the type of Driver & OpenGL packages for the '${POST_ASSOC_ARRAY["VC-BRAND"]}' video card:")
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                   setDriverAndOpenGLPckgs
# DESCRIPTION: Set the Driver & OpenGL packages to be installed based on the value set
#              in the global associative array "POST_ASSOC_ARRAY" with key "VC-TYPE"
#---------------------------------------------------------------------------------------
setDriverAndOpenGLPckgs() {
  local vcdPckgs=()
  case "${POST_ASSOC_ARRAY["VC-TYPE"]}" in
    "${BRAND_AMD_ATI[0]}")
      vcdPckgs=("${AMDGPU_PCKGS[@]}")
    ;;
    "${BRAND_AMD_ATI[1]}")
      vcdPckgs=("${ATI_PCKGS[@]}")
    ;;
    "${BRAND_AMD_ATI[2]}")
      vcdPckgs=("${CATALYST_PCKGS[@]}")
    ;;
    #### NOTE:  Have to use hardcoded values for NVIDIA because the size of the array could change
    "Bumblebee")
      vcdPckgs=("${BUMBLEBEE_PCKGS[@]}")
    ;;
    "Nouveau")
      vcdPckgs=("${NOUVEAU_PCKGS[@]}")
    ;;
    "NVIDIA Proprietary")
      vcdPckgs=("${NVIDIA_PROPRIETARY_PCKGS[@]}")
    ;;
    "Intel Graphics")
      vcdPckgs=("${BRAND_INTEL[@]}")
    ;;
    "Oracle VB")
      vcdPckgs=("${VIRTUALBOX_PCKGS[@]}")
    ;;
    "VMware App")
      vcdPckgs=("${VMWARE_PCKGS[@]}")
    ;;
    *)
      clog_error "Method [${FUNCNAME[0]}]:  Undefined option [${POST_ASSOC_ARRAY["VC-TYPE"]}]"
      exit 1
    ;;
  esac

  local concatStr=$(printf " %s" "${vcdPckgs[@]}")
  echo -e "${concatStr:1}" > "$SEL_AUR_PCKGS"
}

#---------------------------------------------------------------------------------------
#      METHOD:                    appendPckgsForVulkan
# DESCRIPTION: Append the packages to support Vulkan to the Driver & OpenGL packages.
#---------------------------------------------------------------------------------------
appendPckgsForVulkan() {
  local vulkanPckgs=()
  local idx=0
  local helpKey=""
  local img=""
  local -A vulkanDlgOpts=(["menuOptsRef"]="vulkanPckgs" ["optDescsRef"]="VCD_DESCS"
                      ["buttons"]="Confirm${FORM_FLD_SEP}Cancel${FORM_FLD_SEP}Help")
  case "${POST_ASSOC_ARRAY["VC-TYPE"]}" in
    "${BRAND_AMD_ATI[0]}")
      helpKey="Vulkan_${BRAND_AMD_ATI[0]}"
      idx=-2
      img="amd-vulkan.png"
    ;;
    "Intel Graphics")
      helpKey="Vulkan_${POST_ASSOC_ARRAY["VC-BRAND"]}"
      idx=-1
      img="intel-vulkan.png"
    ;;
  esac

  if [ ${idx} -lt 0 ]; then
    vulkanPckgs=("${VULKAN_PCKGS[@]}")
    unset vulkanPckgs[$idx]
    vulkanDlgOpts["helpText"]="${VCD_HELP_ARRAY["$helpKey"]}"
    vulkanDlgOpts["image"]="$img"

    showInstConfDialog "$1" "$2" "vulkanDlgOpts"
    if ${yesNoFlag}; then
      local concatStr=$(printf " %s" "${vulkanPckgs[@]}")
      echo -e "${concatStr:1}" >> "$SEL_AUR_PCKGS"
    fi
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                      setCommandsForVCD
# DESCRIPTION: Set the commands that need to be executed for the Video Card Driver step
#---------------------------------------------------------------------------------------
setCommandsForVCD() {
  local vcdModules=()
  local userName="${POST_ASSOC_ARRAY["UserName"]}"
  local linuxKernel="${POST_ASSOC_ARRAY["Linux-Kernel"]}"
  local regenCmd="Regenerating the initramfs (Initial RAM File System)${FORM_FLD_SEP}mkinitcpio -p $linuxKernel"
  local vcdCmds=()
  case "${POST_ASSOC_ARRAY["VC-TYPE"]}" in
    "${BRAND_AMD_ATI[0]}")
      vcdModules=("amdgpu")
      setLoadModuleCmds "vcdModules"
    ;;
    "${BRAND_AMD_ATI[1]}")
      vcdModules=("radeon")
      setLoadModuleCmds "vcdModules"
    ;;
    "${BRAND_AMD_ATI[2]}")
      setKernelParamCmds "nomodeset"
      vcdCmds=("Enabling the 'atieventsd' service${FORM_FLD_SEP}systemctl enable atieventsd.service"
        "Enabling the 'catalyst-hook' service${FORM_FLD_SEP}systemctl enable catalyst-hook.service"
        "Enabling the 'temp-links-catalyst' service${FORM_FLD_SEP}systemctl enable temp-links-catalyst.service"
        "Configuring X${FORM_FLD_SEP}aticonfig --initial" "$regenCmd")
    ;;
    #### NOTE:  Have to use hardcoded values for NVIDIA because the size of the array could change
    "Bumblebee")
      vcdCmds=(
        "Adding user '$userName' to group 'bumblebee'${FORM_FLD_SEP}usermod -aG bumblebee $userName" "$regenCmd")
    ;;
    "Nouveau")
      vcdModules=("nouveau")
      setLoadModuleCmds "vcdModules"
    ;;
    "NVIDIA Proprietary")
      setKernelParamCmds "nvidia-drm.modeset=1"
      vcdModules=("nvidia" "nvidia_modeset" "nvidia_uvm" "nvidia_drm")
      setLoadModuleCmds "vcdModules"
      local nvidiaCmdTitle="Create an Xorg server configuration file"
      local nvidiaCmd="nvidia-xconfig --add-argb-glx-visuals --allow-glx-with-composite --composite --render-accel"
      vcdCmds=("${nvidiaCmdTitle}${FORM_FLD_SEP}${nvidiaCmd}")
    ;;
    "Intel Graphics")
      vcdModules=("i915")
      setLoadModuleCmds "vcdModules"
    ;;
    "Oracle VB")
      # These modules get loaded by enabling the service:  vboxguest vboxsf vboxvideo
      vcdCmds=("Enabling the VirtualBox service${FORM_FLD_SEP}systemctl enable vboxservice.service"
        "Adding user '$userName' to group 'vboxsf'${FORM_FLD_SEP}usermod -aG vboxsf $userName" "$regenCmd")
    ;;
    "VMware App")
      vcdCmds=("Setting 'arch-release'${FORM_FLD_SEP}cat /proc/version > /etc/arch-release"
        "Enabling the VMware Fuse service${FORM_FLD_SEP}systemctl enable vmware-vmblock-fuse.service"
        "Enabling the VMware Tools service${FORM_FLD_SEP}systemctl enable vmtoolsd.service" "$regenCmd")
    ;;
    *)
      clog_error "Method [${FUNCNAME[0]}]:  Undefined option [${POST_ASSOC_ARRAY["VC-TYPE"]}]"
      exit 1
    ;;
  esac

  if [ ${#vcdCmds[@]} -gt 0 ]; then
    local concatStr=$(printf "\n%s" "${vcdCmds[@]}")
    case "${POST_ASSOC_ARRAY["VC-TYPE"]}" in
      "${BRAND_AMD_ATI[2]}"|"NVIDIA Proprietary")
        echo -e "${concatStr:1}" >> "$POST_INST_CMD_FILE"
      ;;
      *)
        echo -e "${concatStr:1}" > "$POST_INST_CMD_FILE"
      ;;
    esac
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                      setLoadModuleCmds
# DESCRIPTION: Set the commands that need to be executed to add the required module(s)
#              for the video driver to the MODULES array in "/etc/mkinitcpio.conf"
#---------------------------------------------------------------------------------------
setLoadModuleCmds() {
  local -n kernelModules="$1"
  local destFile="/etc/mkinitcpio.conf"
  local filePath="${ROOT_MOUNTPOINT}$destFile"
  local lineNum=$(sed -n '/^MODULES/=' "$filePath")
  local modulesLine=$(sed -n "${lineNum}p" < "$filePath")
  eval "$modulesLine"
  MODULES+=("${kernelModules[@]}")
  local concatStr=$(printf " %s" "${MODULES[@]}")
  concatStr="MODULES=(${concatStr:1})"
  local cmd="sed -i -e 's/'\"$modulesLine\"'/'\"$concatStr\"'/g' \"$destFile\""
  local cmds=(
    "Adding the '${kernelModules[@]}' for the '${POST_ASSOC_ARRAY["VC-TYPE"]}' type${FORM_FLD_SEP}${cmd}"
    "Regenerating the initramfs (Initial RAM File System)${FORM_FLD_SEP}mkinitcpio -p ${POST_ASSOC_ARRAY["Linux-Kernel"]}"
  )
  unset MODULES

  local concatStr=$(printf "\n%s" "${cmds[@]}")
  if [ -f "$POST_INST_CMD_FILE" ]; then
    echo -e "${concatStr:1}" >> "$POST_INST_CMD_FILE"
  else
    echo -e "${concatStr:1}" > "$POST_INST_CMD_FILE"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                     setKernelParamCmds
# DESCRIPTION: Set the commands that need to be executed for adding the appropriate
#              kernel parameters to the installed bootloader's configuration file.
#  Required Params:
#      1) kernelParams - the name of the array containing the names of the modules
#---------------------------------------------------------------------------------------
setKernelParamCmds() {
  local kernelParams="$1"
  local destFile=$(getFilePathForBootloader)
  local filePath="${ROOT_MOUNTPOINT}$destFile"
  local cmds=()
  local cmdOutput=$(getCmdOutForKernelParam "$filePath")
  local lineNums=()
  local repStr=""

  readarray -t lineNums <<< "$cmdOutput"

  for lineNum in "${lineNums[@]}"; do
    local line=$(sed -n "${lineNum}p" < "$filePath")
    local lastPos=$(expr ${#line} - 1)
    local lastChar=$(echo "${line:${lastPos}}")
    local cmdTitle="Adding '$kernelParams' to line #${lineNum} of the config file for '${varMap["BOOT-LOADER"]}'"
    if [ "$lastChar" == '"' ]; then
      repStr=$(echo "${line:0:${lastPos}}")
      repStr=$(printf "%s %s\"" "$repStr" "$kernelParams")
      line=${line//\"/\\\"}
      repStr=${repStr//\"/\\\"}
    else
      repStr=$(printf "%s %s" "$line" "$kernelParams")
    fi

    local cmd="sed -i -e 's/'\"$line\"'/'\"$repStr\"'/g' \"$destFile\""
    cmds+=("${cmdTitle}${FORM_FLD_SEP}${cmd}")
  done

  if [ "${varMap["BOOT-LOADER"]}" == "Grub2" ]; then
    cmds+=("Updating configuration file for 'Grub2'${FORM_FLD_SEP}grub-mkconfig -o /boot/grub/grub.cfg")
  fi

  local concatStr=$(printf "\n%s" "${cmds[@]}")
  echo -e "${concatStr:1}" > "$POST_INST_CMD_FILE"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getFilePathForBootloader
# DESCRIPTION: Get the absolute path to the installed bootloader's configuration file.
#      RETURN: string containing the absolute path
#---------------------------------------------------------------------------------------
function getFilePathForBootloader() {
  local destFile=""

  case "${varMap["BOOT-LOADER"]}" in
    "Grub2")
      destFile="/etc/default/grub"
    ;;
    "rEFInd")
      destFile="/boot/refind_linux.conf"
    ;;
    "Syslinux")
      if ${UEFI_FLAG}; then
        destFile="${EFI_MNT_PT}/EFI/syslinux/syslinux.cfg"
      else
        destFile="/boot/syslinux/syslinux.cfg"
      fi
    ;;
    "systemd-boot")
      destFile="/boot/loader/entries/arch.conf"
    ;;
  esac

  echo "$destFile"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                   getCmdOutForKernelParam
# DESCRIPTION: Get the output (line numbers) of the command that searched for the lines
#              within the installed bootloader's configuration file that require the
#              kernel parameters to be appended to..
#      RETURN: string containing line numbers separated by a carriage return
#  Required Params:
#    1) filePath - "/mnt" + value returned by the "getFilePathForBootloader" function
#---------------------------------------------------------------------------------------
function getCmdOutForKernelParam() {
  local filePath="$1"
  local cmdOutput=""

  case "${varMap["BOOT-LOADER"]}" in
    "Grub2")
      cmdOutput=$(sed -n '/^[[:space:]]*GRUB_CMDLINE_LINUX_DEFAULT/=' "$filePath")
    ;;
    "rEFInd")
      cmdOutput=$(sed -n '/^[[:space:]]*"Boot using default options"/=' "$filePath")
    ;;
    "Syslinux")
      cmdOutput=$(sed -n '/^[[:space:]]*APPEND/=' "$filePath")
    ;;
    "systemd-boot")
      cmdOutput=$(sed -n '/^[[:space:]]*options/=' "$filePath")
    ;;
  esac

  echo "$cmdOutput"
}

#---------------------------------------------------------------------------------------
#      METHOD:                     execDesktopEnvTask
# DESCRIPTION: Execute the task that was selected
#  Required Params:
#    1)  optSel - the option that was selected
#    2) optDesc - the description/name of the option that was selected
#---------------------------------------------------------------------------------------
execDesktopEnvTask() {
  local optSel="$1"
  local optDesc="$2"
  local doneFlag=$(isSoftwareTaskDone "${piAssocArray["chklArray"]}" "$optSel")

  if ! ${doneFlag}; then
    if [[ "$optDesc" =~ "amp;" ]]; then
      optDesc=$(echo "${optDesc/amp;/}")
    fi

    case "$optDesc" in
      "Administer CUPS")
        installAdminCUPS "$optSel" "$optDesc"
      ;;
      "Application Launchers")
        installAppLaunchers "$optSel" "$optDesc"
      ;;
      "Application Menu Editors")
        installAppMenuEditors "$optSel" "$optDesc"
      ;;
      "Composite Manager")
        installCompMan "$optSel" "$optDesc"
      ;;
      "DE or WM")
        installDesktopEnvOrWinMan "$optSel" "$optDesc"
      ;;
      "Desktop & Icon Themes")
        installDeskIconThemes "$optSel" "$optDesc"
      ;;
      "Display Manager")
        installDispMan "$optSel" "$optDesc"
      ;;
      "Fonts")
        installFonts "$optSel" "$optDesc"
      ;;
      "Taskbars")
        installTaskbars "$optSel" "$optDesc"
      ;;
      "Video Card Drivers")
        installVideoCardDrivers "$optSel" "$optDesc"
      ;;
      "Xorg")
        installXorg "$optSel" "$optDesc"
      ;;
      *)
        clog_error "No case statement defined for tabOpt: [$optSel]:  [$optDesc]"
        exit 1
      ;;
    esac
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                         installXorg
# DESCRIPTION: Install the group of packages for Xorg
#  Required Params:
#    1)  optSel - the value of the option that was selected for this task
#    2) optDesc - the description/name of this task
#---------------------------------------------------------------------------------------
installXorg() {
  local idx=-1
  local existFlag=$(isAURPckgInst "adobe-source-code-pro-fonts")
  if ${existFlag}; then
    unset XORG[$idx]
  fi
  local flagVal=0
  local -A funcAssocParams=(["helpText"]=$(getHelpTextForXorg) ["menuOptsRef"]="XORG")
  local windowTitle=$(printf "%s - %s" "${SEL_CHKLIST_OPT[0]}" "${SEL_CHKLIST_OPT[1]}")
  local dialogTitle=$(printf "%s - %s" "$1" "$2")

  funcAssocParams["image"]="xorg.png"
  funcAssocParams["dialogText"]="Confirm to install the listed packages below for the 'Xorg' group:"
  while true; do
    showInstConfDialog "$1" "$2" "funcAssocParams"

    if ${yesNoFlag}; then
      flagVal=1
      break
    else
      showSkipXorgWarning "$1" "$2"
      if ${yesNoFlag}; then
        flagVal=-1
        break
      fi
    fi
  done

  if [ ${flagVal} -lt 0 ]; then
    local startIdx=$(findPositionForString MENU_OPTS_ARRAY "${MENU_OPTS_ARRAY[1]}")
    local lastIdx=$(expr ${#MENU_OPTS_ARRAY[@]} - 1)
    for aryIdx in $(eval echo "{${startIdx}..${lastIdx}}"); do
      updateSoftwareTaskChecklist "${piAssocArray["chklArray"]}" ${flagVal} "${MENU_OPTS_ARRAY[${aryIdx}]}"
    done
  else
    local concatStr=$(printf " %s" "${XORG[@]}")
    echo -e "${concatStr:1}" > "$SEL_AUR_PCKGS"
    showProgBarAndTail
    rm -rf "$SEL_AUR_PCKGS"
    updateSoftwareTaskChecklist "${piAssocArray["chklArray"]}" ${flagVal} "$1"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                     showSkipXorgWarning
# DESCRIPTION: Show a warning message about what happens when skipping the installation
#              Xorg and get confirmation of whether or not to continue.
#  Required Params:
#    1)      optSel - the value of the option that was selected for this task
#    2)     optDesc - the description/name of this task
#---------------------------------------------------------------------------------------
showSkipXorgWarning() {
  local -A funcParams=()
  local windowTitle=$(printf "%s - %s" "$1" "$2")
  local dialogTitle=$(echo "Skipping installation of Xorg!")
  local msgArray=("Graphical User Interfaces (GUIs) are powerful tools that unlock the"
    "graphical abilities of a workstation.  Xorg provides an interface between the"
    "hardware, the graphical software and input devices.  In addition to that, it is also"
    "fully network-aware, which means that one can run an application on one system while"
    "viewing it on a different one.")
  local concatStr=$(printf " %s" "${msgArray[@]}")
  local paragraphTwoArray=("None of this will be possible, including running within a 'Desktop Environment'"
    "(DE) or standalone 'Window Manager' (WM), without having Xorg installed.  Therefore, not only"
    "will the installation of Xorg be skipped, but also the remaining tasks for '${SEL_CHKLIST_OPT[0]}"
    "- ${SEL_CHKLIST_OPT[1]}' and the remaining software tasks of the 'Step#4:  Post Installation' step. "
    "The installer will then return back to the installation guide.")
  local pTwo=$(printf " %s" "${paragraphTwoArray[@]}")
  local dlgBorder="$DIALOG_BORDER"

  if [ "$DIALOG" == "yad" ]; then
    dlgBorder="---------------------------------------------------------------------"
  fi

  msgArray=(" " "${concatStr:1}" " " "${pTwo:1}" "$dlgBorder" " "
    "Are you sure you want to continue to skip installing 'Xorg'?")
  funcParams["dialogText"]=$(getTextForDialog "${msgArray[@]}")

  if [ "$DIALOG" == "yad" ]; then
    funcParams["dialogTitle"]="$windowTitle"
    funcParams["dialogTextHdr"]="$dialogTitle"
  else
    funcParams["dialogBackTitle"]="$windowTitle"
    funcParams["dialogTitle"]=$(echo "WARNING: $dialogTitle")
  fi

  showDesktopConfWarning "funcParams"
  cleanDialogFiles
}

#---------------------------------------------------------------------------------------
#      METHOD:                  installDesktopEnvOrWinMan
# DESCRIPTION: Install the group of packages for either a "Desktop Environment"
#              or standalone "Window Manager"
#  Required Params:
#    1)  optSel - the value of the option that was selected for this task
#    2) optDesc - the description/name of this task
#---------------------------------------------------------------------------------------
installDesktopEnvOrWinMan() {
  local deWM=""
  local -A funcAssocParams=(["helpText"]=$(getHelpTextForDEorWM))
  local windowTitle=$(printf "%s - %s" "${SEL_CHKLIST_OPT[0]}" "${SEL_CHKLIST_OPT[1]}")
  local taskCats=()
  local aryKey=""
  local selSubCat=""
  local gnomefbName=""
  readarray -t taskCats <<< "${SUB_CAT_ASSOC_ARRAY["$2"]//$FORM_FLD_SEP/$'\n'}"

  mbSelVal=""
  while true; do
    setOptsForDesktopEnvOrWinMan "$1" "$2" "funcAssocParams" "$deWM"
    case "$deWM" in
      "Desktop Environment")
        selectFromRadioListDialog "funcAssocParams"
        if [ ${#mbSelVal} -lt 1 ]; then
          deWM=""
        elif [ "$mbSelVal" == "GNOME Flashback" ]; then
          showDialogForFlashbackDE
          gnomefbName="$mbSelVal"
          if [ ${#mbSelVal} -lt 1 ]; then
            deWM=""
            gnomefbName=""
          fi
        fi
        TAB_SEL_VALS=()
      ;;
      "Window Manager")
        showWinManDialog "funcAssocParams"
        if [ ${#TAB_SEL_VALS[@]} -lt 1 ]; then
          deWM=""
        fi
        mbSelVal=""
      ;;
      *)
        selectFromRadioListDialog "funcAssocParams"
        if [ ${#mbSelVal} -gt 0 ]; then
          cleanDialogFiles
          selSubCat="$mbSelVal"
          deWM="${DESC_ASSOC_ARRAY["$mbSelVal"]}"
          POST_ASSOC_ARRAY["DE-OR-WM"]="$deWM"
          continue
        else
          resetValsForDesktopEnvOrWinMan
          break
        fi
      ;;
    esac

    if [ ${#deWM} -gt 0 ]; then
      if [ ${#TAB_SEL_VALS[@]} -gt 0 ]; then
        saveValsForWM
      else
        saveValsForDE "$gnomefbName"
      fi

      showConfForDesktopEnvOrWinMan "$selSubCat"

      if ${yesNoFlag}; then
        break
      else
        rm -rf "$SEL_AUR_PCKGS"
        rm -rf "$POST_INST_CMD_FILE"
        resetValsForDesktopEnvOrWinMan
        deWM=""
        gnomefbName=""
      fi
    fi
  done

  cleanDialogFiles

  if [ ${POST_ASSOC_ARRAY["DE-OR-WM"]+_} ]; then
    updateSoftwareTaskChecklist "${piAssocArray["chklArray"]}" 1 "$1"
    showProgBarAndTail
    rm -rf "$SEL_AUR_PCKGS"
    rm -rf "$POST_INST_CMD_FILE"
    skipTasksForDEorWM
  else
    updateSoftwareTaskChecklist "${piAssocArray["chklArray"]}" -1 "$1"
  fi
 
  local wmConfDir="${ROOT_MOUNTPOINT}/root/aalp-gygi/"
  if [ -d "$wmConfDir" ]; then
    rm -rf ${wmConfDir}
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                     skipTasksForDEorWM
# DESCRIPTION: Set any necessary tasks to be skipped based on the "Desktop Environment"
#              or standalone "Window Manager" that was installed.  For example:
#                 If "KDE Plasma" was installed, the tasks to install a 
#                 "Composite Manager" and a "Display Manager" should be SKIPPED.
#---------------------------------------------------------------------------------------
skipTasksForDEorWM() {
  local keyDeWm="${POST_ASSOC_ARRAY["DE-OR-WM"]}"
  local deOrWM="${POST_ASSOC_ARRAY["$keyDeWm"]}"
  local pos=-1
  local skipTasks=("${MENU_OPTS_ARRAY[3]}" "${MENU_OPTS_ARRAY[4]}")
  for taskNum in "${skipTasks[@]}"; do
    case "$taskNum" in
      "${MENU_OPTS_ARRAY[3]}")
        pos=$(findPositionForString DE_SKIP_DISP_MAN "$deOrWM")
      ;;
      *)
        pos=$(findPositionForString DE_SKIP_COMP_MAN "$deOrWM")
        if [ ${pos} -lt 0 ]; then
          pos=$(findPositionForString WM_SKIP_COMP_MAN "$deOrWM")
        fi
      ;;
    esac
    if [ ${pos} -gt -1 ]; then
      updateSoftwareTaskChecklist "${piAssocArray["chklArray"]}" -1 "$taskNum"
    fi
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:               resetValsForDesktopEnvOrWinMan
# DESCRIPTION: Resets/clears the values to display the dialog to choose to install
#              either a "Desktop Environment" or standalone "Window Manager"
#---------------------------------------------------------------------------------------
resetValsForDesktopEnvOrWinMan() {
  local aryKey="${POST_ASSOC_ARRAY["DE-OR-WM"]}"
  if [ ${#aryKey} -gt 0 ]; then
    unset POST_ASSOC_ARRAY["$aryKey"]
    unset POST_ASSOC_ARRAY["DE-OR-WM"]
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                      saveValsForDE
# DESCRIPTION: Save the values selected within the global associative array
#              "POST_ASSOC_ARRAY" for the name of the "Desktop Environment" to install
#  Required Params:
#    1) gfbName - type of "GNOME Flashback" selected, or ""
#---------------------------------------------------------------------------------------
saveValsForDE() {
  local gfbName="$1"
  local aryKey="${POST_ASSOC_ARRAY["DE-OR-WM"]}"

  if [ ${#gfbName} -gt 0 ]; then
    POST_ASSOC_ARRAY["$aryKey"]="$gfbName"
    POST_ASSOC_ARRAY["$gfbName"]="$mbSelVal"
  else
    POST_ASSOC_ARRAY["$aryKey"]="$mbSelVal"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                  showDialogForFlashbackDE
# DESCRIPTION: Show a dialog to choose the session/window manager when logging into
#              the 'GNOME Flashback' Desktop Environment.
#---------------------------------------------------------------------------------------
showDialogForFlashbackDE() {
  local sessionTypes=("Compiz" "Metacity")
  local -A descs=(["Compiz"]="— uses a 3D compositor to display the windows."
                  ["Metacity"]="— displays the windows in 2D.")
  local -A flashbackDlgOpts=(["helpText"]=$(getHelpTextForFlashbackDE) ["image"]="gnome-flashback.png"
                      ["menuOptsRef"]="sessionTypes" ["optDescsRef"]="descs")
  local cancelLabel="Cancel"
  local aryKey="${POST_ASSOC_ARRAY["DE-OR-WM"]}"

  if [ "$DIALOG" == "yad" ]; then
    flashbackDlgOpts["dialogTitle"]=$(printf "%s - %s" "$cat" "$aryKey")
    flashbackDlgOpts["cancelLabel"]="$cancelLabel"
    flashbackDlgOpts["image"]="${YAD_GRAPHICAL_PATH}/images/post-inst/gnome-flashback.png"
    local cols=("Radio Btn" "Win. Man." "Description")
    local concatStr=$(printf "$FORM_FLD_SEP%s" "${cols[@]}")
    flashbackDlgOpts["columnNames"]=${concatStr:${#FORM_FLD_SEP}}
  else
    flashbackDlgOpts["cancel-label"]="$cancelLabel"
    flashbackDlgOpts["dialogBackTitle"]=$(printf "%s - %s" "$cat" "$aryKey")
    flashbackDlgOpts["dialogText"]=$(echo "Choose the default session/window manager to be used when signing in:")
    flashbackDlgOpts["dialogTitle"]=$(printf "%s (WM)" "$mbSelVal")
  fi

  cleanDialogFiles
  showRadioListDialog "flashbackDlgOpts"
}

#---------------------------------------------------------------------------------------
#      METHOD:                   saveValsForWM
# DESCRIPTION: Save the values selected for the Window Manager to be installed within
#              the global associative array "POST_ASSOC_ARRAY" for:
#                 *  the type of Window Manager chosen
#                 *  the name of the "Window Manager" to install
#---------------------------------------------------------------------------------------
saveValsForWM() {
  local tabKeys=($(printf "%s\n" "${!TAB_SEL_VALS[@]}" | sort ))
  local sep="|"
  local wmType=""
  local wmName=""
  for tabKey in "${tabKeys[@]}"; do
    case "$tabKey" in
      "WM_NAME")
        wmName="${TAB_SEL_VALS["$tabKey"]}"
      ;;
      "WM_TYPE")
        wmType="${TAB_SEL_VALS["$tabKey"]}"
      ;;
      "tab#01")
        readarray -t tabCols <<< "${TAB_SEL_VALS["$tabKey"]//$sep/$'\n'}"
        wmType="${tabCols[1]}"
      ;;
      *)
        readarray -t tabCols <<< "${TAB_SEL_VALS["$tabKey"]//$sep/$'\n'}"
        wmName="${tabCols[1]}"
      ;;
    esac
  done
  local aryKey=$(echo "${POST_ASSOC_ARRAY["DE-OR-WM"]} ($wmType)")
  POST_ASSOC_ARRAY["DE-OR-WM"]="$aryKey"
  POST_ASSOC_ARRAY["$aryKey"]="$wmName"
}

#---------------------------------------------------------------------------------------
#      METHOD:                setOptsForDesktopEnvOrWinMan
# DESCRIPTION: Set the options for the Linux or YAD dialogs to display the options for
#              the "Desktop Environments" and standalone "Window Managers"
#  Required Params:
#    1)        optSel - the value of the option that was selected for this task
#    2)       optDesc - the description/name of this task
#    3) dlgAssocArray - name of the associative array declared in the calling function
#    4)          deWM - "Desktop Environment", "Window Manager", or ""
#---------------------------------------------------------------------------------------
setOptsForDesktopEnvOrWinMan() {
  local -n dlgAssocArray="$3"
  local cancelLabel=""
  local dialogText=""
  local img=""
  local cols=()

  case "$4" in
    "Desktop Environment")
      cancelLabel="Cancel"
      dialogText=$(echo "Choose a '$mbSelVal' to install and configure:")
      img="desktop-env.png"
      cols=("Radio Btn" "Desktop Env." "Description")
      dlgAssocArray["helpText"]=$(getHelpTextForDeskEnv)
      dlgAssocArray["menuOptsRef"]="DESKTOP_ENVIRONMENTS"
      dlgAssocArray["optDescsRef"]="DESKTOP_AUR_DESCS"
      if [ "$DIALOG" == "yad" ]; then
	dlgAssocArray["height"]=700
      fi
    ;;
    "Window Manager")
      cancelLabel="Cancel"
      dialogText=$(echo "Choose a standalone '$mbSelVal' to install and configure:")
      img="window-manager.png"
      dlgAssocArray["helpText"]=$(getHelpTextForWinMan)
      unset dlgAssocArray["menuOptsRef"]
      dlgAssocArray["optDescsRef"]="DESKTOP_AUR_DESCS"
      for wmType in "${WINDOW_MANAGERS[@]}"; do
        case "$wmType" in
          "${WINDOW_MANAGERS[0]}")
            dlgAssocArray["$wmType"]="DYNAMIC_WINDOW_MANAGERS"
          ;;
          "${WINDOW_MANAGERS[1]}")
            dlgAssocArray["$wmType"]="STACKING_WINDOW_MANAGERS"
          ;;
          *)
            dlgAssocArray["$wmType"]="TILING_WINDOW_MANAGERS"
          ;;
        esac
      done
      if [ "$DIALOG" == "yad" ]; then
        dlgAssocArray["height"]=700
      fi
    ;;
    *)
      cancelLabel="Skip"
      dialogText="Choose to install either a 'Desktop Environment' or standalone 'Window Manager':"
      img="de-wm.png"
      cols=("Radio Btn" "Category" "Description")
      dlgAssocArray["helpText"]=$(getHelpTextForDEorWM)
      dlgAssocArray["menuOptsRef"]="taskCats"
      dlgAssocArray["optDescsRef"]="DESC_ASSOC_ARRAY"
      unset dlgAssocArray["height"]
    ;;
  esac

  dlgAssocArray["dialogTitle"]=$(printf "%s - %s" "$1" "$2")
  if [ "$DIALOG" == "yad" ]; then
    dlgAssocArray["cancelLabel"]="$cancelLabel"
    dlgAssocArray["image"]="${YAD_GRAPHICAL_PATH}/images/post-inst/$img"
    local concatStr=$(printf "$FORM_FLD_SEP%s" "${cols[@]}")
    dlgAssocArray["columnNames"]=${concatStr:${#FORM_FLD_SEP}}
  else
    dlgAssocArray["cancel-label"]="$cancelLabel"
    dlgAssocArray["dialogBackTitle"]=$(printf "%s - %s" "${SEL_CHKLIST_OPT[0]}" "${SEL_CHKLIST_OPT[1]}")
    dlgAssocArray["dialogText"]=$(echo "$dialogText")
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                showConfForDesktopEnvOrWinMan
# DESCRIPTION: Show a confirmation dialog to install and configure the "Desktop Environment"
#              or standalone "Window Manager" that was chosen
#  Required Params:
#    1) subCat - the sub category selected for either the DE or WM
#---------------------------------------------------------------------------------------
showConfForDesktopEnvOrWinMan() {
  local keys=("A)")
  local -A keyDescs=(["A)"]="AUR packages to install:")
  local urlRef=""
  local deWMKey="${POST_ASSOC_ARRAY["DE-OR-WM"]}"
  local deWMName="${POST_ASSOC_ARRAY["$deWMKey"]}"
  local dialogText="Confirm the installation and configuration of the '${deWMName}' $deWMKey"
  local windowTitle=$(printf "%s - %s" "${SEL_CHKLIST_OPT[0]}" "${SEL_CHKLIST_OPT[1]}")
  local -A funcParams=(["menuOptsRef"]="keys" ["optDescsRef"]="keyDescs" ["windowTitle"]="$windowTitle")

  setPackagesForDeWm "$deWMName" "keys" "keyDescs"

  case "${POST_ASSOC_ARRAY["DE-OR-WM"]}" in
    "Desktop Environment")
      urlRef="https://wiki.archlinux.org/index.php/desktop_environment"
      setCmdsForDeskEnv "$deWMName" "keys" "keyDescs" "$windowTitle"
    ;;
    *)
      urlRef="https://wiki.archlinux.org/index.php/Window_manager"
      setCmdsForWinMan "$deWMName" "keys" "keyDescs" "$windowTitle"
    ;;
  esac

  funcParams["helpText"]=$(getHelpTextForDeWmConf "$urlRef" "$deWMName" "$deWMKey" "$windowTitle")
  funcParams["dialogText"]="$dialogText"
  setOptsForDeWmConf "$1" "${DESC_ASSOC_ARRAY["$1"]}" "funcParams"

  unset funcParams["windowTitle"]

  showConfirmationDialog "funcParams"
  cleanDialogFiles
}

#---------------------------------------------------------------------------------------
#      METHOD:                     setPackagesForDeWm
# DESCRIPTION: Set the names of the AUR packages to be installed for either a
#              "Desktop Environment" or standalone "Window Manager"
#  Required Params:
#    1) name - the name of the "Desktop Environment" or standalone "Window Manager"
#    2) aurPckgNames - the name of the array to append the package names to
#    3) aurPckgDescs - name of the associative array for the description of the packages
#---------------------------------------------------------------------------------------
setPackagesForDeWm() {
  local -n aurPckgNames="$2"
  local -n aurPckgDescs="$3"
  local aurPckgName="${DESKTOP_AUR_PCKGS["$1"]}"
  local pckgExtras=()
  local sep=" "
  local arrayElem=""

  if [ ${DESKTOP_AUR_PCKGS["$1"]+_} ]; then
    aurPckgName="${DESKTOP_AUR_PCKGS["$1"]}"
  else
    aurPckgName="$1"
  fi

  aurPckgNames+=("$aurPckgName")
  aurPckgDescs["$aurPckgName"]="${DESKTOP_AUR_DESCS["$1"]}"

  case "${POST_ASSOC_ARRAY["DE-OR-WM"]}" in
    "Desktop Environment")
      arrayElem="${DE_EXTRAS["$1"]}"
    ;;
    *)
      arrayElem="${WM_EXTRAS["$1"]}"
    ;;
  esac

  if [ ${#arrayElem} -gt 0 ]; then
    readarray -t pckgExtras <<< "${arrayElem//$sep/$'\n'}"
    for aurPckgName in "${pckgExtras[@]}"; do
      aurPckgNames+=("$aurPckgName")
      aurPckgDescs["$aurPckgName"]="${DESKTOP_AUR_DESCS["$aurPckgName"]}"
    done
  fi

  local concatStr=$(printf " %s" "${aurPckgNames[@]:1}")
  echo -e "${concatStr:1}" > "$SEL_AUR_PCKGS"
}

#---------------------------------------------------------------------------------------
#      METHOD:                      setCmdsForDeskEnv
# DESCRIPTION: Set the commands to be executed in order to configure & run the
#              "Desktop Environment"
#  Required Params:
#    1) deName - the name of the "Desktop Environment"
#    2) cmdKeys - the name of the array to append the command keys to and list them
#                 within the confirmation dialog
#    3) cmdTiles - name of the associative array for the description of the command keys
#    4) title - title of the post installation step
#---------------------------------------------------------------------------------------
setCmdsForDeskEnv() {
  local deName="$1"
  local -n cmdKeys="$2"
  local -n cmdTiles="$3"
  local key=""
  local keyNum=0
  local cmdLine=""
  local execCmds=()
  local userName="${POST_ASSOC_ARRAY["UserName"]}"

  cmdKeys+=("B)")
  cmdTiles["B)"]="Configuration steps:"

  cmdLine=$(getXinitrcLinesForDeskEnv "$deName")

  if [ ${#cmdLine} -gt 0 ]; then
    addCmdForDEorWM "$deName" "$cmdLine" "$userName"
  fi

  addCmdsForDefDispMan "$deName"

  local pos=$(findPositionForString DE_SKIP_DISP_MAN "$deName")
  if [ ${pos} -gt -1 ]; then
    local taskNum="${MENU_OPTS_ARRAY[3]}"
    cmdKeys+=("C)")
    cmdTiles["C)"]="Task for '$4' that will be skipped:"
    cmdKeys+=("$taskNum")
    cmdTiles["$taskNum"]="${DESC_ASSOC_ARRAY["$taskNum"]}"
  fi

  pos=$(findPositionForString DE_SKIP_COMP_MAN "$deName")
  if [ ${pos} -gt -1 ]; then
    local taskNum="${MENU_OPTS_ARRAY[4]}"
    if [ ${cmdTiles["C)"]+_} ]; then
      cmdTiles["C)"]="Tasks for '$4' that will be skipped:"
    else
      cmdKeys+=("C)")
      cmdTiles["C)"]="Task for '$4' that will be skipped:"
    fi
    cmdKeys+=("$taskNum")
    cmdTiles["$taskNum"]="${DESC_ASSOC_ARRAY["$taskNum"]}"
  fi

  local concatStr=$(printf "\n%s" "${execCmds[@]}")
  echo -e "${concatStr:1}" > "$POST_INST_CMD_FILE"
}

#---------------------------------------------------------------------------------------
#      METHOD:                    addCmdsForDefDispMan
# DESCRIPTION: Add the commands to be executed in order to configure the default
#              "Display Manager" for a "Desktop Environment"
#  Required Params:
#    1) deName - name of the "Desktop Environment"
#---------------------------------------------------------------------------------------
addCmdsForDefDispMan() {
  local deName="$1"
  local deCmds=()
  local deCmdTitles=()

  case "$deName" in
    "Deepin")
      deCmds=(
      "sed -i 's/#greeter-session=example-gtk-gnome/greeter-session=lightdm-deepin-greeter/' /etc/lightdm/lightdm.conf"
      "systemctl enable lightdm.service")
      deCmdTitles=("Setting type of greeter for the default 'LightDM' Display Manager"
          "Enabling service for the default 'LightDM' Display Manager")
    ;;
    "GNOME")
      deCmds=("systemctl enable gdm.service")
      deCmdTitles=("Enabling service for the default 'GNOME' Display Manager")
    ;;
    "KDE Plasma"|"LXQt")
      local isDef=$(echo 'true' && return 0)
      setDispManCmdsForSDDM "deCmds" "deCmdTitles" ${isDef}
    ;;
    "LXDE")
      deCmds=("sed -i \"s/\#[[:space:]]*session/session/g\" \"/etc/lxdm.conf\""
      "systemctl enable lxdm.service")
      deCmdTitles=("Setting 'LXDE' as the default session within the conf. file for the default 'LightDM' Display Manager"
      "Enabling service for the default 'LXDM' Display Manager")
    ;;
  esac

  local arrayLen=${#deCmds[@]}
  if [ ${arrayLen} -gt 0 ]; then
    local lastIdx=$(expr ${arrayLen} - 1)
    local keyNum=0
    for aryIdx in $(eval echo "{0..${lastIdx}}"); do
      local title="${deCmdTitles[${aryIdx}]}"
      local cmdLine=$(printf "%s%s%s" "$title" "${FORM_FLD_SEP}" "${deCmds[${aryIdx}]}")

      execCmds+=("$cmdLine")
      keyNum=$(expr ${keyNum} + 1)
      key=$(printf "#%d)" ${keyNum})
      cmdKeys+=("$key")
      if [ "$DIALOG" == "yad" ]; then
        cmdTiles["$key"]=$(escapeSpecialCharacters "$title")
      else
        cmdTiles["$key"]="$title"
      fi
    done
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                       addCmdForDEorWM
# DESCRIPTION: Set the commands to be executed in order to configure the
#              "Display Manager" for "SDDM"
#  Required Params:
#    1) commands - name of the array to store the commands
#    2)   titles - name of the array to store the titles of the commands
#   Optional Param:
#    3) flag to indicate if this is the default "Display Manager"
#---------------------------------------------------------------------------------------
setDispManCmdsForSDDM() {
  local -n commands="$1"
  local -n titles="$2"
  local str="for the"
  local dmStr="DM"

  if [ "$#" -gt 2 ]; then
    str="for the default"
    dmStr="Display Manager"
  fi

  commands=("sddm --example-config > /etc/sddm.conf"
      "sed -i 's/Current=/Current=breeze/' /etc/sddm.conf"
      "sed -i 's/CursorTheme=/CursorTheme=breeze_cursors/' /etc/sddm.conf"
      "sed -i 's/Numlock=none/Numlock=on/' /etc/sddm.conf"
      "systemctl enable sddm.service")
  titles=("Creating configuration file $str 'SDDM'  $dmStr"
      "Setting the theme to 'breeze' within the conf. file $str 'SDDM'  $dmStr"
      "Setting the cursor theme to 'breeze_cursors' within the conf. file $str 'SDDM'  $dmStr"
      "Activating 'Numlock' on bootup within the conf. file $str 'SDDM'  $dmStr"
      "Enabling service $str 'SDDM'  $dmStr")
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getXinitrcLinesForDeskEnv
# DESCRIPTION: Get the lines that will be appended to the user's "xinitrc" file in
#              order to fire up the "Desktop Environment".
#      RETURN: concatenated string
#  Required Params:
#    1) deName - name of the "Desktop Environment"
#---------------------------------------------------------------------------------------
function getXinitrcLinesForDeskEnv() {
  local deName="$1"
  local xinitLines=()

  case "$deName" in
    "Budgie")
      xinitLines=("export XDG_CURRENT_DESKTOP=Budgie:GNOME" "exec budgie-desktop")
    ;;
    "Cinnamon")
      xinitLines=("exec cinnamon-session")
    ;;
    "Deepin")
      xinitLines=("exec startdde")
    ;;
    "Enlightenment")
      xinitLines=("exec enlightenment_start")
    ;;
    "GNOME")
      xinitLines=("export GDK_BACKEND=x11" "exec gnome-session")
    ;;
    "GNOME Flashback")
      case "${POST_ASSOC_ARRAY["$deName"]}" in
        "Compiz")
          xinitLines=("export XDG_CURRENT_DESKTOP=GNOME-Flashback:GNOME"
            "exec gnome-session --session=gnome-flashback-compiz")
        ;;
        *)  # Metacity
          xinitLines=("export XDG_CURRENT_DESKTOP=GNOME-Flashback:GNOME"
            "exec gnome-session --session=gnome-flashback-metacity")
        ;;
      esac
    ;;
    "KDE Plasma")
      xinitLines=("exec XDG_SESSION_TYPE=wayland dbus-run-session startplasmacompositor")
      #exec XDG_SESSION_TYPE=wayland dbus-run-session startplasmacompositor
      #                               OR
      #exec startkde
    ;;
    "LXQt")
      xinitLines=("exec startlxqt")
    ;;
    "MATE")
      xinitLines=("exec mate-session")
    ;;
    "Xfce")
      xinitLines=("exec startxfce4")
    ;;
    *)
      local cmd=$(echo "$deName" | tr "[:upper:]" "[:lower:]")
      cmd=$(echo "exec $cmd")
      xinitLines=("$cmd")
    ;;
  esac

  local concatStr=$(printf "\n%s" "${xinitLines[@]}")
  concatStr=$(printf "configureXinitrc %s \"%s\"" "${POST_ASSOC_ARRAY["UserName"]}" "${concatStr:1}")
  echo -e "$concatStr"
}

#---------------------------------------------------------------------------------------
#      METHOD:                      setCmdsForWinMan
# DESCRIPTION: Set the commands to be executed in order to configure & fire up the
#              standalone "Window Manager"
#  Required Params:
#    1) wmName - the name of the standalone "Window Manager"
#    2) cmdKeys - the name of the array to append the command keys to and list them
#                 within the confirmation dialog
#    3) cmdTiles - name of the associative array for the description of the command keys
#    4) title - title of the post installation step
#---------------------------------------------------------------------------------------
setCmdsForWinMan() {
  local wmName="$1"
  local -n cmdKeys="$2"
  local -n cmdTitles="$3"
  local key=""
  local keyNum=0
  local cmdLine=""
  local execCmds=()
  local userName="${POST_ASSOC_ARRAY["UserName"]}"
  local homeDir="/home/$userName"
  local srcDir="${DATA_DIR}/software"
  local destDir=""

  cmdKeys+=("B)")
  cmdTitles["B)"]="Configuration steps:"

  cmdLine=$(getXinitrcLinesForWinMan "$wmName")

  if [ ${#cmdLine} -gt 0 ]; then
    addCmdForDEorWM "$wmName" "$cmdLine" "$userName"
  fi

  case "$wmName" in
    "cwm")
      srcDir="${srcDir}/cwm"
      destDir="${ROOT_MOUNTPOINT}/$srcDir"
      for cmdNum in $(eval echo "{1..2}"); do
        case ${cmdNum} in
          1)
            cmdLine="cp ${srcDir}/cwmrc ${homeDir}/.cwmrc"
          ;;
          2)
            cmdLine="cp ${srcDir}/xinitrc ${homeDir}/.xinitrc"
          ;;
        esac
        addCmdForDEorWM "$wmName" "$cmdLine" "$userName"
      done
    ;;
    "Openbox")
      cmdLine="cp -R /etc/xdg/openbox ${homeDir}/.config/"
      addCmdForDEorWM "$wmName" "$cmdLine" "$userName"
    ;;
    "EXWM")
      srcDir="${srcDir}/Emacs"
      destDir="${ROOT_MOUNTPOINT}/$srcDir"
      cmdLine="mkdir -p ${homeDir}/.emacs.d"
      addCmdForDEorWM "EXWM/emacs" "$cmdLine" "$userName"
      cmdLine="cp ${srcDir}/exwm-init.el ${homeDir}/.emacs.d/init.el"
      addCmdForDEorWM "EXWM/emacs" "$cmdLine" "$userName"
    ;;
    "Ion3")
      cmdLine="cp /etc/ion3/* ${homeDir}/.ion3"
      addCmdForDEorWM "$wmName" "$cmdLine" "$userName"
    ;;
    "Notion")
      cmdLine="cp /etc/notion/cfg_notion.lua ${homeDir}/.notion"
      addCmdForDEorWM "$wmName" "$cmdLine" "$userName"
    ;;
    "sway")
      srcDir="${srcDir}/sway"
      destDir="${ROOT_MOUNTPOINT}/$srcDir"
      cmdLine="cp ${srcDir}/start-sway.sh /usr/bin/startSway"
      addCmdForDEorWM "$wmName" "$cmdLine" "$userName"
    ;;
    "WMFS2")
      cmdLine="mkdir -p ${homeDir}/.config/wmfs"
      addCmdForDEorWM "$wmName" "$cmdLine" "$userName"
      cmdLine="cp /etc/xdg/wmfs/wmfsrc ${homeDir}/.config/wmfs/wmfsrc"
      addCmdForDEorWM "$wmName" "$cmdLine" "$userName"
    ;;
    "awesome")
      cmdLine="mkdir -p ${homeDir}/.config/awesome"
      addCmdForDEorWM "$wmName" "$cmdLine" "$userName"
      cmdLine="cp /etc/xdg/awesome/rc.lua ${homeDir}/.config/awesome/"
      addCmdForDEorWM "$wmName" "$cmdLine" "$userName"
    ;;
    "echinus")
      cmdLine="cp /etc/xdg/echinus ${homeDir}/.echinus"
      addCmdForDEorWM "$wmName" "$cmdLine" "$userName"
    ;;
    "Qtile")
      cmdLine="mkdir -p ${homeDir}/.config/qtile"
      addCmdForDEorWM "$wmName" "$cmdLine" "$userName"
      cmdLine="cp /usr/share/doc/qtile/default_config.py ${homeDir}/.config/qtile/config.py"
      addCmdForDEorWM "$wmName" "$cmdLine" "$userName"
    ;;
    "wmii")
      cmdLine="mkdir ${homeDir}/.wmii"
      addCmdForDEorWM "$wmName" "$cmdLine" "$userName"
      cmdLine="cp /etc/wmii/wmiirc ${homeDir}/.wmii/wmiirc"
      addCmdForDEorWM "$wmName" "$cmdLine" "$userName"
    ;;
    "xmonad")
      srcDir="${srcDir}/xmonad"
      destDir="${ROOT_MOUNTPOINT}/$srcDir"
      cmdLine="mkdir ${homeDir}/.xmonad"
      addCmdForDEorWM "$wmName" "$cmdLine" "$userName"
      cmdLine="cp ${srcDir}/xmonad.hs ${homeDir}/.xmonad/xmonad.hs"
      addCmdForDEorWM "$wmName" "$cmdLine" "$userName"
    ;;
  esac

  local pos=$(findPositionForString WM_SKIP_COMP_MAN "$wmName")
  if [ ${pos} -gt -1 ]; then
    local taskNum="${MENU_OPTS_ARRAY[4]}"
    cmdKeys+=("C)")
    cmdTitles["C)"]="Task for '$4' that will be skipped:"
    cmdKeys+=("$taskNum")
    cmdTitles["$taskNum"]="${DESC_ASSOC_ARRAY["$taskNum"]}"
  fi

  local concatStr=$(printf "\n%s" "${execCmds[@]}")
  echo -e "${concatStr:1}" > "$POST_INST_CMD_FILE"

  if [ ${#destDir} -gt 0 ]; then
    mkdir -p "$destDir"
    cp -rf ${srcDir}/* ${destDir}/.
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                  getXinitrcLinesForWinMan
# DESCRIPTION: Get the lines that will be appended to the user's "xinitrc" file in
#              order to fire up the standalone "Window Manager".
#      RETURN: concatenated string
#  Required Params:
#    1) wmName - name of the standalone "Window Manager"
# getXinitrcLinesForDeskEnv
# getXinitrcLinesForWinMan
#---------------------------------------------------------------------------------------
function getXinitrcLinesForWinMan() {
  local wmName="$1"
  local xinitLines=()
  local concatStr=""

  case "$wmName" in
    "Compiz")
      xinitLines=("exec fusion-icon")
    ;;
    "cwm")
    ;;
    "Fluxbox")
      xinitLines=("exec startfluxbox")
    ;;
    "FVWM")
      xinitLines=("exec fvwm-crystal")
    ;;
    "IceWM")
      xinitLines=("exec icewm-session")
    ;;
    "Openbox")
      xinitLines=("exec openbox-session")
    ;;
    "Window Maker")
      xinitLines=("exec wmaker")
    ;;
    "Xfwm")
      xinitLines=("exec xfwm4")
    ;;
    "EXWM")
      xinitLines=("exec emacs")
    ;;
    "Ratpoison")
      xinitLines=("# The black/white grid as background doesn't suit my taste."
                  "xsetroot -solid black &"
                  "# Ratpoison is compatible with xcompmgr! now you can have real transparency"
                  "xcompmgr -c -f -D 5 &"
                  "#fire up ratpoison!"
                  "exec /usr/bin/ratpoison")
    ;;
    "sway")
      xinitLines=("exec /usr/bin/startSway")
    ;;
    "WMFS2")
      xinitLines=("exec wmfs")
    ;;
    *)
      local cmd=$(echo "$wnName" | tr "[:upper:]" "[:lower:]")
      cmd=$(echo "exec $cmd")
      xinitLines=("$cmd")
    ;;
  esac

  if [ ${#xinitLines[@]} -gt 0 ]; then
    concatStr=$(printf "\n%s" "${xinitLines[@]}")
    concatStr=$(printf "configureXinitrc %s \"%s\"" "${POST_ASSOC_ARRAY["UserName"]}" "${concatStr:1}")
  fi
  echo -e "$concatStr"
}

#---------------------------------------------------------------------------------------
#      METHOD:                       addCmdForDEorWM
# DESCRIPTION: Add the commands to be executed in order to configure & fire up either a
#              "Desktop Environment" or standalone "Window Manager"
#  Required Params:
#    1) wmName - name of either the "Desktop Environment" or standalone "Window Manager"
#    2) cmdLine - linux command to set either the access or owner permissions
#    3) userName - name of the user
#---------------------------------------------------------------------------------------
addCmdForDEorWM() {
  local wmName="$1"
  local cmdLine="$2"
  local userName="$3"
  local title=""

  if [ "$#" -gt 3 ]; then
    title="$3"
  else
    if [ "$wmName" == "Openbox" ] && [[ ! "$cmdLine" =~ "configureXinitrc" ]]; then
      title="Copy template configuration files for 'Openbox' into the local 'Openbox' conf. directory for user '${userName}'"
    elif [[ "$cmdLine" =~ ^mkdir ]]; then
      title="Create the '$wmName' configuration directory within the home directory for user '${userName}'"
    elif [[ "$cmdLine" =~ ^cp[[:space:]]\/etc\/ ]]; then
      title="Copy the default '$wmName' configuration file into the local '$wmName' directory for user '${userName}'"
    elif [[ "$cmdLine" =~ "configureXinitrc" ]] || [[ "$cmdLine" =~ "xinitrc" ]]; then
      title="Create & configure the 'xinitrc' file into the home directory for user '${userName}' to fire up '$wmName'"
    elif [[ "$cmdLine" =~ "emacs" ]] || [[ "$cmdLine" =~ "xmonad" ]]; then
      title="Create & configure the '$wmName' configuration file into the local '$wmName' conf. directory for user '${userName}'"
    elif [[ "$cmdLine" =~ .*\/*cwmrc ]]; then
      title="Create & configure the '$wmName' configuration file into the home directory for user '${userName}'"
    elif [ "$wmName" == "sway" ]; then
      title="Copy script that is called within 'xinitrc' to fire up 'sway' into the '/usr/bin' directory"
    fi
  fi

  keyNum=$(expr ${keyNum} + 1)
  key=$(printf "#%d)" ${keyNum})
  cmdKeys+=("$key")
  cmdLine=$(printf "%s%s%s" "$title" "${FORM_FLD_SEP}" "$cmdLine")
  execCmds+=("$cmdLine")
  if [ "$DIALOG" == "yad" ]; then
    cmdTitles["$key"]=$(escapeSpecialCharacters "$title")
  else
    cmdTitles["$key"]="$title"
  fi

  addCmdsForFileAccessOwnerPerms "$wmName" "$cmdLine" "$userName"
}

#---------------------------------------------------------------------------------------
#      METHOD:               addCmdsForFileAccessOwnerPerms
# DESCRIPTION: Add the necessary commands to set access & owner permissions to the
#              standalone "Window Manager's" configuration directory and/or files
#  Required Params:
#    1) wmName - name of the standalone "Window Manager"
#    2) cmdLine - linux command to set either the access or owner permissions
#    3) userName - name of the user
#---------------------------------------------------------------------------------------
addCmdsForFileAccessOwnerPerms() {
  local wmName="$1"
  local cmdLine="$2"
  local userName="$3"
  local cmdSplitArray=()
  local fileName=""
  local configFile=""
  local poCmd=""
  local permOwnerCmds=()
  local sep=" "

  if [ "$wmName" == "Openbox" ] && [[ ! "$cmdLine" =~ "configureXinitrc" ]]; then
    readarray -t cmdSplitArray <<< "${cmdLine//$sep/$'\n'}"
    local dir="${cmdSplitArray[-1]}"
    local poCmd=$(printf "Setting owner permissions to directory '%s'%schown -R ${userName}:users %s" "$dir" "$FORM_FLD_SEP" "$dir")
    permOwnerCmds=("$poCmd")
  elif [[ "$cmdLine" =~ ^mkdir ]]; then
    readarray -t cmdSplitArray <<< "${cmdLine//$sep/$'\n'}"
    local dir="${cmdSplitArray[-1]}"
    local poCmd=$(printf "Setting access permissions to directory '%s'%schmod -R 775 %s" "$dir" "$FORM_FLD_SEP" "$dir")
    permOwnerCmds=("$poCmd")
    poCmd=$(printf "Setting owner permissions to directory '%s'%schown -R ${userName}:users %s" "$dir" "$FORM_FLD_SEP" "$dir")
    permOwnerCmds+=("$poCmd")
  elif [ "$wmName" != "sway" ]; then
    if [[ "$cmdLine" =~ "configureXinitrc" ]] || [[ "$cmdLine" =~ "xinitrc" ]]; then
      fileName="/home/${userName}/.xinitrc"
      configFile="'xinitrc' file"
    else
      readarray -t cmdSplitArray <<< "${cmdLine//$sep/$'\n'}"
      fileName="${cmdSplitArray[-1]}"
      configFile=$(printf "'%s' configuration file" "$wmName")
    fi
    poCmd=$(printf "Setting access permissions to the %s%schmod 644 %s" "$configFile" "$FORM_FLD_SEP" "$fileName")
    permOwnerCmds=("$poCmd")
    poCmd=$(printf "Setting owner permissions to the %s%schown ${userName}:users %s" "$configFile" "$FORM_FLD_SEP" "$fileName")
    permOwnerCmds+=("$poCmd")
  fi

  if [ ${#permOwnerCmds[@]} -gt 0 ]; then
    execCmds+=("${permOwnerCmds[@]}")
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                     setOptsForDeWmConf
# DESCRIPTION: Set the options for the Linux or YAD dialogs to display confirmation for
#              installing and configuring either the "Desktop Environments"
#              or standalone "Window Managers"
#  Required Params:
#    1)        optSel - the value of the option that was selected for this task
#    2)       optDesc - the description/name of this task
#    3) dlgAssocArray - name of the associative array declared in the calling function
#    4)    dialogText - Text to display inside the dialog before the list of menu options
#---------------------------------------------------------------------------------------
setOptsForDeWmConf() {
  local -n dlgAssocArray="$3"
  local dialogText="${dlgAssocArray["dialogText"]}"
  local dialogTitle=$(printf "%s - %s Confirmation" "$1" "$2")
  local windowTitle="${dlgAssocArray["windowTitle"]}"

  if [ "$DIALOG" == "yad" ]; then
    dlgAssocArray["width"]=675
    dlgAssocArray["image"]="$YAD_GRAPHICAL_PATH/images/post-inst/confirmation-de-wm.png"
    dlgAssocArray["dialogTitle"]="$windowTitle"
    dlgAssocArray["helpDlgTitle"]="$dialogTitle"
    dialogTitle=$(printf "%10s%s" " " "$dialogTitle")
    local yadTextArray=("$dialogTitle" " " " " "$dialogText")
    dialogText=$(printf "\n%s" "${yadTextArray[@]}")
    dlgAssocArray["dialogText"]="$dialogText"

    local buttons=("Confirm" "Cancel" "Help")
    local concatStr=$(printf "$FORM_FLD_SEP%s" "${buttons[@]}")
    dlgAssocArray["buttons"]="${concatStr:${#FORM_FLD_SEP}}"

    local cols=("Key" "Title / Description")
    concatStr=$(printf "$FORM_FLD_SEP%s" "${cols[@]}")
    dlgAssocArray["cols"]="${concatStr:${#FORM_FLD_SEP}}"
    dlgAssocArray["sel-line"]="true"
  else
    dlgAssocArray["cancel-label"]="Cancel"
    dlgAssocArray["dialogBackTitle"]="$windowTitle"
    dlgAssocArray["dialogText"]="$dialogText"
    dlgAssocArray["dialogTitle"]="$dialogTitle"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                       installDispMan
# DESCRIPTION: Install the group of packages for a "Display Manager"
#  Required Params:
#    1)  optSel - the value of the option that was selected for this task
#    2) optDesc - the description/name of this task
#---------------------------------------------------------------------------------------
installDispMan() {
  local aryKey="Display-Manager"
  local flagVal=0
  local -A funcParams=(["defaultChoice"]="${DISPLAY_MANAGERS[1]}" ["helpText"]=$(getHelpTextForDisplayManager)
                      ["menuOptsRef"]="DISPLAY_MANAGERS" ["optDescsRef"]="DESKTOP_AUR_DESCS")
  local dialogTitle=""
  local windowTitle=""

  setOptsForRadioListDialog "$1" "$2" "funcParams"

  if [ "$DIALOG" == "yad" ]; then
    dialogTitle="${funcParams["dialogTextHdr"]}"
    windowTitle="${funcParams["dialogTitle"]}"
  else
    dialogTitle="${funcParams["dialogTitle"]}"
    windowTitle="${funcParams["dialogBackTitle"]}"
  fi

  while true; do
    selectFromRadioListDialog "funcParams"
    cleanDialogFiles
    if [ ${#mbSelVal} -gt 0 ]; then
      POST_ASSOC_ARRAY["$aryKey"]="$mbSelVal"
      showConfForDispMan "$dialogTitle" "$windowTitle"
      if ${yesNoFlag}; then
        break
      else
        rm -rf "$SEL_AUR_PCKGS"
        rm -rf "$POST_INST_CMD_FILE"
        unset POST_ASSOC_ARRAY["$aryKey"]
      fi
    else
      unset POST_ASSOC_ARRAY["$aryKey"]
      break
    fi
  done

  if [ ${POST_ASSOC_ARRAY["$aryKey"]+_} ]; then
    showProgBarAndTail
    rm -rf "$SEL_AUR_PCKGS"
    rm -rf "$POST_INST_CMD_FILE"
    flagVal=1
  else
    flagVal=-1
  fi

  updateSoftwareTaskChecklist "${piAssocArray["chklArray"]}" ${flagVal} "$1"
}

#---------------------------------------------------------------------------------------
#      METHOD:                   setOptsForRadioListDialog
# DESCRIPTION: Set the menu options that can be selected for a Linux or YAD dialogs
#              of type radio list.
#  Required Params:
#    1)        optSel - the value of the option that was selected for this task
#    2)       optDesc - the description/name of this task
#    3) dlgAssocArray - name of the associative array declared in the calling function
#---------------------------------------------------------------------------------------
setOptsForRadioListDialog() {
  local -n dlgAssocArray="$3"
  local cancelLabel="Skip"
  local windowTitle=$(printf "%s - %s" "${SEL_CHKLIST_OPT[0]}" "${SEL_CHKLIST_OPT[1]}")
  local dialogTitle=$(printf "%s - %s" "$1" "$2")
  local cols=()
  local dialogText=""
  local img=""

  case "$2" in
    "Display Manager")
      cols=("Radio Btn" "Display Manager" "Description")
      dialogText="Choose the 'Display Manager' to be installed & configured:"
      img="display-manager.png"
    ;;
    *)
      cols=("Radio Btn" "Brand" "Types")
      dialogText="Choose the name of the brand for the video card that is installed:"
      img="brands.png"
    ;;
  esac

  if [ "$DIALOG" == "yad" ]; then
    dlgAssocArray["cancelLabel"]="$cancelLabel"
    dlgAssocArray["image"]="${YAD_GRAPHICAL_PATH}/images/post-inst/${img}"
    dlgAssocArray["dialogTitle"]="$windowTitle"
    dlgAssocArray["dialogTextHdr"]="$dialogTitle"
    local concatStr=$(printf "$FORM_FLD_SEP%s" "${cols[@]}")
    dlgAssocArray["columnNames"]=${concatStr:${#FORM_FLD_SEP}}
  else
    dlgAssocArray["cancel-label"]="$cancelLabel"
    dlgAssocArray["dialogBackTitle"]="$windowTitle"
    dlgAssocArray["dialogText"]="$dialogText"
    dlgAssocArray["dialogTitle"]="$dialogTitle"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                     showConfForDispMan
# DESCRIPTION: Show a confirmation dialog to install and configure the "Display Manager"
#              that was chosen
#  Required Params:
#    1) dialogTitle - The string to be displayed at the very of the top of the dialog box
#    2) windowTitle - The string to be displayed in either the window/dialog's title bar,
#                     or on the backdrop that is at the very top of the screen.
#---------------------------------------------------------------------------------------
showConfForDispMan() {
  local keys=("A)")
  local -A keyDescs=(["A)"]="AUR packages to install:")
  local urlRef=""
  local -A dlgConfParams=(["menuOptsRef"]="keys" ["optDescsRef"]="keyDescs")
  local execCmds=()

  setPackagesForDispMan "keys" "keyDescs"

  dlgConfParams["helpText"]=$(getHelpTextForDispManConf)
  dlgConfParams["dialogText"]="$dialogText"

  local title="Configuration & Start/Fire Up steps for the DM:"
  keys+=("B)")
  if [ "$DIALOG" == "yad" ]; then
    keyDescs["B)"]=$(escapeSpecialCharacters "$title")
  else
    keyDescs["B)"]="$title"
  fi
  setCmdsForDispMan

  setOptsForDispManConf "$1" "$2" "dlgConfParams"

  showConfirmationDialog "dlgConfParams"
  cleanDialogFiles
}

#---------------------------------------------------------------------------------------
#      METHOD:                    setPackagesForDispMan
# DESCRIPTION: Set the names of the AUR packages to be installed for either a
#              "Desktop Environment" or standalone "Window Manager"
#  Required Params:
#    1) aurPckgNames - the name of the array to append the package names to
#    2) aurPckgDescs - name of the associative array for the description of the packages
#---------------------------------------------------------------------------------------
setPackagesForDispMan() {
  local -n aurPckgNames="$1"
  local -n aurPckgDescs="$2"
  local dmName="${POST_ASSOC_ARRAY["Display-Manager"]}"
  local aurPckgName="${DESKTOP_AUR_PCKGS["$dmName"]}"
  local pckgExtras=()
  local sep=" "
  local arrayElem="${DM_EXTRAS["$dmName"]}"

  if [ ${DESKTOP_AUR_PCKGS["$dmName"]+_} ]; then
    aurPckgName="${DESKTOP_AUR_PCKGS["$dmName"]}"
  else
    aurPckgName="$dmName"
  fi

  aurPckgNames+=("$aurPckgName")
  aurPckgDescs["$aurPckgName"]="${DESKTOP_AUR_DESCS["$dmName"]}"

  if [ ${#arrayElem} -gt 0 ]; then
    readarray -t pckgExtras <<< "${arrayElem//$sep/$'\n'}"
    for aurPckgName in "${pckgExtras[@]}"; do
      aurPckgNames+=("$aurPckgName")
      aurPckgDescs["$aurPckgName"]="${DESKTOP_AUR_DESCS["$aurPckgName"]}"
    done
  fi

  local concatStr=$(printf " %s" "${aurPckgNames[@]:1}")
  echo -e "${concatStr:1}" > "$SEL_AUR_PCKGS"
}

#---------------------------------------------------------------------------------------
#      METHOD:                      setCmdsForDispMan
# DESCRIPTION: Set the commands to be executed in order to configure the "Display Manager"
#---------------------------------------------------------------------------------------
setCmdsForDispMan() {
  local dmCmds=()
  local dmCmdTitles=()
  local userName="${POST_ASSOC_ARRAY["UserName"]}"
  local concatStr=""

  case "${POST_ASSOC_ARRAY["Display-Manager"]}" in
    "GDM")
      dmCmds=("systemctl enable gdm.service")
      dmCmdTitles=("Enabling service for the 'GNOME' DM")
    ;;
    "LightDM")
      local confFile="/etc/lightdm/lightdm.conf"
      dmCmds=(
      "sed -i 's/#greeter-session=example-gtk-gnome/greeter-session=lightdm-gtk-greeter/' \"$confFile\""
      "sed -i 's/#greeter-setup-script=/greeter-setup-script=\/usr\/bin\/numlockx on/' \"$confFile\""
      "systemctl enable lightdm.service")
      dmCmdTitles=("Setting type of greeter for the 'LightDM' DM"
          "Turning NumLock 'on' by default."
          "Enabling service for the 'LightDM' DM")
    ;;
    "LXDM")
      local confFile="/etc/lxdm/PostLogin"
      dmCmds=("echo \"source ~/.xinitrc\" >> \"${confFile}\""
      "systemctl enable lxdm.service")
      dmCmdTitles=("Appending sourcing of 'xinitrc' to LXDM's 'PostLogin' file in order to fire up the '$deOrWM' $aryKey"
      "Enabling service for the 'LXDM' DM")
    ;;
    "SDDM")
      setDispManCmdsForSDDM "dmCmds" "dmCmdTitles"
    ;;
    "XDM")
      local confFile="/home/${userName}/.xsession"
      local lines=("#!/bin/sh" ". ~/.xinitrc")

      concatStr=$(printf "\n%s" "${lines[@]}")
      dmCmds=("echo \"${concatStr:1}\" > \"$confFile\"" "chmod 775 $confFile" "chown ${userName}:users $confFile"
        "systemctl enable xdm-archlinux.service")
      dmCmdTitles=("Creating the 'xsession' file in order for 'XDM' be able to fire up the '$deOrWM' $aryKey"
        "Setting access permissions to the 'xsession' file" "Setting owner permissions to the 'xsession' file"
        "Enabling service for the 'XDM' DM")
    ;;
  esac

  local arrayLen=${#dmCmds[@]}
  local lastIdx=$(expr ${arrayLen} - 1)
  local keyNum=0
  for aryIdx in $(eval echo "{0..${lastIdx}}"); do
    local title="${dmCmdTitles[${aryIdx}]}"
    local cmdLine=$(printf "%s%s%s" "$title" "${FORM_FLD_SEP}" "${dmCmds[${aryIdx}]}")

    execCmds+=("$cmdLine")
    keyNum=$(expr ${keyNum} + 1)
    key=$(printf "#%d)" ${keyNum})
    keys+=("$key")
    if [ "$DIALOG" == "yad" ]; then
      keyDescs["$key"]=$(escapeSpecialCharacters "$title")
    else
      keyDescs["$key"]="$title"
    fi
  done

  concatStr=$(printf "\n%s" "${execCmds[@]}")
  echo -e "${concatStr:1}" > "$POST_INST_CMD_FILE"
}

#---------------------------------------------------------------------------------------
#      METHOD:                    setOptsForDispManConf
# DESCRIPTION: Set the options for the Linux or YAD dialogs to display confirmation for
#              installing and configuring a "Display Manager".
#  Required Params:
#    1)   dialogTitle - The string to be displayed at the top of the dialog box
#    2)   windowTitle - The string to be displayed in either the window/dialog's title
#                       bar, or on the backdrop that is at the very top of the screen.
#    3) dlgAssocArray - name of the associative array declared in the calling function
#---------------------------------------------------------------------------------------
setOptsForDispManConf() {
  local dialogTitle="$1 Confirmation"
  local windowTitle="$2"
  local -n dlgAssocArray="$3"
  local dmName="${POST_ASSOC_ARRAY["Display-Manager"]}"
  local dialogText="Confirm the installation and configuration of the '${dmName}' Display Manager (DM)"

  if [ "$DIALOG" == "yad" ]; then
    dlgAssocArray["width"]=675

    dlgAssocArray["image"]="${YAD_GRAPHICAL_PATH}/images/post-inst/conf-disp-man.png"

    dlgAssocArray["dialogTitle"]="$windowTitle"
    dlgAssocArray["helpDlgTitle"]="$dialogTitle"
    dialogTitle=$(printf "%10s%s" " " "$dialogTitle")
    local yadTextArray=("$dialogTitle" " " " " "$dialogText")
    dialogText=$(printf "\n%s" "${yadTextArray[@]}")
    dlgAssocArray["dialogText"]="$dialogText"

    local buttons=("Confirm" "Cancel" "Help")
    local concatStr=$(printf "$FORM_FLD_SEP%s" "${buttons[@]}")
    dlgAssocArray["buttons"]="${concatStr:${#FORM_FLD_SEP}}"

    local cols=("Key" "Title / Description")
    concatStr=$(printf "$FORM_FLD_SEP%s" "${cols[@]}")
    dlgAssocArray["cols"]="${concatStr:${#FORM_FLD_SEP}}"
    dlgAssocArray["sel-line"]="true"
  else
    dlgAssocArray["cancel-label"]="Cancel"
    dlgAssocArray["dialogBackTitle"]="$windowTitle"
    dlgAssocArray["dialogText"]="$dialogText"
    dlgAssocArray["dialogTitle"]="$dialogTitle"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                     setHelpTextAndTitle
# DESCRIPTION: Sets the title and text for the help section for the current task to exec.
#---------------------------------------------------------------------------------------
setHelpTextAndTitle() {
  local defVal="${piAssocArray["defaultVal"]}"
  local -n descAssocArray="${piAssocArray["descAssocArray"]}"
  local taskName="${descAssocArray["$defVal"]}"
  piAssocArray["helpTitle"]="Help:  ${defVal} - $taskName"

  piAssocArray["helpText"]=$(getHelpTextForDesktopConf "$taskName")
}

#---------------------------------------------------------------------------------------
#      METHOD:                       installCompMan
# DESCRIPTION: Install the group of packages for a "Composite Manager"
#  Required Params:
#    1)  optSel - the value of the option that was selected for this task
#    2) optDesc - the description/name of this task
#---------------------------------------------------------------------------------------
installCompMan() {
  local flagVal=0
  local -A funcParams=(["defaultChoice"]="${COMPOSITE_MANAGER[0]}" ["helpText"]=$(getHelpTextForCompositeManager)
                      ["menuOptsRef"]="COMPOSITE_MANAGER" ["optDescsRef"]="AUR_PCKG_DESCS"
                      ["cols"]="Radio Btn,Display Manager,Description" ["image"]="composite-manager.png"
                      ["dialogText"]="Choose the 'Composite Manager' to be installed:")

  setOptsForListTypeDlg "$1" "$2" "funcParams"

  selectFromRadioListDialog "funcParams"
  cleanDialogFiles
  if [ ${#mbSelVal} -gt 0 ]; then
    echo -e "${AUR_PCKGS["$mbSelVal"]}" > "$SEL_AUR_PCKGS"

    showProgBarAndTail
    rm -rf "$SEL_AUR_PCKGS"

    POST_ASSOC_ARRAY["Composite-Manager"]="$mbSelVal"
    flagVal=1
  else
    flagVal=-1
  fi

  updateSoftwareTaskChecklist "${piAssocArray["chklArray"]}" ${flagVal} "$1"
}

#---------------------------------------------------------------------------------------
#      METHOD:                    setOptsForListTypeDlg
# DESCRIPTION: Set the menu options that can be selected for a Linux or YAD dialogs
#              of either type checklist or radiolist.
#  Required Params:
#    1)        optSel - the value of the option that was selected for this task
#    2)       optDesc - the description/name of this task
#    3) dlgAssocArray - name of the associative array declared in the calling function
#---------------------------------------------------------------------------------------
setOptsForListTypeDlg() {
  local -n dlgAssocArray="$3"
  local cancelLabel="Skip"
  local windowTitle=$(printf "%s - %s" "${SEL_CHKLIST_OPT[0]}" "${SEL_CHKLIST_OPT[1]}")
  local dialogTitle=$(printf "%s - %s" "$1" "$2")
  local cols=()
  local dialogText=""
  local img="${dlgAssocArray["image"]}"
  local sep=","

  readarray -t cols <<< "${dlgAssocArray["cols"]//$sep/$'\n'}"
  unset dlgAssocArray["cols"]

  if [ "$DIALOG" == "yad" ]; then
    unset dlgAssocArray["dialogText"]
    dlgAssocArray["cancelLabel"]="$cancelLabel"
    dlgAssocArray["image"]="${YAD_GRAPHICAL_PATH}/images/post-inst/${img}"
    dlgAssocArray["dialogTitle"]="$windowTitle"
    dlgAssocArray["dialogTextHdr"]="$dialogTitle"
    local concatStr=$(printf "$FORM_FLD_SEP%s" "${cols[@]}")
    dlgAssocArray["columnNames"]=${concatStr:${#FORM_FLD_SEP}}
    dlgAssocArray["height"]=700
  else
    unset dlgAssocArray["image"]
    dlgAssocArray["cancel-label"]="$cancelLabel"
    dlgAssocArray["dialogBackTitle"]="$windowTitle"
    dlgAssocArray["dialogTitle"]="$dialogTitle"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                    installDeskIconThemes
# DESCRIPTION: Install the group of packages for the "Desktop & Icon Themes" selected
#  Required Params:
#    1)  optSel - the value of the option that was selected for this task
#    2) optDesc - the description/name of this task
#---------------------------------------------------------------------------------------
installDeskIconThemes() {
  local flagVal=0
  local -A funcParams=(["helpText"]=$(getHelpTextForThemes)
                      ["menuOptsRef"]="DESK_ICON_THEMES" ["optDescsRef"]="DIT_AUR_DESCS"
                      ["cols"]="Checkbox,Desktop/Icon Theme,Description" ["image"]="desktop-icon-themes.png"
                      ["dialogText"]="Choose the 'Desktop & Icon Themes' to install:")
  local keyDEorWM="${POST_ASSOC_ARRAY["DE-OR-WM"]}"
  local deOrWM="${POST_ASSOC_ARRAY["$keyDEorWM"]}"

  if [ "$deOrWM" == "KDE Plasma" ]; then
    funcParams["menuOptsRef"]="KDE_THEMES"
    funcParams["optDescsRef"]="KDE_AUR_DESCS"
    installSelectedPackages "$1" "$2" "funcParams"
  else
    installSelectedPackages "$1" "$2" "funcParams" "DIT_AUR_PCKGS"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                     installSelectedPackages
# DESCRIPTION: This method will display a Linux or YAD dialog of type checklist and
#              will either:
#                   A) Install the AUR packages that were selected and mark the
#                      current task as done.
#                   B) Mark the current task as skipped.
#  Required Params:
#    1)        optSel - the value of the option that was selected for this task
#    2)       optDesc - the description/name of this task
#    3) ispAssocArray - name of the associative array declared in the calling function
#   Optional Param:
#    4) name of the associative array containing the names of the AUR packages whose
#       keys match those of the selected values
#---------------------------------------------------------------------------------------
installSelectedPackages() {
  local flagVal=0
  local -n ispAssocArray="$3"

  setOptsForListTypeDlg "$1" "$2" "ispAssocArray"

  showCheckListDialog "funcParams"
  cleanDialogFiles

  if [ ${#mbSelVal} -gt 0 ]; then
    local sep="|"
    local selectedVals=()
    local aurPckgs=()

    if [ "$#" -gt 3 ]; then
      local -n aurPckgRef="$4"
      if [ "$DIALOG" == "yad" ]; then
        local cols=()
        readarray -t selectedVals <<< "$mbSelVal"
        for row in "${selectedVals[@]}"; do
          readarray -t cols <<< "${row//$sep/$'\n'}"
          aurPckgs+=("${aurPckgRef["${cols[1]}"]}")
        done
      else
        readarray -t selectedVals <<< "${mbSelVal//$sep/$'\n'}"
        for val in "${selectedVals[@]:1}"; do
          aurPckgs+=("${aurPckgRef["$val"]}")
        done
      fi
    else
      if [ "$DIALOG" == "yad" ]; then
        local cols=()
        readarray -t selectedVals <<< "$mbSelVal"
        for row in "${selectedVals[@]}"; do
          readarray -t cols <<< "${row//$sep/$'\n'}"
          aurPckgs+=("${cols[1]}")
        done
      else
        readarray -t selectedVals <<< "${mbSelVal//$sep/$'\n'}"
        for val in "${selectedVals[@]:1}"; do
          aurPckgs+=("$val")
        done
      fi
    fi

    local concatStr=$(printf " %s" "${aurPckgs[@]}")
    echo -e "${concatStr:1}" > "$SEL_AUR_PCKGS"

    showProgBarAndTail
    rm -rf "$SEL_AUR_PCKGS"

    INST_AUR_PCKGS["$2"]=$(echo -e "${concatStr:1}")
    declare -p INST_AUR_PCKGS > "${AUR_INSTALLED_PCKGS}"

    flagVal=1
  else
    flagVal=-1
  fi

  updateSoftwareTaskChecklist "${piAssocArray["chklArray"]}" ${flagVal} "$1"
}

#---------------------------------------------------------------------------------------
#      METHOD:                        installFonts
# DESCRIPTION: Install the group of packages for the "Fonts" selected
#  Required Params:
#    1)  optSel - the value of the option that was selected for this task
#    2) optDesc - the description/name of this task
#---------------------------------------------------------------------------------------
installFonts() {
  local flagVal=0
  local -A funcParams=(["helpText"]=$(getHelpTextForFonts)
                      ["menuOptsRef"]="FONTS" ["optDescsRef"]="FONT_AUR_DESCS"
                      ["cols"]="Checkbox,Font Packages,Description" ["image"]="fonts.png"
                      ["dialogText"]="Choose the 'Font packages' to install:")

  installSelectedPackages "$1" "$2" "funcParams" "FONT_AUR_PCKGS"
}

#---------------------------------------------------------------------------------------
#      METHOD:                     installAppLaunchers
# DESCRIPTION: Install the group of packages for the "Application Launchers" selected
#  Required Params:
#    1)  optSel - the value of the option that was selected for this task
#    2) optDesc - the description/name of this task
#---------------------------------------------------------------------------------------
installAppLaunchers() {
  local flagVal=0
  local -A funcParams=(["helpText"]=$(getHelpTextForAppLaunchers)
                      ["menuOptsRef"]="APPLICATION_LAUNCHERS" ["optDescsRef"]="AUR_PCKG_DESCS"
                      ["cols"]="Checkbox,App. Launcher,Description" ["image"]="app-launchers.png"
                      ["dialogText"]="Choose the 'Application Launchers' to install:")

  installSelectedPackages "$1" "$2" "funcParams" "AUR_PCKGS"
}

#---------------------------------------------------------------------------------------
#      METHOD:                    installAppMenuEditors
# DESCRIPTION: Install the group of packages for the "Application Menu Editors" selected
#  Required Params:
#    1)  optSel - the value of the option that was selected for this task
#    2) optDesc - the description/name of this task
#---------------------------------------------------------------------------------------
installAppMenuEditors() {
  local flagVal=0
  local -A funcParams=(["helpText"]=$(getHelpTextForAppMenuEditors)
                      ["menuOptsRef"]="APPLICATION_MENU_EDITORS" ["optDescsRef"]="AUR_PCKG_DESCS"
                      ["cols"]="Checkbox,AUR Package,Description" ["image"]="app-menu-editors.png"
                      ["dialogText"]="Choose the 'Application Menu Editors' to install:")

  local keyDEorWM="${POST_ASSOC_ARRAY["DE-OR-WM"]}"
  local deOrWM="${POST_ASSOC_ARRAY["$keyDEorWM"]}"

  if [ "$deOrWM" == "KDE Plasma" ]; then
    funcParams["menuOptsRef"]="KDE_APP_MENU_EDITOR"
    funcParams["optDescsRef"]="KDE_AUR_DESCS"
    installSelectedPackages "$1" "$2" "funcParams" "KDE_AUR_PCKGS"
  else
    installSelectedPackages "$1" "$2" "funcParams" "AUR_PCKGS"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                       installTaskbars
# DESCRIPTION: Install the group of packages for the "Taskbars" selected
#  Required Params:
#    1)  optSel - the value of the option that was selected for this task
#    2) optDesc - the description/name of this task
#---------------------------------------------------------------------------------------
installTaskbars() {
  local flagVal=0
  local -A funcParams=(["helpText"]=$(getHelpTextForTaskbars)
                      ["menuOptsRef"]="TASKBARS" ["optDescsRef"]="TASKBAR_AUR_DESCS"
                      ["cols"]="Checkbox,App. Launcher,Description" ["image"]="taskbars.png"
                      ["dialogText"]="Choose the 'Taskbars' to install:")

  installSelectedPackages "$1" "$2" "funcParams" "TASKBAR_AUR_PCKGS"
}

#---------------------------------------------------------------------------------------
#      METHOD:                      installAdminCUPS
# DESCRIPTION: Install the group of packages for the "Administer CUPS" packages selected
#  Required Params:
#    1)  optSel - the value of the option that was selected for this task
#    2) optDesc - the description/name of this task
#---------------------------------------------------------------------------------------
installAdminCUPS() {
  local flagVal=0
  local -A funcParams=(["helpText"]=$(getHelpTextForAdministerCUPS)
                      ["menuOptsRef"]="ADMINISTER_CUPS" ["optDescsRef"]="AUR_PCKG_DESCS"
                      ["cols"]="Checkbox,AUR Package,Description" ["image"]="admin-cups.png"
                      ["dialogText"]="Choose the 'Administer CUPS' packages to install:")

  local keyDEorWM="${POST_ASSOC_ARRAY["DE-OR-WM"]}"
  local deOrWM="${POST_ASSOC_ARRAY["$keyDEorWM"]}"

  if [ "$deOrWM" == "KDE Plasma" ]; then
    funcParams["menuOptsRef"]="KDE_ADMIN_CUPS"
    funcParams["optDescsRef"]="KDE_AUR_DESCS"
    installSelectedPackages "$1" "$2" "funcParams" "KDE_AUR_PCKGS"
  else
    installSelectedPackages "$1" "$2" "funcParams" "AUR_PCKGS"
  fi
}

#---------------------------------------------------------------------------------------
#  MAIN
#---------------------------------------------------------------------------------------
main() {
  local errMsgArray=("The \"$2\" software task must be the second software task to run and complete"
  "before the other category/software tasks in the \"Checklist\" tab can be selected!!!!!!!")
  local errMsg=$(printf " %s" "${errMsgArray[@]}")
  local loopFlag=$(echo 'true' && return 0)
  initVars "$@"

  local -A piAssocArray=(["listType"]="radiolist" ["optsArray"]="MENU_OPTS_ARRAY"
                        ["descAssocArray"]="DESC_ASSOC_ARRAY" ["chklArray"]="DESKTOP_ENV_CHECKLIST")
  if [ "$DIALOG" == "yad" ]; then
    piAssocArray+=(["tabName"]="${SEL_CHKLIST_OPT[1]}" ["tabNum"]=2
          ["tabColumns"]="Radio Button${FORM_FLD_SEP}Is Done${FORM_FLD_SEP}Desktop Env. Task${FORM_FLD_SEP}Description"
          ["nbButtons"]="$NB_BTNS"
          ["tabText"]="Click the \"Run\" button to execute the selected task!")
  fi
  while ${loopFlag}; do
    piAssocArray["defaultVal"]=$(getDefaultOption)
    setHelpTextAndTitle
    if [ "${piAssocArray["defaultVal"]}" == "done" ]; then
      clog_info "All tasks for the \"$2\" step are done!!!"
      loopFlag=$(echo 'false' && return 1)
    else
      setNextTaskForStep
      if [ ${#mbSelVal} -gt 0 ]; then
        local optDesc=()
        readarray -t optDesc <<< "${mbSelVal//$FORM_FLD_SEP/$'\n'}"
        if [ ${#optDesc[@]} -gt 1 ]; then
          if [ "${piAssocArray["defaultVal"]}" == "${optDesc[0]}" ]; then
            execDesktopEnvTask "${optDesc[0]}" "${optDesc[1]}"
          else
            local warnMsg=$(getWarnMsg "piAssocArray")
            showWarningDialog "Selection of \"$2\" task!" "$warnMsg"
          fi
        fi
      else
        loopFlag=$(echo 'false' && return 1)
      fi
    fi
  done

  updatePostInstAssocArray "${piAssocArray["chklArray"]}"

  if [ ${DESKTOP_ENV_CHECKLIST[0]} -gt 0 ]; then
    rm -rf ${DATA_DIR}/software/YAD-*
    rm -rf ${POST_INST_YAD_TABS}
  fi
}

main "$@"
exit 0
