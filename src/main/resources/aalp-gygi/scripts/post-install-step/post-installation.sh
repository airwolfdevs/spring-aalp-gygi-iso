#!/bin/bash

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# globals
#===============================================================================
CUR_DIR="${PWD}"
INC_FILE=$(echo "$CUR_DIR/inc/post-inst-inc.sh")
MENU_CONSTS_FILE=$(echo "$CUR_DIR/inc/post-inst/soft-cats.sh")

source "$INC_FILE"
source "$MENU_CONSTS_FILE"
source "$GRAPHICAL_PATH/post-install-inc.sh"

declare BLOCK_DEVICE=""
declare POST_INST_CHECKLIST=(0)
declare -A POST_ASSOC_ARRAY=()

source -- "$ASSOC_ARRAY_FILE"

if [ -f "$POST_ASSOC_ARRAY_FILE" ]; then
  source -- "$POST_ASSOC_ARRAY_FILE"
fi
#===============================================================================
# functions
#===============================================================================

#---------------------------------------------------------------------------------------
#      METHOD:                       initVars
# DESCRIPTION: Initialize the global variables.
#---------------------------------------------------------------------------------------
initVars() {

  if [ -f "$POST_ASSOC_ARRAY_FILE" ] && [ ${POST_ASSOC_ARRAY["$POST_INST_CHKL_KEY"]+_} ]; then
    readarray -t POST_INST_CHECKLIST <<< "${POST_ASSOC_ARRAY["$POST_INST_CHKL_KEY"]//$FORM_FLD_SEP/$'\n'}"
  else
    for menuOpt in "${MENU_OPTS_ARRAY[@]}"; do
      POST_INST_CHECKLIST+=(0)
    done

    local concatStr=$(printf "${FORM_FLD_SEP}%d" "${POST_INST_CHECKLIST[@]}")
    POST_ASSOC_ARRAY["$POST_INST_CHKL_KEY"]=$(echo "${concatStr:${#FORM_FLD_SEP}}")

    declare -p POST_ASSOC_ARRAY > "$POST_ASSOC_ARRAY_FILE"
  fi

  rotateLogs
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    findFirstStepNotDone
# DESCRIPTION: Find the array index for the first step that is marked as not done
#              or skipped (i.e. value in checklist is 0)
#---------------------------------------------------------------------------------------
function findFirstStepNotDone() {
  local lastIdx=$(expr ${#POST_INST_CHECKLIST[@]} - 1)
  local aryIdx=-1
  for aryIdx in $(eval echo "{0..${lastIdx}}"); do
    local idxVal=${POST_INST_CHECKLIST[${aryIdx}]}
    if isValZero ${idxVal} &> /dev/null; then
      break
    fi
  done

  echo ${aryIdx}
}

#---------------------------------------------------------------------------------------
#      METHOD:                      getConfToContinue
# DESCRIPTION: Get confirmation of whether to:
#                 A)  continue with the post-instalation step
#                 B)  skip this step
#---------------------------------------------------------------------------------------
getConfToContinue() {
  local dialogTitle="Confirmation"
  local dialogText=$(getTextForPostInst)
  local helpText=$(getTextForPostInstStep)

  showPostInstOption "$POST_INST_DIALOG_BACK_TITLE" "$dialogTitle" "$dialogText" "$helpText"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     getTextForPostInst
# DESCRIPTION: Text to display in the text area when choosing either to continue or skip
#      RETURN: concatenated string
#---------------------------------------------------------------------------------------
function getTextForPostInst() {
  local extraDashes=""
  local lclTextArray=("Congratulations! You have successfully installed a minimal command line"
  "Arch Linux onto your system.  The installer will now help to create an awesome desktop"
  "environment with sound, video, a choice of window managers, and more.  For the most part,"
  "this step abides by what is stated in the Arch wiki's general recommendations article"
  "for improving and adding functionality to the installed Arch system.")
  local concatStr=$(printf " %s" "${lclTextArray[@]}")
  concatStr=${concatStr:1}
  if [ "$DIALOG" == "yad" ]; then
    extraDashes="-------------"
  fi

  lclTextArray=("$concatStr")
  if [ "$DIALOG" == "yad" ]; then
    local dlgBorder="----------------------------------------------------------------------"
    lclTextArray+=("$dlgBorder" " ")
  else
    lclTextArray+=("$DIALOG_BORDER" " ")
  fi
  lclTextArray+=("General recommendations" "-----------------------"
    "https://wiki.archlinux.org/index.php/General_recommendations"
  )

  local dialogText=$(getTextForDialog "${lclTextArray[@]}")

  echo "$dialogText"
}

#---------------------------------------------------------------------------------------
#      METHOD:                         rotateLogs
# DESCRIPTION: Rotate the log files so that the new entries reflect the installation
#              of the software.
#---------------------------------------------------------------------------------------
rotateLogs() {
  local ary=()
  readarray -t ary <<< "${INSTALLER_LOG//\//$'\n'}"
  local fileName="${DATA_DIR}/steps1-3_${ary[-1]}"
  if [ ! -f "$fileName" ]; then
    mv "$INSTALLER_LOG" "$fileName"
    clog_warn "Rotated log file for steps #1, #2, #3" >> "$INSTALLER_LOG"
  fi

}

#---------------------------------------------------------------------------------------
#      METHOD:                      showDialogOfSelCat
# DESCRIPTION: Display either linux "dialog" or a yad "notebook" dialog displaying the
#              menu options of the selected category/software task.
#---------------------------------------------------------------------------------------
showDialogOfSelCat() {
  local scriptName=$(getNameOfScript)
  local catScriptName=$(getNameOfScriptForCat)

  if [ "$DIALOG" == "yad" ]; then
    showNotebookOfSelCat
  fi
  if [ ${#catScriptName} -gt 0 ]; then
    ${scriptName} "${SEL_CHKLIST_OPT[0]}${FORM_FLD_SEP}${SEL_CHKLIST_OPT[1]}" "$catScriptName"
  else
    ${scriptName} "${SEL_CHKLIST_OPT[0]}${FORM_FLD_SEP}${SEL_CHKLIST_OPT[1]}"
  fi

  updatePostInstChecklists
}

#---------------------------------------------------------------------------------------
#      METHOD:                    showNotebookOfSelCat
# DESCRIPTION: Display a yad "notebook" dialog with the second tab showing the options
#              of the selected category/software task.
#---------------------------------------------------------------------------------------
showNotebookOfSelCat() {
  local scriptName=$(getNameOfScript)
  local srcFile=$(printf "${NB_TAB_DATA_FNS["tabData"]}" 1)

  setDataForPostInstTabNB

  if [ -f "$srcFile" ]; then
    local path=()
    local sep="/"
    readarray -t path <<< "${srcFile//$sep/$'\n'}"
    path[1]="${DATA_DIR}/software"
    local destFile=$(printf "/%s" "${path[@]:1}")
    destFile=${destFile:1}

    mv "$srcFile" "$destFile"
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                       getNameOfScript
# DESCRIPTION: Get the name of the script that matches the selected checklist category
#      RETURN: string
#---------------------------------------------------------------------------------------
function getNameOfScript() {
  local scriptName=""
  case "${SEL_CHKLIST_OPT[-1]}" in
    "Basic Setup")
      scriptName="basic-setup.sh"
    ;;
    "Desktop Configuration")
      scriptName="desktop-config.sh"
    ;;
    "Accessory Software")
      scriptName="cat-soft-task.sh"
    ;;
    "Application & Web Servers")
      scriptName="cat-soft-task.sh"
    ;;
    "Development Software")
      scriptName="cat-soft-task.sh"
    ;;
    "Database Software")
      scriptName="cat-soft-task.sh"
    ;;
    "Documents & Texts Software")
      scriptName="cat-soft-task.sh"
    ;;
    "Internet Software")
      scriptName="cat-soft-task.sh"
    ;;
    "Multimedia Software")
      scriptName="cat-soft-task.sh"
    ;;
    "System & Utility Software")
      scriptName="cat-soft-task.sh"
    ;;
    "Security Software")
      scriptName="cat-soft-task.sh"
    ;;
    "Software Extras")
      scriptName="cat-soft-task.sh"
    ;;
    *)
      clog_error "No case statement defined for \"${SEL_CHKLIST_OPT[-1]}\"!!!!!!"
      exit 1
    ;;
  esac

  scriptName="./post-install-step/$scriptName"
  if [ ! -f "$scriptName" ]; then
    clog_error "The bash script \"$scriptName\" does NOT exist!!!!!!"
    exit 1
  fi
  echo "$scriptName"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                       getNameOfScript
# DESCRIPTION: Get the prefix to the name of the script for a category
#      RETURN: string
#---------------------------------------------------------------------------------------
function getNameOfScriptForCat() {
  local scriptName=""
  case "${SEL_CHKLIST_OPT[-1]}" in
    "Accessory Software")
      scriptName="cat-accessory-soft.sh"
    ;;
    "Application & Web Servers")
      scriptName="cat-servers-soft.sh"
    ;;
    "Database Software")
      scriptName="cat-db-soft.sh"
    ;;
    "Development Software")
      scriptName="cat-dev-soft.sh"
    ;;
    "Documents & Texts Software")
      scriptName="cat-doc-text-soft.sh"
    ;;
    "Internet Software")
      scriptName="cat-internet-soft.sh"
    ;;
    "Multimedia Software")
      scriptName="cat-multimedia-soft.sh"
    ;;
    "System & Utility Software")
      scriptName="cat-sys-util-soft.sh"
    ;;
    "Security Software")
      scriptName="cat-security.sh"
    ;;
    "Software Extras")
      scriptName="cat-soft-extras.sh"
    ;;
  esac

  echo "$scriptName"
}

#---------------------------------------------------------------------------------------
#      METHOD:                  updatePostInstChecklists
# DESCRIPTION: Update the checklists for the post-installation step and
#              its categories/tasks
#---------------------------------------------------------------------------------------
updatePostInstChecklists() {
  if [ -f "$UPD_ASSOC_ARRAY_FILE" ]; then
    source -- "${UPD_ASSOC_ARRAY_FILE}"

    for key in "${!updAssocArray[@]}"; do
      POST_ASSOC_ARRAY["$key"]=$(echo "${updAssocArray[$key]}")
    done

    rm -rf "${UPD_ASSOC_ARRAY_FILE}"

    declare -p POST_ASSOC_ARRAY > "$POST_ASSOC_ARRAY_FILE"
    readarray -t POST_INST_CHECKLIST <<< "${POST_ASSOC_ARRAY["$POST_INST_CHKL_KEY"]//$FORM_FLD_SEP/$'\n'}"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                    showSkipButtonWarning
# DESCRIPTION: Show a warning message that the Skip button was clicked.
#---------------------------------------------------------------------------------------
showSkipButtonWarning() {
  local -A funcParams=()
  local xorgTask="${MENU_OPTS_ARRAY[1]}"
  local dialogTitle="Skipping of the remaining software tasks!"
  local firstLine=""
  local extraDashes=""
  local skipText=$(getSkipText)

  if [ "$DIALOG" == "yad" ]; then
    extraDashes="---------"
    firstLine="The \"Skip\" button was clicked.  The installer $skipText"
  else
    firstLine="<Skip> was selected.  The installer $skipText"
  fi

  local msgArray=("$firstLine" "${DIALOG_BORDER}${extraDashes}" " "
    "Are you sure you want to continue with skipping the remaining software tasks that are either NOT done nor skipped?")
  funcParams["dialogText"]=$(getTextForDialog "${msgArray[@]}")

  if [ "$DIALOG" == "yad" ]; then
    funcParams["dialogTitle"]="${POST_INST_NB_OPTS["title"]}"
    funcParams["dialogTextHdr"]="$dialogTitle"
  else
    funcParams["dialogBackTitle"]="${POST_INST_NB_OPTS["title"]}"
    funcParams["dialogTitle"]=$(echo "WARNING: $dialogTitle")
  fi

  showSkipBtnConfWarning "funcParams"
}

#---------------------------------------------------------------------------------------
#      METHOD:                     skipRemainingTasks
# DESCRIPTION: Set the remaining tasks as skipped for the ones that are marked as either
#              not done or skipped (i.e. value in checklist is 0)
#  Required Params:
#      1)   startIdx - index of the first task that has a value of 0 in the checklist
#---------------------------------------------------------------------------------------
skipRemainingTasks() {
  local startIdx=$1
  local lastIdx=$(expr ${#POST_INST_CHECKLIST[@]} - 1)
  for aryIdx in $(eval echo "{${startIdx}..${lastIdx}}"); do
    local idxVal=${POST_INST_CHECKLIST[${aryIdx}]}
    if isValZero ${idxVal} &> /dev/null; then
      POST_INST_CHECKLIST[${aryIdx}]=-1
    fi
  done

  local concatStr=$(printf "${FORM_FLD_SEP}%d" "${POST_INST_CHECKLIST[@]}")
  POST_ASSOC_ARRAY["$POST_INST_CHKL_KEY"]=$(echo "${concatStr:${#FORM_FLD_SEP}}")
  declare -p POST_ASSOC_ARRAY > "$POST_ASSOC_ARRAY_FILE"

  varMap["POST-INST"]="All Done"
  updateAssocArrayFile
}

#---------------------------------------------------------------------------------------
#      METHOD:                    updateAssocArrayFile
# DESCRIPTION: Update the associative array declared in the Installation Guide script
#---------------------------------------------------------------------------------------
updateAssocArrayFile() {
  local -A updVarMap=()
  for key in "${!varMap[@]}"; do
    updVarMap["$key"]=$(echo "${varMap[$key]}")
  done

  declare -p updVarMap > "${UPD_ASSOC_ARRAY_FILE}"
}

#---------------------------------------------------------------------------------------
#  MAIN
#---------------------------------------------------------------------------------------
main() {
  initVars

  while true; do
    local stepNum=$(findFirstStepNotDone)

    if [ ${stepNum} -gt -1 ]; then
      case ${stepNum} in
        0)
          getConfToContinue
          cleanDialogFiles
          if ${yesNoFlag}; then
            showProgBarAndTail
            POST_INST_CHECKLIST[0]=1
            local concatStr=$(printf "${FORM_FLD_SEP}%d" "${POST_INST_CHECKLIST[@]}")
            POST_ASSOC_ARRAY["POST_INST_CHECKLIST"]=$(echo "${concatStr:${#FORM_FLD_SEP}}")
            declare -p POST_ASSOC_ARRAY > "$POST_ASSOC_ARRAY_FILE"
          else
            varMap["POST-INST"]="Skipped"
            updateAssocArrayFile
            break
          fi
        ;;
        *)
          local aryIdx=$(expr ${stepNum} - 1)
          local -A piAssocArray=(["defaultVal"]="${MENU_OPTS_ARRAY[${aryIdx}]}" ["optsArray"]="MENU_OPTS_ARRAY"
                                 ["descAssocArray"]="MENU_DESC_ASSOC_ARRAY" ["chklArray"]="POST_INST_CHECKLIST")
          if [ ${stepNum} -gt 2 ]; then
            piAssocArray["addSkipBtn"]="true"
          fi

          if [ ${POST_ASSOC_ARRAY["nextSoftCatTask"]+_} ]; then
            SEL_CHKLIST_OPT=()
            SEL_CHKLIST_OPT+=("${POST_ASSOC_ARRAY["nextSoftCatTask"]}")
            SEL_CHKLIST_OPT[1]="${MENU_DESC_ASSOC_ARRAY["${SEL_CHKLIST_OPT[0]}"]}"
            unset POST_ASSOC_ARRAY["nextSoftCatTask"]
            declare -p POST_ASSOC_ARRAY > "$POST_ASSOC_ARRAY_FILE"
          else
            piAssocArray["helpText"]=$(getHelpTextForChecklist)
            showPostInstallChecklist
            cleanDialogFiles
            if [ ${#SEL_CHKLIST_OPT[@]} -gt 0 ]; then
              #### Need to do this for YAD dialogs because of possible special characters
              SEL_CHKLIST_OPT[1]="${MENU_DESC_ASSOC_ARRAY["${SEL_CHKLIST_OPT[0]}"]}"
            fi
          fi

          if [ ${#SEL_CHKLIST_OPT[@]} -gt 0 ]; then
            piAssocArray["defaultVal"]="${SEL_CHKLIST_OPT[0]}"
            showDialogOfSelCat
          else
            if [ ${piAssocArray["skipBtn"]+_} ]; then
              showSkipButtonWarning
              if ${yesNoFlag}; then
                skipRemainingTasks ${stepNum}
                break
              fi
            else
              break
            fi
          fi
        ;;
      esac
    else
      varMap["POST-INST"]="All Done"
      updateAssocArrayFile
      break
    fi
  done

  if [ "$DIALOG" == "yad" ]; then
    rm -rf ${DATA_DIR}/software/YAD-*
    rm -rf ${POST_INST_YAD_TABS}
  fi
}

main "$@"
exit 0
