#!/bin/bash
#set -e

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# globals
#===============================================================================
CUR_DIR="${PWD}"
INC_FILE=$(echo "$CUR_DIR/inc/inst_inc.sh")

source "$INC_FILE"
source "$GRAPHICAL_PATH/pre-install-inc.sh"

declare BLOCK_DEVICE=""
declare DIALOG_BACK_TITLE="Step #1:  Pre-installation"

declare KBL_ISO_CODES=(
    "BY" "BE" "BR" "BG" "CA"
    "KY" "CF" "CR" "CZ" "DK"
    "SV" "ET" "FI" "FR" "DE"
    "GR" "HU" "IS" "IL" "IT"
    "JP" "LA" "LV" "LT" "MK"
    "NL" "NO" "PL" "PT" "RO"
    "RU" "SG" "SK" "ES" "SR"
    "SE" "TJ" "TT" "TR" "UA"
    "UK" "US")

declare -A KBL_OTHER=(["ANSI-dvorak"]="Original Dvorak layout" ["applkey"]=" "
    ["azerty"]="Specific layout for the characters of the Latin alphabet"
    ["backspace"]="Extended layout" ["bashkir"]="QWERTY layout"
    ["carpalx"]="A layout algorithmically generated based on heuristics."
    ["colemak"]="A modern alternative to the QWERTY and Dvorak layouts."
    ["ctrl"]=" " ["defkeymap"]="QWERTY layout" ["dvorak"]="Dvorak Simplified Keyboard"
    ["emacs"]="QWERTY layout" ["emacs2"]="QWERTY layout" ["euro"]="Extended layout"
    ["euro1"]="Extended layout" ["euro2"]="Extended layout" ["kazakh"]="QWERTY layout"
    ["keypad"]="Extended layout" ["kyrgyz"]="QWERTY layout" ["pc110"]="QWERTY layout"
    ["slovene"]="QWERTZ layout" ["sundvorak"]=" " ["sunkeymap"]=" " ["unicode"]=" "
    ["wangbe"]=" " ["wangbe2"]=" " ["windowkeys"]="Enable VT switching (like ALT+Left/Right)")

declare -A OTHER_KBL_TITLES=(
    ["dvorak-l"]="Left handed Dvorak"
    ["dvorak-r"]="Right handed Dvorak"
    ["mac-dvorak"]="Dvorak for the Mac OS"
    ["mac-euro"]="Extended layout for the Mac OS"
    ["mac-euro2"]="Extended layout for the Mac OS"
    ["mac-template"]=" ")

declare -A varMap
source -- "$ASSOC_ARRAY_FILE"

#===============================================================================
# functions
#===============================================================================

#---------------------------------------------------------------------------------------
#      METHOD:                   setBlockDevice
# DESCRIPTION: Sets the device to the global variable
#---------------------------------------------------------------------------------------
setBlockDevice() {
  BLOCK_DEVICE=$(echo "${varMap["BLOCK-DEVICE"]}")
}

#---------------------------------------------------------------------------------------
#      METHOD:                    setKeyboardLayout
# DESCRIPTION: Set the keyboard configuration/layout
#---------------------------------------------------------------------------------------
setKeyboardLayout() {
  local defaultLayout="us"
  local kblOther=("${!KBL_OTHER[@]}")
  local -A kblMap=()
  local dialogTitle="Keyboard Configuration"
  local dlgData=()
  local defCtryCode=$($JSON_PROCESSOR -r '.countryCode' ${DATA_DIR}/geoLoc.json)

  if [ "$DIALOG" == "yad" ]; then
    defaultLayout=$(setxkbmap -print | awk -F"+" '/xkb_symbols/ {print $2}')
  fi

  setKeyboardLayoutOpts

  if [ ${#defCtryCode} -lt 1 ] || [ ! ${kblMap["$defCtryCode"]+_} ]; then
    defCtryCode="US"
  fi

  kblOther=($(printf "%s\n" "${kblOther[@]}" | sort -u))

  getKeyboardLayout "$DIALOG_BACK_TITLE" "$dialogTitle" "$defaultLayout" "$defCtryCode"

  if [ "$mbSelVal" != "$defaultLayout" ]; then
    loadkeys "$mbSelVal"
  fi

  varMap["KEYMAP"]=$(echo "$mbSelVal")
}

#---------------------------------------------------------------------------------------
#      METHOD:                    setKeyboardLayoutOpts
# DESCRIPTION: Set the arrays that contain the keyboard layouts
#---------------------------------------------------------------------------------------
setKeyboardLayoutOpts() {
  local keyboardLayouts=()
  local ctryCode=""

  mapfile -t keyboardLayouts < <( localectl list-keymaps )
  for kbl in "${keyboardLayouts[@]}"; do
    if [[ "$kbl" =~ "amiga" ]] || [[ "$kbl" =~ "atari" ]] || [[ "$kbl" =~ "dvorak-" ]] \
       || [[ "$kbl" =~ "mac-" ]] || [[ "$kbl" =~ "sun-" ]] || [[ "$kbl" =~ "sunt" ]]; then

      IFS='-' read -d '' -a kblSplit <<< "$kbl"

      if [ ${#kblSplit[1]} -gt 2 ] || [ ${#kblSplit[1]} -lt 2 ]; then
        ctryCode=$(echo "${kblSplit[1]}" | tr [a-z] [A-Z])
      else
        ctryCode=$(echo "${kblSplit[1]:0:2}" | tr [a-z] [A-Z])
      fi
      if [ ${ISO_CODE_NAMES["$ctryCode"]+_} ]; then
        kblMap["$ctryCode"]+=$(echo ";$kbl")
      else
        kblOther+=("$kbl")
      fi
    elif [ ! ${KBL_OTHER["$kbl"]+_} ]; then
      ctryCode=$(echo "${kbl:0:2}" | tr [a-z] [A-Z])
      if [ ${ISO_CODE_NAMES["$ctryCode"]+_} ]; then
        kblMap["$ctryCode"]+=$(echo ";$kbl")
      else
        kblOther+=("$kbl")
      fi
    fi
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                      executeScript
# DESCRIPTION: Executes the script specified by the parameter
#  Required Params:
#      1) installStep - string that is mapped to the name of the script to execute
#   Optional Param:
#      2) array of arguments for the script to be called
#---------------------------------------------------------------------------------------
executeScript() {
  local installStep="$1"
  local scriptName=""

  case "$installStep" in
    "step#1.3")
      scriptName="network-configuration.sh $2 1"
    ;;
    "step#1.5")
      scriptName="create-partition-layout.sh"
    ;;
    "step#1.6")
      scriptName="format-mount-devices.sh 'Format'"
    ;;
    "step#1.7")
      scriptName="format-mount-devices.sh 'Mount'"
    ;;
  esac

  declare -p varMap > "$ASSOC_ARRAY_FILE"
  eval "./$scriptName"
  local exitStatus=$?
  if [ -f "${UPD_ASSOC_ARRAY_FILE}" ]; then
    source -- "${UPD_ASSOC_ARRAY_FILE}"

    varMap=()
    for key in "${!updVarMap[@]}"; do
      varMap["$key"]=$(echo "${updVarMap[$key]}")
    done

    rm -rf "${UPD_ASSOC_ARRAY_FILE}"
  fi

  varMap["EXIT_STATUS"]=$exitStatus
}

#---------------------------------------------------------------------------------------
#      METHOD:                      configureNetwork
# DESCRIPTION: Wrapper to call the "network-configuration.sh" script
#  Required Params:
#      1) stepTask - the step/task number
#---------------------------------------------------------------------------------------
configureNetwork() {
  local stepTask="$1"
  local wiredConnFlag=`ip link | grep "ens\|eno\|enp" | awk '{print $2}'| sed 's/://' | sed '1!d'`
  local wirelessConnFlag=`ip link | grep wlp | awk '{print $2}'| sed 's/://' | sed '1!d'`

  if [ ${#wiredConnFlag} -lt 1 ]; then
    wiredConnFlag=" "
  fi

  if [ ${#wirelessConnFlag} -lt 1 ]; then
    wirelessConnFlag=" "
  fi

  local params=("$DIALOG_BACK_TITLE" "$wiredConnFlag" "$wirelessConnFlag")
  local args=$(printf " '%s'" "${params[@]}")
  args=${args:1}

  executeScript "$stepTask" "$args"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                       getErrorMessage
# DESCRIPTION: Get the appropriate error message based on the operation that failed
#      RETURN: concatenated string
#  Required Params:
#      1) "format" or "mount"
#---------------------------------------------------------------------------------------
function getErrorMessage() {
  local msgArray=("Unable to '$1' the")
  if [ "${varMap["PART-LAYOUT"]}" == "${PART_LAYOUTS[0]}" ]; then
    msgArray+=("partitions!  Check")
  else
    msgArray+=("logical volumes and/or partitions!  Check")
  fi

  msgArray+=("the log file $INSTALLER_LOG to determine the root cause of the problem.")

  local concatStr=$(printf " %s" "${msgArray[@]}")
  echo "${concatStr:1}"
}

#---------------------------------------------------------------------------------------
#  MAIN
#---------------------------------------------------------------------------------------
main() {
  local stepTaskArray=("$@")
  local existFlag=0;
  setBlockDevice

  for stepTask in "${stepTaskArray[@]}"; do

    stepTask=$(trimString "$stepTask")

    case "$stepTask" in
      "step#1.1")
        setKeyboardLayout
      ;;
      "step#1.3")
        configureNetwork "$stepTask"
      ;;
      "step#1.5")
        executeScript "$stepTask"
        existFlag=$(isPartitioned "$BLOCK_DEVICE")
        if ! ${existFlag}; then
          unset varMap["PART-LAYOUT"]
          break
        fi
      ;;
      "step#1.6")
        if [ ${varMap["PART-LAYOUT"]+_} ]; then
          executeScript "$stepTask"
          local scriptStatus=${varMap["EXIT_STATUS"]}
          if [ $scriptStatus -gt 1 ]; then
            break
          elif [ $scriptStatus -lt 1 ]; then
            existFlag=$(isFmtOrMnt 1 "$BLOCK_DEVICE")
            if ${existFlag}; then
              varMap["FMT-FS"]=$(echo "done")
            else
              local msg=$(getErrorMessage "format")
              showErrorDialog "$DIALOG_BACK_TITLE" "Devices are NOT assigned an FST" "$msg"
              exit 1
            fi
          fi
        else
          showErrorDialog "Must first create logical volumes and/or partitions to format!"
        fi
      ;;
      "step#1.7")
        if [ ${varMap["FMT-FS"]+_} ]; then
          executeScript "$stepTask"
          local scriptStatus=${varMap["EXIT_STATUS"]}
          if [ $scriptStatus -gt 1 ]; then
            break
          elif [ $scriptStatus -lt 1 ]; then
            existFlag=$(isFmtOrMnt 2 "$BLOCK_DEVICE")
            if ${existFlag}; then
              varMap["MNT-FS"]=$(echo "done")
            else
              local msg=$(getErrorMessage "mount")
              showErrorDialog "$DIALOG_BACK_TITLE" "Devices are NOT mounted" "$msg"
              exit 10
            fi
          fi
        else
          showErrorDialog "Must first create & format logical volumes and/or partitions to mount!"
        fi
      ;;
      *)
        echo "invalid_option:[$stepTask]"
      ;;
    esac
  done

  unset varMap["EXIT_STATUS"]

  local -A updVarMap=()
  for key in "${!varMap[@]}"; do
    updVarMap["$key"]=$(echo "${varMap[$key]}")
  done

  declare -p updVarMap > "${UPD_ASSOC_ARRAY_FILE}"
}

main "$@"
exit 0
