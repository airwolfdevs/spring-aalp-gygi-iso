#!/bin/bash
#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================
CUR_DIR="${PWD}"
INC_FILE=$(echo "$CUR_DIR/inc/inst_inc.sh")
MAN_DEV_INC_FILE=$(echo "$CUR_DIR/inc/manage-device-funcs.sh")

source "$INC_FILE"
source "$MAN_DEV_INC_FILE"
source "$GRAPHICAL_PATH/lvm-part-inc.sh"

declare -A varMap
declare UEFI_FLAG=0
declare BLOCK_DEVICE=""
declare BLOCK_DEVICE_SIZE=""
declare HR_BLOCK_DEVICE_SIZE=""
declare PHYSICAL_VOLUME=""
declare PHYSICAL_VOL_SIZE=""
declare DEVICE_TYPE=""
declare PART_TBL_TYPE=""
declare DIALOG_BACK_TITLE="Semi-Auto Partitioner"
declare EFI_PART_NAME="EFI System"
declare ROOT_PART_NAME="root-part"
declare HOME_PART_NAME="home-part"
declare VAR_PART_NAME="var-part"
declare -A HELP_TEXT_OPTS=()
source -- "$ASSOC_ARRAY_FILE"
declare ADD_CUST_TEXT_ARRAY=(
"      choose the name, other than \"var\" & \"home\", and allocation size"
"      to give more flexibility (i.e. backup operations) and not fill up"
"      other discrete partitions like '/' or home."
)
declare ADD_SWAP_TEXT_ARRAY=(
"      can take the form of a discrete partition or a file. Swap space can be"
"      used for two purposes:"
"      A) extend the virtual memory beyond the installed physical memory"
"         (RAM), a.k.a \"enable swap\""
"      B) for suspend-to-disk support."
)
declare ADD_VAR_TEXT_ARRAY=(
"      will store variable data such as spool directories, files, etc. and"
"      help avoid running out of disk space."
)
declare REMOVE_TEXT_ARRAY=(
"      allows certain discrete devices to be removed from the partition"
"      table that are not required by the system (i.e. root, etc.)"
)

#---------------------------------------------------------------------------------------
#      METHOD:                       initVars
# DESCRIPTION: Initialize the global variables.
#---------------------------------------------------------------------------------------
initVars() {
  varMap["PART-LAYOUT"]="$1"
  if [ "${varMap[BOOT-MODE]}" == "UEFI" ]; then
    UEFI_FLAG=$(echo 'true' && return 0)
  else
    UEFI_FLAG=$(echo 'false' && return 1)
  fi

  BLOCK_DEVICE=$(echo "${varMap["BLOCK-DEVICE"]}")
  if [ ${varMap["BLOCK_DEVICE_SIZE"]+_} ]; then
    HR_BLOCK_DEVICE_SIZE=$(echo "${varMap["BLOCK_DEVICE_SIZE"]}")
    BLOCK_DEVICE_SIZE=$(humanReadableToBytes "$HR_BLOCK_DEVICE_SIZE")
  else
    BLOCK_DEVICE_SIZE=$(blockdev --getsize64 "$BLOCK_DEVICE")
    HR_BLOCK_DEVICE_SIZE=$(bytesToHumanReadable "$BLOCK_DEVICE_SIZE")
  fi
  DEVICE_TYPE="Partition"

  if [ ! ${varMap["PART-TBL-TYPE"]+_} ]; then
    PART_TBL_TYPE=$(parted "$BLOCK_DEVICE" -s print 2> /dev/null | grep 'Partition Table' | awk '{print $3}')
    if [ "$PART_TBL_TYPE" == "unknown" ]; then
      sgdisk -Z "$BLOCK_DEVICE"
      parted "$BLOCK_DEVICE" mklabel gpt
      PART_TBL_TYPE=$(parted "$BLOCK_DEVICE" -s print 2> /dev/null | grep 'Partition Table' | awk '{print $3}')
    fi
    varMap["PART-TBL-TYPE"]=$(echo "$PART_TBL_TYPE")
  else
    PART_TBL_TYPE=$(echo "${varMap[PART-TBL-TYPE]}")
  fi

  local key=""
  for opt in "${DEVICE_MANAGEMENT_OPTS[@]}"; do
    key=$(echo "${opt}Text")
    case "$opt" in
      "addCustom")
        HELP_TEXT_OPTS["$key"]=$(getTextForDialog "${ADD_CUST_TEXT_ARRAY[@]}")
      ;;
      "addSwap")
        HELP_TEXT_OPTS["$key"]=$(getTextForDialog "${ADD_SWAP_TEXT_ARRAY[@]}")
      ;;
      "addVAR")
        HELP_TEXT_OPTS["$key"]=$(getTextForDialog "${ADD_VAR_TEXT_ARRAY[@]}")
      ;;
      "remove")
        HELP_TEXT_OPTS["$key"]=$(getTextForDialog "${REMOVE_TEXT_ARRAY[@]}")
      ;;
    esac
  done

  if [[ ! -f "$DIALOG_IMG_ICON_FILE" ]]; then
    echo "${PART_DIALOG_IMAGE},${PART_WINDOW_ICON}" > "$DIALOG_IMG_ICON_FILE"
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                     getNextStep
# DESCRIPTION: Determine the next step in managing the logical volumes
#      RETURN: string containing the next step key
#---------------------------------------------------------------------------------------
function getNextStep() {
  local stepNum=5
  local nextStep=""
  local numLines=0
  local rows=()
  local cols=()

  readarray -t rows < "$PARTITION_TABLE_FILE"
  local len=${#rows[@]}
  if [ $len -lt 1 ]; then
    if ${UEFI_FLAG}; then
      stepNum=1
    else
      stepNum=2
    fi
  else
    for (( aryIdx = len-1; aryIdx > -1; aryIdx-- )); do
      readarray -t cols <<< "${rows[$aryIdx]//$'\t'/$'\n'}"
      case "${cols[1]}" in
        "$BOOT_PART_NAME")
          stepNum=3
          break
        ;;
        "$EFI_PART_NAME")
          if [ "${varMap["BOOT-DEVICE"]}" == "Skip" ]; then
            stepNum=3
          else
            stepNum=2
          fi
          break
        ;;
        "$LVM_PART_NAME"|"$HOME_PART_NAME")
          stepNum=5
          break
        ;;
        *)
          local existFlag=$(isRootDevice "${cols[-1]}")
          if ${existFlag}; then
            stepNum=4
            break
          fi
        ;;
      esac
    done
  fi
  nextStep=$(printf "step#%02d" "$stepNum")
  echo "$nextStep"
}

#---------------------------------------------------------------------------------------
#      METHOD:                 manageDiscretePartitions
# DESCRIPTION: Do all the processing necessary to create the discrete partitions
#  Required Params:
#      1) nextStep - the next step in the partitioning process
#---------------------------------------------------------------------------------------
manageDiscretePartitions() {
  local nextStep="$1"
  local -A funcParams=(["dialogBackTitle"]="$DIALOG_BACK_TITLE" ["deviceType"]="$DEVICE_TYPE"
    ["statsBlockSize"]="$PHYSICAL_VOL_SIZE"
  )

  case "$nextStep" in
    "step#01")
      funcParams["deviceName"]="$EFI_PART_NAME"
      addEspDevice funcParams
      local numParts=$(cat "$PARTITION_TABLE_FILE" | wc -l)
      if [ $numParts -lt 1 ]; then
        cleanKeyAndVals
      fi
    ;;
    "step#02")
      funcParams["deviceName"]="$BOOT_PART_NAME"
      addBootDevice funcParams
      if ${UEFI_FLAG}; then
        local numParts=$(cat "$PARTITION_TABLE_FILE" | wc -l)
        if [ $numParts -lt 2 ]; then
          varMap["BOOT-DEVICE"]="Skip"
        fi
      fi
    ;;
    "step#03")
      if [[ "${varMap[PART-LAYOUT]}" =~ "LVM" ]]; then
        mbSelVal=$(getTotBytesRemaining)
        appendPartitionToFile "$LVM_PART_NAME"
      else
        funcParams["deviceName"]="$ROOT_PART_NAME"
        addRootDevice funcParams
      fi
    ;;
    "step#04")
      funcParams["lastDevice"]=$(getLastDiscreteDevice)
      runDevManOp "$VAR_PART_NAME" funcParams
    ;;
    "step#05")
      if [[ "${varMap[PART-LAYOUT]}" =~ "LVM" ]]; then
        clog_warn "Creating partitions for 'LVM' has been moved to 'logical-volume-manager.sh' script!" >> "$INSTALLER_LOG"
      else
        saveDataToDisk funcParams
      fi
    ;;
  esac
  cleanDialogFiles
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                    getTotBytesRemaining
# DESCRIPTION: Get the total bytes remaining for either the 'Linux LVM' or '/home'
#              discrete partition
#      RETURN: human readable bytes
#---------------------------------------------------------------------------------------
function getTotBytesRemaining() {
  local rows=()
  local cols=()
  local hrToBytes=0
  local totUsed=0

  readarray -t rows < "$PARTITION_TABLE_FILE"
  for row in "${rows[@]}"; do
    readarray -t cols <<< "${row//$'\t'/$'\n'}"
    hrToBytes=$(humanReadableToBytes "${cols[2]}")
    totUsed=$(expr $totUsed + $hrToBytes)
  done
  local partSize=$(expr $BLOCK_DEVICE_SIZE - $totUsed)

  echo $(bytesToHumanReadable "$partSize")
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                      getLastDiscreteDevice
# DESCRIPTION: Get the last logical volume
#      RETURN: concatenated string delimited by '\t'
#---------------------------------------------------------------------------------------
function getLastDiscreteDevice() {
  local partSize=$(getTotBytesRemaining)
  local cols=("/dev/sda" "$HOME_PART_NAME" "$partSize" "8300" " " " " " ")

  local lineNum=$(wc -l < "$PARTITION_TABLE_FILE")
  lineNum=$(expr $lineNum + 1)
  cols[0]=$(printf "%s%d" "${cols[0]}" "$lineNum")
  local row=$(printf "\t%s" "${cols[@]}")
  row=${row:1}

  echo -e "$row"
}

#---------------------------------------------------------------------------------------
#      METHOD:                      saveDataToDisk
# DESCRIPTION: Save/Write the rows of partitions in the partition table to disk
#  Required Params:
#      1) methodParams - associative array of key/value pairs of parameters for the method:
#                        (see calling method manageDiscretePartitions)
#---------------------------------------------------------------------------------------
saveDataToDisk() {
  local -n methodParams=$1
  local cmdSteps=()
  local linuxCmdKeys=()
  local -A linuxCmdTitles=()
  local -A linuxCmds=()
  local pbTitle="Creating 'Discrete Devices'"

  setDataForPartitionScheme methodParams
  setLinuxCmds
  setStepsForCmds

  methodParams["dialogTitle"]=$(echo "Save Confirmation for creating 'Discrete Devices'")

  showSaveConfirmationDialog cmdSteps methodParams

  case "$inputRetVal" in
    "NO")
      cleanKeyAndVals
    ;;
    "YES")
      executeLinuxCommands "$DIALOG_BACK_TITLE" "$pbTitle" linuxCmdKeys linuxCmdTitles linuxCmds
    ;;
  esac
}

#---------------------------------------------------------------------------------------
#      METHOD:                    setLinuxCmds
# DESCRIPTION: Set the linux commands to execute in order to create the partitions
#---------------------------------------------------------------------------------------
setLinuxCmds() {
  local key="zap"
  local cmdNum=0
  local cmdTitle=""
  local tableRows=()
  local cols=()
  local lastIdx=-1
  local partName=""
  local partSize=""

  linuxCmdKeys=("$key")
  linuxCmdTitles["$key"]="Zapping Current Partition Layout/Scheme"
  linuxCmds["$key"]=$(printf "sgdisk -Z %s" "$BLOCK_DEVICE")

  readarray -t tableRows < "$PARTITION_TABLE_FILE"
  local lastRow="${tableRows[$lastIdx]}"
  unset tableRows[$lastIdx]

  for row in "${tableRows[@]}"; do
    readarray -t cols <<< "${row//$'\t'/$'\n'}"
    partName="${cols[1]}"
    partSize="${cols[2]}"
    local hrSize=(${partSize// /})
    cmd=$(printf "sgdisk -n 0:0:+%s -t 0:%s -c 0:\"%s\" %s" "$hrSize" "${cols[3]}" "$partName" "$BLOCK_DEVICE")
    cmdNum=$(expr $cmdNum + 1)
    key=$(printf "partCmd%02d" "$cmdNum")
    cmdTitle=$(printf "Creating Partition '%s' with size of '%s'" "$partName" "$partSize")
    linuxCmdKeys+=("$key")
    linuxCmds["$key"]="$cmd"
    linuxCmdTitles["$key"]="$cmdTitle"
  done

  readarray -t cols <<< "${lastRow//$'\t'/$'\n'}"
  partName="${cols[1]}"
  cmd=$(printf "sgdisk -n 0:0:0 -t 0:%s -c 0:\"%s\" %s" "${cols[3]}" "$partName" "$BLOCK_DEVICE")
  cmdNum=$(expr $cmdNum + 1)
  key=$(printf "partCmd%02d" "$cmdNum")
  cmdTitle=$(printf "Creating Partition '%s' with size of '%s'" "$partName" "${cols[2]}")
  linuxCmdKeys+=("$key")
  linuxCmds["$key"]="$cmd"
  linuxCmdTitles["$key"]="$cmdTitle"
}

#---------------------------------------------------------------------------------------
#  MAIN
#---------------------------------------------------------------------------------------
main() {
  while true; do
    if [ ${#BLOCK_DEVICE} -lt 1 ]; then
      initVars "$@"
    else
      local nextStep=$(getNextStep)
      manageDiscretePartitions "$nextStep"
      if [ ! ${varMap["PART-LAYOUT"]+_} ]; then
        break
      elif [ "$nextStep" == "step#05" ]; then
        break
      fi
    fi
  done

  local -A updVarMap=()
  for key in "${!varMap[@]}"; do
    updVarMap["$key"]=$(echo "${varMap[$key]}")
  done

  declare -p updVarMap > "${UPD_ASSOC_ARRAY_FILE}"
}

main "$@"
exit 0
