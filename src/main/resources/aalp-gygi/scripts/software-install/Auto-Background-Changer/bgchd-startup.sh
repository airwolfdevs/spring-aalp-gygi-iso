 #!/bin/bash                                                                                                   
#======================================================================================                       
#                                                                                                             
# Author  : Thomas Bednarek                                                                                   
# License : Distributed under the terms of GNU GPL version 2 or later                                         
#                                                                                                             
#======================================================================================                       
                                                                                                              
#===============================================================================                              
# globals                                                                                                     
#===============================================================================                              
IMG_DIR="$HOME/Pictures/Wallpapers"                                                                           
CONFIG_DIR="$HOME/.config/nitrogen"                                                                           
FILE_NAME="$CONFIG_DIR/randList.txt"                                                                          
CFG_FILE="$CONFIG_DIR/bg-saved.cfg"                                                                           
                                                                                                                
#-------------------------------------------------------------------------------                              
#  randomizeList                                                                                              
#-------------------------------------------------------------------------------                              
randomizeList() {                                                                                             
  ls ${IMG_DIR}/* | sort -R | while read file; do                                                             
    echo "$file" >> $FILE_NAME                                                                                
  done                                                                                                        
}                                                                                                             
                                                                                                                
#---------------------------------------------------------------------------------------                      
#  Main                                                                                                       
#---------------------------------------------------------------------------------------                      
main() {                                                                                                      
  if [ ! -f "$FILE_NAME" ]; then                                                                              
    randomizeList                                                                                             
  fi                                                                                                          
  bgchd -dir "$HOME/Pictures/Wallpapers" -intv 60m -bcknd nitrogen -rpl > /dev/null 2>&1 &                    
}                                                                                                             
                                                                                                                
main "$@"                                                                                                     
exit 0                                                                                                        
