#!/bin/bash
#set -e

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# globals
#===============================================================================
CUR_DIR="${PWD}"
SCRIPT_DIR=$(echo "$CUR_DIR/software-install/Nexus-OSS")
INC_FILE=$(echo "$CUR_DIR/inc/inst_inc.sh")

source "$INC_FILE"

declare HXU_NORMALIZE="hxnormalize"
declare HXU_SELECT="hxselect"
declare PHANTOM_JS="phantomjs"
declare AUR_PCKG="nexus-oss"
declare SRC_DIR=""
declare SOFT_PART=""

#---------------------------------------------------------------------------------------
#      METHOD:                       initVars
# DESCRIPTION: Initialize the global variables.
#---------------------------------------------------------------------------------------
initVars() {
  local args=("$@")
  local operator=""

  AUR_HELPER="trizen"
  COMP_BY_USER="$TRIZEN_USER"

  for arg in "${args[@]}"; do
    case "$arg" in
      "-ah"|"-part"|"-u")
        operator="$arg"
      ;;
      *)
        case "$operator" in
          "-ah")
            AUR_HELPER="$arg"
          ;;
          "-part")
            if [ -d "$arg" ]; then
              SOFT_PART="$arg"
            fi
          ;;
          "-u")
            COMP_BY_USER="$arg"
          ;;
          *)
            clog_error "Usage: [-ah \"AUR helper\"] [-u \"user name\"] [-part \"partition\"]"
            exit 1
          ;;
        esac
        operator=""
      ;;
    esac
  done
}

#---------------------------------------------------------------------------------------
#      METHOD:                       instDep
# DESCRIPTION: Install the dependencies needed to run Nexus OSS 
#---------------------------------------------------------------------------------------
instDep() {
  local pckgName="jdk8"
  local existFlag=$(isAURPckgInst "$pckgName")
  if ! ${existFlag}; then
    pckgName="jdk8-openjdk"
    existFlag=$(isAURPckgInst "$pckgName")
    if ! ${existFlag}; then
      local cmd=$(echo "sudo -u $COMP_BY_USER $AUR_HELPER -S --skipinteg --noconfirm $pckgName")
      changeRoot "$cmd"
    fi
  fi

  pckgName="ccze"
  existFlag=$(isAURPckgInst "$pckgName")
  if ! ${existFlag}; then
    local cmd=$(echo "sudo -u $COMP_BY_USER $AUR_HELPER -S --skipinteg --noconfirm $pckgName")
    changeRoot "$cmd"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                          cloneProj
# DESCRIPTION: Clones the project from AUR repository
#---------------------------------------------------------------------------------------
cloneProj() {
  SRC_DIR="${ROOT_MOUNTPOINT}/home/$COMP_BY_USER/$AUR_PCKG"

  if [ -d $SRC_DIR ]; then
    changeRoot "rm -rf $SRC_DIR"
  fi

  changeRoot "git clone https://aur.archlinux.org/$AUR_PCKG.git $SRC_DIR"
  chown -R root:wheel "$SRC_DIR"
  chmod -R 774 "$SRC_DIR"
}

#---------------------------------------------------------------------------------------
#      METHOD:                       setVersionPatch
# DESCRIPTION: Set the latest version and patch number into the array "$versionPatch",
#              which is declared within the calling method.
#---------------------------------------------------------------------------------------
setVersionPatch() {
  local fileName="${SRC_DIR}/nexus3.html"
  local url="https://help.sonatype.com/repomanager3/download/download-archives---repository-manager-3"
  if [ ! -f "$fileName" ]; then
    curl -s "$url" | $HXU_NORMALIZE -x  > "$fileName"
  fi
  local htmlTag=$($HXU_SELECT "h3" < $fileName)
  local htmlLines=()
  local sep="</h3>"
  readarray -t htmlLines <<< "${htmlTag//$sep/$'\n'}"

  sep=" "
  readarray -t htmlLines <<< "${htmlLines[0]//$sep/$'\n'}"
  local nexusVerPatch=$(trimString "${htmlLines[-1]}")

  readarray -t versionPatch <<< "${nexusVerPatch//-/$'\n'}"
}

#---------------------------------------------------------------------------------------
#      METHOD:                       installSoftware
# DESCRIPTION: Install "Nexus OSS"
#---------------------------------------------------------------------------------------
installSoftware() {
  log " "
  log " "
  log "################################################################"
  log "######       Installing     Nexus OSS v3                  ######"
  log "################################################################"

  createGroupAndUser
  configureNexusFiles
  buildAndInstall
  moveAppAndDataDirs
  addIgnorePckg

  clog_info "Installation of Nexus OSS ${versionPatch[0]}-${versionPatch[1]} is complete!"
}

#---------------------------------------------------------------------------------------
#      METHOD:                     configureNexusFiles
# DESCRIPTION: Re-configure some of the properties (i.e. port, executable, etc.) within
#              nexus project directory.
#---------------------------------------------------------------------------------------
configureNexusFiles() {
  local fileNames=("nexus-oss" "nexus-oss.install" "nexus-oss.properties" "nexus-oss.service" "nexus-oss.sysusers"
  "nexus-oss.tmpfiles" "nexus-oss.vmoptions" "PKGBUILD")
  local nexusFN=""

  for fn in "${fileNames[@]}"; do
    nexusFN=$(echo "${ROOT_MOUNTPOINT}/${SRC_DIR}/$fn")
    case "$fn" in
      "nexus-oss")
        cp -rf "${SCRIPT_DIR}/$fn" "${nexusFN}"
      ;;
      "nexus-oss.install")
        sed -i '3,7 d' "${nexusFN}"
        sed -i "s/8081/9091/g" "${nexusFN}"
      ;;
      "nexus-oss.properties")
        sed -i "s/application-port=.*/application-port=9091/g" "${nexusFN}"
      ;;
      "nexus-oss.service")
        sed -i "s/ExecStart=.*/ExecStart=\/usr\/lib\/nexus-oss\/bin\/nexus start/g" "${nexusFN}"
        sed -i "s/ExecStop=.*/ExecStop=\/usr\/lib\/nexus-oss\/bin\/nexus stop/g" "${nexusFN}"
      ;;
      "nexus-oss.sysusers")
        sudo rm -rf "${nexusFN}"
      ;;
      "nexus-oss.tmpfiles")
        if [ ${#SOFT_PART} -gt 0 ]; then
          local tmpLine="d /${SOFT_PART}/tmp 0755 nexus wheel -"
          echo "$tmpLine" > "${nexusFN}"
        fi
      ;;
      "nexus-oss.vmoptions")
        sed -i "s/1200M/2048M/g" "${nexusFN}"
        sed -i "s/2G/4G/g" "${nexusFN}"
        sed -i "s/-XX:LogFile=.*/-XX:LogFile=\/nexus-oss\/log\/jvm.log/g" "${nexusFN}"
        sed -i "s/-Dkaraf\.data=.*/-Dkaraf\.data=\/nexus-oss\/data/g" "${nexusFN}"
        sed -i "s/-Djava\.io\.tmpdir=.*/-Djava\.io\.tmpdir=\/nexus-oss\/tmp/g" "${nexusFN}"
      ;;
      "PKGBUILD")
        sed -i "s/_version=.*/_version=${versionPatch[0]}/g" "${nexusFN}"
        sed -i "s/_patch=.*/_patch=${versionPatch[1]}/g" "${nexusFN}"
	    local depJDK=$(getDepForJDK)
        sed -i "s/jre8-openjdk-headless/$depJDK/g" "${nexusFN}"
        sed -i '/sysusers/d' "${nexusFN}"
      ;;
      *)
        echo "case for $fn not defined"
        exit 1
      ;;
    esac
    clog_info "finished configuring \"$fn\""
  done
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                      getDepForJDK
# DESCRIPTION: Get the version and type of JDK in order to set the dependency for
#              building & compiling Tomcat
#      RETURN: version and type of JDK
#---------------------------------------------------------------------------------------
function getDepForJDK() {
  local jdkDep=""
  local splitArray=()
  local sep=" "
  local cmdOut=$(javac -version)

  readarray -t splitArray <<< "${cmdOut//$sep/$'\n'}"
  sep="."
  readarray -t splitArray <<< "${splitArray[1]//$sep/$'\n'}"

  if [[ $(java -version 2>&1) == *"OpenJDK"* ]]; then
    jdkDep="jdk%d-openjdk"
  else
    jdkDep="jdk%d"
  fi

  jdkDep=$(printf "$jdkDep" ${splitArray[0]})

  echo "$jdkDep"
}

#---------------------------------------------------------------------------------------
#      METHOD:                     createGroupAndUser
# DESCRIPTION: Create the 'nexus' group and user if they don't already exist
#---------------------------------------------------------------------------------------
createGroupAndUser() {
  if ! changeRoot "getent group nexus" &> /dev/null; then
    changeRoot "groupadd nexus"
  else
    clog_warn "group 'nexus' already exists"
  fi

  local homeDir="/usr/lib/nexus-oss"
  if [ ${#SOFT_PART} -gt 0 ]; then
    homeDir="/$SOFT_PART"
  fi

  if ! changeRoot "getent passwd nexus" &> /dev/null; then
    changeRoot "useradd -c 'Nexus OSS Repository Manager' -d ${homeDir} -g nexus --system -G wheel nexus"
  else
    clog_warn "user 'nexus' already exists"
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                          buildAndInstall
# DESCRIPTION: Build and install the latest version
#---------------------------------------------------------------------------------------
buildAndInstall() {
  local cmd=""

  for cmdNum in $(eval echo "{1..2}"); do
    local cmd=$(echo "cd \"$SRC_DIR\"")
    case ${cmdNum} in
      1)
        cmd=$(echo "${cmd};sudo -u $COMP_BY_USER makepkg --skipinteg --skipchecksums --skippgpcheck -cs")
      ;;
      2)
        cmd=$(echo "${cmd};sudo -u $COMP_BY_USER find . -name \"*.pkg.*\" -type f -printf '%f\n'")
        local fileName=$(changeRoot "$cmd")
        echo "fileName=[$fileName]"
        cmd=$(echo "${cmd};sudo -u $COMP_BY_USER $AUR_HELPER -U --noconfirm $fileName")
      ;;
    esac
    changeRoot "$cmd"
  done

}

#---------------------------------------------------------------------------------------
#      METHOD:                     moveAppAndDataDirs
# DESCRIPTION: Move the directories to the partition defined in the global variable
#              "$SOFT_PART"
#---------------------------------------------------------------------------------------
moveAppAndDataDirs() {
  if [ ${#SOFT_PART} -gt 0 ]; then
    local mntPt="${ROOT_MOUNTPOINT}/${SOFT_PART:1}"
    local instAppDir="${ROOT_MOUNTPOINT}/usr/lib/nexus-oss"
    local instDataDir="${ROOT_MOUNTPOINT}/var/lib/nexus-oss"
    local appDir="${mntPt}/${versionPatch[0]}-${versionPatch[1]}"
    local dataDir="${mntPt}/data"

    mv "$instAppDir" "$appDir"
    ln -sf "$appDir" "$instAppDir"
    mv "$instDataDir" "$dataDir"
    ln -sf "$dataDir" "$instDataDir"

    chown -R nexus:wheel ${mntPt}
    chmod 755 ${mntPt}
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                        addIgnorePckg
# DESCRIPTION: Ensure that 'nexus-oss' is not accidentally upgraded to an
#              incompatible version.
#---------------------------------------------------------------------------------------
addIgnorePckg() {
  local srcFile="${ROOT_MOUNTPOINT}/etc/pacman.conf"
  local originalFile="${ROOT_MOUNTPOINT}/etc/pacman-conf.orig"
  local lineNum=$(sed -n "/nexus-oss/=" "$srcFile")
  local appendLine="IgnorePkg = nexus-oss"

  if [ ${#lineNum} -lt 1 ]; then
    clog_info "BEGIN.......[Adding the AUR packages to /etc/pacman.conf to be ignored]"
    lineNum=$(sed -n "/^#IgnorePkg/=" "$srcFile")
    if [ -f "$originalFile" ]; then
      originalFile="${ROOT_MOUNTPOINT}/etc/pacman-conf.bak"
    fi

    cp -rf ${srcFile} ${originalFile}

    sed -i "${lineNum} a $appendLine" "$srcFile"
    clog_info "END.......[Adding the AUR packages to /etc/pacman.conf to be ignored]"
  fi
}

#---------------------------------------------------------------------------------------
#  Main
#---------------------------------------------------------------------------------------
main() {
  if [ ! -d "/mnt/etc" ]; then
    ROOT_MOUNTPOINT=""
  fi

  local args=("$@")
  local parent=$(ps -f -p $PPID | tail -1 | awk '{print $9}')
  local pos=$(findPositionForString EXPECTED_NAMES "$parent")

  if [ ${pos} -lt 0 ]; then
    local concatStr=$(printf "\n'%s'" "${EXPECTED_NAMES[@]}")
    clog_error "Must be called from:$concatStr"
    exit 1
  fi

  initVars "$@"

  instDep

  cloneProj

  local versionPatch=()
  setVersionPatch

  local aurVersion="${versionPatch[0]}.${versionPatch[1]}"
  local verFlag=$(isVersionInstalled "$AUR_PCKG" "$aurVersion")
  if ${verFlag}; then
    clog_warn "Nexus OSS '${versionPatch[0]}-${versionPatch[1]}' is already installed!"
  else
    installSoftware
  fi

  changeRoot "rm -rf $SRC_DIR"
}

main "$@"
exit 0
