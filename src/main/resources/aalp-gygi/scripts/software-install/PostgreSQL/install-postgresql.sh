#!/bin/bash
#set -e

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# globals
#===============================================================================
CUR_DIR="${PWD}"
INC_FILE=$(echo "$CUR_DIR/inc/inst_inc.sh")

source "$INC_FILE"

declare HXU_NORMALIZE="hxnormalize"
declare HXU_SELECT="hxselect"
declare PHANTOM_JS="phantomjs"
declare AUR_PCKG="postgresql"
declare SRC_DIR=""
declare PG_HOME=""
declare PG_VER=""
declare COMP_BY_USER=""
declare AUR_HELPER=""

#---------------------------------------------------------------------------------------
#      METHOD:                       initVars
# DESCRIPTION: Initialize the global variables.
#---------------------------------------------------------------------------------------
initVars() {
  local args=("$@")
  local operator=""

  local operator=""
  AUR_HELPER="trizen"
  COMP_BY_USER="$TRIZEN_USER"
  PG_HOME="/var/lib"

  for arg in "${args[@]}"; do
    case "$arg" in
      "-ah"|"-part"|"-u")
        operator="$arg"
      ;;
      *)
        case "$operator" in
          "-ah")
            AUR_HELPER="$arg"
          ;;
          "-part")
            if [ -d "$arg" ]; then
              PG_HOME="$arg"
            fi
          ;;
          "-u")
            COMP_BY_USER="$arg"
          ;;
          *)
            clog_error "Usage: [-ah \"AUR helper\"] [-u \"user name\"] [-part \"partition\"]"
            exit 1
          ;;
        esac
        operator=""
      ;;
    esac
  done

  PG_HOME+="/postgres"
}

#---------------------------------------------------------------------------------------
#      METHOD:                       installSoftware
# DESCRIPTION: Install "PostgreSQL"
#  Required Params:
#      1) aurPckgName - name of the AUR package to install
#---------------------------------------------------------------------------------------
installSoftware() {
  local existFlag=$(isPckgInst "$1")
  if ! ${existFlag}; then
    buildFromSource
    createSystemUser

    clog_info "BEGIN.......[Installing application]"
    make install
    clog_info "END.......[Installing application]"
  fi
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                         isPckgInst
# DESCRIPTION: Check if an AUR package is installed.
#      RETURN: 0 - true, 1 - false
#  Required Params:
#      1) aurPckgName - name of the AUR package
#---------------------------------------------------------------------------------------
function isPckgInst() {
  local cmd=$(echo "sudo -u $TRIZEN_USER trizen -Qi $1")

  if changeRoot "$cmd" &> /dev/null; then
    echo 'true' && return 0
  fi
  echo 'false' && return 1
}

#---------------------------------------------------------------------------------------
#      METHOD:                       buildFromSource
# DESCRIPTION: Build the database application from source
#---------------------------------------------------------------------------------------
buildFromSource() {
  SRC_DIR="${ROOT_MOUNTPOINT}/home/${COMP_BY_USER}/${AUR_PCKG}"
  if [ -d "$SRC_DIR" ]; then
    changeRoot "rm -rf $SRC_DIR"
  fi
  changeRoot "mkdir $SRC_DIR"

  clog_info "BEGIN.......[Get the latest version of the package to be built and installed]"
  PG_VER=$(getLatestVersion)
  clog_info "END.......[Get the latest version of the package to be built and installed]"
  PG_HOME+="-v$PG_VER"
  clog_info "BEGIN.......[Download and compile the source code]"
  wget -P "$SRC_DIR" "https://ftp.postgresql.org/pub/source/v${PG_VER}/postgresql-${PG_VER}.tar.gz"
  cd "$SRC_DIR"
  tar -xzvf "postgresql-${PG_VER}.tar.gz"
  cd "postgresql-$PG_VER"
  curLoc="/usr/local"
  sed -i -e "s@$curLoc@$PG_HOME@g" configure
  ./configure
  make
  clog_info "END.......[Download and compile the source code]"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                      getLatestVersion
# DESCRIPTION: Get the latest version of the package to be built and installed
#      RETURN: build/package version
#---------------------------------------------------------------------------------------
function getLatestVersion() {
  local pgVer=""
  local versionURL="https://www.postgresql.org/"
  local srcFile=$(echo "$SRC_DIR/index.html")

  curl -s "$versionURL" | $HXU_NORMALIZE -x  > "$srcFile"

  local htmlTag=$($HXU_SELECT "li" < $srcFile)
  local splitStr=()
  local htmlLines=()
  local sep="</li>"
  readarray -t htmlLines <<< "${htmlTag//$sep/$'\n'}"

  for line in "${htmlLines[@]}"; do
    if [[ $line =~ .*middot.* ]]; then
      sep="b>"
      readarray -t splitStr <<< "${line//$sep/$'\n'}"
      line="${splitStr[1]}"

      sep="<"
      readarray -t splitStr <<< "${line//$sep/$'\n'}"

      pgVer="${splitStr[0]}"
      break
    fi
  done

  echo "$pgVer"
}

#---------------------------------------------------------------------------------------
#      METHOD:                      createSystemUser
# DESCRIPTION: Create the 'postgres' system user
#---------------------------------------------------------------------------------------
createSystemUser() {
  clog_info "BEGIN.......[Create the 'postgres' system user]"
  getent group dbadmin > /dev/null || groupadd dbadmin
  getent group postgres > /dev/null || groupadd postgres
  if getent passwd postgres > /dev/null; then
    usermod -d "$PG_HOME"
  else
    useradd -c 'PostgreSQL user' -d "$PG_HOME" -g postgres --system -G dbadmin postgres
  fi

  if [ ! -d $PG_HOME ]; then
    mkdir -p $PG_HOME
    chmod 775 $PG_HOME
    chown -R postgres:wheel "$PG_HOME"
  fi
  clog_info "END.......[Create the 'postgres' system user]"
}

#---------------------------------------------------------------------------------------
#      METHOD:                     createSymbolicLinks
# DESCRIPTION: Create the symbolic links to the executables that were installed
#---------------------------------------------------------------------------------------
createSymbolicLinks() {
  clog_info "BEGIN.......[Create the symbolic links to the executables that were installed]"
  local pgBinDir="${ROOT_MOUNTPOINT}${PG_HOME}/pgsql/bin"
  local destDIR="/usr/bin"
  if [ -d "$pgBinDir" ]; then
    chown -R postgres:wheel "$pgBinDir"
    ls $pgBinDir/* | while read file; do
      local fileName=`basename "$file"`
      if [[ -x "$file" ]]; then
        ln -sf "${pgBinDir}/$fileName" "${destDIR}/$fileName"
      fi
    done
  fi
  clog_info "END.......[Create the symbolic links to the executables that were installed]"
}

#---------------------------------------------------------------------------------------
#      METHOD:                       copyConfigFiles
# DESCRIPTION: Copy the configuration files (i.e. pgsql-srvc) to the appropriate
#              directory and set the home/root directory of the application.
#---------------------------------------------------------------------------------------
copyConfigFiles() {
  clog_info "BEGIN.......[Copying the configuration files to the appropriate directory]"

  local searchStr="PG_ROOT_DIR"
  local srcDir=$(echo "$CUR_DIR/software-install/PostgreSQL")
  local srcFile=""
  local destDir=""
  local splitStr=()
  local sep='/'
  local escChars="\/"

  readarray -t splitStr <<< "${PG_HOME//$sep/$'\n'}"

  local pgRootDir=$(printf "${escChars}%s" "${splitStr[@]}")

  for fileNum in $(eval echo "{1..3}"); do
    case ${fileNum} in
      1)
        srcFile="pgsql-srvc"
        destDir="${ROOT_MOUNTPOINT}/usr/bin"
      ;;
      2)
        srcFile="postgresql.conf"
        destDir="${ROOT_MOUNTPOINT}${PG_HOME}/data"
      ;;
      3)
        srcFile="postgresql.service"
        destDir="${ROOT_MOUNTPOINT}/usr/lib/systemd/system"
      ;;
    esac

    local destFile="${destDir}/$srcFile"
    cp -rf "${srcDir}/$srcFile" "$destFile"
    sed -i -e "s/${searchStr}/${pgRootDir:2}/g" "$destFile"
  done

  clog_info "END.......[Copying the configuration files to the appropriate directory]"
}

#---------------------------------------------------------------------------------------
#      METHOD:                     initDatabaseCluster
# DESCRIPTION: The database cluster must be initialized before "PostgreSQL"
#              can function correctly
#---------------------------------------------------------------------------------------
initDatabaseCluster() {
  clog_info "BEGIN.......[Initializing the database cluster]"
  local cmd=$(echo "sudo -i -u postgres initdb --locale en_US.UTF-8 -E UTF8 -D '${PG_HOME}/data'")
  changeRoot "$cmd"
  clog_info "END.......[Initializing the database cluster]"
}

#---------------------------------------------------------------------------------------
#      METHOD:                        addIgnorePckg
# DESCRIPTION: Ensure that the database is not accidentally upgraded to an
#              incompatible version.
#---------------------------------------------------------------------------------------
addIgnorePckg() {
  local srcFile="${ROOT_MOUNTPOINT}/etc/pacman.conf"
  local originalFile="${ROOT_MOUNTPOINT}/etc/pacman-conf.orig"
  local lineNum=$(sed -n "/postgresql/=" "$srcFile")
  local appendLine="IgnorePkg = postgresql postgresql-libs"

  if [ ${#lineNum} -lt 1 ]; then
    clog_info "BEGIN.......[Adding the AUR packages to /etc/pacman.conf to be ignored]"
    lineNum=$(sed -n "/^#IgnorePkg/=" "$srcFile")
    if [ -f "$originalFile" ]; then
      originalFile="${ROOT_MOUNTPOINT}/etc/pacman-conf.bak"
    fi

    cp -rf ${srcFile} ${originalFile}

    sed -i "${lineNum} a $appendLine" "$srcFile"
    clog_info "END.......[Adding the AUR packages to /etc/pacman.conf to be ignored]"
  fi
}

#---------------------------------------------------------------------------------------
#  Main
#---------------------------------------------------------------------------------------
main() {
  if [ ! -d "/mnt/etc" ]; then
    ROOT_MOUNTPOINT=""
  fi

  local parent=$(ps -f -p $PPID | tail -1 | awk '{print $9}')
  local pos=$(findPositionForString EXPECTED_NAMES "$parent")

  if [ ${pos} -lt 0 ]; then
    local concatStr=$(printf "\n'%s'" "${EXPECTED_NAMES[@]}")
    clog_error "Must be called from:$concatStr"
    exit 1
  fi
  log " "
  log " "
  log "################################################################"
  log "######         Starting installation of PostgreSQL        ######"
  log "################################################################"

  initVars "$@"

  installSoftware "postgresql"
  createSymbolicLinks
  initDatabaseCluster
  copyConfigFiles
  addIgnorePckg

  log " "
  log " "
  log "################################################################"
  log "######         Finished installation of PostgreSQL        ######"
  log "######                   Version:  $PG_VER                   ######"
  log "################################################################"
}

main "$@"
exit 0
