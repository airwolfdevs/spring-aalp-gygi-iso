#!/bin/bash

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# globals
#===============================================================================
CUR_DIR="${PWD}"
INC_FILE=$(echo "$CUR_DIR/inc/inst_inc.sh")

source "$INC_FILE"

declare -A VERSION_URL=(["v7"]="https://tomcat.apache.org/download-70.cgi"
  ["v8"]="https://tomcat.apache.org/download-80.cgi"
  ["v9"]="https://tomcat.apache.org/download-90.cgi")
declare AUR_PCKG="apache-tomcat"
declare AUR_HELPER=""
declare COMP_BY_USER=""
declare SRC_DIR=""
declare SETTINGS_DIR="$CUR_DIR/software-install/Tomcat/settings"
declare HXU_NORMALIZE="hxnormalize"
declare HXU_SELECT="hxselect"
declare TOMCAT_VERSION=""
declare PACKAGE_VERSION=""
declare -A POST_ASSOC_ARRAY=()

#---------------------------------------------------------------------------------------
#      METHOD:                       initVars
# DESCRIPTION: Initialize the global variables.
#---------------------------------------------------------------------------------------
initVars() {
  local args=("$@")
  local operator=""

  AUR_HELPER="trizen"
  COMP_BY_USER="$TRIZEN_USER"

  for arg in "${args[@]}"; do
    case "$arg" in
      "-ah"|"-pv"|"-tv"|"-u")
        operator="$arg"
      ;;
      *)
        case "$operator" in
          "-ah")
            AUR_HELPER="$arg"
          ;;
          "-pv")
            TOMCAT_VERSION="$arg"
            local link=$(getSrcCodeLink "${VERSION_URL["$TOMCAT_VERSION"]}")
            PACKAGE_VERSION=$(getPackageVersion "$link")
            if [ ${#PACKAGE_VERSION} -lt 1 ]; then
              clog_error "Unable to get package version of \"Apache Tomcat $TOMCAT_VERSION\"!!!!!!!"
              exit 1
            fi
            break
          ;;
          "-tv")
            TOMCAT_VERSION="$arg"
          ;;
          "-u")
            COMP_BY_USER="$arg"
          ;;
          *)
            clog_error "Usage: [-ah \"AUR helper\"] [-tv v7|v8|v9] [-pv v7|v8|v9] [-u \"user name\"]"
            exit 1
          ;;
        esac
      ;;
    esac
  done

  if [ ${#TOMCAT_VERSION} -gt 0 ]; then
    POST_ASSOC_ARRAY["UserName"]="$COMP_BY_USER"
    clog_info "AUR Helper=[$AUR_HELPER]"
    clog_info "Compiled By User=[$COMP_BY_USER]"
    clog_info "Tomcat Version=[$TOMCAT_VERSION]"
  else
    clog_error "Usage: -tv v7|v8|v9 [-ah \"AUR helper\"] [-u \"user name\"]"
    exit 1
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                       installSoftware
# DESCRIPTION: Install "Apache Tomcat"
#---------------------------------------------------------------------------------------
installSoftware() {
  log " "
  log " "
  log "################################################################"
  log "######       Installing:   Apache-Tomcat $TOMCAT_VERSION               ######"
  log "################################################################"
  cloneProj >/dev/null 2>&1
  if [ -d "$SRC_DIR" ]; then
    buildAndInstall
    local pckgName=$(echo "${AUR_PCKG}$TOMCAT_VERSION")
    local existFlag=$(isAURPckgInst "$pckgName")
    if ${existFlag}; then
      local catalinaHome="opt/${pckgName}-$PACKAGE_VERSION"
      local installedDir=$(echo "${ROOT_MOUNTPOINT}/$catalinaHome")
      local tomcatUser=$(getNameOfNormalUser)

      cp -rf "${SETTINGS_DIR}/tomcat-users.xml" "${installedDir}/conf/."

      copyConfigFiles "$installedDir"
      configureSetenv "$installedDir"
      configureService "$installedDir" "$tomcatUser"
      setPermissions "$installedDir"

      log " "
      log " "
      log "################################################################"
      log "######       Apache-Tomcat $TOMCAT_VERSION is installed!!!             ######"
      log "################################################################"
    else
      clog_error "Unable to install \"Apache Tomcat $TOMCAT_VERSION\":\nReview the lines above!!!!!!!"
      exit 1
    fi
  else
    clog_error "Unable to install \"Apache Tomcat $TOMCAT_VERSION\":\nUnable to clone the project from AUR repository!!!!!!!"
    exit 1
  fi

  changeRoot "rm -rf $SRC_DIR"
}

#---------------------------------------------------------------------------------------
#      METHOD:                          cloneProj
# DESCRIPTION: Clones the project from AUR repository
#---------------------------------------------------------------------------------------
cloneProj() {
  SRC_DIR="/home/${COMP_BY_USER}/${AUR_PCKG}$TOMCAT_VERSION"

  if [ -d $SRC_DIR ]; then
    changeRoot "rm -rf $SRC_DIR"
  fi

  changeRoot "git clone https://aur.archlinux.org/$AUR_PCKG.git $SRC_DIR"
  chown -R root:wheel "$SRC_DIR"
  chmod -R 774 "$SRC_DIR"
}

#---------------------------------------------------------------------------------------
#      METHOD:                          buildAndInstall
# DESCRIPTION: Build and install the latest version
#---------------------------------------------------------------------------------------
buildAndInstall() {
  local link=$(getSrcCodeLink "${VERSION_URL["$TOMCAT_VERSION"]}")
  local pckgVer=$(getPackageVersion "$link")
  local depJDK=$(getDepForJDK)
  local pckgName=$(echo "${AUR_PCKG}$TOMCAT_VERSION")

  for cmdNum in $(eval echo "{1..5}"); do
    local cmd=$(echo "cd \"$SRC_DIR\"")
    case ${cmdNum} in
      1)
        initPKGBUILD "$pckgName" "$pckgVer" "$depJDK" "$link"
        cmd=""
      ;;
      2)
        cmd=$(echo "${cmd};sudo -u $COMP_BY_USER makepkg --skipinteg --nobuild")
      ;;
      3)
        cmd=$(echo "${cmd};sudo -u $COMP_BY_USER mv \"${SRC_DIR}/src/${AUR_PCKG}-${pckgVer}\" \"${SRC_DIR}/src/${pckgName}-${pckgVer}\"")
      ;;
      4)
        cmd=$(echo "${cmd};sudo -u $COMP_BY_USER makepkg --skipinteg --noextract -cs")
      ;;
      5)
        cmd=$(echo "${cmd};sudo -u $COMP_BY_USER find . -name \"*.pkg.*\" -type f -printf '%f\n'")
        local fileName=$(changeRoot "$cmd")
        echo "fileName=[$fileName]"
        cmd=$(echo "${cmd};sudo -u $COMP_BY_USER $AUR_HELPER -U --noconfirm $fileName")
      ;;
    esac
    if [ ${#cmd} -gt 0 ]; then
      changeRoot "$cmd"
    fi
  done

}

#---------------------------------------------------------------------------------------
#      METHOD:                          buildAndInstall
# DESCRIPTION: Build and install the latest version
#  Required Params:
#      1) pkgname - name of the package to build and install
#      2)  pkgver - the package version to build
#      3) depends - the version and type of JDK to build & compile with
#      4)  source - the link/URL to the source to download
#---------------------------------------------------------------------------------------
initPKGBUILD() {
  local numParams="$#"
  local cmd=""
  clog_info "Package Name=[$1]"
  clog_info "Package Version=[$2]"
  clog_info "JDK=[$3]"

  for paramNum in $(eval echo "{1..${numParams}}"); do
    cmd=$(echo "cd \"$SRC_DIR\"")
    case ${paramNum} in
      1)
        cmd=$(echo "${cmd};sudo -u $COMP_BY_USER sed -i 3s/.*/pkgname=$1/ PKGBUILD")
      ;;
      2)
        PACKAGE_VERSION="$2"
        cmd=$(echo "${cmd};sudo -u $COMP_BY_USER sed -i 4s/.*/pkgver=$2/ PKGBUILD")
      ;;
      3)
        cmd=$(echo "${cmd};sudo -u $COMP_BY_USER sed -i \"10s/.*/depends=('$3')/\" PKGBUILD")
      ;;
      4)
        cmd=$(echo "${cmd};sudo -u $COMP_BY_USER sed -i '11s/.*/source=(\"$4\")/' PKGBUILD")
      ;;
    esac
    changeRoot "$cmd"
  done
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                       getSrcCodeLink
# DESCRIPTION: Get the link to the mirror site to download the source code
#      RETURN: value of latest version
#  Required Params:
#      1) versionURL - link/URL to the HTML to download
#---------------------------------------------------------------------------------------
function getSrcCodeLink() {
  local versionURL="$1"
  local link=""
  local escChars="\/"

  local fileName="${SRC_DIR}/tomcat-download.html"
  if [ ! -f "$fileName" ]; then
    curl -s "$1" | $HXU_NORMALIZE -x  > "$fileName"
  fi

  local htmlTag=$($HXU_SELECT "li" < $fileName)
  local splitStr=()
  local htmlLines=()
  local sep="</li>"
  readarray -t htmlLines <<< "${htmlTag//$sep/$'\n'}"

  for line in "${htmlLines[@]}"; do
    if [[ $line =~ .*\.tar\.gz\" ]]; then
      sep='"'
      readarray -t splitStr <<< "${line//$sep/$'\n'}"
      link="${splitStr[1]}"

      sep='/'
      readarray -t splitStr <<< "${link//$sep/$'\n'}"

      link=$(printf "${escChars}%s" "${splitStr[@]}")
      link="${link:${#escChars}}"
      break
    fi
  done


  echo "$link"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                      getPackageVersion
# DESCRIPTION: Get the version of the package to be built and installed
#      RETURN: build/package version
#  Required Params:
#      1) srcLink - link/URL for the source code to download from
#---------------------------------------------------------------------------------------
function getPackageVersion() {
  local srcLink="$1"
  local splitStr=()
  local buildVer=""
  local sep="/"

  readarray -t splitStr <<< "${srcLink//$sep/$'\n'}"
  buildVer=$(echo "${splitStr[-1]}")

  sep="-"
  readarray -t splitStr <<< "${buildVer//$sep/$'\n'}"
  buildVer=$(echo "${splitStr[-1]}" | sed -e 's/\.tar\.gz//g')

  echo "$buildVer"
}

#---------------------------------------------------------------------------------------
#    FUNCTION:                      getDepForJDK
# DESCRIPTION: Get the version and type of JDK in order to set the dependency for
#              building & compiling Tomcat
#      RETURN: version and type of JDK
#---------------------------------------------------------------------------------------
function getDepForJDK() {
  local jdkDep=""
  local splitArray=()
  local sep=" "
  local cmdOut=$(javac -version)

  readarray -t splitArray <<< "${cmdOut//$sep/$'\n'}"
  sep="."
  readarray -t splitArray <<< "${splitArray[1]//$sep/$'\n'}"

  if [[ $(java -version 2>&1) == *"OpenJDK"* ]]; then
    jdkDep="jdk%d-openjdk"
  else
    jdkDep="jdk%d"
  fi

  jdkDep=$(printf "$jdkDep" ${splitArray[0]})

  echo "$jdkDep"
}

#---------------------------------------------------------------------------------------
#      METHOD:                       copyConfigFiles
# DESCRIPTION: Copy the configuration files (i.e. setenv.sh) to the bin directory
#  Required Params:
#      1) installedDir - full path to the directory where "tomcat" was installed
#---------------------------------------------------------------------------------------
copyConfigFiles() {
  clog_info "BEGIN.......[Copying the configuration files to the bin directory]"

  local installedDir="$1"
  local path=()
  local sep="\/"
  local fileNames=("setenv.sh" "setenv.debug.sh" "remoteDebugging.sh")

  for fileName in "${fileNames[@]}"; do
    cp -rf "${SETTINGS_DIR}/$fileName" "${installedDir}/bin/."
  done

  clog_info "END.......[Copying the configuration files to the bin directory]"
}

#---------------------------------------------------------------------------------------
#      METHOD:                       configureSetenv
# DESCRIPTION: Configure the "setenv.sh" file for the version of Tomcat that was installed
#  Required Params:
#      1) tomcatHome - full path to the directory where "tomcat" was installed
#---------------------------------------------------------------------------------------
configureSetenv() {
  clog_info "BEGIN.......[Configure the \"setenv.sh\" file for the version of Tomcat that was installed]"
  local tomcatHome="$1"
  local pidFile=$(echo "\\$\{CATALINA_HOME\}/logs/tomcat${TOMCAT_VERSION}.pid")
  local escChars="\/"
  local splitStr=()
  local sep='/'
  local envCatalinaHome=""

  readarray -t splitStr <<< "${tomcatHome//$sep/$'\n'}"
  envCatalinaHome=$(printf "${escChars}%s" "${splitStr[@]:1}")

  readarray -t splitStr <<< "${pidFile//$sep/$'\n'}"

  pidFile=$(printf "${escChars}%s" "${splitStr[@]}")
  pidFile="${pidFile:${#escChars}}"

  sed -i "10s/.*/export CATALINA_HOME=$envCatalinaHome/" "${tomcatHome}/bin/setenv.sh"
  sed -i "11s/.*/export CATALINA_PID=$pidFile/" "${tomcatHome}/bin/setenv.sh"
  clog_info "END.......[Configure the \"setenv.sh\" file for the version of Tomcat that was installed]"
}

#---------------------------------------------------------------------------------------
#      METHOD:                      configureService
# DESCRIPTION: Copy and configure the "tomcat.service" file & wrapper script
#  Required Params:
#      1) installedDir - full path to the directory where "tomcat" was installed
#      2)   tomcatUser - name of the normal user that the service will run as
#---------------------------------------------------------------------------------------
configureService() {
  clog_info "BEGIN.......[Copy and configure the service file & wrapper script]"
  local installedDir="$1"
  local tomcatUser="$2"
  local fileName="tomcat.service"
  local path=()
  local sep="\/"
  local destDir=$(echo "${ROOT_MOUNTPOINT}/usr/lib/systemd/system")
  local shareDir=$(echo "${ROOT_MOUNTPOINT}/usr/share/apache")
  local usrBinDir=$(echo "${ROOT_MOUNTPOINT}/usr/bin")
  local softLink=$(echo "${shareDir}/tomcat")

  if [ ! -d "$shareDir" ]; then
    mkdir -p "$shareDir"
  fi

  if [ -L ${softLink} ]; then
    rm -rf ${softLink}
  fi
  ln -s "$installedDir" ${softLink}

  cp -rf "${SETTINGS_DIR}/$fileName" "${destDir}/."
  local newSetting="User=${tomcatUser}"
  sed -i "11s/.*/${newSetting}/" "${destDir}/$fileName"

  fileName="tomcat-srvc"
  cp -rf "${SETTINGS_DIR}/$fileName" "${usrBinDir}/."
  chown root:wheel "${usrBinDir}/$fileName"
  chmod 775 "${usrBinDir}/$fileName"
  clog_info "END.......[Copy and configure the service file & wrapper script]"
}

#---------------------------------------------------------------------------------------
#      METHOD:                       setPermissions
# DESCRIPTION: Set the permissions and ownership on the files and sub-directories
#  Required Params:
#      1) installedDir - full path to the directory where "tomcat" was installed
#---------------------------------------------------------------------------------------
setPermissions() {
  clog_info "BEGIN.......[Set the permissions and ownership on the files and sub-directories]"
  local installedDir="$1"

  chown -R root:wheel "$1"
  chmod 775 "$1"

  for dir in $(find "$1" -type d); do
    chmod 775 "$dir"
    for file in $(find "$dir" -maxdepth 1 -type f); do
      echo "$file" | grep -qE "\.sh$"
      local scriptFlag=$?
      echo "$file" | grep -qE "\.bat$"
      local batFlag=$?
      #echo "file: [$file], $?"
      if [ ${batFlag} -lt 1 ]; then
        rm -rf "$file"
      elif [ ${scriptFlag} -lt 1 ]; then
        chmod 775 "$file"
      else
        chmod 664 "$file"
      fi
    done
  done
  clog_info "END.......[Set the permissions and ownership on the files and sub-directories]"
}

#---------------------------------------------------------------------------------------
#  Main
#---------------------------------------------------------------------------------------
main() {
  if [ ! -d "/mnt/etc" ]; then
    ROOT_MOUNTPOINT=""
  fi

  local parent=$(ps -f -p $PPID | tail -1 | awk '{print $9}')
  local pos=$(findPositionForString EXPECTED_NAMES "$parent")

  if [ ${pos} -lt 0 ]; then
    local concatStr=$(printf "\n'%s'" "${EXPECTED_NAMES[@]}")
    clog_error "Must be called from:$concatStr"
    exit 1
  fi

  initVars "$@"

  if [ ${#PACKAGE_VERSION} -gt 0 ]; then
    echo "$PACKAGE_VERSION"
  else
    local pckgName=$(echo "${AUR_PCKG}$TOMCAT_VERSION")
    local existFlag=$(isAURPckgInst "$pckgName")
    if ${existFlag}; then
      clog_warn "The package '$pckgName' is already installed!"
    else
      installSoftware
    fi
  fi
}

main "$@"
exit 0
