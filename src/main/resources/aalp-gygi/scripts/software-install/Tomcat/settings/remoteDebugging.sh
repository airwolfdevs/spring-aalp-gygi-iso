#!/bin/sh
export JAVA_HOME=/usr/lib/jvm/default/jre
export PATH=${JAVA_HOME}/bin:${PATH}
export CATALINA_HOME=/opt/apache-tomcat-8.5.38
${CATALINA_HOME}/bin/catalina.sh jpda start