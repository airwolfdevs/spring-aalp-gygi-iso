#!/bin/sh
export JAVA_OPTS="$JAVA_OPTS -Xss1024M -Xms2048M -Xmx4096M"
export JAVA_OPTS="$JAVA_OPTS -Djava.awt.headless=true"
export JPDA_OPTS="-agentlib:jdwp=transport=dt_socket,address=1043,server=y,suspend=n"
