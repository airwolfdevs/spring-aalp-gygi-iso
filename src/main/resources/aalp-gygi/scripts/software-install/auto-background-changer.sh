#!/bin/bash

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# globals
#===============================================================================
CUR_DIR="${PWD}"
INC_FILE=$(echo "$CUR_DIR/inc/inst_inc.sh")

source "$INC_FILE"
declare DIALOG_BACK_TITLE="Step #4:  Post-installation"

#===============================================================================
# functions
#===============================================================================

#---------------------------------------------------------------------------------------
#    FUNCTION:                         isPckgInst
# DESCRIPTION: Check if an AUR package is installed.
#      RETURN: 0 - true, 1 - false
#  Required Params:
#      1) aurPckgName - name of the AUR package
#---------------------------------------------------------------------------------------
function isPckgInst() {
  local cmd=$(echo "sudo -u $TRIZEN_USER trizen -Qi $1")

  if changeRoot "$cmd" &> /dev/null; then
    echo 'true' && return 0
  fi
  echo 'false' && return 1
}

copyWallpapers() {
  local userName=$(getNameOfNormalUser)
  local destDir="${ROOT_MOUNTPOINT}/home/${userName}/Pictures"
  local srcDir="${CUR_DIR}/software-install/Auto-Background-Changer/Wallpapers"

  if [ ! -d "${destDir}/Wallpapers" ]; then
    cp -rf "$srcDir" "${destDir}/."
    destDir+="/Wallpapers"
    chown -R ${userName}:users "$destDir"
    chmod 744 "$destDir"
  fi
}

installDeps() {
  local aurPckgs=("nitrogen" "adwaita-dark-darose" "gtk-engine-murrine")
  for pckg in "${aurPckgs[@]}"; do
    local existFlag=$(isPckgInst "$pckg")
    if ! ${existFlag}; then
      sudo -u $TRIZEN_USER trizen -S --skipinteg --noconfirm "$pckg"
    fi
  done
}

installApp() {
  local destDir="${ROOT_MOUNTPOINT}/home/${TRIZEN_USER}/Auto-Background-Changer"
  local srcDir="${CUR_DIR}/software-install/Auto-Background-Changer"
  local pckgFile="${destDir}/$fileName"

  changeRoot "rm -rf $destDir"
  git clone https://github.com/AlvinJian/auto_background_changer "$destDir"

  chown -R root:wheel "$destDir"
  chmod -R 774 "$destDir"

  cp -rf "${srcDir}/random-bgchd" "${ROOT_MOUNTPOINT}/usr/bin/."
  cp -rf "${srcDir}/setup.py" "${destDir}/."
  cp -rf "${srcDir}/bgchd.py" "${destDir}/autobgch/."
  cp -rf "${srcDir}/nitrogen" "${destDir}/autobgch/scripts/."
  cd "$destDir"
  python3 setup.py install

  changeRoot "rm -rf $destDir"
}

#---------------------------------------------------------------------------------------
#  Main
#---------------------------------------------------------------------------------------
main() {
  copyWallpapers
  installDeps
  installApp
}

main "$@"
exit 0
