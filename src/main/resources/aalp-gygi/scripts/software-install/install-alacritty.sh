#!/bin/bash
#======================================================================================
#                                
# Author  : Thomas Bednarek
# License : Distributed under the terms of GNU GPL version 2 or later
#======================================================================================

#===============================================================================
# globals
#===============================================================================
CUR_DIR="${PWD}"
INC_FILE=$(echo "$CUR_DIR/inc/inst_inc.sh")

source "$INC_FILE"

declare -A POST_ASSOC_ARRAY=()

#---------------------------------------------------------------------------------------
#  Main
#---------------------------------------------------------------------------------------
main() {
  if [ ! -d "/mnt/etc" ]; then
    ROOT_MOUNTPOINT=""
  fi

  local args=("$@")
  local parent=$(ps -f -p $PPID | tail -1 | awk '{print $9}')
  local pos=$(findPositionForString EXPECTED_NAMES "$parent")

  if [ ${pos} -lt 0 ]; then
    local concatStr=$(printf "\n'%s'" "${EXPECTED_NAMES[@]}")
    clog_error "Must be called from:$concatStr"
    exit 1
  fi

  POST_ASSOC_ARRAY["UserName"]="$TRIZEN_USER"

  local cmd=""
  local pckgNames="ttf-google-fonts-git"
  local existFlag=$(isAURPckgInst "$pckgNames")
  local pckgs=("xclip" "ranger")
  if ! ${existFlag}; then
    pckgs+=("adobe-source-code-pro-fonts")
  fi

  pckgs+=("awesome-terminal-fonts" "alacritty")
  local aurPckgs=()
  for aurPckg in "${pckgs[@]}"; do
    existFlag=$(isAURPckgInst "$aurPckg")
    if ${existFlag}; then
      clog_warn "'$aurPckg' is already installed!"
    else
      aurPckgs+=("$aurPckg")
    fi
  done

  if [ ${#aurPckgs[@]} -gt 0 ]; then
    pckgNames=$(printf "\n'%s'" "${aurPckgs[@]}")
    clog_info "Installing AUR packages:$pckgNames"
    pckgNames=$(printf " %s" "${aurPckgs[@]}")
    cmd=$(echo "sudo -u $TRIZEN_USER trizen -S --skipinteg --noconfirm ${pckgNames:1}")
    changeRoot "${cmd}"
  fi
}

main "$@"
exit 0
