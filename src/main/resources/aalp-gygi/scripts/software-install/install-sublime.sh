#!/bin/bash

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# globals
#===============================================================================
CUR_DIR="${PWD}"
INC_FILE=$(echo "$CUR_DIR/inc/inst_inc.sh")
GPG_KEY="https://download.sublimetext.com/sublimehq-pub.gpg"

source "$INC_FILE"


#---------------------------------------------------------------------------------------
#    FUNCTION:                         isPckgInst
# DESCRIPTION: Check if an AUR package is installed.
#      RETURN: 0 - true, 1 - false
#  Required Params:
#      1) aurPckgName - name of the AUR package
#---------------------------------------------------------------------------------------
function isPckgInst() {
  local cmd=$(echo "sudo -u $TRIZEN_USER trizen -Qi $1")

  if changeRoot "$cmd" &> /dev/null; then
    echo 'true' && return 0
  fi
  echo 'false' && return 1
}

#---------------------------------------------------------------------------------------
#  Main
#---------------------------------------------------------------------------------------
main() {
  if [ ! -d "/mnt/etc" ]; then
    ROOT_MOUNTPOINT=""
  fi

  local existFlag=$(isPckgInst "$1")
  if ! ${existFlag}; then
    local srcFile="${ROOT_MOUNTPOINT}/etc/pacman.conf"
    local originalFile="${ROOT_MOUNTPOINT}/etc/pacman-conf.orig"

    log " "
    log " "
    log "################################################################"
    log "######       Installing    Sublime Text v3                ######"
    log "################################################################"

    if ! grep -q "sublime-text" ${srcFile}; then
      curl -O "$GPG_KEY"
      pacman-key --add sublimehq-pub.gpg
      pacman-key --lsign-key 8A8F901A
      rm sublimehq-pub.gpg

      local sublimeRepo="\n[sublime-text]\nServer = https://download.sublimetext.com/arch/stable/x86_64\n"

      cp -rf ${srcFile} ${originalFile}
      lineNum=$(awk '/archlinuxfr/{ print NR; exit }' ${originalFile})
      lineNum=$(echo "$(($lineNum - 1))")
      awk -v n=$lineNum -v s="$sublimeRepo" 'NR == n {print s} {print}' ${originalFile} > ${srcFile}
    fi

    local cmd=$(echo "sudo -u $TRIZEN_USER trizen -Syy")
    changeRoot "$cmd"
    cmd=$(echo "sudo -u $TRIZEN_USER trizen -S --skipinteg --noconfirm $1")
    changeRoot "$cmd"

    log " "
    log " "
    log "################################################################"
    log "######        Sublime Text v3 is installed                ######"
    log "################################################################"
  fi
}

main "$@"
exit 0
