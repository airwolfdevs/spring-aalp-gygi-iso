#!/bin/bash

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# globals
#===============================================================================
CUR_DIR="${PWD}"
INC_FILE=$(echo "$CUR_DIR/inc/inst_inc.sh")

source "$INC_FILE"
declare DIALOG_BACK_TITLE="Step #4:  Post-installation"

#===============================================================================
# functions
#===============================================================================

installYAD() {
  local destDir="${ROOT_MOUNTPOINT}/home/${TRIZEN_USER}/yad"
  if [[ -d "$destDir" ]]; then
    changeRoot "rm -rf $destDir"
  fi
  git clone https://github.com/v1cont/yad "$destDir"
  chown -R root:wheel "$destDir"
  chmod -R 774 "$destDir"

  cd "$destDir"
  sudo -u $TRIZEN_USER autoreconf -ivf && intltoolize
  sudo -u $TRIZEN_USER ./configure && make
  make install
}

#---------------------------------------------------------------------------------------
#  Main
#---------------------------------------------------------------------------------------
main() {
  installYAD
}

main "$@"
exit 0
