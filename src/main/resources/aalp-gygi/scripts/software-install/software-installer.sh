#!/bin/bash

#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# globals
#===============================================================================
CUR_DIR="${PWD}"
INC_FILE=$(echo "$CUR_DIR/inc/inst_inc.sh")

source "$INC_FILE"
source "$GRAPHICAL_PATH/soft-inst-inc.sh"

declare SOFT_INST_LOG="$INSTALLER_LOG"
declare AUR_HELPER_USER=""
declare DIALOG_BACK_TITLE="AALP-GYGI's Software Installer"
declare MENU_OPTS=("Alacritty" "Apache Tomcat" "Nexus OSS")
declare -A SI_SCRIPTS=(["Alacritty"]="software-install/Alacritty/install-alacritty.sh"
  ["Apache Tomcat"]="software-install/Tomcat/install-tomcat.sh"
  ["Nexus OSS"]="software-install/Nexus-OSS/install-nexus-oss.sh")
declare -A AUR_PCKG_DESCS=(["Alacritty"]=" — A cross-platform, GPU-accelerated terminal emulator."
  ["Apache Tomcat"]=" — Tomcat is an open source Java Servlet container developed by the Apache Software ..."
  ["Nexus OSS"]=" — Nexus 3 Repository OSS - Software Component Management"
)

declare TOMCAT_KEYS=("Tomcat 7" "Tomcat 8" "Tomcat 9")
declare -A TOMCAT_VERS=(["Tomcat 7"]="v7" ["Tomcat 8"]="v8" ["Tomcat 9"]="v9")
declare -A TOMCAT_VERSIONS=()

#===============================================================================
# functions/methods
#===============================================================================

#---------------------------------------------------------------------------------------
#      METHOD:                          initVars
# DESCRIPTION: Initialize the global variables.
#---------------------------------------------------------------------------------------
initVars() {
  local pbKeys=()
  local -A pbTitles=()

  for key in "${TOMCAT_KEYS[@]}"; do
    pbKeys+=("$key")
    pbTitles["$key"]="Getting latest version for '$key'......."
  done

  pbKeys+=("ahUser")
  pbTitles["ahUser"]="Getting name of user to execute AUR Helper......."

  pbKeys+=("done")
  pbTitles["done"]="Executed ALL the methods to initialize the data for the Installation Guide!!!!"
  dispLoadingDataPB "$DIALOG_BACK_TITLE" "Initializing Data"

  if [ -f "$UPD_ASSOC_ARRAY_FILE" ]; then
    source -- "${UPD_ASSOC_ARRAY_FILE}"
    rm -rf "${UPD_ASSOC_ARRAY_FILE}"
    for key in "${!updVarMap[@]}"; do
      case "$key" in
        "ahUser")
          AUR_HELPER_USER=$(echo "${updVarMap[$key]}")
        ;;
        *)
          TOMCAT_VERSIONS["$key"]=$(echo "${updVarMap[$key]}")
        ;;
      esac
    done
  fi
}

#---------------------------------------------------------------------------------------
#      METHOD:                       callBackMethod
# DESCRIPTION: Calls custom installer script for tomcat to get the latest version.  This
#              is called by the "dispLoadingDataPF" method.
#---------------------------------------------------------------------------------------
callBackMethod() {
  case "$pbKey" in
    "ahUser")
      local ahUser=$(getNameOfNormalUser)
      TOMCAT_VERSIONS["$pbKey"]="$ahUser"
    ;;
    "done")
      local -A updVarMap=()
      for key in "${!TOMCAT_VERSIONS[@]}"; do
        updVarMap["$key"]=$(echo "${TOMCAT_VERSIONS["$key"]}")
      done

      declare -p updVarMap > "${UPD_ASSOC_ARRAY_FILE}"
      sleep 0.5
    ;;
    *)
      local version="${TOMCAT_VERS["$pbKey"]}"
      local tomcatVer=$(./software-install/Tomcat/install-tomcat.sh -lv "$version")
      TOMCAT_VERSIONS["$pbKey"]="$tomcatVer"
      methodNum=$(expr $methodNum + 1)
      pbPerc=$(printf "%.0f" $(echo "scale=3; $methodNum / $totalMethods * 100" | bc))
    ;;
  esac
}

#---------------------------------------------------------------------------------------
#      METHOD:                        showFinalStep
# DESCRIPTION: Show a dialog of software names that have customized scripts to install
#              the software.
#---------------------------------------------------------------------------------------
showMenu() {
  local instScript=""
  local args=()

  showMenuDialog "$DIALOG_BACK_TITLE"

  if [ ${#mbSelVal} -gt 0 ]; then
    case "$mbSelVal" in
      "${MENU_OPTS[1]}")
        instScript="${SI_SCRIPTS["$mbSelVal"]}"
        local versOpts=()
        local -A versDescs=()
        for key in "${TOMCAT_KEYS[@]}"; do
          local ver="${TOMCAT_VERSIONS["$key"]}"
          versOpts+=("$ver")
          versDescs["$ver"]="$key"
        done
        selectTomcatVersion "$DIALOG_BACK_TITLE" "Latest Tomcat Versions" "Select version to install"
        if [ ${#mbSelVal} -gt 0 ]; then
          args=("-tv" "$mbSelVal" "-u" "$AUR_HELPER_USER")
        else
          instScript=""
        fi
      ;;
      *)
        instScript="${SI_SCRIPTS["$mbSelVal"]}"
      ;;
    esac
  fi

  if [ ${#instScript} -gt 0 ]; then
    ${instScript} "${args[@]}" >> "$SOFT_INST_LOG"
  fi
}

#---------------------------------------------------------------------------------------
#  Main
#---------------------------------------------------------------------------------------
main() {
  local isRoot=$(isRootUser)
  if ${isRoot}; then
    local parent=$(ps -f -p $PPID | tail -1 | awk '{print $NF}')
    if [ "$parent" == "/bin/bash" ]; then
      SOFT_INST_LOG="/tmp/aalp-gygi.log"
      initVars
      showMenu
    else
      local args=("$@")
      if [ ${#args[@]} -gt 0 ]; then
        local name="${args[0]}"
        if [ ${SI_SCRIPTS["$name"]+_} ]; then
          local script="${SI_SCRIPTS["$name"]}"
          args=(${args[@]:1})
          #### TODO:  Remove debug code
          ${script} "${args[@]}"
          #${script} "${args[@]}" >> "$SOFT_INST_LOG"
        else
          clog_error "\"$name\" is NOT defined/implemented"
        fi
      else
        clog_error "Usage: \"name of software\" [optional arguments]"
        exit 1
      fi
    fi

  else
    clog_error "You must execute the script as the 'root' user."
    exit 1
  fi
}

main "$@"
exit 0
