DROP INDEX IF EXISTS IDX_LEVEL2;
DROP INDEX IF EXISTS IDX_LEVEL3;
DROP INDEX IF EXISTS IDX_AUR_NAME;
DROP INDEX IF EXISTS IDX_AUR_PCKG;
DROP TABLE IF EXISTS post_install_files;
DROP TABLE IF EXISTS iso_build_commands;
DROP TABLE IF EXISTS custom_category_details;
DROP TABLE IF EXISTS custom_category_header;
DROP TABLE IF EXISTS category_options;
DROP TABLE IF EXISTS aur_packages;
DROP TABLE IF EXISTS menu_level_three;
DROP TABLE IF EXISTS menu_level_two;
DROP TABLE IF EXISTS menu_level_one;
DROP SEQUENCE IF EXISTS build_cmd_seq;
DROP SEQUENCE IF EXISTS level_one_seq;
DROP SEQUENCE IF EXISTS level_two_seq;
DROP SEQUENCE IF EXISTS level_three_seq;
DROP SEQUENCE IF EXISTS category_opts_seq;
DROP SEQUENCE IF EXISTS aur_pckg_seq;
DROP SEQUENCE IF EXISTS cc_hdr_id_seq;
DROP SEQUENCE IF EXISTS cc_dtl_id_seq;
DROP SEQUENCE IF EXISTS post_inst_seq;

CREATE TABLE iso_build_commands (
    id              BIGINT PRIMARY KEY,
    status          VARCHAR(10),
    cmd_name        VARCHAR(50),
    cmd_desc        VARCHAR(200),
    qualifier_name  VARCHAR(50)
);

CREATE TABLE post_install_files (
    id          BIGINT PRIMARY KEY,
    cmd_id      BIGINT,
    file_name   VARCHAR(200),
    hr_exp_size VARCHAR(10),
    output_dir  VARCHAR(50),
    CONSTRAINT FK_CMD FOREIGN KEY (cmd_id) REFERENCES iso_build_commands (id)
);

CREATE TABLE menu_level_one (
    id           BIGINT PRIMARY KEY,
    menu_option  VARCHAR(10),
    menu_label   VARCHAR(100)
);

CREATE TABLE menu_level_two (
    id              BIGINT PRIMARY KEY,
    level_one_id    BIGINT,
    menu_option     VARCHAR(10),
    menu_label      VARCHAR(100),
    script_var_name VARCHAR(100),
    CONSTRAINT FK_LEVEL2_1 FOREIGN KEY (level_one_id) REFERENCES menu_level_one (id)
);

CREATE TABLE menu_level_three (
    id              BIGINT PRIMARY KEY,
    level_two_id    BIGINT,
    menu_option     VARCHAR(10),
    menu_label      VARCHAR(100),
    script_var_name VARCHAR(100),
    CONSTRAINT FK_LEVEL3_2 FOREIGN KEY (level_two_id) REFERENCES menu_level_two (id)
);

CREATE TABLE aur_packages (
    id               BIGINT PRIMARY KEY,
    display_name     VARCHAR(50),
    aur_package      VARCHAR(100),
    custom_script    VARCHAR(100),
    aur_description  VARCHAR(2000)
);

CREATE TABLE category_options (
    id               BIGINT PRIMARY KEY,
    level_two_id     BIGINT,
    level_three_id   BIGINT,
    aur_package_id   BIGINT,
    CONSTRAINT FK_LEVEL2_CHOICE FOREIGN KEY (level_two_id) REFERENCES menu_level_two (id),
    CONSTRAINT FK_LEVEL3_CHOICE FOREIGN KEY (level_three_id) REFERENCES menu_level_three (id),
    CONSTRAINT FK_LEVEL_AUR FOREIGN KEY (aur_package_id) REFERENCES aur_packages (id)
);

CREATE TABLE custom_category_header (
    id              BIGINT PRIMARY KEY,
    name            VARCHAR(50),
    script_var_name VARCHAR(50)
);

CREATE TABLE custom_category_details (
    id              BIGINT PRIMARY KEY,
    cc_hdr_id       BIGINT,
    aur_package_id  BIGINT,
    CONSTRAINT FK_HDR_CHOICE FOREIGN KEY (cc_hdr_id) REFERENCES custom_category_header (id),
    CONSTRAINT FK_DTL_AUR FOREIGN KEY (aur_package_id) REFERENCES aur_packages (id)
);

CREATE SEQUENCE build_cmd_seq AS INTEGER START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE level_one_seq AS INTEGER START WITH 1000 INCREMENT BY 1;
CREATE SEQUENCE level_two_seq AS INTEGER START WITH 2000 INCREMENT BY 2;
CREATE SEQUENCE level_three_seq AS INTEGER START WITH 3000 INCREMENT BY 3;
CREATE SEQUENCE category_opts_seq AS INTEGER START WITH 4000 INCREMENT BY 4;
CREATE SEQUENCE aur_pckg_seq AS INTEGER START WITH 100 INCREMENT BY 1;
CREATE SEQUENCE cc_hdr_id_seq AS INTEGER START WITH 10 INCREMENT BY 1;
CREATE SEQUENCE cc_dtl_id_seq AS INTEGER START WITH 20 INCREMENT BY 2;
CREATE SEQUENCE post_inst_seq AS INTEGER START WITH 100 INCREMENT BY 10;

CREATE UNIQUE INDEX IDX_LEVEL2 ON menu_level_two(menu_option);
CREATE UNIQUE INDEX IDX_LEVEL3 ON menu_level_three(menu_option);
CREATE UNIQUE INDEX IDX_AUR_NAME ON aur_packages(display_name);
CREATE INDEX IDX_AUR_PCKG ON aur_packages(aur_package);