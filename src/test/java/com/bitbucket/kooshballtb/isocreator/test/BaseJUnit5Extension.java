package com.bitbucket.kooshballtb.isocreator.test;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.AfterTestExecutionCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.BeforeTestExecutionCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

@Slf4j
public class BaseJUnit5Extension implements AfterEachCallback, AfterTestExecutionCallback
                                          , BeforeAllCallback, BeforeEachCallback, BeforeTestExecutionCallback {
  private static final String ENTERING_TEXT = "Entering.....................";
  private static final String EXITING_TEXT = "Exiting......................";
  private String lastExecutedMethod = "";
  private Map<String, String> methodResults = new LinkedHashMap<>();

  @Override
  public void afterEach(ExtensionContext extensionContext) throws Exception {
    log.debug(ENTERING_TEXT);
    log.debug(EXITING_TEXT);
  }

  @Override
  public void afterTestExecution(ExtensionContext ctx) throws Exception {
    log.debug(ENTERING_TEXT);
    String testMethodName = ctx.getDisplayName();
    log.debug("dispalyName: [{}]", testMethodName);
    Optional<Throwable> testExc = ctx.getExecutionException();
    if(testExc.isPresent()) {
      log.info("throwable: [{}]", testExc.get().getClass());
      if(testExc.get().getClass().getName().equals("org.opentest4j.AssertionFailedError")) {
        methodResults.put(testMethodName, "FAILED");
      } else {
        methodResults.put(testMethodName, "SKIPPED");
      }
    } else {
      methodResults.put(testMethodName, "PASSED");
    }
    log.debug(EXITING_TEXT);
  }

  @Override
  public void beforeAll(ExtensionContext extensionContext) throws Exception {
    log.debug(ENTERING_TEXT);
    log.debug(EXITING_TEXT);
  }

  @Override
  public void beforeEach(ExtensionContext extensionContext) throws Exception {
    log.debug(ENTERING_TEXT);
    log.debug(EXITING_TEXT);
  }

  @Override
  public void beforeTestExecution(ExtensionContext ctx) throws Exception {
    log.debug(ENTERING_TEXT);
    String status = methodResults.getOrDefault(lastExecutedMethod, "PASSED");
    Assumptions.assumeTrue(status.equalsIgnoreCase("PASSED"));
    lastExecutedMethod = ctx.getDisplayName();
    log.debug(EXITING_TEXT);
  }
}
