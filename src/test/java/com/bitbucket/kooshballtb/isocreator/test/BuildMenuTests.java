package com.bitbucket.kooshballtb.isocreator.test;

import com.bitbucket.kooshballtb.isocreator.common.CommandStatusEnum;
import com.bitbucket.kooshballtb.isocreator.common.FileVisitorUtils;
import com.bitbucket.kooshballtb.isocreator.common.UtilityBean;
import com.bitbucket.kooshballtb.isocreator.config.ApplicationConfig;
import com.bitbucket.kooshballtb.isocreator.config.YAMLConfig;
import com.bitbucket.kooshballtb.isocreator.models.CategoryOptions;
import com.bitbucket.kooshballtb.isocreator.models.CustomCategoryDetails;
import com.bitbucket.kooshballtb.isocreator.models.CustomCategoryHeader;
import com.bitbucket.kooshballtb.isocreator.models.IsoBuildCommands;
import com.bitbucket.kooshballtb.isocreator.models.MenuLevelTwo;
import com.bitbucket.kooshballtb.isocreator.models.PostInstallFiles;
import com.bitbucket.kooshballtb.isocreator.services.commands.GenBashScriptFileService;
import com.bitbucket.kooshballtb.isocreator.services.commands.impl.GenDesktopConfigFileServiceImpl;
import com.bitbucket.kooshballtb.isocreator.services.jpa.CustomCategoryHeaderService;
import com.bitbucket.kooshballtb.isocreator.services.jpa.IsoBuildCommandsService;
import com.bitbucket.kooshballtb.isocreator.services.jpa.MenuLevelAbstractFactory;
import com.bitbucket.kooshballtb.isocreator.test.enums.CustomPckgInstEnum;
import com.bitbucket.kooshballtb.isocreator.test.enums.CustomSoftwareEnum;
import com.bitbucket.kooshballtb.isocreator.test.enums.DesktopEnvironmentsEnum;
import com.bitbucket.kooshballtb.isocreator.test.enums.DisplayManagersEnum;
import com.bitbucket.kooshballtb.isocreator.test.enums.JavaDevKitEnum;
import com.bitbucket.kooshballtb.isocreator.test.enums.WindowManagersEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

import static com.bitbucket.kooshballtb.isocreator.services.commands.impl.GenAurRepoListServiceImpl.AUR_REPO_FILES;
import static com.bitbucket.kooshballtb.isocreator.services.commands.impl.GenDesktopConfigFileServiceImpl.FILE_NAME;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertLinesMatch;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith({SpringExtension.class, BaseJUnit5Extension.class})
@ContextConfiguration(classes = {ApplicationConfig.class, YAMLConfig.class}, initializers = ConfigFileApplicationContextInitializer.class)
@ActiveProfiles("test")
@Slf4j
public class BuildMenuTests {
  private static final Map<String,List<String>> CMD_MAP = new LinkedHashMap<>() {
    {
      put("Initial Setup", List.of("Create the directory for the AALP-GYGInstaller", "buildSetupServiceImpl"));
      put("Download HTML", List.of("Download the HTML files containing the data used to create the scripts for the \"Post-Installation\" step.", "htmlFileDownloadServiceImpl"));
      put("Desktop Conf. File", List.of("Create the script file that is used by the \"Desktop Configuration\" task", "genDesktopConfigFileServiceImpl"));
      put("Required Files", List.of("Create the required data & script files needed by the AALP-GYGInstaller", "genRequiredFilesServiceImpl"));
      put("AUR Repo. Files", List.of("Create the files to select the current signed & unsigned AUR Repositories", "genAurRepoListServiceImpl"));
      put("Post-Inst. Scripts", List.of("Create the script files for the menus of the \"Post-Installation\" step", "genPostInstMenuServiceImpl"));
      put("Validate Files", List.of("Validate the data & script files created for the \"Post-Installation\" step", "validateFilesServiceImpl"));
      put("Build the ISO", List.of("Build the ArchISO to run the AALP-GYGInstaller", "buildIsoServiceImpl"));
    }
  };
  private static final Map<String, List<Map<String, String>>> EXP_FILE_MAP = new HashMap<>() {
    {
      put("Desktop Conf. File", List.of(Map.of("file_name", "de-dm-wm-consts.sh", "hr_exp_size", "13 KB", "output_dir", "inc/post-inst")));
      put("Required Files", List.of(Map.of("file_name", "dialog-split-screenrc", "hr_exp_size", "162 bytes", "output_dir", "data")
                                  , Map.of("file_name", "aalp-gygi-tail.sh", "hr_exp_size", "1 KB", "output_dir", "")));
      put("AUR Repo. Files", List.of(Map.of("file_name", "aur-signed-repos.sh", "hr_exp_size", "11 KB", "output_dir", "inc/post-inst")
                                  , Map.of("file_name", "aur-unsigned-repos.sh", "hr_exp_size", "4 KB", "output_dir", "inc/post-inst")));
      put("Post-Inst. Scripts", List.of(Map.of("file_name", "IDEs.sh", "hr_exp_size", "4 KB", "output_dir", "inc/post-inst")
                                      , Map.of("file_name", "calendars.sh", "hr_exp_size", "2 KB", "output_dir", "inc/post-inst")
                                      , Map.of("file_name", "cat-accessory-soft.sh", "hr_exp_size", "6 KB", "output_dir", "inc/post-inst")
                                      , Map.of("file_name", "cat-basic-setup.sh", "hr_exp_size", "7 KB", "output_dir", "inc/post-inst")
                                      , Map.of("file_name", "cat-db-soft.sh", "hr_exp_size", "4 KB", "output_dir", "inc/post-inst")
                                      , Map.of("file_name", "cat-desktop-config.sh", "hr_exp_size", "4 KB", "output_dir", "inc/post-inst")
                                      , Map.of("file_name", "cat-dev-soft.sh", "hr_exp_size", "5 KB", "output_dir", "inc/post-inst")
                                      , Map.of("file_name", "cat-doc-text-soft.sh", "hr_exp_size", "2 KB", "output_dir", "inc/post-inst")
                                      , Map.of("file_name", "cat-internet-soft.sh", "hr_exp_size", "3 KB", "output_dir", "inc/post-inst")
                                      , Map.of("file_name", "cat-multimedia-soft.sh", "hr_exp_size", "3 KB", "output_dir", "inc/post-inst")
                                      , Map.of("file_name", "cat-security.sh", "hr_exp_size", "5 KB", "output_dir", "inc/post-inst")
                                      , Map.of("file_name", "cat-servers-soft.sh", "hr_exp_size", "2 KB", "output_dir", "inc/post-inst")
                                      , Map.of("file_name", "cat-soft-extras.sh", "hr_exp_size", "3 KB", "output_dir", "inc/post-inst")
                                      , Map.of("file_name", "cat-sys-util-soft.sh", "hr_exp_size", "2 KB", "output_dir", "inc/post-inst")
                                      , Map.of("file_name", "codecs.sh", "hr_exp_size", "2 KB", "output_dir", "inc/post-inst")
                                      , Map.of("file_name", "communication.sh", "hr_exp_size", "2 KB", "output_dir", "inc/post-inst")
                                      , Map.of("file_name", "desktop-icon-themes.sh", "hr_exp_size", "3 KB", "output_dir", "inc/post-inst")
                                      , Map.of("file_name", "file-sharing.sh", "hr_exp_size", "2 KB", "output_dir", "inc/post-inst")
                                      , Map.of("file_name", "files.sh", "hr_exp_size", "5 KB", "output_dir", "inc/post-inst")
                                      , Map.of("file_name", "fonts.sh", "hr_exp_size", "2 KB", "output_dir", "inc/post-inst")
                                      , Map.of("file_name", "image.sh", "hr_exp_size", "3 KB", "output_dir", "inc/post-inst")
                                      , Map.of("file_name", "kde-constants.sh", "hr_exp_size", "7 KB", "output_dir", "inc/post-inst")
                                      , Map.of("file_name", "network-connection.sh", "hr_exp_size", "3 KB", "output_dir", "inc/post-inst")
                                      , Map.of("file_name", "note-taking.sh", "hr_exp_size", "3 KB", "output_dir", "inc/post-inst")
                                      , Map.of("file_name", "password-managers.sh", "hr_exp_size", "3 KB", "output_dir", "inc/post-inst")
                                      , Map.of("file_name", "readers-viewers.sh", "hr_exp_size", "2 KB", "output_dir", "inc/post-inst")
                                      , Map.of("file_name", "soft-cats.sh", "hr_exp_size", "1 KB", "output_dir", "inc/post-inst")
                                      , Map.of("file_name", "system-utils.sh", "hr_exp_size", "3 KB", "output_dir", "inc/post-inst")
                                      , Map.of("file_name", "taskbars.sh", "hr_exp_size", "2 KB", "output_dir", "inc/post-inst")
                                      , Map.of("file_name", "terminal-emulators.sh", "hr_exp_size", "2 KB", "output_dir", "inc/post-inst")
                                      , Map.of("file_name", "text-editors.sh", "hr_exp_size", "2 KB", "output_dir", "inc/post-inst")
                                      , Map.of("file_name", "uml-modelers.sh", "hr_exp_size", "2 KB", "output_dir", "inc/post-inst")
                                      , Map.of("file_name", "video-card-drivers.sh", "hr_exp_size", "5 KB", "output_dir", "inc/post-inst")
                                      , Map.of("file_name", "web-browsers.sh", "hr_exp_size", "3 KB", "output_dir", "inc/post-inst")
                                      , Map.of("file_name", "web-servers.sh", "hr_exp_size", "2 KB", "output_dir", "inc/post-inst")));
    }
  };
  @Autowired
  private YAMLConfig ymlConfig;

  @Autowired
  private UtilityBean utilBean;

  @Autowired
  private FileVisitorUtils fileVisitor;

  @Autowired
  @Qualifier("buildSetupServiceImpl")
  private GenBashScriptFileService setupSrvc;

  @Autowired
  @Qualifier("genDesktopConfigFileServiceImpl")
  private GenBashScriptFileService deskConfigSrvc;

  @Autowired
  @Qualifier("htmlFileDownloadServiceImpl")
  private GenBashScriptFileService htmlSrvc;

  @Autowired
  @Qualifier("genRequiredFilesServiceImpl")
  private GenBashScriptFileService reqSrvc;

  @Autowired
  @Qualifier("genAurRepoListServiceImpl")
  private GenBashScriptFileService repoSrvc;

  @Autowired
  @Qualifier("genPostInstMenuServiceImpl")
  private GenBashScriptFileService postInstSrvc;

  @Autowired
  @Qualifier("validateFilesServiceImpl")
  private GenBashScriptFileService valSrvc;

  @Autowired
  @Qualifier("buildIsoServiceImpl")
  private GenBashScriptFileService isoSrvc;

  @Autowired
  @Qualifier("menuLevelTwoServiceImpl")
  private MenuLevelAbstractFactory level2Service;

  @Autowired
  @Qualifier("customCategoryHeaderServiceImpl")
  private CustomCategoryHeaderService cchSrvc;

  @Autowired
  @Qualifier("isoBuildCommandsServiceImpl")
  private IsoBuildCommandsService cmdSrvc;

  @Autowired
  private ApplicationContext appCtx;

  private Path menuFile;
  private Path dataFile;

  @BeforeEach
  void setUserDir() {
    dataFile = Paths.get(System.getProperty("user.dir"), "src", "main", "resources","data.sql");
    menuFile = Paths.get(System.getProperty("user.dir"), "src", "main", "resources", "config", "menu.txt");
    Path path = Paths.get(System.getProperty("user.dir"), "target");
    System.setProperty("user.dir", path.toString());
  }

  @Test
  public void createDataFileTest() throws IOException {
    Map<String, String> mapLevel1 = new LinkedHashMap<>();
    Map<String, Map<String, String>> mapLevel2 = new LinkedHashMap<>();
    Map<String, Map<String, String>> mapLevel3 = new LinkedHashMap<>();
    readMenuFile(mapLevel1, mapLevel2, mapLevel3);
    createDataFile(mapLevel1, mapLevel2, mapLevel3);
    assertEquals(119, getNumLines(dataFile));

    appendBuildCommands();
    appendAurPackages();
    appendCstomChoicesHeaders();
    assertEquals(594, getNumLines(dataFile));
  }

  private int getNumLines(Path path) throws IOException {
    FileInputStream fis = new FileInputStream(path.toFile());
    byte[] byteArray = new byte[(int)path.toFile().length()];
    fis.read(byteArray);
    String data = new String(byteArray);
    String[] stringArray = StringUtils.split(data, '\n');
    return stringArray.length;
  }

  private void createDataFile(Map<String, String> mapLevel1, Map<String, Map<String, String>> mapLevel2
                            , Map<String, Map<String, String>> mapLevel3) throws IOException {
    List<String> sqlStatements = new ArrayList<>();
    for (Map.Entry<String, String> me : mapLevel1.entrySet()) {
      String keyLevel1 = me.getKey();
      String label = me.getValue();
      sqlStatements.add(String.format("INSERT INTO menu_level_one (id, menu_option, menu_label) VALUES ((NEXT VALUE FOR level_one_seq), '%s', '%s');",
          keyLevel1, label));
      appendLevelTwo(keyLevel1, mapLevel2, mapLevel3, sqlStatements);
    }

    Files.write(dataFile, sqlStatements);
  }

  private void appendBuildCommands() throws IOException {
    List<String> sqlStatements = new ArrayList<>();
    for(Map.Entry<String, List<String>> me: CMD_MAP.entrySet()) {
      List<String> list = me.getValue();
      String cmdName = me.getKey();
      sqlStatements.add(String.format("INSERT INTO iso_build_commands (id, status, cmd_name, cmd_desc, qualifier_name) VALUES ((NEXT VALUE FOR build_cmd_seq), ' ', '%s', '%s', '%s');",
          cmdName, list.get(0), list.get(1)));
      List<Map<String, String>> expFiles = EXP_FILE_MAP.get(cmdName);
      if(CollectionUtils.isNotEmpty(expFiles)) {
        for (Map<String, String> expMap : expFiles) {
          sqlStatements.add(String.format("INSERT INTO post_install_files (%s) VALUES (%s, %s, '%s', '%s', '%s');"
              , "id, cmd_id, file_name, hr_exp_size , output_dir", "(NEXT VALUE FOR post_inst_seq)"
              , "(SELECT MAX(ID) FROM iso_build_commands)", expMap.get("file_name"), expMap.get("hr_exp_size")
              , expMap.get("output_dir")));
        }
      }

    }
    Files.write(dataFile, sqlStatements, StandardOpenOption.APPEND);
  }

  private void appendLevelTwo(String keyLevel1, Map<String, Map<String, String>> mapLevel2, Map<String
                            , Map<String, String>> mapLevel3, List<String> sqlStatements) {
    Map<String, String> map = mapLevel2.get(keyLevel1);
    for (Map.Entry<String, String> me : map.entrySet()) {
      String keyLevel2 = me.getKey();
      String label = me.getValue();
      String varName = getVarName(label);
      if(StringUtils.isNotBlank(varName)) {
        log.info("Level2: {} - {}", label, varName);
        sqlStatements.add(String.format("INSERT INTO menu_level_two (id, level_one_id, menu_option, menu_label, script_var_name) VALUES ((NEXT VALUE FOR level_two_seq), (SELECT MAX(ID) FROM menu_level_one), '%s', '%s', '%s');",
            keyLevel2, label, varName));
      } else {
        sqlStatements.add(String.format("INSERT INTO menu_level_two (id, level_one_id, menu_option, menu_label) VALUES ((NEXT VALUE FOR level_two_seq), (SELECT MAX(ID) FROM menu_level_one), '%s', '%s');",
            keyLevel2, label));
      }
      appendLevelThree(keyLevel2, mapLevel3, sqlStatements);
    }
  }

  private void appendLevelThree(String keyLevel2, Map<String, Map<String, String>> mapLevel3, List<String> sqlStatements) {
    Map<String, String> map = mapLevel3.get(keyLevel2);
    if(MapUtils.isNotEmpty(map)) {
      for (Map.Entry<String, String> me : map.entrySet()) {
        String label = me.getValue();
        String varName = getVarName(label);
        if(StringUtils.isNotBlank(varName)) {
          log.info("Level3: {} - {}", label, varName);
          sqlStatements.add(String.format("INSERT INTO menu_level_three (id, level_two_id, menu_option, menu_label, script_var_name) VALUES ((NEXT VALUE FOR level_three_seq), (SELECT MAX(ID) FROM menu_level_two), '%s', '%s', '%s');",
              me.getKey(), label, varName));
        } else {
          sqlStatements.add(String.format("INSERT INTO menu_level_three (id, level_two_id, menu_option, menu_label) VALUES ((NEXT VALUE FOR level_three_seq), (SELECT MAX(ID) FROM menu_level_two), '%s', '%s');",
              me.getKey(), label));
        }
      }
    }
  }

  private void readMenuFile(Map<String, String> mapLevel1, Map<String, Map<String, String>> mapLevel2, Map<String, Map<String, String>> mapLevel3) {
    int level1 = 0;
    int level2 = 0;
    int level3 = 0;
    String keyLevel1 = "";
    String keyLevel2 = "";
    String keyLevel3;
    Map<String, String> map;

    try (InputStream is = Files.newInputStream(menuFile) ;
         Scanner scanner = new Scanner(is) ) {
      while (scanner.hasNextLine()) {
        String line = scanner.nextLine();
        String opt = line.trim();
        if(StringUtils.isNotBlank(opt)) {
          int count = line.indexOf(opt);
          int level = (count/2) + 1;
          switch(level) {
            case 1: {
              level1++;
              level2 = 0;
              keyLevel1 = String.format("4.%d", level1);
              mapLevel1.put(keyLevel1, opt);
              break;
            }
            case 2: {
              level2++;
              level3 = 0;
              keyLevel2 = String.format("%s.%d", keyLevel1, level2);
              map = mapLevel2.get(keyLevel1);
              if (MapUtils.isEmpty(map)) {
                map = new LinkedHashMap<>();
                mapLevel2.put(keyLevel1, map);
              }
              map.put(keyLevel2, opt);
              break;
            }
            case 3: {
              level3++;
              keyLevel3 = String.format("%s.%d", keyLevel2, level3);
              map = mapLevel3.get(keyLevel2);
              if (MapUtils.isEmpty(map)) {
                map = new LinkedHashMap<>();
                mapLevel3.put(keyLevel2, map);
              }
              map.put(keyLevel3, opt);
              break;
            }
            default: {
              log.error("No case defined for Level: [{}] - [{}]", level, opt);
              break;
            }
          }
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void appendAurPackages() throws IOException {
    List<String> sqlStatements = new ArrayList<>();
    Map<String, String> aurMap = new HashMap<>();

    for (Map.Entry<String, String> me : GenDesktopConfigFileServiceImpl.WINDOW_MANAGER_TYPES.entrySet()) {
      String[] splitArray = StringUtils.split(me.getValue(), "'");
      String desc = (splitArray.length > 1) ? StringUtils.join(splitArray, "''") : me.getValue();
      sqlStatements.add(String.format("INSERT INTO aur_packages (id, display_name, aur_description) VALUES ((NEXT VALUE FOR aur_pckg_seq), '%s', '%s');",
          me.getKey(), desc));
    }

    EnumSet.allOf(CustomSoftwareEnum.class).forEach(custe -> {
      String[] splitArray = StringUtils.split(custe.getDescription(), "'");
      String name = custe.getAurName();
      String pckg = custe.getAurPckg();
      assertFalse(aurMap.containsKey(name), () -> String.format("Duplicate key: [%s] in CustomSoftwareEnum", name));
      aurMap.put(name, pckg);
      String desc = (splitArray.length > 1) ? StringUtils.join(splitArray, "''") : custe.getDescription();
      sqlStatements.add(String.format("INSERT INTO aur_packages (id, display_name, aur_package, aur_description) VALUES ((NEXT VALUE FOR aur_pckg_seq), '%s', '%s', '%s');",
          name, pckg, desc));
    });

    EnumSet.allOf(DesktopEnvironmentsEnum.class).forEach(dee -> {
      String pckgName = dee.getAurName();
      assertFalse(aurMap.containsKey(pckgName), () -> String.format("Duplicate key: [%s] in DesktopEnvironmentsEnum", pckgName));
      aurMap.put(pckgName, pckgName);
      String[] splitArray = StringUtils.split(dee.getDescription(), "'");
      String desc = (splitArray.length > 1) ? StringUtils.join(splitArray, "''") : dee.getDescription();
      sqlStatements.add(String.format("INSERT INTO aur_packages (id, display_name, aur_package, aur_description) VALUES ((NEXT VALUE FOR aur_pckg_seq), '%s', '%s', '%s');",
          pckgName, pckgName, desc));
    });
    EnumSet.allOf(DisplayManagersEnum.class).forEach(dme -> {
      String pckgName = dme.getAurName();
      assertFalse(aurMap.containsKey(pckgName), () -> String.format("Duplicate key: [%s] in DisplayManagersEnum", pckgName));
      aurMap.put(pckgName, pckgName);
      String[] splitArray = StringUtils.split(dme.getDescription(), "'");
      String desc = (splitArray.length > 1) ? StringUtils.join(splitArray, "''") : dme.getDescription();
      sqlStatements.add(String.format("INSERT INTO aur_packages (id, display_name, aur_package, aur_description) VALUES ((NEXT VALUE FOR aur_pckg_seq), '%s', '%s', '%s');",
          pckgName, pckgName, desc));
    });

    EnumSet.allOf(JavaDevKitEnum.class).forEach(jdke -> {
      String name = jdke.getAurName();
      String pckg = jdke.getAurPckg();
      assertFalse(aurMap.containsKey(name), () -> String.format("Duplicate key: [%s] in JavaDevKitEnum", name));
      aurMap.put(name, pckg);
      String[] splitArray = StringUtils.split(jdke.getDescription(), "'");
      String desc = (splitArray.length > 1) ? StringUtils.join(splitArray, "''") : jdke.getDescription();
      sqlStatements.add(String.format("INSERT INTO aur_packages (id, display_name, aur_package, aur_description) VALUES ((NEXT VALUE FOR aur_pckg_seq), '%s', '%s', '%s');",
          name, pckg, desc));
    });

    EnumSet.allOf(WindowManagersEnum.class).forEach(wme -> {
      String pckgName = wme.getAurName();
      assertFalse(aurMap.containsKey(pckgName), () -> String.format("Duplicate key: [%s] in WindowManagersEnum", pckgName));
      aurMap.put(pckgName, pckgName);
      String[] splitArray = StringUtils.split(wme.getDescription(), "'");
      String desc = (splitArray.length > 1) ? StringUtils.join(splitArray, "''") : wme.getDescription();
      sqlStatements.add(String.format("INSERT INTO aur_packages (id, display_name, aur_package, aur_description) VALUES ((NEXT VALUE FOR aur_pckg_seq), '%s', '%s', '%s');",
          pckgName, pckgName, desc));
    });

    EnumSet.allOf(CustomPckgInstEnum.class).forEach(cpie -> {
      String[] splitArray = StringUtils.split(cpie.getDesc(), "'");
      String desc = (splitArray.length > 1) ? StringUtils.join(splitArray, "''") : cpie.getDesc();
      sqlStatements.add(String.format("INSERT INTO aur_packages (id, display_name, custom_script, aur_description) VALUES ((NEXT VALUE FOR aur_pckg_seq), '%s', '%s', '%s');",
          cpie.getPckgName(), cpie.getScriptCmd(), desc));
    });

    Files.write(dataFile, sqlStatements, StandardOpenOption.APPEND);
  }

  private void appendCstomChoicesHeaders() throws IOException {
    List<String> sqlStatements = new ArrayList<>();
    List<String> names = List.of("AMD / ATI", "AMDGPU", "AMD Catalyst", "ATI", "Brands", "Bumblebee", "Intel", "KDE Themes"
        , "KDE Administer CUPS", "KDE Application Menu Editors", "KDE Audio & Video Codecs", "Nouveau", "NVIDIA"
        , "NVIDIA Proprietary", "VirtualBox", "VMware", "Vulkan");
    for (String name : names) {
      String varName = getVarName(name);
      sqlStatements.add(String.format("INSERT INTO custom_category_header (id, name, script_var_name) VALUES ((NEXT VALUE FOR cc_hdr_id_seq), '%s', '%s');",
          name, varName));
    }
    Files.write(dataFile, sqlStatements, StandardOpenOption.APPEND);
  }

  private String getVarName(String key) {
    String varName;
    switch (key) {
      case "AMD / ATI": {
        varName = "BRAND_AMD_ATI";
        break;
      }
      case "AMDGPU": {
        varName = "AMDGPU_PCKGS";
        break;
      }
      case "AMD Catalyst": {
        varName = "CATALYST_PCKGS";
        break;
      }
      case "Anti Malware & File Security": {
        varName = "MALWARE_FILE_SEC";
        break;
      }
      case "Archiving & Compression Tools": {
        varName = "ARCH_COMP_TOOLS";
        break;
      }
      case "ATI": {
        varName = "ATI_PCKGS";
        break;
      }
      case "Brands": {
        varName = "BRANDS";
        break;
      }
      case "Bumblebee": {
        varName = "BUMBLEBEE_PCKGS";
        break;
      }
      case "Command Line": {
        varName = "CLI_AUR_HELPERS";
        break;
      }
      case "Comparison, Diff, Merge": {
        varName = "COMP_DIFF_MERGE";
        break;
      }
      case "Desktop & Icon Themes": {
        varName = "DESK_ICON_THEMES";
        break;
      }
      case "E-book": {
        varName = "E_BOOK";
        break;
      }
      case "Graphical": {
        varName = "GUI_AUR_HELPERS";
        break;
      }
      case "Install CUPS": {
        varName = "CUPS_PCKGS";
        break;
      }
      case "Install NFS": {
        varName = "NFS_PCKG";
        break;
      }
      case "Install Samba": {
        varName = "SAMBA_PCKGS";
        break;
      }
      case "Install SSH": {
        varName = "SSH_IMPLS";
        break;
      }
      case "Install TLP": {
        varName = "TLP_PCKGS";
        break;
      }
      case "Install ZRam Swap": {
        varName = "ZRAM_PCKG";
        break;
      }
      case "Intel": {
        varName = "BRAND_INTEL";
        break;
      }
      case "KDE Themes": {
        varName = key;
        break;
      }
      case "KDE Administer CUPS": {
        varName = "KDE_ADMIN_CUPS";
        break;
      }
      case "KDE Application Menu Editors": {
        varName = "KDE_APP_MENU_EDITOR";
        break;
      }
      case "KDE Audio & Video Codecs": {
        varName = "KDE_CODECS";
        break;
      }
      case "Nouveau": {
        varName = "NOUVEAU_PCKGS";
        break;
      }
      case "NVIDIA": {
        varName = "BRAND_NVIDIA";
        break;
      }
      case "NVIDIA Proprietary": {
        varName = "NVIDIA_PROPRIETARY_PCKGS";
        break;
      }
      case "PDF & DjVu": {
        varName = "PDF_DjVu";
        break;
      }
      case "Screen Savers/Lockers": {
        varName = "SCREEN_SAVERS";
        break;
      }
      case "Set Default Text Editor": {
        varName = "DEFAULT_EDITORS";
        break;
      }
      case "System Log & File Viewers": {
        varName = "LOG_FILE_VIEWERS";
        break;
      }
      case "Threat & Vulnerability Detection": {
        varName = "THREAT_VUL_DETECTION";
        break;
      }
      case "VirtualBox": {
        varName = "VIRTUALBOX_PCKGS";
        break;
      }
      case "VMware": {
        varName = "VMWARE_PCKGS";
        break;
      }
      case "Vulkan": {
        varName = "VULKAN_PCKGS";
        break;
      }
      default: {
        varName = "";
        break;
      }
    }
    return varName;
  }

  @Test
  public void buildSetupTest() {
    CommandStatusEnum cmdStatus = setupSrvc.execService();
    assertEquals(CommandStatusEnum.DONE, cmdStatus);

    Path baseDir = Paths.get("/home", "mustang", "IdeaProjects", "spring-aalp-gygi-iso", "target");
    Path origUserDir = Paths.get(System.getProperty("orig.user.dir"));
    Path scriptsDir = Paths.get(baseDir.toString(), ymlConfig.getBashProj(), "scripts");
    Path userDir = Paths.get(System.getProperty("user.dir"));

    assertEquals(baseDir, origUserDir);
    assertEquals(scriptsDir, userDir);
  }

  @Test
  public void downloadHtmlTest() {
    CommandStatusEnum cmdStatus = htmlSrvc.execService();
    assertEquals(CommandStatusEnum.DONE, cmdStatus);
  }

  @Test
  public void desktopConfigFileTest() throws IOException {
    setupSrvc.execService();

    Path incFile = Paths.get(System.getProperty("user.dir"), "inc", "post-inst", FILE_NAME);
    Files.deleteIfExists(incFile);

    String cmdName = "Desktop Conf. File";
    Optional<IsoBuildCommands> optVal = cmdSrvc.findByName(cmdName);
    assertTrue(optVal.isPresent(), () -> String.format("Unable to find IsoBuildCommands entity with command name: [%s]", cmdName));

    removeExistingFiles(optVal.get());

    CommandStatusEnum cmdStatus = deskConfigSrvc.execService();
    assertEquals(CommandStatusEnum.DONE, cmdStatus);

    validateFilesForCmd(optVal.get());
  }

  private void removeExistingFiles(IsoBuildCommands isoCmd) throws IOException {
    Path baseDir = Paths.get(System.getProperty("user.dir"));
    for (PostInstallFiles pif : isoCmd.getPostInstFiles()) {
      String outDir = pif.getOutputDir();
      Path destDir = (StringUtils.isNotBlank(outDir)) ? Paths.get(baseDir.toString(), outDir) : baseDir;
      Path incFile = Paths.get(destDir.toString(), pif.getFileName());
      Files.deleteIfExists(incFile);
    }
  }

  private void validateFilesForCmd(IsoBuildCommands isoCmd) {
    Path baseDir = Paths.get(System.getProperty("user.dir"));
    for (PostInstallFiles pif : isoCmd.getPostInstFiles()) {
      String outDir = pif.getOutputDir();
      Path destDir = (StringUtils.isNotBlank(outDir)) ? Paths.get(baseDir.toString(), outDir) : baseDir;
      Path incFile = Paths.get(destDir.toString(), pif.getFileName());

      assertTrue(incFile.toFile().exists(), () -> String.format("File was NOT found: [%s]", incFile.toString()));

      String actualSize = FileUtils.byteCountToDisplaySize(FileUtils.sizeOf(incFile.toFile()));
      assertEquals(pif.getHrExpSize(), actualSize);
    }
  }

  @Test
  public void reqFilesTest () throws IOException {
    setupSrvc.execService();

    String cmdName = "Required Files";
    Optional<IsoBuildCommands> optVal = cmdSrvc.findByName(cmdName);
    assertTrue(optVal.isPresent(), () -> String.format("Unable to find IsoBuildCommands entity with command name: [%s]", cmdName));

    removeExistingFiles(optVal.get());

    CommandStatusEnum cmdStatus = reqSrvc.execService();
    assertEquals(CommandStatusEnum.DONE, cmdStatus);

    validateFilesForCmd(optVal.get());
  }

  @Test
  public void aurRepoFilesTest() throws IOException {
    setupSrvc.execService();

    String cmdName = "AUR Repo. Files";
    Optional<IsoBuildCommands> optVal = cmdSrvc.findByName(cmdName);
    assertTrue(optVal.isPresent(), () -> String.format("Unable to find IsoBuildCommands entity with command name: [%s]", cmdName));

    removeExistingFiles(optVal.get());

    CommandStatusEnum cmdStatus = repoSrvc.execService();
    assertEquals(CommandStatusEnum.DONE, cmdStatus);

    validateFilesForCmd(optVal.get());
  }

  @Test
  public void postInstFilesTest() throws IOException {
    setupSrvc.execService();

    String cmdName = "Post-Inst. Scripts";
    Optional<IsoBuildCommands> optVal = cmdSrvc.findByName(cmdName);
    assertTrue(optVal.isPresent(), () -> String.format("Unable to find IsoBuildCommands entity with command name: [%s]", cmdName));

    Path pathToScripts = Paths.get(System.getProperty("user.dir"), "inc", "post-inst");
    if(pathToScripts.toFile().exists()) {
      removeExistingFiles(pathToScripts);
    }
    CommandStatusEnum cmdStatus = postInstSrvc.execService();
    assertEquals(CommandStatusEnum.DONE, cmdStatus);

    dataValidation();

    validateFilesForCmd(optVal.get());
    validateFiles(pathToScripts, optVal.get());
  }

  private void removeExistingFiles(Path pathToScripts) throws IOException {
    Files.walkFileTree(pathToScripts, fileVisitor);
    List<Path> pathList = fileVisitor.getFileVisitResults();
    pathList.sort(Comparator.comparing(Path::getFileName));
    List<String> excludeFileNames = List.of(AUR_REPO_FILES.stream().collect(Collectors.joining(", ")),
        FILE_NAME);
    excludeFileNames.forEach(fn -> {
      Path file = Paths.get(pathToScripts.toString(), fn);
      pathList.remove(file);
    });

    for (Path path : pathList) {
      Files.deleteIfExists(path);
    }

  }

  private void dataValidation() {
    MenuLevelTwo lvl2 = (MenuLevelTwo) level2Service.findOne(BigInteger.valueOf(2016)).get();
    assertEquals("Set Default Text Editor", lvl2.getMenuLabel());
    Set<CategoryOptions> catOpts = lvl2.getLevel2CatOpts();
    assertEquals(9, catOpts.size());
    CategoryOptions catOpt = (CategoryOptions) catOpts.stream().toArray()[catOpts.size() - 1];
    assertEquals("Zile", catOpt.getAurPackage().getDisplayName());

    String customHeaderName = "VMware";
    Optional<CustomCategoryHeader> optVal = cchSrvc.findByName(customHeaderName);
    assertTrue(optVal.isPresent(),
        () -> String.format("The Custom Category Header was not found for name: [%s]", customHeaderName));
    CustomCategoryHeader cch = optVal.get();
    Set<CustomCategoryDetails> ccdList = cch.getDetails();
    assertEquals(7, ccdList.size());
  }

  private void validateFiles(Path pathToScripts, IsoBuildCommands isoCmd) throws IOException {
    List<String> expectedFileNames = isoCmd.getPostInstFiles().stream()
        .map(pif -> pif.getFileName()).collect(Collectors.toList());
        //Files.readAllLines(expectedScriptFiles, Charset.forName("UTF-8"));
    Files.walkFileTree(pathToScripts, fileVisitor);
    List<Path> pathList = fileVisitor.getFileVisitResults();
    pathList.sort(Comparator.comparing(Path::getFileName));

    List<String> excludeFileNames = List.of(AUR_REPO_FILES.stream().collect(Collectors.joining(", ")),
        FILE_NAME);
    excludeFileNames.forEach(fn -> {
      Path file = Paths.get(pathToScripts.toString(), fn);
      pathList.remove(file);
    });

    excludeFileNames = new ArrayList<>();
    for (Path p : pathList) {
      excludeFileNames.add(p.getFileName().toString());
    }

    List<String> scriptFileNames = pathList.stream().map(p -> p.getFileName().toString())
        .collect(Collectors.toCollection(LinkedHashSet::new)).stream().collect(Collectors.toList());

    expectedFileNames.sort(Comparator.naturalOrder());
    assertLinesMatch(expectedFileNames, scriptFileNames);
  }

  @Test
  public void validateServiceTest() {
    setupSrvc.execService();
    CommandStatusEnum cmdStatus = valSrvc.execService();
    assertEquals(CommandStatusEnum.PASSED, cmdStatus);
  }

  @Test
  public void buildIsoServiceTest() {
    setupSrvc.execService();
    CommandStatusEnum cmdStatus = isoSrvc.execService();
    assertEquals(CommandStatusEnum.DONE, cmdStatus);
  }
}
