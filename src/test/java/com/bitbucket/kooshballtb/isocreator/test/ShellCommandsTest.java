package com.bitbucket.kooshballtb.isocreator.test;

import com.bitbucket.kooshballtb.isocreator.common.CommandStatusEnum;
import com.bitbucket.kooshballtb.isocreator.common.UtilityBean;
import com.bitbucket.kooshballtb.isocreator.config.ApplicationConfig;
import com.bitbucket.kooshballtb.isocreator.config.YAMLConfig;
import com.bitbucket.kooshballtb.isocreator.services.commands.GenBashScriptFileService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.shell.Input;
import org.springframework.shell.InputProvider;
import org.springframework.shell.Shell;

import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({SpringExtension.class})
@ContextConfiguration(classes = {ApplicationConfig.class, YAMLConfig.class}, initializers = ConfigFileApplicationContextInitializer.class)
@ActiveProfiles("test")
@Slf4j
public class ShellCommandsTest {
  @Autowired
  private Shell shell;

  @Autowired
  private YAMLConfig ymlConfig;

  @Autowired
  private UtilityBean utilBean;

  @Autowired
  @Qualifier("buildSetupServiceImpl")
  private GenBashScriptFileService setupSrvc;

  @Autowired
  @Qualifier("buildIsoServiceImpl")
  private GenBashScriptFileService buildIsoSrvc;

  @Autowired
  @Qualifier("validateFilesServiceImpl")
  private GenBashScriptFileService valSrvc;

  @BeforeEach
  void setUserDir() {
    Path path = Paths.get(System.getProperty("user.dir"), "target");
    System.setProperty("user.dir", path.toString());
  }

  @Test
  public void runNextTest() throws IOException {
    StringInputProvider sip = new StringInputProvider(List.of("run-next"));
    shell.run(sip);
  }

  @Test
  public void serviceTest() {
    setupSrvc.execService();
    CommandStatusEnum cmdStatus = buildIsoSrvc.execService();
    assertEquals(CommandStatusEnum.DONE, cmdStatus);
  }

  @Test
  public void buildInstCmdTest() throws IOException {
    StringInputProvider sip = new StringInputProvider(List.of("build-installer"));
    shell.run(sip);
    CommandStatusEnum cmdStatus = valSrvc.execService();
    assertEquals(CommandStatusEnum.PASSED, cmdStatus);
  }
}

class StringInputProvider implements InputProvider {

  private final List<String> words;

  private boolean done;

  public StringInputProvider(List<String> words) {
    this.words = words;
  }

  @Override
  public Input readInput() {
    if (!done) {
      done = true;
      return new Input() {
        @Override
        public List<String> words() {
          return words;
        }

        @Override
        public String rawText() {
          return StringUtils.collectionToDelimitedString(words, " ");
        }
      };
    }
    else {
      return null;
    }
  }
}