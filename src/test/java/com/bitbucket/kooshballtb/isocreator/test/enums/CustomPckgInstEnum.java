package com.bitbucket.kooshballtb.isocreator.test.enums;

public enum CustomPckgInstEnum {
  CPS1("Alacritty Terminal"
      , "— A cross-platform, GPU-accelerated terminal emulator"
      , "software-install/install-alacritty.sh"),
  CPS2("Apache Tomcat v7.0.*-1"
      , "— Open source implementation of the Java Servlet 3.0 and JavaServer Pages 2.2 technologies"
      , "software-install/Tomcat/install-tomcat.sh -tv \\\"v7\\\""),
  CPS3("Apache Tomcat v8.5.*-1"
      , "— Open source implementation of the Java Servlet 3.1 and JavaServer Pages 2.3 technologies"
      , "software-install/Tomcat/install-tomcat.sh -tv \\\"v8\\\""),
  CPS4("Apache Tomcat v9.0.*-1"
      , "— Open source implementation of the Java Servlet 4.0 and JavaServer Pages 2.3 technologies"
      , "software-install/Tomcat/install-tomcat.sh -tv \\\"v9\\\""),
  CPS5("Auto Background Changer"
      , "— A simple and automatic wallpaper changer supporting various backends for Linux"
      , "software-install/auto-background-changer.sh"),
  CPS6("Nexus OSS"
      , "— Nexus 3 Repository OSS"
      , "software-install/Nexus-OSS/install-nexus-oss.sh -part \\\"/nexus-oss\\\""),
  CPS7("PostgreSQL DBMS"
      , "— Sophisticated object-relational DBMS"
      , "software-install/install-postgresql.sh -part \\\"/dbhome\\\"");
  private String pckgName;
  private String desc;
  private String scriptCmd;

  CustomPckgInstEnum(String key, String desc, String val) {
    this.pckgName = key;
    this.desc = desc;
    this.scriptCmd = val;
  }

  public String getPckgName() {
    return pckgName;
  }

  public String getDesc() {
    return desc;
  }

  public String getScriptCmd() {
    return scriptCmd;
  }
}
