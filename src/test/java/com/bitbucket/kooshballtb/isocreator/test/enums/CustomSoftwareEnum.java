package com.bitbucket.kooshballtb.isocreator.test.enums;

@SuppressWarnings({"UnusedDeclaration"})
public enum CustomSoftwareEnum {
    IntelliJCE("Community Edition"
            , "— JetBrains IDE (community version) for multiple programming languages with advanced refactoring features"
            , "intellij-idea-ce"),
    IntelliJUE("Ultimate Edition"
            , "— Licensed version of JetBrains IDE for multiple programming languages with advanced refactoring features intensely focused on developer productivity"
            , "intellij-idea-ultimate-edition"),
    STS("Spring Tool Suite(STS)"
            , "— The Spring Tool Suite (STS) from SpringSource."
            , "spring-tool-suite"),
    JaspersoftStudio("TIBCO Jaspersoft"
            , "— Eclipse based Jasper Reports generator"
            , "jaspersoftstudio"),
    VScode("Visual Studio Code (vscode)"
            , "— Editor for building and debugging modern web and cloud applications (official binary version)"
            , "visual-studio-code-bin"),
    AdobeTTF("Adobe TTF"
            , "— Fonts from Adobe Acrobat Reader X including Myriad Pro, Myriad CAD, Minion Pro, Courier Std and Adobe Pi Std."
            , "ttf-adobe-fonts"),
    AppleTTF("Apple TTF"
            , "— Apple OS X TrueType Fonts"
            , "ttf-mac-fonts"),
    BitstreamVera("Bitstream Vera"
            , "— Includes Serif, Sans-Serif, & Monospace fonts."
            , "ttf-bitstream-vera"),
    DejaVu("DejaVu"
            , "— Modifications of the Bitstream Vera fonts designed for greater coverage of Unicode"
            , "ttf-dejavu"),
    FiraCode("FiraCode"
            , "— Monospaced font with programming ligatures"
            , "ttf-fira-code"),
    FontAwesome("Font Awesome"
            , "— Iconic font designed for Bootstrap"
            , "ttf-font-awesome"),
    GoogleNoto("Google Noto"
            , "— Google Noto TTF fonts including support of Chinese, Japanese, & Korean characters"
            , "noto-fonts noto-fonts-cjk noto-fonts-emoji noto-fonts-extra"),
    GoogleTTF("Google TTF"
            , "— TrueType Fonts from the Google Fonts project"
            , "ttf-google-fonts-git"),
    Liberation("Liberation"
            , "— Free metric-compatible substitute for some of the fonts found in Microsoft Office & Windows"
            , "ttf-liberation"),
    MsTTF("Microsoft TTF"
            , "— Windows 10 & Core TrueType Fonts from Microsoft"
            , "ttf-ms-win10 ttf-ms-fonts"),
    WQYMH("WenQuanYi Micro Hei"
            , "— A Sans-Serif style high quality CJK outline font to support Chinese, Japanese, & Korean characters"
            , "wqy-microhei"),
    BytecodeViewer("Bytecode Viewer"
            , "— Java reverse engineering suite, including a decompiler, editor and debugger."
            , "bytecode-viewer"),
    CFR("CFR"
            , "— Java decompiler, supporting modern features of Java 9, 10 and beyond."
            , "cfr"),
    Fernflower("Fernflower"
            , "— Analytical decompiler for Java, developed as part of IntelliJ IDEA."
            , "fernflower-git"),
    JAD("JAD"
            , "— Unmaintained Java decompiler."
            , "jad"),
    JD_Core("JD-Core-java"
            , "— Thin-wrapper for the Java Decompiler."
            , "jd-core-java"),
    Krakatau("Krakatau"
            , "— Java decompiler, assembler, and disassembler."
            , "krakatau-git"),
    ProcyonDecompiler("Procyon Decompiler"
            , "— Experimental Java decompiler, inspired by ILSpy and Mono.Cecil."
            , "procyon-decompiler luyten"),
    GitLab("GitLab"
            , "— Project management and code hosting application that also supports code reviews."
            , "gitlab"),
    Leanote("Leanote"
            , "— Not Just A Notepad - Knowledge, Blog, Sharing, Cooperation."
            , "leanote"),
    Turtl("Turtl"
            , "— Take notes, bookmark websites, and store documents for sensitive projects."
            , "turtl"),
    NixNote("NixNote 2"
            , "— Evernote clone (formerly Nevernote)"
            , "nixnote2-git"),
    LibreOffice("LibreOffice"
            , "— The office productivity suite compatible to the open and standardized ODF document format. Fork of OpenOffice, supported by The Document Foundation."
            , "libreoffice-fresh"),
    AdobeReader("Adobe Reader"
            , "— Adobe Acrobat Reader is a PDF file viewer"
            , "acroread"),
    Atril("Atril"
            , "— Simple multi-page document viewer for MATE. Supports DjVu, DVI, EPS, EPUB, PDF, PostScript, TIFF, XPS and Comicbook."
            , "atril"),
    DjView("DjView"
            , "— Viewer for DjVu documents."
            , "djview"),
    ePDFView("ePDFView"
            , "— Lightweight PDF document viewer using the Poppler and GTK+ libraries. Development stopped."
            , "epdfview"),
    FoxitReader("Foxit Reader"
            , "— Small, fast (compared to Acrobat) proprietary PDF viewer."
            , "foxitreader"),
    qpdfview("qPDFview"
            , "— A tabbed PDF viewer using the poppler library."
            , "qpdfview"),
    Xreader("Xreader"
            , "— Document viewer for files like PDF and Postscript. X-Apps Project."
            , "xreader"),
    Zathura("Zathura"
            , "— Minimalistic document viewer"
            , "zathura"),
    UngoogledChromium("Ungoogled Chromium"
            , "— Modifications to Google Chromium for removing Google integration and enhancing privacy, control, and transparency (binary version)"
            , "ungoogled-chromium-bin"),
    FirefoxDev("Firefox Developer Edition"
            , "— Developer Edition of the popular Firefox web browser"
            , "firefox-developer-edition"),
    bzip2("bzip2"
            , "— A high-quality data compression program"
            , "bzip2"),
    LongRangeZIP("Long Range ZIP(lrzip)"
            , "— Multi-threaded compression with rzip/lzma, lzo, and zpaq"
            , "lrzip"),
    cpio("cpio"
            , "— A tool to copy files into or out of a cpio or tar archive"
            , "cpio"),
    p7zip("7z"
            , "— 7zip file archiver with graphical user interface."
            , "p7zip"),
    ZIP("ZIP & Unzip"
            , "— Compressor/Archiver & Extractor for .zip files."
            , "zip unzip"),
    Unrar("Unrar"
            , "— The RAR uncompression program"
            , "unrar"),
    UUDeview("UUDeview"
            , "— Helps transmitting & receiving binary files using mail or newsgroups. Includes library files."
            , "uudeview"),
    ColorDiff("ColorDiff"
            , "— A Perl script wrapper for 'diff' that produces the same output but with pretty 'syntax' highlighting"
            , "colordiff"),
    KDiff3("KDiff3-Qt"
            , "— Lightweight Qt file comparator/merge tool, but WITHOUT the KDE dependencies."
            , "kdiff3-qt"),
    OwnCloud("OwnCloud"
            , "— A cloud server to store your files centrally on a hardware controlled by you"
            , "owncloud owncloud-client"),
    DejaDup("Déjà Dup"
            , "— Simple backup tool, that hides the complexity of backing up the Right Way and uses duplicity as the backend"
            , "deja-dup"),
    Bash("Bash"
            , "— (Bourne-again Shell) is a command-line shell/programming language by the GNU Project."
            , "bc rsync mlocate bash-completion pkgstats"),
    Dash("Dash"
            , "— (Debian Almquist shell) is a modern POSIX-compliant implementation of /bin/sh (sh, Bourne shell)."
            , "dash"),
    KornShell("MirBSD™ Korn Shell"
            , "— standard/restricted command and programming language developed by AT&T."
            , "mksh"),
    Zsh("Zsh"
            , "— powerful shell that operates as both an interactive shell and as a scripting language interpreter"
            , "zsh zsh-completions antigen-git oh-my-zsh-git zsh-zim-git"),
    OpenSwan("Openswan"
            , "— Open Source implementation of IPsec for the Linux operating system"
            , "xl2tpd openswan"),
    Codec1("codecs64"
        , "— Non-linux native codec pack."
        , "codecs64"),
    Codec2("v4l-utils"
        , "— Userspace tools and conversion library for Video 4 Linux"
        , "v4l-utils"),
    Codec3("alsa-utils"
        , "— An alternative implementation of Linux sound support"
        , "alsa-utils"),
    Codec4("alsa-plugins"
        , "— Extra alsa plugins"
        , "alsa-plugins"),
    Codec5("alsa-firmware"
        , "— ALSA firmware package"
        , "alsa-firmware"),
    Codec6("gstreamer"
        , "— GStreamer open-source multimedia framework core library"
        , "gstreamer"),
    Codec7("gst-plugin-libde265"
        , "— Libde265 plugin (an open h.265 video codec implementation) for gstreamer"
        , "gst-plugin-libde265"),
    Codec8("pulseaudio"
        , "— A featureful, general-purpose sound server"
        , "pulseaudio"),
    Codec9("pulseaudio-alsa"
        , "— ALSA Configuration for PulseAudio"
        , "pulseaudio-alsa"),
    Codec10("pavucontrol"
        , "— PulseAudio Volume Control"
        , "pavucontrol"),
    Codec11("ffmpegthumbnailer"
        , "— Lightweight video thumbnailer that can be used by file managers."
        , "ffmpegthumbnailer"),
    Codec12("phonon-qt4-gstreamer"
        , "— Phonon GStreamer backend for Qt4"
        , "phonon-qt4-gstreamer"),
    Codec13("phonon-qt5-gstreamer"
        , "— Phonon GStreamer backend for Qt5"
        , "phonon-qt5-gstreamer"),
    Codec14("kdegraphics-thumbnailers"
        , "— Thumbnailers for various graphics file formats"
        , "kdegraphics-thumbnailers"),
    Codec15("ffmpegthumbs"
        , "— FFmpeg-based thumbnail creator for video files"
        , "ffmpegthumbs"),
    CUPS1("cups"
            , "— The CUPS Printing System - daemon package"
            , "cups"),
    CUPS2("cups-pdf"
            , "— PDF printer for cups"
            , "cups-pdf"),
    CUPS3("gutenprint"
            , "— Top quality printer drivers for POSIX systems"
            , "gutenprint"),
    CUPS4("ghostscript"
            , "— An interpreter for the PostScript language"
            , "ghostscript"),
    CUPS5("gsfonts"
            , "— (URW)++ Core Font Set [Level 2]"
            , "gsfonts"),
    CUPS6("gsfonts-type1"
            , "— lmello"
            , "gsfonts-type1"),
    CUPS7("foomatic-db"
            , "— Foomatic - The collected knowledge about printers, drivers, and driver options in XML files, used by foomatic-db-engine to generate PPD files."
            , "foomatic-db"),
    CUPS8("foomatic-db-engine"
            , "— Foomatic - Foomatic's database engine generates PPD files from the data in Foomatic's XML database. It also contains scripts to directly generate print queues and handle jobs."
            , "foomatic-db-engine"),
    CUPS9("foomatic-db-nonfree"
            , "— Foomatic - database extension consisting of manufacturer-supplied PPD files released under non-free licenses"
            , "foomatic-db-nonfree"),
    CUPS10("foomatic-db-ppds"
            , "— Foomatic - PPDs from printer manufacturers"
            , "foomatic-db-ppds"),
    CUPS11("foomatic-db-nonfree-ppds"
            , "— Foomatic - non-free PPDs from printer manufacturers"
            , "foomatic-db-nonfree-ppds"),
    CUPS12("foomatic-db-gutenprint-ppds"
            , "— simplified prebuilt ppd files"
            , "foomatic-db-gutenprint-ppds"),
    DB1("Adminer"
        , "— A full-featured MySQL management tool written in PHP."
        , "adminer"),
    DB2("Apache Cassandra"
        , "— Apache Cassandra NoSQL database"
        , "cassandra"),
    DB3("Apache CouchDB"
        , "— A document-oriented database that can be queried and indexed in a MapReduce fashion using JSON"
        , "couchdb"),
    DB4("Apache HBase"
        , "— HBase - the Hadoop database"
        , "hbase"),
    DB5("Apache Lucene"
        , "— Apache Lucene is a high-performance, full-featured text search engine library written entirely in Java."
        , "apache-lucene"),
    DB6("Apache Solr"
        , "— Popular, blazing fast open source enterprise search platform from the Apache Lucene project"
        , "solr"),
    DB7("DBeaver"
        , "— Free universal SQL Client for developers and database administrators (community edition)"
        , "dbeaver"),
    DB8("Elasticsearch"
        , "— Distributed RESTful search engine built on top of Lucene"
        , "elasticsearch"),
    DB9("MariaDB"
        , "— Fast SQL database server, derived from MySQL"
        , "mariadb"),
    DB10("MiraDB"
        , "— cross platform lightweight open source database (oodb) management"
        , "miradb"),
    DB11("MongoDB"
        , "— A high-performance, open source, schema-free document-oriented database"
        , "mongodb-bin"),
    DB12("MongoDB GUI"
        , "— The MongoDB GUI"
        , "mongodb-compass"),
    DB13("MongoDB Tools"
        , "— The MongoDB tools provide import, export, and diagnostic capabilities."
        , "mongodb-tools-bin"),
    DB14("MySQL"
        , "— Fast SQL database server, community edition"
        , "mysql"),
    DB15("MySQL Workbench"
        , "— A cross-platform, visual database design tool developed by MySQL"
        , "mysql-workbench"),
    DB16("Neo4j"
        , "— A fully transactional graph database implemented in Java"
        , "neo4j-community"),
    DB17("Orbada"
        , "— Database SQL, query tool, using JDBC for Oracle, SQLite, Firebird, etc"
        , "orbada"),
    DB18("OrientDB"
        , "— The Graph-Document NoSQL - Community Edition"
        , "orientdb-community"),
    DB19("pgAdmin"
        , "— Comprehensive design and management interface for PostgreSQL"
        , "pgadmin4"),
    DB20("pgModeler"
        , "— PostgreSQL Database Modeler"
        , "pgmodeler"),
    DB21("PostgreSQL"
        , "— Sophisticated object-relational DBMS"
        , "postgresql"),
    DB22("Redis"
        , "—structure store, used as a database, cache and message broker."
        , "redis"),
    DB23("RethinkDB"
        , "— Distributed powerful and scalable NoSQL database"
        , "rethinkdb"),
    DB24("Sequeler"
        , "— SQL Client built in Vala and GTK3"
        , "sequeler-git"),
    DB25("SQLite"
        , "— A C library that implements an SQL database engine"
        , "sqlite"),
    DB26("SQLite Browser"
        , "— SQLite Database browser is a light GUI editor for SQLite databases, built on top of Qt"
        , "sqlitebrowser"),
    DB27("SQL Server"
        , "— Microsoft SQL Server for Linux"
        , "mssql-server"),
    DB28("SQL Server® Tools"
        , "— Microsoft® SQL Server® Tools for Linux"
        , "mssql-tools"),
    DB29("TOra"
        , "— Toolkit for databases with support for MySQL and PostgreSQL"
        , "tora"),
    Xorg1("xf86-input-evdev"
        , "— X.org evdev input driver"
        , "xf86-input-evdev"),
    Xorg2("xf86-input-libinput"
        , "— Generic input driver for the X.Org server based on libinput"
        , "xf86-input-libinput"),
    Xorg3("xorg-apps"
        , "— includes some packages that are necessary for certain configuration tasks"
        , "xorg-apps"),
    Xorg4("xorg-xinit"
        , "— X.Org initialisation program"
        , "xorg-xinit"),
    Xorg5("xorg-twm"
        , "— Tab Window Manager for the X Window System"
        , "xorg-twm"),
    Xorg6("xorg-xclock"
        , "— X clock"
        , "xorg-xclock"),
    Xorg7("mesa"
        , "— An open-source implementation of the OpenGL specification"
        , "mesa"),
    Xorg8("xterm"
        , "— X Terminal Emulator"
        , "xterm"),
    Xorg9("xorg"
            , "— includes packages from xorg-server, and packages from the xorg-apps group and fonts."
            , "xorg"),
    Xorg10("ttf-google-fonts-git"
        , "— TrueType fonts from the Google Fonts project (git version)"
        , "ttf-google-fonts-git"),
    CustSoft1("0nyX"
        , "— Everyday usable dark theme mainly made for Xfce, IceWM or Openbox."
        , "onyx-suite"),
    CustSoft2("Adapta"
        , "— An adaptive Gtk+ theme based on Material Design Guidelines"
        , "adapta-gtk-theme"),
    CustSoft3("Amp"
        , "— A modal text editor for the terminal."
        , "amp"),
    CustSoft4("ARandR"
        , "— Provide a simple visual front end for XRandR 1.2."
        , "arandr"),
    CustSoft5("Arc Faenza"
        , "— Combination of Arc icon theme and Faenza dark icon theme. Also includes some icons from other sets for icon sizes 22 and 24."
        , "arc-faenza-icon-theme"),
    CustSoft6("Arc GTK"
        , "— A flat theme with transparent elements for GTK 3, GTK 2 and Gnome-Shell"
        , "arc-gtk-theme"),
    CustSoft7("Arc Icon"
        , "— Arc icon theme. Official releases only."
        , "arc-icon-theme"),
    CustSoft8("Arc OSX"
        , "— OSX-Arc-Shadow Theme for GTK 3.x"
        , "osx-arc-shadow"),
    CustSoft9("Argon"
        , "— the noble lightweight GUI package manager + update notifier (with full AUR support)"
        , "argon"),
    CustSoft10("Aura"
        , "— A package manager for Arch Linux and its AUR"
        , "aura-git"),
    CustSoft11("Breeze"
        , "— Breeze widget theme for GTK 2 and 3"
        , "breeze-gtk"),
    CustSoft12("Calendar"
        , "— Reminder utility (OpenBSD)"
        , "calendar"),
    CustSoft13("California"
        , "— A new calendar application for GNOME 3. It is currently under heavy development."
        , "california"),
    CustSoft14("Cockpit"
        , "— A systemd web based user interface for Linux servers"
        , "cockpit"),
    CustSoft15("C shell(tcsh)"
        , "— Enhanced version of the Berkeley C shell."
        , "tcsh"),
    CustSoft16("Cumulus"
        , "— Cumulus is a free and Open Source flight computer for Linux"
        , "cumulus"),
    CustSoft17("Cylon"
        , "— Updates, Maintenance, backup and system checks in a menu driven TUI written in Bash"
        , "cylon"),
    CustSoft18("Darktable"
        , "— Utility to organize and develop raw images"
        , "darktable"),
    CustSoft19("Devilspie"
        , "— Window matching utility for X"
        , "devilspie"),
    CustSoft20("DMenu2"
        , "— Fork of dmenu with many useful patches applied and additional options like screen select, dim or opacity change"
        , "dmenu2"),
    CustSoft21("DockbarX"
        , "— TaskBar with groupping and group manipulation"
        , "dockbarx"),
    CustSoft22("Dropbear"
        , "— Lightweight SSH server"
        , "dropbear"),
    CustSoft23("DTE"
        , "— A small and easy to use console text editor"
        , "dte"),
    CustSoft24("Eclipse Modeling Tools"
        , "— The eclipse IDE with the eclipse modeling tools as a standalone package (not directly interfering with a standard eclipse)"
        , "eclipse-modeling-tools"),
    CustSoft25("Emacs"
        , "— The extensible, customizable, self-documenting real-time display editor"
        , "emacs"),
    CustSoft26("Faience GTK"
        , "— GTK3, GTK2, Metacity and Gnome-Shell themes based on Faenza"
        , "faience-themes"),
    CustSoft27("Faience Icon"
        , "— Continued development of Faenza\\Faience Icon Theme"
        , "faience-ng-icon-theme"),
    CustSoft28("Fbpanel"
        , "— NetWM compliant desktop panel"
        , "fbpanel"),
    CustSoft29("Feh"
        , "— Fast and light imlib2-based image viewer"
        , "feh"),
    CustSoft30("Firewall Builder"
        , "— Object-oriented GUI and set of compilers for various firewall platforms"
        , "fwbuilder"),
    CustSoft31("FirewallD"
        , "— Firewall daemon with D-Bus interface"
        , "firewalld"),
    CustSoft32("Fish"
        , "— Smart and user friendly shell intended mostly for interactive use"
        , "fish"),
    CustSoft33("Flameshot"
        , "— Powerful yet simple to use screenshot software"
        , "flameshot"),
    CustSoft34("Flareget"
        , "— A full featured, advanced, multi-threaded, multisegment download manager and accelerator."
        , "flareget"),
    CustSoft35("Gimp"
        , "— GNU Image Manipulation Program"
        , "gimp"),
    CustSoft36("GIS Weather"
        , "— Customizable weather widget"
        , "gis-weather-git"),
    CustSoft37("Glassfish"
        , "— The Open Source Jakarta EE 8 (Java EE 8) Reference Implementation."
        , "glassfish5"),
    CustSoft38("GNOME-Breeze"
        , "— A GTK theme created to match with the new Plasma 5 Breeze (git version)."
        , "gnome-breeze-git"),
    CustSoft39("GNOME-Colors"
        , "— GNOME-Colors icon theme"
        , "gnome-colors-icon-theme"),
    CustSoft40("GNOME-Weather"
        , "— Access current weather conditions and forecasts"
        , "gnome-weather"),
    CustSoft41("GraphicsMagick"
        , "— Image processing system"
        , "graphicsmagick"),
    CustSoft42("Greybird"
        , "— A grey and blue Xfce theme."
        , "xfce-theme-greybird"),
    CustSoft43("GScreenShot"
        , "— A simple screenshot tool supporting multiple backends"
        , "gscreenshot"),
    CustSoft44("GSimpleCal"
        , "— Simple and lightweight GTK calendar"
        , "gsimplecal"),
    CustSoft45("GtkLP"
        , "— a graphical frontend for cups, the linux printing system"
        , "gtklp"),
    CustSoft46("Gufw"
        , "— Uncomplicated way to manage your Linux firewall"
        , "gufw"),
    CustSoft47("GVim"
        , "— Vi Improved, a highly configurable, improved version of the vi text editor (with advanced features, such as a GUI)"
        , "gvim"),
    CustSoft48("HotShots"
        , "— Screenshot tool with some editing features."
        , "hotshots-git"),
    CustSoft49("ImageMagick"
        , "— An image viewing/manipulation program (Q32 HDRI with all libs and features)"
        , "imagemagick-full"),
    CustSoft50("IntelliJ IDEA CE"
        , "— Intellij Idea IDE (community version) with Intellij JDK"
        , "intellij-idea-ce"),
    CustSoft51("IntelliJ IDEA UE"
        , "— An intelligent IDE for Java, Groovy and other programming languages with advanced refactoring features intensely focused on developer productivity."
        , "intellij-idea-ultimate-edition"),
    CustSoft52("Jetty"
        , "— Jetty is a pure Java-based HTTP server and Java Servlet container"
        , "jetty"),
    CustSoft53("KDE Plasma Weather Widget"
        , "— Plasma applet for displaying weather information from yr.no server"
        , "plasma5-applets-weather-widget"),
    CustSoft54("Kimai"
        , "— Kimai is a free open source timetracker"
        , "kimai-git"),
    CustSoft55("KMenuEdit"
        , "— KDE menu editor"
        , "kmenuedit"),
    CustSoft56("KShare"
        , "— The free and open source and cross platform screen sharing software."
        , "kshare"),
    CustSoft57("Lightscreen"
        , "— Simple tool to automate the tedious process of saving and cataloging screenshots"
        , "lightscreen"),
    CustSoft58("LilyTerm"
        , "— A light and easy to use libvte based X terminal emulator"
        , "lilyterm-git"),
    CustSoft59("LogWatch"
        , "— Logwatch is a customizable log analysis system."
        , "logwatch"),
    CustSoft60("Meteo-Qt"
        , "— System tray application for weather status information"
        , "meteo-qt"),
    CustSoft61("Micro"
        , "— A modern and intuitive terminal-based text editor"
        , "micro"),
    CustSoft62("MineTime"
        , "— MineTime is a modern, intuitive and smart calendar application."
        , "minetime"),
    CustSoft63("Moka"
        , "— An open source FreeDesktop icon project by Sam Hewitt"
        , "moka-icon-theme"),
    CustSoft64("MPV"
        , "— a free, open source, and cross-platform media player"
        , "mpv"),
    CustSoft65("MultiTail"
        , "— Lets you view one or multiple files like the original tail program"
        , "multitail"),
    CustSoft66("My Weather Indicator"
        , "— A simple indicator for the weather"
        , "my-weather-indicator-git"),
    CustSoft67("Nano"
        , "— Pico editor clone with enhancements"
        , "nano"),
    CustSoft68("Neovim"
        , "— Fork of Vim aiming to improve user experience, plugins, and GUIs"
        , "neovim neovim-drop-in"),
    CustSoft69("Nextcloud"
        , "— A cloud server to store your files centrally on a hardware controlled by you"
        , "nextcloud nextcloud-client"),
    CustSoft70("Numix Circle & Square Icons"
        , "— Numix project Icon Themes - Updated with Numix Core"
        , "numix-icon-theme-pack-git"),
    CustSoft71("Numix Darkblue"
        , "— A flat and light theme with a modern look (GNOME, Openbox, Unity, Xfce) (Dark Blue Variant)"
        , "numix-themes-darkblue"),
    CustSoft72("Numix GTK"
        , "— A flat and light theme with a modern look (GNOME, Openbox, Unity, Xfce)"
        , "numix-gtk-theme"),
    CustSoft73("Numix Solarized"
        , "— Solarized versions of Numix GTK2 and GTK3 theme, compatible with GTK 3.20"
        , "gtk-theme-numix-solarized"),
    CustSoft74("Obsidian"
        , "— Obsidian Icon Theme"
        , "obsidian-icon-theme"),
    CustSoft75("Octopi"
        , "— A powerful Pacman frontend using Qt libs"
        , "octopi"),
    CustSoft76("Odrive"
        , "— Sync agent and cli for odrive.com"
        , "odrive"),
    CustSoft77("Oh"
        , "— A surprisingly powerful Unix shell."
        , "oh-git"),
    CustSoft78("OpenSSH"
        , "— Premier connectivity tool for remote login with the SSH protocol"
        , "openssh"),
    CustSoft79("Pacaur"
        , "— An AUR helper that minimizes user interaction"
        , "pacaur"),
    CustSoft80("Packer"
        , "— Bash wrapper for pacman and aur (discontinued)"
        , "packer-aur-git"),
    CustSoft81("Pakku"
        , "— Pacman wrapper with AUR support"
        , "pakku"),
    CustSoft82("Pakku GUI"
        , "— GTK frontend for pakku"
        , "pakku-gui"),
    CustSoft83("Pamac"
        , "— A Gtk3 frontend for libalpm"
        , "pamac-aur"),
    CustSoft84("PeerGuardian GUI"
        , "— A privacy oriented firewall application (Daemon, CLI & GUI)."
        , "pgl-git"),
    CustSoft85("Pencil"
        , "— Sketching and GUI prototyping/wireframing tool"
        , "pencil"),
    CustSoft86("Pikaur"
        , "— AUR helper which asks all questions before installing/building. Inspired by pacaur, yaourt and yay."
        , "pikaur"),
    CustSoft87("PkgBrowser"
        , "— A utility for browsing pacman databases and the AUR"
        , "pkgbrowser"),
    CustSoft88("Poseidon"
        , "— A fast, minimal and lightweight browser."
        , "poseidon-browser-git"),
    CustSoft89("PowerShell"
        , "— A cross-platform automation and configuration tool/framework (binary package)"
        , "powershell-bin"),
    CustSoft90("Print Manager"
        , "— A tool for managing print jobs and printers"
        , "print-manager"),
    CustSoft91("Profile Sync Daemon"
        , "— Syncs browser profiles to tmpfs reducing SSD/HDD calls and speeding-up browsers."
        , "profile-sync-daemon"),
    CustSoft92("Project Hamster"
        , "— [In heavy development] A GTK interface to the hamster time tracker."
        , "python-hamster-gtk-git"),
    CustSoft93("QDirStat"
        , "— Qt-based directory statistics (KDirStat/K4DirStat without any KDE - from the original KDirStat author)"
        , "qdirstat"),
    CustSoft94("QFtp"
        , "— A user interface for FTP file transfer"
        , "qftp"),
    CustSoft95("QSpectrumAnalyzer"
        , "— Spectrum analyzer for multiple SDR platforms."
        , "qspectrumanalyzer"),
    CustSoft96("Rofi"
        , "— A window switcher, application launcher and dmenu replacement"
        , "rofi"),
    CustSoft97("Sakura"
        , "— A terminal emulator based on GTK and VTE"
        , "sakura"),
    CustSoft98("samba"
        , "— SMB Fileserver and AD Domain server"
        , "samba"),
    CustSoft99("Shutter"
        , "— A featureful screenshot tool (formerly gscrot)"
        , "perl-goo-canvas shutter"),
    CustSoft100("SlimJet"
        , "— Fast, smart and powerful browser based on Blink"
        , "slimjet"),
    CustSoft101("smbnetfs"
        , "— small C program that mounts Microsoft network neighborhood in single directory."
        , "smbnetfs"),
    CustSoft102("SpeedTest-CLI"
        , "— Command line interface for testing internet bandwidth using speedtest.net"
        , "speedtest-cli"),
    CustSoft103("Sublime Text"
        , "— Sophisticated text editor for code, html and prose - dev build"
        , "sublime-text-dev"),
    CustSoft104("Swatch"
        , "— The active log file monitoring tool"
        , "swatchdog"),
    CustSoft105("System Config Printer"
        , "— A CUPS printer configuration tool and status applet"
        , "system-config-printer"),
    CustSoft106("TigerVNC"
        , "— Suite of VNC servers and clients. Based on the VNC 4 branch of TightVNC."
        , "tigervnc"),
    CustSoft107("TimeCamp"
        , "— Client application for TimeCamp software"
        , "timecamp"),
    CustSoft108("TimeSlotTracker"
        , "— Simple and useful time tracker"
        , "timeslottracker"),
    CustSoft109("TinySSH"
        , "— Small SSH server using NaCl / TweetNaCl"
        , "tinyssh"),
    CustSoft110("Toggl"
        , "— Time Tracking Software."
        , "toggldesktop-bin"),
    CustSoft111("Tomcat Native"
        , "— Optional component for Tomcat to use certain native resources for performance, compatibility"
        , "tomcat-native"),
    CustSoft112("Tomcat v7.0.*-1"
        , "— Open source implementation of the Java Servlet 3.0 and JavaServer Pages 2.2 technologies"
        , "tomcat7"),
    CustSoft113("Tomcat v8.5.*-1"
        , "— Open source implementation of the Java Servlet 3.1 and JavaServer Pages 2.3 technologies"
        , "tomcat8"),
    CustSoft114("Tomcat v9.0.*-1"
        , "— Open source implementation of the Java Servlet 4.0 and JavaServer Pages 2.3 technologies"
        , "tomcat9"),
    CustSoft115("Trizen"
        , "— Trizen AUR Package Manager"
        , "trizen"),
    CustSoft116("Unagi"
        , "— Compositing manager for implementing effects with regular window managers."
        , "unagi"),
    CustSoft117("Vibrancy Colors"
        , "— Modern, clean and customizable flat icon theme"
        , "vibrancy-colors"),
    CustSoft118("Vile"
        , "— vi like emacs"
        , "vile"),
    CustSoft119("Vim"
        , "— Vi Improved, a highly configurable, improved version of the vi text editor"
        , "vim"),
    CustSoft120("Vivacious Colors"
        , "— Vivacious Colors GTK Theme by the RAVEfinity Open Design Team. Features a clean, vibrant and customizable look in 4 Styles. Light, Dark, Blackout, Fusion (Hybrid). Every style comes in 13 vivid colors. In Regular & Pro Versions."
        , "vivacious-colors-gtk-theme"),
    CustSoft121("Volume Wheel"
        , "— Tray icon to change volume with the mouse"
        , "volwheel"),
    CustSoft122("WildFly"
        , "— Wildfly Application Server"
        , "wildfly"),
    CustSoft123("X2Go Client"
        , "— a graphical client (Qt4) for the X2Go system"
        , "x2goclient"),
    CustSoft124("Xfce4 Terminal"
        , "— A modern terminal emulator primarily for the Xfce desktop environment"
        , "xfce4-terminal"),
    CustSoft125("Xlockmore"
        , "— screen saver / locker for the X Window System"
        , "xlockmore"),
    CustSoft126("XScreenSaver (Arch Logo)"
        , "— Screen saver and locker for the X Window System with Arch Linux branding"
        , "xscreensaver-arch-logo"),
    CustSoft127("Yay"
        , "— Yet another yogurt. Pacman wrapper and AUR helper written in go."
        , "yay"),
    CustSoft128("Zile"
        , "— A small, fast, and powerful Emacs clone"
        , "zile"),
    CustSoft129("zramswap"
        , "— Sets up zram-based swap devices on boot"
        , "zramswap"),
    CustSoft130("Zuki"
        , "— Zuki themes for GNOME, Xfce and more."
        , "zuki-themes"),
    CustSoft131("Zukitwo"
        , "— A theme for GTK3, GTK2, Metacity, xfwm4, Gnome Shell and Unity - git version"
        , "zukitwo-themes-git"),
    VCD1("acpid"
        , "— A daemon for delivering ACPI power management events with netlink support"
        , "acpid"),
    VCD2("bumblebee"
        , "— NVIDIA Optimus support for Linux through VirtualGL"
        , "bumblebee"),
    VCD3("catalyst-libgl"
        , "— AMD/ATI drivers. Catalyst drivers libraries symlinks + experimental powerXpress support."
        , "catalyst-libgl"),
    VCD4("catalyst-test"
        , "— AMD/ATI Catalyst drivers for linux AKA Crimson. catalyst-dkms + catalyst-utils + lib32-catalyst-utils + experimental powerXpress suppport. PRE-GCN Radeons are optionally supported"
        , "catalyst-test"),
    VCD5("catalyst-utils"
        , "— AMD/ATI drivers. Utilities and libraries. Radeons HD 2 3 4 xxx ARE NOT SUPPORTED"
        , "catalyst-utils"),
    VCD6("gtkmm3"
        , "— C++ bindings for GTK+ 3"
        , "gtkmm3"),
    VCD7("lib32-catalyst-libgl"
        , "— AMD/ATI drivers. Catalyst drivers libraries symlinks (32-bit)"
        , "lib32-catalyst-libgl"),
    VCD8("lib32-mesa"
        , "— An open-source implementation of the OpenGL specification (32-bit)"
        , "lib32-mesa"),
    VCD9("lib32-nvidia-utils"
        , "— NVIDIA drivers utilities (32-bit)"
        , "lib32-nvidia-utils"),
    VCD10("lib32-virtualgl"
        , "— 32-bit serverside components for 64-bit VirtualGL servers"
        , "lib32-virtualgl"),
    VCD11("lib32-vulkan-icd-loader"
        , "— Vulkan Installable Client Driver (ICD) Loader (32-bit)"
        , "lib32-vulkan-icd-loader"),
    VCD12("nvidia"
        , "— NVIDIA drivers for linux"
        , "nvidia"),
    VCD13("nvidia-dkms"
        , "— NVIDIA driver sources for linux"
        , "nvidia-dkms"),
    VCD14("nvidia-lts"
        , "— NVIDIA drivers for linux-lts"
        , "nvidia-lts"),
    VCD15("nvidia-settings"
        , "— Tool for configuring the NVIDIA graphics driver"
        , "nvidia-settings"),
    VCD16("nvidia-utils"
        , "— NVIDIA drivers utilities"
        , "nvidia-utils"),
    VCD17("nvidia-zen"
        , "— NVIDIA drivers for linux-zen"
        , "nvidia-zen"),
    VCD18("open-vm-tools"
        , "— The Open Virtual Machine Tools (open-vm-tools) are the open source implementation of VMware Tools"
        , "open-vm-tools"),
    VCD19("open-vm-tools-dkms"
        , "— Open Virtual Machine Tools kernel modules (DKMS)"
        , "open-vm-tools-dkms"),
    VCD20("qt4"
        , "— A cross-platform application and UI framework"
        , "qt4"),
    VCD21("virtualbox-guest-dkms"
        , "— VirtualBox Guest kernel modules sources"
        , "virtualbox-guest-dkms"),
    VCD22("virtualbox-guest-modules-arch"
        , "— Virtualbox guest kernel modules for Arch Kernel"
        , "virtualbox-guest-modules-arch"),
    VCD23("virtualbox-guest-utils"
        , "— VirtualBox Guest userspace utilities"
        , "virtualbox-guest-utils"),
    VCD24("vulkan-caps-viewer"
        , "— Vulkan Hardware Capability Viewer"
        , "vulkan-caps-viewer"),
    VCD25("vulkan-icd-loader"
        , "— Vulkan Installable Client Driver (ICD) Loader"
        , "vulkan-icd-loader"),
    VCD26("vulkan-intel"
        , "— Intel's Vulkan mesa driver"
        , "vulkan-intel"),
    VCD27("vulkan-radeon"
        , "— Radeon's Vulkan mesa driver"
        , "vulkan-radeon"),
    VCD28("xf86-input-vmmouse"
        , "— X.org VMWare Mouse input driver"
        , "xf86-input-vmmouse"),
    VCD29("xf86-video-amdgpu"
        , "— X.org amdgpu video driver"
        , "xf86-video-amdgpu"),
    VCD30("xf86-video-ati"
        , "— X.org ati video driver"
        , "xf86-video-ati"),
    VCD31("xf86-video-intel"
        , "— X.org Intel i810/i830/i915/945G/G965+ video drivers"
        , "xf86-video-intel"),
    VCD32("xf86-video-nouveau"
        , "— Open Source 3D acceleration driver for nVidia cards"
        , "xf86-video-nouveau"),
    VCD33("xf86-video-vmware"
        , "— X.org vmware video driver"
        , "xf86-video-vmware"),
    VCD_BRAND01("AMD / ATI"
        , "— AMDGPU (Open Source), ATI (Open Source) & AMD Catalyst (Proprietary)"
        , ""),
    VCD_BRAND02("Intel"
        , "— Intel Graphics (Open Source)"
        , ""),
    VCD_BRAND03("NVIDIA"
        , "— NVIDIA Optimus (Bumblebee), Nouveau (Open Source) & NVIDIA (Proprietary)"
        , ""),
    NFS("nfs-utils"
        , "— Support programs for Network File Systems"
        , "nfs-utils"),
    TimeSet("TimeSet"
        , "— A script & GUI for managing system date and time."
        , "timeset timeset-gui"),
    TLP1("TLP"
        , "— Linux Advanced Power Management"
        , "tlp"),
    TLP2("TLPUI"
      , "— A GTK-UI to change TLP configuration files easily. It has the aim to protect users from setting bad configuration and to deliver a basic overview of all the valid configuration values."
      , "tlpui-git"),
    Compton("Compton"
            , "— X compositor that may fix tearing issues."
            , "compton compton-conf"),
    Xcompmgr("Xcompmgr"
            , "— X compositor that may fix tearing issues."
            , "devilspie transset-df xcompmgr"),
    Tellico("Tellico"
            , "— A collection manager for KDE."
            , "tellico"),
    RainlanderLite("Rainlander Lite"
        , "— A free version of Rainlander Pro"
        , "rainlendar-lite"),
    RainlanderPro("Rainlander Pro"
                 , "— A desktop Calendar, ToDo list and Event list.  Free 30-day trial!"
                 , "rainlendar-pro"),
    AMDGPU("AMDGPU"
        , "— open source graphics driver for the latest AMD Radeon graphics cards."
        , ""),
    ATI("ATI"
        , "— radeon open source driver which supports the majority of AMD (previously ATI) GPUs."
        , ""),
    Catalyst("AMD Catalyst"
        , "— AMD proprietary Catalyst driver."
        , ""),
    Bumblebee("Bumblebee"
        , "— make NVIDIA Optimus enabled laptops work in GNU/Linux systems."
        , ""),
    Nouveau("Nouveau"
        , "— open-source driver for NVIDIA graphics cards."
        , ""),
    NvidiaProprietary("NVIDIA Proprietary"
                 , "— the proprietary NVIDIA graphics card driver."
                 , ""),
    Tint2("Tint2"
            , "— Basic, good-looking task manager for WMs."
            , "tint2 xdotool-git"),
    Conky1("Conky"
            , "— Lightweight system monitor for X"
            , "conky"),
    Conky2("Conky CLI"
            , "— Lightweight system monitor for X, without X11 dependencies"
            , "conky-cli"),
    Conky3("Conky Lua"
            , "— Lightweight system monitor for X, with Lua support enabled"
            , "conky-lua"),
    Conky4("Conky Lua NV"
            , "— An advanced system monitor for X based on torsmo with lua and nvidia enabled"
            , "conky-lua-nv"),
    Conky5("Conky Nvidia"
            , "— Lightweight system monitor for X"
            , "conky-nvidia"),
    IDE1("C/C++"
        , "— Highly extensible IDE for C++"
        , "eclipse-cpp"),
    IDE2("Etoys"
        , "— Educational tool and media-rich authoring environment for teaching children"
        , "etoys"),
    IDE3("Java"
        , "— Highly extensible IDE for Java"
        , "eclipse-java"),
    IDE4("Java EE"
        , "— Highly extensible IDE for JEE"
        , "eclipse-jee"),
    IDE5("JavaScript and Web"
        , "— Highly extensible IDE for JavaScript"
        , "eclipse-javascript"),
    IDE6("KTurtle"
        , "— Educational Programming Environment"
        , "kturtle"),
    IDE7("PHP"
        , "— Highly extensible IDE for PHP"
        , "eclipse-php"),
    IDE8("Processing"
        , "— Programming environment for creating images, animations and interactions"
        , "processing"),
    IDE9("Scratch"
        , "— Create and share your own interactive stories, games, music and art"
        , "scratch"),
    IDE10("Spring Tool Suite (STS)"
        , "— The Spring Tool Suite (STS) from SpringSource"
        , "spring-tool-suite");

    private String aurName;
    private String description;
    private String aurPckg;

    CustomSoftwareEnum(String name, String desc, String pckg) {
        this.aurName = name;
        this.description = desc;
        this.aurPckg = pckg;
    }

    public String getAurName() {
        return aurName;
    }

    public String getDescription() {
        return description;
    }

    public String getAurPckg() {
        return aurPckg;
    }
}
