package com.bitbucket.kooshballtb.isocreator.test.enums;

public enum DesktopEnvironmentsEnum {
    DE1("breeze-icons"
        , "— Breeze icon themes"),
    DE2("deepin"
        , "— AUR package group for Deepin Desktop Environment"),
    DE3("deepin-extra"
        , "— AUR package group that contains some extra applications for a more complete desktop environment."),
    DE4("ecrire-git"
        , "— Simple text editor based on EFL"),
    DE5("edi"
        , "— EFL based IDE."),
    DE6("efbb-git"
        , "— Escape from Booty Bay is an EFL-based physics game that explore EPhysics amazing features"),
    DE7("elemines-git"
        , "— an EFL minesweeper clone"),
    DE8("eluminance-git"
        , "— A fast photo browser, written in Python using EFL"),
    DE9("enjoy-git"
        , "— Music player based on EFL"),
    DE10("enlightenment"
        , "— Enlightenment window manager"),
    DE11("eperiodique"
        , "— A simple Periodic Table Of Elements viewer using the EFL"),
    DE12("ephoto"
        , "— Ephoto"),
    DE13("epour"
        , "— Torrent client based on EFL"),
    DE14("epymc-git"
        , "— Media Center based on EFL - Development version"),
    DE15("equate-git"
        , "— Calculator based on EFL"),
    DE16("eruler-git"
        , "— On-Screen Ruler and Measurement Tools using EFL"),
    DE17("gnome"
        , "— AUR package group for GNOME Desktop Environment"),
    DE18("gnome-extra"
        , "— AUR package group that contains further GNOME applications, including an archive manager, disk manager, text editor, and a set of games."),
    DE19("gnome-flashback"
        , "— GNOME Flashback session"),
    DE20("kde-applications"
        , "— AUR package group that contains the full set of KDE Applications."),
    DE21("lxqt"
        , "— AUR package group for LXQt Desktop Environment"),
    DE22("mate"
        , "— AUR package group for MATE Desktop Environment"),
    DE23("mate-extra"
        , "— AUR package group that contains additional utilities and applications that integrate well with the MATE desktop."),
    DE24("plasma"
        , "— AUR package group for KDE Plasma Desktop Environment"),
    DE25("plasma-wayland-session"
        , "— Plasma Wayland session"),
    DE26("rage"
        , "— Video Player based on EFL"),
    DE27("sugar"
        , "— Sugar GTK shell"),
    DE28("sugar-fructose"
        , "— AUR package group that contains the base activities (Fructose) including a web browser, a text editor, a media player and a terminal emulator."),
    DE29("sugar-runner"
        , "— Scripts to run Sugar"),
    DE30("terminology"
        , "— EFL based terminal emulator"),
    DE31("xfce4"
        , "— AUR package group for Xfce Desktop Environment"),
    DE32("xfce4-goodies"
        , "— AUR package group which includes extra plugins and a number of useful utilities.");

    private String aurName;
    private String description;

    DesktopEnvironmentsEnum(String name, String desc) {
        this.aurName = name;
        this.description = desc;
    }

    public String getAurName() {
        return aurName;
    }

    public String getDescription() {
        return description;
    }

}
