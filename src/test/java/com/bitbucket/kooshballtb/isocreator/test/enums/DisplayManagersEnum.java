package com.bitbucket.kooshballtb.isocreator.test.enums;

public enum DisplayManagersEnum {
    DM1("accountsservice"
        , "— D-Bus interface for user account query and manipulation"),
    DM2("breeze"
        , "— Artwork, styles and assets for the Breeze visual style for the Plasma Desktop"),
    DM3("breeze-gtk"
        , "— Breeze widget theme for GTK 2 and 3"),
    DM4("gdm3setup-utils"
        , "— Utilities to configure GDM3, autologin options and change Shell theme"),
    DM5("librsvg"
        , "— SVG rendering library"),
    DM6("lightdm-gtk-greeter"
        , "— GTK+ greeter for LightDM"),
    DM7("lightdm-gtk-greeter-settings"
        , "— Settings editor for the LightDM GTK+ Greeter"),
    DM8("light-locker"
        , "— A simple session locker for LightDM"),
    DM9("lxdm-themes"
        , "— Archlinux, ArchlinuxFull, ArchlinuxTop, Arch-Dark, Arch-Stripes and IndustrialArch lxdm themes"),
    DM10("numlockx"
        , "— Turns on the numlock key in X11."),
    DM11("qiv"
        , "— Quick Image Viewer (qiv) is a very small and fast GDK/Imlib image viewer"),
    DM12("sddm-config-editor-git"
        , "— SDDM Configuration Editor"),
    DM13("xdm-archlinux"
        , "— An XDM setup that looks better than the defaults");

    private String aurName;
    private String description;

    DisplayManagersEnum(String name, String desc) {
        this.aurName = name;
        this.description = desc;
    }

    public String getAurName() {
        return aurName;
    }

    public String getDescription() {
        return description;
    }

}
