package com.bitbucket.kooshballtb.isocreator.test.enums;

public enum JavaDevKitEnum {
  JDK1("Java SE 8"
      , "— Oracle Java 8 Development Kit"
      , "jdk8"),
  JDK2("Java SE 9"
      , "— Oracle Java 9 Development Kit (public release - end of support)"
      , "jdk9"),
  JDK3("Java SE 10"
      , "— Oracle Java 10 Development Kit (public release - end of support)"
      , "jdk10"),
  JDK4("Java SE 11"
      , "— Oracle Java Development Kit"
      , "jdk11"),
  JDK5("OpenJDK 8"
      , "— OpenJDK Java 8 development kit"
      , "jdk8-openjdk"),
  JDK6("OpenJDK 10"
      , "— OpenJDK Java 10 development kit"
      , "jdk10-openjdk"),
  JDK7("OpenJDK 11"
      , "— OpenJDK Java 11 development kit"
      , "jdk-openjdk");

  private String aurName;
  private String description;
  private String aurPckg;

  JavaDevKitEnum(String name, String desc, String pckg) {
    this.aurName = name;
    this.description = desc;
    this.aurPckg = pckg;
  }

  public String getAurName() {
    return aurName;
  }

  public String getDescription() {
    return description;
  }

  public String getAurPckg() {
    return aurPckg;
  }
}
