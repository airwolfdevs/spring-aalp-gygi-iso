package com.bitbucket.kooshballtb.isocreator.test.enums;

public enum WindowManagersEnum {
    WM1("dmenu"
        , "— Generic menu for X"),
    WM2("dzen2"
        , "— General purpose messaging, notification and menuing program for X11"),
    WM3("emacs"
        , "— The extensible, customizable, self-documenting real-time display editor"),
    WM4("emerald"
        , "— Emerald window decorator"),
    WM5("emerald-themes"
        , "— Themes for emerald"),
    WM6("fusion-icon"
        , "— Simple tray icon for Compiz 0.8"),
    WM7("gtk2-perl"
        , "— Perl bindings for GTK+ 2.x"),
    WM8("ipager"
        , "— a lightweight beautiful pager from Fluxbox"),
    WM9("j4-dmenu-desktop-git"
        , "— A rewrite of i3-dmenu-desktop, which is much faster"),
    WM10("lxappearance-obconf"
        , "— Plugin for LXAppearance to configure Openbox"),
    WM11("lxrandr"
        , "— Monitor configuration tool (part of LXDE)"),
    WM12("obapps"
        , "— Graphical tool for configuring application settings in Openbox."),
    WM13("ob-autostart"
        , "— A simple autostart application for Openbox"),
    WM14("obconf"
        , "— A GTK+ based configuration tool for the Openbox windowmanager"),
    WM15("obkey-git"
        , "— Openbox Key Editor - converted to PyGObject."),
    WM16("oblogout"
        , "— Openbox logout script"),
    WM17("obmenu"
        , "— Openbox menu editor."),
    WM18("obmenu-generator"
        , "— A fast pipe/static menu generator for the Openbox Window Manager (with icons support)."),
    WM19("openbox-themes"
        , "— Various themes for the Openbox window manager."),
    WM20("ourico"
        , "— A lightweight EWMH taskbar, originally designed for echinus"),
    WM21("pekwm-menu"
        , "— Dynamic xdg menu creator for pekwm"),
    WM22("swayidle"
        , "— Idle management daemon for Wayland"),
    WM23("swaylock"
        , "— Screen locker for Wayland"),
    WM24("transset-df"
        , "— A patched version of X.Org's transset with added functionality."),
    WM25("windowmaker-extra"
        , "— Extra WindowMaker icons and themes"),
    WM26("xcompmgr"
        , "— Composite Window-effects manager for X.org"),
    WM27("xfce4-settings"
        , "— Settings manager for xfce"),
    WM28("xfwm4-themes"
        , "— A set of additional themes for the Xfce window manager"),
    WM29("xmobar"
        , "— Minimalistic Text Based Status Bar"),
    WM30("xmonad-contrib"
        , "— Add-ons for xmonad");

    private String aurName;
    private String description;

    WindowManagersEnum(String name, String desc) {
        this.aurName = name;
        this.description = desc;
    }

    public String getAurName() {
        return aurName;
    }

    public String getDescription() {
        return description;
    }

}
