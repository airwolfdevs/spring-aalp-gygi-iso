#!/bin/bash
#======================================================================================
#
# Author  : Thomas Bednarek
# License : This program is free software: you can redistribute it and/or modify
#           it under the terms of the GNU General Public License as published by
#           the Free Software Foundation, either version 3 of the License, or
#           (at your option) any later version.
#
#           This program is distributed in the hope that it will be useful,
#           but WITHOUT ANY WARRANTY; without even the implied warranty of
#           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#           GNU General Public License for more details.
#
#           You should have received a copy of the GNU General Public License
#           along with this program.  If not, see <http://www.gnu.org/licenses/>.
#======================================================================================

#===============================================================================
# globals
#===============================================================================
declare HXU_NORMALIZE="hxnormalize"

source "/home/mustang/Workspace/IdeaProjects/Spring-AALP-GYGInstaller/src/main/resources/AALP-GYGInstaller/scripts/inc/log_funcs.sh"

#===============================================================================
# functions/methods
#===============================================================================

appendEnum() {
  local pckgStr="$1"
  local varName="$2"
  local fileName="$3"
  local aurPckgs=()
  local desc=()
  local sep=":"

  readarray -t aurPckgs <<< "$pckgStr"
  local lastIdx=$(expr ${#aurPckgs[@]} - 1)

  for aryIdx in $(eval echo "{0..$lastIdx}"); do
    local pckg="${aurPckgs[${aryIdx}]}"
    case "$pckg" in
      "xorg")
        desc=("Description" " includes packages from xorg-server, and packages from the xorg-apps group and fonts.")
      ;;
      "xorg-apps")
        desc=("Description" " includes some packages that are necessary for certain configuration tasks")
      ;;
      *)
        local cmdOutput=$(trizen -Si "$pckg")
        readarray -t lines <<< "$cmdOutput"
        for line in "${lines[@]}"; do
          if [[ "$line" =~ "Description" ]]; then
            readarray -t desc <<< "${line//$sep/$'\n'}"
            break
          fi
        done
      ;;
    esac

    local enumStr=$(printf "%s%d(\"%s\"" "$varName" $(expr ${aryIdx} + 1) "$pckg")
    lines=("$enumStr" ", \"—${desc[1]}\"" ", \"${pckg}\"),")
    local concatStr=$(printf "\n%s" "${lines[@]}")
    echo "${concatStr:1}" >> "$fileName"
  done
}


declare -A CUST_SOFT=(
["Glassfish"]="glassfish5"
["Jetty"]="jetty"
["WildFly"]="wildfly"
["DockbarX"]="dockbarx"
["Profile Sync Daemon"]="profile-sync-daemon"
["SpeedTest-CLI"]="speedtest-cli"
["Volume Wheel"]="volwheel"
["ARandR"]="arandr"
["QDirStat"]="qdirstat"
["QSpectrumAnalyzer"]="qspectrumanalyzer"
["DMenu2"]="dmenu2"
["Rofi"]="rofi"
["Flareget"]="flareget"
["QFtp"]="qftp"
["LogWatch"]="logwatch"
["MultiTail"]="multitail"
["Swatch"]="swatchdog"
["Calendar"]="calendar"
["California"]="california"
["GSimpleCal"]="gsimplecal"
["MineTime"]="minetime"
["Darktable"]="darktable"
["Gimp"]="gimp"
["Pencil"]="pencil"
["MPV"]="mpv"
["Feh"]="feh"
["Eclipse Modeling Tools"]="eclipse-modeling-tools"
["LilyTerm"]="lilyterm-git"
["Sakura"]="sakura"
["Xfce4 Terminal"]="xfce4-terminal"
["TigerVNC"]="tigervnc"
["X2Go Client"]="x2goclient"
["Adapta"]="adapta-gtk-theme"
["Arc Faenza"]="arc-faenza-icon-theme"
["Arc GTK"]="arc-gtk-theme"
["Arc Icon"]="arc-icon-theme"
["Arc OSX"]="osx-arc-shadow"
["Breeze"]="breeze-gtk"
["Faience GTK"]="faience-themes"
["Faience Icon"]="faience-ng-icon-theme"
["Greybird"]="xfce-theme-greybird"
["GNOME-Breeze"]="gnome-breeze-git"
["GNOME-Colors"]="gnome-colors-icon-theme"
["Moka"]="moka-icon-theme"
["Numix Darkblue"]="numix-themes-darkblue"
["Numix GTK"]="numix-gtk-theme"
["Numix Solarized"]="gtk-theme-numix-solarized"
["Numix Circle & Square Icons"]="numix-icon-theme-pack-git"
["0nyX"]="onyx-suite"
["Obsidian"]="obsidian-icon-theme"
["Vibrancy Colors"]="vibrancy-colors"
["Vivacious Colors"]="vivacious-colors-gtk-theme"
["Zuki"]="zuki-themes"
["Zukitwo"]="zukitwo-themes-git"
["Xlockmore"]="xlockmore"
["XScreenSaver (Arch Logo)"]="xscreensaver-arch-logo"
["Tomcat Native"]="tomcat-native"
["Tomcat v7.0.*-1"]="tomcat7"
["Tomcat v8.5.*-1"]="tomcat8"
["Tomcat v9.0.*-1"]="tomcat9"
["Unagi"]="unagi"
["IntelliJ IDEA CE"]="intellij-idea-ce"
["IntelliJ IDEA UE"]="intellij-idea-ultimate-edition"
["SlimJet"]="slimjet"
["Aura"]="aura-git"
["Pacaur"]="pacaur"
["Packer"]="packer-aur-git"
["Pakku"]="pakku"
["Pikaur"]="pikaur"
["Trizen"]="trizen"
["Yay"]="yay"
["Argon"]="argon"
["Cylon"]="cylon"
["Pakku GUI"]="pakku-gui"
["Pamac"]="pamac-aur"
["PkgBrowser"]="pkgbrowser"
["Octopi"]="octopi"
["Flameshot"]="flameshot"
["GScreenShot"]="gscreenshot"
["HotShots"]="hotshots-git"
["Lightscreen"]="lightscreen"
["KShare"]="kshare"
["Shutter"]="shutter"
["Firewall Builder"]="fwbuilder"
["FirewallD"]="firewalld"
["Gufw"]="gufw"
["PeerGuardian GUI"]="pgl-git"
["Odrive"]="odrive"
["Kimai"]="kimai-git"
["Project Hamster"]="python-hamster-gtk-git"
["TimeCamp"]="timecamp"
["TimeSlotTracker"]="timeslottracker"
["Toggl"]="toggldesktop-bin"
["Cockpit"]="cockpit"
["Cumulus"]="cumulus"
["GIS Weather"]="gis-weather-git"
["GNOME-Weather"]="gnome-weather"
["KDE Plasma Weather Widget"]="plasma5-applets-weather-widget"
["Meteo-Qt"]="meteo-qt"
["My Weather Indicator"]="my-weather-indicator-git"
["Nextcloud"]="nextcloud"
["ImageMagick"]="imagemagick-full"
["GraphicsMagick"]="graphicsmagick"
["GVim"]="gvim"
["Amp"]="amp"
["DTE"]="dte"
["Emacs"]="emacs"
["Micro"]="micro"
["Nano"]="nano"
["Neovim"]="neovim"
["Vile"]="vile"
["Vim"]="vim"
["Zile"]="zile"
["C shell(tcsh)"]="tcsh"
["Fish"]="fish"
["Oh"]="oh-git"
["PowerShell"]="powershell-bin"
["Dropbear"]="dropbear"
["OpenSSH"]="openssh"
["TinySSH"]="tinyssh"
["GtkLP"]="gtklp"
["Print Manager"]="print-manager"
["System Config Printer"]="system-config-printer"
["samba"]="samba"
["smbnetfs"]="smbnetfs"
["zramswap"]="zramswap"
["Poseidon"]="poseidon-browser-git"
["Devilspie"]="devilspie"
["KMenuEdit"]="kmenuedit"
["Fbpanel"]="fbpanel"
["Sublime Text"]="sublime-text-dev"
)

appendCustSoft() {
  local varName="$1"
  local fileName="$2"
  local varCntr=1
  local sortedKeys=$(printf "%s\n" "${!CUST_SOFT[@]}" | sort)
  local keys=()

  readarray -t keys <<< "$sortedKeys"

  for aurPckgName in "${keys[@]}"; do
    local pckg="${CUST_SOFT["$aurPckgName"]}"
    clog_info "[$aurPckgName]=\"$pckg\""
    local cmdOutput=$(trizen -Si "$pckg")
    readarray -t lines <<< "$cmdOutput"
    for line in "${lines[@]}"; do
      if [[ "$line" =~ "Description" ]]; then
        readarray -t desc <<< "${line//$sep/$'\n'}"
        break
      fi
    done
    local enumStr=$(printf "%s%d(\"%s\"" "$varName" ${varCntr} "$aurPckgName")
    lines=("$enumStr" ", \"—${desc[1]}\"" ", \"${pckg}\"),")
    local concatStr=$(printf "\n%s" "${lines[@]}")
    echo "${concatStr:1}" >> "$fileName"
    varCntr=$(expr ${varCntr} + 1)
  done
}

declare -A VIDEO_DRIVERS=(
["xf86-video-vmware"]="xf86-video-vmware"
["xf86-input-vmmouse"]="xf86-input-vmmouse"
["open-vm-tools"]="open-vm-tools"
["open-vm-tools-dkms"]="open-vm-tools-dkms"
["gtkmm3"]="gtkmm3"
["xf86-video-intel"]="xf86-video-intel"
["lib32-mesa"]="lib32-mesa"
["virtualbox-guest-modules-arch"]="virtualbox-guest-modules-arch"
["virtualbox-guest-utils"]="virtualbox-guest-utils"
["catalyst-test"]="catalyst-test"
["catalyst-libgl"]="catalyst-libgl"
["lib32-catalyst-libgl"]="lib32-catalyst-libgl"
["catalyst-utils"]="catalyst-utils"
["acpid"]="acpid"
["qt4"]="qt4"
["xf86-video-amdgpu"]="xf86-video-amdgpu"
["bumblebee"]="bumblebee"
["nvidia"]="nvidia"
["nvidia-dkms"]="nvidia-dkms"
["nvidia-lts"]="nvidia-lts"
["nvidia-zen"]="nvidia-zen"
["xf86-video-intel"]="xf86-video-intel"
["lib32-virtualgl"]="lib32-virtualgl"
["lib32-nvidia-utils"]="lib32-nvidia-utils"
["nvidia-utils"]="nvidia-utils"
["nvidia-settings"]="nvidia-settings"
["xf86-video-nouveau"]="xf86-video-nouveau"
["virtualbox-guest-dkms"]="virtualbox-guest-dkms"
["virtualbox-guest-utils"]="virtualbox-guest-utils"
["xf86-video-ati"]="xf86-video-ati"
["vulkan-icd-loader"]="vulkan-icd-loader"
["lib32-vulkan-icd-loader"]="lib32-vulkan-icd-loader"
["vulkan-intel"]="vulkan-intel"
["vulkan-radeon"]="vulkan-radeon"
["vulkan-caps-viewer"]="vulkan-caps-viewer"
)

appendSoftForVideoDrivers() {
  local varName="$1"
  local fileName="$2"
  local varCntr=1
  local sortedKeys=$(printf "%s\n" "${!VIDEO_DRIVERS[@]}" | sort)
  local keys=()

  readarray -t keys <<< "$sortedKeys"

  for aurPckgName in "${keys[@]}"; do
    local pckg="${VIDEO_DRIVERS["$aurPckgName"]}"
    clog_info "[$aurPckgName]=\"$pckg\""
    local cmdOutput=$(trizen -Si "$pckg")
    readarray -t lines <<< "$cmdOutput"
    for line in "${lines[@]}"; do
      if [[ "$line" =~ "Description" ]]; then
        readarray -t desc <<< "${line//$sep/$'\n'}"
        break
      fi
    done
    local enumStr=$(printf "%s%d(\"%s\"" "$varName" ${varCntr} "$aurPckgName")
    lines=("$enumStr" ", \"—${desc[1]}\"" ", \"${pckg}\"),")
    local concatStr=$(printf "\n%s" "${lines[@]}")
    echo "${concatStr:1}" >> "$fileName"
    varCntr=$(expr ${varCntr} + 1)
  done
}

declare -A DesktopEnvExtras=(
["gnome-flashback"]="gnome-flashback"
["gnome"]="gnome"
["gnome-extra"]="gnome-extra"
["enlightenment"]="enlightenment"
["ecrire-git"]="ecrire-git"
["edi"]="edi"
["eluminance-git"]="eluminance-git"
["enjoy-git"]="enjoy-git"
["eperiodique"]="eperiodique"
["ephoto"]="ephoto"
["epour"]="epour"
["epymc-git"]="epymc-git"
["equate-git"]="equate-git"
["eruler-git"]="eruler-git"
["efbb-git"]="efbb-git"
["elemines-git"]="elemines-git"
["terminology"]="terminology"
["rage"]="rage"
["mate"]="mate-extra"
["mate-extra"]="mate-extra"
["deepin"]="deepin"
["deepin-extra"]="deepin-extra"
["xfce4"]="xfce4"
["xfce4-goodies"]="xfce4-goodies"
["plasma"]="plasma"
["plasma-wayland-session"]="plasma-wayland-session"
["kde-applications"]="kde-applications"
["lxqt"]="lxqt"
["breeze-icons"]="breeze-icons"
["sugar"]="sugar"
["sugar-fructose"]="sugar-fructose"
["sugar-runner"]="sugar-runner"
)
declare -A WinManExtras=(
["emerald"]="emerald"
["emerald-themes"]="emerald-themes"
["fusion-icon"]="fusion-icon"
["obconf"]="obconf"
["lxappearance-obconf"]="lxappearance-obconf"
["openbox-themes"]="openbox-themes"
["lxrandr"]="lxrandr"
["ob-autostart"]="ob-autostart"
["obapps"]="obapps"
["obkey-git"]="obkey-git"
["oblogout"]="oblogout"
["pekwm-menu"]="pekwm-menu"
["windowmaker-extra"]="windowmaker-extra"
["xfce4-settings"]="xfce4-settings"
["xfwm4-themes"]="xfwm4-themes"
["j4-dmenu-desktop-git"]="j4-dmenu-desktop-git"
["transset-df"]="transset-df"
["xcompmgr"]="xcompmgr"
["swaylock"]="swaylock"
["swayidle"]="swayidle"
["dzen2"]="dzen2"
["dmenu"]="dmenu"
["ipager"]="ipager"
["ourico"]="ourico"
["xmonad-contrib"]="xmonad-contrib"
["xmobar"]="xmobar"
["xterm"]="xterm"
["emacs"]="emacs"
["gtk2-perl"]="gtk2-perl"
["obmenu"]="obmenu"
["obmenu-generator"]="obmenu-generator"
)

declare -A DispManExtras=(
["gdm3setup-utils"]="gdm3setup-utils"
["accountsservice"]="accountsservice"
["light-locker"]="light-locker"
["lightdm-gtk-greeter"]="lightdm-gtk-greeter"
["lightdm-gtk-greeter-settings"]="lightdm-gtk-greeter-settings"
["numlockx"]="numlockx"
["librsvg"]="librsvg"
["lxdm-themes"]="lxdm-themes"
["breeze"]="breeze"
["breeze-gtk"]="breeze-gtk"
["sddm-config-editor-git"]="sddm-config-editor-git"
["qiv"]="qiv"
["xdm-archlinux"]="xdm-archlinux"
)

appendSoftExtras() {
  local varName="$1"
  local fileName="$2"
  local -n assocArray="$3"
  local varCntr=1
  local sortedKeys=$(printf "%s\n" "${!assocArray[@]}" | sort)
  local keys=()

  readarray -t keys <<< "$sortedKeys"

  for aurPckgName in "${keys[@]}"; do
    local pckg="${assocArray["$aurPckgName"]}"
    clog_info "[$aurPckgName]=\"$pckg\""

    local seDesc=("" " AUR package group ")
    local cmdOutput=$(trizen -Si "$pckg")
    if [ ${#cmdOutput} -gt 0 ]; then
      readarray -t lines <<< "$cmdOutput"
      for line in "${lines[@]}"; do
        if [[ "$line" =~ "Description" ]]; then
          readarray -t seDesc <<< "${line//$sep/$'\n'}"
          break
        fi
      done
    fi

    local enumStr=$(printf "%s%d(\"%s\"" "$varName" ${varCntr} "$aurPckgName")
    lines=("$enumStr" ", \"—${seDesc[1]}\"),")
    local concatStr=$(printf "\n%s" "${lines[@]}")
    echo "${concatStr:1}" >> "$fileName"
    varCntr=$(expr ${varCntr} + 1)
  done
}

declare -A IDEs=(
["Etoys"]="etoys"
["KTurtle"]="kturtle"
["Processing"]="processing"
["Scratch"]="scratch"
["Java EE"]="eclipse-jee"
["Java"]="eclipse-java"
["C/C++"]="eclipse-cpp"
["PHP"]="eclipse-php"
["JavaScript and Web"]="eclipse-javascript"
["Spring Tool Suite (STS)"]="spring-tool-suite"
)

appendIDEs() {
  local varName="$1"
  local fileName="$2"
  local varCntr=1
  local sortedKeys=$(printf "%s\n" "${!IDEs[@]}" | sort)
  local keys=()

  readarray -t keys <<< "$sortedKeys"

  for aurPckgName in "${keys[@]}"; do
    local pckg="${IDEs["$aurPckgName"]}"
    clog_info "[$aurPckgName]=\"$pckg\""
    local cmdOutput=$(trizen -Si "$pckg")
    readarray -t lines <<< "$cmdOutput"
    for line in "${lines[@]}"; do
      if [[ "$line" =~ "Description" ]]; then
        readarray -t desc <<< "${line//$sep/$'\n'}"
        break
      fi
    done
    local enumStr=$(printf "%s%d(\"%s\"" "$varName" ${varCntr} "$aurPckgName")
    lines=("$enumStr" ", \"—${desc[1]}\"" ", \"${pckg}\"),")
    local concatStr=$(printf "\n%s" "${lines[@]}")
    echo "${concatStr:1}" >> "$fileName"
    varCntr=$(expr ${varCntr} + 1)
  done
}

declare -A DB_PCKGS=(
["Apache Cassandra"]="cassandra"
["Apache CouchDB"]="couchdb"
["Apache HBase"]="hbase"
["MongoDB"]="mongodb-bin"
["Neo4j"]="neo4j-community"
["OrientDB"]="orientdb-community"
["Redis"]="redis"
["RethinkDB"]="rethinkdb"
["MariaDB"]="mariadb"
["MiraDB"]="miradb"
["MySQL"]="mysql"
["PostgreSQL"]="postgresql"
["SQLite"]="sqlite"
["SQL Server"]="mssql-server"
["Apache Lucene"]="apache-lucene"
["Apache Solr"]="solr"
["Elasticsearch"]="elasticsearch"
["Adminer"]="adminer"
["DBeaver"]="dbeaver"
["MongoDB GUI"]="mongodb-compass"
["MongoDB Tools"]="mongodb-tools-bin"
["MySQL Workbench"]="mysql-workbench"
["Orbada"]="orbada"
["pgAdmin"]="pgadmin4"
["pgModeler"]="pgmodeler"
["Sequeler"]="sequeler-git"
["SQLite Browser"]="sqlitebrowser"
["SQL Server® Tools"]="mssql-tools"
["TOra"]="tora"
)

appendDatabaseSoft() {
  local varName="$1"
  local fileName="$2"
  local -n assocArray="$3"
  local varCntr=1
  local sortedKeys=$(printf "%s\n" "${!assocArray[@]}" | sort)
  local keys=()
  local desc=()

  readarray -t keys <<< "$sortedKeys"

  for aurPckgName in "${keys[@]}"; do
    local pckg="${assocArray["$aurPckgName"]}"
    clog_info "[$aurPckgName]=\"$pckg\""
    case "$pckg" in
      "redis")
        desc=("an open source (BSD licensed), in-memory data"
              "structure store, used as a database, cache and message broker.")
      ;;
      *)
        local cmdOutput=$(trizen -Si "$pckg")
        readarray -t lines <<< "$cmdOutput"
        for line in "${lines[@]}"; do
          if [[ "$line" =~ "Description" ]]; then
            readarray -t desc <<< "${line//$sep/$'\n'}"
            break
          fi
        done
      ;;
    esac
    local enumStr=$(printf "%s%d(\"%s\"" "$varName" ${varCntr} "$aurPckgName")
    lines=("$enumStr" ", \"—${desc[1]}\"" ", \"${pckg}\"),")
    local concatStr=$(printf "\n%s" "${lines[@]}")
    echo "${concatStr:1}" >> "$fileName"
    varCntr=$(expr ${varCntr} + 1)
  done
}

declare -A CUSTOM_PCKGS=(
["Alacritty Terminal"]="alacritty"
["Apache Tomcat v7.0.*-1"]="tomcat7"
["Apache Tomcat v8.5.*-1"]="tomcat8"
["Apache Tomcat v9.0.*-1"]="tomcat9"
["Nexus OSS"]="nexus-oss"
["PostgreSQL DBMS"]="postgresql"
["Auto Background Changer"]="python-autobgch"
)

declare -A CUSTOM_SCRIPTS=(
["alacritty"]="software-install/install-alacritty.sh"
["tomcat7"]="software-install/Tomcat/install-tomcat.sh -tv \\\"v7\\\""
["tomcat8"]="software-install/Tomcat/install-tomcat.sh -tv \\\"v8\\\""
["tomcat9"]="software-install/Tomcat/install-tomcat.sh -tv \\\"v9\\\""
["nexus-oss"]="software-install/Nexus-OSS/install-nexus-oss.sh -part \\\"/nexus-oss\\\""
["postgresql"]="software-install/install-postgresql.sh -part \\\"/dbhome\\\""
["python-autobgch"]="software-install/auto-background-changer.sh"
)

appendCustPckgScripts() {
  local varName="CPS"
  local fileName="$1"
  local varCntr=1
  local sortedKeys=$(printf "%s\n" "${!CUSTOM_PCKGS[@]}" | sort)
  local keys=()
  local desc=()

  readarray -t keys <<< "$sortedKeys"

  for aurPckgName in "${keys[@]}"; do
    local pckg="${CUSTOM_PCKGS["$aurPckgName"]}"
    local scriptName="${CUSTOM_SCRIPTS["$pckg"]}"
    clog_info "[$aurPckgName]=\"$pckg\""
    local cmdOutput=$(trizen -Si "$pckg")
    readarray -t lines <<< "$cmdOutput"
    for line in "${lines[@]}"; do
      if [[ "$line" =~ "Description" ]]; then
        readarray -t desc <<< "${line//$sep/$'\n'}"
        break
      fi
    done
    local enumStr=$(printf "%s%d(\"%s\"" "$varName" ${varCntr} "$aurPckgName")
    lines=("$enumStr" ", \"—${desc[1]}\"" ", \"${scriptName}\"),")
    local concatStr=$(printf "\n%s" "${lines[@]}")
    echo "${concatStr:1}" >> "$fileName"
    varCntr=$(expr ${varCntr} + 1)
  done
}

declare -A ConkyPackages=(
["Conky"]="conky"
["Conky CLI"]="conky-cli"
["Conky Lua"]="conky-lua"
["Conky Lua NV"]="conky-lua-nv"
["Conky Nvidia"]="conky-nvidia"
)

appendConkyPckgs() {
  local varName="$1"
  local fileName="$2"
  local varCntr=1
  local sortedKeys=$(printf "%s\n" "${!ConkyPackages[@]}" | sort)
  local keys=()

  readarray -t keys <<< "$sortedKeys"

  for aurPckgName in "${keys[@]}"; do
    local pckg="${ConkyPackages["$aurPckgName"]}"
    clog_info "[$aurPckgName]=\"$pckg\""
    local cmdOutput=$(trizen -Si "$pckg")
    readarray -t lines <<< "$cmdOutput"
    for line in "${lines[@]}"; do
      if [[ "$line" =~ "Description" ]]; then
        readarray -t desc <<< "${line//$sep/$'\n'}"
        break
      fi
    done
    local enumStr=$(printf "%s%d(\"%s\"" "$varName" ${varCntr} "$aurPckgName")
    lines=("$enumStr" ", \"—${desc[1]}\"" ", \"${pckg}\"),")
    local concatStr=$(printf "\n%s" "${lines[@]}")
    echo "${concatStr:1}" >> "$fileName"
    varCntr=$(expr ${varCntr} + 1)
  done
}

#---------------------------------------------------------------------------------------
#  MAIN
main() {
  local cups=(
    "cups"
    "cups-pdf"
    "gutenprint"
    "ghostscript"
    "gsfonts"
    "gsfonts-type1"
    "foomatic-db"
    "foomatic-db-engine"
    "foomatic-db-nonfree"
    "foomatic-db-ppds"
    "foomatic-db-nonfree-ppds"
    "foomatic-db-gutenprint-ppds"
  )

  local lines=()
  local desc=()
  local sep=":"
  local lastIdx=$(expr ${#codec[@]} - 1)
  local fileName="pckgDescs.txt"
  if [ -f "$fileName" ]; then
    rm -rf "$fileName"
  fi

  local concatStr=$(printf "\n%s" "${cups[@]}")
  appendEnum "${concatStr:1}" "CUPS" "$fileName"

  appendXorg "$fileName"
}

appendXorg() {
  local fileName="$1"
  local Xorg=(
    "xf86-input-evdev"
    "xf86-input-libinput"
    "xorg-apps"
    "xorg-xinit"
    "xorg-twm"
    "xorg-xclock"
    "mesa"
    "xorg"
    "ttf-google-fonts-git")
  local concatStr=$(printf "\n%s" "${Xorg[@]}")
  appendEnum "${concatStr:1}" "Xorg" "$fileName"
}

appendCodecs() {
  local fileName="$1"
  local codec=(
    "codecs64"
    "v4l-utils"
    "alsa-utils"
    "alsa-plugins"
    "alsa-firmware"
    "gstreamer"
    "gst-plugin-libde265"
    "pulseaudio"
    "pulseaudio-alsa"
    "pavucontrol"
    "ffmpegthumbnailer"
    "phonon-qt4-gstreamer"
    "phonon-qt5-gstreamer"
    "kdegraphics-thumbnailers"
    "ffmpegthumbs"
  )
  local concatStr=$(printf "\n%s" "${codec[@]}")
  appendEnum "${concatStr:1}" "Codec" "$fileName"
}

appendJavaDevKits() {
  local varName="$1"
  local fileName="$2"
  local varCntr=1
  local startVer=8
  local endVer=11
  local jdkNames=("Java SE" "OpenJDK")
  local -A fmts=(["Java SE"]="jdk%d" ["OpenJDK"]="jdk%d-openjdk")
  local jdkPckg=""
  local choicesArray=()

  for jdkName in "${jdkNames[@]}"; do
    for ver in $(eval echo "{${startVer}..${endVer}}"); do
      if [ ${ver} -gt 10 ] && [ "$jdkName" ==  "OpenJDK" ]; then
        jdkPckg="jdk-openjdk"
      else
        jdkPckg=$(printf "${fmts["$jdkName"]}" ${ver})
      fi
      clog_info "jdkPckg=[$jdkPckg]"
      local cmdOutput=$(trizen -Si "$jdkPckg")
      if [ ${#cmdOutput} -gt 0 ]; then
        readarray -t lines <<< "$cmdOutput"
        for line in "${lines[@]}"; do
          if [[ "$line" =~ "Description" ]]; then
            readarray -t desc <<< "${line//$sep/$'\n'}"
            break
          fi
        done
        local aurPckgName=$(printf "%s %d" "$jdkName" ${ver})
        local enumStr=$(printf "%s%d(\"%s\"" "$varName" ${varCntr} "$aurPckgName")
        lines=("$enumStr" ", \"—${desc[1]}\"" ", \"${jdkPckg}\"),")
        local concatStr=$(printf "\n%s" "${lines[@]}")
        echo "${concatStr:1}" >> "$fileName"
        varCntr=$(expr ${varCntr} + 1)
        choicesArray+=("$aurPckgName")
      else
        clog_error "No entry found for \"$jdkPckg\""
      fi
    done
  done

  updateFilesForJavaChoices "choicesArray" "$fileName"
}

updateFilesForJavaChoices() {
  local -n javaChoices="$1"
  local fileName="$2"
  local mainDir="/home/mustang/Workspace/IdeaProjects/Spring-AALP-GYGInstaller/src/main"
  local javaFileName="${mainDir}/java/com/bitbucket/kooshballtb/common/JavaDevKitEnum.java"
  local choicesFile="${mainDir}/resources/config/choices.txt"
  local lastLineNum=$(cat "$fileName" | wc -l)
  local lastLine=$(sed "${lastLineNum}q;d" "$fileName")

  #### JavaDevKitEnum.java
  sed -i "${lastLineNum}s/,$/;/g" "$fileName"
  local start=$(grep -n '^public' "$javaFileName" | cut -d : -f1)
  local end=$(grep -n ');' "$javaFileName" | cut -d : -f1)
  start=$(expr ${start} + 1)
  sed -i "${start},${end}d" "$javaFileName"
  start=$(expr ${start} - 1)
  sed -i -e "${start}r $fileName" "$javaFileName"

  #### choices.txt
  start=$(grep -n '^Java Development Kits' "$choicesFile" | cut -d : -f1)
  start=$(expr ${start} + 1)

  end=$(sed -n "${start},/^\s*$/{=}" "$choicesFile" | tail -1)
  end=$(expr ${end} - 1)
  sed -i "${start},${end}d" "$choicesFile"
  start=$(expr ${start} - 1)

  local concatStr=$(printf "\n  %s" "${javaChoices[@]}")
  echo -e "${concatStr:1}" > "/tmp/javaChoices.txt"
  sed -i -e "${start}r /tmp/javaChoices.txt" "$choicesFile"
}

debugMain() {
  local lines=()
  local desc=()
  local sep=":"
  local lastIdx=$(expr ${#codec[@]} - 1)
  local fileName="pckgDescs.txt"
  if [ -f "$fileName" ]; then
    rm -rf "$fileName"
  fi

  #appendCustSoft "CustSoft" "$fileName"
  #appendSoftForVideoDrivers "VCD" "$fileName"
  #appendSoftExtras "DE" "$fileName" "DesktopEnvExtras"
  #appendXorg "$fileName"
  #appendCodecs "$fileName"
  #appendSoftExtras "WM" "$fileName" "WinManExtras"
  #appendSoftExtras "DM" "$fileName" "DispManExtras"
  #appendIDEs "IDE" "$fileName"
  #appendJavaDevKits "JDK" "$fileName"
  #modifyFiles "$fileName"
  #appendDatabaseSoft "DB" "$fileName" "DB_PCKGS"
  #appendCustPckgScripts "$fileName"
  appendConkyPckgs "Conky" "$fileName"
}

debugMain "$@"
#main "$@"
exit 0
